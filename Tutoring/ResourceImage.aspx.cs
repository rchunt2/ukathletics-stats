﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Tutoring
{
    public partial class ResourceImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var resourceId = Request.QueryString["ResourceID"];
            if (!string.IsNullOrEmpty(resourceId))
            {
                Guid actualId;
                if (Guid.TryParse(resourceId, out actualId))
                {
                    using (var context = new STATSEntities())
                    {
                        var resource = context.Resources.Include("ResourcePicture").SingleOrDefault(x => x.ResourceID == actualId);
                        if (resource != null)
                        {
                            var resourcePic = resource.ResourcePicture != null ? resource.ResourcePicture.ResourcePictureBytes : null;
                            if (resourcePic != null)
                            {
                                ResourcePictureImg.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(resourcePic);
                                ResourcePictureImg.Visible = true;
                                NoImageLabel.Visible = false;
                            }
                        }
                    }
                }
            }
        }
    }
}