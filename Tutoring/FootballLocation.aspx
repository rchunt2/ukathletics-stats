﻿<%@ Page Title="STATS Tutoring - Football Location" Language="C#" AutoEventWireup="true" MasterPageFile="~/UserAuthentication.Master" CodeBehind="FootballLocation.aspx.cs" Inherits="UK.STATS.Tutoring.FootballLocation" EnableEventValidation="false" %>

<%@ Register TagPrefix="SIS" TagName="ApplicationVersion" Src="~/ApplicationVersion.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="./Stylesheets/Tutoring.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/jquery.qtip.min.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True"></asp:ScriptManager>
	<script src="./Scripts/jquery-1.10-min.js" type="text/javascript"></script>
    <script src="./Scripts/jquery.qtip.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	    function alertRoom() {
	        var roomNumber = $('input[id*="hfRoomNumber"][type="hidden"]').val();
//			alert("roomNumber: " + roomNumber);
	        if (roomNumber != "") {
	            if (roomNumber == "Personal") {
	                alert("Please go to your personally designated room.");
	                $('input[id*="hfRoomNumber"][type="hidden"]').val("");
	            }
	            else {
	                alert("Please go to room " + roomNumber + ".");
	                $('input[id*="hfRoomNumber"][type="hidden"]').val("");
	            }
	        }
	        
	        $('#ContentBody_upWaiting table tbody tr').find('td:eq(0)').each(function () {
	            $(this).qtip({
	                content: {
	                    text: function (event, api) {
	                        $.ajax({
	                            url: './ResourceImage.aspx?ResourceId=' + $.trim($(this).parent().find('td:eq(3)').text())
	                        })
                            .then(function (content) {
                                api.set('content.text', content);
                            }, function (xhr, status, error) {
                                api.set('content.text', status + ': ' + error);
                            });

	                        return 'Loading...';
	                    }
	                },
	                position: {
	                    my: 'left center',
	                    at: 'right center',
	                    target: $(this).parent()
	                }
	            });
	        });
	        
	        $('#ContentBody_upWaiting table tbody tr').find(' td:eq(1)').each(function () {
	            $(this).qtip({
	                content: {
	                    text: function (event, api) {
	                        $.ajax({
	                            url: './ResourceImage.aspx?ResourceId=' + $.trim($(this).parent().find('td:eq(4)').text())
	                        })
                            .then(function (content) {
                                api.set('content.text', content);
                            }, function (xhr, status, error) {
                                api.set('content.text', status + ': ' + error);
                            });

	                        return 'Loading...';
	                    }
	                },
	                position: {
	                    my: 'left center',
	                    at: 'right center',
	                    target: $(this).parent()
	                }
	            });
	        });

	        $('#ContentBody_upActive table tbody tr').find('td:eq(0)').each(function () {
	            $(this).qtip({
	                content: {
	                    text: function (event, api) {
	                        $.ajax({
	                            url: './ResourceImage.aspx?ResourceId=' + $.trim($(this).parent().find('td:eq(2)').text())
	                        })
                            .then(function (content) {
                                api.set('content.text', content);
                            }, function (xhr, status, error) {
                                api.set('content.text', status + ': ' + error);
                            });

	                        return 'Loading...';
	                    }
	                },
	                position: {
	                    my: 'right center',
	                    at: 'left center',
	                    target: $(this).parent()
	                }
	            });
	        });

	        $('#ContentBody_upActive table tbody tr').find('td:eq(1)').each(function () {
	            $(this).qtip({
	                content: {
	                    text: function (event, api) {
	                        $.ajax({
	                            url: './ResourceImage.aspx?ResourceId=' + $.trim($(this).parent().find('td:eq(3)').text())
	                        })
                            .then(function (content) {
                                api.set('content.text', content);
                            }, function (xhr, status, error) {
                                api.set('content.text', status + ': ' + error);
                            });

	                        return 'Loading...';
	                    }
	                },
	                position: {
	                    my: 'right center',
	                    at: 'left center',
	                    target: $(this).parent()
	                }
	            });
	        });
	    }

	    function detectEvent(e) {
	        var evt = window.event;
	        // Someone pressed the Enter key
	        if (evt.keyCode == 13) {
//				alert("Someone pressed the enter key");
	            // We are on the main login screen
	            if ($('input[id*="hfPageToggle"][type="hidden"]').val() == "false") {
//					alert("we are on the main screen");
	                $('input[id*="hfPageToggle"][type="hidden"]').val("true");
//					$('input[id*="hfLoading"][type="hidden"]').val("true");
	                $('input[id*="btnLogin"]').click();
	            }
	                // We are on the user's login screen (messaging, time tracker)
	            else {
//					alert("we are on the users login screen");
	                // This is a scheduled session, click btnContinueLogin
//					alert("hfScheduledSession.Value: " + $('input[id*="hfScheduledSession"]').val());
//					alert("btnContinueLogin.Name: " + $('input[id*="btnContinueLogin"]').attr("alt"));
	                if ($('input[id*="btnContinueLogin"]').attr("alt") == "true") {
//						alert("we clicked btnContinueLogin");
	                    $('input[id*="hfPageToggle"][type="hidden"]').val("false");
	                    $('input[id*="btnContinueLogin"]').click();
	                }
	                    // This is an unscheduled session, click btnUnscheduled
	                else {
//						alert("we clicked btnUnscheduled");
	                    $('input[id*="hfPageToggle"][type="hidden"]').val("false");
	                    $('input[id*="btnUnscheduled"]').click();
	                }
	            }
	        }
	    }

	    $(document).keypress(detectEvent);
	    window.onload = alertRoom;

	    // This function is used when the user is logging in while in another session
	    // If they select Yes then they continue forward with the login
	    function confirmProceed(confirmed) {
	        if (confirmed) {
	            $('input[id*="hfUserFeedback"][type="hidden"]').val("true");
	            //                alert("you have been confirmed");
	        }
	        else {
	            $('input[id*="hfUserFeedback"][type="hidden"]').val("false");
	            //                alert("you have NOT been confirmed");
	            window.location = window.location;
	        }
	    }

	    Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(
	        function () {
	            if (document.getElementById) {
	                var progress = document.getElementById('progress');
	                var blur = document.getElementById('blur');
	                progress.style.width = '300px';
	                progress.style.height = '30px';
	                blur.style.height = document.documentElement.clientHeight;
	                blur.style.width = document.documentElement.clientWidth;
	                //			        progress.style.top = document.documentElement.clientHeight / 3 - progress.style.height.replace('px', '') / 2 + 'px';
	                //			        progress.style.left = document.body.offsetWidth / 2 - progress.style.width.replace('px', '') / 2 + 'px';
	            }
	        }
	    );
	</script>
	<input type="hidden" id="hfPageToggle" runat="server" />
	<input type="hidden" id="hfUserFeedback" runat="server" />
	<input type="hidden" id="hfScheduledSession" runat="server" />
	<%--<input type="hidden" id="hfLoading" runat="server" />--%>
	<input type="hidden" id="hfRoomNumber" runat="server" />
	<asp:UpdatePanel ID="upPage" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="pLoginStudents" Visible="true" runat="server">
				<div class="TutorApplicationContainer">
					<div class="TutorApplicationLeft">
						<h1>Waiting List</h1>
						<asp:UpdatePanel ID="upWaiting" runat="server" UpdateMode="Always">
							<ContentTemplate>
								<asp:GridView ID="gvMissing" runat="server" AutoGenerateColumns="False" 
									CellPadding="4" ForeColor="#333333" Width="100%" 
									AllowSorting="True" OnSorting="gvMissing_Sorting" OnRowCommand="gvMissing_RowCommand" >
									<AlternatingRowStyle BackColor="White" />
									<Columns>
										<asp:BoundField DataField="Present" HeaderText="Present" ReadOnly="True" SortExpression="Present" />
										<asp:BoundField DataField="Missing" HeaderText="Missing" ReadOnly="True" SortExpression="Missing" ItemStyle-ForeColor="Red" ItemStyle-BorderColor="Black" ControlStyle-BorderColor="Black" >
										<ControlStyle BorderColor="Black" />
                                        <ItemStyle BorderColor="Black" ForeColor="Red" />
                                        </asp:BoundField>
										<asp:BoundField DataField="Subject" HeaderText="Subject" ReadOnly="True" SortExpression="Subject" />
									    <asp:TemplateField HeaderText="Resource Image">
									        <HeaderStyle CssClass="HiddenCol" />
                                            <ItemTemplate>
                                                <%# Eval("MissingResourceID") %>
                                            </ItemTemplate>
									        <ItemStyle CssClass="HiddenCol" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Session Image">
									        <HeaderStyle CssClass="HiddenCol" />
                                            <ItemTemplate>
                                                <%# Eval("PresentResourceID") %>
                                            </ItemTemplate>
									        <ItemStyle CssClass="HiddenCol" />
                                        </asp:TemplateField>
									</Columns>
									<EditRowStyle BackColor="#2461BF" />
									<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
									<RowStyle BackColor="#EFF3FB" />
									<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
									<SortedAscendingCellStyle BackColor="#F5F7FB" />
									<SortedAscendingHeaderStyle BackColor="#6D95E1" />
									<SortedDescendingCellStyle BackColor="#E9EBEF" />
									<SortedDescendingHeaderStyle BackColor="#4870BE" />
								</asp:GridView>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
					<div class="TutorApplicationCenter" style="position: relative;">
						<div class="TutorApplicationLogo">
							<img class="TutorApplicationLogoImage" src="Images/Logo_Full.png" alt="" /><br />
							<br />
							University of Kentucky C.A.T.S.<br />
							(Center for Academic and Tutorial Services) <br />
                            <strong>Football Location</strong>
						</div>

						<div id="TutorUpdateProgressContainer" style="position: absolute; width: 100%; text-align: center;">
							<asp:UpdateProgress runat="server" ID="TutorUpdateProgress">
								<ProgressTemplate>
									<div id="blur" class="blur"></div>
									<div id="progress" class="progress">
										<asp:Image  Width="100px" Height="100px" ID="TutorUpdateProgressImage" runat="server" ImageUrl="~/Images/loading.gif" />
									</div>
								</ProgressTemplate>
							</asp:UpdateProgress>
						</div>

						<div class="TutorApplicationLogin">
							<asp:UpdatePanel ID="upLogin" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									Enter User ID:
									<br />
									<asp:TextBox ID="txtLogin" runat="server" Width="150px" MaxLength="9"></asp:TextBox>
									<br />
								</ContentTemplate>
							</asp:UpdatePanel>
				   
							<asp:UpdatePanel runat="server" ID="upButtons" UpdateMode="Conditional">
								<ContentTemplate>
									<div class="TutorApplicationLoginError">
										<asp:RequiredFieldValidator ID="rfdLogin" runat="server" ControlToValidate="txtLogin" ErrorMessage="Error: ID Required" ForeColor="Red"></asp:RequiredFieldValidator>
										<br />
										<asp:Label ID="lblError" runat="server" ForeColor="Red" Text="lblError" />    
									</div>
									<asp:Button ID="btnLogin" runat="server" Height="40px" Text="Login" Width="140px" OnClick="btnLogin_Click" />
									<asp:Button ID="Button1" runat="server" Height="40px" Text="Refresh" Width="140px" OnClick="btnLogout_Click" UseSubmitBehavior="False" CausesValidation="False" /><br />
                                    <asp:CheckBox id="chkForceUnscheduled" runat="server" Text ="Force Unscheduled" />
								</ContentTemplate>
							</asp:UpdatePanel>
							<br />

							<asp:UpdatePanel ID="upTime" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div class="TutorApplicationLargeTime">
										<asp:Label ID="lblCurrentTime" runat="server" Text="lblCurrentTime" />
									</div>
									<div class="TutorApplicationLargeDate">
										<asp:Label ID="lblCurrentDate" runat="server" Text="lblCurrentDate" />
									</div>
									<div class="TutorDebug">
										<asp:Label ID="lblSyncTime" runat="server" Text="lblSyncTime" Visible="True" />
									</div>                    
								</ContentTemplate>
							</asp:UpdatePanel>
					

							<div class="TutorApplicationStudentList">
								<span class="TutorApplicationUnscheduledTitle">Unscheduled</span>
								<br />
								<div style="text-align: center; font-size: 10pt;">
									<u>Instructions:</u><br />
									<b>1.</b> Have Students Login<br />
									<b>2.</b> Have Tutor Login<br />
									<b>3.</b> Have Tutor enter ID above<br />
									<b>4.</b> Select a Student below<br />
									<b>5.</b> Click the Login button.<br />
								</div>
								<br />
								<asp:UpdatePanel ID="upUnscheduled" runat="server" UpdateMode="Always">
									<ContentTemplate>
										<asp:ListBox ID="lbStudent" runat="server" CssClass="TutorApplicationStudentListbox ListboxPicker" Rows="5" SelectionMode="Multiple"></asp:ListBox>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						</div>
				
						<div class="PageFooter">
								C.A.T.S. Tutoring Center<br />
								<SIS:ApplicationVersion ID="ApplicationVersionControl" runat="server" />
								Copyright &copy; 2011 University of Kentucky
						</div>
					</div>
					<div class="TutorApplicationRight">
						<h1>Current Sessions</h1>
						<asp:UpdatePanel ID="upActive" runat="server" UpdateMode="Always">
							<ContentTemplate>
								<asp:GridView ID="gvActive" runat="server" AutoGenerateColumns="False" CellPadding="4"
									ForeColor="#333333" Width="100%" OnRowCommand="gvActive_RowCommand" 
									AllowSorting="True" OnSorting="gvActive_Sorting">
									<AlternatingRowStyle BackColor="White" />
									<Columns>
										<asp:TemplateField HeaderText="AttendanceID_HIDDEN" Visible="False">
											<ItemTemplate>
												<asp:Label ID="lblAttendanceID" runat="server" Text='<%# Bind("AttendanceID") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
                                        <asp:BoundField DataField="TutorName" HeaderText="Tutor" ReadOnly="True" SortExpression="TutorName" />
										<asp:BoundField DataField="StudentName" HeaderText="Student" ReadOnly="True" SortExpression="StudentName" />
                                        <asp:TemplateField HeaderText="Resource Image">
									        <HeaderStyle CssClass="HiddenCol" />
                                            <ItemTemplate>
                                                <%# Eval("Tutor.ResourceID") %>
                                            </ItemTemplate>
									        <ItemStyle CssClass="HiddenCol" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Session Image">
									        <HeaderStyle CssClass="HiddenCol" />
                                            <ItemTemplate>
                                                <%# Eval("Student.ResourceID") %>
                                            </ItemTemplate>
									        <ItemStyle CssClass="HiddenCol" />
                                        </asp:TemplateField>
										<asp:BoundField DataField="Subject" HeaderText="Subject" ReadOnly="True" />
										<asp:BoundField DataField="Room" HeaderText="Room" ReadOnly="True" SortExpression="Room" />
										<asp:BoundField DataField="StartTime" HeaderText="Start Time" ReadOnly="True" />
										<asp:ButtonField Text="End Session" CommandName="Logout" />
									</Columns>
									<EditRowStyle BackColor="#2461BF" />
									<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
									<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
									<RowStyle BackColor="#EFF3FB" />
									<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
									<SortedAscendingCellStyle BackColor="#F5F7FB" />
									<SortedAscendingHeaderStyle BackColor="#6D95E1" />
									<SortedDescendingCellStyle BackColor="#E9EBEF" />
									<SortedDescendingHeaderStyle BackColor="#4870BE" />
								</asp:GridView>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
					<div class="clear"></div>

					<asp:UpdatePanel ID="upTimer" runat="server" UpdateMode="Always">
						<ContentTemplate>
							<asp:Timer ID="timerUpdatePage" runat="server" OnTick="timerUpdatePage_Tick"></asp:Timer>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
			</asp:Panel>
			<asp:Panel ID="pViewMessages" runat="server" Visible="false">
					<div class="TimeTracker">
						<iframe id="ifTimeTracker" src="" width="100%" height="270px" runat="server"></iframe>
					</div>
					<asp:UpdatePanel ID="upLoginMesages" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<iframe id="ifMessage" src="" width="100%" height="400px" runat="server"></iframe>
						</ContentTemplate>
					</asp:UpdatePanel>
					<asp:UpdatePanel ID="upContinueLogin" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Timer ID="tmrContinueLogin" runat="server" OnTick="tmrContinueLogin_Tick"></asp:Timer>
							<asp:Button BackColor="#99CCFF" Enabled="False" Height="60px" ID="btnContinueLogin" OnClick="btnContinueLogin_Click" runat="server" Text="Continue Login" Width="165px" />
							<asp:Button BackColor="#FF9900" Enabled="False" Height="60px" ID="btnUnscheduled" onclick="btnUnscheduled_Click" runat="server" Text="No Scheduled Session - Unscheduled?" Width="250px" />
                            <asp:Button BackColor="#FF6666" Enabled="True" Height="60px" ID="btnCancel" onclick="btnCancel_Click" runat="server" Text="Cancel" Width="100px" />
							<asp:HyperLink ID="hpPrintSchedule" Target="_blank" NavigateUrl="" runat="server">Print Schedule</asp:HyperLink>
							<div id="TutorContinueUpdateProgressContainer" style="position: absolute; width: 100%; text-align: center;">
								<asp:UpdateProgress runat="server" ID="TutorContinueUpdateProgress">
									<ProgressTemplate>
										<div id="blur" class="blur2"></div>
										<div id="progress" class="progress2">
											<asp:Image  Width="100px" Height="100px" ID="TutorContinueUpdateProgressImage" runat="server" ImageUrl="~/Images/loading.gif" />
										</div>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<%--<div id="TutorContinueUpdateProgressContainer" style="position: absolute; width: 100%; text-align: center;">
						<asp:UpdateProgress runat="server" ID="TutorContinueUpdateProgress">
							<ProgressTemplate>
								<div id="blur" class="blur"></div>
								<div id="progress" class="progress">
									<asp:Image  Width="100px" Height="100px" ID="TutorContinueUpdateProgressImage" runat="server" ImageUrl="~/Images/loading.gif" />
								</div>
							</ProgressTemplate>
						</asp:UpdateProgress>
					</div>--%>
			</asp:Panel>
                                <telerik:RadToolTipManager ID="RadToolTipManager" runat="server" Position="BottomRight"
                                    Animation="Fade" OnAjaxUpdate="RadToolTipManager_OnAjaxUpdate" RelativeTo="Mouse" 
                                    RenderInPageRoot="true" Skin="Web20">
                                </telerik:RadToolTipManager>
		</ContentTemplate>
	</asp:UpdatePanel>

    
</asp:Content>