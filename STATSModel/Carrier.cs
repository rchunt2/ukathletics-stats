﻿using System;
using System.Linq;

namespace UK.STATS.STATSModel
{
    public partial class Carrier
    {
        public static Carrier Get(Guid CarrierID)
        {
            var context = new STATSEntities();
            return (from Carrier obj in context.Attendances where obj.CarrierID == CarrierID select obj).First();
        }

        public static Carrier[] Get()
        {
            var context = new STATSEntities();
            var eAttendances = (from Carrier obj in context.Attendances select obj).ToArray();
            return eAttendances;
        }
    }
}
