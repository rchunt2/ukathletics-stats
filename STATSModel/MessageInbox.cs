﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class MessageInbox
    {
        public Boolean MarkRead()
        {
            var context = new STATSModel.STATSEntities();
            var eMessageInbox = (from MessageInbox obj in context.MessageInboxes where obj.MessageInboxID == this.MessageInboxID select obj).First();
            eMessageInbox.MessageInboxDateRead = DateTime.Now;
            context.SaveChanges();
            return true;
        }

        public static int NumberMessagesUnread(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var count = (from STATSModel.MessageInbox obj in context.MessageInboxes
                         where
                             obj.MessageInboxDateRead == null &&
                             obj.MessageInboxDateExpires > DateTime.Today &&
                             obj.Message.MessageToResourceID == eResource.ResourceID &&
                             obj.MessageInboxDateSent <= DateTime.Today
                         select obj).Count();
            return count;
        }

        public static int NumberMessages(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var count = (from STATSModel.MessageInbox obj in context.MessageInboxes 
                         where 
                            obj.MessageInboxDateExpires > DateTime.Today && 
                            obj.Message.MessageToResourceID == eResource.ResourceID  &&
                            obj.MessageInboxDateSent <= DateTime.Today
                         select obj).Count();
            return count;
        }


        public static MessageInbox Get(Guid MessageInboxID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageInbox obj in context.MessageInboxes 
                          where obj.MessageInboxID == MessageInboxID 
                          select obj).Single();
            return result;
        }

        public static MessageInbox[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageInbox obj in context.MessageInboxes orderby obj.MessageInboxDateSent descending select obj).ToArray();
            return result;
        }

        public static MessageInbox[] Get(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageInbox obj in context.MessageInboxes.Include("Message") 
                          where obj.Message.MessageToResourceID == eResource.ResourceID
                          orderby obj.MessageInboxDateSent descending
                          select obj).ToArray();
            return result;
        }


        public static MessageInbox[] GetAllNotExpired(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageInbox obj in context.MessageInboxes.Include("Message") 
                          where 
                            obj.MessageInboxDateExpires > DateTime.Today && 
                            obj.Message.MessageToResourceID == eResource.ResourceID &&
                            obj.MessageInboxDateSent <= DateTime.Today
                          orderby obj.MessageInboxDateSent descending
                          select obj).ToArray();
            return result;
        }

        public static MessageInbox[] GetAllNewNotExpired(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageInbox obj in context.MessageInboxes.Include("Message")
                          where
                            obj.MessageInboxDateExpires > DateTime.Today &&
                            obj.Message.MessageToResourceID == eResource.ResourceID &&
                            obj.MessageInboxDateSent <= DateTime.Today &&
                            obj.MessageInboxDateRead == null
                          orderby obj.MessageInboxDateSent descending
                          select obj).ToArray();
            return result;
        }

        public static Boolean ReadAllMessages(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var eMessageInboxes = (from STATSModel.MessageInbox obj in context.MessageInboxes.Include("Message") 
                                   where 
                                    obj.Message.MessageToResourceID.HasValue && 
                                    obj.Message.MessageToResourceID.Value == eResource.ResourceID && 
                                    obj.MessageInboxDateExpires > DateTime.Today &&
                                    obj.MessageInboxDateSent <= DateTime.Today &&
                                    obj.MessageInboxDateRead == null 
                                   select obj).ToArray();
            foreach (var eMessageInbox in eMessageInboxes)
            {
                eMessageInbox.MessageInboxDateRead = DateTime.Now;
            }
            int cntChanged = context.SaveChanges();
            return true;
        }


    }
}
