﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Error
    {
        public static Error Get(Guid ErrorID)
        {
            var context = new STATSModel.STATSEntities();
            Error eError = (from STATSModel.Error obj in context.Authentications where obj.ErrorID == ErrorID select obj).First();
            return eError;
        }

        public static Error[] Get()
        {
            var context = new STATSModel.STATSEntities();
            Error[] eErrors = (from STATSModel.Error obj in context.Errors select obj).ToArray();
            return eErrors;
        }

        private static Boolean LogError(DateTime errorDateTime, String errorText, String errorIPAddress, Resource eResource = null)
        {
            String ip = errorIPAddress;
            if (ip.Length > 15)
            {
                const String lenError = "An attempt was made to log an Error with an IP Address that was over 15 characters in length.  This value would not fit within the database, so it has been truncated before being stored.  In addition, the original value has been appended to the end of the Error message.";
                LogError(DateTime.Now, lenError, "localhost");
                errorText += " -- Original IP Address: " + ip;
                ip = ip.Substring(0, 15);
            }

            var context = new STATSModel.STATSEntities();
            Error eError = STATSModel.Error.CreateError(Guid.NewGuid(), errorDateTime, errorText, ip);
            if (eResource != null)
            {
                eError.ResourceID = eResource.ResourceID;
            }
            context.Errors.AddObject(eError);
            context.SaveChanges();
            return true;
        }

        public static Boolean LogMessage(String Message, Resource eResource, String IPAddress)
        {
            Boolean result = LogError(DateTime.Now, Message, IPAddress, eResource);
            return result;
        }

        public static Boolean LogException(Exception ex, Resource eResource,  String IPAddress)
        {
            String errorMessage = ex.ToString();
            String ip = IPAddress;

            return LogError(DateTime.Now, errorMessage, ip, eResource);
        }
    }
}
