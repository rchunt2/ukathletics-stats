﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Message
    {
        public static Message Get(Guid MessageID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Message obj in context.Messages where obj.MessageID == MessageID select obj).First();
            return result;
        }

        public static Message[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Message obj in context.Messages select obj).ToArray();
            return result;
        }

        public static Message[] Get(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Message obj in context.Messages where obj.MessageToResourceID == eResource.ResourceID select obj).ToArray();
            return result;
        }

        public String GetBody()
        {
            return this.MessageContent.MessageContentBody;
        }

        public String GetSubject()
        {
            return this.MessageContent.MessageContentSubject;
        }

        public static Boolean SendIndividualMessage(MessageType eMessageType, Resource eResourceFrom, CourseSection eCourseSectionTo, String MessageSubject, String MessageBody, DateTime Expire)
        {
            if(eCourseSectionTo.InstructorID.HasValue)
            {
                var eInstructor = eCourseSectionTo.Instructor;
                var eInstructorEmail = eInstructor.InstructorEmail;
                SendEmail(eResourceFrom.ResourceEmail, eInstructorEmail, MessageSubject, MessageBody);
                return true;
            }
            return false;
        }

        public static Boolean SendIndividualMessage(MessageType eMessageType, Resource eResourceFrom, Resource eResourceTo, String MessageSubject, String MessageBody, DateTime Expire, DateTime SendDate)
        {
            if (Expire < SendDate)
            {
                throw new Exception("This Message is set to expire before it will be sent!");
            }

            var context = new STATSModel.STATSEntities();
            MessageContent eMessageContent = MessageContent.CreateMessageContent(Guid.NewGuid(), MessageSubject, MessageBody);
            Message eMessage = Message.CreateMessage(Guid.NewGuid(), eMessageType.MessageTypeID, eMessageContent.MessageContentID, eResourceFrom.ResourceID);
            eMessage.MessageToResourceID = eResourceTo.ResourceID; // This is a nullable field, so it isn't required in the constructor.  Woops!

            context.AddToMessageContents(eMessageContent);
            context.AddToMessages(eMessage);
            
            switch(eMessageType.MessageTypeName)
            {
                case "Application":
                    MessageInbox eMessageInbox = MessageInbox.CreateMessageInbox(Guid.NewGuid(), eMessage.MessageID, SendDate);
                    eMessageInbox.MessageInboxDateExpires = Expire;
                    context.AddToMessageInboxes(eMessageInbox);
                    break;
                case "SMS (Text Message)":
                    Carrier eCarrier = eResourceTo.Carrier;
                    String TextMessageEmail = eResourceTo.ResourcePhoneNumber + eCarrier.CarrierEmailSuffix;
                    if (eCarrier.CarrierEmailSuffix.Length > 0)
                    {
                        SendEmail(eResourceFrom.ResourceEmail, TextMessageEmail, MessageSubject, eResourceTo.ResourceNameFirst + " " + eResourceTo.ResourceNameLast + ", " + MessageBody);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    break;
                case "Email":
                    SendEmail(eResourceFrom.ResourceEmail, eResourceTo.ResourceEmail, MessageSubject, MessageBody);
                    break;
            }

            context.SaveChanges();

            return true;
        }

        public static Boolean SendGroupMessage(MessageType eMessageType, Resource eResourceFrom, Group eGroupTo, String MessageSubject, String MessageBody, DateTime Expire, DateTime SendDate)
        {
            // TODO: This could be optimized by inserting all the records at once, rather than individually.
            Resource[] eResources = eGroupTo.GetMembers();
            foreach (var eResourceTo in eResources)
            {
                SendIndividualMessage(eMessageType, eResourceFrom, eResourceTo, MessageSubject, MessageBody, Expire, SendDate);
            }
            return true;
        }

        private static void SendEmail(String From, String To, String Subject, String Body)
        {
            var oMail = new MailMessage(From, To, Subject, Body);
            oMail.CC.Add(new MailAddress(From));

            Setting eSettingHost = Setting.GetByKey("SMTPHost");
            Setting eSettingPort = Setting.GetByKey("SMTPPort");

            String SMTPHost = eSettingHost.SettingValue;
            int SMTPPort = Int32.Parse(eSettingPort.SettingValue);

            var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

            try
            {
                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }

        }

    }
}
