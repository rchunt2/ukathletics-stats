﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class FreeTime
    {
        public String SessionTypeName
        {
            get { return this.SessionType.SessionTypeName; }
        }

        public static Boolean Add(Resource eResource, SessionType eSessionType, DateTime ApplyOnDay, int AmountInMinutes)
        {
            var context = new STATSEntities();
            FreeTime eFreeTime = FreeTime.CreateFreeTime(Guid.NewGuid(), eResource.ResourceID, eSessionType.SessionTypeID, ApplyOnDay, AmountInMinutes, DateTime.Today);
            context.FreeTimes.AddObject(eFreeTime);
            context.SaveChanges();
            return true;
        }

        public static FreeTime[] Get(Resource eResource)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.FreeTime obj in context.FreeTimes where obj.ResourceID == eResource.ResourceID select obj).ToArray();
            return result;
        }

        public static FreeTime Get(Guid FreeTimeID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.FreeTime obj in context.FreeTimes where obj.FreeTimeID == FreeTimeID select obj).First();
            return result;
        }

        public static FreeTime[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.FreeTime obj in context.FreeTimes select obj).ToArray();
            return result;
        }

    }
}
