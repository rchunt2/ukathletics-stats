﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Instructor
    {
        public Boolean CanBeDeleted
        {
            get
            {
                int iCourseSections = this.CourseSections.Count;
                int iTotal = iCourseSections;
                return (iTotal == 0);
            }
        }

        public static Instructor Get(Guid InstructorID)
        {
            var context = new STATSEntities();
            var result = (from Instructor obj in context.Instructors where obj.InstructorID == InstructorID select obj).Single();
            return result;
        }
    }
}
