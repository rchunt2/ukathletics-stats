﻿using System;
using System.Linq;

namespace UK.STATS.STATSModel
{
    public partial class Authentication
    {
        /// <summary>
        /// Deletes the current Authentication object.
        /// </summary>
        /// <returns>A Boolean value indicating whether or not the operation was successful.</returns>
        public Boolean Expire()
        {
            var context = new STATSEntities();
            var eAuthentications = (from Authentication obj in context.Authentications where obj.AuthenticationID == AuthenticationID select obj).ToArray();
            var cntAuthentications = eAuthentications.Count();

            if(cntAuthentications > 0)
            {
                foreach (var eAuthentication in eAuthentications)
                {
                    context.DeleteObject(eAuthentication);
                }
            }
            
            int cntDeleted = context.SaveChanges();
            if(cntDeleted > 1)
            {
                // TODO: this shouldn't have happened.
            }
            return cntDeleted == 1;
        }

        /// <summary>
        /// Deletes any Authentication past due its Expiration time.
        /// </summary>
        /// <returns>The number of Authentication objects that were deleted during the operation.</returns>
        public static int ExpireSessions()
        {
            var context = new STATSEntities();
            var eAuthentications = (from Authentication obj in context.Authentications where obj.AuthenticationExpire < DateTime.Now select obj).ToArray();
            foreach (var eAuthentication in eAuthentications)
            {
                context.DeleteObject(eAuthentication);
            }
            int cntDeleted = context.SaveChanges();
            return cntDeleted;
        }

        public static Authentication Get(Guid AuthenticationID)
        {
            var context = new STATSEntities();
            Authentication eAuthentication = (from Authentication obj in context.Authentications where obj.AuthenticationID == AuthenticationID select obj).First();
            return eAuthentication;
        }

        public static Authentication[] Get()
        {
            var context = new STATSEntities();
            Authentication[] eAuthentications = (from Authentication obj in context.Authentications select obj).ToArray();
            return eAuthentications;
        }

        public static Authentication Get(Resource eResource)
        {
            var context = new STATSEntities();
            int numAuth = (from Authentication obj in context.Authentications where obj.ResourceID == eResource.ResourceID select obj).Count();
            Authentication eAuthentication;
            if(numAuth > 0)
            {
                eAuthentication = (from Authentication obj in context.Authentications where obj.ResourceID == eResource.ResourceID select obj).First();
            } 
            else
            {
                String strSessionTimeout = Setting.GetByKey("SessionTimeout").SettingValue;
                int SessionTimeout = Int32.Parse(strSessionTimeout);
                eAuthentication = CreateAuthentication(Guid.NewGuid(), eResource.ResourceID, DateTime.Now.AddMinutes(SessionTimeout));
                context.AddToAuthentications(eAuthentication);
                context.SaveChanges();
            }
            return eAuthentication;
        }
    }
}
