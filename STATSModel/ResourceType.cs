﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class ResourceType
    {
        public static ResourceType Get(Guid ResourceTypeID)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.ResourceType obj in context.ResourceTypes where obj.ResourceTypeID == ResourceTypeID select obj).First();
        }

        public static ResourceType[] Get()
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.ResourceType obj in context.ResourceTypes select obj).ToArray();
        }

        public static ResourceType Get(String ResourceTypeName)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.ResourceType obj in context.ResourceTypes where obj.ResourceTypeName == ResourceTypeName select obj).First();
        }

    }
}
