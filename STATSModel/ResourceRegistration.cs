﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class ResourceRegistration
    {
        /// <summary>
        /// Registers the specified Resource for the specified Course Section.
        /// </summary>
        /// <param name="eCourseSection">The Course Section that will be registered for.</param>
        /// <param name="eResource">The Resource that to be registered.</param>
        /// <returns>true if the registration was successful; otherwise false.</returns>
        public static Boolean Register(CourseSection eCourseSection, Resource eResource, Boolean DisregardConflicts)
        {
            Boolean ConflictsDetected = false;
            var eResourceRegistrations = new List<ResourceRegistration>();
            var context = new STATSEntities();
            var eCourseSections = (from CourseSection obj in context.CourseSections where obj.CourseSectionPackageID == eCourseSection.CourseSectionPackageID select obj).ToList();
            foreach (var courseSection in eCourseSections)
            {
                if (!DisregardConflicts)
                {
                    Boolean CourseConflicts = DoesTimeConflict(eResource, courseSection);
                    if (CourseConflicts) { ConflictsDetected = true; }    
                }

                var eResourceRegistration = CreateResourceRegistration(Guid.NewGuid(), eResource.ResourceID, courseSection.CourseSectionID);
                eResourceRegistrations.Add(eResourceRegistration);
            }

            if (!ConflictsDetected)
            {
                foreach (ResourceRegistration registration in eResourceRegistrations)
                {
                    context.ResourceRegistrations.AddObject(registration);
                }
                try
                {
                    int i = context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return true;
                //return (i > 0);
            }
            return false;
        }

        /// <summary>
        /// Checks that the specified Course Section will not conflict with the schedule of the specified Resource.
        /// </summary>
        /// <param name="eResource">The Resource whose schedule will be checked against.</param>
        /// <param name="eCourseSection">The Course Section for which availability will be verified.</param>
        /// <returns>true if there was no conflict; otherwise false.</returns>
        public static Boolean DoesTimeConflict(Resource eResource, CourseSection eCourseSection)
        {
            Boolean Conflict = false;

            // Get a list of the course-to-check's sessions.
            Session[] eSessions = ResourceRegistration.GetCourseSectionSessions(eCourseSection);

            // Verify No Conflicting Courses
            CourseSection[] RegisteredCourses = GetRegisteredCourses(eResource);
            List<Session> DerivedSessions = new List<Session>();
            foreach (var registeredCourse in RegisteredCourses)
            {
                Session[] DerivedSessionsForThisCourse = GetCourseSectionSessions(registeredCourse);
                DerivedSessions.AddRange(DerivedSessionsForThisCourse);
            }

            foreach (Session CheckSession in eSessions) // Iterate over the checking course's sessions.
            {
                DateTime StartTime = CheckSession.SessionDateTimeStart;
                DateTime EndTime = CheckSession.SessionDateTimeEnd;

                foreach (Session ExistingSession in DerivedSessions) // Iterate over all the sessions for pre-existing registered courses.
                {
                    if (ExistingSession.SessionDateTimeStart < DateTime.Today) break;
                    if (Conflict) { return true; } // Don't look any further if we've already found an time conflict.

                    // Key:
                    //     N = New Session (StartTime, EndTime)
                    //     E = Existing Session (ExistingSession.SessionDateTimeStart, ExistingSession.SessionDateTimeEnd)

                    // Case 1: Starts after Existing Session Starts and also starts before Existing Session Ends
                    //       N-------N
                    //    E------E 
                    if (StartTime >= ExistingSession.SessionDateTimeStart && StartTime < ExistingSession.SessionDateTimeEnd)
                    {
                        Conflict = true;
                    }

                    // Case 2: Ends before the Existing Session Ends, and also Ends after the Existing Session Starts
                    //    N-------N
                    //        E---------E
                    if (EndTime <= ExistingSession.SessionDateTimeEnd && EndTime > ExistingSession.SessionDateTimeStart)
                    {
                        Conflict = true;
                    }

                    // Case 3: Starts before the Existing Session Starts, and Ends after the Existing Session Ends
                    //  N----------N
                    //     E----E
                    if (StartTime <= ExistingSession.SessionDateTimeStart && EndTime >= ExistingSession.SessionDateTimeEnd)
                    {
                        Conflict = true;
                    }
                }


                // Verify No Conflicting Tutoring Sessions (Scheduled Tutoring Sessions)
                var context = new STATSEntities();
                var eSchedules = (from Schedule obj in context.Schedules.Include("Session") where obj.ResourceID == eResource.ResourceID && obj.Session.SessionIsScheduled && !obj.Session.SessionIsCancelled && obj.Session.SessionDateTimeStart > DateTime.Today select obj).ToList();

                foreach (Schedule eSchedule in eSchedules) // Iterate over all Non-Class Schedules
                {
                    var ExistingSession = eSchedule.Session;
                    DateTime AddMeStartTime = ExistingSession.SessionDateTimeStart;
                    DateTime AddMeEndTime = ExistingSession.SessionDateTimeEnd;

                    if (!Conflict)
                        Conflict = CheckSessionsForConflicts(AddMeStartTime, AddMeEndTime, CheckSession);
                    else return Conflict;

                    //    if (Conflict) { return true; } // Don't look any further if we've already found an time conflict.

                    //    // THE EXACT SAME TIME, EXACTLY - It's the same course.  How could I have forgotten this one?!
                    //    if (eSession.SessionDateTimeStart == StartTime && eSession.SessionDateTimeEnd == EndTime)
                    //    {
                    //        Conflict = true;
                    //    }

                    //    // OVERLAP WITH RANGE START - Session starts before the range starts, but ends after the range has started. 
                    //    if (eSession.SessionDateTimeStart <= StartTime && eSession.SessionDateTimeEnd >= StartTime)
                    //    {
                    //        Conflict = true;
                    //    }

                    //    // OVERLAP WITH RANGE END - Session starts before the range ends, but ends after the range has ended.
                    //    if (eSession.SessionDateTimeStart <= EndTime && eSession.SessionDateTimeEnd >= EndTime)
                    //    {
                    //        Conflict = true;
                    //    }

                    //    // RANGE FULLY CONTAINS - Session starts after the range starts, but ends before the range has ended.
                    //    if (eSession.SessionDateTimeStart >= StartTime && eSession.SessionDateTimeEnd <= EndTime)
                    //    {
                    //        Conflict = true;
                    //    }

                    //    // RANGE IS FULLED CONTAINED - Session starts before the range starts, and the Session ends after the range has ended.
                    //    if (eSession.SessionDateTimeStart <= StartTime && eSession.SessionDateTimeEnd >= EndTime)
                    //    {
                    //        Conflict = true;
                    //    }
                }
            }
            return Conflict;
        }

        /// <summary>
        /// Drops the specified Course from the specified Resource's schedule.
        /// </summary>
        /// <param name="eCourseSection">The Course Section that will be dropped.</param>
        /// <param name="eResource">The Resource to be dropped from the Course Section.</param>
        /// <returns>true if the Course Section was dropped successfully; otherwise false.</returns>
        public static Boolean DropCourse(CourseSection eCourseSection, Resource eResource)
        {
            var context = new STATSEntities();
            var eResourceRegistrations = (from ResourceRegistration obj in context.ResourceRegistrations 
                                         where obj.ResourceID == eResource.ResourceID && obj.CourseSectionID == eCourseSection.CourseSectionID 
                                         select obj).ToList();
            eResourceRegistrations.ForEach(context.DeleteObject);

            //If we're dropping the course we should also remove any text message entries that are scheduled
            //to send text messages 10 minutes prior to class
            var eResourceTextMessage = (from TextMessage obj in context.TextMessages
                                        where obj.ResourceID == eResource.ResourceID && obj.CourseSectionID == eCourseSection.CourseSectionID
                                        select obj).ToList();
            eResourceTextMessage.ForEach(context.DeleteObject);

            int i = context.SaveChanges();
            return (i > 0);
        }

        /// <summary>
        /// Gets an array of the Resources that are registered for the specified Course Section
        /// </summary>
        /// <param name="eCourseSection">The Course Section to return the roster for.</param>
        /// <returns>An array of Resources that are registered for the specified Course Section.</returns>
        public static Resource[] GetCourseRoster(CourseSection eCourseSection)
        {
            var context = new STATSEntities();
            var eResources = (from ResourceRegistration obj in context.ResourceRegistrations.Include("Resource") 
                              where obj.CourseSectionID == eCourseSection.CourseSectionID 
                              select obj.Resource).ToArray();
            return eResources;
        }

        /// <summary>
        /// Gets an array of Course Sections that the specified Resource is registered for.
        /// </summary>
        /// <param name="eResource">The Resource to get the Course Sections for.</param>
        /// <returns>An array of Course Sections that that specified Resource is registered for.</returns>
        public static CourseSection[] GetRegisteredCourses(Resource eResource)
        {
            var context = new STATSEntities();
            var eCourseSections = (from ResourceRegistration obj in context.ResourceRegistrations.Include("CourseSection")
                                   where obj.ResourceID == eResource.ResourceID
                                   select obj.CourseSection).ToArray();
            return eCourseSections;
        }

        /// <summary>
        /// Derives the Sessions for the specified Course Section.
        /// </summary>
        /// <param name="eCourseSection">The Course Section to retrieve the Sessions of.</param>
        /// <returns>An array of dynamically generated Sessions associated with the provided Course Section.</returns>
        public static Session[] GetCourseSectionSessions(CourseSection eCourseSection)
        {
            SessionType eSessionTypeClass = SessionType.GetByName("Class");
            Guid SessionTypeIDClass = eSessionTypeClass.SessionTypeID;
            var DerivedSessions = new List<Session>();

            String DaysMeet = eCourseSection.CourseSectionDaysMeet;
            char[] DaysArray = DaysMeet.ToCharArray();

            DateTime DateStart = eCourseSection.CourseSectionDateStart;
            //DateTime DateStart = DateTime.Today;
            DateTime DateEnd = eCourseSection.CourseSectionDateEnd;

            TimeSpan TimeStart = eCourseSection.CourseSectionTimeStart;
            TimeSpan TimeEnd = eCourseSection.CourseSectionTimeEnd;

            DateTime CurrentDate = DateStart;
            while (CurrentDate <= DateEnd)
            {
                Boolean doesMeet = false;
                switch (CurrentDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (DaysArray.Contains('M')) { doesMeet = true; } break;
                    case DayOfWeek.Tuesday:
                        if (DaysArray.Contains('T')) { doesMeet = true; } break;
                    case DayOfWeek.Wednesday:
                        if (DaysArray.Contains('W')) { doesMeet = true; } break;
                    case DayOfWeek.Thursday:
                        if (DaysArray.Contains('R')) { doesMeet = true; } break;
                    case DayOfWeek.Friday:
                        if (DaysArray.Contains('F')) { doesMeet = true; } break;
                    case DayOfWeek.Saturday:
                        if (DaysArray.Contains('S')) { doesMeet = true; } break;
                    case DayOfWeek.Sunday:
                        // NOTE: Sunday Sessions do not get automatically generated.
                        break;
                    default:
                        break;
                }
                if (doesMeet)
                {
                    DateTime ExactTimeTodayStart = CurrentDate.Date.Add(TimeStart);
                    DateTime ExactTimeTodayEnd = CurrentDate.Date.Add(TimeEnd);
                    Session DerivedSession = new Session()
                                                 {
                                                     SessionID = Guid.Empty,
                                                     CourseSectionID = eCourseSection.CourseSectionID,
                                                     SessionTypeID = eSessionTypeClass.SessionTypeID,
                                                     SessionDateTimeStart = ExactTimeTodayStart, 
                                                     SessionDateTimeEnd = ExactTimeTodayEnd,
                                                     SessionIsCancelled = false,
                                                     SessionIsScheduled = true
                                                 };
                    DerivedSessions.Add(DerivedSession);
                }
                CurrentDate = CurrentDate.AddDays(1);
            }
            return DerivedSessions.ToArray();
        }

        private static bool CheckSessionsForConflicts(DateTime AddMeStartTime, DateTime AddMeEndTime, Session oldSession)
        {
            var oldStartTime = oldSession.SessionDateTimeStart.TimeOfDay;
            var oldEndTime = oldSession.SessionDateTimeEnd.TimeOfDay;
            var newStartTime = AddMeStartTime.TimeOfDay;
            var newEndTime = AddMeEndTime.TimeOfDay;
            Boolean Conflict = false;

            if (oldSession.SessionDateTimeStart.DayOfWeek == AddMeStartTime.DayOfWeek)
            {
                if (Conflict) { Conflict = true; } // Don't look any further if we've already found an time conflict.

                // Case 1: New session starts and ends before old session
                // N--------N
                //     E---------E
                if ((newStartTime < oldStartTime) && (newEndTime < oldEndTime) && (newEndTime > oldStartTime))
                {
                    Conflict = true;
                }

                // Case 2: New session starts and ends after old session
                //     N---------N
                // E--------E
                if ((newStartTime > oldStartTime) && (newEndTime > oldEndTime) && (newStartTime < oldEndTime))
                {
                    Conflict = true;
                }

                // Case 3: Sessions are at the same time
                // N-------N
                // E-------E
                if ((newStartTime == oldStartTime) && (newEndTime == oldEndTime))
                {
                    Conflict = true;
                }

                // Case 4: New session is contained by the old session
                //    N--N
                // E-------E
                if ((newStartTime > oldStartTime) && (newEndTime < oldEndTime))
                {
                    Conflict = true;
                }

                // Case 5: New session contains the old session
                // N-------N
                //    E--E
                if ((newStartTime < oldStartTime) && (newEndTime > oldEndTime))
                {
                    Conflict = true;
                }

                // Case 6: New session starts at the same time as the old session
                // N---N
                // E-------E
                if (newStartTime == oldStartTime)
                {
                    Conflict = true;
                }

                // Case 7: New session ends at the same time as the old session
                //     N---N
                // E-------E
                if (newEndTime == oldEndTime)
                {
                    Conflict = true;
                }
            }
            return Conflict;
        }
    }
}
