﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Subject
    {
        public Boolean CanBeDeleted
        {
            get
            {
                int iCourses = this.Courses.Count;
                int iTotal = iCourses;
                return (iTotal == 0);
            }
        }

        public static Subject[] Search(String SearchQuery)
        {
            var context = new STATSEntities();
            var result = (from Subject obj in context.Subjects where obj.SubjectNameShort.Contains(SearchQuery) orderby obj.SubjectNameShort ascending select obj).ToArray();
            return result;
        }


        public static Subject[] Get()
        {
            var context = new STATSEntities();
            var result = (from Subject obj in context.Subjects orderby obj.SubjectNameShort ascending select obj).ToArray();
            return result;
        }

        public static Subject Get(Guid SubjectID)
        {
            var context = new STATSEntities();
            var result = (from Subject obj in context.Subjects where obj.SubjectID == SubjectID select obj).First();
            return result;
        }

        public static Boolean Exists(String SubjectNameShort)
        {
            var context = new STATSEntities();
            var result = (from Subject obj in context.Subjects where obj.SubjectNameShort == SubjectNameShort select obj).Count();
            return result > 0;
        }

        public static Subject Get(String SubjectNameShort)
        {
            var context = new STATSEntities();
            var result = (from Subject obj in context.Subjects where obj.SubjectNameShort == SubjectNameShort select obj).First();
            return result;
        }

    }
}
