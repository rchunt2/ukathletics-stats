﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class OperationRange
    {
        public static OperationRange Get(SessionType eSessionType, DayOfWeek dayOfWeek)
        {
            String dow = "";
            switch(dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    dow = "SUN";
                    break;
                case DayOfWeek.Monday:
                    dow = "MON";
                    break;
                case DayOfWeek.Tuesday:
                    dow = "TUE";
                    break;
                case DayOfWeek.Wednesday:
                    dow = "WED";
                    break;
                case DayOfWeek.Thursday:
                    dow = "THU";
                    break;
                case DayOfWeek.Friday:
                    dow = "FRI";
                    break;
                case DayOfWeek.Saturday:
                    dow = "SAT";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("dayOfWeek");
            }
            var context = new STATSEntities();
            var result = (from OperationRange obj in context.OperationRanges where obj.SessionTypeID == eSessionType.SessionTypeID && obj.OperationRangeDayOfWeek == dow select obj).Single();
            return result;
        }

    }
}
