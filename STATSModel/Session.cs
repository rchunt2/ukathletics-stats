﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Session : ICloneable
    {
        /// <summary>
        /// Gets a value indicating whether Resources can currently login/attend to this Session.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can be attended; otherwise, <c>false</c>.
        /// </value>
        public Boolean CanAttend
        {
            get
            {
                Setting eSettingLoginEarly = Setting.GetByKey("LoginEarlyMinutes");
                Setting eSettingLoginLate = Setting.GetByKey("LoginLateMinutes");

                int iLoginEarly = int.Parse(eSettingLoginEarly.SettingValue);
                int iLoginLate = int.Parse(eSettingLoginLate.SettingValue);

                DateTime dtNow = DateTime.Now;
                DateTime dtStart = SessionDateTimeStart.AddMinutes(-iLoginEarly);
                DateTime dtEnd = SessionDateTimeStart.AddMinutes(iLoginLate);

                if (SessionIsCancelled)
                {
                    return false;
                }
                return (dtStart < dtNow && dtNow < dtEnd);
            }
        }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        /// <summary>
        /// Gets a value indicating whether this session is currently taking place.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is currently taking place; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsOccuring
        {
            get
            {
                DateTime dtNow = DateTime.Now;
                return (SessionDateTimeStart < dtNow && dtNow < SessionDateTimeEnd);
            }
        }

        /// <summary>
        /// Gets the duration of this session.
        /// </summary>
        public TimeSpan Duration
        {
            get { return (SessionDateTimeEnd - SessionDateTimeStart); }
        }

        /// <summary>
        /// Value just to hold a variable that we're checking for Group Sessions to display the Remove Student on Plan Sheet
        /// </summary>
        public Boolean IsGroup
        {
            get { return GetResourceInSession().Count() > 2; }
        }

        /// <summary>
        /// Property to hold the particular resource ID.  This is used for group tutoring sessions for display purposes only.
        /// </summary>
        public Guid ResourceID
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a new Tutoring Session.
        /// </summary>
        /// <param name="eCourse">The course that the session will cover.</param>
        /// <param name="eResourceTutor">The Tutor Resource of the created session.</param>
        /// <param name="eResources">The collection of student Resources that will be added to the created session.</param>
        /// <param name="dtOccurs">The date and time the created session will start.</param>
        /// <param name="Recurring">if set to <c>true</c>, the session will reoccur weekly.</param>
        /// <param name="football">if set to <c>true</c>, the session will occur at the new football location.</param>
        /// <returns>(WARNING: ALWAYS RETURNS TRUE, REGARDLESS OF SUCCESS) true if the session was created successfully; otherwise, <c>false</c>.</returns>
        public static Boolean CreateTutoringSession(Course eCourse, Resource eResourceTutor, IEnumerable<Resource> eResources, DateTime dtOccurs, Boolean Recurring, Boolean football, Boolean virtualSession)
        {
            SessionType eSessionTypeTutor = football ? SessionType.GetByName("Tutoring - Football") : SessionType.GetByName("Tutoring - CATS");
            Setting eSettingTutorSessionLength = Setting.GetByKey("TutorSessionLength");

            int iTutorSessionLength = Int32.Parse(eSettingTutorSessionLength.SettingValue.ToString());

            DateTime StartPeriod = dtOccurs.Date;
            DateTime EndPeriod = StartPeriod;

            if (Recurring)
            {
                EndPeriod = DateTime.Parse(Setting.GetByKey("SemesterDateEnd").SettingValue);
            }

            var context = new STATSEntities();

            while (StartPeriod <= EndPeriod)
            {
                if (StartPeriod.DayOfWeek == dtOccurs.DayOfWeek)
                {
                    DateTime sessionStartTime = StartPeriod.Date.Add(dtOccurs.TimeOfDay);

                    // Note: The two lines below can be used to manually inject a session at the specified time for debugging purposes
                    //var temp = new TimeSpan(13, 10, 00);
                    //DateTime sessionStartTime = StartPeriod.Date.Add(temp);
                    DateTime sessionEndTime = sessionStartTime.AddMinutes(iTutorSessionLength);

                    var eSession = Session.CreateSession(Guid.NewGuid(), eSessionTypeTutor.SessionTypeID, sessionStartTime, sessionEndTime, false, true);
                    eSession.CourseSectionID = null;

                    int teamworksID = 0;
                    if (eResourceTutor.TeamworksID.HasValue)
                    {
                        //Add the event to Teamworks
                        if (sessionStartTime > DateTime.Now)
                            teamworksID = Teamworks.TeamworksAPI.AddSession(eResourceTutor.TeamworksID.Value, "Tutoring - " + eCourse.CourseDisplayName, sessionStartTime, sessionEndTime, true);
                    }
                    foreach (Resource student in eResources)
                    {
                        if (student.TeamworksID.HasValue)
                        {
                            //Add the event to Teamworks
                            if (sessionStartTime > DateTime.Now)
                                teamworksID = Teamworks.TeamworksAPI.AddSession(student.TeamworksID.Value, "Tutoring - " + eCourse.CourseDisplayName, sessionStartTime, sessionEndTime, true);
                        }
                    }

                    eSession.TeamworksID = teamworksID;
                    context.Sessions.AddObject(eSession);
 
                    var eSessionTutor = SessionTutor.CreateSessionTutor(Guid.NewGuid(), eSession.SessionID);
                    eSessionTutor.CourseID = eCourse.CourseID;
                    eSessionTutor.RoomID = null;
                    context.SessionTutors.AddObject(eSessionTutor);

                    var eScheduleTutor = Schedule.CreateSchedule(Guid.NewGuid(), eResourceTutor.ResourceID, eSession.SessionID, true, virtualSession);
                    context.Schedules.AddObject(eScheduleTutor);

                    foreach (var eResource in eResources)
                    {
                        var eScheduleResource = Schedule.CreateSchedule(Guid.NewGuid(), eResource.ResourceID, eSession.SessionID, true, virtualSession  );
                        context.Schedules.AddObject(eScheduleResource);
                    }
                }
                StartPeriod = StartPeriod.AddDays(1);
            }
            context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Join an existing tutoring session
        /// </summary>
        /// <param name="eSessionID">A current sessionID from the group</param>
        /// <param name="eResourceTutor">The Tutor Resource of the created session.</param>
        /// <param name="eResources">The collection of student Resources that will be joined to the session.</param>
        /// <returns>(WARNING: ALWAYS RETURNS TRUE, REGARDLESS OF SUCCESS) true if the session was created successfully; otherwise, <c>false</c>.</returns>
        public static Boolean JoinTutoringSession(Guid eSessionID, Resource eResourceTutor, IEnumerable<Resource> eNewResources)
        {
            Setting eSettingTutorSessionLength = Setting.GetByKey("TutorSessionLength");

            var context = new STATSEntities();

            Session eSession = Session.Get(eSessionID);

            Session[] allTutoringSessions = eResourceTutor.TutoringSessions;
            List<Session> applicableSessions = new List<Session>();
            foreach (Session session in allTutoringSessions)
            {
                if(session.SessionDateTimeStart.DayOfWeek == eSession.SessionDateTimeStart.DayOfWeek
                    && session.SessionDateTimeStart.TimeOfDay == eSession.SessionDateTimeStart.TimeOfDay
                    && session.CourseSectionID == eSession.CourseSectionID 
                    )
                    applicableSessions.Add(session);
            }


            //TEAMWORKS

            //The difficulty in joining an existing Group tutoring session is recurrence.  
            foreach (Session session in applicableSessions)
            {
                //Create new schedule for the resource given the specific Sessions that already exist
                foreach (var eResource in eNewResources)
                {
                    var eScheduleResource = Schedule.CreateSchedule(Guid.NewGuid(), eResource.ResourceID, session.SessionID, true, false);
                    context.Schedules.AddObject(eScheduleResource);

                    //Add the session to Teamworks
                    int teamworksID = 0;
                    if (eResource.TeamworksID.HasValue)
                    {
                        //Add the event to Teamworks -- only if it's in the future
                        if (session.SessionDateTimeStart > DateTime.Now)
                            teamworksID = Teamworks.TeamworksAPI.AddSession(eResource.TeamworksID.Value, session.CourseSection.CourseDisplayName, session.SessionDateTimeStart, session.SessionDateTimeEnd, true);
                    }
                    session.TeamworksID = teamworksID;
                }
                context.SaveChanges();
            }

            //while (StartPeriod <= EndPeriod)
            //{
            //    if (StartPeriod.DayOfWeek == dtOccurs.DayOfWeek)
            //    {
            //        DateTime sessionStartTime = StartPeriod.Date.Add(dtOccurs.TimeOfDay);

            ////        // Note: The two lines below can be used to manually inject a session at the specified time for debugging purposes
            ////        //var temp = new TimeSpan(13, 10, 00);
            ////        //DateTime sessionStartTime = StartPeriod.Date.Add(temp);
            //        DateTime sessionEndTime = sessionStartTime.AddMinutes(iTutorSessionLength);

            //        var eSession = Session.CreateSession(Guid.NewGuid(), eSessionTypeTutor.SessionTypeID, sessionStartTime, sessionEndTime, false, true);
            ////        eSession.CourseSectionID = null;
            ////        context.Sessions.AddObject(eSession);

            ////        var eSessionTutor = SessionTutor.CreateSessionTutor(Guid.NewGuid(), eSession.SessionID);
            ////        eSessionTutor.CourseID = eCourse.CourseID;
            ////        eSessionTutor.RoomID = null;
            ////        context.SessionTutors.AddObject(eSessionTutor);

            ////        var eScheduleTutor = Schedule.CreateSchedule(Guid.NewGuid(), eResourceTutor.ResourceID, eSession.SessionID, true);
            ////        context.Schedules.AddObject(eScheduleTutor);

            ////        foreach (var eResource in eResources)
            ////        {
            ////            var eScheduleResource = Schedule.CreateSchedule(Guid.NewGuid(), eResource.ResourceID, eSession.SessionID, true);
            ////            context.Schedules.AddObject(eScheduleResource);
            ////        }
            ////    }
            //    StartPeriod = StartPeriod.AddDays(1);
            //}
            context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Gets an array of sessions that are possible to be attended at the current time by a supplied resource.
        /// </summary>
        /// <param name="eResource">The resource to find available sessions for.</param>
        /// <param name="eSessionType">The SessionType of the returned sessions.</param>
        /// <returns>An array of sessions that the supplied resource can currently attend.</returns>
        public static Session[] GetActive(Resource eResource, SessionType eSessionType)
        {
            Setting eSettingLoginEarly = Setting.GetByKey("LoginEarlyMinutes");
            Setting eSettingLoginLate = Setting.GetByKey("LoginLateMinutes");

            // This sounds really silly; but the calculation HAS to be done in reverse.  Test it if you don't believe me.
            int late = Int32.Parse(eSettingLoginEarly.SettingValue);
            int early = Int32.Parse(eSettingLoginLate.SettingValue);

            DateTime Start = DateTime.Now.AddMinutes(early * -1);
            DateTime End = DateTime.Now.AddMinutes(late);

            var context = new STATSEntities();
            var eSessions = (from Schedule obj in context.Schedules.Include("Session")
                             where
                                 obj.ResourceID == eResource.ResourceID &&
                                 obj.Session.SessionTypeID == eSessionType.SessionTypeID &&
                                 obj.Session.SessionDateTimeStart >= Start &&
                                 obj.Session.SessionDateTimeStart <= End &&
                                 obj.Session.SessionIsScheduled == true &&
                                 obj.Session.SessionIsCancelled == false
                             select obj.Session).ToArray();
            return eSessions;
        }

        /// <summary>
        /// Gets an array of sessions that are possible to be attended at the current time.
        /// </summary>
        /// <param name="eSessionType">The SessionType of the returned sessions.</param>
        /// <returns>An array of sessions that can currently be attended.</returns>
        public static Session[] GetActive(SessionType eSessionType)
        {
            Setting eSettingLoginEarly = Setting.GetByKey("LoginEarlyMinutes");
            Setting eSettingLoginLate = Setting.GetByKey("LoginLateMinutes");

            int early = Int32.Parse(eSettingLoginEarly.SettingValue);
            int late = Int32.Parse(eSettingLoginLate.SettingValue);

            DateTime Start = DateTime.Now.AddMinutes(-early);
            DateTime End = DateTime.Now.AddMinutes(late);

            var context = new STATSEntities();
            var eSessions = (from Session obj in context.Sessions
                             where
                                 obj.SessionTypeID == eSessionType.SessionTypeID &&
                                 obj.SessionDateTimeStart >= Start &&
                                 obj.SessionDateTimeStart <= End
                             select obj).ToArray();
            return eSessions;
        }

        /// <summary>
        /// Determines whether this session has any metadata.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if session has additional metadata; otherwise, <c>false</c>.
        /// </returns>
        public Boolean HasAdditionalData()
        {
            var context = new STATSEntities();
            var cnt = (from SessionTutor obj in context.SessionTutors where obj.SessionID == this.SessionID select obj).Count();
            return cnt > 0;
        }

        /// <summary>
        /// Gets the additional metadata for this session.
        /// </summary>
        /// <returns>A <c>SessionTutor</c> object containing metadata.</returns>
        /// <exception cref="ArgumentNullException">If this session has no associated metadata.</exception>
        public SessionTutor GetAdditionalData()
        {
            var context = new STATSEntities();
            var eSessionTutor = (from SessionTutor obj in context.SessionTutors where obj.SessionID == this.SessionID select obj).First();
            return eSessionTutor;
        }

        /// <summary>
        /// Gets a session object by its ID.
        /// </summary>
        /// <param name="SessionID">The ID of the Session.</param>
        /// <returns>The session object associated with the supplied SessionID</returns>
        /// <exception cref="ArgumentNullException">If no session exists with the supplied SessionID.</exception>
        public static Session Get(Guid SessionID)
        {
            var context = new STATSEntities();
            return (from STATSModel.Session obj in context.Sessions where obj.SessionID == SessionID select obj).First();
        }

        /// <summary>
        /// Gets an array containing every session in the database.
        /// </summary>
        /// <returns>An array containing every session in the database.</returns>
        public static Session[] Get()
        {
            var context = new STATSEntities();
            return (from STATSModel.Session obj in context.Sessions select obj).ToArray();
        }

        /// <summary>
        /// Determines whether this instance has a scheduled tutor.  This does not indicate that a tutor is present.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance has a scheduled tutor; otherwise, <c>false</c>.
        /// </returns>
        public Boolean HasTutor(ResourceType resourceTypeTutor)
        {
            Resource[] Participants = GetResourceInSession();
            Guid ResourceTypeTutorID = resourceTypeTutor.ResourceTypeID;

            foreach (Resource ResourceObj in Participants)
            {
                if (ResourceObj.ResourceTypeID == ResourceTypeTutorID)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the Tutor for this session, if one exists.
        /// </summary>
        /// <returns>The Tutor for this session, if one exists; otherwise, a blank Resource object.</returns>
        public Resource GetTutor(ResourceType resourceTypeTutor)
        {
            Resource[] Participants = GetResourceInSession();
            Guid ResourceTypeTutorID = resourceTypeTutor.ResourceTypeID;

            foreach (Resource ResourceObj in Participants)
            {
                if (ResourceObj.ResourceTypeID == ResourceTypeTutorID)
                {
                    return ResourceObj;
                }
            }
            return new Resource();
        }

        /// <summary>
        /// Gets an array of Students for this session.
        /// </summary>
        /// <returns>An array containing the students scheduled for this session.</returns>
        public Resource[] GetStudents()
        {
            Resource[] Participants = GetResourceInSession();
            ResourceType eResourceType = ResourceType.Get("Student");
            Guid ResourceTypeStudentID = eResourceType.ResourceTypeID;
            List<Resource> eResources = new List<Resource>();
            foreach (Resource ResourceObj in Participants)
            {
                if (ResourceObj.ResourceTypeID == ResourceTypeStudentID)
                {
                    eResources.Add(ResourceObj);
                }
            }
            return eResources.ToArray();
        }

        /// <summary>
        /// Gets an array of Resources that are scheduled for this session.
        /// </summary>
        /// <returns>An array of Resources that are scheduled for this session.</returns>
        public Resource[] GetResourceInSession()
        {
            return GetResourceInSession(this.SessionID);
        }

        /// <summary>
        /// Gets an array of Resources that are scheduled for this session.
        /// </summary>
        /// <param name="SessionID">The ID of the session to be queried on.</param>
        /// <returns>An array of Resources that are scheduled for the session cooresponding to the provided Session ID.</returns>
        public static Resource[] GetResourceInSession(Guid SessionID)
        {
            var context = new UK.STATS.STATSModel.STATSEntities();
            var Matching = new List<Resource>();
            var eSession = (from STATSModel.Session obj in context.Sessions.Include("Schedules").Include("Schedules.Resource") where obj.SessionID == SessionID select obj).First();
            foreach (var eSchedule in eSession.Schedules)
            {
                STATSModel.Resource eResource = eSchedule.Resource;
                Matching.Add(eResource);
            }
            return Matching.ToArray();
        }

        /// <summary>
        /// Attempts to assign a room to this session.
        /// </summary>
        /// <returns>If a room was available (and assigned) returns that room; otherwise, null.</returns>
        /// <exception cref="Exception">If the requirements were not met for this session to be assigned a room.</exception>
        public Room AssignRoom(ResourceType resourceTypeTutor, Guid LocationID)
        {
            if (HasTutorAndStudentPresent())
            {
                var context = new STATSEntities();
                SessionTutor eSessionTutor;
                if (HasAdditionalData())
                {
                    SessionTutor oSessionTutor = GetAdditionalData(); // Temporary, just to get the ID.
                    Guid SessionTutorID = oSessionTutor.SessionTutorID;
                    eSessionTutor = (from SessionTutor obj in context.SessionTutors where obj.SessionTutorID == SessionTutorID select obj).First();
                    var curTutor = this.GetTutor(resourceTypeTutor);
                    if (curTutor.ResourceRoom.GetValueOrDefault(false))
                    {
                        //Room eRoom = new Room();
                        //eRoom.RoomID = Guid.NewGuid();
                        //eRoom.RoomName = "---";
                        //eRoom.RoomPriority = 500;
                        //eRoom.RoomCapacity = 10;
                        //eSessionTutor.Room = eRoom;
                        //eSessionTutor.RoomID = eRoom.RoomID;

                        //Room eRoom = Room.GetPersonalRoom();
                        //eSessionTutor.Room = eRoom;
                        //eSessionTutor.RoomID = eRoom.RoomID;

                        int Count = GetResourceInSession().Count();
                        Room eRoom = Room.GetAvailableRoom(context, Count, true, LocationID);
                        if (eRoom == null)
                        {
                            throw new Exception("No rooms available.");
                        }
                        eSessionTutor.Room = eRoom;
                        eSessionTutor.RoomID = eRoom.RoomID;
                    }
                    else
                    {
                        if (!eSessionTutor.RoomID.HasValue || eSessionTutor.Room.RoomName == "N/A")
                        {
                            int Count = GetResourceInSession().Count();
                            Room eRoom = Room.GetAvailableRoom(context, Count, false, LocationID);
                            if (eRoom == null)
                            {
                                throw new Exception("No rooms available.");
                            }
                            eSessionTutor.RoomID = eRoom.RoomID;
                        }
                    }
                    context.SaveChanges();
                }
                else
                {
                    eSessionTutor = SessionTutor.CreateSessionTutor(Guid.NewGuid(), SessionID);
                    var curTutor = this.GetTutor(resourceTypeTutor);

                    //var curTutor = STATSModel.Resource.Get(eSessionTutor.SessionTutorID);
                    if (curTutor.ResourceRoom.GetValueOrDefault(false))
                    {
                        //Room eRoom = new Room();
                        //eRoom.RoomID = Guid.NewGuid();
                        //eRoom.RoomName = "---";
                        //eRoom.RoomPriority = 1;
                        //eRoom.RoomCapacity = 10;
                        //eSessionTutor.Room = eRoom;
                        //eSessionTutor.RoomID = eRoom.RoomID;

                        //Room eRoom = Room.GetPersonalRoom();
                        //eSessionTutor.Room = eRoom;
                        //eSessionTutor.RoomID = eRoom.RoomID;

                        int Count = GetResourceInSession().Count();
                        Room eRoom = Room.GetAvailableRoom(context, Count, true, LocationID);

                        if (eRoom == null)
                        {
                            throw new Exception("No rooms available.");
                        }
                        eSessionTutor.Room = eRoom;
                        eSessionTutor.RoomID = eRoom.RoomID;
                    }
                    else
                    {
                        int Count = GetResourceInSession().Count();
                        Room eRoom = Room.GetAvailableRoom(context, Count, false, LocationID);
                        if (eRoom == null)
                        {
                            throw new Exception("No rooms available.");
                        }
                        eSessionTutor.RoomID = eRoom.RoomID;
                    }
                    context.SessionTutors.AddObject(eSessionTutor);
                    context.SaveChanges();
                }
                return eSessionTutor.Room;
            }
            else
            {
                throw new Exception("Rooms cannot be assigned to a session without a Tutor and a Student being present.");
            }
        }

        /// <summary>
        /// Determines whether this session has both a tutor and a student currently present.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if a tutor and student are present; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>In order to be assigned a room, this condition must be met.</remarks>
        public Boolean HasTutorAndStudentPresent()
        {
            ResourceType eResourceTypeTutor = ResourceType.Get("Tutor");
            ResourceType eResourceTypeStudent = ResourceType.Get("Student");

            Boolean hasTutor = false;
            Boolean hasStudent = false;
            Resource[] eResources = GetResourceInSession();
            foreach (var eResource in eResources)
            {
                if (eResource.IsPresent(this))
                {
                    if (eResource.ResourceTypeID == eResourceTypeTutor.ResourceTypeID)
                    {
                        hasTutor = true;
                    }
                    if (eResource.ResourceTypeID == eResourceTypeStudent.ResourceTypeID)
                    {
                        hasStudent = true;
                    }
                }
            }
            return (hasTutor && hasStudent);
        }

        /// <summary>
        /// Fills the tutoring session arrays using a new faster way
        /// </summary>
        /// <param name="eSessionsActiveAndScheduled">Array to output with Active and Scheduled</param>
        /// <param name="eSessionsActiveAndUnScheduled">Array to output with Active and Unscheduled</param>
        /// <param name="eSessionsScheduledInactive">Array to output with Inactive and Scheduled</param>
        /// <param name="eTutoringSessions">Array to output with Inactive and Unscheduled</param>
        public static void FillTutoringSessionArrays(out Session[] eSessionsActiveAndScheduled, out Session[] eSessionsActiveAndUnScheduled, out Session[] eSessionsScheduledInactive, out Session[] eTutoringSessions, bool footballLocation)
        {
            using (var context = new UK.STATS.STATSModel.STATSEntities())
            {
                //Call stored proc to get complex type result with {SessionId, SessionIsScheduled, StudentAndTutorPresent} columns
                Guid SessionTypeID = footballLocation ? SessionType.GetByName("Tutoring - Football").SessionTypeID : SessionType.GetByName("Tutoring - CATS").SessionTypeID;
                var presenceInfo = context.GetPresenceInfo().Where(x=>x.SessionTypeID == SessionTypeID).ToList();


                //3/2/2017 - RCH (Apax) - Make sure that all queries include SessionTypeID just to make sure they're only pulling
                //entries for the correct location

                //Get All the session IDs into an array
                Guid[] sessionIds = (from STATSModel.GetPresenceInfo_Result obj in presenceInfo
                                     where obj.SessionTypeID == SessionTypeID
                                     select obj.SessionID).ToArray();

                //Get all the Session objects from the database
                Dictionary<Guid, Session> allReturnedSessions = (from STATSModel.Session obj in context.Sessions
                                                                 where sessionIds.Contains(obj.SessionID) && obj.SessionTypeID == SessionTypeID
                                                                 select obj).ToDictionary(o => o.SessionID);

                //Now sort the sessions out into their appropriate arrays

                //Old equivalent: GetTutoringSessions(true, true)
                eSessionsActiveAndScheduled = (from STATSModel.GetPresenceInfo_Result obj in presenceInfo
                                               where obj.StudentAndTutorPresent.Value && obj.SessionIsScheduled && obj.SessionTypeID == SessionTypeID
                                               select allReturnedSessions[obj.SessionID]).ToArray();

                //Old equivalent: GetTutoringSessions(true, false)
                eSessionsActiveAndUnScheduled = (from STATSModel.GetPresenceInfo_Result obj in presenceInfo
                                                 where obj.StudentAndTutorPresent.Value && !obj.SessionIsScheduled && obj.SessionTypeID == SessionTypeID
                                                 select allReturnedSessions[obj.SessionID]).ToArray();

                //Old equivalent: GetTutoringSessions(false, true)
                eSessionsScheduledInactive = (from STATSModel.GetPresenceInfo_Result obj in presenceInfo
                                              where !obj.StudentAndTutorPresent.Value && obj.SessionIsScheduled && obj.SessionTypeID == SessionTypeID
                                              select allReturnedSessions[obj.SessionID]).ToArray();

                //Old equivalent: GetTutoringSessions(false, false)
                eTutoringSessions = (from STATSModel.GetPresenceInfo_Result obj in presenceInfo
                                     where !obj.StudentAndTutorPresent.Value && !obj.SessionIsScheduled && obj.SessionTypeID == SessionTypeID
                                     select allReturnedSessions[obj.SessionID]).ToArray();
            }
        }

        /// <summary>
        /// Gets the students who are currently present in this session.
        /// </summary>
        /// <returns>An array of resources currently in attendance.</returns>
        public Resource[] GetPresentResources()
        {
            var eResourcesPresent = new List<Resource>();
            var eResources = GetResourceInSession();
            eResources = (from Resource obj in eResources select obj).ToArray();

            foreach (var eResource in eResources)
            {
                Boolean isPresent = eResource.IsPresent(this);
                if (isPresent)
                {
                    eResourcesPresent.Add(eResource);
                }
            }

            return eResourcesPresent.ToArray();
        }

        /// <summary>
        /// Gets the students who are currently present in this session.
        /// </summary>
        /// <returns>An array of student resources currently in attendance.</returns>
        public Resource[] GetPresentStudents()
        {
            ResourceType eResourceTypeTutor = ResourceType.Get("Tutor");
            var eResourcesPresent = new List<Resource>();
            var eResources = GetResourceInSession();
            eResources = (from Resource obj in eResources where obj.ResourceTypeID != eResourceTypeTutor.ResourceTypeID select obj).ToArray();

            foreach (var eResource in eResources)
            {
                Boolean isPresent = eResource.IsPresent(this);
                if (isPresent)
                {
                    eResourcesPresent.Add(eResource);
                }
            }

            return eResourcesPresent.ToArray();
        }

        /// <summary>
        /// Gets a value indicating whether there is at least one resource absent.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has a resource absent; otherwise, <c>false</c>.
        /// </value>
        public Boolean HasResourceMissing
        {
            get
            {
                Resource[] eResources = GetResourceInSession();
                foreach (var eResource in eResources)
                {
                    if (!eResource.IsPresent(this))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there is at least one resource present.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has resource present; otherwise, <c>false</c>.
        /// </value>
        public Boolean HasResourcePresent
        {
            get
            {
                Resource[] eResources = GetResourceInSession();
                foreach (var eResource in eResources)
                {
                    if (eResource.IsPresent(this))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Checks that the specified Course Section will not conflict with the schedule of the specified Resource.
        /// </summary>
        /// <param name="eResource">The Resource whose schedule will be checked against.</param>
        /// <param name="eCourseSection">The Course Section for which availability will be verified.</param>
        /// <returns>true if there was no conflict; otherwise false.</returns>
        public static Boolean DoesTimeConflict(Resource eResource, CourseSection eCourseSection)
        {
            return DoesTimeConflict(eResource, eCourseSection.CourseSectionDateStart, eCourseSection.CourseSectionDateStart.AddMinutes(55));
        }

        /// <summary>
        /// Checks that the specified start/end time range will not conflict with the schedule of the specified Resource.
        /// </summary>
        /// <param name="eResource">The Resource whose schedule will be checked against.</param>
        /// <param name="AddMeStartTime">The end of the time range to check</param>
        /// <param name="AddMeEndTime">The end of the time range to check</param>
        /// <returns>true if there was no conflict; otherwise false.</returns>
        public static Boolean DoesTimeConflict(Resource eResource, DateTime AddMeStartTime, DateTime AddMeEndTime)
        {
            Boolean Conflict = false;

            // Verify No Conflicting Courses
           
            
            CourseSection[] RegisteredCourses = GetRegisteredCourses(eResource);
            List<Session> DerivedSessions = new List<Session>();
            foreach (var registeredCourse in RegisteredCourses)
            {
                Session[] DerivedSessionsForThisCourse = GetCourseSectionSessions(registeredCourse);
                DerivedSessions.AddRange(DerivedSessionsForThisCourse);
            }

            //foreach (Session CheckSession in eSessions) // Iterate over the checking course's sessions.
            //{
            // Note: Check for overlapping courses
            foreach (Session ExistingSession in DerivedSessions) // Iterate over all the sessions for pre-existing registered courses.
            {
                if (!Conflict)
                    Conflict = CheckSessionsForConflicts(AddMeStartTime, AddMeEndTime, ExistingSession);
                else return Conflict;
            }

            //}

            // Verify No Conflicting Tutoring Sessions (Scheduled Tutoring Sessions)
            var context = new STATSEntities();
            var eSchedules = (from Schedule obj in context.Schedules.Include("Session") where obj.ResourceID == eResource.ResourceID && obj.Session.SessionIsScheduled && !obj.Session.SessionIsCancelled && obj.Session.SessionDateTimeStart > DateTime.Today select obj).ToList();

            foreach (Schedule eSchedule in eSchedules) // Iterate over all Non-Class Schedules
            {
                var eSession = eSchedule.Session;
                if( !eSession.SessionIsCancelled)
                    if (!Conflict)
                        Conflict = CheckSessionsForConflicts(AddMeStartTime, AddMeEndTime, eSession);
                    else return Conflict;
            }

            // Verify no conflicting free form entry notes
            //var temp = "notes";
            //var eNotes = (from Schedule obj in context.Schedules.Include("Session") where obj.ResourceID == eResource.ResourceID && obj.Session.SessionType.SessionTypeName == "Note" && obj.Session.SessionDateTimeStart > DateTime.Today select obj).ToList();

            SessionType eSessionTypeNote = SessionType.GetByName("note");
            var eSchedulesNotes = (
                                      from Schedule obj
                                          in context.Schedules.Include("Session").Include("Session.SessionType").Include("Session.CourseSection").Include("Session.CourseSection.Course").Include("Session.CourseSection.Course.Subject")
                                      where
                                          obj.Session.SessionTypeID == eSessionTypeNote.SessionTypeID
                                          &&
                                          obj.ResourceID == eResource.ResourceID
                                          &&
                                          obj.Session.SessionIsScheduled
                                          &&
                                          !obj.Session.SessionIsCancelled
                                      select obj
                                    ).ToList();
            foreach (Schedule eSchedule in eSchedulesNotes) // Iterate over all Non-Class Schedules
            {
                var eSession = eSchedule.Session;
                if (!Conflict)
                    Conflict = CheckSessionsForConflicts(AddMeStartTime, AddMeEndTime, eSession);
                else return Conflict;
            }

            return Conflict;
        }

        /// <summary>
        /// Gets an array of Course Sections that the specified Resource is registered for.
        /// </summary>
        /// <param name="eResource">The Resource to get the Course Sections for.</param>
        /// <returns>An array of Course Sections that that specified Resource is registered for.</returns>
        public static CourseSection[] GetRegisteredCourses(Resource eResource)
        {
            var context = new STATSEntities();
            var eCourseSections = (from ResourceRegistration obj in context.ResourceRegistrations.Include("CourseSection")
                                   where obj.ResourceID == eResource.ResourceID
                                   select obj.CourseSection).ToArray();
            return eCourseSections;
        }

        /// <summary>
        /// Derives the Sessions for the specified Course Section.
        /// </summary>
        /// <param name="eCourseSection">The Course Section to retrieve the Sessions of.</param>
        /// <returns>An array of dynamically generated Sessions associated with the provided Course Section.</returns>
        public static Session[] GetCourseSectionSessions(CourseSection eCourseSection)
        {
            SessionType eSessionTypeClass = SessionType.GetByName("Class");
            var DerivedSessions = new List<Session>();

            String DaysMeet = eCourseSection.CourseSectionDaysMeet;
            char[] DaysArray = DaysMeet.ToCharArray();

            DateTime DateStart = eCourseSection.CourseSectionDateStart;
            DateTime DateEnd = eCourseSection.CourseSectionDateEnd;

            TimeSpan TimeStart = eCourseSection.CourseSectionTimeStart;
            TimeSpan TimeEnd = eCourseSection.CourseSectionTimeEnd;

            DateTime CurrentDate = DateStart;
            while (CurrentDate <= DateEnd)
            {
                Boolean doesMeet = false;
                switch (CurrentDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (DaysArray.Contains('M')) { doesMeet = true; } break;
                    case DayOfWeek.Tuesday:
                        if (DaysArray.Contains('T')) { doesMeet = true; } break;
                    case DayOfWeek.Wednesday:
                        if (DaysArray.Contains('W')) { doesMeet = true; } break;
                    case DayOfWeek.Thursday:
                        if (DaysArray.Contains('R')) { doesMeet = true; } break;
                    case DayOfWeek.Friday:
                        if (DaysArray.Contains('F')) { doesMeet = true; } break;
                    case DayOfWeek.Saturday:
                        if (DaysArray.Contains('S')) { doesMeet = true; } break;
                    case DayOfWeek.Sunday:

                        // NOTE: Sunday Sessions do not get automatically generated.
                        break;
                    default:
                        break;
                }
                if (doesMeet)
                {
                    DateTime ExactTimeTodayStart = CurrentDate.Date.Add(TimeStart);
                    DateTime ExactTimeTodayEnd = CurrentDate.Date.Add(TimeEnd);
                    Session DerivedSession = new Session()
                    {
                        SessionID = Guid.Empty,
                        CourseSectionID = eCourseSection.CourseSectionID,
                        SessionTypeID = eSessionTypeClass.SessionTypeID,
                        SessionDateTimeStart = ExactTimeTodayStart,
                        SessionDateTimeEnd = ExactTimeTodayEnd,
                        SessionIsCancelled = false,
                        SessionIsScheduled = true
                    };
                    DerivedSessions.Add(DerivedSession);
                }
                CurrentDate = CurrentDate.AddDays(1);
            }
            return DerivedSessions.ToArray();
        }

        private static bool CheckSessionsForConflicts(DateTime AddMeStartTime, DateTime AddMeEndTime, Session oldSession)
        {
            var oldStartTime = oldSession.SessionDateTimeStart.TimeOfDay;
            var oldEndTime = oldSession.SessionDateTimeEnd.TimeOfDay;
            var newStartTime = AddMeStartTime.TimeOfDay;
            var newEndTime = AddMeEndTime.TimeOfDay;
            Boolean Conflict = false;

            if (oldSession.SessionDateTimeStart.DayOfWeek == AddMeStartTime.DayOfWeek)
            {
                if (Conflict) { Conflict = true; } // Don't look any further if we've already found an time conflict.

                // Case 1: New session starts and ends before old session
                // N--------N
                //     E---------E
                if ((newStartTime < oldStartTime) && (newEndTime < oldEndTime) && (newEndTime > oldStartTime))
                {
                    Conflict = true;
                }

                // Case 2: New session starts and ends after old session
                //     N---------N
                // E--------E
                if ((newStartTime > oldStartTime) && (newEndTime > oldEndTime) && (newStartTime < oldEndTime))
                {
                    Conflict = true;
                }

                // Case 3: Sessions are at the same time
                // N-------N
                // E-------E
                if ((newStartTime == oldStartTime) && (newEndTime == oldEndTime))
                {
                    Conflict = true;
                }

                // Case 4: New session is contained by the old session
                //    N--N
                // E-------E
                if ((newStartTime > oldStartTime) && (newEndTime < oldEndTime))
                {
                    Conflict = true;
                }

                // Case 5: New session contains the old session
                // N-------N
                //    E--E
                if ((newStartTime < oldStartTime) && (newEndTime > oldEndTime))
                {
                    Conflict = true;
                }

                // Case 6: New session starts at the same time as the old session
                // N---N
                // E-------E
                if (newStartTime == oldStartTime)
                {
                    Conflict = true;
                }

                // Case 7: New session ends at the same time as the old session
                //     N---N
                // E-------E
                if (newEndTime == oldEndTime)
                {
                    Conflict = true;
                }
            }
            return Conflict;
        }
    }
}