﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Course
    {
        public Boolean CanBeDeleted
        {
            get
            {
                int iCourseSections = this.CourseSections.Count;
                int iCourseTutors = this.CourseTutors.Count;
                int sum = iCourseSections + iCourseTutors;
                return (sum == 0);
            }
        }


        public String CourseDisplayName
        {
            get
            {
                return this.Subject.SubjectNameShort + this.CourseNumber;
            }
        }

        public static Course[] Get()
        {
            var context = new STATSEntities();
            return (from Course obj in context.Courses orderby obj.CourseNumber ascending select obj).ToArray();
        }

        public static Course Get(Guid CourseID)
        {
            var context = new STATSEntities();
            return (from Course obj in context.Courses where obj.CourseID == CourseID select obj).Single();
        }

        public static Course Get(Guid SubjectID, String CourseNumber)
        {
            var context = new STATSEntities();
            return (from Course obj in context.Courses where obj.SubjectID == SubjectID && obj.CourseNumber == CourseNumber select obj).Single();
        }

        public static Boolean Exists(Guid SubjectID, String CourseNumber)
        {
            var context = new STATSEntities();
            var result = (from Course obj in context.Courses where obj.SubjectID == SubjectID && obj.CourseNumber == CourseNumber select obj).Count();
            return result > 0;
        }

        public void AddTutor(Resource eResource)
        {
            var context = new STATSEntities();
            Boolean isDuplicate = (from CourseTutor obj in CourseTutors where obj.ResourceID == eResource.ResourceID select obj).Count() > 0;
            if (!isDuplicate)
            {
                var eCourseTutor = new CourseTutor();
                eCourseTutor.CourseTutorID = Guid.NewGuid();
                eCourseTutor.CourseID = this.CourseID;
                eCourseTutor.ResourceID = eResource.ResourceID;
                context.CourseTutors.AddObject(eCourseTutor);
                context.SaveChanges();    
            }
        }

        public void RemoveTutor(Resource eResource)
        {
            var context = new STATSEntities();
            var eCourseTutors = eResource.CourseTutors;
            foreach (var eCourseTutor in eCourseTutors)
            {
                if (eCourseTutor.CourseID == this.CourseID)
                {
                    var eCourseTutorToDelete = (from CourseTutor obj in context.CourseTutors where obj.CourseTutorID == eCourseTutor.CourseTutorID select obj).First();
                    context.CourseTutors.DeleteObject(eCourseTutorToDelete);
                }
            }
            context.SaveChanges();
        }
    }
}
