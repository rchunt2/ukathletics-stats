﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using UK.STATS.STATSModel;

namespace UK.STATS.STATSModel.Teamworks
{
    public class TeamworksAPI
    {
        public static string RetrieveTeams()
        {
            var eSettingHost = STATSModel.Setting.GetByKey("TeamworksBaseURL");
            string strURLTWBaseURL = String.Format(eSettingHost.SettingValue + "user/v2/teams");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            WebRequest requestObjGet = System.Net.WebRequest.Create(strURLTWBaseURL);
            requestObjGet.Method = "GET";
            requestObjGet.Headers["Authorization"] = "Bearer " + Setting.GetByKey("TeamworksBearerToken").SettingValue;
            
            HttpWebResponse responseObjGet = null;
            responseObjGet = (HttpWebResponse)requestObjGet.GetResponse();

            string strResultTest = null;
            using (Stream stream = responseObjGet.GetResponseStream())
            {
                StreamReader sr = new StreamReader(stream);
                strResultTest = sr.ReadToEnd();
                sr.Close();
            }

            return strResultTest;
        }

        public static string RetrievePlayersByTeam(string teamID)
        {
            var eSettingHost = Setting.GetByKey("TeamworksBaseURL");
            string strURLTWBaseURL = String.Format(eSettingHost.SettingValue + "user/v2/teams/" + teamID + "/users");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            WebRequest requestObjGet = System.Net.WebRequest.Create(strURLTWBaseURL);
            requestObjGet.Method = "GET";
            requestObjGet.Headers["Authorization"] = "Bearer " + Setting.GetByKey("TeamworksBearerToken").SettingValue;

            HttpWebResponse responseObjGet = null;
            responseObjGet = (HttpWebResponse)requestObjGet.GetResponse();

            string strResultTest = null;
            using (Stream stream = responseObjGet.GetResponseStream())
            {
                StreamReader sr = new StreamReader(stream);
                strResultTest = sr.ReadToEnd();
                sr.Close();
            }

            return strResultTest;
        }

        public static void SyncTeamworksIDs()
        {
            JArray jArray = JArray.Parse(RetrieveTeams());
            foreach (JObject o in jArray.Children<JObject>())
            {
                string teamworksID = o["id"].ToString();
                string teamworksName = o["label"].ToString();
                using (var context = new STATSEntities())
                {
                    //Get all of the Players for the Team and add their Teamwork IDs  
                    JArray jArray2 = JArray.Parse(RetrievePlayersByTeam(teamworksID));
                    foreach (JObject o2 in jArray2.Children<JObject>())
                    {
                        string playerTeamworksID = o2["id"].ToString();
                        string playerOrgID = o2["orgID"].ToString();
                        string playerADLogin = playerOrgID.Split('@')[0];
                        try
                        {
                            Resource resource = context.Resources.FirstOrDefault(x => x.ResourceADLogin == playerADLogin);
                            if (resource != null)
                            {
                                resource.TeamworksID = int.Parse(playerTeamworksID);
                                context.SaveChanges();
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
        }

        public static int AddSession(int attendeeTeamworksID, string sessionName, DateTime startDate, DateTime endDate, bool recurs)
        {
            var eSettingHost = Setting.GetByKey("TeamworksBaseURL");
            string strURLTWBaseURL = String.Format(eSettingHost.SettingValue + "calendar/v1/calendars/events");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            WebRequest requestObj = System.Net.WebRequest.Create(strURLTWBaseURL);
            requestObj.Method = "POST";
            requestObj.ContentType = "application/json";
            requestObj.Headers["Authorization"] = "Bearer " + Setting.GetByKey("TeamworksBearerToken").SettingValue;

            List<Attendee> attendeeList = new List<Attendee>
            {
                new Attendee { id = attendeeTeamworksID, type= "individual"}
            };
            
            JObject oJsonObject = new JObject();
            oJsonObject.Add("allDay", false);
            oJsonObject.Add("appointmentType", "Academic Tutoring");
            oJsonObject.Add("attendees", JToken.FromObject(attendeeList));
            oJsonObject.Add("attending", "attending");
            oJsonObject.Add("end", endDate.ToString());
            oJsonObject.Add("label", sessionName);
            oJsonObject.Add("recurs", recurs);
            oJsonObject.Add("start", startDate.ToString());
            oJsonObject.Add("timezone", "America/New_York");

            using (var streamWriter = new StreamWriter(requestObj.GetRequestStream()))
            {
                streamWriter.Write(oJsonObject);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)requestObj.GetResponse();

            int createdEventID = 0;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string ResponseText = streamReader.ReadToEnd().ToString();
                //Return is a JSON  "uri" : "https://api.teamworksapp.com/api/calendar/v1/calendars/events/170292097\"
                createdEventID = Convert.ToInt32((ResponseText.Split('/').Last()).Split('"').First());
            };

            return createdEventID;
        }

        public static void RemoveSession(int sessionTeamworksID)
        {
            try
            {
                var eSettingHost = Setting.GetByKey("TeamworksBaseURL");
                string strURLTWBaseURL = String.Format(eSettingHost.SettingValue + "calendar/v1/calendars/events/" + sessionTeamworksID);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                WebRequest requestObj = System.Net.WebRequest.Create(strURLTWBaseURL);
                requestObj.Method = "DELETE";
                requestObj.ContentType = "application/json";
                requestObj.Headers["Authorization"] = "Bearer " + Setting.GetByKey("TeamworksBearerToken").SettingValue;

                var httpResponse = (HttpWebResponse)requestObj.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string ResponseText = streamReader.ReadToEnd().ToString();
                    //Return is a JSON  "uri" : "https://api.teamworksapp.com/api/calendar/v1/calendars/events/170292097"
                };
            }
            catch(Exception ex)
            {
                //Do nothing - it means the event wasn't found in Teamworks
            }
        }


        public static int UpdateSession(int sessionTeamworksID, int attendeeTeamworksID, string sessionName, DateTime startDate, DateTime endDate, bool recurs)
        {
            var eSettingHost = Setting.GetByKey("TeamworksBaseURL");
            string strURLTWBaseURL = String.Format(eSettingHost.SettingValue + "calendar/v1/calendars/events/" + sessionTeamworksID);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

            WebRequest requestObj = System.Net.WebRequest.Create(strURLTWBaseURL);
            requestObj.Method = "PUT";
            requestObj.ContentType = "application/json";
            requestObj.Headers["Authorization"] = "Bearer " + Setting.GetByKey("TeamworksBearerToken").SettingValue;

            
            List<Attendee> attendeeList = new List<Attendee>
            {
                new Attendee { id = attendeeTeamworksID, type= "individual"}
            };

            JObject oJsonObject = new JObject();
            oJsonObject.Add("allDay", false);
            oJsonObject.Add("appointmentType", "Academic Tutoring");
            oJsonObject.Add("attendees", JToken.FromObject(attendeeList));
            oJsonObject.Add("attending", "attending");
            oJsonObject.Add("end", endDate.ToString());
            oJsonObject.Add("label", "Test Tutoring Session");
            oJsonObject.Add("recurs", recurs);
            oJsonObject.Add("start", startDate.ToString());
            oJsonObject.Add("timezone", "America/New_York");

            using (var streamWriter = new StreamWriter(requestObj.GetRequestStream()))
            {
                streamWriter.Write(oJsonObject);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)requestObj.GetResponse();

            int createdEventID = 0;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string ResponseText = streamReader.ReadToEnd().ToString();
                //Return is a JSON  "uri" : "https://api.teamworksapp.com/api/calendar/v1/calendars/events/170292097"
                createdEventID = Convert.ToInt32(ResponseText.Split('/').Last().Split('"').First());
            };

            return createdEventID;
        }

        class Attendee
        {
            public int id { get; set; }
            public string type { get; set; }
        }
    }
}