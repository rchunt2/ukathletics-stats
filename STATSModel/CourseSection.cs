﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UK.STATS.STATSModel;

namespace UK.STATS.STATSModel
{
    partial class CourseSection
    {
        public Boolean CanBeDeleted
        {
            get
            {
                int references = this.ResourceRegistrations.Count;
                references += this.Sessions.Count;
                return (references == 0);
            }
        }

        public String CourseDisplayName
        {
            get
            {
                if (!this.CourseReference.IsLoaded) { this.CourseReference.Load(); }
                var eCourse = this.Course;
                if (!eCourse.SubjectReference.IsLoaded) { eCourse.SubjectReference.Load(); }
                var eSubject = eCourse.Subject;
                String displayName = eSubject.SubjectNameShort + eCourse.CourseNumber;
                return displayName;
            }
        }

        public static Boolean Exists(Guid CourseID, String CourseSectionNumber)
        {
            var context = new STATSEntities();
            var result = (from CourseSection obj in context.CourseSections where obj.CourseSectionNumber == CourseSectionNumber && obj.CourseID == CourseID select obj).Count();
            return result > 0;
        }

        public static Boolean Exists(Guid CourseID, String CourseSectionNumber, TimeSpan TimeStart, TimeSpan TimeEnd, String CourseSectionDaysMeet)
        {
            var context = new STATSEntities();
            var result = (from CourseSection obj in context.CourseSections 
                          where 
                            obj.CourseSectionNumber == CourseSectionNumber && 
                            obj.CourseID == CourseID &&
                            obj.CourseSectionTimeStart == TimeStart &&
                            obj.CourseSectionTimeEnd == TimeEnd && 
                            obj.CourseSectionDaysMeet == CourseSectionDaysMeet
                          select obj).Count();
            return result > 0;
        }

        public static CourseSection Get(Guid CourseID, String CourseSectionNumber)
        {
            var context = new STATSEntities();
            var result = (from CourseSection obj in context.CourseSections where obj.CourseSectionNumber == CourseSectionNumber && obj.CourseID == CourseID select obj).First();
            return result;
        }

        public static CourseSection Get(Guid CourseSectionID)
        {
            var context = new STATSEntities();
            return (from CourseSection obj in context.CourseSections where obj.CourseSectionID == CourseSectionID select obj).First();
        }

        public static CourseSection[] Get()
        {
            var context = new STATSEntities();
            var eCourseSections = (from CourseSection obj in context.CourseSections orderby obj.Course.Subject.SubjectNameShort ascending, obj.Course.CourseNumber ascending, obj.CourseSectionNumber ascending select obj).ToArray();
            return eCourseSections;
        }
    }
}