﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Group
    {
        public Boolean CanBeDeleted
        {
            get
            {
                int iGroupMembers = this.GroupMembers.Count;
                int iMessages = this.Messages.Count;
                int iTotal = iGroupMembers + iMessages;
                return (iTotal == 0);
            }
        }

        public static Group[] Search(String SearchQuery)
        {
            var context = new STATSEntities();
            return (from Group obj in context.Groups where obj.GroupName.Contains(SearchQuery) orderby obj.GroupName ascending select obj).ToArray();
        }

        public Resource[] GetMembers()
        {
            // TODO: Clean this up a little?  Maybe make it a property?
            List<Resource> eResources = new List<Resource>();
            GroupMember[] eGroupMembers = this.GroupMembers.ToArray();
            foreach (var eGroupMember in eGroupMembers)
            {
                if(eGroupMember.Resource.ResourceIsActive.Value)
                    eResources.Add(eGroupMember.Resource);
            }
            return eResources.ToArray();
        }

        public void AddMember(Resource eResource)
        {
            var context = new STATSEntities();

            Boolean isDuplicate = (from Resource obj in GetMembers() where obj.ResourceID == eResource.ResourceID select obj).Count() > 0;
            if (!isDuplicate)
            {
                var eGroupMember = new GroupMember();
                eGroupMember.GroupMemberID = Guid.NewGuid();
                eGroupMember.GroupID = this.GroupID;
                eGroupMember.ResourceID = eResource.ResourceID;

                context.GroupMembers.AddObject(eGroupMember);
                context.SaveChanges();
            }
        }

        public void RemoveMember(Resource eResource)
        {
            var context = new STATSEntities();
            if(!eResource.GroupMembers.IsLoaded)
            {
                eResource.GroupMembers.Load();
            }

            var eGroupMembers = eResource.GroupMembers;
            foreach (var eGroupMember in eGroupMembers)
            {
                if(eGroupMember.GroupID == this.GroupID)
                {
                    
                    var eGroupMemberToDelete = (from GroupMember obj in context.GroupMembers where obj.GroupMemberID == eGroupMember.GroupMemberID select obj).First();
                    context.GroupMembers.DeleteObject(eGroupMemberToDelete);
                }
            }
            context.SaveChanges();
        }

        public static Group Get(Guid GroupID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Group obj in context.Groups where obj.GroupID == GroupID select obj).Single();
            return result;
        }

        public static Group Get(String GroupName)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Group obj in context.Groups where obj.GroupName == GroupName  select obj).First();
            return result;
        }

        public static Group[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Group obj in context.Groups orderby obj.GroupName ascending select obj).ToArray();
            return result;
        }

    }
}
