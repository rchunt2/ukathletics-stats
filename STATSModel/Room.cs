﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Room
    {
        

        public Boolean CanBeDeleted
        {
            get
            {
                int iSessionTutors = this.SessionTutors.Count;
                int iTotal = iSessionTutors;
                return (iTotal == 0);
            }
        }

        public static Room GetPersonalRoom(Guid LocationID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Room obj in context.Rooms where obj.RoomName == "---" select obj).First();
            return result;
        }

        public static Room[] GetAvailableRooms(STATSModel.STATSEntities context, int MinimumCapacity, bool CheckPersonalRooms, Guid LocationID)
        {
            
            // NOTE: Check this for correctness.
            var eRooms = new List<STATSModel.Room>();
            // We want to pull from rooms that ARE used for tutors with personally designated rooms
            if (CheckPersonalRooms)
            {
                eRooms = (from STATSModel.Room obj in context.Rooms orderby obj.RoomPriority ascending orderby obj.RoomName ascending where obj.RoomPriority > 0 && obj.RoomName == "N/A" && obj.RoomEnabled == true && obj.LocationID == LocationID select obj).ToList();
            }
            // We want to pull from rooms that ARE NOT used for tutors with personally designated rooms
            else
            {
                eRooms = (from STATSModel.Room obj in context.Rooms orderby obj.RoomPriority ascending orderby obj.RoomName ascending where obj.RoomPriority > 0 && obj.RoomName != "N/A" && obj.RoomCapacity >= MinimumCapacity && obj.RoomEnabled == true && obj.LocationID == LocationID select obj).ToList();
            }
            var eSessions = (from STATSModel.Attendance obj in context.Attendances.Include("Schedule").Include("Session") where obj.AttendanceTimeOut == null select obj.Schedule.Session).ToList();
            var ActiveSessions = new List<Guid>();

            foreach (var eSession in eSessions)
            {
                if (!ActiveSessions.Contains(eSession.SessionID))
                {
                    ActiveSessions.Add(eSession.SessionID);
                }
            }

            var eSessionTutors = (from STATSModel.SessionTutor obj in context.SessionTutors where ActiveSessions.Contains(obj.SessionID) select obj).ToList();

            var RoomsInUse = new List<Guid>();
            foreach (var eSessionTutor in eSessionTutors)
            {
                if (eSessionTutor.RoomID.HasValue)
                {
                    if (!RoomsInUse.Contains(eSessionTutor.RoomID.Value))
                    {
                        RoomsInUse.Add(eSessionTutor.RoomID.Value);
                    }
                }
            }

            var eRoomsNotInUse = new List<Room>();
            foreach (var eRoom in eRooms)
            {
                if (!RoomsInUse.Contains(eRoom.RoomID))
                {
                    eRoomsNotInUse.Add(eRoom);
                }
            }

            return eRoomsNotInUse.OrderBy(x=>x.RoomPriority).ThenBy(x=>x.RoomName).ToArray();
        }

        public static bool RoomStillAvailable(STATSModel.STATSEntities context, int MinimumCapacity, bool CheckPersonalRooms, Guid LocationID, Guid RoomID)
        {

            // NOTE: Check this for correctness.
            var eRooms = new List<STATSModel.Room>();
            // We want to pull from rooms that ARE used for tutors with personally designated rooms
            if (CheckPersonalRooms)
            {
                eRooms = (from STATSModel.Room obj in context.Rooms orderby obj.RoomPriority ascending orderby obj.RoomName ascending where obj.RoomPriority > 0 && obj.RoomName == "N/A" && obj.RoomEnabled == true && obj.LocationID == LocationID select obj).ToList();
            }
            // We want to pull from rooms that ARE NOT used for tutors with personally designated rooms
            else
            {
                eRooms = (from STATSModel.Room obj in context.Rooms orderby obj.RoomPriority ascending orderby obj.RoomName ascending where obj.RoomPriority > 0 && obj.RoomName != "N/A" && obj.RoomCapacity >= MinimumCapacity && obj.RoomEnabled == true && obj.LocationID == LocationID select obj).ToList();
            }
            var eSessions = (from STATSModel.Attendance obj in context.Attendances.Include("Schedule").Include("Session") where obj.AttendanceTimeOut == null select obj.Schedule.Session).ToList();
            var ActiveSessions = new List<Guid>();

            foreach (var eSession in eSessions)
            {
                if (!ActiveSessions.Contains(eSession.SessionID))
                {
                    ActiveSessions.Add(eSession.SessionID);
                }
            }

            var eSessionTutors = (from STATSModel.SessionTutor obj in context.SessionTutors where ActiveSessions.Contains(obj.SessionID) select obj).ToList();

            var RoomsInUse = new List<Guid>();
            foreach (var eSessionTutor in eSessionTutors)
            {
                if (eSessionTutor.RoomID.HasValue)
                {
                    if (!RoomsInUse.Contains(eSessionTutor.RoomID.Value))
                    {
                        RoomsInUse.Add(eSessionTutor.RoomID.Value);
                    }
                }
            }

            if(!RoomsInUse.Contains(RoomID))
            {
                return true;
            }
            return false;

        }


        public static Room GetAvailableRoom(STATSModel.STATSEntities context, int MinimumCapacity, bool CheckPersonalRooms, Guid LocationID)
        {
            Room[] eRooms = GetAvailableRooms(context, MinimumCapacity, CheckPersonalRooms, LocationID);

            if (eRooms.Count() > 0)
            {
                //Do a duplicate check here to make sure that the room wasn't just assigned
                foreach (Room room in eRooms)
                {
                    if (RoomStillAvailable(context, MinimumCapacity, CheckPersonalRooms, LocationID, room.RoomID))
                    {
                        return room;
                    }
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        public static Room Get(STATSModel.STATSEntities context, Guid RoomID)
        {
            var result = (from STATSModel.Room obj in context.Rooms where obj.RoomID == RoomID select obj).First();
            return result;
        }


        public static Room Get(Guid RoomID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Room obj in context.Rooms where obj.RoomID == RoomID select obj).First();
            return result;
        }

        public static Room[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Room obj in context.Rooms select obj).ToArray();
            return result;
        }

        
    }
}
