﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class MessageType
    {
        public static MessageType Get(Guid MessageTypeID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageType obj in context.MessageTypes where obj.MessageTypeID == MessageTypeID select obj).First();
            return result;
        }

        public static MessageType[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.MessageType obj in context.MessageTypes select obj).ToArray();
            return result;
        }

        public static MessageType Get(String MessageTypeName)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.MessageType obj in context.MessageTypes where obj.MessageTypeName == MessageTypeName select obj).First();
        }
    }
}
