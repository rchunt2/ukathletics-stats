﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class ResourcePicture
    {
        /// <summary>
        /// Determines if a resource has a picture already uploaded or not
        /// </summary>
        /// <param name="resourceID">The resource ID to check if a picture exists for</param>
        public static bool HasPicture(Guid resourceID)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                bool hasPicture = (from STATSModel.ResourcePicture pic in context.ResourcePictures
                                   where pic.ResourceID == resourceID
                                   select pic).Any();
                return hasPicture;
            }
        }
        /// <summary>
        /// Gets a picture for a resource
        /// </summary>
        /// <param name="resourceID">The resource ID to get a picture for (if any)</param>
        public static byte[] GetPicture(Guid resourceID)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                byte[] picture = (from STATSModel.ResourcePicture pic in context.ResourcePictures
                                   where pic.ResourceID == resourceID
                                   select pic.ResourcePictureBytes).FirstOrDefault();
                return picture;
            }
        }

        /// <summary>
        /// Deletes any picture that exists for a resource ID
        /// </summary>
        /// <param name="resourceID">The resource to delete the picture for</param>
        public static void DeletePicture(Guid resourceID)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                var pictures = (from STATSModel.ResourcePicture pic in context.ResourcePictures
                                where pic.ResourceID == resourceID
                                select pic);
                foreach (var picture in pictures)
                {
                    context.ResourcePictures.DeleteObject(picture);
                }

                context.SaveChanges();
            }
        }


        /// <summary>
        /// Saves the picture for a resource ID
        /// </summary>
        /// <param name="resourceID">The resource to save a picture for</param>
        public static void SavePicture(Guid resourceID, byte[] pictureBytes)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                var picture = (from STATSModel.ResourcePicture pic in context.ResourcePictures
                                where pic.ResourceID == resourceID
                                select pic).FirstOrDefault();

                if (picture == null)
                {
                    picture = new ResourcePicture();
                    picture.ResourceID = resourceID;
                    context.ResourcePictures.AddObject(picture);
                }
                picture.ResourcePictureBytes = pictureBytes;

                context.SaveChanges();
            }
        }

        public static bool IsValidPicture(byte[] pictureBytes)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(pictureBytes))
                {
                    //Try to get an image from the stream, then immediately dispose of it
                    Image.FromStream(stream).Dispose();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
