﻿using System;
using System.Linq;

namespace UK.STATS.STATSModel
{
    /// <summary>
    /// Annotations are used for storing notes regarding other objects.  The Annotation table has no explicit relationships, and as such can refer to any other object in the database.
    /// </summary>
    public partial class Annotation
    {
        /// <summary>
        /// Gets a collection containing all Annotations.
        /// </summary>
        /// <returns>A collection containing all Annotations.</returns>
        public static Annotation[] Get()
        {
            var context = new STATSEntities();
            return (from Annotation obj in context.Annotations select obj).ToArray();
        }

        /// <summary>
        /// Gets a collection of Annotation objects with the provided GUID matching the source object's GUID.
        /// </summary>
        /// <param name="ObjectID">The GUID of the originating source object.</param>
        /// <returns>A collection of Annotation objects.</returns>
        public static Annotation[] Get(Guid ObjectID)
        {
            var context = new STATSEntities();
            return (from Annotation obj in context.Annotations where obj.SourceID == ObjectID select obj).ToArray();
        }

        /// <summary>
        /// Delete all annotations for a resource
        /// </summary>
        public static void Delete(Resource resource)
        {
            using (var context = new STATSEntities())
            {
                Guid[] scheduleIds = (from Schedule sched in context.Schedules where sched.ResourceID == resource.ResourceID select sched.ScheduleID).ToArray();
                var annotations = (from Annotation obj in context.Annotations where scheduleIds.Contains(obj.SourceID) select obj);
                foreach (var annotation in annotations)
                {
                    context.Annotations.DeleteObject(annotation);
                }
                context.SaveChanges();
            }
        }

    }
}
