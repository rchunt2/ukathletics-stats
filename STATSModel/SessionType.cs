﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    partial class SessionType
    {
        public Boolean CanLogin
        {
           get
           {
               DateTime today = DateTime.Today;
               DayOfWeek dow = today.DayOfWeek;
               OperationRange or = OperationRange.Get(this, dow);
               DateTime todayStart = today.Add(or.OperationRangeTimeStart);
               DateTime todayEnd = today.Add(or.OperationRangeTimeEnd);
               if (DateTime.Now >= todayStart && DateTime.Now <= todayEnd)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
        }

        public static SessionType GetByName(String Name)
        {
            var context = new STATSEntities();
            var eSessionType = (from STATSModel.SessionType obj in context.SessionTypes where obj.SessionTypeName == Name select obj).First();
            return eSessionType;
        }

        public static SessionType Get(Guid SessionTypeID)
        {
            var context = new STATSEntities();
            var eSessionTypes = (from STATSModel.SessionType obj in context.SessionTypes where obj.SessionTypeID == SessionTypeID select obj).First();
            return eSessionTypes;
        }

        public static SessionType[] Get()
        {
            var context = new STATSEntities();
            var eSessionTypes = (from STATSModel.SessionType obj in context.SessionTypes select obj).ToArray();
            return eSessionTypes;
        }
    }
}
