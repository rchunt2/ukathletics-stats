﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UK.STATS.STATSModel
{
    public partial class Attendance
    {
        // Automatically performs a logout action on an individual that has been in a session long enough to be logged out.
        public static void AutoLogout(ResourceType resourceTypeTutor, ResourceType resourceTypeStudent)
        {
            var eAttendances = GetActive();
            var context = new STATSEntities();
            Dictionary<Guid, OperationRange> operationRangeLookup = new Dictionary<Guid, OperationRange>();

            foreach (var eAttendance in eAttendances)
            {
                // Automatically terminate TUTORING sessions that have gone past their end time.
                SessionType CurrentSessionType = eAttendance.Schedule.Session.SessionType;
                if (DateTime.Now > eAttendance.Schedule.Session.SessionDateTimeEnd && CurrentSessionType.SessionTypeName.StartsWith("Tutoring"))
                {
                    eAttendance.LogOut(resourceTypeTutor, resourceTypeStudent);
                }

                OperationRange opDate = null;

                // Automatically terminate any session that is still in progress outside the Operating Ranges.
                if (operationRangeLookup.ContainsKey(CurrentSessionType.SessionTypeID))
                {
                    opDate = operationRangeLookup[CurrentSessionType.SessionTypeID];
                }
                else
                {
                    //not cached in dictionary yet, get it and add it to dictionary cache.
                    opDate = OperationRange.Get(CurrentSessionType, DateTime.Now.DayOfWeek);
                    operationRangeLookup.Add(CurrentSessionType.SessionTypeID, opDate);
                }

                if (DateTime.Now < DateTime.Today.Add(opDate.OperationRangeTimeStart) || DateTime.Now > DateTime.Today.Add(opDate.OperationRangeTimeEnd) && CurrentSessionType.SessionTypeName != "Computer Lab")
                {
                    var eAttendanceContext = (from Attendance obj in context.Attendances where obj.AttendanceID == eAttendance.AttendanceID select obj).Single();
                    eAttendanceContext.AttendanceTimeOut = eAttendance.AttendanceTimeIn.Date.Add(opDate.OperationRangeTimeEnd);
                }

                if (CurrentSessionType.SessionTypeName.StartsWith("Tutoring"))
                {
                    // Automatically terminate any TUTORING session for unscheduled/unpaired resources after the LateLoginTime.
                    Schedule eSchedule = eAttendance.Schedule;
                    Session eSession = eSchedule.Session;

                    Setting LoginLateMinutes = Setting.GetByKey("LoginLateMinutes");
                    String strLoginLateMinutes = LoginLateMinutes.SettingValue;
                    int iLoginLateMinutes = Int32.Parse(strLoginLateMinutes);

                    if (DateTime.Now > eSession.SessionDateTimeStart.AddMinutes(iLoginLateMinutes))
                    {
                        Resource[] eResourcesInSession = eAttendance.Schedule.Session.GetResourceInSession();
                        int iResourcesInSession = eResourcesInSession.Length;
                        if (iResourcesInSession < 2)
                        {
                            if (iResourcesInSession == 1)
                            {
                                Resource eResource = eResourcesInSession.First();
                                if (eResource.IsPresent(eSession))
                                {
                                    eAttendance.LogOut(resourceTypeTutor, resourceTypeStudent);
                                }
                            }
                            else
                            {
                                var someoneMissing = eAttendance.Schedule.Session.HasResourceMissing;
                                if (someoneMissing)
                                {
                                    eAttendance.LogOut(resourceTypeTutor, resourceTypeStudent);
                                }
                            }
                        }
                        else if (iResourcesInSession == 2)
                        {
                            var someoneMissing = eAttendance.Schedule.Session.HasResourceMissing;
                            if (someoneMissing)
                            {
                                eAttendance.LogOut(resourceTypeTutor, resourceTypeStudent);
                            }
                        }
                        // The code block below deals with the scenario where a tutor is logged in past the logout time but
                        // none of the students have showed up yet.  If there are no students logged into a group session
                        // past the logout time then end the session. ASW 12/3/12
                        else
                        {
                            var studentPresent = false;
                            var tutorPresent = false;
                            foreach (var curResource in eResourcesInSession)
                            {
                                // If you are NOT a Tutor and are currently logged into tutoring we want to keep the session alive
                                if (curResource.ResourceType.ResourceTypeName != "Tutor" && (curResource.IsLoggedInElsewhere.Value == true && curResource.IsLoggedInElsewhere.Key.StartsWith("Tutoring")))
                                {
                                    studentPresent = true;
                                }
                                if (curResource.ResourceType.ResourceTypeName == "Tutor" && (curResource.IsLoggedInElsewhere.Value == true && curResource.IsLoggedInElsewhere.Key.StartsWith("Tutoring")))
                                {
                                    tutorPresent = true;
                                }
                            }
                            // If no students OR no tutor is present then end the group session
                            if (!studentPresent || !tutorPresent)
                                eAttendance.LogOut(resourceTypeTutor, resourceTypeStudent);
                        }
                    }
                }
            }
            context.SaveChanges();
        }

        public static Attendance Get(Guid AttendanceID)
        {
            var context = new STATSEntities();
            return (from Attendance obj in context.Attendances where obj.AttendanceID == AttendanceID select obj).First();
        }

        public Boolean IsLoggedIn
        {
            // If you have NOT logged out, then you ARE logged in.
            // Simply put, negate whether or not a value exists for your time out.
            // I love nullable types.  <3
            get { return !AttendanceTimeOut.HasValue; }
        }

        /// <summary>
        /// Ends the current in-progress session.  To log a Resource in, call the Login method from a Resource object.
        /// </summary>
        /// <returns>(WARNING: ALWAYS RETURNS TRUE, REGARDLESS OF SUCCESS) true if the logout was successful; otherwise, <c>false</c>.</returns>
        public Boolean LogOut(ResourceType resourceTypeTutor, ResourceType resourceTypeStudent)
        {
            if (IsLoggedIn)
            {
                var context = new STATSEntities();
                var eAttendance = (from Attendance obj in context.Attendances.Include("Schedule.Session") where obj.AttendanceID == AttendanceID select obj).First();
                var eSchedule = eAttendance.Schedule;
                var eSession = eSchedule.Session;
                Boolean OutOfRange = (DateTime.Now > eSession.SessionDateTimeEnd);
                DateTime dtLogout;
                if (eSession.SessionType.SessionTypeName != "Computer Lab")
                {
                    dtLogout = OutOfRange ? eSession.SessionDateTimeEnd : DateTime.Now;
                }
                else dtLogout = DateTime.Now;
                DateTime? studentOldestTimeIn = null;
                DateTime? studentNewestTimeOut = null;
                Boolean isTutor = (eSchedule.Resource.ResourceTypeID == resourceTypeTutor.ResourceTypeID);
                Resource[] eResources = null;

                if (!eSession.SessionIsScheduled)
                {
                    Boolean bOthersPresent = false;
                    if (eResources == null) { eResources = eSession.GetResourceInSession(); } //Get the eresources if not already retrieved
                    foreach (var eResource in eResources)
                    {
                        if (eResource.IsPresent(eSession) && eResource.ResourceID != this.Schedule.ResourceID)
                        {
                            bOthersPresent = true;
                        }
                    }

                    if (!bOthersPresent)
                    {
                        eSession.SessionDateTimeEnd = dtLogout;
                    }
                }

                //if we are looking at logging out the tutor
                if (isTutor)
                {
                    //If this is the tutor's session we are logging out, we need to get all of the students sessions to determine proper in/out time to use
                    //Get all Students sessions, loop through, get the oldest TimeIn, use that if it is newer than the SessionDateTimeStart
                    //Also, get the Newest TimeOut, use that if it is older than SessionDateTimeEnd
                    if (eResources == null) { eResources = eSession.GetResourceInSession(); } //Get the eresources if not already retrieved
                    foreach (Resource resource in eResources)
                    {
                        if (resource.ResourceTypeID == resourceTypeStudent.ResourceTypeID)
                        {
                            Attendance[] studentAttendances = Attendance.Get(resource, eAttendance.Schedule.Session);
                            foreach (Attendance studentAttendance in studentAttendances)
                            {
                                if (!studentOldestTimeIn.HasValue || studentAttendance.AttendanceTimeIn < studentOldestTimeIn)
                                {
                                    studentOldestTimeIn = studentAttendance.AttendanceTimeIn;
                                }
                                if (!studentNewestTimeOut.HasValue || (studentAttendance.AttendanceTimeOut.HasValue && studentAttendance.AttendanceTimeOut < studentNewestTimeOut))
                                {
                                    studentNewestTimeOut = studentAttendance.AttendanceTimeOut;
                                }
                            }
                        }
                    }
                }

                // The code used to check that the resource was a Student.
                // I was informed that this is incorrect logic.
                // It should perform the check on both Tutors AND Students.
                //if (eSession.SessionTypeID == eSessionTypeTutoring.SessionTypeID && eSchedule.Resource.ResourceTypeID == eResourceTypeStudent.ResourceTypeID)
                var eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
                var eSessionTypeTutoringFootball = SessionType.GetByName("Tutoring - Football");
                if (eSession.SessionTypeID == eSessionTypeTutoring.SessionTypeID || eSession.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID)
                {
                    Boolean HasTutor = eAttendance.Schedule.Session.HasTutor(resourceTypeTutor);
                    if (HasTutor)
                    {
                        Resource eResourceTutor = eAttendance.Schedule.Session.GetTutor(resourceTypeTutor);

                        DateTime scheduledSessionStart = eAttendance.Schedule.Session.SessionDateTimeStart;
                        DateTime scheduledSessionEnd = eAttendance.Schedule.Session.SessionDateTimeEnd;
                        double minutesCounted = 0;

                        var eAttendancesTutor = Attendance.Get(eResourceTutor, eAttendance.Schedule.Session);

                        foreach (var eAttendanceTutor in eAttendancesTutor)
                        {
                            DateTime StartLatest = eAttendance.AttendanceTimeIn;
                            DateTime EndLatest = DateTime.Now;

                            //Check the tutor's time in against the student's, if tutor arrived after student, use tutors time in
                            if (eAttendanceTutor.AttendanceTimeIn > StartLatest)
                            {
                                StartLatest = eAttendanceTutor.AttendanceTimeIn;
                            }

                            //Check the tutor's time out against the student's, if tutor left before student, use tutors time out
                            if (eAttendanceTutor.AttendanceTimeOut.HasValue && eAttendanceTutor.AttendanceTimeOut.Value < EndLatest)
                            {
                                EndLatest = eAttendanceTutor.AttendanceTimeOut.Value;
                            }

                            //The following two lines ensure that no time is counted outside the scheduled time slot.
                            if (StartLatest < scheduledSessionStart) { StartLatest = scheduledSessionStart; }
                            if (EndLatest > scheduledSessionEnd) { EndLatest = scheduledSessionEnd; }

                            if (isTutor)
                            {
                                //use the oldest student Time In if it is newer than the start time
                                if (studentOldestTimeIn.HasValue && studentOldestTimeIn.Value > StartLatest)
                                {
                                    StartLatest = studentOldestTimeIn.Value;
                                }

                                //Use the newest student Time Out, if it is older than the end time
                                if (studentNewestTimeOut.HasValue && studentNewestTimeOut.Value < EndLatest)
                                {
                                    EndLatest = studentNewestTimeOut.Value;
                                }
                            }

                            TimeSpan duration = EndLatest - StartLatest;
                            minutesCounted += duration.TotalMinutes;

                            //Fix a problem if they login before scheduled start and then logout before scheduled start, gives a negative #, they should not be credited for this time
                            if (minutesCounted < 0)
                            {
                                minutesCounted = 0;
                            }
                        }

                        //If we are logging out the tutor, and there was no student logged in, they should get 0 minutes counted for the session.
                        if (isTutor && !studentOldestTimeIn.HasValue && !studentNewestTimeOut.HasValue)
                        {
                            minutesCounted = 0;
                        }

                        eAttendance.AttendanceTimeOverride = (int)Math.Ceiling(minutesCounted);
                    }
                    else
                    {
                        eAttendance.AttendanceTimeOverride = 0;
                    }
                }

                //4/28/15 - RCH (Apax) - If at least 10 minutes have not been logged in Study Hall, don't award any credit
                //3/02/17 - RCH (Apax) - Updated to 9 minutes per client's request
                if(eAttendance.AttendanceTimeOverride == null 
                    && (dtLogout - eAttendance.AttendanceTimeIn).Duration().TotalMinutes < 9
                    && eSession.SessionType.SessionTypeName.StartsWith("Study Hall") ) {
                    eAttendance.AttendanceTimeOverride = 0;
                }

                eAttendance.AttendanceTimeOut = dtLogout;
                context.SaveChanges();
                return true;
            }
            return false;

            //throw new Exception("An attempt was made to finalize an Attendance record that was already closed.");
        }

        public static void LogoutExpiredSessions()
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances.Include("Schedule").Include("Schedule.Session").Include("Schedule.Resource").Include("Schedule.Resource.ResourceType").Include("Schedule.Session.SessionType") where obj.AttendanceTimeOut == null select obj).ToList();

            foreach (var eAttendance in eAttendances)
            {
                if (DateTime.Now > eAttendance.Schedule.Session.SessionDateTimeEnd)
                {
                    eAttendance.AttendanceTimeOut = eAttendance.Schedule.Session.SessionDateTimeEnd;
                }
            }
            context.SaveChanges();
        }

        public static Boolean HasData(Resource eResource, Session eSession)
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances.Include("Schedule").Include("Schedule.Session") where obj.Schedule.SessionID == eSession.SessionID && obj.Schedule.ResourceID == eResource.ResourceID select obj).ToList();
            var iAttendances = eAttendances.Count;
            return (iAttendances >= 1);
        }

        public static Attendance[] Get(Resource eResource, Session eSession)
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances.Include("Schedule").Include("Schedule.Session") where obj.Schedule.SessionID == eSession.SessionID && obj.Schedule.ResourceID == eResource.ResourceID orderby obj.AttendanceTimeIn descending select obj).ToArray();
            return eAttendances;
        }

        public static Attendance[] Get()
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances select obj).ToArray();
            return eAttendances;
        }

        public static Attendance[] Get(Resource eResource)
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances where obj.Schedule.ResourceID == eResource.ResourceID orderby obj.AttendanceTimeIn descending select obj).ToArray();
            return eAttendances;
        }

        public static Attendance[] Get(Resource eResource, DateTime RangeStart, DateTime RangeEnd)
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances where obj.Schedule.ResourceID == eResource.ResourceID && obj.AttendanceTimeIn < RangeStart && obj.AttendanceTimeOut > RangeEnd select obj).ToArray();
            return eAttendances;
        }

        public static Attendance[] GetActive()
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances
                                where obj.AttendanceTimeOut == null
                                orderby obj.Schedule.Resource.ResourceNameLast , obj.Schedule.Resource.ResourceNameFirst
                                select obj).ToArray();
            return eAttendances;
        }

        public static Attendance[] GetActive(Resource eResource)
        {
            var context = new STATSEntities();
            var eAttendances = (from Attendance obj in context.Attendances where obj.AttendanceTimeOut == null && obj.Schedule.ResourceID == eResource.ResourceID select obj).ToArray();
            return eAttendances;
        }

        public static void CalculateTime(Resource eResource, DateTime RangeStart, AttendanceTime attendanceTime)
        {
            DateTime RangeEnd = RangeStart.AddDays(7);

            var context = new STATSEntities();

            var eAttendances =
                (from Attendance obj in
                     context.Attendances.Include("Schedule").Include("Schedule.Session").Include("Schedule.Resource").Include("Schedule.Session.SessionType")
                 where obj.Schedule.Session.SessionDateTimeStart >= RangeStart && obj.Schedule.Session.SessionDateTimeStart <= RangeEnd && obj.AttendanceTimeOut != null && obj.Schedule.ResourceID == eResource.ResourceID
                 select obj).ToList();

            foreach (var eAttendance in eAttendances)
            {
                Boolean isOverridden = false;
                int DurationInSeconds = 0;

                if (eAttendance.AttendanceTimeOverride.HasValue)
                {
                    isOverridden = true;
                    DurationInSeconds = eAttendance.AttendanceTimeOverride.Value * 60;
                }
                else
                {
                    if (eAttendance.AttendanceTimeOut.HasValue)
                    {
                        TimeSpan diff = eAttendance.AttendanceTimeOut.Value - eAttendance.AttendanceTimeIn;
                        DurationInSeconds = (int)Math.Ceiling(diff.TotalSeconds);
                        if ((DurationInSeconds < 120 && !eAttendance.Schedule.Session.SessionType.SessionTypeName.Contains("Study Hall")))
                        {
                            //Ignore records of less than 2 minutes
                            DurationInSeconds = 0;
                        }
                    }
                }

                switch (eAttendance.Schedule.Session.SessionType.SessionTypeName)
                {
                    case "Tutoring - CATS":
                    case "Tutoring - Football":
                        switch (eAttendance.AttendanceTimeIn.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                attendanceTime.TutoringSunday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Monday:
                                attendanceTime.TutoringMonday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Tuesday:
                                attendanceTime.TutoringTuesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Wednesday:
                                attendanceTime.TutoringWednesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Thursday:
                                attendanceTime.TutoringThursday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Friday:
                                attendanceTime.TutoringFriday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Saturday:
                                attendanceTime.TutoringSaturday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                        }
                        break;

                    case "Study Hall - CATS":
                    case "Study Hall - Football":
                        switch (eAttendance.AttendanceTimeIn.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                attendanceTime.StudyHallSunday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Monday:
                                attendanceTime.StudyHallMonday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Tuesday:
                                attendanceTime.StudyHallTuesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Wednesday:
                                attendanceTime.StudyHallWednesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Thursday:
                                attendanceTime.StudyHallThursday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Friday:
                                attendanceTime.StudyHallFriday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                            case DayOfWeek.Saturday:
                                attendanceTime.StudyHallSaturday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                break;
                        }
                        break;

                    case "Computer Lab":
                        if (isOverridden)
                        {
                            switch(eAttendance.AttendanceTimeIn.DayOfWeek)
                            {
                                case DayOfWeek.Sunday:
                                    attendanceTime.CompLabSunday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Monday:
                                    attendanceTime.CompLabMonday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Tuesday:
                                    attendanceTime.CompLabTuesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Wednesday:
                                    attendanceTime.CompLabWednesday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Thursday:
                                    attendanceTime.CompLabThursday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Friday:
                                    attendanceTime.CompLabFriday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                                case DayOfWeek.Saturday:
                                    attendanceTime.CompLabSaturday += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                                    break;
                            }
                        }
                        break;
                }
            }

            var eFreeTimes = (from FreeTime obj in context.FreeTimes.Include("SessionType").Include("Resource") where obj.FreeTimeApplyDate >= RangeStart && obj.FreeTimeApplyDate <= RangeEnd && obj.ResourceID == eResource.ResourceID select obj).ToList();

            foreach (var eFreeTime in eFreeTimes)
            {
                switch (eFreeTime.SessionType.SessionTypeName)
                {
                    case "Tutoring - CATS":
                    case "Tutoring - Football":
                        switch (eFreeTime.FreeTimeApplyDate.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                attendanceTime.TutoringSunday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Monday:
                                attendanceTime.TutoringMonday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Tuesday:
                                attendanceTime.TutoringTuesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Wednesday:
                                attendanceTime.TutoringWednesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Thursday:
                                attendanceTime.TutoringThursday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Friday:
                                attendanceTime.TutoringFriday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Saturday:
                                attendanceTime.TutoringSaturday += eFreeTime.FreeTimeAmount;
                                break;
                        }
                        break;

                    case "Study Hall - CATS":
                    case "Study Hall - Football":
                        switch (eFreeTime.FreeTimeApplyDate.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                attendanceTime.StudyHallSunday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Monday:
                                attendanceTime.StudyHallMonday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Tuesday:
                                attendanceTime.StudyHallTuesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Wednesday:
                                attendanceTime.StudyHallWednesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Thursday:
                                attendanceTime.StudyHallThursday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Friday:
                                attendanceTime.StudyHallFriday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Saturday:
                                attendanceTime.StudyHallSaturday += eFreeTime.FreeTimeAmount;
                                break;
                        }
                        break;

                    case "Computer Lab":
                        switch (eFreeTime.FreeTimeApplyDate.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                attendanceTime.CompLabSunday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Monday:
                                attendanceTime.CompLabMonday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Tuesday:
                                attendanceTime.CompLabTuesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Wednesday:
                                attendanceTime.CompLabWednesday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Thursday:
                                attendanceTime.CompLabThursday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Friday:
                                attendanceTime.CompLabFriday += eFreeTime.FreeTimeAmount;
                                break;
                            case DayOfWeek.Saturday:
                                attendanceTime.CompLabSaturday += eFreeTime.FreeTimeAmount;
                                break;
                        }
                        break;
                }
            }
        }
    }
}