﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;

namespace UK.STATS.STATSModel
{
    public partial class Resource
    {
        /// <summary>
        /// Returns a list of tutoring sessions that will occur in the future.
        /// </summary>
        public Session[] TutoringSessions
        {
            get
            {
                var eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
                var eSessionTypeTutoringFootball = SessionType.GetByName("Tutoring - Football");
                var context = new STATSEntities();

                var eSessions = (
                                 from Schedule obj in context.Schedules
                                                             .Include("Session")
                                                             .Include("Resource")
                                                             .Include("Session.SessionTutors")
                                                             .Include("Attendances")
                                 where
                                      ((obj.Session.SessionTypeID == eSessionTypeTutoring.SessionTypeID) || (obj.Session.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID)) &&
                                      obj.Attendances.Count == 0 && // Has NO Attendances
                                      obj.ResourceID == this.ResourceID &&  // Matching Resource
                                      obj.Resource.ResourceIsActive == true && // Has not been soft deleted
                                      obj.Session.SessionIsScheduled && // Is Scheduled
                                      !obj.Session.SessionIsCancelled && // Is NOT Cancelled
                                      obj.ScheduleRequireAttend &&  // Required Attendance
                                      obj.Session.SessionTutors.Count > 0  &&           // 1+ SessionTutor records
                                      //obj.Session.SessionTutors.Count > 0 && // 1+ SessionTutor records
                                      obj.Session.SessionDateTimeStart >= DateTime.Now// the session occurs in the future
                                 orderby obj.Session.SessionDateTimeStart ascending
                                 select obj.Session).ToList();

                return eSessions.ToArray();
            }
        }

        public Boolean CanBeDeleted
        {
            get
            {
                int iSchedules = this.Schedules.Count;
                int iGroups = this.GroupMembers.Count;
                int iMessagesFrom = this.MessagesFrom.Count;
                int iMessagesTo = this.MessagesTo.Count;
                int iCourseTutors = this.CourseTutors.Count;
                int iErrors = this.Errors.Count;
                int iFreeTimes = this.FreeTimes.Count;
                int iTotal = iSchedules + iGroups + iMessagesFrom + iMessagesTo + iCourseTutors + iErrors + iFreeTimes;
                return (iTotal == 0);
            }
        }

        public Boolean IsPresent(Session eSession)
        {
            var context = new STATSEntities();
            int eAttendancesCount = (from Attendance obj in context.Attendances
                                     where
                                          obj.AttendanceTimeOut == null &&
                                          obj.Schedule.ResourceID == this.ResourceID &&
                                          obj.Schedule.Session.SessionID == eSession.SessionID
                                     select obj).Count();
            //Changed to return the count from the linq query instead of entire objects using ToArray
            return eAttendancesCount > 0;
        }

        public static Boolean IsPresent(Resource eResource, Session eSession)
        {
            return eResource.IsPresent(eSession);
        }

        public String ResourceNameDisplayType
        {
            get
            {
                if (!this.ResourceTypeReference.IsLoaded)
                {
                    this.ResourceTypeReference.Load();
                }
                return this.ResourceNameDisplayLastFirst + " [" + this.ResourceType.ResourceTypeName + "]";
            }
        }

        public String ResourceNameDisplayLastFirst
        {
            get
            {
                return ResourceNameLast + ", " + ResourceNameFirst;
            }
        }

        /// <summary>
        /// Gets a phone number link for use in a href tags. Returns empty string if invalid phone number
        /// </summary>
        public String ResourcePhoneNumberLink
        {
            get
            {
                string plainNumber = ResourcePhoneNumberPlain;
                if (string.IsNullOrWhiteSpace(plainNumber))
                {
                    return string.Empty;
                }
                return "tel:+1" + plainNumber;
            }
        }

        /// <summary>
        /// Gets a formatted phone number (123) 123-1234 or returns an empty string if invalid phone number
        /// </summary>
        public String ResourcePhoneNumberFormatted
        {
            get
            {
                try
                {
                    string plainNumber = ResourcePhoneNumberPlain;
                    if (string.IsNullOrWhiteSpace(plainNumber))
                    {
                        return string.Empty;
                    }
                    return string.Format("{0:(###) ###-####}", long.Parse(plainNumber));
                }
                catch (Exception)
                {
                    return ResourcePhoneNumber;
                }
            }
        }

        /// <summary>
        /// Gets the phone number without any spaces, dashes, or country code. Returns an empty string if empty/null/whitespace.
        /// </summary>
        public string ResourcePhoneNumberPlain
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.ResourcePhoneNumber))
                {
                    return string.Empty;
                }
                string phoneNumber = ResourcePhoneNumber.Replace(" ", "").Replace("-", "").Replace("(", "").Replace(")", "");
                if (phoneNumber.Length > 10 && phoneNumber[0] == '1')
                {
                    //remove leading country code of 1
                    phoneNumber = phoneNumber.Substring(1);
                }
                return phoneNumber;
            }
        }

        /// <summary>
        /// Gets a mailto: email address link for use in a href tags.
        /// </summary>
        public String ResourceEmailLink
        {
            get
            {
                return "mailto:" + this.ResourceEmail;
            }
        }

        //public Boolean DoesTimeConflict(DateTime StartTime, DateTime EndTime)
        //{
        //    var context = new STATSEntities();
        //    var eSchedules = (from STATSModel.Schedule obj in context.Schedules.Include("Session") where obj.ResourceID == ResourceID select obj).ToList();

        //    Boolean bConflict = false;
        //    foreach (var eSchedule in eSchedules)
        //    {
        //        if(bConflict) { return true; } // Don't look any further if we've already found an time conflict.
        //        STATSModel.Session eSession = eSchedule.Session;

        //        // THE EXACT SAME TIME, EXACTLY - It's the same course.  How could I have forgotten this one?!
        //        if (eSession.SessionDateTimeStart == StartTime && eSession.SessionDateTimeEnd == StartTime)
        //        {
        //            bConflict = true;
        //        }

        //        // OVERLAP WITH RANGE START - Session starts before the range starts, but ends after the range has started. 
        //        if (eSession.SessionDateTimeStart <= StartTime && eSession.SessionDateTimeEnd >= StartTime)
        //        {
        //            bConflict = true;
        //        }

        //        // OVERLAP WITH RANGE END - Session starts before the range ends, but ends after the range has ended.
        //        if (eSession.SessionDateTimeStart <= EndTime && eSession.SessionDateTimeEnd >= EndTime)
        //        {
        //            bConflict = true;
        //        }

        //        // RANGE FULLY CONTAINS - Session starts after the range starts, but ends before the range has ended.
        //        if (eSession.SessionDateTimeStart >= StartTime && eSession.SessionDateTimeEnd <= EndTime)
        //        {
        //            bConflict = true;
        //        }
        //    }
        //    return bConflict;
        //}

        public Group[] Groups
        {
            get
            {
                var context = new STATSModel.STATSEntities();
                var eGroups = (from GroupMember obj in context.GroupMembers.Include("Group").Include("Resource") where obj.ResourceID == this.ResourceID && obj.Resource.ResourceIsActive == true select obj.Group).ToArray();
                return eGroups;
            }
        }

        public Course[] GetCoursesTutored()
        {
            List<Course> eCourses = new List<Course>();
            CourseTutor[] eCourseTutors = this.CourseTutors.ToArray();
            foreach (var eCourseTutor in eCourseTutors)
            {
                eCourses.Add(eCourseTutor.Course);
            }

            eCourses = (from Course obj in eCourses orderby obj.Subject.SubjectNameShort ascending, obj.CourseNumber ascending select obj).ToList();

            return eCourses.ToArray();
        }

        /// <summary>
        /// /Checks if a resource has an AD login associated with it or not
        /// </summary>
        /// <param name="resourceLogin">Legacy resource username</param>
        /// <returns></returns>
        public static bool DoesResourceHaveADLoginAssociated(string resourceLogin)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                var eResource = (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == resourceLogin && obj.ResourceIsActive == true select obj).FirstOrDefault();
                if(eResource != null && !string.IsNullOrWhiteSpace(eResource.ResourceADLogin))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if the given login is associated with any resource
        /// </summary>
        /// <param name="resourceADLogin"></param>
        /// <returns></returns>
        public static bool IsADLoginAssociatedWithResource(string resourceADLogin)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                var eResourcesCount = (from STATSModel.Resource obj in context.Resources where obj.ResourceADLogin == resourceADLogin && obj.ResourceIsActive == true select obj).Count();
                return (eResourcesCount > 0);
            }
        }

        /// <summary>
        /// Checks if a given login and password are valid Active Directory credentials
        /// </summary>
        /// <param name="resourceADLogin">The AD username</param>
        /// <param name="resourcePassword">The AD Password</param>
        /// <returns>True if valid AD account, false if not or if an error occurred.</returns>
        public static Boolean IsValidADLogin(string resourceADLogin, string resourcePassword)
        {
            string errorMessage;
            //This requires the app using this method has these "ADDomainName" and "ADLDAPPath" settings in the web.config or app.config
            if (!AuthenticateUserAgainstAD(System.Configuration.ConfigurationManager.AppSettings["ADDomainName"], resourceADLogin, resourcePassword, System.Configuration.ConfigurationManager.AppSettings["ADLDAPPath"], out errorMessage))
                return AuthenticateUserAgainstAD(System.Configuration.ConfigurationManager.AppSettings["ADDomainName2"], resourceADLogin, resourcePassword, System.Configuration.ConfigurationManager.AppSettings["ADLDAPPath2"], out errorMessage);

            return true;
        }

        /// <summary>
        /// Checks if the login is a valid Legacy login
        /// </summary>
        /// <param name="ResourceLogin"></param>
        /// <param name="ResourcePassword"></param>
        /// <returns></returns>
        public static Boolean IsValidLegacyLogin(string ResourceLogin, string ResourcePassword)
        {
            var context = new STATSModel.STATSEntities();
            var eResourcesCount = (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == ResourceLogin && obj.ResourcePassword == ResourcePassword && obj.ResourceIsActive == true select obj).Count();
            return eResourcesCount > 0;
        }

        /// <summary>
        /// Associates a AD login with a Resource
        /// </summary>
        /// <param name="legacyResourceLogin">Legacy username</param>
        /// <param name="resourceADLogin">New AD username to associate</param>
        /// <returns></returns>
        public static bool AssociateADLogin(string legacyResourceLogin, string resourceADLogin)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                Resource resource = (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == legacyResourceLogin && obj.ResourceIsActive == true select obj).First();
                if (resource != null)
                {
                    resource.ResourceADLogin = resourceADLogin;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Inactivates a resource
        /// </summary>
        /// <param name="ResourceID">Resource to inactivate</param>
        /// <returns></returns>
        public static bool InactivateResource(Guid ResourceID)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                Resource resource = (from STATSModel.Resource obj in context.Resources where obj.ResourceID == ResourceID && obj.ResourceIsActive == true select obj).First();
                if (resource != null)
                {
                    resource.ResourceIsActive = false;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Activates a resource
        /// </summary>
        /// <param name="ResourceID">Resource to activate</param>
        /// <returns></returns>
        public static bool ActivateResource(Guid ResourceID)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                Resource resource = (from STATSModel.Resource obj in context.Resources where obj.ResourceID == ResourceID select obj).First();
                if (resource != null)
                {
                    resource.ResourceIsActive = true;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static Boolean Exists(String ResourceLogin)
        {
            var context = new STATSModel.STATSEntities();
            int eResourcesCount = (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == ResourceLogin && obj.ResourceIsActive == true select obj).Count();
            return eResourcesCount > 0;
        }

        public static Resource[] Get(CourseSection eCourseSection)
        {
            var eResourcesAll = new List<Resource>();

            var context = new STATSModel.STATSEntities();

            var eSessions = eCourseSection.Sessions;
            foreach (var eSession in eSessions)
            {
                var eSchedules = eSession.Schedules;
                foreach (var eSchedule in eSchedules)
                {
                    var eResource = eSchedule.Resource;
                    eResourcesAll.Add(eResource);
                }
            }

            var eResources = (from Resource obj in eResourcesAll where obj.ResourceIsActive == true orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending select obj).Distinct().ToArray();

            //var eCourseSectionInclude = (from STATSModel.CourseSection obj in context.CourseSections.Include("Sessions") 
            //                             where obj.CourseSectionID == eCourseSection.CourseSectionID 
            //                             select obj).Single();

            //var eSessions = eCourseSection.Sessions;
            //var eSessionIDs = (from Session obj in eSessions select obj.SessionID).ToArray();
            //var  = (from Session obj in context.Sessions where eSessionIDs.Contains(obj.SessionID) select obj.Schedules.ToArray())..ToArray();
            //var eResources = (from Schedule obj in eSchedules select obj.Resource).ToArray();
            return eResources;
        }

        public static Resource[] Get()
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources where obj.ResourceIsActive == true orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending select obj).ToArray();
        }

        public static Resource[] Get(ResourceType eResourceType)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources where obj.ResourceTypeID == eResourceType.ResourceTypeID && obj.ResourceIsActive == true select obj).ToArray();
        }

        public static Resource Get(String ResourceLogin)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == ResourceLogin && obj.ResourceIsActive == true select obj).First();
        }


        public static Resource GetByADLogin(string resourceADLogin)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                return (from STATSModel.Resource obj in context.Resources where obj.ResourceADLogin == resourceADLogin && obj.ResourceIsActive == true select obj).First();
            }
        }

        public static Guid Get(String ResourceFirstName, String ResourceLastName)
        {
            using (var context = new STATSModel.STATSEntities())
            {
                return (from STATSModel.Resource obj in context.Resources
                        where obj.ResourceNameFirst == ResourceFirstName &&
                              obj.ResourceNameLast == ResourceLastName &&
                              obj.ResourceIsActive == true
                        select obj.ResourceID).FirstOrDefault();
            }
        }

        /// <summary>
        /// Counts the Resources that have the provided ResourceLogin.
        /// </summary>
        /// <param name="ResourceLogin">The Login name to count the instances of.</param>
        /// <returns>The number of resources that match the provided Login</returns>
        public static int CountResourceByLoginName(String ResourceLogin)
        {
            var context = new STATSModel.STATSEntities();
            int iCount = (from STATSModel.Resource obj in context.Resources where obj.ResourceLogin == ResourceLogin && obj.ResourceIsActive == true select obj).Count();
            return iCount;
        }

        /// <summary>
        /// Counts the Resources that have the provided ResourceADLogin.
        /// </summary>
        /// <param name="resourceADLogin">The AD Login name to count the instances of.</param>
        /// <returns>The number of resources that match the provided AD Login</returns>
        public static int CountResourceByADLoginName(String resourceADLogin)
        {
            var context = new STATSModel.STATSEntities();
            int iCount = (from STATSModel.Resource obj in context.Resources where obj.ResourceADLogin == resourceADLogin && obj.ResourceIsActive == true select obj).Count();
            return iCount;
        }

        public static Resource[] Search(String SearchQuery)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources
                    where ((obj.ResourceNameFirst + " " + obj.ResourceNameLast).Contains(SearchQuery) || obj.ResourceLogin.Contains(SearchQuery) || obj.ResourceEmail.Contains(SearchQuery) || obj.ResourcePhoneNumber.Contains(SearchQuery))
                    orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending
                    select obj).ToArray();
        }

        public static Resource[] Search(String SearchQuery, Group eGroup)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources.Include("GroupMembers").Include("GroupMembers.Group")
                    where obj.ResourceIsActive == true && ((obj.ResourceNameFirst + " " + obj.ResourceNameLast).Contains(SearchQuery) || obj.ResourceLogin.Contains(SearchQuery) || obj.ResourceEmail.Contains(SearchQuery) || obj.ResourcePhoneNumber.Contains(SearchQuery)) &&
                          (from GroupMember gmObj in obj.GroupMembers
                           where gmObj.GroupID == eGroup.GroupID
                           select gmObj.GroupMemberID).Count() > 0
                    orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending
                    select obj).ToArray();
        }

        public static Resource[] Search(String SearchQuery, ResourceType eResourceType)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources
                    where ((obj.ResourceNameFirst + " " + obj.ResourceNameLast).Contains(SearchQuery) || obj.ResourceLogin.Contains(SearchQuery) || obj.ResourceEmail.Contains(SearchQuery) || obj.ResourcePhoneNumber.Contains(SearchQuery)) && obj.ResourceTypeID == eResourceType.ResourceTypeID 
                    orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending
                    select obj).ToArray();
        }

        public static Resource[] Search(String SearchQuery, Group eGroup, ResourceType eResourceType)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources.Include("GroupMembers").Include("GroupMembers.Group")
                    where obj.ResourceTypeID == eResourceType.ResourceTypeID &&
                          ( (obj.ResourceNameFirst + " " + obj.ResourceNameLast).Contains(SearchQuery) || obj.ResourceLogin.Contains(SearchQuery) || obj.ResourceEmail.Contains(SearchQuery) || obj.ResourcePhoneNumber.Contains(SearchQuery) ) &&
                          (from GroupMember gmObj in obj.GroupMembers
                           where gmObj.GroupID == eGroup.GroupID
                           select gmObj.GroupMemberID).Count() > 0
                    orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending
                    select obj).ToArray();
        }

        public static Resource Get(Guid ResourceID)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources where obj.ResourceID == ResourceID && obj.ResourceIsActive == true select obj).FirstOrDefault();
        }

        public static Resource Get(Guid ResourceID, bool isActive)
        {
            var context = new STATSModel.STATSEntities();
            return (from STATSModel.Resource obj in context.Resources where obj.ResourceID == ResourceID select obj).FirstOrDefault();
        }

        /// <summary>
        /// Creates a new logged in session for the current Resource Object.  The analog to this method (for Logging Out) can be called from the returned Attendance Object.
        /// </summary>
        /// <param name="SessionTypeObj">The SessionType to be associated with the new Session.</param>
        /// <param name="IsScheduled">Whether or not this session was previously scheduled.  Usually <c>false</c>.</param>
        /// <param name="LoginMachineName">Name of the machine where the login originated from.</param>
        /// <returns>A new Attendance Object for the current Resource.</returns>
        public Attendance Login(SessionType SessionTypeObj, Boolean IsScheduled, String LoginMachineName, ResourceType resourceTypeTutor, ResourceType resourceTypeStudent)
        {
            if (!SessionTypeObj.CanLogin)
            {
                throw new Exception("This SessionType (" + SessionTypeObj.SessionTypeName + ") is not available at this time.");
            }

            using (var context = new STATSEntities())
            {
                // Ensure that a user is logged out of existing sessions before a new one is created.
                // NOTE: If the Resource is a Tutor, this does NOT apply.  Tutors can login to multiple locations at a time.
                if (this.ResourceType.ResourceTypeName != "Tutor")
                {
                    STATSModel.Attendance[] eAttendances = STATSModel.Attendance.GetActive(Resource.Get(this.ResourceID));
                    foreach (var eA in eAttendances)
                    {
                        eA.LogOut(resourceTypeTutor, resourceTypeStudent);
                    }
                }

                DateTime CurrentTime = DateTime.Now;


                var eSession = new STATSModel.Session();
                eSession.SessionID = Guid.NewGuid();
                eSession.CourseSectionID = null;
                eSession.SessionTypeID = SessionTypeObj.SessionTypeID;
                eSession.SessionDateTimeStart = CurrentTime;

                // If we have to "make stuff up" when creating a session, we don't know when to end it.
                // In this scenario, I am getting the closing time of the Area, and setting that.  Makes sense.
                var eOperatingRange = OperationRange.Get(SessionTypeObj, DateTime.Today.DayOfWeek);
                eSession.SessionDateTimeEnd = DateTime.Today.Add(eOperatingRange.OperationRangeTimeEnd);
                eSession.SessionIsCancelled = false;
                eSession.SessionIsScheduled = IsScheduled;
                context.AddToSessions(eSession);

                STATSModel.Schedule eSchedule = new STATSModel.Schedule();
                eSchedule.ScheduleID = Guid.NewGuid();
                eSchedule.ResourceID = this.ResourceID;
                eSchedule.SessionID = eSession.SessionID;
                eSchedule.ScheduleRequireAttend = IsScheduled;
                context.AddToSchedules(eSchedule);

                STATSModel.Attendance eAttendance = new STATSModel.Attendance();
                eAttendance.AttendanceID = Guid.NewGuid();
                eAttendance.ScheduleID = eSchedule.ScheduleID;
                eAttendance.AttendanceTimeIn = CurrentTime;
                eAttendance.AttendanceMachineName = LoginMachineName;
                context.AddToAttendances(eAttendance);

                context.SaveChanges();
                return eAttendance;
            }
            
        }

        public Attendance Login(Schedule ScheduleObj, String LoginMachineName, ResourceType resourceTypeTutor, ResourceType resourceTypeStudent)
        {
            Session eSession = ScheduleObj.Session;
            SessionType eSessionType = eSession.SessionType;
            
            if (!eSessionType.CanLogin)
            {
                throw new Exception("This SessionType (" + eSessionType.SessionTypeName + ") is not available at this time.");
            }
            SessionType eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
            SessionType eSessionTypeTutoringFootball = SessionType.GetByName("Tutoring - Football");

            // Ensure that a user is logged out of existing sessions before a new one is created.
            // NOTE: If the Resource is a Tutor, this does NOT apply.  Tutors can login to multiple locations at a time.
            // NOTE: However, it should be noted that tutors can only login to any single application once.  Log them out of existing sessions in that area.
            Boolean IsTutor = (ResourceTypeID == resourceTypeTutor.ResourceTypeID);
            Attendance ReturnedTutorAttendance = null;
            if (IsTutor) // Tutors can login to each application at most one (1) time.
            {
                Attendance[] eAttendances = Attendance.GetActive(Resource.Get(this.ResourceID));
                foreach (var TutorAttendance in eAttendances)
                {
                    var TutorSchedule = TutorAttendance.Schedule;
                    var TutorSession = TutorSchedule.Session;
                    var TutorSessionType = TutorSession.SessionType;
                    Boolean SameAsExisting = (TutorSessionType.SessionTypeID == eSessionType.SessionTypeID);

                    Boolean IsTutoring = (eSessionType.SessionTypeID == eSessionTypeTutoring.SessionTypeID || eSessionType.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID);
                    if (SameAsExisting && !IsTutoring)
                    {
                        // Note: If I terminate the existing session anytime a match is found, it will teminate unscheduled tutoring sessions.
                        // Note: This could cause some unexpected results.  Let's not do this for tutoring.
                        TutorAttendance.LogOut(resourceTypeTutor, resourceTypeStudent); // If the new session is the same type as an existing session, terminate the existing session.
                        // Note: This causes comp lab to work as expected, and log correctly, but tutoring will not have any side-effects.
                    }
                    else
                    {
                        // Note: We want to make sure that we return the right type of session
                        // If someone is logging into tutoring only return this session if it
                        // matches that type, otherwise we'll need to create a new one
                        var TutorAttendanceType = TutorAttendance.Schedule.Session.SessionType.SessionTypeName;
                        var LoggingInType = ScheduleObj.Session.SessionType.SessionTypeName;
                        if (TutorAttendanceType == LoggingInType)
                            ReturnedTutorAttendance = TutorAttendance;
                    }
                }
                if (ReturnedTutorAttendance != null)
                {
                    return ReturnedTutorAttendance;
                }
            }
            else // Non-Tutors can only have a SINGLE active login regardless of the SessionType.
            {
                Attendance[] eAttendances = Attendance.GetActive(Resource.Get(this.ResourceID));
                foreach (Attendance eA in eAttendances)
                {
                    eA.LogOut(resourceTypeTutor, resourceTypeStudent); // Always terminate all other sessions when a non-tutor is logging in.
                }
            }

            // Ensure that it is still valid to login to this session.
            // Users cannot log in to a session before a certain point, and after a certain point.

            String loginearly = Setting.GetByKey("LoginEarlyMinutes").SettingValue;
            String loginlate = Setting.GetByKey("LoginLateMinutes").SettingValue;
            int early = Int32.Parse(loginearly);
            int late = Int32.Parse(loginlate);

            Session oSession = ScheduleObj.Session;

            DateTime Start = oSession.SessionDateTimeStart.AddMinutes(early * -1);
            DateTime End = oSession.SessionDateTimeStart.AddMinutes(late);

            if (DateTime.Now > Start && DateTime.Now < End)
            {
                var context = new STATSModel.STATSEntities();

                Attendance eAttendance = new Attendance();
                eAttendance.AttendanceID = Guid.NewGuid();
                eAttendance.ScheduleID = ScheduleObj.ScheduleID;
                eAttendance.AttendanceTimeIn = DateTime.Now;
                eAttendance.AttendanceMachineName = LoginMachineName;

                context.AddToAttendances(eAttendance);
                context.SaveChanges();
                return eAttendance;
            }
            else
            {
                throw new Exception("You have attempted to login to a session that is not currently accepting new attendances.");
            }
        }

        public KeyValuePair<String, bool> IsLoggedInElsewhere
        {
            get
            {
                var loggedIn = new KeyValuePair<String, bool>("none", false);

                STATSModel.Attendance[] eAttendances = STATSModel.Attendance.GetActive(Resource.Get(this.ResourceID));
                if (eAttendances.Length > 0)
                {
                    var temp = eAttendances[0];
                    var eSchedule = eAttendances[0].Schedule;
                    var eSession = eSchedule.Session;
                    var eSessionType = eSession.SessionType;
                    loggedIn = new KeyValuePair<string, bool>(eSessionType.SessionTypeName, true);
                    return loggedIn;
                }
                else
                {
                    return loggedIn;
                }
            }
        }

        private static bool AuthenticateUserAgainstAD(string domain, string username, string password, string ldapPath, out string errorMessage)
        {
            errorMessage = "";
            string domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(ldapPath, domainAndUsername, password);
            try
            {
                // Bind to the native AdsObject to force authentication.
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();
                if (null == result)
                {
                    return false;
                }
                // Update the new path to the user in the directory
                ldapPath = result.Path;
                string filterAttribute = (String)result.Properties["cn"][0];
                System.Diagnostics.Debug.WriteLine(filterAttribute);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
                //throw new Exception("Error authenticating user." + ex.Message);
            }
            return true;
        }


    }
}