﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class Schedule
    {
        public static Schedule Get(Resource eResource, Session eSession)
        {
            var eSchedules = eSession.Schedules;
            foreach (var eSchedule in eSchedules)
            {
                if (eSchedule.ResourceID == eResource.ResourceID)
                {
                    return eSchedule;
                }
            }
            throw new Exception("This Resource does not have a Schedule record for this Session!");
        }


        /// <summary>
        /// Cancel all sessions, delete annotations for a resource
        /// </summary>
        public static void Delete(Resource resource)
        {
            using (var context = new STATSEntities())
            {
                var schedules = (from Schedule sched in context.Schedules where sched.ResourceID == resource.ResourceID select sched);
                Guid[] scheduleIDs = (from Schedule sched in schedules select sched.ScheduleID).ToArray();
                Guid[] sessionIDs = (from Schedule sched in schedules select sched.SessionID).ToArray();

                var annotations = (from Annotation obj in context.Annotations where scheduleIDs.Contains(obj.SourceID) select obj);
                foreach (var annotation in annotations)
                {
                    context.Annotations.DeleteObject(annotation);
                }

                var sessions = (from Session sess in context.Sessions where sessionIDs.Contains(sess.SessionID) && sess.SessionIsCancelled == false select sess);
                foreach (var session in sessions)
                {
                    session.SessionIsCancelled = true;
                    if(session.TeamworksID.HasValue && session.TeamworksID.Value != 0)
                    {
                        Teamworks.TeamworksAPI.RemoveSession(session.TeamworksID.Value);
                    }
                }
                //foreach (var schedule in schedules)
                //{
                //    context.Schedules.DeleteObject(schedule);
                //      //if you delete the schedule, you also need to delete attendance records for it
                //}
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Delete a specific schedule for a Resource.  This would occur if the user
        /// was removed from a group that already had scheduled events.  Removes the
        /// groups' events.  
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="group"></param>
        public static void DeleteFromGroup(Resource resource, Guid groupID)
        {
            using (var context = new STATSEntities())
            {
                //Session IDs specific to the group
                Guid[] sessionIDsForGroup = (from Session sess in context.Sessions where sess.GroupID == groupID select sess.SessionID).ToArray();

                var schedulesForIndividual = (from Schedule sched in context.Schedules
                                              where sched.ResourceID == resource.ResourceID 
                                              select sched);

                foreach(Schedule s in schedulesForIndividual)
                {
                    //If the schedule was part of a session that was assigned to a group
                    if(sessionIDsForGroup.Contains(s.SessionID))
                    {
                        //Also delete from Teamworks
                        if(s.Session.TeamworksID.HasValue && s.Session.TeamworksID.Value !=0)
                        {
                            Teamworks.TeamworksAPI.RemoveSession(s.Session.TeamworksID.Value);
                        }
                        context.Schedules.DeleteObject(s);
                    }
                }
                
                context.SaveChanges();
            }
        }

        public static Schedule Get(Guid ScheduleID)
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Schedule obj in context.Schedules where obj.ScheduleID == ScheduleID select obj).First();
            return result;
        }

        public static Schedule[] Get()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.Schedule obj in context.Schedules select obj).ToArray();
            return result;
        }

        public static Schedule[] Get(Resource eResource, DateTime RangeStart, DateTime RangeEnd)
        {
            var context = new STATSEntities();
            var eSchedules = (  from Schedule obj 
                                in context.Schedules.Include("Session").Include("Session.SessionType").Include("Session.CourseSection").Include("Session.CourseSection.Course").Include("Session.CourseSection.Course.Subject") 
                                where 
                                    obj.Session.SessionDateTimeStart >= RangeStart 
                                    && 
                                    obj.Session.SessionDateTimeEnd <= RangeEnd 
                                    && 
                                    obj.ResourceID == eResource.ResourceID 
                                    && 
                                    obj.Session.SessionIsScheduled 
                                    && 
                                    !obj.Session.SessionIsCancelled 
                                select obj).ToList();

            // Previously, I was only returning valid schedules.
            // See below for the added logic.
            //return eSchedules;

            SessionType eSessionTypeNote = SessionType.GetByName("note");
            var eSchedulesNotes = (
                                      from Schedule obj
                                          in context.Schedules.Include("Session").Include("Session.SessionType").Include("Session.CourseSection").Include("Session.CourseSection.Course").Include("Session.CourseSection.Course.Subject")
                                      where
                                          obj.Session.SessionTypeID == eSessionTypeNote.SessionTypeID
                                          &&
                                          obj.ResourceID == eResource.ResourceID
                                          &&
                                          obj.Session.SessionIsScheduled
                                          &&
                                          !obj.Session.SessionIsCancelled
                                      select obj
                                    ).ToList();

            // Regular schedule entries are retrieved only if they fall within the requested range.
            // Free-form entries are retrieved regardless of any range logic.  This is so that they can display re-occurring.
            var CompositeSchedules = eSchedules.Union(eSchedulesNotes);
            return CompositeSchedules.ToArray();
        }
    }
}
