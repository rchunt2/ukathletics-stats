﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public partial class TextMessage
    {
        public static List<TextMessage> GetAllToSend()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.TextMessage obj in context.TextMessages where obj.MessageSent == false select obj);
            return result.ToList();
        }

        public static List<TextMessage> GetSentMessages()
        {
            var context = new STATSModel.STATSEntities();
            var result = (from STATSModel.TextMessage obj in context.TextMessages where obj.MessageSent == true && obj.CourseSectionID != null select obj);
            return result.ToList();
        }

        public static List<TextMessage> Get(Resource resource, Guid? sessionID, Guid? courseSectionID)
        {
            if(sessionID.HasValue)
            {
                var context = new STATSModel.STATSEntities();
                var result = (from STATSModel.TextMessage obj in context.TextMessages 
                              where obj.ResourceID == resource.ResourceID && obj.SessionID == sessionID
                              select obj);
                return result.ToList();
            }
            else
            {
                var context = new STATSModel.STATSEntities();
                var result = (from STATSModel.TextMessage obj in context.TextMessages
                              where obj.ResourceID == resource.ResourceID && obj.CourseSectionID == courseSectionID
                              select obj);
                return result.ToList();
            }
        }


        /// <summary>
        /// Creates a new TextMessaging entry
        /// </summary>
        public static Boolean CreateTextMessage(Resource eResource, Guid? sessionID, Guid? courseSectionID)
        {
            try
            {
                var context = new STATSEntities();

                TextMessage textMessage = new TextMessage();
                textMessage.TextMessageID = Guid.NewGuid();
                textMessage.ResourceID = eResource.ResourceID;
                textMessage.SessionID = sessionID;
                textMessage.CourseSectionID = courseSectionID;
                textMessage.MessageSent = false;

                context.TextMessages.AddObject(textMessage);
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean ResetSentFlag(bool sent)
        {
            try
            {
                var context = new STATSEntities();
                var message = context.TextMessages.Single(p => p.TextMessageID== TextMessageID); // or some other way to get the person in question
                message.MessageSent = sent;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes a new TextMessaging entry
        /// </summary>
        public static Boolean DeleteTextMessage(Resource eResource, Guid? sessionID, Guid? courseSectionID)
        {
            var db = new STATSModel.STATSEntities();
            // Query the database for the rows to be deleted.
            var deleteTextMessages =
                from messages in db.TextMessages
                where messages.ResourceID == eResource.ResourceID && (messages.SessionID == sessionID || messages.CourseSectionID == courseSectionID)
                select messages;

            try
            {
                foreach (var message in deleteTextMessages)
                {
                    db.TextMessages.DeleteObject(message);
                }
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
