﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UK.STATS.STATSModel
{
    public class AttendanceTime
    {
        public int CompLabSunday { get; set; }
        public int CompLabMonday { get; set; }
        public int CompLabTuesday { get; set; }
        public int CompLabWednesday { get; set; }
        public int CompLabThursday { get; set; }
        public int CompLabFriday { get; set; }
        public int CompLabSaturday { get; set; }
        public int CompLabTotal
        {
            get { return CompLabSunday + CompLabMonday + CompLabTuesday + CompLabWednesday + CompLabThursday + CompLabFriday + CompLabSaturday; }
        }

        public int TutoringSunday { get; set; }
        public int TutoringMonday { get; set; }
        public int TutoringTuesday { get; set; }
        public int TutoringWednesday { get; set; }
        public int TutoringThursday { get; set; }
        public int TutoringFriday { get; set; }
        public int TutoringSaturday { get; set; }
        public int TutoringTotal
        {
            get { return TutoringSunday + TutoringMonday + TutoringTuesday + TutoringWednesday + TutoringThursday + TutoringFriday + TutoringSaturday; }
        }

        public int StudyHallSunday { get; set; }
        public int StudyHallMonday { get; set; }
        public int StudyHallTuesday { get; set; }
        public int StudyHallWednesday { get; set; }
        public int StudyHallThursday { get; set; }
        public int StudyHallFriday { get; set; }
        public int StudyHallSaturday { get; set; }
        public int StudyHallTotal
        {
            get { return StudyHallSunday + StudyHallMonday + StudyHallTuesday + StudyHallWednesday + StudyHallThursday + StudyHallFriday + StudyHallSaturday; }
        }

        public int SundayTotal
        {
            get { return CompLabSunday + TutoringSunday + StudyHallSunday; }
        }
        public int MondayTotal
        {
            get { return CompLabMonday + TutoringMonday + StudyHallMonday; }
        }
        public int TuesdayTotal
        {
            get { return CompLabTuesday + TutoringTuesday + StudyHallTuesday; }
        }
        public int WednesdayTotal
        {
            get { return CompLabWednesday + TutoringWednesday + StudyHallWednesday; }
        }
        public int ThursdayTotal
        {
            get { return CompLabThursday + TutoringThursday + StudyHallThursday; }
        }
        public int FridayTotal
        {
            get { return CompLabFriday + TutoringFriday + StudyHallFriday; }
        }
        public int SaturdayTotal
        {
            get { return CompLabSaturday + TutoringSaturday + StudyHallSaturday; }
        }
        public int OverallTotal
        {
            get { return CompLabTotal + TutoringTotal + StudyHallTotal; }
        }
    }
}
