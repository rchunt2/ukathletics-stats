﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.ReusableServerControls
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ResourceInfoPopup runat=server></{0}:ResourceInfoPopup>")]
    public class ResourceInfoPopup : WebControl
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The ResourceID to show information for")]
        public Guid ResourceID
        {
            get
            {
                if (ViewState["ResourceID"] == null)
                {
                    return Guid.Empty;
                }
                else
                {
                    return (Guid)ViewState["ResourceID"];
                }
            }
            set
            {
                ViewState["ResourceID"] = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            Telerik.Web.UI.RadBinaryImage rbiResourcePicture = new Telerik.Web.UI.RadBinaryImage();
            rbiResourcePicture.Height = new Unit(145, UnitType.Pixel);
            rbiResourcePicture.Width = new Unit(105, UnitType.Pixel);
            rbiResourcePicture.ResizeMode = Telerik.Web.UI.BinaryImageResizeMode.Fit;

            bool hasPicture = ResourcePicture.HasPicture(ResourceID);
            if (hasPicture)
            {
                rbiResourcePicture.DataValue = ResourcePicture.GetPicture(ResourceID);
                rbiResourcePicture.Visible = true;
            }
            else
            {
                rbiResourcePicture.Visible = false;
            }
            Resource resource = Resource.Get(ResourceID);

            //output.Write(@"<div style=""border: 1px solid #999999; margin: 5px; background-color: #0A9DFF"">");
            rbiResourcePicture.RenderControl(output);
            output.Write("<br /><span style=\"font-weight: bold\">");
            output.Write(resource.ResourceNameDisplayLastFirst);
            output.Write("</span><br />");
            output.Write(resource.ResourceType.ResourceTypeName);
            //output.Write(@"</div>");
        }
    }
}