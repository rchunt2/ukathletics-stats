﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UK.STATS.Tutoring
{
    public class GVAbsent
    {
        private String _present;
        private String _missing;
        private String _subject;
        private Guid _presentResourceID;
        private Guid _missingResourceID;

        public Guid PresentResourceID
        {
            get { return _presentResourceID; }
        }

        public Guid MissingResourceID
        {
            get { return _missingResourceID; }
        }

        public String Present
        {
            get { return _present; }
        }

        public String Missing
        {
            get { return _missing; }
        }

        public String Subject
        {
            get { return _subject; }
        }

        public GVAbsent()
        {
        }

        public GVAbsent(String Present, String Missing, String Subject)
        {
            _present = Present;
            _missing = Missing;
            _subject = Subject;
        }

        public GVAbsent(String Present, String Missing, String Subject, Guid MissingID, Guid PresentID)
        {
            _present = Present;
            _missing = Missing;
            _subject = Subject;
            _missingResourceID = MissingID;
            _presentResourceID = PresentID;
        }
    }


    public class GVActive
    {
        private Guid _attendanceID;
        private String _tutor;
        private STATSModel.Resource _tutorResource;
        private String _student;
        private STATSModel.Resource _studentResource;
        private String _start;
        private String _subject;
        private String _room;

        public Guid AttendanceID
        {
            get { return _attendanceID; }
        }

        public String TutorName
        {
            get { return _tutor; }
        }

        public STATSModel.Resource Tutor
        {
            get { return _tutorResource; }
        }

        public String StudentName
        {
            get { return _student; }
        }

        public STATSModel.Resource Student
        {
            get { return _studentResource; }
        }

        public String StartTime
        {
            get { return _start; }
        }

        public String Subject
        {
            get { return _subject; }
        }

        public String Room
        {
            get { return _room; }
        }
        
        public GVActive()
        {
        }

        public GVActive(String TutorName, String StudentName, String StartTime, Guid AttendanceID, String Subject, String Room, Guid TutorGuid, Guid StudentGuid)
        {
            _tutor = TutorName;
            _tutorResource = STATSModel.Resource.Get(TutorGuid);
            _student = StudentName;
            _studentResource = STATSModel.Resource.Get(StudentGuid);
            _start = StartTime;
            _attendanceID = AttendanceID;
            _subject = Subject;
            _room = Room;
        }
    }
}