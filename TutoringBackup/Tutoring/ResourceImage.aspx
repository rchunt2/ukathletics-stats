﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResourceImage.aspx.cs" Inherits="UK.STATS.Tutoring.ResourceImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div>
        <asp:Image runat="server" ID="ResourcePictureImg" Visible="false" />
        <asp:Label Text="No Image Found" runat="server" ID="NoImageLabel" Visible="true" />
    </div>
</body>
</html>
