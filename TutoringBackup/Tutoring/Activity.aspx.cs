﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Tutoring
{
    public partial class Activity : Page
    {
        //DM - 9/25/2012 - changed resource types to pull from Application variable to prevent lots of unnessary hits to the DB since they do not change.
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        private readonly SessionType eSessionType = SessionType.GetByName("Tutoring - CATS");
        private const String MachineName = "Tutoring Machine";
        private Guid LocationID = new Guid("C9C609CC-85EB-E511-82C9-E82AEA768F71");

        public int TutoringSyncInterval
        {
            get
            {
                String strTutoringSyncInterval = Setting.GetByKey("TutoringSyncInterval").SettingValue;
                int iTutoringSyncInterval = Convert.ToInt32(strTutoringSyncInterval);
                return iTutoringSyncInterval;
            }
        }

        private Boolean AutoUpdateEnabled
        {
            get { return (TutoringSyncInterval > 0); }
        }

        private Session[] eSessionsActiveAndScheduled
        {
            //Changed to cache these in the ApplicationState so that multiple visitors (or quick refreshes) will not hit the database to load the same information
            get
            {
                return (Session[])Application["eSessionsActiveAndScheduled"];
            }
            set
            {
                Application["eSessionsActiveAndScheduled"] = value;
            }
        }

        private Session[] eSessionsActiveAndUnScheduled
        {
            //Changed to cache these in the ApplicationState so that multiple visitors (or quick refreshes) will not hit the database to load the same information
            get
            {
                return (Session[])Application["eSessionsActiveAndUnScheduled"];
            }
            set
            {
                Application["eSessionsActiveAndUnScheduled"] = value;
            }
        }

        private Session[] eSessionsScheduledInactive
        {
            //Changed to cache these in the ApplicationState so that multiple visitors (or quick refreshes) will not hit the database to load the same information
            get
            {
                return (Session[])Application["eSessionsScheduledInactive"];
            }
            set
            {
                Application["eSessionsScheduledInactive"] = value;
            }
        }

        private Session[] eTutoringSessions
        {
            //Changed to cache these in the ApplicationState so that multiple visitors (or quick refreshes) will not hit the database to load the same information
            get
            {
                return (Session[])Application["eTutoringSessions"];
            }
            set
            {
                Application["eTutoringSessions"] = value;
            }
        }

        private DateTime LastTimeDataRefreshed
        {
            //Since the above data is cached, store a time of when it was last refreshed so we know when to reload the data
            get
            {
                if (Application["LastTimeDataRefreshed"] == null)
                {
                    Application["LastTimeDataRefreshed"] = DateTime.MinValue;
                }
                return (DateTime)Application["LastTimeDataRefreshed"];
            }
            set
            {
                Application["LastTimeDataRefreshed"] = value;
            }
        }

        public void ForceReloadPage()
        {
            LastTimeDataRefreshed = DateTime.MinValue; //Force refresh of data next time
            Response.Redirect(Request.Url.OriginalString, true);
        }

        private void AssignRoom(Session eSession)
        {
            Boolean RequirementsMet = eSession.HasTutorAndStudentPresent();
            if (RequirementsMet)
            {
                eSession.AssignRoom(ResourceTypeTutor, LocationID);
                SessionTutor SessionTutorRefresh = eSession.GetAdditionalData();
                if (SessionTutorRefresh.RoomID.HasValue)
                {
                    if (SessionTutorRefresh.Room.RoomName == "N/A")
                    {
                        ScriptManager.RegisterClientScriptBlock(lblCurrentDate, lblCurrentDate.GetType(), "RoomNotifier", "<script language='javascript'>alert('Go to your personally designated room.');</script>", false);
                        hfRoomNumber.Value = "Personal";
                        Session["RoomNumber"] = "Personal";
                    }
                    else
                    {
                        //07/13/15 - RCH - UK wants a message to be displayed to the user if a "900" level room is assigned.
                        //This means that none of the actual rooms are available and the tutors should find another location
                        //for the session.
                        if (SessionTutorRefresh.Room.RoomName.StartsWith("9"))
                        {
                            ScriptManager.RegisterClientScriptBlock(lblCurrentDate, lblCurrentDate.GetType(), "RoomNotifier", "<script language='javascript'>alert('All rooms are full.  Please use Computer Lab or an alternate space.');</script>", false);
                            hfRoomNumber.Value = SessionTutorRefresh.Room.RoomName.ToString();
                            Session["RoomNumber"] = SessionTutorRefresh.Room.RoomName.ToString();
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(lblCurrentDate, lblCurrentDate.GetType(), "RoomNotifier", "<script language='javascript'>alert('Go to room " + SessionTutorRefresh.Room.RoomName + "');</script>", false);
                            hfRoomNumber.Value = SessionTutorRefresh.Room.RoomName.ToString();
                            Session["RoomNumber"] = SessionTutorRefresh.Room.RoomName.ToString();
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(lblCurrentDate, lblCurrentDate.GetType(), "RoomNotifier", "<script language='javascript'>alert('There were no available rooms.  Find a quiet location for your session.');</script>", false);
                }
            }
        }

        private void PopulateActive()
        {
            var lstActive = GetActiveDatasource();
            gvActive.DataSource = lstActive;
            gvActive.DataBind();
        }

        private List<GVActive> GetActiveDatasource()
        {
            RadToolTipManager.TargetControls.Clear();
            var lstActive = new List<GVActive>();

            //var eSessionsActiveAndScheduled = STATSModel.Session.GetTutoringSessions(true, true);
            //var eSessionsActiveAndUnScheduled = STATSModel.Session.GetTutoringSessions(true, false);

            var eSessions = new List<Session>();
            eSessions.AddRange(eSessionsActiveAndScheduled);
            eSessions.AddRange(eSessionsActiveAndUnScheduled);

            foreach (var eSession in eSessions.Where(x => x.SessionTypeID == eSessionType.SessionTypeID))
            {
                Boolean HasMetadata = eSession.HasAdditionalData();
                Resource Tutor = eSession.GetTutor(ResourceTypeTutor);
                Guid tutorGuid = Tutor.ResourceID;
                Guid studentGuid = new Guid();
                String strTutorName = Tutor.ResourceNameDisplayLastFirst;
                String strSubject = "N/A";
                String strRoom = "N/A";

                if (HasMetadata)
                {
                    SessionTutor eSessionTutor = eSession.GetAdditionalData();
                    if (eSessionTutor.CourseID.HasValue)
                    {
                        var eCourse = eSessionTutor.Course;
                        var eSubject = eCourse.Subject;
                        strSubject = eSubject.SubjectNameShort + eCourse.CourseNumber;
                    }
                    if (eSessionTutor.RoomID.HasValue)
                    {
                        strRoom = eSessionTutor.Room.RoomName;
                    }
                }

                var eResources = eSession.GetPresentStudents();
                foreach (var eResource in eResources)
                {
                    String strNameStudent = eResource.ResourceNameDisplayLastFirst;
                    studentGuid = eResource.ResourceID;
                    String strTimeStart = eSession.SessionDateTimeStart.ToShortTimeString();

                    Attendance[] eAttendances = Attendance.GetActive(eResource);
                    Guid AttendanceID = eAttendances[0].AttendanceID;

                    var gva = new GVActive(strTutorName, strNameStudent, strTimeStart, AttendanceID, strSubject, strRoom, tutorGuid, studentGuid);
                    lstActive.Add(gva);
                }
            }
            /*
            using (var context = new STATSEntities())
            {
                var tutorGuid = Guid.Parse("db387642-8238-e111-8e81-005056936d51");
                var studentGuid = Guid.Parse("d9387642-8238-e111-8e81-005056936d51");
                var tutor = context.Resources.Where(x => x.ResourceTypeID == tutorGuid).Take(1).Single();
                var student = context.Resources.Where(x => x.ResourceTypeID == studentGuid).Take(1).Single();
                lstActive.Add(new GVActive(tutor.ResourceNameDisplayLastFirst, student.ResourceNameDisplayLastFirst, DateTime.Now.ToShortDateString(), Guid.NewGuid(), "Some Subject", "204", tutor.ResourceID, student.ResourceID, tutor.ResourcePicture != null ? tutor.ResourcePicture.ResourcePictureBytes : null, student.ResourcePicture != null ? student.ResourcePicture.ResourcePictureBytes : null));
            }
             */
            return lstActive;
        }

        protected void gvActive_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RadToolTipManager.TargetControls.Add(e.Row.FindControl("lblTutor").ClientID, ((GVActive)e.Row.DataItem).Tutor.ResourceID.ToString(), true);
                RadToolTipManager.TargetControls.Add(e.Row.FindControl("lblStudent").ClientID, ((GVActive)e.Row.DataItem).Student.ResourceID.ToString(), true);
            }
        }


        protected void RadToolTipManager_OnAjaxUpdate(object sender, Telerik.Web.UI.ToolTipUpdateEventArgs args)
        {
            UK.STATS.ReusableServerControls.ResourceInfoPopup infoPopup = new ReusableServerControls.ResourceInfoPopup();
            infoPopup.ResourceID = Guid.Parse(args.Value);
            args.UpdatePanel.ContentTemplateContainer.Controls.Add(infoPopup);
        }

        private void PopulateMissing()
        {
            var lstAbsent = GetMissingDatasource();
            gvMissing.DataSource = lstAbsent;
            gvMissing.DataBind();
        }

        private List<GVAbsent> GetMissingDatasource()
        {
            List<Session> allReturnedSessions = null;
            var lstAbsent = new List<GVAbsent>();

            using (var context = new UK.STATS.STATSModel.STATSEntities())
            {
                //Call stored proc to get complex type result with {SessionId, NumStudentsPresent, NumTutorsPresent} columns
                var sessionsMissingResource = context.GetSessionsWithMissingResource().Where(x => x.SessionTypeID == eSessionType.SessionTypeID).ToList();

                //Get All the session IDs into an array

                //allReturnedSessions =
                //    context.Sessions.Where(x => x.SessionDateTimeStart > new DateTime(2013, 12, 1)).Take(20).ToList();
                Guid[] sessionIds = (from STATSModel.GetSessionsWithMissingResource_Result obj in sessionsMissingResource
                                     select obj.SessionID).ToArray();

                //Get all the Session objects from the database
                allReturnedSessions = (from STATSModel.Session obj in context.Sessions
                                       where sessionIds.Contains(obj.SessionID)
                                       select obj).ToList();
            }

            foreach (var eSession in allReturnedSessions)
            {
                var eResources = eSession.GetResourceInSession();
                foreach (var eResource in eResources)
                {
                    if (eResource.IsPresent(eSession))
                    {
                        lstAbsent.AddRange(GetMissingRequirements(eResource, eSession));
                    }
                }
            }

            return lstAbsent;
        }

        private void PopulateUnscheduled()
        {
            //var eSessions = STATSModel.Session.GetTutoringSessions(false, false);

            foreach (var eSession in eTutoringSessions)
            {
                Resource[] eResources = eSession.GetResourceInSession();
                int iResources = eResources.Length;
                if (iResources == 1) // Only a single resource in the session.
                {
                    Resource eResource = eResources.First(); // Get the only resource in that session.
                    if (eResource.IsPresent(eSession))
                    {
                        var sb = new StringBuilder();
                        sb.Append(eResource.ResourceNameDisplayLastFirst);                              // Name
                        sb.Append(" [" + eSession.SessionDateTimeStart.ToShortTimeString() + "]");      // Login Time
                        sb.Append(" [" + eResource.ResourceType.ResourceTypeName + "]");                // Resource Type

                        String strTxt = sb.ToString();
                        String strVal = eResource.ResourceID.ToString();
                    }
                }
            }
            SortUnscheduledList();
        }

        private void PopulateGrids()
        {
            RefreshData();
            PopulateActive();
            PopulateMissing();
            PopulateUnscheduled();
            upPage.Update();
        }

        private void RefreshData()
        {
            //Only refresh if its been more than 10 seconds between last refresh
            //May be able to increase this time for better performance, but may need less time if updates aren't showing often enough
            if ((DateTime.Now - LastTimeDataRefreshed).TotalSeconds > 10)
            {
                LastTimeDataRefreshed = DateTime.Now;

                Session[] activeScheduled = null;
                Session[] activeUnscheduled = null;
                Session[] scheduledInactive = null;
                Session[] tutoringSessions = null;
                
                STATSModel.Session.FillTutoringSessionArrays(out activeScheduled, out activeUnscheduled, out scheduledInactive, out tutoringSessions, false);
                eSessionsActiveAndScheduled = activeScheduled;
                eSessionsActiveAndUnScheduled = activeUnscheduled;
                eSessionsScheduledInactive = scheduledInactive;
                eTutoringSessions = tutoringSessions;
            }
        }

        private int NumberOfUnscheduleStudents()
        {
            int count = 0;
            var eSessions = STATSModel.Session.GetActive(eSessionType);
            foreach (var eSession in eSessions)
            {
                if (!eSession.SessionIsScheduled && !eSession.HasTutor(ResourceTypeTutor))
                {
                    Resource[] eResources = eSession.GetResourceInSession();
                    if (eResources.Length == 1)
                    {
                        Resource eResource = eResources[0];
                        if (eResource.IsPresent(eSession))
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }

        private void SortUnscheduledList()
        {
            //ListItem[] ListItems = (from ListItem obj in lbStudent.Items select obj).ToArray();
            //ListItem[] Tutors = (from ListItem obj in ListItems orderby StripUnscheduledText(obj.Text) ascending where obj.Text.Contains("[Tutor]") select obj).ToArray();
            //ListItem[] Students = (from ListItem obj in ListItems orderby StripUnscheduledText(obj.Text) ascending where obj.Text.Contains("[Student]") select obj).ToArray();
            //ListItem[] Everyone = (from ListItem obj in ListItems orderby StripUnscheduledText(obj.Text) ascending select obj).ToArray();
            //lbStudent.Items.Clear();
            //foreach (var listItem in Tutors)
            //{
            //    lbStudent.Items.Add(listItem);
            //}
            //foreach (var listItem in Students)
            //{
            //    lbStudent.Items.Add(listItem);
            //}
            //lbStudent.Items.Clear();
            //foreach (var listItem in Everyone)
            //{
            //    lbStudent.Items.Add(listItem);
            //}
        }

        private String StripUnscheduledText(String Input)
        {
            int index = Input.IndexOf('[');
            if (index >= 0)
            {
                return Input.Substring(0, index - 1);
            }
            return Input;
        }

        private List<GVAbsent> GetMissingRequirements(Resource eResource, Session eSession)
        {
            // NOTE: Tutors require ALL Students.
            // NOTE: Students require ONE Tutor.
            var result = new List<GVAbsent>();
            if (!eSession.SessionIsScheduled)
            {
                return result;
            } // If this isn't scheduled, it's not required for anyone...

            //if (!eResource.IsPresent(eSession))
            //{
            //    return result;
            //} // If this resource isn't here, of course there are no missing requirements.

            String strSubject = "N/A";

            if (eSession.HasAdditionalData())
            {
                var eSessionTutor = eSession.GetAdditionalData();
                var eCourse = eSessionTutor.Course;
                if (eCourse != null)
                {
                    var eSubject = eCourse.Subject;
                    strSubject = eSubject.SubjectNameShort + eCourse.CourseNumber;
                }
            }

            if (eResource.ResourceType.ResourceTypeName == "Tutor")
            {
                Resource[] eResources = eSession.GetResourceInSession();
                foreach (var eResourceInSession in eResources)
                {
                    if (eResource.ResourceID != eResourceInSession.ResourceID) // Resources can't require themselves.
                    {
                        if (!eResourceInSession.IsPresent(eSession))
                        {
                            var gva = new GVAbsent(eResource.ResourceNameDisplayLastFirst, eResourceInSession.ResourceNameDisplayLastFirst, strSubject, eResource.ResourceID, eResourceInSession.ResourceID);
                            result.Add(gva);
                        }
                    }
                }
            }
            else
            {
                var eResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                if (!eResourceTutor.IsPresent(eSession))
                {
                    var gva = new GVAbsent(eResource.ResourceNameDisplayLastFirst, eResourceTutor.ResourceNameDisplayLastFirst, strSubject, eResource.ResourceID, eResourceTutor.ResourceID);
                    result.Add(gva);
                }
            }
            return result;
        }

        private Boolean CanTutorLoginForUnscheduledSession()
        {
            return (NumberOfUnscheduleStudents() > 0);
        }

        private Boolean HasScheduledSession(Resource eResource)
        {
            // Note: This actually returns a value signifying whether or not there is a Scheduled Session that can still be logged into.
            Session[] eSessions = STATSModel.Session.GetActive(eResource, eSessionType);
            return (eSessions.Count() > 0);
        }

        private Session GetScheduledSession(Resource eResource)
        {
            Session[] eSessions = STATSModel.Session.GetActive(eResource, eSessionType);
            var eSession = eSessions[0];
            return eSession;
        }

        private void LoginResource(Resource eResource)
        {
            // Is this resource logged in to Tutoring already?
            // If so, we may want to alert the user about their already-logged-in state.
            // However, for now I'll allow the login attempt.
            // Implications: Their current session will be ended, and a new one will be created.

            // Is there a scheduled session for this Resource?
            Boolean isScheduled = HasScheduledSession(eResource);
            if (isScheduled)
            {
                var eSession = GetScheduledSession(eResource);
                var eSchedule = Schedule.Get(eResource, eSession);
                eResource.Login(eSchedule, MachineName, ResourceTypeTutor, ResourceTypeStudent); //eResource.Login(eSchedule, Request.UserHostAddress);
                PreAssignRoom(eResource);
                SessionTutor temp = eSession.GetAdditionalData();
                if (temp.Room != null)
                {
                    //if both student and tutor are not logged in, temp.Room is null here
                    hfRoomNumber.Value = temp.Room.RoomName;
                }
            }
            else // There's no Scheduled session right now.
            {
                CreateUnscheduledSessionAndLogin(eResource);
            }
        }

        private void PreAssignRoom(Resource eResource)
        {
            Session[] eSessions = STATSModel.Session.GetActive(eResource, eSessionType);
            if (eSessions.Count() > 0)
            {
                var eSession = eSessions[0];
                AssignRoom(eSession);
            }
        }

        private void CreateUnscheduledSessionAndLogin(Resource eResource)
        {
            // ToDo: these sessions are going on the unscheduled list of Tutoring.  Maybe don't make these logins last for the length of a Tutoring Session.
            int TutorSessionLength = Convert.ToInt32(Setting.GetByKey("TutorSessionLength").SettingValue);
            Session eSession = STATSModel.Session.CreateSession(Guid.NewGuid(), eSessionType.SessionTypeID, DateTime.Now, DateTime.Now.AddMinutes(TutorSessionLength), false, false);
            Schedule eSchedule = Schedule.CreateSchedule(Guid.NewGuid(), eResource.ResourceID, eSession.SessionID, false, false);

            var context = new STATSEntities();
            context.Sessions.AddObject(eSession);
            context.Schedules.AddObject(eSchedule);
            context.SaveChanges();
            eResource.Login(eSchedule, MachineName, ResourceTypeTutor, ResourceTypeStudent);
        }

        private void AddUnscheduledResourceToTutoringSession(Resource eResourceTutor, Resource eResourceStudent)
        {
            var context = new STATSEntities();

            // Make sure that the session is a tutoring session before using it to login
            Attendance eAttendanceTutor = (from Attendance obj in context.Attendances.Include("Schedule").Include("Schedule.Session") where obj.Schedule.ResourceID == eResourceTutor.ResourceID && obj.AttendanceTimeOut == null && obj.Schedule.Session.SessionType.SessionTypeName == eSessionType.SessionTypeName select obj).Single();
            Attendance eAttendanceStudent = (from Attendance obj in context.Attendances.Include("Schedule").Include("Schedule.Session") where obj.Schedule.ResourceID == eResourceStudent.ResourceID && obj.AttendanceTimeOut == null && obj.Schedule.Session.SessionType.SessionTypeName == eSessionType.SessionTypeName select obj).Single();
            Schedule eScheduleTutor = eAttendanceTutor.Schedule;
            Session eSessionTutor = eScheduleTutor.Session;

            // We should set the student's Session to be "cancelled" because their actual session will be with their tutor.
            eAttendanceStudent.Schedule.Session.SessionIsCancelled = true;
            context.SaveChanges();

            // The last and final step is to log the Student out of their session, and into the Tutor's session.
            eAttendanceStudent.LogOut(ResourceTypeTutor, ResourceTypeStudent);
            Schedule eScheduleStudent = Schedule.CreateSchedule(Guid.NewGuid(), eResourceStudent.ResourceID, eSessionTutor.SessionID, false, false);
            context.Schedules.AddObject(eScheduleStudent);
            context.SaveChanges();
            eResourceStudent.Login(eScheduleStudent, MachineName, ResourceTypeTutor, ResourceTypeStudent);

            //txtLogin.Text = "";
            AssignRoom(eSessionTutor);
        }

        private void LogoutResource(Resource eResource)
        {
            Attendance[] eAttendances = Attendance.GetActive(eResource);
            foreach (Attendance AttendanceObj in eAttendances)
            {
                Schedule ScheduleObj = AttendanceObj.Schedule;
                Session SessionObj = ScheduleObj.Session;
                if (SessionObj.SessionTypeID == eSessionType.SessionTypeID)
                {
                    // If this is a Tutor, we have to log out all of the students in his session as well.
                    if (eResource.ResourceTypeID == ResourceTypeTutor.ResourceTypeID)
                    {
                        Resource[] ResourceObjArray = SessionObj.GetResourceInSession();
                        foreach (Resource CurrentResource in ResourceObjArray)
                        {
                            Attendance[] eAttendances2 = Attendance.GetActive(CurrentResource);
                            foreach (Attendance eAttendance in eAttendances2)
                            {
                                Schedule CurrentResourceScheduleObj = eAttendance.Schedule;
                                Session CurrentResourceSessionObj = CurrentResourceScheduleObj.Session;
                                if (CurrentResourceSessionObj.SessionTypeID == eSessionType.SessionTypeID)
                                {
                                    eAttendance.LogOut(ResourceTypeTutor, ResourceTypeStudent);
                                }
                            }
                        }
                    }
                    else // If it's not a tutor, just log this user out.
                    {
                        AttendanceObj.LogOut(ResourceTypeTutor, ResourceTypeStudent);
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // On the initial loading of the page, check for room assignment, load the grids and focus on the login box
            if (!Page.IsPostBack)
            {
                // Populate the grids/lists, and give the login box focus!
                //UpdateButtons();
                UpdateDateTimeDisplay();
                PopulateGrids();
                //txtLogin.Focus();
                //lblError.Text = "";
                hfPageToggle.Value = "false";

                //txtLogin.Attributes.Add("onKeyPress", "doClick('" + btnLogin.ClientID + "',event)");
            }

            PrepareTimer();
        }

        private void PrepareTimer()
        {
            int UpdateInterval = TutoringSyncInterval;
            timerUpdatePage.Enabled = true; // Always true, even if disabled.  (The page only updates if it's enabled though.)
            UpdateInterval = 30; // The page should not update, but should still check for global settings changes.
            timerUpdatePage.Interval = UpdateInterval * 1000;
            UpdateDateTimeDisplay();
        }

        private void UpdateDateTimeDisplay()
        {
            // Set the clock to the current time.
            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            lblCurrentDate.Text = DateTime.Now.ToShortDateString();
            //lblSyncTime.Text = "Last Refreshed: " + DateTime.Now.ToLongTimeString();
            //upTime.Update(); // Don't call this when the UpdatePanel's UpdateMode property is UpdateMode="Always"
        }

        protected void timerUpdatePage_Tick(object sender, EventArgs e)
        {
            UpdateDateTimeDisplay();
            PopulateGrids();
        }

        protected void gvActive_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void gvActive_Sorting(object sender, GridViewSortEventArgs e)
        {
            List<GVActive> currentActive = GetActiveDatasource();
            switch (e.SortExpression)
            {
                case "TutorName":

                    // Sort the list of tutoring sessions by the tutor's last name
                    //currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Tutor.ResourceNameLast.CompareTo(y.Tutor.ResourceNameLast); });
                    //currentActive.OrderBy(obj => obj.Tutor.ResourceNameLast);//.ThenBy(obj => obj.Tutor.ResourceNameFirst);
                    currentActive.Sort(delegate(GVActive x, GVActive y) { return x.TutorName.ToString().CompareTo(y.TutorName.ToString()); });
                    gvActive.DataSource = currentActive;
                    gvActive.DataBind();
                    break;

                case "StudentName":

                    // Sort the list of tutoring sessions by the student's last name
                    //currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Student.ResourceNameLast.CompareTo(y.Student.ResourceNameLast); });
                    //currentActive.OrderBy(obj => obj.Student.ResourceNameLast);//.ThenBy(obj => obj.Student.ResourceNameFirst);
                    currentActive.Sort(delegate(GVActive x, GVActive y) { return x.StudentName.ToString().CompareTo(y.StudentName.ToString()); });
                    gvActive.DataSource = currentActive;
                    gvActive.DataBind();
                    break;

                case "Room":

                    //currentActive.OrderBy(obj => obj.Room);
                    currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Room.ToString().CompareTo(y.Room.ToString()); });
                    gvActive.DataSource = currentActive;
                    gvActive.DataBind();
                    break;
            }
        }

        protected void gvMissing_Sorting(object sender, GridViewSortEventArgs e)
        {
            List<GVAbsent> currentMissing = GetMissingDatasource();
            switch (e.SortExpression)
            {
                case "Present":

                    //currentMissing.OrderBy(obj => obj.Present);
                    // Sort the list of tutoring sessions by the tutor's last name
                    currentMissing.Sort(delegate(GVAbsent x, GVAbsent y) { return x.Present.CompareTo(y.Present); });
                    gvMissing.DataSource = currentMissing;
                    gvMissing.DataBind();
                    break;

                case "Subject":

                    //currentMissing.OrderBy(obj => obj.Subject);
                    // Sort the list of tutoring sessions by the tutor's last name
                    currentMissing.Sort(delegate(GVAbsent x, GVAbsent y) { return x.Subject.CompareTo(y.Subject); });
                    gvMissing.DataSource = currentMissing;
                    gvMissing.DataBind();
                    break;

                case "Missing":

                    //currentMissing.OrderBy(obj => obj.Subject);
                    // Sort the list of tutoring sessions by the tutor's last name
                    currentMissing.Sort(delegate(GVAbsent x, GVAbsent y) { return x.Missing.CompareTo(y.Missing); });
                    gvMissing.DataSource = currentMissing;
                    gvMissing.DataBind();
                    break;
            }
        }

        protected void gvMissing_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            List<GVAbsent> currentMissing = GetMissingDatasource();
            //rfdLogin.Enabled = false;
            switch (e.CommandName)
            {
                case "SortMissing":

                    //currentMissing.OrderBy(obj => obj.Missing);
                    currentMissing.Sort(delegate(GVAbsent x, GVAbsent y) { return x.Missing.CompareTo(y.Missing); });
                    gvMissing.DataSource = currentMissing;
                    gvMissing.DataBind();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Cancel logging in, just reload page
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ForceReloadPage();
        }

       
    }
}