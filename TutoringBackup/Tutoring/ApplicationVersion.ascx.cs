﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Tutoring
{
    public partial class ApplicationVersion : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String ApplicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersionApp.Text = ApplicationVersion;
        }
    }
}