﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace UK.STATS.STATSImport
{
    public class CSVDocument
    {
        private String[] _columnHeader;
        private String _filePath;
        private Char _delimiter;
        private Boolean _isFirstRowData;
        private StreamReader _fileReader;
        private List<List<String>> _fileData;

        public CSVDocument(String Path)
        {
            _filePath = Path;
            _delimiter = ',';
            _isFirstRowData = false;
            CommonInstantiationSequence();
        }
        public CSVDocument(String Path, Char Delimiter)
        {
            _filePath = Path;
            _delimiter = Delimiter;
            _isFirstRowData = false;
            CommonInstantiationSequence();
        }
        public CSVDocument(String Path, Char Delimiter, String[] ColumnHeaderNames)
        {
            _filePath = Path;
            _delimiter = Delimiter;
            _columnHeader = ColumnHeaderNames;
            _isFirstRowData = true;
            CommonInstantiationSequence();
        }

        public String[] ColumnNames
        {
            get { return _columnHeader; }
            set { _columnHeader = value; }
        }
        public String FilePath { get { return _filePath; } }
        public Char Delimiter { get { return _delimiter; } }
        public Boolean FirstRowContainsData { get { return _isFirstRowData; } }

        private void CommonInstantiationSequence()
        {
            if (!File.Exists(_filePath))
            {
                throw new FileNotFoundException("An attempt to open the file was prevented because the file does not exist.", _filePath);
            }

            _fileReader = PrepareReader(_filePath);

            if (_isFirstRowData)
            {
                String[] _line = ReadColumnNamesFromFile(_fileReader);
                for (int i = 0; i < _line.Length; i++)
                {
                    _columnHeader[i] = "Column" + (i + 1).ToString();
                }
            }
            else
            {
                _columnHeader = ReadColumnNamesFromFile(_fileReader);
            }

            _fileReader = ResetFileReader(_fileReader);
            _fileData = ReadData(_fileReader, _delimiter);
            _fileReader.Close();
            _fileReader.Dispose();

            //System.Xml.Serialization.XmlSerializer _serializer = new System.Xml.Serialization.XmlSerializer(_fileData.GetType());
            //_serializer.Serialize(new StreamWriter("C:\\Serialized.xml"), _fileData);
        }

        private StreamReader PrepareReader(String Filepath)
        {
            return new StreamReader(FilePath);
        }
        private StreamReader ResetFileReader(StreamReader FileReader)
        {
            // Modifying the underlying stream of an inherited stream type is generally a bad idea.
            // However, if you do decide to modify it, ensure that there are no unintended side-effects.
            // In the case of moving the position within the file, you must discard the data in the buffer.
            FileReader.BaseStream.Seek(0, SeekOrigin.Begin);
            FileReader.DiscardBufferedData();
            return FileReader;
        }
        private String[] ReadColumnNamesFromFile(StreamReader FileReader)
        {
            String _firstLine = FileReader.ReadLine();
            return SplitStringByDelimiter(_firstLine, _delimiter, '\\');
        }
        private String[] SplitStringByDelimiter(String Source, Char Delimiter, Char Escape)
        {
            List<String> _split = new List<String>();
            String _currentField = "";
            bool _inQuotes = false;
            for (int i = 0; i < Source.Length; i++)
            {
                char _previousChar;
                char _currentChar = Source[i];
                if (i < 1) { _previousChar = ' '; } else { _previousChar = Source[i - 1]; }

                if (_inQuotes)
                {
                    if (_currentChar == '"' && _previousChar != '\\')
                    {
                        //_split.Add(_currentField);
                        _inQuotes = false;
                    }
                    else
                    {
                        _currentField += _currentChar;
                    }
                }
                else
                {
                    if (_currentChar == '"' && _previousChar != '\\')
                    {
                        _inQuotes = true;
                    }
                    else if (_currentChar == ',')
                    {
                        _split.Add(_currentField);
                        _currentField = "";
                    }
                    else
                    {
                        _currentField += _currentChar;
                    }
                }
                _previousChar = _currentChar;
            }
            _split.Add(_currentField); // To add the last entry.
            return _split.ToArray();
        }
        private List<List<String>> ReadData(StreamReader FileReader, Char Delimiter)
        {
            List<List<String>> _contents = new List<List<string>>();
            int _lineCounter = 1;
            while (!FileReader.EndOfStream)
            {
                String[] _fields = SplitStringByDelimiter(FileReader.ReadLine(), Delimiter, '\\');
                int _lineFieldCount = _fields.Length;
                int _fileFieldCount = _columnHeader.Length;

                if (_lineFieldCount != _fileFieldCount)
                {
                    throw new MissingFieldException("The number of values per line has changed. (Line " + _lineCounter.ToString() + ")");
                }

                List<String> _record = new List<string>(_fileFieldCount);
                for (int i = 0; i < _fields.Length; i++)
                {
                    _record.Add(_fields[i]);
                }

                _contents.Add(_record);

                _lineCounter++;
            }
            return _contents;
        }

        public int Length
        {
            get { return _fileData.Count(); }
        }

        public Dictionary<String, String> GetRowAt(int RowNumber)
        {
            Dictionary<String, String> _row = new Dictionary<String, String>();
            for (int i = 0; i < _columnHeader.Length; i++)
            {
                _row.Add(_columnHeader[i], _fileData[RowNumber][i]);
            }
            return _row;
        }

        public List<String> GetRowValuesAsList(int RowNumber)
        {
            List<String> _row = new List<String>(_columnHeader.Length);
            for (int i = 0; i < _columnHeader.Length; i++)
            {
                _row.Add(_fileData[RowNumber][i]);
            }
            return _row;
        }

    }
}
