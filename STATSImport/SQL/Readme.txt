In this directory, there are CSV files that the client provided to us for loading data into the database.

The file with "Missing" in the name is the one that they provided.  
The file with "Fixed" in the name has the same data.

I have added additional columns to the "Fixed" file.

The added columns are blank, allowing it to work with the STATSImport application.

In addition to this document, refer to the Documentation for this application as well.