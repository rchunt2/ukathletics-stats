USE [UKSTATS]

DELETE FROM [Authentication]
DELETE FROM [Error]
DELETE FROM [Annotation]
DELETE FROM [MessageInbox]
DELETE FROM [Message]
DELETE FROM [MessageContent]
DELETE FROM [FreeTime]
DELETE FROM [Attendance]
DELETE FROM [Schedule]
DELETE FROM [SessionTutor]
DELETE FROM [Session]
DELETE FROM [ResourceRegistration]
DELETE FROM [CourseSection]
DELETE FROM [Instructor]
DELETE FROM [CourseTutor]
DELETE FROM [Course]
DELETE FROM [Subject]
DELETE FROM [GroupMember] WHERE ResourceID IN (SELECT ResourceID FROM [Resource] WHERE ResourceIsActive = 0)
DELETE FROM [Resource] WHERE ResourceIsActive = 0

/*
	Retained: 
		Carrier
		Group
		GroupMember
		MessageType
		OperationRange
		Resource
		ResourceType
		Room
		SessionType
		Setting

	Cleared: 
		Authentication
		Error
		Annotation
		MessageInbox
		Message
		MessageContent
		FreeTime
		Attendance
		Schedule
		SessionTutor
		Session
		ResourceRegistration
		CourseSection
		Instructor
		CourseTutor
		Course
		Subject
*/
