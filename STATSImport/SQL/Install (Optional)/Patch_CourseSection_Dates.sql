
USE [UKSTATS];
GO

DECLARE @DateStart datetime;
DECLARE @DateEnd datetime;
SET @DateStart = (SELECT TOP(1) SettingValue FROM Setting WHERE SettingKey = 'SemesterDateBegin');
SET @DateEnd = (SELECT TOP(1) SettingValue FROM Setting WHERE SettingKey = 'SemesterDateEnd');

UPDATE CourseSection SET CourseSection.CourseSectionDateStart = @DateStart, CourseSection.CourseSectionDateEnd = @DateEnd