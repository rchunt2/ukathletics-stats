USE [UKSTATS]

INSERT INTO Resource 
(
	ResourceTypeID, 
	ResourceLogin, 
	ResourcePassword, 
	ResourceNameFirst, 
	ResourceNameLast, 
	ResourcePhoneNumber, 
	ResourceEmail, 
	CarrierID, 
	ResourceTextingEnabled
)
VALUES
(
	/* This takes into consideration the spelling error in the existing application.  Just being safe, I suppose. */
	(SELECT TOP(1) ResourceTypeID FROM ResourceType WHERE ResourceTypeName = 'Administrator' OR  ResourceTypeName = 'Adminstrator'), 
	'Admin', 
	'Password@001', 
	'DefaultFirst', 
	'DefaultLast', 
	'', 
	'', 
	NULL, 
	0
)