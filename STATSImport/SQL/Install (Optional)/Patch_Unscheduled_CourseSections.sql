USE [UKSTATS]

/*

Note: This script is actually optional, but is recommended, nonetheless.

Purpose:
	This script takes any Course Section that does not have 
	an explicit meeting time defined, and updates it so that 
	it will appear on Resource calendars as taking place
	on Saturday morning from 8AM-9AM.
	
	Relatively speaking, the time can be modified in the 
	following script before being executed on the live data,
	but as this could cause potential issues on down the line
	it is not recommended from a maintenance point-of-view.

*/

UPDATE 
	CourseSection
SET 
	CourseSectionDaysMeet = 'S',
	CourseSectionTimeStart = '08:00:00',
	CourseSectionTimeEnd = '09:00:00'
WHERE 
	CourseSectionDaysMeet = ''
	AND
	CourseSectionTimeStart = '00:00:00'
	AND
	CourseSectionTimeEnd = '00:00:00'