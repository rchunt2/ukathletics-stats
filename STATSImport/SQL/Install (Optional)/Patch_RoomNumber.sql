﻿USE [UKSTATS]
GO

/*

	Note: This is optional.  
	Run this script ONLY if you DO NOT want room numbers to be prefixed with "Rm."

	Purpose: Searches the CourseSections table, and removes any instance of the string "Rm." from the room number field.

*/


UPDATE CourseSection SET CourseSection.CourseSectionRoom = REPLACE(CourseSection.CourseSectionRoom, 'Rm.', '')