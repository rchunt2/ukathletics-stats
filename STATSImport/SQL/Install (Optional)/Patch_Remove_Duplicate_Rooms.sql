  ALTER TABLE [UKSTATS].[dbo].[Room]
    DROP COLUMN [autoid]
  ;
  ALTER TABLE [UKSTATS].[dbo].[Room]
    ADD [autoid] INT IDENTITY(1,1)
  ;
  SELECT * FROM [UKSTATS].[dbo].[Room] WHERE [autoid] NOT IN (SELECT MIN([autoid]) _
	FROM [UKSTATS].[dbo].[Room] GROUP BY [RoomName],[RoomPriority],[RoomCapacity])
  ;
  DELETE FROM [UKSTATS].[dbo].[Room] WHERE [autoid] NOT IN (SELECT MIN([autoid]) _
	FROM [UKSTATS].[dbo].[Room] GROUP BY [RoomName],[RoomPriority],[RoomCapacity])
  ;
  --SELECT * FROM [UKSTATS_TEST].[dbo].[Room]
  --ORDER BY [RoomName] DESC