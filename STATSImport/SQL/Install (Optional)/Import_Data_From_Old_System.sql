USE [UKSTATS]

/* Import Resources */

INSERT INTO [Resource] (ResourceTypeID, ResourceLogin, ResourcePassword, ResourceNameFirst, ResourceNameLast, ResourcePhoneNumber, ResourceEmail, CarrierID, ResourceTextingEnabled) 
SELECT
	(SELECT ResourceTypeID FROM ResourceType WHERE ResourceTypeName = Resource_Types.Resource_Description) AS ResourceTypeID,
	LTRIM(RTRIM(Resources.ResourceID)) AS ResourceLogin,
	LTRIM(RTRIM(Resources.Resource_Password)) AS ResourcePassword,
	LTRIM(RTRIM(Resources.Resource_Name_First)) AS ResourceNameFirst,
	LTRIM(RTRIM(Resources.Resource_Name_Last)) AS ResourceNameLast,
	REPLACE(LTRIM(RTRIM(Resources.PhoneNumber)), '-', '') AS ResourcePhoneNumber,
	LTRIM(RTRIM(Resources.Comments)) AS ResourceEmail,
	'00000000-0000-0000-0000-000000000000' AS CarrierID,
	0 AS ResourceTextingEnabled
FROM UK2.UK.Resources
LEFT JOIN UK2.UK.Resource_Types ON Resources.Resource_Type = Resource_Types.Resource_Type
WHERE Resources.Resource_Type <= 5


/* Import Rooms */

INSERT INTO Room (RoomName, RoomPriority, RoomCapacity)
SELECT 
	Room_ID AS RoomName, 
	Room_Priority AS RoomPriority, 
	Room_Capacity AS RoomCapacity
FROM UK2.UK.Rooms

