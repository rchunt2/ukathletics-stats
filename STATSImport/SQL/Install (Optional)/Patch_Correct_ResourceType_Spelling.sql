USE [UKSTATS]

/* 
	NOTE: 
	Strangely enough, in the original database for the STATS application,
	there was a spelling error in the Resource_Types table.  Normally this
	wouldn't cause a problem, but since we had to provide a supported
	upgrade path from the old schema to the new schema, this file 
	attempts to correct that little issue.
*/

UPDATE ResourceType SET ResourceTypeName = 'Administrator' WHERE ResourceTypeName = 'Adminstrator'