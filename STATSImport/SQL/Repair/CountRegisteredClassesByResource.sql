USE [UKSTATS]

SELECT 
	Resource.ResourceID
	,Resource.ResourceNameFirst
	,Resource.ResourceNameLast
	,Resource.ResourceLogin
	,COUNT(ResourceRegistration.ResourceRegistrationID) AS RegisteredClasses

FROM Resource 
LEFT JOIN ResourceRegistration ON ResourceRegistration.ResourceID = Resource.ResourceID
GROUP BY 
	Resource.ResourceID
	,Resource.ResourceNameFirst
	,Resource.ResourceNameLast
	,Resource.ResourceLogin
	
ORDER BY COUNT(ResourceRegistration.ResourceRegistrationID) DESC