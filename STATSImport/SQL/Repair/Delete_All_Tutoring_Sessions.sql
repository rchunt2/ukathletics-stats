﻿USE [UKSTATS]

-- Operate only on Tutoring sessions.
DECLARE @SessionTypeName varchar(MAX);
SET @SessionTypeName = 'Tutoring';

-- Retrieve the Session Type ID for a Tutoring Session.
DECLARE @SessionTypeID uniqueidentifier;
SET @SessionTypeID = (SELECT SessionType.SessionTypeID FROM SessionType WHERE SessionType.SessionTypeName = @SessionTypeName)

-- In order to delete the records in the [Session] table, we must first delete any dependencies.

-- Dependency: Attendance 
-- The records in the attendance table signify when a Resource is present for a Session.
DELETE FROM Attendance WHERE Attendance.AttendanceID IN 
(
	SELECT Attendance.AttendanceID FROM Attendance WHERE Attendance.ScheduleID IN 
	(
		SELECT Schedule.ScheduleID 
		FROM Schedule 
		WHERE Schedule.SessionID IN	
		(
			SELECT Session.SessionID 
			FROM Session 
			WHERE Session.SessionTypeID = @SessionTypeID
		)
	)
)

-- Dependency: Schedule 
-- The schedule table links together Resources and Sessions.
DELETE FROM Schedule WHERE Schedule.ScheduleID IN
(
	SELECT Schedule.ScheduleID 
	FROM Schedule 
	WHERE Schedule.SessionID IN	
	(
		SELECT Session.SessionID 
		FROM Session 
		WHERE Session.SessionTypeID = @SessionTypeID
	)
)


-- Dependency: SessionTutor
-- The SessionTutor table stores additional metadata for Tutoring sessions.
DELETE FROM SessionTutor WHERE SessionTutor.SessionTutorID IN 
(
	SELECT SessionTutor.SessionTutorID
	FROM SessionTutor
	LEFT JOIN Session ON SessionTutor.SessionID = Session.SessionID
	WHERE Session.SessionTypeID = @SessionTypeID
)


-- Now that the dependencies have been removed, we can remove the Session records for them.
DELETE FROM Session WHERE Session.SessionTypeID = @SessionTypeID