USE [UKSTATS]

SELECT 
	Resource.ResourceID
	,Resource.ResourceNameFirst
	,Resource.ResourceNameLast
	,Resource.ResourceLogin
	,COUNT(Schedule.ScheduleID) AS ScheduleRecords

FROM Resource 
LEFT JOIN Schedule ON Schedule.ResourceID = Resource.ResourceID
GROUP BY 
	Resource.ResourceID
	,Resource.ResourceNameFirst
	,Resource.ResourceNameLast
	,Resource.ResourceLogin
	
ORDER BY COUNT(Schedule.ScheduleID) ASC