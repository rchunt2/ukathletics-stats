USE [UKSTATS]

/* INSERT Subjects */

INSERT INTO Subject (SubjectNameShort, SubjectName, SubjectDescription)
SELECT
	USC3.SubjectNameShort AS SubjectNameShort,
	USC3.SubjectNameShort AS SubjectName,
	'' AS SubjectDescription
FROM
(
	SELECT
		DISTINCT(USC.SubjectNameShort)
	FROM
	(
		SELECT
			LTRIM(RTRIM(SUBSTRING(MA, 1, CHARINDEX(' ', MA)))) AS SubjectNameShort
		FROM
		(
			SELECT
				(SELECT TOP(1) ModuleAbbr FROM SAPImport WHERE SAPImport.ModuleID = t1.MID) AS MA
			FROM
				(SELECT DISTINCT(ModuleID) AS MID FROM SAPImport GROUP BY ModuleID) AS t1
		) AS USC2
	) AS USC
	WHERE 
		SubjectNameShort IS NOT NULL
	AND
		NOT SubjectNameShort = ''
) AS USC3
ORDER BY USC3.SubjectNameShort



/* INSERT Courses */

INSERT INTO Course (SubjectID, CourseName, CourseNumber, CourseModuleID)
SELECT
	(SELECT SubjectID FROM Subject WHERE USC.SubjectNameShort = Subject.SubjectNameShort) AS SubjectID,
	USC.CourseName,
	USC.CourseNumber,
	USC.MID AS CourseModuleID
FROM
(
	SELECT
		LTRIM(RTRIM(SUBSTRING(MA, 1, CHARINDEX(' ', MA)))) AS SubjectNameShort,
		LTRIM(RTRIM(SUBSTRING(MA, CHARINDEX(' ', MA), 99))) AS CourseNumber,
		MD AS CourseName,
		MID		
	FROM
	(
		SELECT
			(SELECT TOP(1) ModuleAbbr FROM SAPImport WHERE SAPImport.ModuleID = t1.MID) AS MA,
			(SELECT TOP(1) ModuleDesc FROM SAPImport WHERE SAPImport.ModuleID = t1.MID) AS MD,
			MID
		FROM
			(SELECT DISTINCT(ModuleID) AS MID FROM SAPImport GROUP BY ModuleID) AS t1
	) AS USC2
) AS USC
WHERE 
	USC.SubjectNameShort IS NOT NULL
AND
	NOT USC.SubjectNameShort = ''
	
ORDER BY USC.SubjectNameShort, USC.CourseNumber

/* INSERT Instructors */

INSERT INTO Instructor (InstructorName)
	SELECT
		DISTINCT(BusinessEventInstructorDescription)
	FROM SAPImport
		WHERE BusinessEventInstructorDescription IS NOT NULL AND NOT BusinessEventInstructorDescription = ''
	ORDER BY BusinessEventInstructorDescription


/* INSERT Course Sections */

INSERT INTO CourseSection (
CourseID, 
InstructorID, 
CourseSectionDateStart, 
CourseSectionDateEnd, 
CourseSectionDaysMeet, 
CourseSectionTimeStart, 
CourseSectionTimeEnd, 
CourseSectionNumber,
CourseSectionPackageID, 
CourseSectionBuilding, 
CourseSectionRoom, 
CourseSectionEventType)

SELECT 
	(SELECT CourseID FROM Course WHERE Course.CourseModuleID = ModuleID) AS CourseID
	,(SELECT InstructorID FROM Instructor WHERE InstructorName = BusinessEventInstructorDescription) AS InstructorID
	,CAST(StartDate AS Date) AS CourseSectionStartDate
	,CAST(EndDate AS Date) AS CourseSectionEndDate
	,(REPLACE(Sunday, 'X', 'S') + REPLACE(Monday, 'X', 'M') + REPLACE(Tuesday, 'X', 'T') + REPLACE(Wednesday, 'X', 'W') + REPLACE(Thursday, 'X', 'R') + REPLACE(Friday, 'X', 'F') + REPLACE(Saturday, 'X', 'S')) AS CourseSectionDaysMeet
	,CAST(StartTime AS Time(0)) AS CourseSectionStartTime
	,CAST(EndTime AS Time(0)) AS CourseSectionEndTime
	,LTRIM(RTRIM(REPLACE(EventPackageAbbr, 'Section ', ''))) AS SectionNumber
	,EventPackageID AS CourseSectionPackageID
	,Building
	,RoomShortName
	,BusinessEventAbbr
FROM SAPImport
WHERE InCatalog = 'X'





