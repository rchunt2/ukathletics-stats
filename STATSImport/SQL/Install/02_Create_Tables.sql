USE [UKSTATS]
GO
/****** Object:  Table [dbo].[Annotation]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Annotation](
	[AnnotationID] [uniqueidentifier] NOT NULL,
	[SourceID] [uniqueidentifier] NOT NULL,
	[AnnotationText] [text] NOT NULL,
 CONSTRAINT [PK_Annotation] PRIMARY KEY CLUSTERED 
(
	[AnnotationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TutorSkillsBackup]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TutorSkillsBackup](
	[TutorSkillsBackupID] [uniqueidentifier] NOT NULL,
	[ResourceLogin] [varchar](32) NOT NULL,
	[ResourceNameFirst] [varchar](32) NOT NULL,
	[ResourceNameLast] [varchar](32) NOT NULL,
	[SubjectNameShort] [varchar](8) NOT NULL,
	[SubjectName] [varchar](64) NOT NULL,
	[CourseName] [varchar](40) NOT NULL,
	[CourseNumber] [varchar](8) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subject](
	[SubjectID] [uniqueidentifier] NOT NULL,
	[SubjectNameShort] [varchar](8) NOT NULL,
	[SubjectName] [varchar](64) NOT NULL,
	[SubjectDescription] [text] NOT NULL,
 CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Setting](
	[SettingID] [uniqueidentifier] NOT NULL,
	[SettingKey] [varchar](64) NOT NULL,
	[SettingValue] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SessionType]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SessionType](
	[SessionTypeID] [uniqueidentifier] NOT NULL,
	[SessionTypeName] [varchar](32) NOT NULL,
	[SessionTypeDescription] [text] NOT NULL,
 CONSTRAINT [PK_SessionType] PRIMARY KEY CLUSTERED 
(
	[SessionTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SAPImport]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SAPImport](
	[ModuleID] [nvarchar](128) NULL,
	[ModuleAbbr] [nvarchar](128) NULL,
	[EventPackageID] [nvarchar](128) NULL,
	[EventPackageAbbr] [nvarchar](128) NULL,
	[EventPackageDescription] [nvarchar](128) NULL,
	[BusinessEventType] [nvarchar](128) NULL,
	[AcademicEventOT] [nvarchar](128) NULL,
	[ObjectID] [nvarchar](128) NULL,
	[BusinessEventAbbr] [nvarchar](128) NULL,
	[NameOfTheBusinessEvent] [nvarchar](128) NULL,
	[StartDate] [nvarchar](128) NULL,
	[EndDate] [nvarchar](128) NULL,
	[Monday] [nvarchar](128) NULL,
	[Tuesday] [nvarchar](128) NULL,
	[Wednesday] [nvarchar](128) NULL,
	[Thursday] [nvarchar](128) NULL,
	[Friday] [nvarchar](128) NULL,
	[Saturday] [nvarchar](128) NULL,
	[Sunday] [nvarchar](128) NULL,
	[StartTime] [nvarchar](128) NULL,
	[EndTime] [nvarchar](128) NULL,
	[Building] [nvarchar](128) NULL,
	[ObjectName] [nvarchar](128) NULL,
	[RoomShortName] [nvarchar](128) NULL,
	[RoomName] [nvarchar](128) NULL,
	[BusinessEventInstructorDescription] [nvarchar](128) NULL,
	[MaximumCapacity] [nvarchar](128) NULL,
	[OptimumCapacity] [nvarchar](128) NULL,
	[EventPackageLocationAbbr] [nvarchar](128) NULL,
	[EventPackageLocationName] [nvarchar](128) NULL,
	[MaxEventCapacity] [nvarchar](128) NULL,
	[OptimalEventCapacity] [nvarchar](128) NULL,
	[Location] [nvarchar](128) NULL,
	[Location2] [nvarchar](128) NULL,
	[MaximumRoomCapacity] [nvarchar](128) NULL,
	[OptimumRoomCapacity] [nvarchar](128) NULL,
	[NumberStudentsEnrolled] [nvarchar](128) NULL,
	[NumberGradedStudents] [nvarchar](128) NULL,
	[WaitingListPlaces] [nvarchar](128) NULL,
	[WaitListBooked] [nvarchar](128) NULL,
	[StdPctForWLPlaces] [nvarchar](128) NULL,
	[MaximumCredits] [nvarchar](128) NULL,
	[WaitListDisabled] [nvarchar](128) NULL,
	[AvailableForWebRegistration] [nvarchar](128) NULL,
	[InCatalog] [nvarchar](128) NULL,
	[WLManualMoveUp] [nvarchar](128) NULL,
	[Location3] [nvarchar](128) NULL,
	[BusinessEventInstructorAbbr] [nvarchar](128) NULL,
	[RoomObjectID] [nvarchar](128) NULL,
	[ModuleDesc] [nvarchar](128) NULL,
	[Blank1] [nvarchar](128) NULL,
	[Blank2] [nvarchar](128) NULL,
	[Blank3] [nvarchar](128) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Room](
	[RoomID] [uniqueidentifier] NOT NULL,
	[RoomName] [varchar](16) NOT NULL,
	[RoomPriority] [int] NOT NULL,
	[RoomCapacity] [int] NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ResourceType]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResourceType](
	[ResourceTypeID] [uniqueidentifier] NOT NULL,
	[ResourceTypeName] [varchar](32) NOT NULL,
	[ResourceTypeDescription] [text] NOT NULL,
 CONSTRAINT [PK_ResourceType] PRIMARY KEY CLUSTERED 
(
	[ResourceTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group](
	[GroupID] [uniqueidentifier] NOT NULL,
	[GroupName] [varchar](32) NOT NULL,
	[GroupDescription] [text] NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Carrier]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carrier](
	[CarrierID] [uniqueidentifier] NOT NULL,
	[CarrierName] [varchar](32) NOT NULL,
	[CarrierEmailSuffix] [varchar](32) NOT NULL,
 CONSTRAINT [PK_Carrier] PRIMARY KEY CLUSTERED 
(
	[CarrierID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MessageType]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageType](
	[MessageTypeID] [uniqueidentifier] NOT NULL,
	[MessageTypeName] [varchar](32) NOT NULL,
	[MessageTypeDescription] [text] NOT NULL,
 CONSTRAINT [PK_MessageType] PRIMARY KEY CLUSTERED 
(
	[MessageTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Instructor]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Instructor](
	[InstructorID] [uniqueidentifier] NOT NULL,
	[InstructorName] [varchar](40) NOT NULL,
	[InstructorEmail] [varchar](128) NULL,
	[InstructorPhone] [varchar](16) NULL,
 CONSTRAINT [PK_Instructor] PRIMARY KEY CLUSTERED 
(
	[InstructorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MessageContent]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageContent](
	[MessageContentID] [uniqueidentifier] NOT NULL,
	[MessageContentSubject] [varchar](128) NOT NULL,
	[MessageContentBody] [text] NOT NULL,
 CONSTRAINT [PK_MessageContent] PRIMARY KEY CLUSTERED 
(
	[MessageContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Resource]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Resource](
	[ResourceID] [uniqueidentifier] NOT NULL,
	[ResourceTypeID] [uniqueidentifier] NOT NULL,
	[ResourceLogin] [varchar](32) NOT NULL,
	[ResourcePassword] [varchar](32) NOT NULL,
	[ResourceNameFirst] [varchar](32) NOT NULL,
	[ResourceNameLast] [varchar](32) NOT NULL,
	[ResourcePhoneNumber] [varchar](16) NULL,
	[ResourceEmail] [varchar](128) NULL,
	[CarrierID] [uniqueidentifier] NULL,
	[ResourceTextingEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperationRange]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OperationRange](
	[OperationRangeID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[SessionTypeID] [uniqueidentifier] NOT NULL,
	[OperationRangeDayOfWeek] [char](3) NOT NULL,
	[OperationRangeTimeStart] [time](0) NOT NULL,
	[OperationRangeTimeEnd] [time](0) NOT NULL,
 CONSTRAINT [PK_OperationRange] PRIMARY KEY CLUSTERED 
(
	[OperationRangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Course]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Course](
	[CourseID] [uniqueidentifier] NOT NULL,
	[SubjectID] [uniqueidentifier] NOT NULL,
	[CourseName] [varchar](40) NOT NULL,
	[CourseNumber] [varchar](8) NOT NULL,
	[CourseModuleID] [varchar](16) NOT NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FreeTime]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeTime](
	[FreeTimeID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[SessionTypeID] [uniqueidentifier] NOT NULL,
	[FreeTimeApplyDate] [datetime] NOT NULL,
	[FreeTimeAmount] [int] NOT NULL,
	[FreeTimeAddDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FreeTime] PRIMARY KEY CLUSTERED 
(
	[FreeTimeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Error]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Error](
	[ErrorID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[ResourceID] [uniqueidentifier] NULL,
	[ErrorDateTime] [datetime] NOT NULL,
	[ErrorText] [text] NOT NULL,
	[ErrorIPAddress] [varchar](15) NOT NULL,
 CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseTutor]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseTutor](
	[CourseTutorID] [uniqueidentifier] NOT NULL,
	[CourseID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_CourseTutor] PRIMARY KEY CLUSTERED 
(
	[CourseTutorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CourseSection]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CourseSection](
	[CourseSectionID] [uniqueidentifier] NOT NULL,
	[CourseID] [uniqueidentifier] NOT NULL,
	[InstructorID] [uniqueidentifier] NULL,
	[CourseSectionDateStart] [date] NOT NULL,
	[CourseSectionDateEnd] [date] NOT NULL,
	[CourseSectionDaysMeet] [varchar](7) NOT NULL,
	[CourseSectionTimeStart] [time](0) NOT NULL,
	[CourseSectionTimeEnd] [time](0) NOT NULL,
	[CourseSectionNumber] [varchar](8) NOT NULL,
	[CourseSectionPackageID] [varchar](16) NOT NULL,
	[CourseSectionBuilding] [varchar](32) NOT NULL,
	[CourseSectionRoom] [varchar](32) NOT NULL,
	[CourseSectionEventType] [varchar](4) NOT NULL,
 CONSTRAINT [PK_CourseSection] PRIMARY KEY CLUSTERED 
(
	[CourseSectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Authentication]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authentication](
	[AuthenticationID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[AuthenticationExpire] [datetime] NOT NULL,
 CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED 
(
	[AuthenticationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Message]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[MessageID] [uniqueidentifier] NOT NULL,
	[MessageTypeID] [uniqueidentifier] NOT NULL,
	[MessageContentID] [uniqueidentifier] NOT NULL,
	[MessageFromResourceID] [uniqueidentifier] NOT NULL,
	[MessageToResourceID] [uniqueidentifier] NULL,
	[MessageToGroupID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupMember]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupMember](
	[GroupMemberID] [uniqueidentifier] NOT NULL,
	[GroupID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_GroupMember] PRIMARY KEY CLUSTERED 
(
	[GroupMemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MessageInbox]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageInbox](
	[MessageInboxID] [uniqueidentifier] NOT NULL,
	[MessageID] [uniqueidentifier] NOT NULL,
	[MessageInboxDateRead] [datetime] NULL,
	[MessageInboxDateExpires] [datetime] NULL,
	[MessageInboxDateDeleted] [datetime] NULL,
	[MessageInboxDateSent] [datetime] NOT NULL,
 CONSTRAINT [PK_MessageInbox] PRIMARY KEY CLUSTERED 
(
	[MessageInboxID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ResourceRegistration]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResourceRegistration](
	[ResourceRegistrationID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[CourseSectionID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ResourceRegistration] PRIMARY KEY CLUSTERED 
(
	[ResourceRegistrationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Session]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Session](
	[SessionID] [uniqueidentifier] NOT NULL,
	[CourseSectionID] [uniqueidentifier] NULL,
	[SessionTypeID] [uniqueidentifier] NOT NULL,
	[SessionDateTimeStart] [datetime] NOT NULL,
	[SessionDateTimeEnd] [datetime] NOT NULL,
	[SessionIsCancelled] [bit] NOT NULL,
	[SessionIsScheduled] [bit] NOT NULL,
 CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED 
(
	[SessionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SessionTutor]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionTutor](
	[SessionTutorID] [uniqueidentifier] NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[RoomID] [uniqueidentifier] NULL,
	[CourseID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SessionTutor] PRIMARY KEY CLUSTERED 
(
	[SessionTutorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[ScheduleID] [uniqueidentifier] NOT NULL,
	[ResourceID] [uniqueidentifier] NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[ScheduleRequireAttend] [bit] NOT NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 02/27/2012 13:07:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Attendance](
	[AttendanceID] [uniqueidentifier] NOT NULL,
	[ScheduleID] [uniqueidentifier] NOT NULL,
	[AttendanceTimeIn] [datetime] NOT NULL,
	[AttendanceTimeOut] [datetime] NULL,
	[AttendanceMachineName] [varchar](32) NULL,
	[AttendanceTimeOverride] [int] NULL,
 CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED 
(
	[AttendanceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Attendance] UNIQUE NONCLUSTERED 
(
	[AttendanceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_Carrier_CarrierID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Carrier] ADD  CONSTRAINT [DF_Carrier_CarrierID]  DEFAULT (newsequentialid()) FOR [CarrierID]
GO
/****** Object:  Default [DF_Course_CourseID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Course] ADD  CONSTRAINT [DF_Course_CourseID]  DEFAULT (newsequentialid()) FOR [CourseID]
GO
/****** Object:  Default [DF_CourseSection_CourseSectionID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[CourseSection] ADD  CONSTRAINT [DF_CourseSection_CourseSectionID]  DEFAULT (newsequentialid()) FOR [CourseSectionID]
GO
/****** Object:  Default [DF_Error_ErrorID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Error] ADD  CONSTRAINT [DF_Error_ErrorID]  DEFAULT (newsequentialid()) FOR [ErrorID]
GO
/****** Object:  Default [DF_Instructor_InstructorID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Instructor] ADD  CONSTRAINT [DF_Instructor_InstructorID]  DEFAULT (newsequentialid()) FOR [InstructorID]
GO
/****** Object:  Default [DF_MessageInbox_MessageInboxDateSent]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[MessageInbox] ADD  CONSTRAINT [DF_MessageInbox_MessageInboxDateSent]  DEFAULT (getdate()) FOR [MessageInboxDateSent]
GO
/****** Object:  Default [DF_MessageType_MessageTypeID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[MessageType] ADD  CONSTRAINT [DF_MessageType_MessageTypeID]  DEFAULT (newsequentialid()) FOR [MessageTypeID]
GO
/****** Object:  Default [DF_OperationRange_OperationRangeID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[OperationRange] ADD  CONSTRAINT [DF_OperationRange_OperationRangeID]  DEFAULT (newsequentialid()) FOR [OperationRangeID]
GO
/****** Object:  Default [DF_Resource_ResourceID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Resource] ADD  CONSTRAINT [DF_Resource_ResourceID]  DEFAULT (newsequentialid()) FOR [ResourceID]
GO
/****** Object:  Default [DF_Resource_ResourceTextingEnabled]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Resource] ADD  CONSTRAINT [DF_Resource_ResourceTextingEnabled]  DEFAULT ((0)) FOR [ResourceTextingEnabled]
GO
/****** Object:  Default [DF_ResourceType_ResourceTypeID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[ResourceType] ADD  CONSTRAINT [DF_ResourceType_ResourceTypeID]  DEFAULT (newsequentialid()) FOR [ResourceTypeID]
GO
/****** Object:  Default [DF_Room_RoomID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Room] ADD  CONSTRAINT [DF_Room_RoomID]  DEFAULT (newsequentialid()) FOR [RoomID]
GO
/****** Object:  Default [DF_SessionTutor_SessionTutorID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[SessionTutor] ADD  CONSTRAINT [DF_SessionTutor_SessionTutorID]  DEFAULT (newsequentialid()) FOR [SessionTutorID]
GO
/****** Object:  Default [DF_SessionType_SessionTypeID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[SessionType] ADD  CONSTRAINT [DF_SessionType_SessionTypeID]  DEFAULT (newsequentialid()) FOR [SessionTypeID]
GO
/****** Object:  Default [DF_Setting_SettingID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Setting] ADD  CONSTRAINT [DF_Setting_SettingID]  DEFAULT (newsequentialid()) FOR [SettingID]
GO
/****** Object:  Default [DF_Subject_SubjectID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Subject] ADD  CONSTRAINT [DF_Subject_SubjectID]  DEFAULT (newsequentialid()) FOR [SubjectID]
GO
/****** Object:  Default [DF_TutorSkillsBackup_TutorSkillsBackupID]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[TutorSkillsBackup] ADD  CONSTRAINT [DF_TutorSkillsBackup_TutorSkillsBackupID]  DEFAULT (newsequentialid()) FOR [TutorSkillsBackupID]
GO
/****** Object:  ForeignKey [FK_Schedule_Attendance]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Attendance] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Schedule_Attendance]
GO
/****** Object:  ForeignKey [FK_Authentication_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Authentication]  WITH CHECK ADD  CONSTRAINT [FK_Authentication_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Authentication] CHECK CONSTRAINT [FK_Authentication_Resource]
GO
/****** Object:  ForeignKey [FK_Course_Subject]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [FK_Course_Subject] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subject] ([SubjectID])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [FK_Course_Subject]
GO
/****** Object:  ForeignKey [FK_CourseSection_Course]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[CourseSection]  WITH CHECK ADD  CONSTRAINT [FK_CourseSection_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[CourseSection] CHECK CONSTRAINT [FK_CourseSection_Course]
GO
/****** Object:  ForeignKey [FK_CourseSection_Instructor]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[CourseSection]  WITH CHECK ADD  CONSTRAINT [FK_CourseSection_Instructor] FOREIGN KEY([InstructorID])
REFERENCES [dbo].[Instructor] ([InstructorID])
GO
ALTER TABLE [dbo].[CourseSection] CHECK CONSTRAINT [FK_CourseSection_Instructor]
GO
/****** Object:  ForeignKey [FK_CourseTutor_Course]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[CourseTutor]  WITH CHECK ADD  CONSTRAINT [FK_CourseTutor_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[CourseTutor] CHECK CONSTRAINT [FK_CourseTutor_Course]
GO
/****** Object:  ForeignKey [FK_CourseTutor_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[CourseTutor]  WITH CHECK ADD  CONSTRAINT [FK_CourseTutor_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[CourseTutor] CHECK CONSTRAINT [FK_CourseTutor_Resource]
GO
/****** Object:  ForeignKey [FK_Error_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Error]  WITH CHECK ADD  CONSTRAINT [FK_Error_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[Error] CHECK CONSTRAINT [FK_Error_Resource]
GO
/****** Object:  ForeignKey [FK_FreeTime_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[FreeTime]  WITH CHECK ADD  CONSTRAINT [FK_FreeTime_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[FreeTime] CHECK CONSTRAINT [FK_FreeTime_Resource]
GO
/****** Object:  ForeignKey [FK_FreeTime_SessionType]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[FreeTime]  WITH CHECK ADD  CONSTRAINT [FK_FreeTime_SessionType] FOREIGN KEY([SessionTypeID])
REFERENCES [dbo].[SessionType] ([SessionTypeID])
GO
ALTER TABLE [dbo].[FreeTime] CHECK CONSTRAINT [FK_FreeTime_SessionType]
GO
/****** Object:  ForeignKey [FK_GroupMember_Group]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[GroupMember]  WITH CHECK ADD  CONSTRAINT [FK_GroupMember_Group] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Group] ([GroupID])
GO
ALTER TABLE [dbo].[GroupMember] CHECK CONSTRAINT [FK_GroupMember_Group]
GO
/****** Object:  ForeignKey [FK_GroupMember_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[GroupMember]  WITH CHECK ADD  CONSTRAINT [FK_GroupMember_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[GroupMember] CHECK CONSTRAINT [FK_GroupMember_Resource]
GO
/****** Object:  ForeignKey [FK_Message_Group_To]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Group_To] FOREIGN KEY([MessageToGroupID])
REFERENCES [dbo].[Group] ([GroupID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Group_To]
GO
/****** Object:  ForeignKey [FK_Message_MessageContent]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_MessageContent] FOREIGN KEY([MessageContentID])
REFERENCES [dbo].[MessageContent] ([MessageContentID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_MessageContent]
GO
/****** Object:  ForeignKey [FK_Message_MessageType]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_MessageType] FOREIGN KEY([MessageTypeID])
REFERENCES [dbo].[MessageType] ([MessageTypeID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_MessageType]
GO
/****** Object:  ForeignKey [FK_Message_Resource_From]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Resource_From] FOREIGN KEY([MessageFromResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Resource_From]
GO
/****** Object:  ForeignKey [FK_Message_Resource_To]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Resource_To] FOREIGN KEY([MessageToResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Resource_To]
GO
/****** Object:  ForeignKey [FK_MessageInbox_Message]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[MessageInbox]  WITH CHECK ADD  CONSTRAINT [FK_MessageInbox_Message] FOREIGN KEY([MessageID])
REFERENCES [dbo].[Message] ([MessageID])
GO
ALTER TABLE [dbo].[MessageInbox] CHECK CONSTRAINT [FK_MessageInbox_Message]
GO
/****** Object:  ForeignKey [FK_OperationRange_SessionType]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[OperationRange]  WITH CHECK ADD  CONSTRAINT [FK_OperationRange_SessionType] FOREIGN KEY([SessionTypeID])
REFERENCES [dbo].[SessionType] ([SessionTypeID])
GO
ALTER TABLE [dbo].[OperationRange] CHECK CONSTRAINT [FK_OperationRange_SessionType]
GO
/****** Object:  ForeignKey [FK_Resource_Carrier]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Resource]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[Carrier] ([CarrierID])
GO
ALTER TABLE [dbo].[Resource] CHECK CONSTRAINT [FK_Resource_Carrier]
GO
/****** Object:  ForeignKey [FK_Resource_ResourceType]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Resource]  WITH CHECK ADD  CONSTRAINT [FK_Resource_ResourceType] FOREIGN KEY([ResourceTypeID])
REFERENCES [dbo].[ResourceType] ([ResourceTypeID])
GO
ALTER TABLE [dbo].[Resource] CHECK CONSTRAINT [FK_Resource_ResourceType]
GO
/****** Object:  ForeignKey [FK_ResourceRegistration_CourseSection]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[ResourceRegistration]  WITH CHECK ADD  CONSTRAINT [FK_ResourceRegistration_CourseSection] FOREIGN KEY([CourseSectionID])
REFERENCES [dbo].[CourseSection] ([CourseSectionID])
GO
ALTER TABLE [dbo].[ResourceRegistration] CHECK CONSTRAINT [FK_ResourceRegistration_CourseSection]
GO
/****** Object:  ForeignKey [FK_ResourceRegistration_Resource]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[ResourceRegistration]  WITH CHECK ADD  CONSTRAINT [FK_ResourceRegistration_Resource] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[ResourceRegistration] CHECK CONSTRAINT [FK_ResourceRegistration_Resource]
GO
/****** Object:  ForeignKey [FK_Resource_Schedule]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Schedule] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Resource_Schedule]
GO
/****** Object:  ForeignKey [FK_Schedule_Session]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Session] FOREIGN KEY([SessionID])
REFERENCES [dbo].[Session] ([SessionID])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Session]
GO
/****** Object:  ForeignKey [FK_Session_CourseSection]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Session]  WITH CHECK ADD  CONSTRAINT [FK_Session_CourseSection] FOREIGN KEY([CourseSectionID])
REFERENCES [dbo].[CourseSection] ([CourseSectionID])
GO
ALTER TABLE [dbo].[Session] CHECK CONSTRAINT [FK_Session_CourseSection]
GO
/****** Object:  ForeignKey [FK_Session_SessionType]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[Session]  WITH CHECK ADD  CONSTRAINT [FK_Session_SessionType] FOREIGN KEY([SessionTypeID])
REFERENCES [dbo].[SessionType] ([SessionTypeID])
GO
ALTER TABLE [dbo].[Session] CHECK CONSTRAINT [FK_Session_SessionType]
GO
/****** Object:  ForeignKey [FK_SessionTutor_Course]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[SessionTutor]  WITH CHECK ADD  CONSTRAINT [FK_SessionTutor_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[SessionTutor] CHECK CONSTRAINT [FK_SessionTutor_Course]
GO
/****** Object:  ForeignKey [FK_SessionTutor_Room]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[SessionTutor]  WITH CHECK ADD  CONSTRAINT [FK_SessionTutor_Room] FOREIGN KEY([RoomID])
REFERENCES [dbo].[Room] ([RoomID])
GO
ALTER TABLE [dbo].[SessionTutor] CHECK CONSTRAINT [FK_SessionTutor_Room]
GO
/****** Object:  ForeignKey [FK_SessionTutor_Session]    Script Date: 02/27/2012 13:07:44 ******/
ALTER TABLE [dbo].[SessionTutor]  WITH CHECK ADD  CONSTRAINT [FK_SessionTutor_Session] FOREIGN KEY([SessionID])
REFERENCES [dbo].[Session] ([SessionID])
GO
ALTER TABLE [dbo].[SessionTutor] CHECK CONSTRAINT [FK_SessionTutor_Session]
GO
