﻿USE [UKSTATS]
GO
/****** Object:  StoredProcedure [dbo].[ExtractGuidFromXML]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/23/2012
-- Description:	Accepts an XML serialization of an array of Resource IDs and performs a select operation to retrieve the data within.
-- =============================================
CREATE PROCEDURE [dbo].[ExtractGuidFromXML]
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT
		*
	FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
	WITH 
	( 
		guid uniqueidentifier '.'
	)	

END
GO
/****** Object:  StoredProcedure [dbo].[ResourceNamesFromXML]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- =============================================
CREATE PROCEDURE [dbo].[ResourceNamesFromXML]
	@xml nvarchar(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml
	
	SELECT Resource.ResourceNameFirst, Resource.ResourceNameLast
	FROM Resource
	WHERE Resource.ResourceID IN 
	(
		SELECT
			*
		FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
		WITH 
		( 
			guid uniqueidentifier '.'
		)
	)

END
GO
/****** Object:  StoredProcedure [dbo].[PerformTutorSkillsRestore]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/13/2012
-- Description:	Restores the flattened Tutor Skills data to its relational format.
-- =============================================
CREATE PROCEDURE [dbo].[PerformTutorSkillsRestore]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO CourseTutor (CourseTutorID, CourseID, ResourceID)
		SELECT 
			NEWID(), Course.CourseID, Resource.ResourceID
		FROM TutorSkillsBackup
		LEFT JOIN Resource ON 
			TutorSkillsBackup.ResourceLogin = Resource.ResourceLogin 
			AND 
			TutorSkillsBackup.ResourceNameFirst = Resource.ResourceNameFirst 
			AND 
			TutorSkillsBackup.ResourceNameLast = Resource.ResourceNameLast
		LEFT JOIN Subject ON 
			TutorSkillsBackup.SubjectName = Subject.SubjectName
			AND
			TutorSkillsBackup.SubjectNameShort = Subject.SubjectNameShort
		LEFT JOIN Course ON
			TutorSkillsBackup.CourseName = Course.CourseName
			AND
			TutorSkillsBackup.CourseNumber = Course.CourseNumber
			AND
			Course.SubjectID = Subject.SubjectID
		WHERE
			Course.CourseID IS NOT NULL
			AND
			Resource.ResourceID IS NOT NULL
END
GO
/****** Object:  StoredProcedure [dbo].[PerformTutorSkillsBackup]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/13/2012
-- Description:	Flattens out the relational mapping of the Tutor Skills into a table.
-- =============================================
CREATE PROCEDURE [dbo].[PerformTutorSkillsBackup]

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM TutorSkillsBackup

	INSERT INTO TutorSkillsBackup
		SELECT 
			NEWID()
			,Resource.ResourceLogin
			,Resource.ResourceNameFirst
			,Resource.ResourceNameLast
			,Subject.SubjectName
			,Subject.SubjectNameShort
			,Course.CourseName
			,Course.CourseNumber
			
		FROM CourseTutor

		LEFT JOIN Resource ON CourseTutor.ResourceID = Resource.ResourceID
		LEFT JOIN Course ON CourseTutor.CourseID = Course.CourseID
		LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID
		
	
END
GO
/****** Object:  StoredProcedure [dbo].[CourseTutorSkills]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- =============================================
CREATE PROCEDURE [dbo].[CourseTutorSkills]
		-- Add the parameters for the stored procedure here
	@ResourceID uniqueidentifier = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		Resource.ResourceID
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,(Subject.SubjectNameShort + ' ' + Course.CourseNumber) AS Course
		,Course.CourseName
	FROM CourseTutor
	LEFT JOIN Course ON CourseTutor.CourseID = Course.CourseID
	LEFT JOIN Resource ON CourseTutor.ResourceID = Resource.ResourceID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID

	WHERE
		ResourceType.ResourceTypeName = 'Tutor'
		AND
		(
			@ResourceID IS NULL
			OR
			Resource.ResourceID = @ResourceID
		)

	ORDER BY Resource.ResourceNameLast ASC, Resource.ResourceNameFirst ASC, Subject.SubjectNameShort ASC, Course.CourseNumber ASC 
END
GO
/****** Object:  StoredProcedure [dbo].[TutorSkills]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/16/2011
-- =============================================
CREATE PROCEDURE [dbo].[TutorSkills] 
	-- Add the parameters for the stored procedure here
	@ResourceID uniqueidentifier = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		Resource.ResourceID
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,Subject.SubjectNameShort
		,Course.CourseNumber
		,Course.CourseName
	FROM CourseTutor
	LEFT JOIN Course ON CourseTutor.CourseID = Course.CourseID
	LEFT JOIN Resource ON CourseTutor.ResourceID = Resource.ResourceID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID

	WHERE
		ResourceType.ResourceTypeName = 'Tutor'
		AND
		(
			@ResourceID IS NULL
			OR
			Resource.ResourceID = @ResourceID
		)

	ORDER BY Resource.ResourceNameLast ASC, Resource.ResourceNameFirst ASC, Subject.SubjectNameShort ASC, Course.CourseNumber ASC 

END
GO
/****** Object:  StoredProcedure [dbo].[ResourceReport]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/3/2012
-- =============================================
CREATE PROCEDURE [dbo].[ResourceReport]
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

    SELECT 
		Resource.ResourceID
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,Resource.ResourceLogin
		,Resource.ResourceEmail
		,Resource.ResourcePhoneNumber
		,COALESCE([Group].GroupName, 'N/A') AS GroupName
		,ResourceType.ResourceTypeName
	FROM Resource
	LEFT JOIN GroupMember ON Resource.ResourceID = GroupMember.ResourceID
	LEFT JOIN [Group] ON GroupMember.GroupID = [Group].GroupID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID

	WHERE
		@xml IS NULL
		OR
		(
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
		)
	ORDER BY 
		Resource.ResourceNameLast ASC
		,Resource.ResourceNameFirst ASC

END
GO
/****** Object:  StoredProcedure [dbo].[CourseRoster]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- =============================================
CREATE PROCEDURE [dbo].[CourseRoster]
	@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT
		Subject.SubjectNameShort,
		Course.CourseNumber,
		CourseSection.CourseSectionNumber,
		Instructor.InstructorName,
		Instructor.InstructorEmail,
		Instructor.InstructorPhone,
		CourseSection.CourseSectionDaysMeet,
		CourseSection.CourseSectionTimeStart,
		CourseSection.CourseSectionTimeEnd,
		CourseSection.CourseSectionBuilding,
		CourseSection.CourseSectionRoom,
		Resource.ResourceNameLast,
		Resource.ResourceNameFirst

	FROM ResourceRegistration
	LEFT JOIN Resource ON ResourceRegistration.ResourceID = Resource.ResourceID
	LEFT JOIN CourseSection ON ResourceRegistration.CourseSectionID = CourseSection.CourseSectionID
	LEFT JOIN Course ON CourseSection.CourseID = Course.CourseID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID
	LEFT JOIN Instructor ON CourseSection.InstructorID = Instructor.InstructorID

	WHERE 
		(
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
		)
		
	ORDER BY 
		Subject.SubjectNameShort ASC,
		Course.CourseNumber ASC,
		CourseSection.CourseSectionNumber ASC,
		Resource.ResourceNameLast ASC,
		Resource.ResourceNameFirst ASC
END
GO
/****** Object:  StoredProcedure [dbo].[CourseCheckSheet]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- =============================================
CREATE PROCEDURE [dbo].[CourseCheckSheet]
	@dt Date = NULL,
	--@xml nvarchar(MAX) = NULL
	@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	DECLARE @day varchar(MAX)
	SET @day = datepart(dw, @dt)

	SET @day = CASE @day
					WHEN 1 THEN 'U'
					WHEN 2 THEN 'M'
					WHEN 3 THEN 'T'
					WHEN 4 THEN 'W'
					WHEN 5 THEN 'R'
					WHEN 6 THEN 'F'
					WHEN 7 THEN 'S'
					ELSE 'FAIL'
				END
	SELECT 
		Resource.ResourceID,
		Course.CourseName,
		Subject.SubjectNameShort,
		Course.CourseNumber,
		CourseSection.CourseSectionNumber,
		Resource.ResourceNameFirst,
		Resource.ResourceNameLast,
		CourseSection.CourseSectionDateStart,
		CourseSection.CourseSectionDateEnd,
		CourseSection.CourseSectionDaysMeet,
		CourseSection.CourseSectionBuilding,
		courseSection.CourseSectionRoom,
		CourseSection.CourseSectionTimeStart

	FROM ResourceRegistration
	LEFT JOIN Resource ON ResourceRegistration.ResourceID = Resource.ResourceID
	LEFT JOIN CourseSection ON ResourceRegistration.CourseSectionID = CourseSection.CourseSectionID
	LEFT JOIN Course ON CourseSection.CourseID = Course.CourseID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID


	WHERE 
		@dt BETWEEN CourseSection.CourseSectionDateStart AND CourseSection.CourseSectionDateEnd
		AND 
		CourseSection.CourseSectionDaysMeet LIKE '%' + @day + '%'
		AND
		(
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
		)
	ORDER BY 
		-- Building, Start Time, Room Number, Last Name, First Name.
		CourseSection.CourseSectionBuilding, 
		CourseSection.CourseSectionTimeStart, 
		CourseSection.CourseSectionRoom, 
		Resource.ResourceNameLast, 
		Resource.ResourceNameFirst
END
GO
/****** Object:  StoredProcedure [dbo].[WeeklyTutorSchedule]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/15/2011
-- =============================================
CREATE PROCEDURE [dbo].[WeeklyTutorSchedule] 
	-- Add the parameters for the stored procedure here
	--@DateInput DATETIME = NULL,
	@DateRangeStart DATETIME = NULL,
	@DateRangeEnd DATETIME = NULL,
	@TutorID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	--SET @DateInput = '9/15/2011'
	--SET @DateRangeStart = dateadd(d,1-datepart(dw,@DateInput),@DateInput)
	--SET @DateRangeEnd = DATEADD(DAY, 7, @DateRangeStart)
	--SET @TutorID = 'EA94FEC6-F106-4C1D-AB9E-EF2F4E6D3BFA'

	SELECT
		Session.SessionID
		,DATENAME(dw, Session.SessionDateTimeStart) AS ScheduledDay
		,CONVERT(VARCHAR, Session.SessionDateTimeStart, 108) AS ScheduledTime
		,COALESCE(Subject.SubjectNameShort + Course.CourseNumber, 'N/A') AS Subject
		,SessionType.SessionTypeName AS SessionType
		,(Resource.ResourceNameLast + ', ' + Resource.ResourceNameFirst) AS TutorName 
		,Resource.ResourcePhoneNumber AS StudentPhone		
		,tblStudents.StudentName
	FROM Session
		LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID
		LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
		LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
		LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
		LEFT JOIN Schedule ON schedule.SessionID = Session.SessionID
		LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
		LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
		INNER JOIN (
			SELECT	
				Session.SessionID,
				Resource.ResourceNameLast + ', ' + Resource.ResourceNameFirst AS StudentName
			FROM Session
				LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID
				LEFT JOIN Schedule ON schedule.SessionID = Session.SessionID
				LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
				LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
				LEFT JOIN CourseSection ON Session.CourseSectionID = CourseSection.CourseSectionID
				LEFT JOIN Course ON Course.CourseID = CourseSection.CourseID
				LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
			WHERE
				Resource.ResourceID IS NOT NULL
				AND	ResourceType.ResourceTypeName = 'Student'
				AND Session.SessionIsScheduled = 1
				AND Session.SessionIsCancelled = 0
				AND
				(
					@DateRangeStart IS NULL
					OR
					@DateRangeStart <= Session.SessionDateTimeStart
				)
				AND
				(
					@DateRangeEnd IS NULL
					OR
					@DateRangeEnd >= Session.SessionDateTimeStart
				)
		) tblStudents ON Session.SessionID = tblStudents.SessionID
	WHERE 
		Resource.ResourceID IS NOT NULL
		AND
		ResourceType.ResourceTypeName = 'Tutor'
		AND 
		Session.SessionIsScheduled = 1
		AND
		Session.SessionIsCancelled = 0
		AND
		(
			@DateRangeStart IS NULL
			OR
			@DateRangeStart <= Session.SessionDateTimeStart
		)
		AND
		(
			@DateRangeEnd IS NULL
			OR
			@DateRangeEnd >= Session.SessionDateTimeStart
		)
		AND 
		(
			@TutorID IS NULL
		OR
			Resource.ResourceID = @TutorID
		)
		
	ORDER BY Resource.ResourceID, Session.SessionDateTimeStart


END
GO
/****** Object:  StoredProcedure [dbo].[WeeklyActivity]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- =============================================
CREATE PROCEDURE [dbo].[WeeklyActivity]
	-- Add the parameters for the stored procedure here
	@DateRangeStart DateTime = NULL,
	@DateRangeEnd DateTime = NULL,
	@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml
	
	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);
	
	SELECT
		Resource.ResourceNameLast,
		Resource.ResourceNameFirst,

		COALESCE(
			(
				(
					COALESCE(
					(
						SELECT 
							SUM(COALESCE(Attendance_Tutoring.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance_Tutoring.AttendanceTimeIn, Attendance_Tutoring.AttendanceTimeOut), 0)) AS MinutesCounted
						FROM Session AS Session_Tutoring
						LEFT JOIN SessionType AS SessionType_Tutoring ON Session_Tutoring.SessionTypeID = SessionType_Tutoring.SessionTypeID 
						LEFT JOIN Schedule AS Schedule_Tutoring ON Schedule_Tutoring.SessionID = Session_Tutoring.SessionID
						LEFT JOIN Resource AS Resource_Tutoring ON Schedule_Tutoring.ResourceID = Resource_Tutoring.ResourceID
						LEFT JOIN Attendance AS Attendance_Tutoring ON Schedule_Tutoring.ScheduleID = Attendance_Tutoring.ScheduleID
						WHERE 
							Resource_Tutoring.ResourceID = Resource.ResourceID
							AND
							SessionType_Tutoring.SessionTypeName = 'Tutoring'
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= Session_Tutoring.SessionDateTimeStart
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= Session_Tutoring.SessionDateTimeStart
							)

						GROUP BY Resource_Tutoring.ResourceID, SessionType_Tutoring.SessionTypeName
					), 0)			
				)
				+
				(
					COALESCE(
					(
						SELECT 
							SUM(FreeTime.FreeTimeAmount) AS MinutesCounted 
						FROM FreeTime 
						LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID 
						WHERE 
							SessionType_FreeTime.SessionTypeName = 'Tutoring'
							AND 
							FreeTime.ResourceID = Resource.ResourceID
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= FreeTime.FreeTimeApplyDate
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= FreeTime.FreeTimeApplyDate
							)
					), 0)
				)
			), 0) AS MinutesTutoring,

		COALESCE(
			(
				(
					COALESCE(
					(
						SELECT 
							SUM(Attendance_CompLab.AttendanceTimeOverride) AS MinutesCounted
						FROM Session AS Session_CompLab
						LEFT JOIN SessionType AS SessionType_CompLab ON Session_CompLab.SessionTypeID = SessionType_CompLab.SessionTypeID 
						LEFT JOIN Schedule AS Schedule_CompLab ON Schedule_CompLab.SessionID = Session_CompLab.SessionID
						LEFT JOIN Resource AS Resource_CompLab ON Schedule_CompLab.ResourceID = Resource_CompLab.ResourceID
						LEFT JOIN Attendance AS Attendance_CompLab ON Schedule_CompLab.ScheduleID = Attendance_CompLab.ScheduleID
						WHERE 
							Resource_CompLab.ResourceID = Resource.ResourceID
							AND 
							SessionType_CompLab.SessionTypeName = 'Computer Lab'
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= Session_CompLab.SessionDateTimeStart
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= Session_CompLab.SessionDateTimeStart
							)

						GROUP BY Resource_CompLab.ResourceID, SessionType_CompLab.SessionTypeName
					), 0)			
				)
				+
				(
					COALESCE(
					(
						SELECT 
							SUM(FreeTime.FreeTimeAmount) AS MinutesCounted 
						FROM FreeTime 
						LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID 
						WHERE 
							SessionType_FreeTime.SessionTypeName = 'Computer Lab'
							AND 
							FreeTime.ResourceID = Resource.ResourceID
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= FreeTime.FreeTimeApplyDate
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= FreeTime.FreeTimeApplyDate
							)
					), 0)
				)
			), 0) AS MinutesCompLab,
			
		COALESCE(
			(
				(
					COALESCE(
					(
						SELECT 
							SUM(COALESCE(Attendance_StudyHall.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance_StudyHall.AttendanceTimeIn, Attendance_StudyHall.AttendanceTimeOut), 0)) AS MinutesCounted
						FROM Session AS Session_StudyHall
						LEFT JOIN SessionType AS SessionType_StudyHall ON Session_StudyHall.SessionTypeID = SessionType_StudyHall.SessionTypeID 
						LEFT JOIN Schedule AS Schedule_StudyHall ON Schedule_StudyHall.SessionID = Session_StudyHall.SessionID
						LEFT JOIN Resource AS Resource_StudyHall ON Schedule_StudyHall.ResourceID = Resource_StudyHall.ResourceID
						LEFT JOIN Attendance AS Attendance_StudyHall ON Schedule_StudyHall.ScheduleID = Attendance_StudyHall.ScheduleID
						WHERE 
							Resource_StudyHall.ResourceID = Resource.ResourceID
							AND 
							SessionType_StudyHall.SessionTypeName = 'Study Hall'
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= Session_StudyHall.SessionDateTimeStart
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= Session_StudyHall.SessionDateTimeStart
							)

						GROUP BY Resource_StudyHall.ResourceID, SessionType_StudyHall.SessionTypeName
					),0 )
				)
				+
				(
					COALESCE(
					(
						SELECT 
							SUM(FreeTime.FreeTimeAmount) AS MinutesCounted 
						FROM FreeTime 
						LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID 
						WHERE 
							SessionType_FreeTime.SessionTypeName = 'Study Hall' 
							AND 
							FreeTime.ResourceID = Resource.ResourceID
							AND
							(
								@DateRangeStart IS NULL 
								OR
								@DateRangeStart <= FreeTime.FreeTimeApplyDate
							)
							AND
							(
								@DateRangeEnd IS NULL 
								OR
								@DateRangeEnd >= FreeTime.FreeTimeApplyDate
							)
					), 0)
				)
			), 0) AS MinutesStudyHall

		FROM Resource

		WHERE
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
			
		ORDER BY ResourceNameLast, ResourceNameFirst
END
GO
/****** Object:  StoredProcedure [dbo].[UsageReport]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 09/22/2011
-- =============================================
CREATE PROCEDURE [dbo].[UsageReport] 
	-- Add the parameters for the stored procedure here
	@DateStart datetime = NULL, 
	@DateEnd datetime = NULL,
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	-- If this is null, I don't think we should be adding days to it.
	--SET @DateEnd = DATEADD(DAY, 1, @DateEnd);

    -- Insert statements for procedure here
	SELECT SessionType.SessionTypeName
	--,SUM(DATEDIFF(n, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut)) AS TimeAccrued
	,SUM(COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0)) AS TimeAccrued
	,COUNT(Session.SessionID) AS SessionCount

	FROM Attendance LEFT JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID 
		LEFT JOIN Session ON Schedule.SessionID = Session.SessionID 
		LEFT JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID 
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
		
	WHERE 
		(
			@DateStart IS NULL
			OR
			Session.SessionDateTimeStart >= @DateStart 
		)
		AND 
		(
			@DateEnd IS NULL
			OR
			Session.SessionDateTimeStart <= DATEADD(DAY, 1, @DateEnd)  
		)
		AND
		(
			(
				@xml IS NULL 
			)
			OR
			(
				Resource.ResourceID IN 
				(
					SELECT
						*
					FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
					WITH 
					( 
						guid uniqueidentifier '.'
					)
				)
			)
		)

	GROUP BY Session.SessionTypeID, SessionType.SessionTypeName
END
GO
/****** Object:  StoredProcedure [dbo].[TutoringPayroll]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/6/2011
-- Description:	Gets a dataset used for Reporting.  In particular, this retrieves the time that tutors have logged in the system.
-- =============================================
CREATE PROCEDURE [dbo].[TutoringPayroll] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart datetime = null, 
	@DateRangeEnd datetime = null, 
	@ResourceID uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);
	
	SELECT
		Resource.ResourceNameLast,
		Resource.ResourceNameFirst, 
		COALESCE((
			SELECT 
				COUNT(DISTINCT(Session_1.SessionID))
			FROM Attendance AS Attendance_1
			LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
			LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
			WHERE 
				Session_1.SessionIsScheduled = 1
				AND
				Schedule_1.ResourceID = Resource.ResourceID
				AND 
				(@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
				AND
				(@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
		), 0) AS Scheduled,
		COALESCE((
			SELECT 
				COUNT(DISTINCT(Session_1.SessionID))
			FROM Attendance AS Attendance_1
			LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
			LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
			WHERE 
				Session_1.SessionIsScheduled = 0
				AND
				Schedule_1.ResourceID = Resource.ResourceID
				AND 
				(@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
				AND
				(@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
		), 0) AS Unscheduled,
		COALESCE((
			SELECT 
				COUNT(DISTINCT(Session_1.SessionID))
			FROM Attendance AS Attendance_1
			LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
			LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
			WHERE 
				Schedule_1.ResourceID = Resource.ResourceID
				AND 
				(@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
				AND
				(@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
		), 0) AS Attended,
		AVG(COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0)) AS AverageMinutes,
		SUM(COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0)) AS TotalMinutes
	    
	FROM Attendance 
	LEFT OUTER JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID 
	LEFT OUTER JOIN Resource ON Schedule.ResourceID = Resource.ResourceID 
	LEFT OUTER JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
	LEFT OUTER JOIN Session ON Session.SessionID = Schedule.SessionID 
	LEFT OUTER JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID 

	WHERE 
		ResourceType.ResourceTypeName = 'Tutor'
		AND
		SessionType.SessionTypeName = 'Tutoring'
		AND
		(
			(@DateRangeStart IS NULL OR Session.SessionDateTimeStart >= @DateRangeStart)
			AND
			(@DateRangeEnd IS NULL OR Session.SessionDateTimeStart <= @DateRangeEnd)
		)
		AND
		(@ResourceID IS NULL OR Resource.ResourceID = @ResourceID)

	GROUP BY 
		Resource.ResourceID,
		Resource.ResourceNameFirst, 
		Resource.ResourceNameLast

	ORDER BY Resource.ResourceNameLast ASC
END
GO
/****** Object:  StoredProcedure [dbo].[TutoringActivity]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/15/2011
-- =============================================
CREATE PROCEDURE [dbo].[TutoringActivity] 
	@DateRangeStart DATETIME = NULL,
	@DateRangeEnd DATETIME = NULL,
	@TutorID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	SELECT 
		Session.SessionIsScheduled
		,Session.SessionID
		,CONVERT(VARCHAR(10),Session.SessionDateTimeStart,101) AS SessionDate
		,CONVERT(VARCHAR(10),Session.SessionDateTimeStart,108) AS SessionTime
		,COALESCE(Subject.SubjectNameShort, 'N/A') AS SubjectName
		,COALESCE(Course.CourseNumber, 'N/A') AS CourseNumber
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,(CASE WHEN  Attendance.AttendanceID IS NULL THEN 'No Show' ELSE 'Attended' END) AS Attendance
		,COALESCE(CONVERT(VARCHAR(10),Attendance.AttendanceTimeIn,108), '') AS 'TimeIn'
		,COALESCE(CONVERT(VARCHAR(10),Attendance.AttendanceTimeOut,108), '') AS 'TimeOut'
	FROM Session
	LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
	LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
	LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
	LEFT JOIN Schedule ON Session.SessionID = Schedule.SessionID
	LEFT JOIN Attendance ON Schedule.ScheduleID = Attendance.ScheduleID
	LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID

	WHERE 
		Resource.ResourceID IS NOT NULL
		AND
		Session.SessionIsCancelled = 0
		AND
		(
			@DateRangeStart IS NULL
			OR
			@DateRangeStart <= Session.SessionDateTimeStart
		)
		AND
		(
			@DateRangeEnd IS NULL
			OR
			@DateRangeEnd >= Session.SessionDateTimeStart
		)
		AND
		(
			@TutorID IS NULL
			OR
			(
				Session.SessionID IN 
				(
					SELECT Session_TutorID.SessionID
					FROM Session AS Session_TutorID 
					LEFT JOIN Schedule AS Schedule_TutorID ON Session_TutorID.SessionID = Schedule_TutorID.SessionID
					LEFT JOIN Resource AS Resource_TutorID ON Schedule_TutorID.ResourceID = Resource_TutorID.ResourceID
					WHERE Resource_TutorID.ResourceID = @TutorID
				)
			)
		)

	ORDER BY Session.SessionIsScheduled DESC, Session.SessionDateTimeStart ASC, Session.SessionID ASC, ResourceType.ResourceTypeName DESC, Resource.ResourceNameLast ASC, Resource.ResourceNameFirst ASC


END
GO
/****** Object:  StoredProcedure [dbo].[Transactions]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/12/2011
-- =============================================
CREATE PROCEDURE [dbo].[Transactions] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart DateTime = NULL,
	@DateRangeEnd DateTime = NULL,
	@SessionTypeID uniqueidentifier = NULL,
	@xml XML = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT 
		ResourceNameLast, 
		ResourceNameFirst, 
		SessionTypeName, 
		AttendanceTimeIn, 
		AttendanceTimeOut, 
		AttendanceMachineName
	 FROM
	(
		SELECT
			Resource.ResourceNameLast
			,Resource.ResourceNameFirst
			,SessionType.SessionTypeName
			,Attendance.AttendanceTimeIn
			,Attendance.AttendanceTimeOut
			,Attendance.AttendanceMachineName
			,Session.SessionDateTimeStart
		FROM Attendance
		LEFT JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
		LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
		LEFT JOIN Session ON Schedule.SessionID = Session.SessionID
		LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID 
		
		WHERE
			(	
				Session.SessionDateTimeStart >= @DateRangeStart
				OR
				@DateRangeStart IS NULL
			)
			AND
			(
				Session.SessionDateTimeStart <= DATEADD(DAY, 1, @DateRangeEnd)
				OR
				@DateRangeEnd IS NULL
			)
			AND
			(
				Resource.ResourceID IN 
				(
					SELECT
						*
					FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
					WITH 
					( 
						guid uniqueidentifier '.'
					)
				)
			)
	) AS t1

	WHERE
	@SessionTypeID IS NULL
	OR
	(
		t1.SessionTypeName = (SELECT SessionType.SessionTypeName FROM SessionType WHERE SessionType.SessionTypeID = @SessionTypeID)
	)

	ORDER BY t1.SessionDateTimeStart ASC
END
GO
/****** Object:  StoredProcedure [dbo].[StudentActivity]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/12/2011
-- =============================================
CREATE PROCEDURE [dbo].[StudentActivity] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart DateTime = NULL,
	@DateRangeEnd DateTime = NULL,
	@SessionTypeID uniqueidentifier = NULL,
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml
	
	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);
	
    SELECT 
	*
	FROM (
		SELECT 
			Resource.ResourceNameLast
			,Resource.ResourceNameFirst

			,Session.SessionDateTimeStart
			,Session.SessionDateTimeEnd
			
			,Attendance.AttendanceTimeIn
			,Attendance.AttendanceTimeOut

			,CASE WHEN Attendance.AttendanceID IS NULL THEN 'No Show' ELSE 'Attended' END AS Attend

			,SessionType.SessionTypeName
			,COALESCE(Subject.SubjectNameShort, 'N/A') AS SubjectName
			,COALESCE(Course.CourseNumber, 'N/A') AS CourseNumber

			,COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0) AS Duration
			FROM Session
			LEFT JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID
			LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
			LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
			LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
			LEFT JOIN Schedule ON Schedule.SessionID = Session.SessionID
			LEFT JOIN Attendance ON Attendance.ScheduleID = Schedule.ScheduleID
			LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
			LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
			LEFT JOIN CourseSection ON CourseSection.CourseSectionID = Session.CourseSectionID

			WHERE 
				SessionType.SessionTypeName NOT IN ('Class', 'Note')
				AND
				ResourceType.ResourceTypeName = 'Student'
				AND
				(	
					Session.SessionDateTimeStart >= @DateRangeStart
					OR
					@DateRangeStart IS NULL
				)
				AND
				(
					Session.SessionDateTimeStart <= @DateRangeEnd
					OR
					@DateRangeEnd IS NULL
				)
				AND
				(
					Resource.ResourceID IN 
					(
						SELECT
							*
						FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
						WITH 
						( 
							guid uniqueidentifier '.'
						)
					)
				)
				AND
				(
					-- Disregard activity of less than 2 minutes?
					DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) > 2
				)
		) AS t1
		
	WHERE 
	@SessionTypeID IS NULL
	OR
	(
		t1.SessionTypeName = (SELECT SessionType.SessionTypeName FROM SessionType WHERE SessionType.SessionTypeID = @SessionTypeID)
	)
	
	ORDER BY t1.SessionDateTimeStart ASC
END
GO
/****** Object:  StoredProcedure [dbo].[ExceptionReport]    Script Date: 02/27/2012 13:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/29/2011
-- =============================================
CREATE PROCEDURE [dbo].[ExceptionReport] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart DATETIME = NULL, 
	@DateRangeEnd DATETIME = NULL,
	@TutorID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	SELECT 
		SessionOut.SessionIsScheduled
		,SessionOut.SessionID
		,CONVERT(VARCHAR(10),SessionOut.SessionDateTimeStart,101) AS SessionDate
		,CONVERT(VARCHAR(10),SessionOut.SessionDateTimeStart,108) AS SessionTime
		,COALESCE(SubjectOut.SubjectNameShort, 'N/A') AS SubjectName
		,COALESCE(CourseOut.CourseNumber, 'N/A') AS CourseNumber
		,ResourceOut.ResourceNameLast
		,ResourceOut.ResourceNameFirst
		,(CASE WHEN  AttendanceOut.AttendanceID IS NULL THEN 'No Show' ELSE 'Attended' END) AS Attendance
		,COALESCE(CONVERT(VARCHAR(10),AttendanceOut.AttendanceTimeIn,108), '') AS 'TimeIn'
		,COALESCE(CONVERT(VARCHAR(10),AttendanceOut.AttendanceTimeOut,108), '') AS 'TimeOut'
	FROM Session AS SessionOut 
	LEFT JOIN SessionTutor AS SessionTutorOut ON SessionOut.SessionID = SessionTutorOut.SessionID
	LEFT JOIN Course AS CourseOut ON CourseOut.CourseID = SessionTutorOut.CourseID
	LEFT JOIN Subject AS SubjectOut ON SubjectOut.SubjectID = CourseOut.SubjectID
	LEFT JOIN Schedule AS ScheduleOut ON SessionOut.SessionID = ScheduleOut.SessionID
	LEFT JOIN Attendance AS AttendanceOut ON ScheduleOut.ScheduleID = AttendanceOut.ScheduleID
	LEFT JOIN Resource AS ResourceOut ON ScheduleOut.ResourceID = ResourceOut.ResourceID
	LEFT JOIN ResourceType AS ResourceTypeOut ON ResourceOut.ResourceTypeID = ResourceTypeOut.ResourceTypeID

	WHERE SessionOut.SessionID IN 
	(
		SELECT 
			--Session.SessionIsScheduled
			Session.SessionID
			--,CONVERT(VARCHAR(10),Session.SessionDateTimeStart,101) AS SessionDate
			--,CONVERT(VARCHAR(10),Session.SessionDateTimeStart,108) AS SessionTime
			--,COALESCE(Subject.SubjectNameShort, 'N/A') AS SubjectName
			--,COALESCE(Course.CourseNumber, 'N/A') AS CourseNumber
			--,Resource.ResourceNameLast
			--,Resource.ResourceNameFirst
			--,(CASE WHEN  Attendance.AttendanceID IS NULL THEN 'No Show' ELSE 'Attended' END) AS Attendance
			--,COALESCE(CONVERT(VARCHAR(10),Attendance.AttendanceTimeIn,108), '') AS 'TimeIn'
			--,COALESCE(CONVERT(VARCHAR(10),Attendance.AttendanceTimeOut,108), '') AS 'TimeOut'
		FROM Session
		LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
		LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
		LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
		LEFT JOIN Schedule ON Session.SessionID = Schedule.SessionID
		LEFT JOIN Attendance ON Schedule.ScheduleID = Attendance.ScheduleID
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
		LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID

		WHERE 
			Session.SessionTypeID IN (SELECT SessionTypeID FROM SessionType WHERE SessionTypeName = 'Tutoring')
			AND
			Resource.ResourceID IS NOT NULL
			AND
			Session.SessionIsCancelled = 0
			AND
			Session.SessionIsScheduled = 1
			AND
			(
				@DateRangeStart IS NULL
				OR
				@DateRangeStart <= Session.SessionDateTimeStart
			)
			AND
			(
				@DateRangeEnd IS NULL
				OR
				@DateRangeEnd >= Session.SessionDateTimeStart
			)
			AND
			(
				@TutorID IS NULL
				OR
				(
					Session.SessionID IN 
					(
						SELECT Session_TutorID.SessionID
						FROM Session AS Session_TutorID 
						LEFT JOIN Schedule AS Schedule_TutorID ON Session_TutorID.SessionID = Schedule_TutorID.SessionID
						LEFT JOIN Resource AS Resource_TutorID ON Schedule_TutorID.ResourceID = Resource_TutorID.ResourceID
						WHERE Resource_TutorID.ResourceID = @TutorID
					)
				)
			)
			AND
			(
				Attendance.AttendanceID IS NULL
			)
	)
	ORDER BY SessionOut.SessionIsScheduled DESC, SessionOut.SessionDateTimeStart ASC, SessionOut.SessionID ASC, ResourceTypeOut.ResourceTypeName DESC, ResourceOut.ResourceNameLast ASC, ResourceOut.ResourceNameFirst ASC

	
	
	
	
END
GO
