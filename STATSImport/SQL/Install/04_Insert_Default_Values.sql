USE [UKSTATS]

/* Delete Existing SessionType */
DELETE FROM SessionType

/* Populate Default SessionTypes */
INSERT INTO SessionType (SessionTypeName, SessionTypeDescription) VALUES ('Class', '')
INSERT INTO SessionType (SessionTypeName, SessionTypeDescription) VALUES ('Study Hall', '')
INSERT INTO SessionType (SessionTypeName, SessionTypeDescription) VALUES ('Tutoring', '')
INSERT INTO SessionType (SessionTypeName, SessionTypeDescription) VALUES ('Computer Lab', '')
INSERT INTO SessionType (SessionTypeName, SessionTypeDescription) VALUES ('Note', '')


/* Delete Existing Settings */
DELETE FROM Setting

/* Populate Default Settings */
INSERT Setting (SettingKey, SettingValue) VALUES (N'ApplicationName', N'University of Kentucky - Ohio Casualty Group Center for Academic and Tutorial Services (C.A.T.S)')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ApplicationNameShort', N'C.A.T.S.')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ComputerLabBarHeight', N'36')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ComputerLabIdleLogoffMinutes', N'15')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ComputerLabIdleWarningMinutes', N'5')
INSERT Setting (SettingKey, SettingValue) VALUES (N'LoginEarlyMinutes', N'15')
INSERT Setting (SettingKey, SettingValue) VALUES (N'LoginLateMinutes', N'30')
INSERT Setting (SettingKey, SettingValue) VALUES (N'MessageSubjectDefault', N'C.A.T.S. Message')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorClass', N'Green')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorCompLab', N'Blue')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorDefault', N'Gray')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorFreeForm', N'Pink')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorStudyHall', N'Yellow')
INSERT Setting (SettingKey, SettingValue) VALUES (N'ScheduleColorTutoring', N'SkyBlue')
INSERT Setting (SettingKey, SettingValue) VALUES (N'SemesterDateBegin', N'2011-08-25')
INSERT Setting (SettingKey, SettingValue) VALUES (N'SemesterDateEnd', N'2012-12-31')
INSERT Setting (SettingKey, SettingValue) VALUES (N'SessionTimeout', N'20')
INSERT Setting (SettingKey, SettingValue) VALUES (N'SMTPHost', N'localhost')
INSERT Setting (SettingKey, SettingValue) VALUES (N'SMTPPort', N'25')
INSERT Setting (SettingKey, SettingValue) VALUES (N'StudyHallSyncInterval', N'5')
INSERT Setting (SettingKey, SettingValue) VALUES (N'TutoringSyncInterval', N'10')
INSERT Setting (SettingKey, SettingValue) VALUES (N'TutorSessionLength', N'55')
INSERT Setting (SettingKey, SettingValue) VALUES (N'WebHost', N'http://superman.thinksis.com/UKSTATS/Administration/')

/* Delete Existing Carriers */
DELETE FROM Carrier

/* Populate Default Carriers */
INSERT INTO Carrier (CarrierID, CarrierName, CarrierEmailSuffix) VALUES ('00000000-0000-0000-0000-000000000000', N'None', N'')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'3 River Wireless', N'@sms.3rivers.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'ACS Wireless', N'@paging.acswireless.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Alltel', N'@message.alltel.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'AT&T', N'@txt.att.net ')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Bell Mobility Canada 1', N'@txt.bellmobility.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Bell Mobility Canada 2', N'@bellmobility.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Bell Mobility Canada 3', N'@txt.bell.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Blue Sky Frog', N'@blueskyfrog.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Bluegrass Cellular', N'@sms.bluecell.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Boost Mobile', N'@myboostmobile.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'BPL Mobile', N'@bplmobile.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Carolina West Wireless', N'@cwwsms.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Cellular One', N'@mobile.celloneusa.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Cellular South', N'@csouth1.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Centennial Wireless', N'@cwemail.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'CenturyTel', N'@messaging.centurytel.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Clearnet', N'@msg.clearnet.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Comcast', N'@comcastpcs.textmsg.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Corr Wireless Communications', N'@corrwireless.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Dobson', N'@mobile.dobson.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Edge Wireless', N'@sms.edgewireless.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Fido', N'@fido.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Golden Telecom', N'@sms.goldentele.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Helio', N'@messaging.sprintpcs.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Houston Cellular', N'@text.houstoncellular.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Idea Cellular', N'@ideacellular.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Illinois Valley Cellular', N'@ivctext.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Inland Cellular Telephone', N'@inlandlink.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'MCI', N'@pagemci.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Metrocall', N'@page.metrocall.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Metrocall 2-way', N'@my2way.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Metro PCS', N'@mymetropcs.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Microcell', N'@fido.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Midwest Wireless', N'@clearlydigital.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Mobilcomm', N'@mobilecomm.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'MTS', N'@text.mtsmobility.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Nextel', N'@messaging.nextel.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'OnlineBeep', N'@onlinebeep.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'PCS One', N'@pcsone.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'President''s Choice', N'@txt.bell.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Public Service Cellular', N'@sms.pscel.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Qwest', N'@qwestmp.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Rogers AT&T Wireless', N'@pcs.rogers.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Rogers Canada', N'@pcs.rogers.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Satellink', N'.pageme@satellink.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Solo Mobile', N'@txt.bell.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Southwestern Bell', N'@email.swbw.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Sprint', N'@messaging.sprintpcs.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Sumcom', N'@tms.suncom.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Surewest Communications', N'@mobile.surewest.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'T-Mobile', N'@tmomail.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Telus', N'@msg.telus.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Tracfone', N'@txt.att.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Triton', N'@tms.suncom.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Unicel', N'@utext.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'US Cellular', N'@email.uscc.net ')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'US West', N'@uswestdatamail.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Verizon', N'@vtext.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Virgin Mobile', N'@vmobl.com')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Virgin Mobile Canada', N'@vmobile.ca')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'West Central Wireless', N'@sms.wcc.net')
INSERT INTO Carrier (CarrierName, CarrierEmailSuffix) VALUES (N'Western Wireless', N'@cellularonewest.com')

/* Delete Existing ResourceTypes */
DELETE FROM ResourceType

/* Populate Default ResourceTypes */
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Adminstrator', N'') /* Spelling error intentionally left in. */
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Director', N'')
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Monitor', N'')
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Student', N'')
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Time Mgr', N'')
INSERT INTO ResourceType (ResourceTypeName, ResourceTypeDescription) VALUES (N'Tutor', N'')


/* Delete Existing MessageTypes */
DELETE FROM MessageType

/* Populate Default MessageTypes */
INSERT INTO MessageType (MessageTypeName, MessageTypeDescription) VALUES (N'Application', N'')
INSERT INTO MessageType (MessageTypeName, MessageTypeDescription) VALUES (N'SMS (Text Message)', N'')
INSERT INTO MessageType (MessageTypeName, MessageTypeDescription) VALUES (N'Email', N'')


/* Delete Existing Operation Ranges */
DELETE FROM OperationRange

DECLARE @stid_StudyHall uniqueidentifier
DECLARE @stid_Tutoring uniqueidentifier
DECLARE @stid_CompLab uniqueidentifier
SET @stid_StudyHall = (SELECT SessionTypeID FROM SessionType WHERE SessionTypeName = 'Study Hall');
SET @stid_Tutoring = (SELECT SessionTypeID FROM SessionType WHERE SessionTypeName = 'Tutoring');
SET @stid_CompLab = (SELECT SessionTypeID FROM SessionType WHERE SessionTypeName = 'Computer Lab');

/* Populate Default Operation Ranges */
-- Study Hall
-- Note: On Saturday, this application is never available.
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'SUN', '14:00:00', '22:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'MON', '08:00:00', '22:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'TUE', '08:00:00', '22:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'WED', '08:00:00', '22:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'THU', '08:00:00', '22:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'FRI', '08:00:00', '12:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_StudyHall, 'SAT', '00:00:00', '00:00:00')

-- Tutoring
-- Note: On Saturday, this application is never available.
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'SUN', '12:00:00', '23:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'MON', '07:00:00', '23:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'TUE', '07:00:00', '23:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'WED', '07:00:00', '23:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'THU', '07:00:00', '23:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'FRI', '07:00:00', '18:00:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_Tutoring, 'SAT', '00:00:00', '00:00:00')

-- Comp Lab
-- Note: Comp Lab is always open; rather, it is according to their old database.
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'SUN', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'MON', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'TUE', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'WED', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'THU', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'FRI', '00:00:00', '23:59:00')
INSERT dbo.OperationRange (SessionTypeID, OperationRangeDayOfWeek, OperationRangeTimeStart, OperationRangeTimeEnd) VALUES (@stid_CompLab, 'SAT', '00:00:00', '23:59:00')


-- Rooms
-- Note: These are the rooms originally imported from their old system.
DELETE FROM Room
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'204', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'205', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'206', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'207', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'208', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'209', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'211', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'212', 1, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'214', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'215', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'216', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'217', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'219', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'221', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'222', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'223', 2, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'225', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'226', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'227', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'228', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'231', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'232', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'233', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'235', 3, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'237', 4, 30)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'911', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'912', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'913', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'914', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'915', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'916', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'917', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'918', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'919', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'920', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'921', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'922', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'923', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'924', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'925', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'926', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'927', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'928', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'929', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'930', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'931', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'932', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'933', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'934', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'935', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'936', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'937', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'938', 5, 4)
INSERT Room (RoomName, RoomPriority, RoomCapacity) VALUES (N'100', 1, 2)