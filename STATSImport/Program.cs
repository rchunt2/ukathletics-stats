﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using System.Xml;
using System.IO;
namespace UK.STATS.STATSImport
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"+--------------------------------------+");
            Console.WriteLine(@"|   UK STATS Data Import Application   |");
            Console.WriteLine(@"+--------------------------------------+");
            Console.WriteLine(@"|               WARNING:               |");
            Console.WriteLine(@"| This application executes code that  |");
            Console.WriteLine(@"|   performs DELETE operations on a    |");
            Console.WriteLine(@"|  database.  Please make a backup of  |");
            Console.WriteLine(@"|      all data before proceeding!     |");
            Console.WriteLine(@"+--------------------------------------+");

            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            int numArgs = args.Length;

            if (!ValidArguments(args)) { return; } // Exit application if invalid arguments were passed.

            String mode = args[0];
            String path = args[1];

            DirectoryInfo di = new DirectoryInfo(path);
            path = di.FullName;

            if (!PromptUserToProceed()) // Ensure that the user wants to proceed.
            {
                return; // If not, exit the application.
            }

            String[] filesToConvert = GetFilesToImport(path);

            if (filesToConvert.Length > 0)
            {
                Console.WriteLine(@"");
                Console.WriteLine(@"Directory: " + path);
                Console.WriteLine(@"");
                Console.WriteLine(@"Files that will be converted: ");
                foreach (var filePath in filesToConvert)
                {
                    FileInfo fi = new FileInfo(filePath);
                    Console.WriteLine(" " + fi.Name);
                }

                Console.WriteLine(@"");
                Console.WriteLine(@"If the files in this list are relatively large, the conversion process could take some time to complete.  The application may appear unresponsive during this time.");
                if (!PromptUserToProceed()) // Ensure that the user wants to proceed.
                {
                    return; // If not, exit the application.
                }

                String ConvertedSQL = GenerateSQL(filesToConvert);

                String OutputFileName = "UK_STATS_SAP_Import_Data_" + UnixTime().ToString() + ".sql";

                StreamWriter sw = File.CreateText(path + @"\" + OutputFileName);
                sw.Write(ConvertedSQL);
                sw.Flush();
                sw.Close();

                Console.WriteLine(@"");
                Console.WriteLine(@"Output Location: " + path + @"\" + OutputFileName);
            }
            else
            {
                Console.WriteLine(@"No files available for conversion in the supplied directory.");
                Console.WriteLine(@"Press any key to exit.");
                Console.ReadKey(false);
                return; // Exit Application.
            }
        }

        private static int UnixTime()
        {
            TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            double time = ts.TotalSeconds;
            return (int)(Math.Ceiling(time));
        }

        private static Boolean PromptUserToProceed()
        {
            Console.WriteLine(@"Press Y to proceed.  Otherwise, press any other key to exit.");
            ConsoleKeyInfo cki = Console.ReadKey(false);
            Console.WriteLine(@"");
            if (cki.KeyChar != 'y' && cki.KeyChar != 'Y')
            {
                return false; 
            }
            else
            {
                return true;    
            }
        }

        private static Boolean ValidArguments(String[] args)
        {
            Boolean isValid = true;
            int numArgs = args.Length;

            if (numArgs == 2)
            {
                String mode = args[0];
                switch (mode)
                {
                    case "convert":
                        break;
                    default:
                        isValid = false;
                        break;
                }

                String path = args[1];
                if (!Directory.Exists(path))
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            if (!isValid)
            {
                Console.WriteLine(@"");
                Console.WriteLine(@"Error: Invalid Arguments!");
                Console.WriteLine(@"");
                Console.WriteLine(@"USAGE: STATSImport.exe <mode> <path_to_csv_files>");
                Console.WriteLine(@"EXAMPLE: STATSImport.exe convert C:\Resources\");
                Console.WriteLine(@"");    
            }

            return isValid;
        }

        #region Import CSV Into Temporary Database
 
        public static String[] GetFilesToImport(String Path)
        {
            String[] files = Directory.GetFiles(Path);
            var FilesToParse = (from file in files let fi = new FileInfo(file) where fi.Extension == ".csv" select file).ToArray();
            return FilesToParse;
        }

        public static String GenerateSQL(String[] arrFilepath)
        {
            var sb = new StringBuilder();
            sb.AppendLine("DELETE FROM SAPImport;");
            sb.AppendLine("");
            
            foreach (var filepath in arrFilepath)
            {
                FileInfo fi = new FileInfo(filepath);

                Console.Write("Converting: " + fi.Name + "...");

                var csv = new CSVDocument(filepath);
                int NumRows = csv.Length;

                for (int i = 1; i < NumRows; i++) // Start at 1, the first row is for the column headings.
                {
                    List<String> Values = csv.GetRowValuesAsList(i);
                    var sbQuery = new StringBuilder();
                    sbQuery.Append("INSERT INTO SAPImport ( [ModuleID],  [ModuleAbbr],  [EventPackageID],  [EventPackageAbbr], [EventPackageDescription],  [BusinessEventType],  [AcademicEventOT],  [ObjectID], [BusinessEventAbbr], [NameOfTheBusinessEvent], [StartDate], [EndDate], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday], [Sunday], [StartTime], [EndTime], [Building], [ObjectName], [RoomShortName], [RoomName], [BusinessEventInstructorDescription], [MaximumCapacity], [OptimumCapacity], [EventPackageLocationAbbr], [EventPackageLocationName], [MaxEventCapacity], [OptimalEventCapacity], [Location], [Location2], [MaximumRoomCapacity], [OptimumRoomCapacity], [NumberStudentsEnrolled], [NumberGradedStudents], [WaitingListPlaces], [WaitListBooked], [StdPctForWLPlaces], [MaximumCredits], [WaitListDisabled], [AvailableForWebRegistration], [InCatalog], [WLManualMoveUp], [Location3], [BusinessEventInstructorAbbr], [RoomObjectID], [ModuleDesc], [Blank1], [Blank2], [Blank3]) VALUES (");

                    foreach (var sValue in Values)
                    {
                        String sValueTemp = sValue;
                        if (sValue.Contains('\''))
                        {
                            sValueTemp = sValue.Replace("'", "''");
                        }
                        sbQuery.Append("N'" + sValueTemp + "', ");
                    }
                    sbQuery.Remove(sbQuery.Length - 2, 2);
                    sbQuery.Append(");");

                    sb.AppendLine(sbQuery.ToString());
                }
                Console.WriteLine("Complete.");
            }
            Console.WriteLine("");
            Console.WriteLine("File Conversion Successful!");
            return sb.ToString();
        }


        #endregion

    }
}
