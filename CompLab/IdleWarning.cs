﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UK.STATS.CompLab
{
    public partial class IdleWarning : Form
    {
        public MainForm CompLabMainForm { get; set; }
        public String Mode { get; set; }
        private const String idleText = "You have been inactive for some time. Please resume work to prevent your session from ending.";
        private const String logoutText = "Your session has been ended because your account has been logged in elsewhere.";

        public IdleWarning(MainForm Parent)
        {
            InitializeComponent();
            CompLabMainForm = Parent;
            this.TopMost = true;
        }

        public IdleWarning(MainForm Parent, String mode)
        {
            InitializeComponent();
            CompLabMainForm = Parent;
            Mode = mode;
            switch(Mode)
            {
                case "Idle":
                    lblWarning.Text = idleText;
                    break;
                case "Logout":
                    lblWarning.Text = logoutText;
                    break;
                default:
                    break;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            CompLabMainForm.CurrentResourceState = ClientLogic.ClientState.Active;
            this.Close();
        }
    }
}
