﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
    public partial class TimeTrackerForm : Form
    {
        private DateTime _dateStart = DateTime.Today;

        public MainForm CompLabMainForm { get; set; }

        public Resource ResourceObj { get; set; }

        public DateTime RangeStart
        {
            get { return _dateStart; }
            set { _dateStart = value; }
        }

        public DateTime RangeEnd { get { return RangeStart.AddDays(7); } }

        public TimeTrackerForm(MainForm Parent)
        {
            InitializeComponent();
            CompLabMainForm = Parent;
        }

        public void Calculate()
        {
            RangeStart = RangeStart.Date;

            while (RangeStart.DayOfWeek != DayOfWeek.Sunday)
            {
                RangeStart = RangeStart.AddDays(-1);
            }

            String DateRange = RangeStart.ToShortDateString() + " - " + RangeEnd.ToShortDateString();
            lblDateRange.Text = "Date Range: " + DateRange;

            var attendanceTime = new AttendanceTime();

            STATSModel.Attendance.CalculateTime(ResourceObj, RangeStart, attendanceTime);

            var tsCompLabMonday = TimeSpan.FromMinutes(attendanceTime.CompLabMonday);
            var tsCompLabTuesday = TimeSpan.FromMinutes(attendanceTime.CompLabTuesday);
            var tsCompLabWednesday = TimeSpan.FromMinutes(attendanceTime.CompLabWednesday);
            var tsCompLabThursday = TimeSpan.FromMinutes(attendanceTime.CompLabThursday);
            var tsCompLabFriday = TimeSpan.FromMinutes(attendanceTime.CompLabFriday);
            var tsCompLabTotal = TimeSpan.FromMinutes(attendanceTime.CompLabTotal);

            var tsTutoringMonday = TimeSpan.FromMinutes(attendanceTime.TutoringMonday);
            var tsTutoringTuesday = TimeSpan.FromMinutes(attendanceTime.TutoringTuesday);
            var tsTutoringWednesday = TimeSpan.FromMinutes(attendanceTime.TutoringWednesday);
            var tsTutoringThursday = TimeSpan.FromMinutes(attendanceTime.TutoringThursday);
            var tsTutoringFriday = TimeSpan.FromMinutes(attendanceTime.TutoringFriday);
            var tsTutoringTotal = TimeSpan.FromMinutes(attendanceTime.TutoringTotal);

            var tsStudyHallMonday = TimeSpan.FromMinutes(attendanceTime.StudyHallMonday);
            var tsStudyHallTuesday = TimeSpan.FromMinutes(attendanceTime.StudyHallTuesday);
            var tsStudyHallWednesday = TimeSpan.FromMinutes(attendanceTime.StudyHallWednesday);
            var tsStudyHallThursday = TimeSpan.FromMinutes(attendanceTime.StudyHallThursday);
            var tsStudyHallFriday = TimeSpan.FromMinutes(attendanceTime.StudyHallFriday);
            var tsStudyHallTotal = TimeSpan.FromMinutes(attendanceTime.StudyHallTotal);

            var tsMondayTotal = TimeSpan.FromMinutes(attendanceTime.MondayTotal);
            var tsTuesdayTotal = TimeSpan.FromMinutes(attendanceTime.TuesdayTotal);
            var tsWednesdayTotal = TimeSpan.FromMinutes(attendanceTime.WednesdayTotal);
            var tsThursdayTotal = TimeSpan.FromMinutes(attendanceTime.ThursdayTotal);
            var tsFridayTotal = TimeSpan.FromMinutes(attendanceTime.FridayTotal);
            var tsOverallTotal = TimeSpan.FromMinutes(attendanceTime.OverallTotal);

            lblCompLabMonday.Text = tsCompLabMonday.Hours.ToString() + ":" + tsCompLabMonday.Minutes.ToString().PadLeft(2, '0');
            lblCompLabTuesday.Text = tsCompLabTuesday.Hours.ToString() + ":" + tsCompLabTuesday.Minutes.ToString().PadLeft(2, '0');
            lblCompLabWednesday.Text = tsCompLabWednesday.Hours.ToString() + ":" + tsCompLabWednesday.Minutes.ToString().PadLeft(2, '0');
            lblCompLabThursday.Text = tsCompLabThursday.Hours.ToString() + ":" + tsCompLabThursday.Minutes.ToString().PadLeft(2, '0');
            lblCompLabFriday.Text = tsCompLabFriday.Hours.ToString() + ":" + tsCompLabFriday.Minutes.ToString().PadLeft(2, '0');
            lblCompLabTotal.Text = tsCompLabTotal.Hours.ToString() + ":" + tsCompLabTotal.Minutes.ToString().PadLeft(2, '0');

            lblTutoringMonday.Text = tsTutoringMonday.Hours.ToString() + ":" + tsTutoringMonday.Minutes.ToString().PadLeft(2, '0');
            lblTutoringTuesday.Text = tsTutoringTuesday.Hours.ToString() + ":" + tsTutoringTuesday.Minutes.ToString().PadLeft(2, '0');
            lblTutoringWednesday.Text = tsTutoringWednesday.Hours.ToString() + ":" + tsTutoringWednesday.Minutes.ToString().PadLeft(2, '0');
            lblTutoringThursday.Text = tsTutoringThursday.Hours.ToString() + ":" + tsTutoringThursday.Minutes.ToString().PadLeft(2, '0');
            lblTutoringFriday.Text = tsTutoringFriday.Hours.ToString() + ":" + tsTutoringFriday.Minutes.ToString().PadLeft(2, '0');
            lblTutoringTotal.Text = tsTutoringTotal.Hours.ToString() + ":" + tsTutoringTotal.Minutes.ToString().PadLeft(2, '0');

            lblStudyHallMonday.Text = tsStudyHallMonday.Hours.ToString() + ":" + tsStudyHallMonday.Minutes.ToString().PadLeft(2, '0');
            lblStudyHallTuesday.Text = tsStudyHallTuesday.Hours.ToString() + ":" + tsStudyHallTuesday.Minutes.ToString().PadLeft(2, '0');
            lblStudyHallWednesday.Text = tsStudyHallWednesday.Hours.ToString() + ":" + tsStudyHallWednesday.Minutes.ToString().PadLeft(2, '0');
            lblStudyHallThursday.Text = tsStudyHallThursday.Hours.ToString() + ":" + tsStudyHallThursday.Minutes.ToString().PadLeft(2, '0');
            lblStudyHallFriday.Text = tsStudyHallFriday.Hours.ToString() + ":" + tsStudyHallFriday.Minutes.ToString().PadLeft(2, '0');
            lblStudyHallTotal.Text = tsStudyHallTotal.Hours.ToString() + ":" + tsStudyHallTotal.Minutes.ToString().PadLeft(2, '0');

            lblMondayTotal.Text = tsMondayTotal.Hours.ToString() + ":" + tsMondayTotal.Minutes.ToString().PadLeft(2, '0');
            lblTuesdayTotal.Text = tsTuesdayTotal.Hours.ToString() + ":" + tsTuesdayTotal.Minutes.ToString().PadLeft(2, '0');
            lblWednesdayTotal.Text = tsWednesdayTotal.Hours.ToString() + ":" + tsWednesdayTotal.Minutes.ToString().PadLeft(2, '0');
            lblThursdayTotal.Text = tsThursdayTotal.Hours.ToString() + ":" + tsThursdayTotal.Minutes.ToString().PadLeft(2, '0');
            lblFridayTotal.Text = tsFridayTotal.Hours.ToString() + ":" + tsFridayTotal.Minutes.ToString().PadLeft(2, '0');
            lblOverallTotal.Text = tsOverallTotal.Hours.ToString() + ":" + tsOverallTotal.Minutes.ToString().PadLeft(2, '0');
        }

        private void TimeTrackerForm_Load(object sender, EventArgs e)
        {
            ResourceType eResourceTypeTutor = ResourceType.Get("tutor");
            btnPrintTutorReports.Enabled = (this.ResourceObj.ResourceTypeID == eResourceTypeTutor.ResourceTypeID);
            Calculate();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPrintTutorReports_Click(object sender, EventArgs e)
        {
            TutorReportsForm tutorReportsForm = new TutorReportsForm();
            tutorReportsForm.ResourceID = ResourceObj.ResourceID;
            tutorReportsForm.ShowDialog();
        }

        private void btnPrintResourceSchedule_Click(object sender, EventArgs e)
        {
            PrintScheduleForm printScheduleForm = new PrintScheduleForm();
            printScheduleForm.ResourceID = ResourceObj.ResourceID;
            printScheduleForm.ShowDialog();
        }
    }
}