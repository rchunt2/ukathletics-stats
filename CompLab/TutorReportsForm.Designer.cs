﻿namespace UK.STATS.CompLab
{
    partial class TutorReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printTheseReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPayroll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDetailedSummary = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBlankLogSheet = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpPayroll = new System.Windows.Forms.TabPage();
            this.rvPayroll = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tpDetailedSummary = new System.Windows.Forms.TabPage();
            this.rvDetailedSummary = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tpBlankLogSheet = new System.Windows.Forms.TabPage();
            this.rvBlankLogSheet = new Microsoft.Reporting.WinForms.ReportViewer();
            this.rdtpFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rdtpTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.printDlg = new System.Windows.Forms.PrintDialog();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpPayroll.SuspendLayout();
            this.tpDetailedSummary.SuspendLayout();
            this.tpBlankLogSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rdtpFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdtpTo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(564, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.printToolStripMenuItem.Text = "&Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printTheseReportsToolStripMenuItem,
            this.tsmiPayroll,
            this.tsmiDetailedSummary,
            this.tsmiBlankLogSheet});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "&Reports";
            // 
            // printTheseReportsToolStripMenuItem
            // 
            this.printTheseReportsToolStripMenuItem.Enabled = false;
            this.printTheseReportsToolStripMenuItem.Name = "printTheseReportsToolStripMenuItem";
            this.printTheseReportsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.printTheseReportsToolStripMenuItem.Text = "Print These Reports:";
            // 
            // tsmiPayroll
            // 
            this.tsmiPayroll.Checked = true;
            this.tsmiPayroll.CheckOnClick = true;
            this.tsmiPayroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiPayroll.Name = "tsmiPayroll";
            this.tsmiPayroll.Size = new System.Drawing.Size(179, 22);
            this.tsmiPayroll.Text = "&Payroll";
            // 
            // tsmiDetailedSummary
            // 
            this.tsmiDetailedSummary.Checked = true;
            this.tsmiDetailedSummary.CheckOnClick = true;
            this.tsmiDetailedSummary.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiDetailedSummary.Name = "tsmiDetailedSummary";
            this.tsmiDetailedSummary.Size = new System.Drawing.Size(179, 22);
            this.tsmiDetailedSummary.Text = "&Detailed Summary";
            // 
            // tsmiBlankLogSheet
            // 
            this.tsmiBlankLogSheet.Checked = true;
            this.tsmiBlankLogSheet.CheckOnClick = true;
            this.tsmiBlankLogSheet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsmiBlankLogSheet.Name = "tsmiBlankLogSheet";
            this.tsmiBlankLogSheet.Size = new System.Drawing.Size(179, 22);
            this.tsmiBlankLogSheet.Text = "&Blank Log Sheet";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpPayroll);
            this.tabControl1.Controls.Add(this.tpDetailedSummary);
            this.tabControl1.Controls.Add(this.tpBlankLogSheet);
            this.tabControl1.Location = new System.Drawing.Point(12, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(540, 424);
            this.tabControl1.TabIndex = 2;
            // 
            // tpPayroll
            // 
            this.tpPayroll.Controls.Add(this.rvPayroll);
            this.tpPayroll.Location = new System.Drawing.Point(4, 22);
            this.tpPayroll.Name = "tpPayroll";
            this.tpPayroll.Padding = new System.Windows.Forms.Padding(3);
            this.tpPayroll.Size = new System.Drawing.Size(532, 398);
            this.tpPayroll.TabIndex = 0;
            this.tpPayroll.Text = "Payroll";
            this.tpPayroll.UseVisualStyleBackColor = true;
            // 
            // rvPayroll
            // 
            this.rvPayroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvPayroll.LocalReport.ReportEmbeddedResource = "";
            this.rvPayroll.LocalReport.ReportPath = "";
            this.rvPayroll.Location = new System.Drawing.Point(3, 3);
            this.rvPayroll.Name = "rvPayroll";
            this.rvPayroll.Size = new System.Drawing.Size(526, 392);
            this.rvPayroll.TabIndex = 0;
            // 
            // tpDetailedSummary
            // 
            this.tpDetailedSummary.Controls.Add(this.rvDetailedSummary);
            this.tpDetailedSummary.Location = new System.Drawing.Point(4, 22);
            this.tpDetailedSummary.Name = "tpDetailedSummary";
            this.tpDetailedSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tpDetailedSummary.Size = new System.Drawing.Size(532, 398);
            this.tpDetailedSummary.TabIndex = 1;
            this.tpDetailedSummary.Text = "Detailed Summary";
            this.tpDetailedSummary.UseVisualStyleBackColor = true;
            // 
            // rvDetailedSummary
            // 
            this.rvDetailedSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvDetailedSummary.Location = new System.Drawing.Point(3, 3);
            this.rvDetailedSummary.Name = "rvDetailedSummary";
            this.rvDetailedSummary.Size = new System.Drawing.Size(526, 392);
            this.rvDetailedSummary.TabIndex = 0;
            // 
            // tpBlankLogSheet
            // 
            this.tpBlankLogSheet.Controls.Add(this.rvBlankLogSheet);
            this.tpBlankLogSheet.Location = new System.Drawing.Point(4, 22);
            this.tpBlankLogSheet.Name = "tpBlankLogSheet";
            this.tpBlankLogSheet.Size = new System.Drawing.Size(532, 398);
            this.tpBlankLogSheet.TabIndex = 2;
            this.tpBlankLogSheet.Text = "Blank Log Sheet";
            this.tpBlankLogSheet.UseVisualStyleBackColor = true;
            // 
            // rvBlankLogSheet
            // 
            this.rvBlankLogSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvBlankLogSheet.Location = new System.Drawing.Point(0, 0);
            this.rvBlankLogSheet.Name = "rvBlankLogSheet";
            this.rvBlankLogSheet.Size = new System.Drawing.Size(532, 398);
            this.rvBlankLogSheet.TabIndex = 0;
            // 
            // rdtpFrom
            // 
            this.rdtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdtpFrom.Location = new System.Drawing.Point(83, 30);
            this.rdtpFrom.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdtpFrom.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdtpFrom.Name = "rdtpFrom";
            this.rdtpFrom.NullableValue = new System.DateTime(2012, 1, 9, 13, 36, 43, 694);
            this.rdtpFrom.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdtpFrom.Size = new System.Drawing.Size(176, 20);
            this.rdtpFrom.TabIndex = 3;
            this.rdtpFrom.TabStop = false;
            this.rdtpFrom.Text = "Monday, January 09, 2012";
            this.rdtpFrom.Value = new System.DateTime(2012, 1, 9, 13, 36, 43, 694);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Date Range:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "to";
            // 
            // rdtpTo
            // 
            this.rdtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdtpTo.Location = new System.Drawing.Point(287, 30);
            this.rdtpTo.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdtpTo.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdtpTo.Name = "rdtpTo";
            this.rdtpTo.NullableValue = new System.DateTime(2012, 1, 9, 13, 36, 43, 694);
            this.rdtpTo.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdtpTo.Size = new System.Drawing.Size(176, 20);
            this.rdtpTo.TabIndex = 6;
            this.rdtpTo.TabStop = false;
            this.rdtpTo.Text = "Monday, January 09, 2012";
            this.rdtpTo.Value = new System.DateTime(2012, 1, 9, 13, 36, 43, 694);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(469, 28);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(83, 25);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // printDlg
            // 
            this.printDlg.UseEXDialog = true;
            // 
            // TutorReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 492);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.rdtpTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rdtpFrom);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TutorReportsForm";
            this.Text = "Tutor Reports";
            this.Load += new System.EventHandler(this.TutorReportsForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpPayroll.ResumeLayout(false);
            this.tpDetailedSummary.ResumeLayout(false);
            this.tpBlankLogSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rdtpFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdtpTo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printTheseReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiPayroll;
        private System.Windows.Forms.ToolStripMenuItem tsmiDetailedSummary;
        private System.Windows.Forms.ToolStripMenuItem tsmiBlankLogSheet;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpPayroll;
        private Microsoft.Reporting.WinForms.ReportViewer rvPayroll;
        private System.Windows.Forms.TabPage tpDetailedSummary;
        private System.Windows.Forms.TabPage tpBlankLogSheet;
        private Microsoft.Reporting.WinForms.ReportViewer rvDetailedSummary;
        private Microsoft.Reporting.WinForms.ReportViewer rvBlankLogSheet;
        private Telerik.WinControls.UI.RadDateTimePicker rdtpFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadDateTimePicker rdtpTo;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.PrintDialog printDlg;
    }
}