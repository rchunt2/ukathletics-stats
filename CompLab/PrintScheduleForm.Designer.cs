﻿namespace UK.STATS.CompLab
{
    partial class PrintScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.DateTimeInterval dateTimeInterval1 = new Telerik.WinControls.UI.DateTimeInterval();
            Telerik.WinControls.UI.SchedulerDailyPrintStyle schedulerDailyPrintStyle1 = new Telerik.WinControls.UI.SchedulerDailyPrintStyle();
            this.ResourceRadScheduler = new Telerik.WinControls.UI.RadScheduler();
            this.label1 = new System.Windows.Forms.Label();
            this.lblResourceName = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblLandscapeReminder = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ResourceRadScheduler)).BeginInit();
            this.SuspendLayout();
            // 
            // ResourceRadScheduler
            // 
            dateTimeInterval1.End = new System.DateTime(((long)(0)));
            dateTimeInterval1.Start = new System.DateTime(((long)(0)));
            this.ResourceRadScheduler.AccessibleInterval = dateTimeInterval1;
            this.ResourceRadScheduler.ActiveViewType = Telerik.WinControls.UI.SchedulerViewType.Week;
            this.ResourceRadScheduler.AllowAppointmentCreateInline = false;
            this.ResourceRadScheduler.AllowAppointmentMove = false;
            this.ResourceRadScheduler.AllowAppointmentResize = false;
            this.ResourceRadScheduler.AppointmentTitleFormat = null;
            this.ResourceRadScheduler.AutoSize = true;
            this.ResourceRadScheduler.Culture = new System.Globalization.CultureInfo("en-US");
            this.ResourceRadScheduler.DataSource = null;
            this.ResourceRadScheduler.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.ResourceRadScheduler.GroupType = Telerik.WinControls.UI.GroupType.None;
            this.ResourceRadScheduler.HeaderFormat = "ddd";
            this.ResourceRadScheduler.Location = new System.Drawing.Point(12, 33);
            this.ResourceRadScheduler.Name = "ResourceRadScheduler";
            schedulerDailyPrintStyle1.AppointmentFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            schedulerDailyPrintStyle1.DateEndRange = new System.DateTime(2012, 11, 8, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.DateHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            schedulerDailyPrintStyle1.DateStartRange = new System.DateTime(2012, 6, 25, 0, 0, 0, 0);
            schedulerDailyPrintStyle1.PageHeadingFont = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.ResourceRadScheduler.PrintStyle = schedulerDailyPrintStyle1;
            this.ResourceRadScheduler.ReadOnly = true;
            this.ResourceRadScheduler.Size = new System.Drawing.Size(1000, 750);
            this.ResourceRadScheduler.TabIndex = 0;
            this.ResourceRadScheduler.Text = "radScheduler1";
            this.ResourceRadScheduler.AppointmentFormatting += new System.EventHandler<Telerik.WinControls.UI.SchedulerAppointmentEventArgs>(this.ResourceRadScheduler_AppointmentFormatting);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Schedule for ";
            // 
            // lblResourceName
            // 
            this.lblResourceName.AutoSize = true;
            this.lblResourceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResourceName.Location = new System.Drawing.Point(90, 13);
            this.lblResourceName.Name = "lblResourceName";
            this.lblResourceName.Size = new System.Drawing.Size(97, 13);
            this.lblResourceName.TabIndex = 2;
            this.lblResourceName.Text = "Resource Name";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(856, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(937, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblLandscapeReminder
            // 
            this.lblLandscapeReminder.AutoSize = true;
            this.lblLandscapeReminder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandscapeReminder.ForeColor = System.Drawing.Color.Red;
            this.lblLandscapeReminder.Location = new System.Drawing.Point(495, 7);
            this.lblLandscapeReminder.Name = "lblLandscapeReminder";
            this.lblLandscapeReminder.Size = new System.Drawing.Size(355, 15);
            this.lblLandscapeReminder.TabIndex = 5;
            this.lblLandscapeReminder.Text = "Remember to print your plan sheet in landscape mode";
            // 
            // PrintScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 862);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblLandscapeReminder);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lblResourceName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ResourceRadScheduler);
            this.Name = "PrintScheduleForm";
            this.Text = "PrintScheduleForm";
            this.Load += new System.EventHandler(this.PrintScheduleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ResourceRadScheduler)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadScheduler ResourceRadScheduler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblResourceName;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblLandscapeReminder;
    }
}