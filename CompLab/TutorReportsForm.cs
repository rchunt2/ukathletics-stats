﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UK.STATS.CompLab;
using UK.STATS.CompLab.UKSTATSDSReportsTableAdapters;
using Microsoft.Reporting.WinForms;
using UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
    public partial class TutorReportsForm : Form
    {
        private int m_currentPageIndex;
        private List<Stream> renderStreams; 

        private Guid _resourceID;
        public Guid ResourceID
        {
            get { return _resourceID; }
            set { _resourceID = value; }
        }

        public TutorReportsForm()
        {
            InitializeComponent();
        }

        private void RefreshReports(DateTime dtFrom, DateTime dtTo, Guid rscID)
        {
            try
            {
                // Tutoring Payroll Report
                var tp_ta = new TutoringPayrollTableAdapter();
                var tp_tbl = new UKSTATSDSReports.TutoringPayrollDataTable();
                tp_ta.Fill(tp_tbl, dtFrom, dtTo, rscID);
                //rvPayroll.LocalReport.ReportPath = "Reports\\TutoringPayroll.rdlc";
                //rvPayroll.LocalReport.ReportEmbeddedResource = "Reports\\TutoringPayroll.rdlc";
                //rvPayroll.LocalReport.ReportPath = @"C:\Source\UK STATS\CompLab\Reports";
                //rvPayroll.LocalReport.ReportEmbeddedResource = @"C:\Source\UK STATS\CompLab\Reports";
                rvPayroll.LocalReport.ReportEmbeddedResource = "UK.STATS.CompLab.Reports.TutoringPayroll.rdlc";
                rvPayroll.LocalReport.SetParameters(new ReportParameter("DateRangeStart", dtFrom.ToShortDateString()));
                rvPayroll.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", dtTo.ToShortDateString()));
                rvPayroll.LocalReport.SetParameters(new ReportParameter("ResourceID", rscID.ToString()));
                rvPayroll.LocalReport.DataSources.Clear();
                rvPayroll.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (DataTable)tp_tbl));

                // Tutoring Activity Report
                var ta_ta = new TutoringActivityTableAdapter();
                var ta_tbl = new UKSTATSDSReports.TutoringActivityDataTable();
                ta_ta.Fill(ta_tbl, dtFrom, dtTo, rscID);
                //rvDetailedSummary.LocalReport.ReportPath = "Reports\\TutoringActivity.rdlc";
                //rvDetailedSummary.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActivity.rdlc";
                rvDetailedSummary.LocalReport.ReportEmbeddedResource = "UK.STATS.CompLab.Reports.TutoringActivity.rdlc";
                rvDetailedSummary.LocalReport.SetParameters(new ReportParameter("DateRangeStart", dtFrom.ToShortDateString()));
                rvDetailedSummary.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", dtTo.ToShortDateString()));
                rvDetailedSummary.LocalReport.SetParameters(new ReportParameter("TutorID", rscID.ToString()));
                rvDetailedSummary.LocalReport.DataSources.Clear();
                rvDetailedSummary.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)ta_tbl));

                // Blank Log Sheet Report
                //rvBlankLogSheet.LocalReport.ReportPath = "Reports\\TutoringLogSheet.rdlc";
                //rvBlankLogSheet.LocalReport.ReportEmbeddedResource = "Reports\\TutoringLogSheet.rdlc";
                rvBlankLogSheet.LocalReport.ReportEmbeddedResource = "UK.STATS.CompLab.Reports.TutoringLogSheet.rdlc";
                rvBlankLogSheet.LocalReport.DataSources.Clear();

                rvPayroll.RefreshReport();
                rvDetailedSummary.RefreshReport();
                rvBlankLogSheet.RefreshReport();
            }
            catch (Exception e)
            {
                //Error.LogException(e, new Resource(), "1.1.1.1");
                var temp = e;
            }
        }

        private void TutorReportsForm_Load(object sender, EventArgs e)
        {
            renderStreams = new List<Stream>();
            DateTime End = DateTime.Today;
            DateTime Start = End.AddDays(-7);

            rdtpFrom.Value = Start;
            rdtpTo.Value = End;

            RefreshReports(rdtpFrom.Value, rdtpTo.Value, ResourceID);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshReports(rdtpFrom.Value, rdtpTo.Value, ResourceID);
        }

        private enum PageLayout
        {
            Portrait,
            Landscape
        }

        private void ExportReport(LocalReport localReport, PageLayout pageLayout)
        {
            String deviceInfo = "";

            switch(pageLayout)
            {
                case PageLayout.Portrait:
                    deviceInfo =
                        "<DeviceInfo>" +
                        "  <OutputFormat>EMF</OutputFormat>" +
                        "  <PageWidth>8.5in</PageWidth>" +
                        "  <PageHeight>11in</PageHeight>" +
                        "  <MarginTop>0.25in</MarginTop>" +
                        "  <MarginLeft>0.25in</MarginLeft>" +
                        "  <MarginRight>0.25in</MarginRight>" +
                        "  <MarginBottom>0.25in</MarginBottom>" +
                        "</DeviceInfo>";
                    break;
                case PageLayout.Landscape:
                    deviceInfo =
                        "<DeviceInfo>" +
                        "  <OutputFormat>EMF</OutputFormat>" +
                        "  <PageWidth>11in</PageWidth>" +
                        "  <PageHeight>8.5in</PageHeight>" +
                        "  <MarginTop>0.25in</MarginTop>" +
                        "  <MarginLeft>0.25in</MarginLeft>" +
                        "  <MarginRight>0.25in</MarginRight>" +
                        "  <MarginBottom>0.25in</MarginBottom>" +
                        "</DeviceInfo>";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("pageLayout");
            }

            Warning[] warnings;
            renderStreams = new List<Stream>();
            localReport.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in renderStreams)
            {
                stream.Position = 0;
            }
        }

        private Stream CreateStream(String name, String fileNameExtension, Encoding encoding, String mimeType, Boolean willSeek)
        {
            Stream stream = new FileStream(@"..\..\" + name + "." + fileNameExtension, FileMode.Create);
            renderStreams.Add(stream);
            return stream;
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new Metafile(renderStreams[m_currentPageIndex]);
            ev.Graphics.DrawImage(pageImage, ev.PageBounds);
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < renderStreams.Count);
        }
        
        private void Print(PrinterSettings printerSettings)
        {
            if (renderStreams == null || renderStreams.Count == 0)
            {
                return;
            }

            var printDoc = new PrintDocument();

            //const string printerName = "Microsoft Office Document Image Writer";
            //printerSettings.PrinterName = printerName;
            printDoc.PrinterSettings = printerSettings;

            if (!printDoc.PrinterSettings.IsValid)
            {
                string msg = String.Format("Can't find printer \"{0}\".", printDoc.PrinterSettings.PrinterName);
                MessageBox.Show(msg, "Print Error");
                return;
            }

            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            printDoc.Print();
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Boolean printPayroll = (tsmiPayroll.Checked);
            Boolean printSummary = (tsmiDetailedSummary.Checked);
            Boolean printBlankLog = (tsmiBlankLogSheet.Checked);

            DialogResult dlgResult = printDlg.ShowDialog();
            if(dlgResult == DialogResult.OK)
            {
                PrinterSettings printerSettings = printDlg.PrinterSettings;

                if (printPayroll)
                {
                    ExportReport(rvPayroll.LocalReport, PageLayout.Portrait);
                    m_currentPageIndex = 0;
                    Print(printDlg.PrinterSettings);
                    if (renderStreams != null)
                    {
                        foreach (Stream stream in renderStreams)
                            stream.Close();
                        renderStreams = null;
                    }
                }

                if (printSummary)
                {
                    ExportReport(rvDetailedSummary.LocalReport, PageLayout.Portrait);
                    m_currentPageIndex = 0;
                    Print(printDlg.PrinterSettings);
                    if (renderStreams != null)
                    {
                        foreach (Stream stream in renderStreams)
                            stream.Close();
                        renderStreams = null;
                    }
                }

                if (printBlankLog)
                {
                    ExportReport(rvBlankLogSheet.LocalReport, PageLayout.Landscape);
                    m_currentPageIndex = 0;
                    printDlg.PrinterSettings.DefaultPageSettings.Landscape = true;
                    Print(printDlg.PrinterSettings);
                    if (renderStreams != null)
                    {
                        foreach (Stream stream in renderStreams)
                            stream.Close();
                        renderStreams = null;
                    }
                }

                
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
