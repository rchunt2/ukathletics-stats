﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UK.STATS.STATSModel;

using BLL = UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
    public static partial class ClientLogic
    {
        #region Win32 API

        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct LASTINPUTINFO
        {
            public static readonly int SizeOf = Marshal.SizeOf(typeof(LASTINPUTINFO));

            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;

            [MarshalAs(UnmanagedType.U4)]
            public int dwTime;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct APPBARDATA
        {
            public int cbSize;
            public IntPtr hWnd;
            public int uCallbackMessage;
            public int uEdge;
            public RECT rc;
            public IntPtr lParam;
        }

        /***
         * advapi32.dll handles advanced functions of the Windows kernel
         * (access to Windows registry, shutdown/restart system, manage user accounts)
         * http://en.wikipedia.org/wiki/Windows_API
         ***/
        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

        /***
         * kernel32.dll provides access to the base level functions of the Windows kernel
         * (access to the file system, devices, processes, threads, error handling)
         * http://en.wikipedia.org/wiki/Windows_API
         ***/
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetCurrentProcess();

        [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
        private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

        [DllImport("USER32")]
        private static extern int GetSystemMetrics(int Index);

        /***
         * user32.dll provides access to the Windows user interface
         * (receive mouse and keyboard input)
         * http://en.wikipedia.org/wiki/Windows_API
         ***/
        [DllImport("User32.dll", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern int RegisterWindowMessage(string msg);

        [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool ExitWindowsEx(int flg, int rea);

        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        [DllImport("user32.dll")]
        private static extern bool GetLastInputInfo(out LASTINPUTINFO plii);

        [DllImport("User32.dll")]
        private static extern Int32 SetForegroundWindow(int hWnd);

        [DllImport("user32.dll")]
        private static extern int SetWindowsHookEx(int idHook, LowLevelKeyboardProcDelegate lpfn, int hMod, int dwThreadId);

        [DllImport("user32.dll")]
        private static extern int UnhookWindowsHookEx(int hHook);

        [DllImport("user32.dll")]
        private static extern int CallNextHookEx(int nHook, int nCode, int wParam, KBDLLHOOKSTRUCT lParam);

        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);

        public enum ApplicationBarMessage : int
        {
            ABM_NEW = 0,
            ABM_REMOVE,
            ABM_QUERYPOS,
            ABM_SETPOS,
            ABM_GETSTATE,
            ABM_GETTASKBARPOS,
            ABM_ACTIVATE,
            ABM_GETAUTOHIDEBAR,
            ABM_SETAUTOHIDEBAR,
            ABM_WINDOWPOSCHANGED,
            ABM_SETSTATE
        }

        public enum ApplicationBarNotify : int
        {
            ABN_STATECHANGE = 0,
            ABN_POSCHANGED,
            ABN_FULLSCREENAPP,
            ABN_WINDOWARRANGE
        }

        public enum ApplicationBarEdge : int
        {
            ABE_LEFT = 0,
            ABE_TOP,
            ABE_RIGHT,
            ABE_BOTTOM
        }

        private const int SE_PRIVILEGE_ENABLED = 0x00000002;
        private const int TOKEN_QUERY = 0x00000008;
        private const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;
        private const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        private const int EWX_LOGOFF = 0x00000000;
        private const int EWX_SHUTDOWN = 0x00000001;
        private const int EWX_REBOOT = 0x00000002;
        private const int EWX_FORCE = 0x00000004;
        private const int EWX_POWEROFF = 0x00000008;
        private const int EWX_FORCEIFHUNG = 0x00000010;
        private const int WH_KEYBOARD_LL = 13;
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 1;

        #endregion Win32 API

        public static int uCallBack;
        private static bool fBarRegistered = false;
        private static IntPtr hWnd;

        public static void ApplicationBarRegister(IntPtr WindowHandle, int pixelWidth, int pixelHeight)
        {
            try
            {
            hWnd = WindowHandle;
            var abd = new APPBARDATA();
            abd.cbSize = Marshal.SizeOf(abd);
            abd.hWnd = hWnd;
            if (!fBarRegistered)
            {
                uCallBack = RegisterWindowMessage("AppBarMessage");
                abd.uCallbackMessage = uCallBack;
                uint ret = SHAppBarMessage((int)ApplicationBarMessage.ABM_NEW, ref abd);
                fBarRegistered = true;
                ApplicationBarSetPosition(pixelWidth, pixelHeight);
            }
            else
            {
                SHAppBarMessage((int)ApplicationBarMessage.ABM_REMOVE, ref abd);
                fBarRegistered = false;
            }
        }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.ApplicationBarRegister method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
            }
        }

        public static void ApplicationBarSetPosition(int pixelWidth, int pixelHeight)
        {
            try
            {
            var abd = new APPBARDATA();
            abd.cbSize = Marshal.SizeOf(abd);
            abd.hWnd = hWnd;
            abd.uEdge = (int)ApplicationBarEdge.ABE_TOP;

            if (abd.uEdge == (int)ApplicationBarEdge.ABE_LEFT || abd.uEdge == (int)ApplicationBarEdge.ABE_RIGHT)
            {
                abd.rc.top = 0;
                abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
                if (abd.uEdge == (int)ApplicationBarEdge.ABE_LEFT)
                {
                    abd.rc.left = 0;
                    abd.rc.right = pixelWidth;
                }
                else
                {
                    abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
                    abd.rc.left = abd.rc.right - pixelWidth;
                }
            }
            else
            {
                abd.rc.left = 0;
                abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
                if (abd.uEdge == (int)ApplicationBarEdge.ABE_TOP)
                {
                    abd.rc.top = 0;
                    abd.rc.bottom = pixelHeight;
                }
                else
                {
                    abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
                    abd.rc.top = abd.rc.bottom - pixelHeight;
                }
            }

            SHAppBarMessage((int)ApplicationBarMessage.ABM_QUERYPOS, ref abd);

            switch (abd.uEdge)
            {
                case (int)ApplicationBarEdge.ABE_LEFT:
                    abd.rc.right = abd.rc.left + pixelWidth;
                    break;

                case (int)ApplicationBarEdge.ABE_RIGHT:
                    abd.rc.left = abd.rc.right - pixelWidth;
                    break;

                case (int)ApplicationBarEdge.ABE_TOP:
                    abd.rc.bottom = abd.rc.top + pixelHeight;
                    break;

                case (int)ApplicationBarEdge.ABE_BOTTOM:
                    abd.rc.top = abd.rc.bottom - pixelHeight;
                    break;
            }

            SHAppBarMessage((int)ApplicationBarMessage.ABM_SETPOS, ref abd);
            MoveWindow(abd.hWnd, abd.rc.left, abd.rc.top,
                    abd.rc.right - abd.rc.left, abd.rc.bottom - abd.rc.top, true);
        }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.ApplicationBarSetPosition method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
            }
        }

        // Maybe useful: http://msdn.microsoft.com/en-us/library/windows/desktop/ms644967(v=vs.85).aspx
        private static int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            try
            {
            Boolean isAltPressed = ((lParam.flags & 32) == lParam.flags);
            Boolean isKeyDown = ((lParam.flags & 128) == lParam.flags);
            Boolean isExtendedKey = ((lParam.flags & 1) == lParam.flags);

            Boolean ignoreKeypress = false;

            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:

                    // Orig:
                    //blnEat =  ((lParam.vkCode == 9) && (lParam.flags == 32)) |
                    //          ((lParam.vkCode == 27) && (lParam.flags == 32)) |
                    //          ((lParam.vkCode == 27) && (lParam.flags == 0)) |
                    //          ((lParam.vkCode == 91) && (lParam.flags == 1)) |
                    //          ((lParam.vkCode == 92) && (lParam.flags == 1));

                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key

                    Boolean AltTab = (lParam.vkCode == (int)vkCode.Tab) && (isAltPressed);
                    Boolean AltEsc = (lParam.vkCode == (int)vkCode.Escape) && (isAltPressed);
                    Boolean CtrlEsc = (lParam.vkCode == (int)vkCode.Escape) && (!isExtendedKey);
                    Boolean WinLeft = (lParam.vkCode == (int)vkCode.LeftWindows) && (isExtendedKey);
                    Boolean WinRight = (lParam.vkCode == (int)vkCode.RightWindows) && (isExtendedKey);

                    ignoreKeypress = AltTab || AltEsc || CtrlEsc || WinLeft || WinRight;

                    //ignoreKeypress =
                    //    ((lParam.vkCode == (int)vkCode.Tab) && (isAltPressed) |                // ALT + TAB
                    //    ((lParam.vkCode == (int)vkCode.Escape)) && (isAltPressed)) |           // ALT + ESC
                    //    ((lParam.vkCode == (int)vkCode.Escape) && (!isExtendedKey)) |          // CTRL + ESC
                    //    ((lParam.vkCode == (int)vkCode.LeftWindows) && (isExtendedKey)) |      // WIN (RIGHT)
                    //    ((lParam.vkCode == (int)vkCode.RightWindows) && (isExtendedKey));      // WIN (LEFT)
                    break;
            }

            if (!ignoreKeypress)
            {
                return CallNextHookEx(0, nCode, wParam, lParam);
            }
            return 1;
        }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.LowLevelKeyboardProc method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
                return -1;
            }
        }

        private static void DoExitWin(int flg)
        {
            try
            {
            bool ok;
            TokPriv1Luid tp;
            IntPtr hproc = GetCurrentProcess();
            IntPtr htok = IntPtr.Zero;
            ok = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
            tp.Count = 1;
            tp.Luid = 0;
            tp.Attr = SE_PRIVILEGE_ENABLED;
            ok = LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tp.Luid);
            ok = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
            ok = ExitWindowsEx(flg, 0);
        }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.DoExitWin method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
            }
        }

        //public static void KillStartMenu()
        //{
        //    //int hwnd = FindWindow("Shell_TrayWnd", "");
        //    //ShowWindow(hwnd, SW_HIDE);
        //    Taskbar.Hide();
        //}
        //public static void ShowStartMenu()
        //{
        //    int hwnd = FindWindow("Shell_TrayWnd", "");
        //    ShowWindow(hwnd, SW_SHOW);
        //}

        public static int TurnOffHotKeys()
        {
            try
            {
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    Module module = assembly.GetModules()[0];
                    int intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, Marshal.GetHINSTANCE(module).ToInt32(), 0);
                    return intLLKey;
                }
            }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.TurnOffHotKeys method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
                return -1;
            }
            return 0;
        }

        //public static void TurnOnHotKeys(int myIntLLKey)
        //{
        //    UnhookWindowsHookEx(myIntLLKey);
        //}

        private static SessionType _currentSessionType;

        public static SessionType CurrentSessionType
        {
            get
            {
                if (_currentSessionType == null)
                {
                    _currentSessionType = SessionType.GetByName("Computer Lab");
                }
                return _currentSessionType;
            }
        }

        private static STATSModel.ResourceType _resourceTypeTutor;

        public static STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (_resourceTypeTutor == null)
                {
                    _resourceTypeTutor = STATSModel.ResourceType.Get("Tutor");
                }
                return _resourceTypeTutor;
            }
        }

        private static STATSModel.ResourceType _resourceTypeStudent;

        public static STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (_resourceTypeStudent == null)
                {
                    _resourceTypeStudent = STATSModel.ResourceType.Get("Student");
                }
                return _resourceTypeStudent;
            }
        }

        public enum ClientState { Login, Active, Idle }

        public static String ComputerName { get { return SystemInformation.ComputerName; } }

        public static int GetUserIdleTimeInSeconds()
        {
            try
            {
            int idleTime = 0;
            var lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;
            int envTicks = Environment.TickCount; // NOTE: THIS IS NOT TICKS, BUT RATHER MILLISECONDS!  (Way to go, Microsoft!)
            if (GetLastInputInfo(out lastInputInfo))
            {
                int lastInputTick = lastInputInfo.dwTime;
                idleTime = envTicks - lastInputTick;
            }
            TimeSpan tsIdle = TimeSpan.FromMilliseconds(idleTime);
            return (int)Math.Ceiling(tsIdle.TotalSeconds);
        }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.GetUserIdleTimeInSeconds method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());
                return -1;
            }
        }

        public static void RestartComputer()
        {
            //MessageBox.Show("RESTART");
            //System.Diagnostics.Process.Start("ShutDown", "-r -f -t 00");
            try
            {
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    DoExitWin(EWX_REBOOT + EWX_FORCE);
                }
            }
            catch (Exception ex)
            {
                StringBuilder eventLogContents = new StringBuilder();
                eventLogContents.AppendLine("There has been an error in the ClientLogic.RestartComputer method.");
                eventLogContents.AppendLine("******************************************************************");
                eventLogContents.AppendLine(ex.ToString());
                EventLogging.WriteToEventLog(eventLogContents.ToString());

                // TODO: This may be erroneous, but we'll give it a go.
                Error.LogException(ex, null, ComputerName);
            }
        }
    }
}