﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace UK.STATS.CompLab
{
    class EventLogging
    {
        public EventLogging()
        {

        }

        public static Boolean WriteToEventLog(string inEvent)
        {
            Boolean result = false;
            String sSource = "SIS CompLab";
            String sLog = "Application";
            String sEvent = inEvent;

            try
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);

                EventLog.WriteEntry(sSource, sEvent);

                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }
}
