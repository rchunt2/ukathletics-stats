﻿namespace UK.STATS.CompLab
{
    partial class TimeTrackerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblDateRange = new System.Windows.Forms.Label();
            this.lblComputerLab = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTutoring = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblStudyHall = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnPrintTutorReports = new System.Windows.Forms.Button();
            this.btnPrintResourceSchedule = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelMonday = new System.Windows.Forms.Label();
            this.lblTuesday = new System.Windows.Forms.Label();
            this.lblWednesday = new System.Windows.Forms.Label();
            this.lblThursday = new System.Windows.Forms.Label();
            this.lblFriday = new System.Windows.Forms.Label();
            this.lblAllTotal = new System.Windows.Forms.Label();
            this.lblCompLabMonday = new System.Windows.Forms.Label();
            this.lblCompLabTuesday = new System.Windows.Forms.Label();
            this.lblCompLabWednesday = new System.Windows.Forms.Label();
            this.lblCompLabThursday = new System.Windows.Forms.Label();
            this.lblCompLabFriday = new System.Windows.Forms.Label();
            this.lblCompLabTotal = new System.Windows.Forms.Label();
            this.lblTutoringMonday = new System.Windows.Forms.Label();
            this.lblTutoringTuesday = new System.Windows.Forms.Label();
            this.lblTutoringWednesday = new System.Windows.Forms.Label();
            this.lblTutoringThursday = new System.Windows.Forms.Label();
            this.lblTutoringFriday = new System.Windows.Forms.Label();
            this.lblTutoringTotal = new System.Windows.Forms.Label();
            this.lblStudyHallMonday = new System.Windows.Forms.Label();
            this.lblStudyHallTuesday = new System.Windows.Forms.Label();
            this.lblStudyHallWednesday = new System.Windows.Forms.Label();
            this.lblStudyHallThursday = new System.Windows.Forms.Label();
            this.lblStudyHallFriday = new System.Windows.Forms.Label();
            this.lblStudyHallTotal = new System.Windows.Forms.Label();
            this.lblMondayTotal = new System.Windows.Forms.Label();
            this.lblTuesdayTotal = new System.Windows.Forms.Label();
            this.lblWednesdayTotal = new System.Windows.Forms.Label();
            this.lblThursdayTotal = new System.Windows.Forms.Label();
            this.lblFridayTotal = new System.Windows.Forms.Label();
            this.lblOverallTotal = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(12, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(505, 55);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Accumulated Time";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateRange
            // 
            this.lblDateRange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDateRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateRange.Location = new System.Drawing.Point(16, 64);
            this.lblDateRange.Name = "lblDateRange";
            this.lblDateRange.Size = new System.Drawing.Size(501, 18);
            this.lblDateRange.TabIndex = 1;
            this.lblDateRange.Text = "Date Range: MM/DD/YYYY - MM/DD/YYYY";
            this.lblDateRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblComputerLab
            // 
            this.lblComputerLab.Location = new System.Drawing.Point(3, 20);
            this.lblComputerLab.Name = "lblComputerLab";
            this.lblComputerLab.Size = new System.Drawing.Size(78, 19);
            this.lblComputerLab.TabIndex = 2;
            this.lblComputerLab.Text = "Computer Lab:";
            this.lblComputerLab.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(501, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shown in the format HH:MM";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTutoring
            // 
            this.lblTutoring.Location = new System.Drawing.Point(3, 40);
            this.lblTutoring.Name = "lblTutoring";
            this.lblTutoring.Size = new System.Drawing.Size(78, 19);
            this.lblTutoring.TabIndex = 4;
            this.lblTutoring.Text = "Tutoring:";
            this.lblTutoring.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(3, 80);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(78, 19);
            this.lblTotal.TabIndex = 8;
            this.lblTotal.Text = "Total:";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStudyHall
            // 
            this.lblStudyHall.Location = new System.Drawing.Point(3, 60);
            this.lblStudyHall.Name = "lblStudyHall";
            this.lblStudyHall.Size = new System.Drawing.Size(78, 19);
            this.lblStudyHall.TabIndex = 6;
            this.lblStudyHall.Text = "Study Hall:";
            this.lblStudyHall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(12, 308);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(505, 29);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnPrintTutorReports
            // 
            this.btnPrintTutorReports.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintTutorReports.Enabled = false;
            this.btnPrintTutorReports.Location = new System.Drawing.Point(12, 273);
            this.btnPrintTutorReports.Name = "btnPrintTutorReports";
            this.btnPrintTutorReports.Size = new System.Drawing.Size(505, 29);
            this.btnPrintTutorReports.TabIndex = 10;
            this.btnPrintTutorReports.Text = "Print Tutor Reports";
            this.btnPrintTutorReports.UseVisualStyleBackColor = true;
            this.btnPrintTutorReports.Click += new System.EventHandler(this.btnPrintTutorReports_Click);
            // 
            // btnPrintResourceSchedule
            // 
            this.btnPrintResourceSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintResourceSchedule.Location = new System.Drawing.Point(12, 236);
            this.btnPrintResourceSchedule.Name = "btnPrintResourceSchedule";
            this.btnPrintResourceSchedule.Size = new System.Drawing.Size(505, 29);
            this.btnPrintResourceSchedule.TabIndex = 11;
            this.btnPrintResourceSchedule.Text = "Print Schedule";
            this.btnPrintResourceSchedule.UseVisualStyleBackColor = true;
            this.btnPrintResourceSchedule.Click += new System.EventHandler(this.btnPrintResourceSchedule_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.66667F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.Controls.Add(this.lblTotal, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHall, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoring, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblComputerLab, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelMonday, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTuesday, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblWednesday, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblThursday, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFriday, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAllTotal, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabMonday, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabTuesday, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabWednesday, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabThursday, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabFriday, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCompLabTotal, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringMonday, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringTuesday, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringWednesday, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringThursday, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringFriday, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTutoringTotal, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallMonday, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallTuesday, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallWednesday, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallThursday, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallFriday, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStudyHallTotal, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblMondayTotal, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblTuesdayTotal, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblWednesdayTotal, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblThursdayTotal, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblFridayTotal, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblOverallTotal, 6, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 130);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(503, 100);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // labelMonday
            // 
            this.labelMonday.AutoSize = true;
            this.labelMonday.Location = new System.Drawing.Point(88, 0);
            this.labelMonday.Name = "labelMonday";
            this.labelMonday.Size = new System.Drawing.Size(45, 13);
            this.labelMonday.TabIndex = 9;
            this.labelMonday.Text = "Monday";
            // 
            // lblTuesday
            // 
            this.lblTuesday.AutoSize = true;
            this.lblTuesday.Location = new System.Drawing.Point(149, 0);
            this.lblTuesday.Name = "lblTuesday";
            this.lblTuesday.Size = new System.Drawing.Size(48, 13);
            this.lblTuesday.TabIndex = 10;
            this.lblTuesday.Text = "Tuesday";
            // 
            // lblWednesday
            // 
            this.lblWednesday.AutoSize = true;
            this.lblWednesday.Location = new System.Drawing.Point(203, 0);
            this.lblWednesday.Name = "lblWednesday";
            this.lblWednesday.Size = new System.Drawing.Size(64, 13);
            this.lblWednesday.TabIndex = 11;
            this.lblWednesday.Text = "Wednesday";
            // 
            // lblThursday
            // 
            this.lblThursday.AutoSize = true;
            this.lblThursday.Location = new System.Drawing.Point(292, 0);
            this.lblThursday.Name = "lblThursday";
            this.lblThursday.Size = new System.Drawing.Size(51, 13);
            this.lblThursday.TabIndex = 12;
            this.lblThursday.Text = "Thursday";
            // 
            // lblFriday
            // 
            this.lblFriday.AutoSize = true;
            this.lblFriday.Location = new System.Drawing.Point(371, 0);
            this.lblFriday.Name = "lblFriday";
            this.lblFriday.Size = new System.Drawing.Size(35, 13);
            this.lblFriday.TabIndex = 13;
            this.lblFriday.Text = "Friday";
            // 
            // lblAllTotal
            // 
            this.lblAllTotal.AutoSize = true;
            this.lblAllTotal.Location = new System.Drawing.Point(440, 0);
            this.lblAllTotal.Name = "lblAllTotal";
            this.lblAllTotal.Size = new System.Drawing.Size(31, 13);
            this.lblAllTotal.TabIndex = 14;
            this.lblAllTotal.Text = "Total";
            // 
            // lblCompLabMonday
            // 
            this.lblCompLabMonday.AutoSize = true;
            this.lblCompLabMonday.Location = new System.Drawing.Point(88, 20);
            this.lblCompLabMonday.Name = "lblCompLabMonday";
            this.lblCompLabMonday.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabMonday.TabIndex = 15;
            this.lblCompLabMonday.Text = "00:00";
            // 
            // lblCompLabTuesday
            // 
            this.lblCompLabTuesday.AutoSize = true;
            this.lblCompLabTuesday.Location = new System.Drawing.Point(149, 20);
            this.lblCompLabTuesday.Name = "lblCompLabTuesday";
            this.lblCompLabTuesday.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabTuesday.TabIndex = 16;
            this.lblCompLabTuesday.Text = "00:00";
            // 
            // lblCompLabWednesday
            // 
            this.lblCompLabWednesday.AutoSize = true;
            this.lblCompLabWednesday.Location = new System.Drawing.Point(203, 20);
            this.lblCompLabWednesday.Name = "lblCompLabWednesday";
            this.lblCompLabWednesday.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabWednesday.TabIndex = 17;
            this.lblCompLabWednesday.Text = "00:00";
            // 
            // lblCompLabThursday
            // 
            this.lblCompLabThursday.AutoSize = true;
            this.lblCompLabThursday.Location = new System.Drawing.Point(292, 20);
            this.lblCompLabThursday.Name = "lblCompLabThursday";
            this.lblCompLabThursday.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabThursday.TabIndex = 18;
            this.lblCompLabThursday.Text = "00:00";
            // 
            // lblCompLabFriday
            // 
            this.lblCompLabFriday.AutoSize = true;
            this.lblCompLabFriday.Location = new System.Drawing.Point(371, 20);
            this.lblCompLabFriday.Name = "lblCompLabFriday";
            this.lblCompLabFriday.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabFriday.TabIndex = 19;
            this.lblCompLabFriday.Text = "00:00";
            // 
            // lblCompLabTotal
            // 
            this.lblCompLabTotal.AutoSize = true;
            this.lblCompLabTotal.Location = new System.Drawing.Point(440, 20);
            this.lblCompLabTotal.Name = "lblCompLabTotal";
            this.lblCompLabTotal.Size = new System.Drawing.Size(34, 13);
            this.lblCompLabTotal.TabIndex = 20;
            this.lblCompLabTotal.Text = "00:00";
            // 
            // lblTutoringMonday
            // 
            this.lblTutoringMonday.AutoSize = true;
            this.lblTutoringMonday.Location = new System.Drawing.Point(88, 40);
            this.lblTutoringMonday.Name = "lblTutoringMonday";
            this.lblTutoringMonday.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringMonday.TabIndex = 21;
            this.lblTutoringMonday.Text = "00:00";
            // 
            // lblTutoringTuesday
            // 
            this.lblTutoringTuesday.AutoSize = true;
            this.lblTutoringTuesday.Location = new System.Drawing.Point(149, 40);
            this.lblTutoringTuesday.Name = "lblTutoringTuesday";
            this.lblTutoringTuesday.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringTuesday.TabIndex = 22;
            this.lblTutoringTuesday.Text = "00:00";
            // 
            // lblTutoringWednesday
            // 
            this.lblTutoringWednesday.AutoSize = true;
            this.lblTutoringWednesday.Location = new System.Drawing.Point(203, 40);
            this.lblTutoringWednesday.Name = "lblTutoringWednesday";
            this.lblTutoringWednesday.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringWednesday.TabIndex = 23;
            this.lblTutoringWednesday.Text = "00:00";
            // 
            // lblTutoringThursday
            // 
            this.lblTutoringThursday.AutoSize = true;
            this.lblTutoringThursday.Location = new System.Drawing.Point(292, 40);
            this.lblTutoringThursday.Name = "lblTutoringThursday";
            this.lblTutoringThursday.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringThursday.TabIndex = 24;
            this.lblTutoringThursday.Text = "00:00";
            // 
            // lblTutoringFriday
            // 
            this.lblTutoringFriday.AutoSize = true;
            this.lblTutoringFriday.Location = new System.Drawing.Point(371, 40);
            this.lblTutoringFriday.Name = "lblTutoringFriday";
            this.lblTutoringFriday.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringFriday.TabIndex = 25;
            this.lblTutoringFriday.Text = "00:00";
            // 
            // lblTutoringTotal
            // 
            this.lblTutoringTotal.AutoSize = true;
            this.lblTutoringTotal.Location = new System.Drawing.Point(440, 40);
            this.lblTutoringTotal.Name = "lblTutoringTotal";
            this.lblTutoringTotal.Size = new System.Drawing.Size(34, 13);
            this.lblTutoringTotal.TabIndex = 26;
            this.lblTutoringTotal.Text = "00:00";
            // 
            // lblStudyHallMonday
            // 
            this.lblStudyHallMonday.AutoSize = true;
            this.lblStudyHallMonday.Location = new System.Drawing.Point(88, 60);
            this.lblStudyHallMonday.Name = "lblStudyHallMonday";
            this.lblStudyHallMonday.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallMonday.TabIndex = 27;
            this.lblStudyHallMonday.Text = "00:00";
            // 
            // lblStudyHallTuesday
            // 
            this.lblStudyHallTuesday.AutoSize = true;
            this.lblStudyHallTuesday.Location = new System.Drawing.Point(149, 60);
            this.lblStudyHallTuesday.Name = "lblStudyHallTuesday";
            this.lblStudyHallTuesday.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallTuesday.TabIndex = 28;
            this.lblStudyHallTuesday.Text = "00:00";
            // 
            // lblStudyHallWednesday
            // 
            this.lblStudyHallWednesday.AutoSize = true;
            this.lblStudyHallWednesday.Location = new System.Drawing.Point(203, 60);
            this.lblStudyHallWednesday.Name = "lblStudyHallWednesday";
            this.lblStudyHallWednesday.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallWednesday.TabIndex = 29;
            this.lblStudyHallWednesday.Text = "00:00";
            // 
            // lblStudyHallThursday
            // 
            this.lblStudyHallThursday.AutoSize = true;
            this.lblStudyHallThursday.Location = new System.Drawing.Point(292, 60);
            this.lblStudyHallThursday.Name = "lblStudyHallThursday";
            this.lblStudyHallThursday.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallThursday.TabIndex = 30;
            this.lblStudyHallThursday.Text = "00:00";
            // 
            // lblStudyHallFriday
            // 
            this.lblStudyHallFriday.AutoSize = true;
            this.lblStudyHallFriday.Location = new System.Drawing.Point(371, 60);
            this.lblStudyHallFriday.Name = "lblStudyHallFriday";
            this.lblStudyHallFriday.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallFriday.TabIndex = 31;
            this.lblStudyHallFriday.Text = "00:00";
            // 
            // lblStudyHallTotal
            // 
            this.lblStudyHallTotal.AutoSize = true;
            this.lblStudyHallTotal.Location = new System.Drawing.Point(440, 60);
            this.lblStudyHallTotal.Name = "lblStudyHallTotal";
            this.lblStudyHallTotal.Size = new System.Drawing.Size(34, 13);
            this.lblStudyHallTotal.TabIndex = 32;
            this.lblStudyHallTotal.Text = "00:00";
            // 
            // lblMondayTotal
            // 
            this.lblMondayTotal.AutoSize = true;
            this.lblMondayTotal.Location = new System.Drawing.Point(88, 80);
            this.lblMondayTotal.Name = "lblMondayTotal";
            this.lblMondayTotal.Size = new System.Drawing.Size(34, 13);
            this.lblMondayTotal.TabIndex = 33;
            this.lblMondayTotal.Text = "00:00";
            // 
            // lblTuesdayTotal
            // 
            this.lblTuesdayTotal.AutoSize = true;
            this.lblTuesdayTotal.Location = new System.Drawing.Point(149, 80);
            this.lblTuesdayTotal.Name = "lblTuesdayTotal";
            this.lblTuesdayTotal.Size = new System.Drawing.Size(34, 13);
            this.lblTuesdayTotal.TabIndex = 34;
            this.lblTuesdayTotal.Text = "00:00";
            // 
            // lblWednesdayTotal
            // 
            this.lblWednesdayTotal.AutoSize = true;
            this.lblWednesdayTotal.Location = new System.Drawing.Point(203, 80);
            this.lblWednesdayTotal.Name = "lblWednesdayTotal";
            this.lblWednesdayTotal.Size = new System.Drawing.Size(34, 13);
            this.lblWednesdayTotal.TabIndex = 35;
            this.lblWednesdayTotal.Text = "00:00";
            // 
            // lblThursdayTotal
            // 
            this.lblThursdayTotal.AutoSize = true;
            this.lblThursdayTotal.Location = new System.Drawing.Point(292, 80);
            this.lblThursdayTotal.Name = "lblThursdayTotal";
            this.lblThursdayTotal.Size = new System.Drawing.Size(34, 13);
            this.lblThursdayTotal.TabIndex = 36;
            this.lblThursdayTotal.Text = "00:00";
            // 
            // lblFridayTotal
            // 
            this.lblFridayTotal.AutoSize = true;
            this.lblFridayTotal.Location = new System.Drawing.Point(371, 80);
            this.lblFridayTotal.Name = "lblFridayTotal";
            this.lblFridayTotal.Size = new System.Drawing.Size(34, 13);
            this.lblFridayTotal.TabIndex = 37;
            this.lblFridayTotal.Text = "00:00";
            // 
            // lblOverallTotal
            // 
            this.lblOverallTotal.AutoSize = true;
            this.lblOverallTotal.Location = new System.Drawing.Point(440, 80);
            this.lblOverallTotal.Name = "lblOverallTotal";
            this.lblOverallTotal.Size = new System.Drawing.Size(34, 13);
            this.lblOverallTotal.TabIndex = 38;
            this.lblOverallTotal.Text = "00:00";
            // 
            // TimeTrackerForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 349);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnPrintResourceSchedule);
            this.Controls.Add(this.btnPrintTutorReports);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDateRange);
            this.Controls.Add(this.lblHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeTrackerForm";
            this.Text = "Time Tracker";
            this.Load += new System.EventHandler(this.TimeTrackerForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblDateRange;
        private System.Windows.Forms.Label lblComputerLab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTutoring;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblStudyHall;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnPrintTutorReports;
        private System.Windows.Forms.Button btnPrintResourceSchedule;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelMonday;
        private System.Windows.Forms.Label lblTuesday;
        private System.Windows.Forms.Label lblWednesday;
        private System.Windows.Forms.Label lblThursday;
        private System.Windows.Forms.Label lblFriday;
        private System.Windows.Forms.Label lblAllTotal;
        private System.Windows.Forms.Label lblCompLabMonday;
        private System.Windows.Forms.Label lblCompLabTuesday;
        private System.Windows.Forms.Label lblCompLabWednesday;
        private System.Windows.Forms.Label lblCompLabThursday;
        private System.Windows.Forms.Label lblCompLabFriday;
        private System.Windows.Forms.Label lblCompLabTotal;
        private System.Windows.Forms.Label lblTutoringMonday;
        private System.Windows.Forms.Label lblTutoringTuesday;
        private System.Windows.Forms.Label lblTutoringWednesday;
        private System.Windows.Forms.Label lblTutoringThursday;
        private System.Windows.Forms.Label lblTutoringFriday;
        private System.Windows.Forms.Label lblTutoringTotal;
        private System.Windows.Forms.Label lblStudyHallMonday;
        private System.Windows.Forms.Label lblStudyHallTuesday;
        private System.Windows.Forms.Label lblStudyHallWednesday;
        private System.Windows.Forms.Label lblStudyHallThursday;
        private System.Windows.Forms.Label lblStudyHallFriday;
        private System.Windows.Forms.Label lblStudyHallTotal;
        private System.Windows.Forms.Label lblMondayTotal;
        private System.Windows.Forms.Label lblTuesdayTotal;
        private System.Windows.Forms.Label lblWednesdayTotal;
        private System.Windows.Forms.Label lblThursdayTotal;
        private System.Windows.Forms.Label lblFridayTotal;
        private System.Windows.Forms.Label lblOverallTotal;
    }
}