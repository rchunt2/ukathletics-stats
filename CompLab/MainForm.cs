﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Runtime.InteropServices;
using BLL = UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
    public partial class MainForm : Form
    {
        public int IdleTimeWarning;
        public int IdleTimeTerminate;
        public static int ApplicationBarHeight { get { return Int32.Parse(BLL.Setting.GetByKey("ComputerLabBarHeight").SettingValue); } }
        private readonly BLL.ResourceType eResourceTypeTutor = BLL.ResourceType.Get("Tutor");
        
        public BLL.Attendance CurrentAttendance;
        public ClientLogic.ClientState CurrentResourceState;
        public Timer timerCheck;
        public LoginForm frmLogin;
        public MessagesForm frmMessages;
        public TimeTrackerForm frmTimeTracker;
        public IdleWarning frmIdleWarning;

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == ClientLogic.uCallBack)
            {
                switch (m.WParam.ToInt32())
                {
                    case (int)ClientLogic.ApplicationBarNotify.ABN_POSCHANGED:
                        ClientLogic.ApplicationBarSetPosition(this.Size.Width, this.Size.Height);
                        break;
                }
            }
            base.WndProc(ref m);
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= (~0x00C00000); // WS_CAPTION
                cp.Style &= (~0x00800000); // WS_BORDER
                cp.ExStyle = 0x00000080 | 0x00000008; // WS_EX_TOOLWINDOW | WS_EX_TOPMOST
                return cp;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            AutoScaleBaseSize = new Size(5, 13);
            ClientSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, ApplicationBarHeight);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Just to cover all the bases, we should always run some code if the application exits.
            Application.ApplicationExit += Application_ApplicationExit;
            ClientLogic.ApplicationBarRegister(this.Handle, this.Size.Width, this.Size.Height);
            ClientLogic.TurnOffHotKeys();

            frmLogin = new LoginForm(this);
            frmMessages = new MessagesForm(this);
            frmTimeTracker = new TimeTrackerForm(this);
            frmIdleWarning = new IdleWarning(this);
            timerCheck = new Timer();
            timerCheck.Tick += timerCheck_Tick;
            timerCheck.Interval = 1000;
            timerCheck.Start();
            lblName.Text = ""; // Blank the label for a cleaner look and feel.
            lblTime.Text = "";
            pbLogo.Image = Image.FromFile("./_images/ukentucky_logo.png");
            btnLogout.Image = Image.FromFile("./_images/TestButton.png");
            CurrentResourceState = ClientLogic.ClientState.Login;
            IdleTimeWarning = Int32.Parse(BLL.Setting.GetByKey("ComputerLabIdleWarningMinutes").SettingValue);
            IdleTimeTerminate = Int32.Parse(BLL.Setting.GetByKey("ComputerLabIdleLogoffMinutes").SettingValue);
        }

        void Application_ApplicationExit(object sender, EventArgs e)
        {
            Boolean isActive = (CurrentResourceState == ClientLogic.ClientState.Active);
            Boolean hasAttendance = (CurrentAttendance != null);
            if (isActive && hasAttendance)
            {
                // Logout the resource if one exists?    
                CurrentAttendance.LogOut(ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                ClientLogic.RestartComputer();
            }
        }

        void timerCheck_Tick(object sender, EventArgs e)
        {
            int IdleSeconds = ClientLogic.GetUserIdleTimeInSeconds();
            int IdleMinutes = IdleSeconds / 60;
            try
            {
                
            }
            catch (Exception ex)
            {
                BLL.Error.LogException(ex,CurrentAttendance.Schedule.Resource,"1.1.1.1");
            }
            
            switch (CurrentResourceState)
            {
                case ClientLogic.ClientState.Active:
                    // NOTE: All of our previous session stuff from earlier
                    if (CurrentAttendance != null && CurrentAttendance.Schedule.Resource.ResourceType.ResourceTypeName != eResourceTypeTutor.ResourceTypeName)
                    {
                        // Check to see if the current user is logged in elsewhere
                        var currentSession = BLL.Session.Get(CurrentAttendance.Schedule.SessionID);
                        BLL.Attendance[] otherSessions = BLL.Attendance.GetActive(CurrentAttendance.Schedule.Resource);
                        Boolean loggedInElsewhere = false;
                        Boolean stillActive = true;
                        if (otherSessions.Count() == 0)
                        {
                            stillActive = false;
                            loggedInElsewhere = false;
                            // Check to see if the user's current session has been terminated elsewhere
                            if (!stillActive)
                            {
                                timerCheck.Interval = 60000;
                                CurrentAttendance.LogOut(ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                                timerCheck.Stop();
                                frmLogin = new LoginForm(this, "Logout");
                                frmLogin.Show();
                                break;
                            }
                        }
                        else
                        {
                            foreach (BLL.Attendance curAttendance in otherSessions)
                            {
                                if (curAttendance.AttendanceID != CurrentAttendance.AttendanceID && CurrentAttendance.Schedule.Session.IsOccuring)
                                {
                                    loggedInElsewhere = true;
                                    //break;
                                }
                                if (curAttendance.AttendanceID == CurrentAttendance.AttendanceID && curAttendance.Schedule.Session.IsOccuring == false)
                                {
                                    stillActive = false;
                                }
                            }
                            // This user has been logged into another location and must be logged out of this session
                            if ((otherSessions.Length > 0) && loggedInElsewhere) // && (CurrentResourceState != ClientLogic.ClientState.Login))
                            //if (currentSession.SessionDateTimeEnd.Date != DateTime.Today && currentSession.SessionDateTimeEnd.TimeOfDay != new TimeSpan(23, 59, 00))
                            {
                                timerCheck.Interval = 60000;
                                //var temp = "Whats up now \r\n otherSessions.Length: " + otherSessions.Length + "; \r\n loggedInElsewhere: " + loggedInElsewhere + "; \r\n CurrentResourceState: " + CurrentResourceState;
                                //MessageBox.Show("Your session has been ended because your account has been logged in elsewhere.", "Notice!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //frmIdleWarning = new IdleWarning(this, "Logout");
                                //frmIdleWarning.TopMost = true;
                                //frmIdleWarning.Activate();
                                //frmIdleWarning.Show();
                                CurrentAttendance.LogOut(ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                                timerCheck.Stop();
                                frmLogin = new LoginForm(this, "Elsewhere");
                                frmLogin.Show();
                                break;
                                //CurrentAttendance.LogOut();
                                //ClientLogic.RestartComputer();
                                //Application.Exit();
                            }
                        }
                    }

                    lblName.Text = CurrentAttendance.Schedule.Resource.ResourceNameDisplayLastFirst;
                    lblName.BackColor = SystemColors.Control;
                    lblTime.Text = "Login Time: " + CurrentAttendance.AttendanceTimeIn.ToShortTimeString();
                    // Determine if Warning needs to be displayed.
                    // If the user has been idle for a long period of time, (configurable) display the warning prompt.
                    if (IdleMinutes >= (IdleTimeTerminate - IdleTimeWarning))
                    {
                        if (CurrentResourceState == ClientLogic.ClientState.Active)
                        {
                            CurrentResourceState = ClientLogic.ClientState.Idle;
                            String Prompt = "You have been inactive for some time." + Environment.NewLine + Environment.NewLine + "Please resume work to prevent your session from ending.";
                            //+Environment.NewLine + Environment.NewLine
                            //    + "IdleMinutes: " + IdleMinutes + "; IdleTimeTerminate: " + IdleTimeTerminate + "; IdleTimeWarning: " + IdleTimeWarning;
                            lblName.Text = lblName.Text + "You are inactive. Resume work to prevent your session from ending.";
                            lblName.BackColor = System.Drawing.Color.Red;
                            frmIdleWarning = new IdleWarning(this,"Idle");
                            frmIdleWarning.TopMost = true;
                            frmIdleWarning.Activate();
                            frmIdleWarning.Show();
                            //DialogResult dr = MessageBox.Show(Prompt, "Notice!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //if (dr == System.Windows.Forms.DialogResult.OK)
                            //{
                            //    CurrentResourceState = ClientLogic.ClientState.Active;
                            //}
                        }
                    }
                    break;
                case ClientLogic.ClientState.Idle:
                    lblName.Text = "Idle Time: " + TimeSpan.FromSeconds(ClientLogic.GetUserIdleTimeInSeconds()).ToString();
                    // Determine if Session needs to be terminated.
                    if (IdleMinutes >= IdleTimeTerminate)
                    {
                        frmIdleWarning.Dispose();
                        CurrentAttendance.LogOut(ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                        CurrentResourceState = ClientLogic.ClientState.Login;
                    }
                    //MessageBox.Show("We have been idle for: " + IdleMinutes, "timerCheck_Tick", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    break;
                case ClientLogic.ClientState.Login:
                    // No user is currently logged in.  Allow a user to login to the system.
                    timerCheck.Stop();
                    frmLogin = new LoginForm(this);
                    frmLogin.Show();
                    break;
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            String Prompt = "This will shut down all running applications!" + Environment.NewLine + Environment.NewLine + "If you have not saved, please click \"No\" and save your work!" + Environment.NewLine + Environment.NewLine + "Continue?";
            DialogResult dr = MessageBox.Show(Prompt, "Notice!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes)
            {
                timerCheck.Stop();
                CurrentAttendance.LogOut(ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                DialogResult drTimeTracker = frmTimeTracker.ShowDialog();
                ClientLogic.RestartComputer();
                Application.Exit();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClientLogic.RestartComputer();
            Application.Exit();
        }
    }
}
