﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
	public partial class MessagesForm : Form
	{
		public MainForm CompLabMainForm { get; set; }
		public Resource ResourceObj { get; set; }

		public MessagesForm(MainForm Parent)
		{
			InitializeComponent();
			CompLabMainForm = Parent;
		}

		public void BindMessages()
		{
			MessageInbox[] eMessageInboxes = MessageInbox.Get(ResourceObj);

			foreach (var eMessageInbox in eMessageInboxes)
			{
				var eMessage = eMessageInbox.Message;
				Resource eResourceFrom = eMessage.ResourceFrom;
				String strResourceFromName = eResourceFrom.ResourceNameFirst.Trim() + " " + eResourceFrom.ResourceNameLast.Trim();
				String Subject = eMessage.GetSubject();
				String Expires = "";
				if (eMessageInbox.MessageInboxDateExpires.HasValue)
				{
					Expires = eMessageInbox.MessageInboxDateExpires.Value.ToShortDateString();
				}
				else
				{
					Expires = "Never";
				}
				
 
				var FieldList = new List<string>();

				FieldList.Add(eMessageInbox.MessageInboxID.ToString());
				if (eMessageInbox.MessageInboxDateRead.HasValue)
				{
					FieldList.Add(eMessageInbox.MessageInboxDateRead.Value.ToShortDateString());
				}
				else
				{
					FieldList.Add("UNREAD");
				}

				FieldList.Add(strResourceFromName);
				FieldList.Add(Subject);
				FieldList.Add(Expires);
				var lvi = new ListViewItem(FieldList.ToArray());
				lvMessages.Items.Add(lvi);
			}
		}

		private void lvMessages_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lvMessages.SelectedItems.Count > 0)
			{
				ListViewItem lvi = lvMessages.SelectedItems[0];
				Guid MessageInboxID = Guid.Parse(lvi.Text);
				var context = new STATSEntities();
				var eMessageInbox = (from MessageInbox obj in context.MessageInboxes where obj.MessageInboxID == MessageInboxID select obj).Single();
				var eMessage = eMessageInbox.Message;
				txtBody.Text = eMessage.GetBody();
				if (!eMessageInbox.MessageInboxDateRead.HasValue)
				{
					eMessageInbox.MessageInboxDateRead = DateTime.Now;
					context.SaveChanges();
					lvi.SubItems[1].Text = DateTime.Now.ToShortDateString();    
				}
			}
		}

		private void MessagesForm_Load(object sender, EventArgs e)
		{
			BindMessages();
		}

		private void MessagesForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			// If all the messages haven't been read yet, display a messagebox.
			if (MessageInbox.NumberMessagesUnread(ResourceObj) > 0)
			{
				MessageBox.Show("Please read all unread messages to continue.", "Unread Messages", MessageBoxButtons.OK, MessageBoxIcon.Error);
				e.Cancel = true;
			}
		}
	}
}
