﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UK.STATS.STATSModel;
using Resource = UK.STATS.STATSModel.Resource;

namespace UK.STATS.CompLab
{
    public partial class PrintScheduleForm : Form
    {
        private System.Drawing.Color ColorClassBorder;
        private System.Drawing.Color ColorTutoringCATSBorder;
        private System.Drawing.Color ColorTutoringFootballBorder;
        private System.Drawing.Color ColorCompLabBorder;
        private System.Drawing.Color ColorFreeFormBorder;
        private System.Drawing.Color ColorStudyHallCATSBorder;
        private System.Drawing.Color ColorStudyHallFootballBorder;
        private System.Drawing.Color ColorDefaultBorder;

        private System.Drawing.Color ColorClass;
        private System.Drawing.Color ColorTutoringCATS;
        private System.Drawing.Color ColorTutoringFootball;
        private System.Drawing.Color ColorCompLab;
        private System.Drawing.Color ColorFreeForm;
        private System.Drawing.Color ColorStudyHallCATS;
        private System.Drawing.Color ColorStudyHallFootball;
        private System.Drawing.Color ColorDefault;

        private DateTime RangeStart;
        private DateTime RangeEnd;

        private Guid _resourceID;
        public Guid GroupID;

        private PrintDocument printDocument1 = new PrintDocument();
        private PrintDialog printDialog1 = new PrintDialog();
        private Bitmap memoryImage;

        public Resource ResourceObj { get; set; }

        public Guid ResourceID
        {
            get { return _resourceID; }
            set { _resourceID = value; }
        }

        private List<Resource> _resources
        {
            get
            {
                var eResources = new List<Resource>();
                foreach (Guid rid in _resourceIDs)
                {
                    Resource eResource = Resource.Get(rid);
                    eResources.Add(eResource);
                }
                return eResources;
            }
            set
            {
                var resourceIDList = new List<Guid>();
                List<Resource> eResources = value;
                foreach (var eResource in eResources)
                {
                    resourceIDList.Add(eResource.ResourceID);
                }
                _resourceIDs = resourceIDList;
            }
        }

        private List<Guid> _resourceIDs { get; set; }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                return STATSModel.ResourceType.Get("Student");
            }
        }

        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                return STATSModel.ResourceType.Get("Tutor");
            }
        }


        public PrintScheduleForm()
        {
            InitializeComponent();
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            LoadColors();
        }

        private void PrintScheduleForm_Load(object sender, EventArgs e)
        {
            ResourceRadScheduler.ReadOnly = true;
            ResourceObj = Resource.Get(ResourceID);
            if (ResourceObj.Groups.Any())
            {
                GroupID = ResourceObj.Groups[0].GroupID;
            }
            lblResourceName.Text = ResourceObj.ResourceNameDisplayLastFirst;
            RunScheduler();
        }

        public void LoadColors()
        {
            // Get the configuration settings for the colors of the individual calendar items.
            String StrColorClass = Setting.GetByKey("ScheduleColorClass").SettingValue;
            String StrColorTutoringCATS = Setting.GetByKey("ScheduleColorTutoringCATS").SettingValue;
            String StrColorTutoringFootball = Setting.GetByKey("ScheduleColorTutoringFootball").SettingValue;
            String StrColorCompLab = Setting.GetByKey("ScheduleColorCompLab").SettingValue;
            String StrColorFreeForm = Setting.GetByKey("ScheduleColorFreeForm").SettingValue;
            String StrColorStudyHallCATS = Setting.GetByKey("ScheduleColorStudyHallCATS").SettingValue;
            String StrColorStudyHallFootball = Setting.GetByKey("ScheduleColorStudyHallFootball").SettingValue;
            String StrColorDefault = Setting.GetByKey("ScheduleColorDefault").SettingValue;
            
            ColorClassBorder = System.Drawing.Color.FromName(StrColorClass);
            ColorTutoringCATSBorder = System.Drawing.Color.FromName(StrColorTutoringCATS);
            ColorTutoringFootballBorder = System.Drawing.Color.FromName(StrColorTutoringFootball);
            ColorCompLabBorder = System.Drawing.Color.FromName(StrColorCompLab);
            ColorFreeFormBorder = System.Drawing.Color.FromName(StrColorFreeForm);
            ColorStudyHallCATSBorder = System.Drawing.Color.FromName(StrColorStudyHallCATS);
            ColorStudyHallFootballBorder = System.Drawing.Color.FromName(StrColorStudyHallFootball);
            ColorDefaultBorder = System.Drawing.Color.FromName(StrColorDefault);

            // Set the fill color of the calendar items.
            ColorClass = System.Drawing.Color.FromName("Light" + StrColorClass);
            ColorTutoringCATS = System.Drawing.Color.FromName("Light" + StrColorTutoringCATS);
            ColorTutoringFootball = System.Drawing.Color.FromName("Light" + StrColorTutoringFootball);
            ColorCompLab = System.Drawing.Color.FromName("Light" + StrColorCompLab);
            ColorFreeForm = System.Drawing.Color.FromName("Light" + StrColorFreeForm);
            ColorStudyHallCATS = System.Drawing.Color.FromName("Light" + StrColorStudyHallCATS);
            ColorStudyHallFootball = System.Drawing.Color.FromName("Light" + StrColorStudyHallFootball);
            ColorDefault = System.Drawing.Color.FromName("Light" + StrColorDefault);

            AppointmentBackgroundInfo appointmentBackgroundInfo = new AppointmentBackgroundInfo(12, "Class", ColorClass, ColorClass, ColorClass, ColorClassBorder);
            AppointmentBackgroundInfo backgroundInfo = this.ResourceRadScheduler.Backgrounds[0] as AppointmentBackgroundInfo;
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(12, "Class", ColorClass));

            appointmentBackgroundInfo = new AppointmentBackgroundInfo(13, "Tutoring", ColorTutoringCATS, ColorTutoringCATS, ColorTutoringCATS, ColorTutoringCATSBorder);
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(13, "Tutoring", ColorTutoring));

            appointmentBackgroundInfo = new AppointmentBackgroundInfo(14, "CompLab", ColorCompLab, ColorCompLab, ColorCompLab, ColorCompLabBorder);
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(14, "CompLab", ColorCompLab));

            appointmentBackgroundInfo = new AppointmentBackgroundInfo(15, "FreeForm", ColorFreeForm, ColorFreeForm, ColorFreeForm, ColorFreeFormBorder);
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(15, "FreeForm", ColorFreeForm));

            appointmentBackgroundInfo = new AppointmentBackgroundInfo(16, "StudyHall", ColorStudyHallCATS, ColorStudyHallCATS, ColorStudyHallCATS, ColorStudyHallCATSBorder);
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(16, "StudyHall", ColorStudyHall));

            appointmentBackgroundInfo = new AppointmentBackgroundInfo(17, "Default", ColorDefault, ColorDefault, ColorDefault, ColorDefaultBorder);
            appointmentBackgroundInfo.ShadowStyle = backgroundInfo.ShadowStyle;
            appointmentBackgroundInfo.ShadowColor = backgroundInfo.ShadowColor;
            appointmentBackgroundInfo.ShadowWidth = backgroundInfo.ShadowWidth;
            appointmentBackgroundInfo.BorderBoxStyle = backgroundInfo.BorderBoxStyle;
            appointmentBackgroundInfo.BorderColor2 = backgroundInfo.BorderColor2;
            appointmentBackgroundInfo.DateTimeColor = backgroundInfo.DateTimeColor;
            appointmentBackgroundInfo.DateTimeFont = backgroundInfo.DateTimeFont;
            appointmentBackgroundInfo.ForeColor = Color.Black;
            this.ResourceRadScheduler.Backgrounds.Add(appointmentBackgroundInfo);
            //this.ResourceRadScheduler.Backgrounds.Add(new AppointmentBackgroundInfo(17, "Default", ColorDefault));

            //// Note: The scheduler only displays things that start AFTER this range value.
            //// Note: If it starts at 8AM, and this value is 8AM, it WILL NOT DISPLAY.
            //// Note: The same goes for the RangeEnd value, just in reverse.
            //int diff = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            //if (diff < 0)
            //    diff += 7;
            //RangeStart = DateTime.Today.AddDays(-1 * diff).Date;
            //RangeEnd = RangeStart.AddDays(7);
            //ResourceRadScheduler.AccessibleInterval.Start = RangeStart;
            //ResourceRadScheduler.AccessibleInterval.End = RangeEnd;

            //var temp = ResourceRadScheduler.GetWeekView();
            //temp.RulerStartScale = 8;
            //temp.RulerEndScale = 23;
            //temp.RangeFactor = ScaleRange.HalfHour;
            //temp.ShowAllDayArea = false;
            //temp.WorkTime = new TimeInterval(new TimeSpan(8, 0, 0), new TimeSpan(23, 0, 0));
            //temp.StartDate = RangeStart.AddDays(1);
        }

        public void RunScheduler()
        {
            String strSemesterDateBegin = Setting.GetByKey("SemesterDateBegin").SettingValue;
            String strSemesterDateEnd = Setting.GetByKey("SemesterDateEnd").SettingValue;
            DateTime SemesterDateBegin = DateTime.Parse(strSemesterDateBegin);
            DateTime SemesterDateBeginFollowingWeek = SemesterDateBegin.AddDays(7);
            DateTime SemesterDateEnd = DateTime.Parse(strSemesterDateEnd);
            if (DateTime.Today > SemesterDateBeginFollowingWeek) { SemesterDateBeginFollowingWeek = DateTime.Today; }

            // If we are within two weeks of the end of the semester we need to set the calendar to look at the current week 10-5-12 ASW
            var currentCultureCalendar = System.Threading.Thread.CurrentThread.CurrentCulture;
            int currentWeekNumber = currentCultureCalendar.Calendar.GetWeekOfYear(DateTime.Today, currentCultureCalendar.DateTimeFormat.CalendarWeekRule, currentCultureCalendar.DateTimeFormat.FirstDayOfWeek);
            int endSemesterWeekNumber = currentCultureCalendar.Calendar.GetWeekOfYear(SemesterDateEnd, currentCultureCalendar.DateTimeFormat.CalendarWeekRule, currentCultureCalendar.DateTimeFormat.FirstDayOfWeek);
            if ((endSemesterWeekNumber - currentWeekNumber) <= 2)
                while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(-1); }
            else
                while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(1); }
            //while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.Subtract(new TimeSpan(1,0,0,0)); }

            // Walk backwards from today until we're on a Monday
            //while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(1); }
            //ResourceRadScheduler.AccessibleInterval.Start = SemesterDateBeginFollowingWeek;
            //ResourceRadScheduler.AccessibleInterval.End = SemesterDateBeginFollowingWeek.AddDays(7);
            RangeStart = SemesterDateBeginFollowingWeek;
            RangeEnd = SemesterDateBeginFollowingWeek.AddDays(7);
            var temp = ResourceRadScheduler.GetWeekView();
            temp.RulerStartScale = 8;
            temp.RulerEndScale = 23;
            temp.RangeFactor = ScaleRange.HalfHour;
            temp.ShowAllDayArea = false;
            temp.WorkTime = new TimeInterval(new TimeSpan(8, 0, 0), new TimeSpan(23, 0, 0));
            temp.StartDate = RangeStart.AddDays(1);

            var ResourceIDList = new List<Guid>();
            var eResources = new List<Resource>();
            var eResource = Resource.Get(ResourceID);
            eResources.Add(eResource);

            _resources = eResources;
            SchedulerBindingDataSource dataSource = new SchedulerBindingDataSource();
            AppointmentMappingInfo appointmentMappingInfo = new AppointmentMappingInfo();
            appointmentMappingInfo.Start = "Start";
            appointmentMappingInfo.End = "End";
            appointmentMappingInfo.Summary = "Subject";
            appointmentMappingInfo.Description = "Description";
            appointmentMappingInfo.Location = "Location";
            appointmentMappingInfo.UniqueId = "Id";
            SchedulerMapping idMapping = appointmentMappingInfo.FindByDataSourceProperty("id");
            dataSource.EventProvider.Mapping = appointmentMappingInfo;
            dataSource.EventProvider.DataSource = BindScheduler(eResources.ToArray());
            ResourceRadScheduler.DataSource = dataSource;
            ResourceRadScheduler.DataBind();
        }

        public List<Appointment> BindScheduler(IEnumerable<Resource> eResources)
        {
            SessionType eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
            SessionType eSessionTypeTutoringFootball = SessionType.GetByName("Tutoring - Football");
            SessionType eSessionTypeClass = SessionType.GetByName("Class");
            SessionType eSessionTypeNote = SessionType.GetByName("Note");
            var AppointmentList = new List<Appointment>();

            foreach (var eResource in eResources)
            {
                // Class Sessions
                // Note: Class Sessions used to have Session and Schedule records associated with them.
                // Note: These are now dynamically derived when requested.
                CourseSection[] RegisteredCourses = ResourceRegistration.GetRegisteredCourses(eResource);
                foreach (var RegisteredCourse in RegisteredCourses)
                {
                    Session[] Sessions = ResourceRegistration.GetCourseSectionSessions(RegisteredCourse);
                    String strResourceName = eResource.ResourceNameFirst + " " + eResource.ResourceNameLast;
                    foreach (var ClassSession in Sessions)
                    {
                        Boolean AfterStart = (ClassSession.SessionDateTimeStart >= RangeStart);
                        Boolean BeforeEnd = (ClassSession.SessionDateTimeEnd <= RangeEnd);
                        if (AfterStart && BeforeEnd) // Check if the session falls within the expected time range.
                        {
                            var eCourse = RegisteredCourse.Course;
                            var eSubject = eCourse.Subject;

                            String strSubject = eSubject.SubjectNameShort;
                            String strCourse = eCourse.CourseNumber;
                            String strSection = "-" + RegisteredCourse.CourseSectionNumber;
                            String strBuilding = RegisteredCourse.CourseSectionBuilding;
                            String strRoom = RegisteredCourse.CourseSectionRoom;
                            String strSessionTime = ClassSession.SessionDateTimeStart.ToShortTimeString() + "-" + ClassSession.SessionDateTimeEnd.ToShortTimeString();

                            // NOTE: SessionTypeName isn't displayed.  Why?  Because it's color coded, that's why.
                            String ApptText = strBuilding + " " + strRoom + " " + strSessionTime + "\r\n" + strResourceName + " " + " " + strSubject + strCourse + strSection;

                            if (ClassSession.SessionIsScheduled)
                            {
                                ApptText = "SessionType=" + eSessionTypeClass.SessionTypeName + "$$$$" + ApptText;
                            }

                            //var a = new Appointment(Guid.NewGuid(), ClassSession.SessionDateTimeStart, ClassSession.SessionDateTimeEnd, ApptText);
                            var a = new Appointment(ClassSession.SessionDateTimeStart, ClassSession.SessionDateTimeEnd, ApptText, Guid.NewGuid().ToString());
                            AppointmentList.Add(a);
                        }
                    }
                }

                Schedule[] eSchedules = Schedule.Get(eResource, RangeStart, RangeEnd);
                foreach (Schedule eSchedule in eSchedules)
                {
                    var eSession = eSchedule.Session;
                    var eSessionType = eSession.SessionType;

                    String strAnnotation = "";
                    String strSubject = "";
                    String strCourse = "";
                    String strSection = "";
                    String strBuilding = "";
                    String strRoom = "";
                    String strSessionTime = eSession.SessionDateTimeStart.ToShortTimeString() + "-" + eSession.SessionDateTimeEnd.ToShortTimeString();
                    String strTutorInfo = "";
                    String strResourceName = eResource.ResourceNameFirst + " " + eResource.ResourceNameLast;

                    if (eSession.SessionTypeID == eSessionTypeNote.SessionTypeID)
                    {
                        // If this session falls outside the range (expected for FreeForm events)
                        if (eSession.SessionDateTimeStart < RangeStart || eSession.SessionDateTimeStart > RangeEnd)
                        {
                            // We'll need to "pretend" that it falls within the current week.
                            DateTime OldDateStart = eSession.SessionDateTimeStart.Date;
                            DayOfWeek DayStart = OldDateStart.DayOfWeek;
                            TimeSpan OldTimeStart = eSession.SessionDateTimeStart.TimeOfDay;
                            TimeSpan OldTimeEnd = eSession.SessionDateTimeEnd.TimeOfDay;

                            DateTime dtIteratorStart = RangeStart;
                            while (dtIteratorStart.DayOfWeek != DayStart)
                            {
                                dtIteratorStart = dtIteratorStart.AddDays(1);
                            }

                            DateTime dtIteratorEnd = dtIteratorStart;
                            while (dtIteratorEnd.DayOfWeek != DayStart)
                            {
                                dtIteratorEnd = dtIteratorEnd.AddDays(1);
                            }

                            DateTime NewDateTimeStart = dtIteratorStart.Date.Add(OldTimeStart);
                            DateTime NewDateTimeEnd = dtIteratorEnd.Date.Add(OldTimeEnd);
                            eSession.SessionDateTimeStart = NewDateTimeStart;
                            eSession.SessionDateTimeEnd = NewDateTimeEnd;
                        }
                    }

                    // Free-Form Sessions
                    Annotation[] eAnnotations = Annotation.Get(eSchedule.ScheduleID);
                    if (eAnnotations.Length > 0)
                    {
                        foreach (Annotation eAnnotation in eAnnotations)
                        {
                            strAnnotation += eAnnotation.AnnotationText + " ";
                        }
                    }

                    // Tutoring Sessions
                    if (eSessionType.SessionTypeID == eSessionTypeTutoring.SessionTypeID || eSessionType.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID)
                    {
                        //strTutorInfo = "Tutoring: ";
                        STATSModel.Resource[] eResourcesTutoring = eSession.GetResourceInSession();
                        int NumberScheduled = eResourcesTutoring.Count();
                        if (NumberScheduled > 1) // There's more than a single tutor and a student, so it's a group session.
                        {
                            STATSModel.Resource oResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                            if (NumberScheduled > 2)
                            {
                                if (eResource.ResourceID == oResourceTutor.ResourceID)
                                {
                                    strTutorInfo += "Group Tutoring Session";
                                }
                                else
                                {
                                    strTutorInfo += oResourceTutor.ResourceNameFirst + " " + oResourceTutor.ResourceNameLast;
                                }
                            }
                            else
                            {
                                foreach (var eResourceInSession in eResourcesTutoring)
                                {
                                    if (eResourceInSession.ResourceID != eResource.ResourceID)
                                    {
                                        strTutorInfo += " " + eResourceInSession.ResourceNameFirst + " " + eResourceInSession.ResourceNameLast;
                                    }
                                }
                            }
                            if (eSession.HasAdditionalData())
                            {
                                STATSModel.SessionTutor eSessionTutor = eSession.GetAdditionalData();
                                if (eSessionTutor.CourseID.HasValue)
                                {
                                    Course eCourse = eSessionTutor.Course;
                                    Subject eSubject = eCourse.Subject;
                                    strSubject = eSubject.SubjectNameShort;
                                    strCourse = eCourse.CourseNumber;
                                }
                            }
                        }
                    }

                    // NOTE: SessionTypeName isn't displayed.  Why?  Because it's color coded, that's why.
                    String ApptText = strBuilding + " " + strRoom + " " + strSessionTime + "\r\n" + strResourceName + " " + strAnnotation + " " + strSubject + strCourse + strSection + "\r\n" +  strTutorInfo;

                    if (!eSession.SessionIsScheduled)
                    {
                        ApptText = "SessionType=U" + eSessionType.SessionTypeName + "$$$$" + ApptText;
                    }
                    else
                    {
                        ApptText = "SessionType=" + eSessionType.SessionTypeName + "$$$$" + ApptText;
                    }

                    //var a = new Appointment(eSchedule.ScheduleID, eSession.SessionDateTimeStart, eSession.SessionDateTimeEnd, ApptText);
                    var a = new Appointment(eSession.SessionDateTimeStart, eSession.SessionDateTimeEnd, ApptText, eSchedule.ScheduleID.ToString());
                    AppointmentList.Add(a);
                }
            }
            return AppointmentList;
        }

        private void ResourceRadScheduler_AppointmentFormatting(object sender, SchedulerAppointmentEventArgs e)
        {
            e.AppointmentElement.TextWrap = true;
            try
            {
                if (e.Appointment.Summary.Length > 0 && e.Appointment.Summary.IndexOf("$$$$") > 0)
                {
                    string classType = e.Appointment.Summary.Substring(0, e.Appointment.Summary.IndexOf("$$$$"));
                    classType = classType.Substring(classType.IndexOf("=") + 1);
                    e.Appointment.Summary = e.Appointment.Summary.Replace(e.Appointment.Summary.Substring(0, e.Appointment.Summary.IndexOf("$$$$") + 4), "");

                    //// By Default, no schedule entries should be able to be edited.
                    e.Appointment.AllowEdit = false;
                    e.Appointment.AllowDelete = false;
                    e.AppointmentElement.ShowAppointmentDescription = true;
                    e.AppointmentElement.NumberOfColors = 1;
                    e.AppointmentElement.BorderGradientStyle = Telerik.WinControls.GradientStyles.Solid;
                    e.AppointmentElement.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                    e.AppointmentElement.ZIndex = 0;
                    e.AppointmentElement.TitleFormat = this.ResourceRadScheduler.ActiveView.AppointmentTitleFormat;

                    switch (classType.ToLower())
                    {
                        case "tutoring - cats":
                            e.Appointment.BackgroundId = 13;
                            break;
                        case "tutoring - football":
                            e.Appointment.BackgroundId = 23;
                            break;
                        case "class":
                            e.Appointment.BackgroundId = 12;
                            break;
                        case "computer lab":
                            e.Appointment.BackgroundId = 14;
                            break;
                        case "note":
                            e.Appointment.BackgroundId = 15;
                            break;
                        case "study hall - cats":
                            e.Appointment.BackgroundId = 16;
                            break;
                        case "study hall - football":
                            e.Appointment.BackgroundId = 26;
                            break;
                        default:
                            e.Appointment.BackgroundId = 17;
                            break;
                    }
                }
            }
            catch (Exception)
            {
                // do something
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //this.ResourceRadScheduler.Print();
            StartPrint();
            //CaptureScreen();
            //this.printDocument1.Print();
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        private void CaptureScreen()
        {
            this.btnPrint.Visible = false;
            Graphics myGraphics = this.ResourceRadScheduler.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            //memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
            memoryGraphics.CopyFromScreen((this.label1.PointToScreen(Point.Empty).X), (this.label1.PointToScreen(Point.Empty).Y), 0, 0, s);
        }

        public void StartPrint()
        {
            this.printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            //this.streamToPrint = streamToPrint;
            //this.streamType = streamType;
            System.Windows.Forms.PrintDialog PrintDialog1 = new PrintDialog();
            PrintDialog1.AllowSomePages = true;
            PrintDialog1.ShowHelp = true;
            PrintDialog1.Document = printDocument1;
            CaptureScreen();
            DialogResult result = PrintDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument1.Print();
                //printDocument1.Print();
                //docToPrint.Print();
            }
            this.btnPrint.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}