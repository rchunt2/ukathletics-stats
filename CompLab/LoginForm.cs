﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using BLL = UK.STATS.STATSModel;

namespace UK.STATS.CompLab
{
    public partial class LoginForm : Form
    {
        private readonly BLL.ResourceType eResourceTypeTutor = BLL.ResourceType.Get("Tutor");

        public Boolean AllowClose { get; set; }

        public MainForm CompLabMainForm { get; set; }

        public String Feedback { get; set; }
        private const String idleText = "You have been inactive for some time. Please resume work to prevent your session from ending.";
        private const String elsewhereText = "Your session has been ended because your account has been logged in elsewhere.";
        private const String logoutText = "Your session has been ended because you have been logged out elsewhere.";

        public LoginForm(MainForm Parent)
        {
            InitializeComponent();
            CompLabMainForm = Parent;
            btnLogin.Enabled = ClientLogic.CurrentSessionType.CanLogin;
            AllowClose = false;
            txtFeedback.Visible = false;
            Feedback = "None";

            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //ConfigurationSection connStrings = config.GetSection("connectionStrings");
                ConfigurationSection connStrings = config.ConnectionStrings;
                //if (!connStrings.IsReadOnly() && !connStrings.SectionInformation.IsProtected && !connStrings.SectionInformation.IsLocked)
                var temp = (connStrings != null);
                temp = (!connStrings.SectionInformation.IsProtected);
                temp = (!connStrings.SectionInformation.IsLocked);
                if (connStrings != null && !connStrings.SectionInformation.IsLocked)// && !connStrings.SectionInformation.IsProtected )
                {
                    string provider = "RsaProtectedConfigurationProvider";
                    connStrings.SectionInformation.ProtectSection(provider);

                    connStrings.SectionInformation.ForceSave = true;
                    config.Save(ConfigurationSaveMode.Full);
                }
                else
                {
                    MessageBox.Show("The configuration file is not accessible.  Please check that your folder level authorization is valid.", "UKSTATS: Error opening config file", MessageBoxButtons.OK);
                }
            }
            catch (Exception ex)
            {
                //We're here in this exception
                String errorMessage = "Failure to save config file.\r\n" + ex.Message + "\r\n *********** \r\n" + ex.ToString() + "\r\n *********** \r\n" + ex.InnerException.ToString();
                MessageBox.Show(errorMessage, "UKSTATS: Error saving config file", MessageBoxButtons.OK);
            }
        }

        public LoginForm(MainForm Parent, String feedback)
        {
            InitializeComponent();
            CompLabMainForm = Parent;
            btnLogin.Enabled = ClientLogic.CurrentSessionType.CanLogin;
            AllowClose = false;
            Feedback = feedback;
            switch (Feedback)
            {
                case "Idle":
                    txtFeedback.Text = idleText;
                    txtFeedback.Visible = true;
                    Feedback = "Idle";
                    break;
                case "Elsewhere":
                    txtFeedback.Text = elsewhereText;
                    txtFeedback.Visible = true;
                    Feedback = "Elsewhere";
                    break;
                case "Logout":
                    txtFeedback.Text = logoutText;
                    txtFeedback.Visible = true;
                    Feedback = "Logout";
                    break;
                default:
                    txtFeedback.Visible = false;
                    Feedback = "None";
                    break;
            }
        }


        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Left = 0;
            this.Top = Screen.PrimaryScreen.WorkingArea.Top;
            pbLock.ImageLocation = "./_images/lock.png";
            pbLock.SizeMode = PictureBoxSizeMode.Zoom;
            pbLock.Visible = false;

            String ApplicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersion.Text = "Version: " + ApplicationVersion;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String ResourceLogin = txtUserID.Text;
            if (ResourceLogin.Length > 9)
            {
                // The username was invalid.
                MessageBox.Show("The entered username was invalid.  Please enter less than 9 characters.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserID.Focus(); // Set focus to the textbox again.
                return;
            }
            Boolean isValid = BLL.Resource.Exists(ResourceLogin);

            if (isValid)
            {
                BLL.Resource CurrentResource = BLL.Resource.Get(ResourceLogin);

                Boolean isTutor = (CurrentResource.ResourceTypeID == eResourceTypeTutor.ResourceTypeID);
                var result = System.Windows.Forms.DialogResult.Yes;
                if (CurrentResource.IsLoggedInElsewhere.Value == true && !isTutor)
                {
                    result = MessageBox.Show("You are currently logged into a " + CurrentResource.IsLoggedInElsewhere.Key + " session.  Continuing will log you out of this session.", "Confirm Login", MessageBoxButtons.OKCancel);
                }

                if (result != System.Windows.Forms.DialogResult.Cancel)
                {
                    if (CompLabMainForm.CurrentAttendance == null)
                    {
                        txtUserID.Text = "";
                        AllowClose = true;
                        this.Hide();
                        this.Close();

                        CompLabMainForm.frmMessages.ResourceObj = CurrentResource;
                        CompLabMainForm.frmTimeTracker.ResourceObj = CurrentResource;

                        CompLabMainForm.CurrentAttendance = CompLabMainForm.frmMessages.ResourceObj.Login(ClientLogic.CurrentSessionType, false, ClientLogic.ComputerName, ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                        CompLabMainForm.CurrentResourceState = ClientLogic.ClientState.Active;
                        CompLabMainForm.timerCheck.Start();

                        DialogResult drMessages = CompLabMainForm.frmMessages.ShowDialog();
                        DialogResult drTimeTracker = CompLabMainForm.frmTimeTracker.ShowDialog();
                    }
                    else
                    {
                        if (CurrentResource.ResourceLogin == CompLabMainForm.CurrentAttendance.Schedule.Resource.ResourceLogin)
                        {
                            CompLabMainForm.CurrentAttendance = CompLabMainForm.frmMessages.ResourceObj.Login(ClientLogic.CurrentSessionType, false, ClientLogic.ComputerName, ClientLogic.ResourceTypeTutor, ClientLogic.ResourceTypeStudent);
                            CompLabMainForm.CurrentResourceState = ClientLogic.ClientState.Active;
                            CompLabMainForm.timerCheck.Start();
                            txtUserID.Text = "";
                            AllowClose = true;
                            this.Hide();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("There is another user Logged into this machine.  In order to login, you must restart this machine.", "Existing Session", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtUserID.Focus(); // Set focus to the textbox again.
                        }
                    }
                }
            }
            else
            {
                // The username was invalid.
                MessageBox.Show("The entered username was invalid.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserID.Focus(); // Set focus to the textbox again.
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            String msg = "This will reboot the machine.  If there is a user logged in, any open applications will be closed and any unsaved work will be lost." + Environment.NewLine + Environment.NewLine + "Continue?";
            DialogResult dr = MessageBox.Show(msg, "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes)
            {
                ClientLogic.RestartComputer();
                Application.Exit();
            }
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!AllowClose)
            {
                ClientLogic.RestartComputer();
            }
            else
            {
                AllowClose = false; // Reset the flag.
            }
        }

        private void LoginForm_Shown(object sender, EventArgs e)
        {
            if (CompLabMainForm.CurrentAttendance != null)
            {
                pbLock.Visible = true;
                btnCancel.Text = "Restart";
                btnLogin.Text = "Resume";
            }
            if (Feedback == "None" || string.IsNullOrEmpty(Feedback))
            {
                txtFeedback.Visible = false;
                Feedback = "None";
            }
            else
            {
                switch (Feedback)
                {
                    case "Idle":
                        txtFeedback.Text = idleText;
                        txtFeedback.Visible = true;
                        Feedback = "Idle";
                        break;
                    case "Elsewhere":
                        txtFeedback.Text = elsewhereText;
                        txtFeedback.Visible = true;
                        Feedback = "Elsewhere";
                        break;
                    case "Logout":
                        txtFeedback.Text = logoutText;
                        txtFeedback.Visible = true;
                        Feedback = "Logout";
                        break;
                    default:
                        txtFeedback.Visible = false;
                        Feedback = "None";
                        break;
                }
            }
        }
    }
}