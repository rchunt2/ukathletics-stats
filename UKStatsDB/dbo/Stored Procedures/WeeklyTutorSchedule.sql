﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/15/2011
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[WeeklyTutorSchedule] 
	-- Add the parameters for the stored procedure here
	--@DateInput DATETIME = NULL,
	@DateRangeStart DATETIME = NULL,
	@DateRangeEnd DATETIME = NULL,
	@TutorID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	--SET @DateInput = '9/15/2011'
	--SET @DateRangeStart = dateadd(d,1-datepart(dw,@DateInput),@DateInput)
	--SET @DateRangeEnd = DATEADD(DAY, 7, @DateRangeStart)
	--SET @TutorID = 'EA94FEC6-F106-4C1D-AB9E-EF2F4E6D3BFA'

	SELECT
		Session.SessionID
		,DATENAME(dw, Session.SessionDateTimeStart) AS ScheduledDay
		,CONVERT(VARCHAR, Session.SessionDateTimeStart, 108) AS ScheduledTime
		,COALESCE(Subject.SubjectNameShort + Course.CourseNumber, 'N/A') AS Subject
		,SessionType.SessionTypeName AS SessionType
		,(Resource.ResourceNameLast + ', ' + Resource.ResourceNameFirst) AS TutorName 
		,tblStudents.StudentPhone AS StudentPhone		
		,tblStudents.StudentName
	FROM Session
		LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID
		LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
		LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
		LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
		LEFT JOIN Schedule ON schedule.SessionID = Session.SessionID
		LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
		LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
		INNER JOIN (
			SELECT	
				Session.SessionID,
				Resource.ResourceNameLast + ', ' + Resource.ResourceNameFirst AS StudentName,
				Resource.ResourcePhoneNumber AS StudentPhone
			FROM Session
				LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID
				LEFT JOIN Schedule ON schedule.SessionID = Session.SessionID
				LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
				LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
				LEFT JOIN CourseSection ON Session.CourseSectionID = CourseSection.CourseSectionID
				LEFT JOIN Course ON Course.CourseID = CourseSection.CourseID
				LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
			WHERE
				Resource.ResourceID IS NOT NULL
				AND Resource.ResourceIsActive = 1
				AND	ResourceType.ResourceTypeName = 'Student'
				AND Session.SessionIsScheduled = 1
				AND Session.SessionIsCancelled = 0
				AND
				(
					@DateRangeStart IS NULL
					OR
					@DateRangeStart <= Session.SessionDateTimeStart
				)
				AND
				(
					@DateRangeEnd IS NULL
					OR
					@DateRangeEnd >= Session.SessionDateTimeStart
				)
		) tblStudents ON Session.SessionID = tblStudents.SessionID
	WHERE 
		Resource.ResourceID IS NOT NULL
		AND
		Resource.ResourceIsActive = 1
		AND
		ResourceType.ResourceTypeName = 'Tutor'
		AND 
		Session.SessionIsScheduled = 1
		AND
		Session.SessionIsCancelled = 0
		AND
		(
			@DateRangeStart IS NULL
			OR
			@DateRangeStart <= Session.SessionDateTimeStart
		)
		AND
		(
			@DateRangeEnd IS NULL
			OR
			@DateRangeEnd >= Session.SessionDateTimeStart
		)
		AND 
		(
			@TutorID IS NULL
		OR
			Resource.ResourceID = @TutorID
		)
		
	ORDER BY Resource.ResourceID, Session.SessionDateTimeStart


END
