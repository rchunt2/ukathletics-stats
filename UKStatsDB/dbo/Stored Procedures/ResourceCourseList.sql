﻿-- =============================================
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================

CREATE PROCEDURE [dbo].[ResourceCourseList]
(
	@xml XML = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT [Resource].ResourceID,
		[Resource].ResourceNameFirst + ' ' + [Resource].ResourceNameLast AS ResourceName,
		[Subject].SubjectNameShort,
		[Course].CourseNumber,
		[CourseSection].CourseSectionNumber,
		[Course].CourseName,
		[CourseSection].CourseSectionDaysMeet,
		CONVERT(VARCHAR(15), [CourseSection].CourseSectionTimeStart, 100) + ' - ' + CONVERT(VARCHAR(15), [CourseSection].CourseSectionTimeEnd, 100) AS CourseSectionTime,
		[CourseSection].CourseSectionBuilding + ' ' + [CourseSection].CourseSectionRoom AS CourseSectionBuildingRoom,
		[Instructor].InstructorName
	FROM [Resource]
		LEFT JOIN [ResourceRegistration] ON [Resource].ResourceID = [ResourceRegistration].ResourceID
		LEFT JOIN [CourseSection] ON [ResourceRegistration].CourseSectionID = [CourseSection].CourseSectionID
		LEFT JOIN [Course] ON [CourseSection].CourseID = Course.CourseID
		LEFT JOIN [Subject] ON [Course].SubjectID = [Subject].SubjectID 
		LEFT JOIN [Instructor] ON [CourseSection].InstructorID = [Instructor].InstructorID
	WHERE Resource.ResourceIsActive = 1 AND (Resource.ResourceID IN (SELECT * FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1) WITH (guid uniqueidentifier '.')))
	ORDER BY  [Resource].ResourceNameLast,
		[Resource].ResourceNameFirst,
		CASE SUBSTRING([CourseSection].CourseSectionDaysMeet, 1, 1)
			WHEN 'M' THEN 10
			WHEN 'T' THEN 20
			WHEN 'W' THEN 30
			WHEN 'R' THEN 40
			WHEN 'F' THEN 50
			WHEN 'S' THEN 60
		END,
		CASE SUBSTRING([CourseSection].CourseSectionDaysMeet, 2, 1)
			WHEN 'M' THEN 10
			WHEN 'T' THEN 20
			WHEN 'W' THEN 30
			WHEN 'R' THEN 40
			WHEN 'F' THEN 50
			WHEN 'S' THEN 60
		END,
		CASE SUBSTRING([CourseSection].CourseSectionDaysMeet, 3, 1)
			WHEN 'M' THEN 10
			WHEN 'T' THEN 20
			WHEN 'W' THEN 30
			WHEN 'R' THEN 40
			WHEN 'F' THEN 50
			WHEN 'S' THEN 60
		END,
		CASE SUBSTRING([CourseSection].CourseSectionDaysMeet, 4, 1)
			WHEN 'M' THEN 10
			WHEN 'T' THEN 20
			WHEN 'W' THEN 30
			WHEN 'R' THEN 40
			WHEN 'F' THEN 50
			WHEN 'S' THEN 60
		END,
		CASE SUBSTRING([CourseSection].CourseSectionDaysMeet, 5, 1)
			WHEN 'M' THEN 10
			WHEN 'T' THEN 20
			WHEN 'W' THEN 30
			WHEN 'R' THEN 40
			WHEN 'F' THEN 50
			WHEN 'S' THEN 60
		END, [CourseSection].CourseSectionDaysMeet,
		[CourseSection].CourseSectionTimeStart

	EXEC sp_xml_removedocument @idoc
END
