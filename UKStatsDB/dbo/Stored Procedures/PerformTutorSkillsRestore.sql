﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/13/2012
-- Description:	Restores the flattened Tutor Skills data to its relational format.
-- Modified by:	Ryan Hunter
-- Modify date:	6/2/2015
--			-Only insert records into CourseTutor if it doesn't already exist.
--		6/7 -Added ResourceIsActive check
-- =============================================
CREATE PROCEDURE [dbo].[PerformTutorSkillsRestore]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO CourseTutor (CourseTutorID, CourseID, ResourceID)
		SELECT 
			NEWID(), Course.CourseID, Resource.ResourceID
		FROM TutorSkillsBackup
		LEFT JOIN Resource ON 
			TutorSkillsBackup.ResourceLogin = Resource.ResourceLogin 
			AND 
			TutorSkillsBackup.ResourceNameFirst = Resource.ResourceNameFirst 
			AND 
			TutorSkillsBackup.ResourceNameLast = Resource.ResourceNameLast
		LEFT JOIN Subject ON 
			TutorSkillsBackup.SubjectName = Subject.SubjectName
			AND
			TutorSkillsBackup.SubjectNameShort = Subject.SubjectNameShort
		LEFT JOIN Course ON
			TutorSkillsBackup.CourseName = Course.CourseName
			AND
			TutorSkillsBackup.CourseNumber = Course.CourseNumber
			AND
			Course.SubjectID = Subject.SubjectID
		WHERE
			Course.CourseID IS NOT NULL
			AND
			Resource.ResourceID IS NOT NULL AND
			Resource.ResourceIsActive = 1 
			--Only insert the skill if it doesn't already exist
			AND NOT EXISTS 
			(	SELECT * 
				FROM CourseTutor
				WHERE CourseID = Course.CourseID AND
					  ResourceID = Resource.ResourceID)
END
