﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 09/22/2011
-- Modified date:	6/1/2015
--			-Only include Study Hall time if it is greater than or equal to 10 minutes, else return 0
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
--		6/1/2016
--		Added multi-location support with SessionTypeID
-- =============================================
CREATE PROCEDURE [dbo].[UsageReport]
	-- Add the parameters for the stored procedure here
	@DateStart DATETIME = NULL
	,@DateEnd DATETIME = NULL
	,@SessionTypeIDs nvarchar(max) = NULL
	,@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc INT

	EXEC sp_xml_preparedocument @idoc OUTPUT
		,@xml

	-- If this is null, I don't think we should be adding days to it.
	--SET @DateEnd = DATEADD(DAY, 1, @DateEnd);
	-- Insert statements for procedure here
	SELECT SessionType.SessionTypeName
		--,SUM(DATEDIFF(n, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut)) AS TimeAccrued
		--DM 9/20/2012 Changed DateDiff to show in Seconds instead of minutes, then divide by 60 for minutes and do a CEILING to match the math used elsewhere in application
		--DM 9/20/2012 Also changed so that Computer Lab time only counts if override given
		--DM 10/11/2012 Changed /60 to /60.0 to use floating math instead of integer
		,SUM(COALESCE(Attendance.AttendanceTimeOverride, CASE
															WHEN SessionType.SessionTypeName = 'Computer Lab'
																THEN Attendance.AttendanceTimeOverride
																--Include Study Hall sessions for display but override to 0 if less than 10 minutes
															WHEN SessionType.SessionTypeName LIKE 'Study Hall%' AND CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0) < 10
																THEN 0
															ELSE CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0)
															END
				, 0)) AS TimeAccrued
		,COUNT(Session.SessionID) AS SessionCount
	FROM Attendance
		LEFT JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID
		LEFT JOIN Session ON Schedule.SessionID = Session.SessionID
		LEFT JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
	WHERE (
			@DateStart IS NULL
			OR Session.SessionDateTimeStart >= @DateStart
			)
		AND (
			@DateEnd IS NULL
			OR Session.SessionDateTimeStart <= DATEADD(DAY, 1, @DateEnd)
			)
		AND (
			@SessionTypeIDs IS NULL
			OR (
				SessionType.SessionTypeID IN (SELECT CAST(val AS uniqueidentifier) AS sessionTypeID from [dbo].f_split(@SessionTypeIDs, ','))
				)
			)
		AND (
			(@xml IS NULL)
			OR (
				Resource.ResourceID IN (
					SELECT * FROM OPENXML(@idoc, '/ArrayOfGuid/guid', 1) WITH (guid UNIQUEIDENTIFIER '.')
					)
				)
			)
		AND (
			-- Disregard activity of less than 2 minutes?
			--DM 9/20/2012 Changed DateDiff to show in Seconds instead of minutes, then divide by 60 for minutes and do a CEILING to match the math used elsewhere in application
			COALESCE(Attendance.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0), 0) >= 2
			OR  
			SessionType.SessionTypeName LIKE 'Study Hall%' --DM - 3/11/2013 - Include ALL time for study hall
			)
		AND Resource.ResourceIsActive = 1
	GROUP BY Session.SessionTypeID, SessionType.SessionTypeName
END


