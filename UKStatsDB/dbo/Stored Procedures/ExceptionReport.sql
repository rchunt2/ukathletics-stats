﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/29/2011
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[ExceptionReport] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart DATETIME = NULL, 
	@DateRangeEnd DATETIME = NULL,
	@TutorIDs XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @TutorIDs

	SELECT 
		SessionOut.SessionIsScheduled
		,SessionOut.SessionID
		,CONVERT(VARCHAR(10),SessionOut.SessionDateTimeStart,101) AS SessionDate
		,CONVERT(VARCHAR(10),SessionOut.SessionDateTimeStart,108) AS SessionTime
		,COALESCE(SubjectOut.SubjectNameShort, 'N/A') AS SubjectName
		,COALESCE(CourseOut.CourseNumber, 'N/A') AS CourseNumber
		,ResourceOut.ResourceNameLast
		,ResourceOut.ResourceNameFirst
		,(CASE WHEN  AttendanceOut.AttendanceID IS NULL THEN 'No Show' ELSE 'Attended' END) AS Attendance
		,COALESCE(CONVERT(VARCHAR(10),AttendanceOut.AttendanceTimeIn,108), '') AS 'TimeIn'
		,COALESCE(CONVERT(VARCHAR(10),AttendanceOut.AttendanceTimeOut,108), '') AS 'TimeOut'
		,COALESCE([ResourceGroupList].GroupList, '') AS [GroupList]
	FROM Session AS SessionOut 
		LEFT JOIN SessionTutor AS SessionTutorOut ON SessionOut.SessionID = SessionTutorOut.SessionID
		LEFT JOIN Course AS CourseOut ON CourseOut.CourseID = SessionTutorOut.CourseID
		LEFT JOIN Subject AS SubjectOut ON SubjectOut.SubjectID = CourseOut.SubjectID
		LEFT JOIN Schedule AS ScheduleOut ON SessionOut.SessionID = ScheduleOut.SessionID
		LEFT JOIN Attendance AS AttendanceOut ON ScheduleOut.ScheduleID = AttendanceOut.ScheduleID
		LEFT JOIN Resource AS ResourceOut ON ScheduleOut.ResourceID = ResourceOut.ResourceID
		LEFT JOIN ResourceType AS ResourceTypeOut ON ResourceOut.ResourceTypeID = ResourceTypeOut.ResourceTypeID
		LEFT JOIN (SELECT DISTINCT [Resource].ResourceID, STUFF((
							SELECT ', ' + [Group].GroupName
							FROM [GroupMember] 
								JOIN [Group] ON [Group].GroupID = [GroupMember].GroupID
							WHERE [Resource].ResourceID = [GroupMember].ResourceID
							ORDER BY [Group].GroupName
							FOR XML PATH('')), 1, 2, '') AS GroupList
					FROM [Resource]
					GROUP BY [Resource].ResourceID
					) ResourceGroupList ON [ResourceOut].ResourceID = [ResourceGroupList].ResourceID
	WHERE 
	ResourceOut.ResourceIsActive = 1 AND
	SessionOut.SessionID IN 
	(
		SELECT Session.SessionID
		FROM Session
			LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
			LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
			LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
			LEFT JOIN Schedule ON Session.SessionID = Schedule.SessionID
			LEFT JOIN Attendance ON Schedule.ScheduleID = Attendance.ScheduleID
			LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
			LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
		WHERE 
			Session.SessionTypeID IN (SELECT SessionTypeID FROM SessionType WHERE SessionTypeName = 'Tutoring')
			AND Resource.ResourceID IS NOT NULL
			AND Session.SessionIsCancelled = 0
			AND Session.SessionIsScheduled = 1
			AND (@DateRangeStart IS NULL OR @DateRangeStart <= Session.SessionDateTimeStart)
			AND (@DateRangeEnd IS NULL OR @DateRangeEnd >= Session.SessionDateTimeStart)
			AND (@TutorIDs IS NULL OR ( Session.SessionID IN (
																SELECT Session_TutorID.SessionID
																FROM Session AS Session_TutorID 
																	LEFT JOIN Schedule AS Schedule_TutorID ON Session_TutorID.SessionID = Schedule_TutorID.SessionID
																	LEFT JOIN Resource AS Resource_TutorID ON Schedule_TutorID.ResourceID = Resource_TutorID.ResourceID
																	LEFT JOIN ResourceType ON Resource_TutorID.ResourceTypeID = ResourceType.ResourceTypeID
																WHERE Resource_TutorID.ResourceID IN  (SELECT * FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1) WITH (guid uniqueidentifier '.'))
																	AND ResourceType.ResourceTypeName IN ('Tutor', 'Student')
															)
									)
				)
			AND (Attendance.AttendanceID IS NULL)
	)
	ORDER BY SessionOut.SessionIsScheduled DESC, SessionOut.SessionDateTimeStart ASC, SessionOut.SessionID ASC, 
			ResourceTypeOut.ResourceTypeName DESC, ResourceOut.ResourceNameLast ASC, ResourceOut.ResourceNameFirst ASC

	EXEC sp_xml_removedocument @idoc
END
