﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/3/2012
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[ResourceReport]
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

    SELECT 
		Resource.ResourceID
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,Resource.ResourceLogin
		,Resource.ResourceEmail
		,Resource.ResourcePhoneNumber
		,COALESCE([Group].GroupName, 'N/A') AS GroupName
		,ResourceType.ResourceTypeName
	FROM Resource
	LEFT JOIN GroupMember ON Resource.ResourceID = GroupMember.ResourceID
	LEFT JOIN [Group] ON GroupMember.GroupID = [Group].GroupID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID

	WHERE
		Resource.ResourceIsActive = 1 AND
		@xml IS NULL
		OR
		(
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
		)
	ORDER BY 
		Resource.ResourceNameLast ASC
		,Resource.ResourceNameFirst ASC

END
