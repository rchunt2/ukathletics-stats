﻿-- =============================================
-- Author:		David Morrison
-- Rewrote huge for loops and many linq queries that took a lot of time into a stored procedure
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 3/31/2016
--		Add Tutoring Type at new football location
-- =============================================
CREATE PROCEDURE [dbo].[GetPresenceInfo]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Now DATETIME
	DECLARE @LoginEarlyMinutes AS INT
	DECLARE @LoginLateMinutes AS INT
	SET @Now = GETDATE()
	SELECT @LoginEarlyMinutes = CAST(SettingValue AS INT) FROM Setting WHERE SettingKey='LoginEarlyMinutes'
	SELECT @LoginLateMinutes = CAST(SettingValue AS INT) FROM Setting WHERE SettingKey='LoginLateMinutes'
	
	--The first part of the union will return results only where student AND tutor exists
	--The second part of the union will return results only when (sudent OR tutor) does NOT exist

	SELECT [Session].SessionID
		,[Session].SessionIsScheduled
		,[Session].SessionTypeID
		,CAST(1 AS BIT) AS [StudentAndTutorPresent]
	FROM [Session]
	WHERE SessionTypeID IN (SELECT SessionTypeID FROM [SessionType] WHERE SessionTypeName IN ('Tutoring', 'Tutoring - Football'))
		AND ((SessionDateTimeStart >= DATEADD(MINUTE, -@LoginEarlyMinutes, @Now) AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginLateMinutes, @Now))
			OR (SessionDateTimeStart >= @Now AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginEarlyMinutes, @Now))
			OR (SessionDateTimeStart <= @Now AND SessionDateTimeend > @Now))
		AND SessionIsCancelled = 0
		AND EXISTS (SELECT * 
					FROM [Attendance]
						JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
						JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
						JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
					WHERE [Resource].ResourceIsActive = 1
						AND AttendanceTimeOut IS NULL
						AND [Schedule].SessionID = [Session].SessionID
						AND [ResourceType].ResourceTypeName='Student')
		AND EXISTS (SELECT * 
					FROM [Attendance]
						JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
						JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
						JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
					WHERE [Resource].ResourceIsActive = 1
						AND AttendanceTimeOut IS NULL
						AND [Schedule].SessionID = [Session].SessionID
						AND [ResourceType].ResourceTypeName='Tutor')
UNION
	SELECT [Session].SessionID
		,[Session].SessionIsScheduled
		,[Session].SessionTypeID
		,CAST(0 AS BIT) AS [StudentAndTutorPresent]
	FROM [Session]
	WHERE SessionTypeID IN (SELECT SessionTypeID FROM [SessionType] WHERE SessionTypeName IN ('Tutoring', 'Tutoring - Football'))
		AND ((SessionDateTimeStart >= DATEADD(MINUTE, -@LoginEarlyMinutes, @Now) AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginLateMinutes, @Now))
			OR (SessionDateTimeStart >= @Now AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginEarlyMinutes, @Now))
			OR (SessionDateTimeStart <= @Now AND SessionDateTimeend > @Now))
		AND SessionIsCancelled = 0
		--The 2 below NOT EXISTS are OR'd together, for "If either student or Tutor does not exist"
		AND ((NOT EXISTS (SELECT * 
						FROM [Attendance]
							JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
							JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
							JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
						WHERE [Resource].ResourceIsActive = 1
							AND AttendanceTimeOut IS NULL
							AND [Schedule].SessionID = [Session].SessionID
							AND [ResourceType].ResourceTypeName='Student'))
			OR (NOT EXISTS (SELECT * 
						FROM [Attendance]
							JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
							JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
							JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
						WHERE 
							[Resource].ResourceIsActive = 1
							AND AttendanceTimeOut IS NULL
							AND [Schedule].SessionID = [Session].SessionID
							AND [ResourceType].ResourceTypeName='Tutor'))
			)

END
