﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/15/2011
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 6/3/2016
--		Added multi-location support
-- ============================================
CREATE PROCEDURE [dbo].[TutoringActivity] @DateRangeStart DATETIME = NULL
	,@DateRangeEnd DATETIME = NULL
	,@TutorID UNIQUEIDENTIFIER = NULL
	,@SessionTypeID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	SELECT Session.SessionIsScheduled
		,SessionType.SessionTypeName
		,Session.SessionID
		,CONVERT(VARCHAR(10), Session.SessionDateTimeStart, 101) AS SessionDate
		,CONVERT(VARCHAR(10), Session.SessionDateTimeStart, 108) AS SessionTime
		,COALESCE(Subject.SubjectNameShort, 'N/A') AS SubjectName
		,COALESCE(Course.CourseNumber, 'N/A') AS CourseNumber
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,(
			CASE 
				WHEN Attendance.AttendanceID IS NULL
					THEN 'No Show'
				ELSE 'Attended'
				END
			) AS Attendance
		,COALESCE(CONVERT(VARCHAR(10), Attendance.AttendanceTimeIn, 108), '') AS 'TimeIn'
		,COALESCE(CONVERT(VARCHAR(10), Attendance.AttendanceTimeOut, 108), '') AS 'TimeOut'
		, CASE ResourceType.ResourceTypeName
			WHEN 'Tutor' THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT)
		  END AS IsTutor
	FROM Session
		JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID
		LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
		LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
		LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
		LEFT JOIN Schedule ON Session.SessionID = Schedule.SessionID
		LEFT JOIN Attendance ON Schedule.ScheduleID = Attendance.ScheduleID
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
		LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
	WHERE Resource.ResourceID IS NOT NULL
		AND Resource.ResourceIsActive = 1
		AND Session.SessionIsCancelled = 0
		AND SessionType.SessionTypeName != 'Computer Lab'
		AND (
			@DateRangeStart IS NULL
			OR @DateRangeStart <= Session.SessionDateTimeStart
			)
		AND (
			@DateRangeEnd IS NULL
			OR @DateRangeEnd >= Session.SessionDateTimeStart
			)
		AND (
			@TutorID IS NULL
			OR (
				Session.SessionID IN (
					SELECT Session_TutorID.SessionID
					FROM Session AS Session_TutorID
					LEFT JOIN Schedule AS Schedule_TutorID ON Session_TutorID.SessionID = Schedule_TutorID.SessionID
					LEFT JOIN Resource AS Resource_TutorID ON Schedule_TutorID.ResourceID = Resource_TutorID.ResourceID
					WHERE Resource_TutorID.ResourceID = @TutorID AND Resource_TutorID.ResourceIsActive = 1
					)
				)
			)
		AND (
			@SessionTypeID IS NULL
			OR SessionType.SessionTypeID  = @SessionTypeID
			)
	ORDER BY ResourceType.ResourceTypeName DESC
END


