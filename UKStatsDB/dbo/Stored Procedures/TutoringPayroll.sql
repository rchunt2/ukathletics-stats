﻿-- =============================================
-- Author:        Joshua Shearer
-- Create date: 9/6/2011
-- Description:   Gets a dataset used for Reporting.  In particular, this retrieves the time that tutors have logged in the system.
-- Update: 09/28/2012 – Wcomeaux – Correcting some fields by eliminating unwanted records for ‘comp lab’ and also correcting the “average” field to be the value provided
--                                  in a bug request made by LWells
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- ============================================
CREATE PROCEDURE [dbo].[TutoringPayroll] 
      -- Add the parameters for the stored procedure here
      @DateRangeStart datetime = null, 
      @DateRangeEnd datetime = null, 
      @ResourceID uniqueidentifier = null
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);
      
      SELECT
            Resource.ResourceNameLast,
            Resource.ResourceNameFirst, 
            COALESCE((
                  SELECT 
                        COUNT(DISTINCT(Session_1.SessionID))
                  FROM Attendance AS Attendance_1
                  LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
                  LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
                  WHERE 
                        Session_1.SessionIsScheduled = 1
                        AND
                        Schedule_1.ResourceID = Resource.ResourceID
                        AND
                        Session_1.SessionTypeID <> (SELECT SessionTypeID FROM SessionType where SessionTypeName = 'Computer Lab')
                        AND
                        (@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
                        AND
                        (@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
            ), 0) AS Scheduled,
            COALESCE((
                  SELECT 
                        COUNT(DISTINCT(Session_1.SessionID))
                  FROM Attendance AS Attendance_1
                  LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
                  LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
                  WHERE 
                        Session_1.SessionIsScheduled = 0
                        AND
                        Schedule_1.ResourceID = Resource.ResourceID
                        AND 
                        Session_1.SessionTypeID <> (SELECT SessionTypeID FROM SessionType where SessionTypeName = 'Computer Lab')
                        AND 
                        (@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
                        AND
                        (@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
            ), 0) AS Unscheduled,
            COALESCE((
                  SELECT 
                        COUNT(DISTINCT(Session_1.SessionID))
                  FROM Attendance AS Attendance_1
                  LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
                  LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
                  WHERE 
                        Schedule_1.ResourceID = Resource.ResourceID
                        AND 
                        Session_1.SessionTypeID <> (SELECT SessionTypeID FROM SessionType where SessionTypeName = 'Computer Lab')
                        AND
                        (@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
                        AND
                        (@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
            ), 0) AS Attended,
            SUM(COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0)) /
            COALESCE((
                  SELECT 
                        COUNT(DISTINCT(Session_1.SessionID))
                  FROM Attendance AS Attendance_1
                  LEFT JOIN Schedule AS Schedule_1 ON Attendance_1.ScheduleID = Schedule_1.ScheduleID 
                  LEFT JOIN Session AS Session_1 ON Schedule_1.SessionID = Session_1.SessionID
                  WHERE 
                        Schedule_1.ResourceID = Resource.ResourceID
                        AND 
                        Session_1.SessionTypeID <> (SELECT SessionTypeID FROM SessionType where SessionTypeName = 'Computer Lab')
                        AND
                        (@DateRangeStart IS NULL OR Session_1.SessionDateTimeStart >= @DateRangeStart)
                        AND
                        (@DateRangeEnd IS NULL OR Session_1.SessionDateTimeStart <= @DateRangeEnd)
            ), 0) AS AverageMinutes,
            SUM(COALESCE(Attendance.AttendanceTimeOverride, DATEDIFF(MINUTE, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut), 0)) AS TotalMinutes
          
      FROM Attendance 
      LEFT OUTER JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID 
      LEFT OUTER JOIN Resource ON Schedule.ResourceID = Resource.ResourceID 
      LEFT OUTER JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
      LEFT OUTER JOIN Session ON Session.SessionID = Schedule.SessionID 
      LEFT OUTER JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID 

      WHERE 
            ResourceType.ResourceTypeName = 'Tutor'
            AND
            SessionType.SessionTypeName = 'Tutoring'
            AND
            (
                  (@DateRangeStart IS NULL OR Session.SessionDateTimeStart >= @DateRangeStart)
                  AND
                  (@DateRangeEnd IS NULL OR Session.SessionDateTimeStart <= @DateRangeEnd)
            )
            AND
            (@ResourceID IS NULL OR Resource.ResourceID = @ResourceID)
			AND 
				Resource.ResourceIsActive = 1

      GROUP BY 
            Resource.ResourceID,
            Resource.ResourceNameFirst, 
            Resource.ResourceNameLast

      ORDER BY Resource.ResourceNameLast ASC
END


