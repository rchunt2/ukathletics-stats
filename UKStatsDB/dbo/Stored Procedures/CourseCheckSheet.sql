﻿-- =============================================
-- Author:		Joshua Shearer
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[CourseCheckSheet]
	@dt Date = NULL,
	--@xml nvarchar(MAX) = NULL
	@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	DECLARE @day varchar(MAX)
	SET @day = datepart(dw, @dt)

	SET @day = CASE @day
					WHEN 1 THEN 'U'
					WHEN 2 THEN 'M'
					WHEN 3 THEN 'T'
					WHEN 4 THEN 'W'
					WHEN 5 THEN 'R'
					WHEN 6 THEN 'F'
					WHEN 7 THEN 'S'
					ELSE 'FAIL'
				END
	SELECT 
		Resource.ResourceID,
		Course.CourseName,
		Subject.SubjectNameShort,
		Course.CourseNumber,
		CourseSection.CourseSectionNumber,
		Resource.ResourceNameFirst,
		Resource.ResourceNameLast,
		CourseSection.CourseSectionDateStart,
		CourseSection.CourseSectionDateEnd,
		CourseSection.CourseSectionDaysMeet,
		CourseSection.CourseSectionBuilding,
		courseSection.CourseSectionRoom,
		CourseSection.CourseSectionTimeStart,
		CourseSection.CourseSectionTimeEnd
	FROM ResourceRegistration
		LEFT JOIN Resource ON ResourceRegistration.ResourceID = Resource.ResourceID
		LEFT JOIN CourseSection ON ResourceRegistration.CourseSectionID = CourseSection.CourseSectionID
		LEFT JOIN Course ON CourseSection.CourseID = Course.CourseID
		LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID
	WHERE 
		@dt BETWEEN CourseSection.CourseSectionDateStart AND CourseSection.CourseSectionDateEnd
		AND CourseSection.CourseSectionDaysMeet LIKE '%' + @day + '%'
		AND Resource.ResourceIsActive = 1
		AND (Resource.ResourceID IN (SELECT * FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1) WITH (guid uniqueidentifier '.')))
	ORDER BY 
		-- Building, Start Time, Room Number, Last Name, First Name.
		CourseSection.CourseSectionTimeStart, 
		CourseSection.CourseSectionBuilding, 
		CourseSection.CourseSectionRoom, 
		Resource.ResourceNameLast, 
		Resource.ResourceNameFirst

	EXEC sp_xml_removedocument @idoc
END
