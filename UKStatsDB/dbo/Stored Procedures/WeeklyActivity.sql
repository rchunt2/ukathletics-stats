﻿-- =============================================
-- Author:		Joshua Shearer
-- Modified by:		Ryan Hunter
-- Modified date:	6/1/2015
--			-Only include Study Hall time if it is greater than or equal to 10 minutes, else return 0
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 5/17/2016
--		Added multi-location support
-- =============================================
CREATE PROCEDURE [dbo].[WeeklyActivity]
	-- Add the parameters for the stored procedure here
	@DateRangeStart DATETIME = NULL
	,@DateRangeEnd DATETIME = NULL
	,@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @idoc INT

	EXEC sp_xml_preparedocument @idoc OUTPUT
		,@xml

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	--DM 10/11/2012 Changed /60 to /60.0 to use floating math instead of integer
	SELECT Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,COALESCE((
				(
					COALESCE((
							SELECT SUM(COALESCE(Attendance_Tutoring.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_Tutoring.AttendanceTimeIn, Attendance_Tutoring.AttendanceTimeOut) / 60.0), 0)) AS MinutesCounted
							FROM Session AS Session_Tutoring
								LEFT JOIN SessionType AS SessionType_Tutoring ON Session_Tutoring.SessionTypeID = SessionType_Tutoring.SessionTypeID
								LEFT JOIN Schedule AS Schedule_Tutoring ON Schedule_Tutoring.SessionID = Session_Tutoring.SessionID
								LEFT JOIN Resource AS Resource_Tutoring ON Schedule_Tutoring.ResourceID = Resource_Tutoring.ResourceID
								LEFT JOIN Attendance AS Attendance_Tutoring ON Schedule_Tutoring.ScheduleID = Attendance_Tutoring.ScheduleID
							WHERE Resource_Tutoring.ResourceID = Resource.ResourceID
								AND Resource_Tutoring.ResourceIsActive = 1
								AND SessionType_Tutoring.SessionTypeName = 'Tutoring - CATS'
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= Session_Tutoring.SessionDateTimeStart
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= Session_Tutoring.SessionDateTimeStart
									)
								AND COALESCE(Attendance_Tutoring.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_Tutoring.AttendanceTimeIn, Attendance_Tutoring.AttendanceTimeOut) / 60.0), 0) >= 2
							GROUP BY Resource_Tutoring.ResourceID
								,SessionType_Tutoring.SessionTypeName
							), 0)
					) + (
					COALESCE((
							SELECT SUM(FreeTime.FreeTimeAmount) AS MinutesCounted
							FROM FreeTime
								LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID
							WHERE SessionType_FreeTime.SessionTypeName = 'Tutoring - CATS'
								AND FreeTime.ResourceID = Resource.ResourceID
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= FreeTime.FreeTimeApplyDate
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= FreeTime.FreeTimeApplyDate
									)
							), 0)
					)
				), 0) AS MinutesTutoring
		,COALESCE((
				(
					COALESCE((
							SELECT SUM(COALESCE(Attendance_Tutoring.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_Tutoring.AttendanceTimeIn, Attendance_Tutoring.AttendanceTimeOut) / 60.0), 0)) AS MinutesCounted
							FROM Session AS Session_Tutoring
								LEFT JOIN SessionType AS SessionType_Tutoring ON Session_Tutoring.SessionTypeID = SessionType_Tutoring.SessionTypeID
								LEFT JOIN Schedule AS Schedule_Tutoring ON Schedule_Tutoring.SessionID = Session_Tutoring.SessionID
								LEFT JOIN Resource AS Resource_Tutoring ON Schedule_Tutoring.ResourceID = Resource_Tutoring.ResourceID
								LEFT JOIN Attendance AS Attendance_Tutoring ON Schedule_Tutoring.ScheduleID = Attendance_Tutoring.ScheduleID
							WHERE Resource_Tutoring.ResourceID = Resource.ResourceID
								AND Resource_Tutoring.ResourceIsActive = 1
								AND SessionType_Tutoring.SessionTypeName = 'Tutoring - Football'
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= Session_Tutoring.SessionDateTimeStart
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= Session_Tutoring.SessionDateTimeStart
									)
								AND COALESCE(Attendance_Tutoring.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_Tutoring.AttendanceTimeIn, Attendance_Tutoring.AttendanceTimeOut) / 60.0), 0) >= 2
							GROUP BY Resource_Tutoring.ResourceID
								,SessionType_Tutoring.SessionTypeName
							), 0)
					) + (
					COALESCE((
							SELECT SUM(FreeTime.FreeTimeAmount) AS MinutesCounted
							FROM FreeTime
								LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID
							WHERE SessionType_FreeTime.SessionTypeName = 'Tutoring - Football'
								AND FreeTime.ResourceID = Resource.ResourceID
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= FreeTime.FreeTimeApplyDate
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= FreeTime.FreeTimeApplyDate
									)
							), 0)
					)
				), 0) AS MinutesTutoringFootball
		,COALESCE((
				(
					COALESCE((
							SELECT SUM(Attendance_CompLab.AttendanceTimeOverride) AS MinutesCounted
							FROM Session AS Session_CompLab
								LEFT JOIN SessionType AS SessionType_CompLab ON Session_CompLab.SessionTypeID = SessionType_CompLab.SessionTypeID
								LEFT JOIN Schedule AS Schedule_CompLab ON Schedule_CompLab.SessionID = Session_CompLab.SessionID
								LEFT JOIN Resource AS Resource_CompLab ON Schedule_CompLab.ResourceID = Resource_CompLab.ResourceID
								LEFT JOIN Attendance AS Attendance_CompLab ON Schedule_CompLab.ScheduleID = Attendance_CompLab.ScheduleID
							WHERE Resource_CompLab.ResourceID = Resource.ResourceID
								AND Resource_CompLab.ResourceIsActive = 1
								AND SessionType_CompLab.SessionTypeName = 'Computer Lab'
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= Session_CompLab.SessionDateTimeStart
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= Session_CompLab.SessionDateTimeStart
									)
							GROUP BY Resource_CompLab.ResourceID
								,SessionType_CompLab.SessionTypeName
							), 0)
					) + (
					COALESCE((
							SELECT SUM(FreeTime.FreeTimeAmount) AS MinutesCounted
							FROM FreeTime
								LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID
							WHERE SessionType_FreeTime.SessionTypeName = 'Computer Lab'
								AND FreeTime.ResourceID = Resource.ResourceID
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= FreeTime.FreeTimeApplyDate
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= FreeTime.FreeTimeApplyDate
									)
							), 0)
					)
				), 0) AS MinutesCompLab
		,COALESCE((
				(
					COALESCE((
							SELECT SUM(COALESCE(Attendance_StudyHall.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_StudyHall.AttendanceTimeIn, Attendance_StudyHall.AttendanceTimeOut) / 60.0), 0)) AS MinutesCounted
							FROM Session AS Session_StudyHall
								LEFT JOIN SessionType AS SessionType_StudyHall ON Session_StudyHall.SessionTypeID = SessionType_StudyHall.SessionTypeID
								LEFT JOIN Schedule AS Schedule_StudyHall ON Schedule_StudyHall.SessionID = Session_StudyHall.SessionID
								LEFT JOIN Resource AS Resource_StudyHall ON Schedule_StudyHall.ResourceID = Resource_StudyHall.ResourceID
								LEFT JOIN Attendance AS Attendance_StudyHall ON Schedule_StudyHall.ScheduleID = Attendance_StudyHall.ScheduleID
							WHERE Resource_StudyHall.ResourceID = Resource.ResourceID
								AND Resource_StudyHall.ResourceIsActive = 1
								AND SessionType_StudyHall.SessionTypeName = 'Study Hall - CATS'
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= Session_StudyHall.SessionDateTimeStart
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= Session_StudyHall.SessionDateTimeStart
									)
								--DM - 3/11/2013 - Include ALL time for study hall, commented this out. 
								AND COALESCE(Attendance_StudyHall.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_StudyHall.AttendanceTimeIn, Attendance_StudyHall.AttendanceTimeOut) / 60.0), 0) >= 10
							GROUP BY Resource_StudyHall.ResourceID
								,SessionType_StudyHall.SessionTypeName
							), 0)
					) + (
					COALESCE((
							SELECT SUM(FreeTime.FreeTimeAmount) AS MinutesCounted
							FROM FreeTime
								LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID
							WHERE SessionType_FreeTime.SessionTypeName = 'Study Hall - CATS'
								AND FreeTime.ResourceID = Resource.ResourceID
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= FreeTime.FreeTimeApplyDate
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= FreeTime.FreeTimeApplyDate
									)
							), 0)
					)
				), 0) AS MinutesStudyHall
		,COALESCE((
				(
					COALESCE((
							SELECT SUM(COALESCE(Attendance_StudyHall.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_StudyHall.AttendanceTimeIn, Attendance_StudyHall.AttendanceTimeOut) / 60.0), 0)) AS MinutesCounted
							FROM Session AS Session_StudyHall
								LEFT JOIN SessionType AS SessionType_StudyHall ON Session_StudyHall.SessionTypeID = SessionType_StudyHall.SessionTypeID
								LEFT JOIN Schedule AS Schedule_StudyHall ON Schedule_StudyHall.SessionID = Session_StudyHall.SessionID
								LEFT JOIN Resource AS Resource_StudyHall ON Schedule_StudyHall.ResourceID = Resource_StudyHall.ResourceID
								LEFT JOIN Attendance AS Attendance_StudyHall ON Schedule_StudyHall.ScheduleID = Attendance_StudyHall.ScheduleID
							WHERE Resource_StudyHall.ResourceID = Resource.ResourceID
								AND Resource_StudyHall.ResourceIsActive = 1
								AND SessionType_StudyHall.SessionTypeName = 'Study Hall - Football'
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= Session_StudyHall.SessionDateTimeStart
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= Session_StudyHall.SessionDateTimeStart
									)
								--DM - 3/11/2013 - Include ALL time for study hall, commented this out. 
								AND COALESCE(Attendance_StudyHall.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance_StudyHall.AttendanceTimeIn, Attendance_StudyHall.AttendanceTimeOut) / 60.0), 0) >= 10
							GROUP BY Resource_StudyHall.ResourceID
								,SessionType_StudyHall.SessionTypeName
							), 0)
					) + (
					COALESCE((
							SELECT SUM(FreeTime.FreeTimeAmount) AS MinutesCounted
							FROM FreeTime
								LEFT JOIN SessionType AS SessionType_FreeTime ON SessionType_FreeTime.SessionTypeID = FreeTime.SessionTypeID
							WHERE SessionType_FreeTime.SessionTypeName = 'Study Hall - Football'
								AND FreeTime.ResourceID = Resource.ResourceID
								AND (
									@DateRangeStart IS NULL
									OR @DateRangeStart <= FreeTime.FreeTimeApplyDate
									)
								AND (
									@DateRangeEnd IS NULL
									OR @DateRangeEnd >= FreeTime.FreeTimeApplyDate
									)
							), 0)
					)
				), 0) AS MinutesStudyHallFootball
	FROM Resource
	WHERE Resource.ResourceID IN (
			SELECT *
			FROM OPENXML(@idoc, '/ArrayOfGuid/guid', 1) WITH (guid UNIQUEIDENTIFIER '.')
			)
			AND Resource.ResourceIsActive = 1
	ORDER BY ResourceNameLast
		,ResourceNameFirst
END


