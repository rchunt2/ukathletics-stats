﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/12/2011
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 5/24/2016
--		Added support for multilocation
-- =============================================
CREATE PROCEDURE [dbo].[Transactions] 
	-- Add the parameters for the stored procedure here
	@DateRangeStart DateTime = NULL,
	@DateRangeEnd DateTime = NULL,
	@SessionTypeIDs nvarchar(max) = NULL,
	@xml XML = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT 
		ResourceNameLast, 
		ResourceNameFirst, 
		SessionTypeName, 
		AttendanceTimeIn, 
		AttendanceTimeOut, 
		AttendanceMachineName
	 FROM
	(
		SELECT
			Resource.ResourceNameLast
			,Resource.ResourceNameFirst
			,SessionType.SessionTypeName
			,Attendance.AttendanceTimeIn
			,Attendance.AttendanceTimeOut
			,Attendance.AttendanceMachineName
			,Session.SessionDateTimeStart
		FROM Attendance
		LEFT JOIN Schedule ON Attendance.ScheduleID = Schedule.ScheduleID
		LEFT JOIN Resource ON Schedule.ResourceID = Resource.ResourceID
		LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
		LEFT JOIN Session ON Schedule.SessionID = Session.SessionID
		LEFT JOIN SessionType ON Session.SessionTypeID = SessionType.SessionTypeID 
		
		WHERE
			(	
				Session.SessionDateTimeStart >= @DateRangeStart
				OR
				@DateRangeStart IS NULL
			)
			AND
			(
				Session.SessionDateTimeStart <= DATEADD(DAY, 1, @DateRangeEnd)
				OR
				@DateRangeEnd IS NULL
			)
			AND
			(
				Resource.ResourceIsActive = 1 AND
				Resource.ResourceID IN 
				(
					SELECT
						*
					FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
					WITH 
					( 
						guid uniqueidentifier '.'
					)
				)
			)
	) AS t1

	WHERE
	@SessionTypeIDs IS NULL
	OR
	(
		t1.SessionTypeName IN (SELECT SessionType.SessionTypeName FROM SessionType WHERE SessionType.SessionTypeID IN (SELECT CAST(val AS uniqueidentifier) AS sessionTypeID from [dbo].f_split(@SessionTypeIDs, ',')))
	)

	ORDER BY t1.SessionDateTimeStart ASC
END


