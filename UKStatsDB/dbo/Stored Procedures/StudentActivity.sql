﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 9/12/2011
-- Modified by:		Ryan Hunter
-- Modified date:	5/27/2015
--			-Added parameter for IsScheduled so that report can be filtered on Unscheduled Tutor sessions
-- Modified date:	6/1/2015
--			-Only include Study Hall time if it is greater than or equal to 10 minutes, else return 0
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 5/16/2016
--		Added code for multi-location
-- =============================================
CREATE PROCEDURE [dbo].[StudentActivity]
	-- Add the parameters for the stored procedure here
	@DateRangeStart DATETIME = NULL
	,@DateRangeEnd DATETIME = NULL
	,@SessionTypeIDs nvarchar(max) = NULL
	,@IsScheduled BIT = NULL
	,@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc INT
	
	EXEC sp_xml_preparedocument @idoc OUTPUT
		,@xml

	SET @DateRangeEnd = DATEADD(DAY, 1, @DateRangeEnd);

	SELECT *
	FROM (
		SELECT Resource.ResourceNameLast
			,Resource.ResourceNameFirst
			,Session.SessionDateTimeStart
			,Session.SessionDateTimeEnd
			,Session.SessionTypeID
			,Attendance.AttendanceTimeIn
			,Attendance.AttendanceTimeOut
			,CASE 
				WHEN Attendance.AttendanceID IS NULL
					THEN 'No Show'
				ELSE 'Attended'
				END AS Attend
			,(SessionType.SessionTypeName + 
						CASE 
							WHEN Session.SessionIsScheduled = 0 AND SessionType.SessionTypeName = 'Tutoring - CATS'
								THEN ' - Unscheduled'
							WHEN Session.SessionIsScheduled = 1 AND SessionType.SessionTypeName = 'Tutoring - CATS'
								THEN ' - Scheduled'
							WHEN Session.SessionIsScheduled = 0 AND SessionType.SessionTypeName = 'Tutoring - Football'
								THEN ' - Unscheduled'
							WHEN Session.SessionIsScheduled = 1 AND SessionType.SessionTypeName = 'Tutoring - Football'
								THEN ' - Scheduled'
							ELSE ''
						END) AS SessionTypeName
			,COALESCE(Subject.SubjectNameShort, 'N/A') AS SubjectName
			,COALESCE(Course.CourseNumber, 'N/A') AS CourseNumber
			--DM 9/20/2012 Changed DateDiff to show in Seconds instead of minutes, then divide by 60 for minutes and do a CEILING to match the math used elsewhere in application
			--DM 9/20/2012 Also changed so that Computer Lab time only counts if override given
			--DM 10/11/2012 Changed /60 to /60.0 to use floating math instead of integer
			,COALESCE(Attendance.AttendanceTimeOverride, CASE
															WHEN SessionType.SessionTypeName = 'Computer Lab'
																THEN Attendance.AttendanceTimeOverride
															--Include Study Hall sessions for display but override to 0 if less than 10 minutes
															WHEN (SessionType.SessionTypeName = 'Study Hall - CATS' OR SessionType.SessionTypeName = 'Study Hall - Football' ) AND CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0) < 10
																THEN 0
															ELSE CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0)
														 END
				, 0) AS Duration
		FROM Session
			LEFT JOIN SessionType ON SessionType.SessionTypeID = Session.SessionTypeID
			LEFT JOIN SessionTutor ON Session.SessionID = SessionTutor.SessionID
			LEFT JOIN Course ON Course.CourseID = SessionTutor.CourseID
			LEFT JOIN Subject ON Subject.SubjectID = Course.SubjectID
			LEFT JOIN Schedule ON Schedule.SessionID = Session.SessionID
			LEFT JOIN Attendance ON Attendance.ScheduleID = Schedule.ScheduleID
			LEFT JOIN Resource ON Resource.ResourceID = Schedule.ResourceID
			LEFT JOIN ResourceType ON ResourceType.ResourceTypeID = Resource.ResourceTypeID
			LEFT JOIN CourseSection ON CourseSection.CourseSectionID = Session.CourseSectionID
		WHERE SessionType.SessionTypeName NOT IN ('Class','Note')
			AND ResourceType.ResourceTypeName = 'Student'
			AND (
				Session.SessionDateTimeStart >= @DateRangeStart
				OR @DateRangeStart IS NULL
				)
			AND (
				Session.SessionDateTimeStart <= @DateRangeEnd
				OR @DateRangeEnd IS NULL
				)
			AND (
				Resource.ResourceIsActive = 1 AND
				Resource.ResourceID IN (
					SELECT *
					FROM OPENXML(@idoc, '/ArrayOfGuid/guid', 1) WITH (guid UNIQUEIDENTIFIER '.')
					)
				)
			AND (
				-- Disregard activity of less than 2 minutes?
				--DM 9/20/2012 Changed DateDiff to show in Seconds instead of minutes, then divide by 60 for minutes and do a CEILING to match the math used elsewhere in application
				--DM 10/11/2012 Changed /60 to /60.0 to use floating math instead of integer
				COALESCE(Attendance.AttendanceTimeOverride, CEILING(DATEDIFF(SECOND, Attendance.AttendanceTimeIn, Attendance.AttendanceTimeOut) / 60.0), 0) >= 2
				OR  
				(SessionType.SessionTypeName = 'Study Hall - CATS' OR SessionType.SessionTypeName = 'Study Hall - Football') --DM - 3/11/2013 - Include ALL time for study hall
				)
			AND
				(
				@IsScheduled IS NULL OR Session.SessionIsScheduled = @IsScheduled 
				)
		) AS t1
	WHERE @SessionTypeIDs IS NULL
		OR (
			t1.SessionTypeID IN (SELECT CAST(val AS uniqueidentifier) AS sessionTypeID from [dbo].f_split(@SessionTypeIDs, ','))
			)
			
	ORDER BY t1.SessionDateTimeStart ASC



END


