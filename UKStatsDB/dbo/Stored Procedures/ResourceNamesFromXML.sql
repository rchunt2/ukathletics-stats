﻿-- =============================================
-- Author:		Joshua Shearer
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[ResourceNamesFromXML]
	@xml nvarchar(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml
	
	SELECT Resource.ResourceNameFirst, Resource.ResourceNameLast
	FROM Resource
	WHERE Resource.ResourceIsActive = 1 AND Resource.ResourceID IN 
	(
		SELECT
			*
		FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
		WITH 
		( 
			guid uniqueidentifier '.'
		)
	)

END
