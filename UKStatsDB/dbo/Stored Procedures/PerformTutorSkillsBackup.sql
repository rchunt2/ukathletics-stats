﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/13/2012
-- Update date: 5/4/2015
-- Updated by : Ryan Hunter
-- Description:	Flattens out the relational mapping of the Tutor Skills into a table.
--		5/4 - Removed the section to delete the TutorSkillsBackup table.  This table
--				will remain in perpetuity only adding new rows when the item doesn't
--				already exist.  The purpose is to maintain skills past just one semester.
--		6/17 - Added ResourceIsActive check
-- =============================================
CREATE PROCEDURE [dbo].[PerformTutorSkillsBackup]

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--DELETE FROM TutorSkillsBackup

	INSERT INTO TutorSkillsBackup
		SELECT 
			NEWID()
			,Resource.ResourceLogin
			,Resource.ResourceNameFirst
			,Resource.ResourceNameLast
			,Subject.SubjectName
			,Subject.SubjectNameShort
			,Course.CourseName
			,Course.CourseNumber
			
		FROM CourseTutor

		LEFT JOIN Resource ON CourseTutor.ResourceID = Resource.ResourceID
		LEFT JOIN Course ON CourseTutor.CourseID = Course.CourseID
		LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID
		
		--Only insert new rows if they don't already exist
		WHERE NOT EXISTS 
			(	SELECT * 
				FROM TutorSkillsBackup
				WHERE ResourceLogin = Resource.ResourceLogin AND
					  ResourceNameFirst = Resource.ResourceNameFirst AND
					  ResourceNameLast = Resource.ResourceNameLast AND
					  SubjectName = Subject.SubjectName AND
					  SubjectNameShort = Subject.SubjectNameShort AND
					  CourseName = Course.CourseName AND
					  CourseNumber = Course.CourseNumber)
			AND Resource.ResourceIsActive = 1
		
		
END
