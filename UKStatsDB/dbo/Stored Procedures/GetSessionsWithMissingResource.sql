﻿-- =============================================
-- Author:		David Morrison
-- Rewrote "var eSessionsScheduledInactivePresent = (from Session obj in eSessionsScheduledInactive where obj.HasResourcePresent select obj).ToArray();"
--	into a stored procedure
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- Updated Date: 3/31/2016
--		Added Tutoring type at new Football Location (Tutoring - Football)
-- =============================================
CREATE PROCEDURE [dbo].[GetSessionsWithMissingResource]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Now DATETIME
	DECLARE @LoginEarlyMinutes AS INT
	DECLARE @LoginLateMinutes AS INT
	SET @Now = GETDATE()
	SELECT @LoginEarlyMinutes = CAST(SettingValue AS INT) FROM Setting WHERE SettingKey='LoginEarlyMinutes'
	SELECT @LoginLateMinutes = CAST(SettingValue AS INT) FROM Setting WHERE SettingKey='LoginLateMinutes'
	--The second part of the union will return results only when (sudent OR tutor) does NOT exist
	SELECT SessionInfo.*
	FROM (	SELECT [Session].SessionID, [Session].SessionTypeID
			, (SELECT COUNT(*)
				FROM [Attendance]
					JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
					JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
					JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
				WHERE [Resource].ResourceIsActive = 1 
					AND AttendanceTimeOut IS NULL
					AND [Schedule].SessionID = [Session].SessionID
					AND [ResourceType].ResourceTypeName='Student') AS NumStudentsPresent
			, (SELECT COUNT(*)
				FROM [Attendance]
					JOIN [Schedule] ON [Attendance].ScheduleID = [Schedule].ScheduleID
					JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
					JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
				WHERE [Resource].ResourceIsActive = 1 
					AND AttendanceTimeOut IS NULL
					AND [Schedule].SessionID = [Session].SessionID
					AND [ResourceType].ResourceTypeName='Tutor') AS NumTutorsPresent
			FROM [Session]
			WHERE SessionTypeID IN (SELECT SessionTypeID FROM [SessionType] WHERE SessionTypeName IN ('Tutoring', 'Tutoring - Football'))
				AND ((SessionDateTimeStart >= DATEADD(MINUTE, -@LoginEarlyMinutes, @Now) AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginLateMinutes, @Now))
					OR (SessionDateTimeStart >= @Now AND SessionDateTimeStart <= DATEADD(MINUTE, @LoginEarlyMinutes, @Now))
					OR (SessionDateTimeStart <= @Now AND SessionDateTimeend > @Now))
				AND SessionIsCancelled=0
				AND SessionIsScheduled=1
		) SessionInfo
		JOIN (SELECT SessionID, COUNT(*) NumStudentsRequired
				FROM [Schedule]
					JOIN [Resource] ON [Resource].ResourceID = [Schedule].ResourceID
					JOIN [ResourceType] ON [Resource].ResourceTypeID = [ResourceType].ResourceTypeID
				WHERE [Resource].ResourceIsActive = 1 
					AND ScheduleRequireAttend=1
					AND [ResourceType].ResourceTypeName='Student'
				GROUP BY SessionID
			 ) SessionNumStudentsRequired ON SessionNumStudentsRequired.SessionID = SessionInfo.SessionID
	WHERE ((NumStudentsPresent=0 OR NumTutorsPresent=0)
			AND (NumStudentsPresent>=1 OR NumTutorsPresent>=1))
			OR (NumStudentsPresent>0 AND NumStudentsPresent!=NumStudentsRequired)

END


