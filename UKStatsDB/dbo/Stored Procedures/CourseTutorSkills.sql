﻿-- =============================================
-- Author:		Joshua Shearer
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[CourseTutorSkills]
		-- Add the parameters for the stored procedure here
	@ResourceID uniqueidentifier = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		Resource.ResourceID
		,Resource.ResourceNameLast
		,Resource.ResourceNameFirst
		,(Subject.SubjectNameShort + ' ' + Course.CourseNumber) AS Course
		,Course.CourseName
	FROM CourseTutor
	LEFT JOIN Course ON CourseTutor.CourseID = Course.CourseID
	LEFT JOIN Resource ON CourseTutor.ResourceID = Resource.ResourceID
	LEFT JOIN ResourceType ON Resource.ResourceTypeID = ResourceType.ResourceTypeID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID

	WHERE
		ResourceType.ResourceTypeName = 'Tutor'
		AND
		(
			@ResourceID IS NULL
			OR
			Resource.ResourceID = @ResourceID
		)
		AND
			Resource.ResourceIsActive = 1

	ORDER BY Resource.ResourceNameLast ASC, Resource.ResourceNameFirst ASC, Subject.SubjectNameShort ASC, Course.CourseNumber ASC 
END
