﻿-- =============================================
-- Author:		Joshua Shearer
-- Create date: 2/23/2012
-- Description:	Accepts an XML serialization of an array of Resource IDs and performs a select operation to retrieve the data within.
-- =============================================
CREATE PROCEDURE ExtractGuidFromXML
	@xml XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT
		*
	FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
	WITH 
	( 
		guid uniqueidentifier '.'
	)	

END
