﻿-- =============================================
-- Author:		Joshua Shearer
-- Updated By:	Ryan Hunter
-- Updated Date: 6/17/2015
--		Added IsActive check on Resource
-- =============================================
CREATE PROCEDURE [dbo].[CourseRoster]
	@xml XML = NULL
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	SELECT
		Subject.SubjectNameShort,
		Course.CourseNumber,
		CourseSection.CourseSectionNumber,
		Instructor.InstructorName,
		Instructor.InstructorEmail,
		Instructor.InstructorPhone,
		CourseSection.CourseSectionDaysMeet,
		CourseSection.CourseSectionTimeStart,
		CourseSection.CourseSectionTimeEnd,
		CourseSection.CourseSectionBuilding,
		CourseSection.CourseSectionRoom,
		Resource.ResourceNameLast,
		Resource.ResourceNameFirst

	FROM ResourceRegistration
	LEFT JOIN Resource ON ResourceRegistration.ResourceID = Resource.ResourceID
	LEFT JOIN CourseSection ON ResourceRegistration.CourseSectionID = CourseSection.CourseSectionID
	LEFT JOIN Course ON CourseSection.CourseID = Course.CourseID
	LEFT JOIN Subject ON Course.SubjectID = Subject.SubjectID
	LEFT JOIN Instructor ON CourseSection.InstructorID = Instructor.InstructorID

	WHERE 
		(
			Resource.ResourceID IN 
			(
				SELECT
					*
				FROM OPENXML (@idoc, '/ArrayOfGuid/guid',1)
				WITH 
				( 
					guid uniqueidentifier '.'
				)
			)
			AND 
				Resource.ResourceIsActive = 1
		)
		
	ORDER BY 
		Subject.SubjectNameShort ASC,
		Course.CourseNumber ASC,
		CourseSection.CourseSectionNumber ASC,
		Resource.ResourceNameLast ASC,
		Resource.ResourceNameFirst ASC
END