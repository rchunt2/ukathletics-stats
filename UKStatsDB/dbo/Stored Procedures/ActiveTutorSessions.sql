﻿-- =============================================
-- Author:		Donald Slagle
-- Updated by:  Ryan Hunter
-- Update date: 6-17-2015
--				Removed join with Authentication table as Tutors may not necessarily Authenticate before starting a session
--				Added Where clause with timestamp
--				Add IsActive checks on Resource selects
--				3-23-2016
--				Add LocationName and Parameter for filtering by location
-- Update date  6-1-2016
--				Used SessionType instead of location for Filtering
-- Create date: 1-4-2014
-- Description:	Gets all active tutor sessions and the students that are attending the course
-- =============================================
CREATE PROCEDURE [dbo].[ActiveTutorSessions]
	@SessionTypeID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT 
	'ResourceID'				=	[r].[ResourceID],
	'ResourceName'				=	[r].[ResourceNameFirst] + ' ' + [r].[ResourceNameLast],
	'ResourcePhoneNumber'		=	[r].[ResourcePhoneNumber],
	'ResourceEmail'				=	[r].[ResourceEmail],
	'SessionTypeName'			=   [sessionType].[SessionTypeName],
	'RoomNumber'				=	[room].[RoomName],
	'Students'					=	STUFF((SELECT 
												[sr].[ResourceNameFirst] + ' ' + [sr].[ResourceNameLast] AS StudentName 
											FROM [STATS].[dbo].[Resource] sr 
											LEFT JOIN [STATS].[dbo].[Schedule] ssch
												ON sr.ResourceID = ssch.ResourceID
											LEFT JOIN [STATS].[dbo].[Session] ss
												ON ss.SessionID = ssch.SessionID
											WHERE ss.SessionID = [s].[SessionID]
											AND [sr].ResourceTypeID = 'D9387642-8238-E111-8E81-005056936D51'
											AND [sr].ResourceIsActive = 1
											/*FOR XML PATH ('')*/), 1, 0, '')
	FROM [STATS].[dbo].[Resource] r 
	--Tutors may not necessarily authenticate to the system (just entering student ID on Tutoring page) so 
	--this join was removed as no data was returned.
	/*JOIN [STATS].[dbo].[Authentication] a
		ON r.ResourceID = a.ResourceID*/
	JOIN [STATS].[dbo].[Schedule] sch
		ON sch.ResourceID = r.ResourceID
	JOIN [STATS].[dbo].[Session] s
		ON s.SessionID = sch.SessionID
	JOIN [STATS].[dbo].[SessionTutor] st
		ON st.SessionID = s.SessionID
	LEFT JOIN [STATS].[dbo].[Room] room
		ON room.RoomID = st.RoomID
	LEFT JOIN [STATS].[dbo].SessionType sessionType
		ON s.SessionTypeID = sessionType.SessionTypeID
	WHERE [r].[ResourceTypeID] = 'DB387642-8238-E111-8E81-005056936D51'
		AND
			s.SessionDateTimeEnd > CURRENT_TIMESTAMP
		AND 
			s.SessionDateTimeStart < CURRENT_TIMESTAMP
		AND
			[r].ResourceIsActive = 1
		AND 
			(@SessionTypeID  IS NULL OR ([sessionType].SessionTypeID  = @SessionTypeID ))
	ORDER BY RoomNumber ASC
END


