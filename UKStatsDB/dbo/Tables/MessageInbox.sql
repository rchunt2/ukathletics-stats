﻿CREATE TABLE [dbo].[MessageInbox] (
    [MessageInboxID]          UNIQUEIDENTIFIER NOT NULL,
    [MessageID]               UNIQUEIDENTIFIER NOT NULL,
    [MessageInboxDateRead]    DATETIME         NULL,
    [MessageInboxDateExpires] DATETIME         NULL,
    [MessageInboxDateDeleted] DATETIME         NULL,
    [MessageInboxDateSent]    DATETIME         CONSTRAINT [DF_MessageInbox_MessageInboxDateSent] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessageInbox] PRIMARY KEY CLUSTERED ([MessageInboxID] ASC),
    CONSTRAINT [FK_MessageInbox_Message] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[Message] ([MessageID])
);

