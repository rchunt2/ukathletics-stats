﻿CREATE TABLE [dbo].[ResourcePicture] (
    [ResourceID]           UNIQUEIDENTIFIER NOT NULL,
    [ResourcePictureBytes] VARBINARY (MAX)  NOT NULL,
    CONSTRAINT [PK_ResourcePicture] PRIMARY KEY CLUSTERED ([ResourceID] ASC),
    CONSTRAINT [FK_ResourcePicture_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID]) ON DELETE CASCADE
);

