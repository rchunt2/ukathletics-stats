﻿CREATE TABLE [dbo].[MessageType] (
    [MessageTypeID]          UNIQUEIDENTIFIER CONSTRAINT [DF_MessageType_MessageTypeID] DEFAULT (newsequentialid()) NOT NULL,
    [MessageTypeName]        VARCHAR (32)     NOT NULL,
    [MessageTypeDescription] TEXT             NOT NULL,
    CONSTRAINT [PK_MessageType] PRIMARY KEY CLUSTERED ([MessageTypeID] ASC)
);

