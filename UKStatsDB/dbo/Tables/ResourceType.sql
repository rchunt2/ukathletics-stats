﻿CREATE TABLE [dbo].[ResourceType] (
    [ResourceTypeID]          UNIQUEIDENTIFIER CONSTRAINT [DF_ResourceType_ResourceTypeID] DEFAULT (newsequentialid()) NOT NULL,
    [ResourceTypeName]        VARCHAR (32)     NOT NULL,
    [ResourceTypeDescription] TEXT             NOT NULL,
    CONSTRAINT [PK_ResourceType] PRIMARY KEY CLUSTERED ([ResourceTypeID] ASC)
);

