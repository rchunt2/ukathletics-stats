﻿CREATE TABLE [dbo].[Message] (
    [MessageID]             UNIQUEIDENTIFIER NOT NULL,
    [MessageTypeID]         UNIQUEIDENTIFIER NOT NULL,
    [MessageContentID]      UNIQUEIDENTIFIER NOT NULL,
    [MessageFromResourceID] UNIQUEIDENTIFIER NOT NULL,
    [MessageToResourceID]   UNIQUEIDENTIFIER NULL,
    [MessageToGroupID]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK_Message_Group_To] FOREIGN KEY ([MessageToGroupID]) REFERENCES [dbo].[Group] ([GroupID]),
    CONSTRAINT [FK_Message_MessageContent] FOREIGN KEY ([MessageContentID]) REFERENCES [dbo].[MessageContent] ([MessageContentID]),
    CONSTRAINT [FK_Message_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [dbo].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_Message_Resource_From] FOREIGN KEY ([MessageFromResourceID]) REFERENCES [dbo].[Resource] ([ResourceID]),
    CONSTRAINT [FK_Message_Resource_To] FOREIGN KEY ([MessageToResourceID]) REFERENCES [dbo].[Resource] ([ResourceID])
);

