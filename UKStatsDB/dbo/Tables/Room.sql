﻿CREATE TABLE [dbo].[Room] (
    [RoomID]       UNIQUEIDENTIFIER CONSTRAINT [DF_Room_RoomID] DEFAULT (newsequentialid()) NOT NULL,
    [RoomName]     VARCHAR (16)     NOT NULL,
    [RoomPriority] INT              NOT NULL,
    [RoomCapacity] INT              NOT NULL,
    [RoomEnabled] BIT NOT NULL DEFAULT 1, 
    [LocationID] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED ([RoomID] ASC)
);

