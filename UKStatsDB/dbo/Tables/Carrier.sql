﻿CREATE TABLE [dbo].[Carrier] (
    [CarrierID]          UNIQUEIDENTIFIER CONSTRAINT [DF_Carrier_CarrierID] DEFAULT (newsequentialid()) NOT NULL,
    [CarrierName]        VARCHAR (32)     NOT NULL,
    [CarrierEmailSuffix] VARCHAR (32)     NOT NULL,
    CONSTRAINT [PK_Carrier] PRIMARY KEY CLUSTERED ([CarrierID] ASC)
);

