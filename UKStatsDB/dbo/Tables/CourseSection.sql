﻿CREATE TABLE [dbo].[CourseSection] (
    [CourseSectionID]        UNIQUEIDENTIFIER CONSTRAINT [DF_CourseSection_CourseSectionID] DEFAULT (newsequentialid()) NOT NULL,
    [CourseID]               UNIQUEIDENTIFIER NOT NULL,
    [InstructorID]           UNIQUEIDENTIFIER NULL,
    [CourseSectionDateStart] DATE             NOT NULL,
    [CourseSectionDateEnd]   DATE             NOT NULL,
    [CourseSectionDaysMeet]  VARCHAR (7)      NOT NULL,
    [CourseSectionTimeStart] TIME (0)         NOT NULL,
    [CourseSectionTimeEnd]   TIME (0)         NOT NULL,
    [CourseSectionNumber]    VARCHAR (8)      NOT NULL,
    [CourseSectionPackageID] VARCHAR (16)     NOT NULL,
    [CourseSectionBuilding]  VARCHAR (32)     NOT NULL,
    [CourseSectionRoom]      VARCHAR (32)     NOT NULL,
    [CourseSectionEventType] VARCHAR (4)      NOT NULL,
    CONSTRAINT [PK_CourseSection] PRIMARY KEY CLUSTERED ([CourseSectionID] ASC),
    CONSTRAINT [FK_CourseSection_Course] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID]),
    CONSTRAINT [FK_CourseSection_Instructor] FOREIGN KEY ([InstructorID]) REFERENCES [dbo].[Instructor] ([InstructorID])
);

