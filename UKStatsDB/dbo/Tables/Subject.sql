﻿CREATE TABLE [dbo].[Subject] (
    [SubjectID]          UNIQUEIDENTIFIER CONSTRAINT [DF_Subject_SubjectID] DEFAULT (newsequentialid()) NOT NULL,
    [SubjectNameShort]   VARCHAR (8)      NOT NULL,
    [SubjectName]        VARCHAR (64)     NOT NULL,
    [SubjectDescription] TEXT             NOT NULL,
    CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED ([SubjectID] ASC)
);

