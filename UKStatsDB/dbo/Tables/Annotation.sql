﻿CREATE TABLE [dbo].[Annotation] (
    [AnnotationID]   UNIQUEIDENTIFIER NOT NULL,
    [SourceID]       UNIQUEIDENTIFIER NOT NULL,
    [AnnotationText] TEXT             NOT NULL,
    CONSTRAINT [PK_Annotation] PRIMARY KEY CLUSTERED ([AnnotationID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SourceID]
    ON [dbo].[Annotation]([SourceID] ASC);

