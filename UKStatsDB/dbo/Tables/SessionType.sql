﻿CREATE TABLE [dbo].[SessionType] (
    [SessionTypeID]          UNIQUEIDENTIFIER CONSTRAINT [DF_SessionType_SessionTypeID] DEFAULT (newsequentialid()) NOT NULL,
    [SessionTypeName]        VARCHAR (32)     NOT NULL,
    [SessionTypeDescription] TEXT             NOT NULL,
    CONSTRAINT [PK_SessionType] PRIMARY KEY CLUSTERED ([SessionTypeID] ASC)
);

