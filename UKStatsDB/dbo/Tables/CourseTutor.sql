﻿CREATE TABLE [dbo].[CourseTutor] (
    [CourseTutorID] UNIQUEIDENTIFIER NOT NULL,
    [CourseID]      UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_CourseTutor] PRIMARY KEY CLUSTERED ([CourseTutorID] ASC),
    CONSTRAINT [FK_CourseTutor_Course] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID]),
    CONSTRAINT [FK_CourseTutor_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID])
);

