﻿CREATE TABLE [dbo].[MessageContent] (
    [MessageContentID]      UNIQUEIDENTIFIER NOT NULL,
    [MessageContentSubject] VARCHAR (128)    NOT NULL,
    [MessageContentBody]    TEXT             NOT NULL,
    CONSTRAINT [PK_MessageContent] PRIMARY KEY CLUSTERED ([MessageContentID] ASC)
);

