﻿CREATE TABLE [dbo].[TextMessage] (
    [TextMessageID]   UNIQUEIDENTIFIER CONSTRAINT [DF_TextMessage_TextMessageID] DEFAULT (newsequentialid()) NOT NULL,
    [ResourceID]      UNIQUEIDENTIFIER NOT NULL,
    [SessionID]       UNIQUEIDENTIFIER NULL,
    [CourseSectionID] UNIQUEIDENTIFIER NULL,
    [MessageSent]     BIT              CONSTRAINT [DF_TextMessage_MessageSent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TextMessage] PRIMARY KEY CLUSTERED ([TextMessageID] ASC)
);

