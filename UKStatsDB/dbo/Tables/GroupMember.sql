﻿CREATE TABLE [dbo].[GroupMember] (
    [GroupMemberID] UNIQUEIDENTIFIER NOT NULL,
    [GroupID]       UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_GroupMember] PRIMARY KEY CLUSTERED ([GroupMemberID] ASC),
    CONSTRAINT [FK_GroupMember_Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[Group] ([GroupID]),
    CONSTRAINT [FK_GroupMember_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID])
);

