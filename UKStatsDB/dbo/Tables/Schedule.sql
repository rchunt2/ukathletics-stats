﻿CREATE TABLE [dbo].[Schedule] (
    [ScheduleID]            UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]            UNIQUEIDENTIFIER NOT NULL,
    [SessionID]             UNIQUEIDENTIFIER NOT NULL,
    [ScheduleRequireAttend] BIT              NOT NULL,
    CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED ([ScheduleID] ASC),
    CONSTRAINT [FK_Resource_Schedule] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID]),
    CONSTRAINT [FK_Schedule_Session] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
);


GO
CREATE NONCLUSTERED INDEX [IX_SessionID]
    ON [dbo].[Schedule]([SessionID] ASC);

