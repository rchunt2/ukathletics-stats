﻿CREATE TABLE [dbo].[Location] (
    [LocationID]   UNIQUEIDENTIFIER CONSTRAINT [DF_Location_LocationID] DEFAULT (newsequentialid()) NOT NULL,
    [LocationName] VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);

