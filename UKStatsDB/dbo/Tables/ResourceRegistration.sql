﻿CREATE TABLE [dbo].[ResourceRegistration] (
    [ResourceRegistrationID] UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]             UNIQUEIDENTIFIER NOT NULL,
    [CourseSectionID]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_ResourceRegistration] PRIMARY KEY CLUSTERED ([ResourceRegistrationID] ASC),
    CONSTRAINT [FK_ResourceRegistration_CourseSection] FOREIGN KEY ([CourseSectionID]) REFERENCES [dbo].[CourseSection] ([CourseSectionID]),
    CONSTRAINT [FK_ResourceRegistration_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID])
);

