﻿CREATE TABLE [dbo].[Setting] (
    [SettingID]    UNIQUEIDENTIFIER CONSTRAINT [DF_Setting_SettingID] DEFAULT (newsequentialid()) NOT NULL,
    [SettingKey]   VARCHAR (64)     NOT NULL,
    [SettingValue] VARCHAR (1024)   NOT NULL,
    CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED ([SettingID] ASC)
);

