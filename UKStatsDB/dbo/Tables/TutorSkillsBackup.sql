﻿CREATE TABLE [dbo].[TutorSkillsBackup] (
    [TutorSkillsBackupID] UNIQUEIDENTIFIER CONSTRAINT [DF_TutorSkillsBackup_TutorSkillsBackupID] DEFAULT (newsequentialid()) NOT NULL,
    [ResourceLogin]       VARCHAR (32)     NOT NULL,
    [ResourceNameFirst]   VARCHAR (32)     NOT NULL,
    [ResourceNameLast]    VARCHAR (32)     NOT NULL,
    [SubjectNameShort]    VARCHAR (8)      NOT NULL,
    [SubjectName]         VARCHAR (64)     NOT NULL,
    [CourseName]          VARCHAR (40)     NOT NULL,
    [CourseNumber]        VARCHAR (8)      NOT NULL
);

