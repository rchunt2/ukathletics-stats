﻿CREATE TABLE [dbo].[Authentication] (
    [AuthenticationID]     UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]           UNIQUEIDENTIFIER NOT NULL,
    [AuthenticationExpire] DATETIME         NOT NULL,
    CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED ([AuthenticationID] ASC),
    CONSTRAINT [FK_Authentication_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID]) ON DELETE CASCADE
);

