﻿CREATE TABLE [dbo].[SessionTutor] (
    [SessionTutorID] UNIQUEIDENTIFIER CONSTRAINT [DF_SessionTutor_SessionTutorID] DEFAULT (newsequentialid()) NOT NULL,
    [SessionID]      UNIQUEIDENTIFIER NOT NULL,
    [RoomID]         UNIQUEIDENTIFIER NULL,
    [CourseID]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SessionTutor] PRIMARY KEY CLUSTERED ([SessionTutorID] ASC),
    CONSTRAINT [FK_SessionTutor_Course] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID]),
    CONSTRAINT [FK_SessionTutor_Room] FOREIGN KEY ([RoomID]) REFERENCES [dbo].[Room] ([RoomID]),
    CONSTRAINT [FK_SessionTutor_Session] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
);


GO
CREATE NONCLUSTERED INDEX [IX_SessionID]
    ON [dbo].[SessionTutor]([SessionID] ASC);

