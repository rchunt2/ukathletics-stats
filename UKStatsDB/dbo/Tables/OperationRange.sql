﻿CREATE TABLE [dbo].[OperationRange] (
    [OperationRangeID]        UNIQUEIDENTIFIER CONSTRAINT [DF_OperationRange_OperationRangeID] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [SessionTypeID]           UNIQUEIDENTIFIER NOT NULL,
    [OperationRangeDayOfWeek] CHAR (3)         NOT NULL,
    [OperationRangeTimeStart] TIME (0)         NOT NULL,
    [OperationRangeTimeEnd]   TIME (0)         NOT NULL,
    CONSTRAINT [PK_OperationRange] PRIMARY KEY CLUSTERED ([OperationRangeID] ASC),
    CONSTRAINT [FK_OperationRange_SessionType] FOREIGN KEY ([SessionTypeID]) REFERENCES [dbo].[SessionType] ([SessionTypeID])
);

