﻿CREATE TABLE [dbo].[Error] (
    [ErrorID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Error_ErrorID] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [ResourceID]     UNIQUEIDENTIFIER NULL,
    [ErrorDateTime]  DATETIME         NOT NULL,
    [ErrorText]      TEXT             NOT NULL,
    [ErrorIPAddress] VARCHAR (15)     NOT NULL,
    CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED ([ErrorID] ASC),
    CONSTRAINT [FK_Error_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID])
);

