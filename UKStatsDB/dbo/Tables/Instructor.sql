﻿CREATE TABLE [dbo].[Instructor] (
    [InstructorID]    UNIQUEIDENTIFIER CONSTRAINT [DF_Instructor_InstructorID] DEFAULT (newsequentialid()) NOT NULL,
    [InstructorName]  VARCHAR (40)     NOT NULL,
    [InstructorEmail] VARCHAR (128)    NULL,
    [InstructorPhone] VARCHAR (16)     NULL,
    CONSTRAINT [PK_Instructor] PRIMARY KEY CLUSTERED ([InstructorID] ASC)
);

