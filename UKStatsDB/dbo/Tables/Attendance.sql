﻿CREATE TABLE [dbo].[Attendance] (
    [AttendanceID]           UNIQUEIDENTIFIER NOT NULL,
    [ScheduleID]             UNIQUEIDENTIFIER NOT NULL,
    [AttendanceTimeIn]       DATETIME         NOT NULL,
    [AttendanceTimeOut]      DATETIME         NULL,
    [AttendanceMachineName]  VARCHAR (32)     NULL,
    [AttendanceTimeOverride] INT              NULL,
    CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED ([AttendanceID] ASC),
    CONSTRAINT [FK_Schedule_Attendance] FOREIGN KEY ([ScheduleID]) REFERENCES [dbo].[Schedule] ([ScheduleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AttendanceTimeOut]
    ON [dbo].[Attendance]([AttendanceTimeOut] ASC);

