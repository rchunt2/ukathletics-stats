﻿CREATE TABLE [dbo].[Session] (
    [SessionID]            UNIQUEIDENTIFIER NOT NULL,
    [CourseSectionID]      UNIQUEIDENTIFIER NULL,
    [SessionTypeID]        UNIQUEIDENTIFIER NOT NULL,
    [SessionDateTimeStart] DATETIME         NOT NULL,
    [SessionDateTimeEnd]   DATETIME         NOT NULL,
    [SessionIsCancelled]   BIT              NOT NULL,
    [SessionIsScheduled]   BIT              NOT NULL,
	[GroupID]			   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED ([SessionID] ASC),
    CONSTRAINT [FK_Session_CourseSection] FOREIGN KEY ([CourseSectionID]) REFERENCES [dbo].[CourseSection] ([CourseSectionID]),
    CONSTRAINT [FK_Session_SessionType] FOREIGN KEY ([SessionTypeID]) REFERENCES [dbo].[SessionType] ([SessionTypeID]),
	CONSTRAINT [FK_Session_Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[Group] ([GroupID])
);

