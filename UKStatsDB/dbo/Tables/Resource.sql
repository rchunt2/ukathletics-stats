﻿CREATE TABLE [dbo].[Resource] (
    [ResourceID]             UNIQUEIDENTIFIER CONSTRAINT [DF_Resource_ResourceID] DEFAULT (newsequentialid()) NOT NULL,
    [ResourceTypeID]         UNIQUEIDENTIFIER NOT NULL,
    [ResourceLogin]          VARCHAR (32)     NOT NULL,
    [ResourcePassword]       VARCHAR (32)     NOT NULL,
    [ResourceNameFirst]      VARCHAR (32)     NOT NULL,
    [ResourceNameLast]       VARCHAR (32)     NOT NULL,
    [ResourcePhoneNumber]    VARCHAR (16)     NULL,
    [ResourceEmail]          VARCHAR (128)    NULL,
    [CarrierID]              UNIQUEIDENTIFIER NULL,
    [ResourceTextingEnabled] BIT              CONSTRAINT [DF_Resource_ResourceTextingEnabled] DEFAULT ((0)) NOT NULL,
    [ResourceRoom]           BIT              NULL,
    [ResourceADLogin]        VARCHAR (20)     NULL,
    [ResourceIsActive] BIT NULL, 
    CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED ([ResourceID] ASC),
    CONSTRAINT [FK_Resource_Carrier] FOREIGN KEY ([CarrierID]) REFERENCES [dbo].[Carrier] ([CarrierID]),
    CONSTRAINT [FK_Resource_ResourceType] FOREIGN KEY ([ResourceTypeID]) REFERENCES [dbo].[ResourceType] ([ResourceTypeID])
);

