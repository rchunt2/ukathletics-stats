﻿CREATE TABLE [dbo].[Group] (
    [GroupID]          UNIQUEIDENTIFIER NOT NULL,
    [GroupName]        VARCHAR (32)     NOT NULL,
    [GroupDescription] TEXT             NOT NULL,
    CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([GroupID] ASC)
);

