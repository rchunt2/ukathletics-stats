﻿CREATE TABLE [dbo].[FreeTime] (
    [FreeTimeID]        UNIQUEIDENTIFIER NOT NULL,
    [ResourceID]        UNIQUEIDENTIFIER NOT NULL,
    [SessionTypeID]     UNIQUEIDENTIFIER NOT NULL,
    [FreeTimeApplyDate] DATETIME         NOT NULL,
    [FreeTimeAmount]    INT              NOT NULL,
    [FreeTimeAddDate]   DATETIME         NOT NULL,
    CONSTRAINT [PK_FreeTime] PRIMARY KEY CLUSTERED ([FreeTimeID] ASC),
    CONSTRAINT [FK_FreeTime_Resource] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[Resource] ([ResourceID]),
    CONSTRAINT [FK_FreeTime_SessionType] FOREIGN KEY ([SessionTypeID]) REFERENCES [dbo].[SessionType] ([SessionTypeID])
);

