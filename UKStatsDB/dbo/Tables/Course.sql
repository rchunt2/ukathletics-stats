﻿CREATE TABLE [dbo].[Course] (
    [CourseID]       UNIQUEIDENTIFIER CONSTRAINT [DF_Course_CourseID] DEFAULT (newsequentialid()) NOT NULL,
    [SubjectID]      UNIQUEIDENTIFIER NOT NULL,
    [CourseName]     VARCHAR (40)     NOT NULL,
    [CourseNumber]   VARCHAR (8)      NOT NULL,
    [CourseModuleID] VARCHAR (16)     NOT NULL,
    CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED ([CourseID] ASC),
    CONSTRAINT [FK_Course_Subject] FOREIGN KEY ([SubjectID]) REFERENCES [dbo].[Subject] ([SubjectID])
);

