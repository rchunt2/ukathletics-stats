﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Student
{
    public partial class Login1 : System.Web.UI.Page
    {
        public UK.STATS.STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = UK.STATS.STATSModel.ResourceType.Get("Tutor");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = UK.STATS.STATSModel.ResourceType.Get("Student");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeDirector
        {
            get
            {
                if (Application["ResourceTypeDirector"] == null)
                {
                    Application["ResourceTypeDirector"] = UK.STATS.STATSModel.ResourceType.Get("Director");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeDirector"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = UK.STATS.STATSModel.ResourceType.Get("Administrator");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeTimeMgr
        {
            get
            {
                if (Application["ResourceTypeTimeMgr"] == null)
                {
                    Application["ResourceTypeTimeMgr"] = UK.STATS.STATSModel.ResourceType.Get("Time Mgr");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeTimeMgr"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeMonitor
        {
            get
            {
                if (Application["ResourceTypeMonitor"] == null)
                {
                    Application["ResourceTypeMonitor"] = UK.STATS.STATSModel.ResourceType.Get("Monitor");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeMonitor"];
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            lblApplicationName.Text = UK.STATS.STATSModel.Setting.GetByKey("ApplicationName").SettingValue.ToString() + " STUDENT VERSION";

            //UK.STATS.STATSModel.Authentication.ExpireSessions();
            Guid AuthKey = Guid.Empty;
            if (Session["AuthenticationKey"] != null)
            {
                AuthKey = Guid.Parse(Session["AuthenticationKey"].ToString());
            }

            if (AuthKey != Guid.Empty)
            {
                Response.Redirect("Default.aspx", true);
            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            String ResourceLogin = txtUserName.Text;
            Boolean isValid = UK.STATS.STATSModel.Resource.Exists(ResourceLogin);

            if (isValid)
            {
                //lblError.Text = "";
                UK.STATS.STATSModel.Resource CurrentResource = UK.STATS.STATSModel.Resource.Get(ResourceLogin);
                
                //Tutors can login and view their plan sheet
                Boolean isTutor = (CurrentResource.ResourceTypeID == ResourceTypeTutor.ResourceTypeID);

                System.Web.HttpContext.Current.Session["ResourceLogin"] = ResourceLogin;
                

                if (isTutor)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tutors Cannot Login", "<script language='javascript'>alert('Tutors are not allowed to login to Study Hall.');</script>", false);
                    //txtResourceLogin.Text = "";
                    //return;
                    Response.Redirect("Pages/ResourceSchedule.aspx");
                }
                else
                {
                    Response.Redirect("Pages/TimeTracker.aspx");
                }

                
            }
            else
            {
                FailureText.Text = "The entered username was invalid.<br />Please try again.";

            }
        }

    }
}