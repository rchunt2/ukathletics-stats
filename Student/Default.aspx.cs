﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Student
{
    public partial class Default : System.Web.UI.Page
    {
        public UK.STATS.STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = UK.STATS.STATSModel.ResourceType.Get("Tutor");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = UK.STATS.STATSModel.ResourceType.Get("Student");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UK.STATS.STATSModel.Attendance.AutoLogout(ResourceTypeTutor, ResourceTypeStudent);
        }

        
    }
}