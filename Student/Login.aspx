﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserAuthentication.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Student.Login1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
    <div class="LoginScreenTitle">
        <img class="LoginScreenLogo" src="Images/Logo_Full.png" /><br />
        <asp:Label ID="lblApplicationName" runat="server"></asp:Label>
    </div>
    <div class="LoginScreenForm">
                <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                    <tr>
                        <td>
                            <table cellpadding="0">
                                <tr>
                                    <td align="center" colspan="2">
                                       Student Log In</td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="txtUserName">User ID:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUserName" runat="server" Width="125px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                            ControlToValidate="txtUserName" ErrorMessage="User ID is required." 
                                            ToolTip="User ID is required." ValidationGroup="LoginControl">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="color:Red;">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2"><br />
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
                                            ValidationGroup="LoginControl" OnClick="LoginButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            
    </div>
</asp:Content>
