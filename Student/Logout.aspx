﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserAuthentication.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="UK.Stats.Student.Logout" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
    <h1>Logout Successful</h1>
    <p>Please wait while we redirect you to the login page.</p>
</asp:Content>
