﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace Student
{
    public partial class Layout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblApplicationName.Text = Setting.GetByKey("ApplicationName").SettingValue.ToString();

            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["ResourceLogin"] == null || System.Web.HttpContext.Current.Session["ResourceLogin"].ToString() == String.Empty)
            {   // There is no authenticated user.
                Response.Redirect("~/Login.aspx", true);
            }
        }
    }
}