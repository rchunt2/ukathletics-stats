﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace Student.UserControls
{
    public partial class NavigationMenu : System.Web.UI.UserControl
    {
        //DM - 9/25/2012 - changed resource types to pull from Application variable to prevent lots of unnessary hits to the DB since they do not change.

        public UK.STATS.STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = UK.STATS.STATSModel.ResourceType.Get("Tutor");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = UK.STATS.STATSModel.ResourceType.Get("Student");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = UK.STATS.STATSModel.ResourceType.Get("Administrator");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        public Guid ResourceID
        {
            get
            {
                string _resourceLogin = System.Web.HttpContext.Current.Session["ResourceLogin"].ToString();
                Resource _currentResource = UK.STATS.STATSModel.Resource.Get(_resourceLogin);
                return _currentResource.ResourceID;
            }
        }


        UK.STATS.STATSModel.Resource eResource;
        UK.STATS.STATSModel.ResourceType eResourceType;

        protected void Page_Load(object sender, EventArgs e)
        {
            eResource = Resource.Get(ResourceID);
            eResourceType = eResource.ResourceType;
        }

        protected void tvNavigation_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            if (e.Node.Parent != null)
            {
                
                ////DM - 9/25/2012 - moved these vars to be global on the page since TreeNodeDataBound is called for each node, and it was hitting the DB 3x for each node, now it is just 3 calls per postback.

                if (eResourceType.ResourceTypeID == ResourceTypeTutor.ResourceTypeID)
                {
                    if (e.Node.Text == "Time Totals" || e.Node.Text == "Class Schedule")
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);
                    }
                }
                else if (eResourceType.ResourceTypeID == ResourceTypeStudent.ResourceTypeID)
                {
                    // Everything
                }
                //else if (eResourceType.ResourceTypeID == ResourceTypeMonitor.ResourceTypeID)
                //{
                //    if (e.Node.Text != "Home" && e.Node.Text != "Group") //leave home in for monitor, may add other pages to leave access to here. Also leave in Group page
                //    {
                //        e.Node.Parent.ChildNodes.Remove(e.Node);
                //    }
                //}
                //else if (eResourceType.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID)
                //{
                //    if (e.Node.Text == "Settings")
                //    {
                //        e.Node.Parent.ChildNodes.Remove(e.Node);
                //    }
                //}
                else
                {
                    // Remove all the nodes!
                    e.Node.Parent.ChildNodes.Remove(e.Node);
                }
            }

        }
    }
}