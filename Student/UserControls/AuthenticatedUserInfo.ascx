﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthenticatedUserInfo.ascx.cs" Inherits="Student.UserControls.AuthenticatedUserInfo" %>

<asp:Panel ID="panelAuthenticatedUserInfo" Visible="false" runat="server">
    <div class="AuthUserInfo">
        <span>Welcome back,</span>
        <span style="font-style: italic; font-weight: bold"><asp:Label ID="lblName" runat="server" /></span> |
        <span style="font-weight: bold"><asp:HyperLink ID="logoutLink" runat="server" NavigateUrl="../Logout.aspx">Logout</asp:HyperLink></span>
    </div>
</asp:Panel>