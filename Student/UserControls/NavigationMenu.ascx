﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMenu.ascx.cs" Inherits="Student.UserControls.NavigationMenu" %>

<div id="navigation">
    <asp:TreeView ID="tvNavigation" DataSourceID="SiteMapDataSource1" runat="server" ViewStateMode="Enabled" NodeIndent="0" ShowExpandCollapse="False" ontreenodedatabound="tvNavigation_TreeNodeDataBound"></asp:TreeView>
    
    <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" SiteMapProvider="NavigationSiteMap" />
</div>