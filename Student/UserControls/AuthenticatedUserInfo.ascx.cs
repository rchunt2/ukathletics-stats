﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace Student.UserControls
{
    public partial class AuthenticatedUserInfo : System.Web.UI.UserControl
    {
        public Boolean isVisible
        {
            get { return panelAuthenticatedUserInfo.Visible; }
            set { panelAuthenticatedUserInfo.Visible = value; }
        }

        public void ShowAndBind()
        {
            this.Visible = true;
            panelAuthenticatedUserInfo.Visible = true;
            this.Bind();
        }

        private void Bind()
        {
            if (System.Web.HttpContext.Current.Session["ResourceLogin"] != null &&
                !String.IsNullOrEmpty(System.Web.HttpContext.Current.Session["ResourceLogin"].ToString()))
            {
                string _resourceLogin = System.Web.HttpContext.Current.Session["ResourceLogin"].ToString();
                Resource _currentResource = UK.STATS.STATSModel.Resource.Get(_resourceLogin);
                if (this.Visible)
                {
                    lblName.Text = _currentResource.ResourceNameFirst + " ";
                    lblName.ToolTip = _currentResource.ResourceID.ToString();
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowAndBind();
        }
    }
}