﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserAuthentication.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="UK.Stats.Student.Error" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
    <div class="ErrorScreen">
        <div class="ErrorScreenLogo">
            <img src="Images/Logo_Full.png" />
        </div>
        
        <div class="ErrorScreenTitle">
            Error!
        </div>

        <div class="ErrorScreenError">
            <asp:Label ID="lblErrorText" runat="server" Text="" />
        </div>
    </div>
</asp:Content>