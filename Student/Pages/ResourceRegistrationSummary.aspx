﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Layout.Master" CodeBehind="ResourceRegistrationSummary.aspx.cs" Inherits="Student.Pages.ResourceRegistrationSummary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">        
<div id="ResourceRegSummary">
    <asp:Panel ID="panelResourceRegistration" runat="server" Visible="true">
        <asp:Label ID="lblResourceName" runat="server" Text="ResourceName"></asp:Label>
        <asp:GridView ID="gvResourceRegistration" runat="server" CellPadding="2" AutoGenerateColumns="False" 
                EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left"
                onrowdatabound="gvResourceRegistration_RowDataBound" Width="100%"  >
                <Columns>
                    <asp:TemplateField HeaderText="CourseSectionID_HIDDEN" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="CourseSectionID" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CourseDisplayName" HeaderText="Course" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionDaysMeet" HeaderText="Days Meet" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeStart" HeaderText="Start Time" DataFormatString="{0:t}" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeEnd" HeaderText="End Time" DataFormatString="{0:t}" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" runat="server" Text="N/A"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <div style="clear: both; height: 0;"></div>
    </asp:Panel>
</div>
</asp:Content>
