﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace Student.Pages
{
    public partial class ResourceRegistrationSummary : System.Web.UI.Page
    {
        public UK.STATS.STATSModel.Resource ResourceObject { 
            get 
            {
                string _resourceLogin = System.Web.HttpContext.Current.Session["ResourceLogin"].ToString();
                Resource _currentResource = UK.STATS.STATSModel.Resource.Get(_resourceLogin);
                return _currentResource;
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = UK.STATS.STATSModel.ResourceType.Get("Tutor");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public UK.STATS.STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = UK.STATS.STATSModel.ResourceType.Get("Student");
                }
                return (UK.STATS.STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindAndShow();
        }

        public void BindAndShow()
        {
            lblResourceName.Text = this.ResourceObject.ResourceNameDisplayLastFirst;
            CourseSection[] eCourseSections = ResourceRegistration.GetRegisteredCourses(ResourceObject);
            eCourseSections = (from UK.STATS.STATSModel.CourseSection obj in eCourseSections orderby obj.Course.Subject.SubjectNameShort ascending, obj.Course.CourseNumber ascending select obj).ToArray();

            if (eCourseSections.Length > 0 && eCourseSections[0] != null)
            {
                gvResourceRegistration.DataSource = eCourseSections;
                gvResourceRegistration.DataBind();
            }

        }

        protected void gvResourceRegistration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                UK.STATS.STATSModel.CourseSection CourseSectionObj = (UK.STATS.STATSModel.CourseSection)e.Row.DataItem;
                ((Label)e.Row.FindControl("CourseSectionID")).Text = CourseSectionObj.CourseSectionID.ToString();
                ((Label)e.Row.FindControl("lblLocation")).Text = CourseSectionObj.CourseSectionBuilding + " " + CourseSectionObj.CourseSectionRoom;
            }
        }

        protected void gvResourceRegistrationTutoring_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                UK.STATS.STATSModel.Schedule ScheduleObj = (UK.STATS.STATSModel.Schedule)e.Row.DataItem;

                ((Label)e.Row.FindControl("ScheduleID")).Text = ScheduleObj.ScheduleID.ToString();
                ((Label)e.Row.FindControl("lblCourseName")).Text = ScheduleObj.Session.CourseSection.CourseDisplayName;
                ((Label)e.Row.FindControl("lblTutorName")).Text = ScheduleObj.Session.GetTutor(ResourceTypeTutor).ResourceNameLast + ", " + ScheduleObj.Session.GetTutor(ResourceTypeTutor).ResourceNameFirst;
                ((Label)e.Row.FindControl("lblSessionDate")).Text = ScheduleObj.Session.SessionDateTimeStart.Date.ToShortDateString();
                ((Label)e.Row.FindControl("lblSessionTimeStart")).Text = ScheduleObj.Session.SessionDateTimeStart.ToShortTimeString();
                ((Label)e.Row.FindControl("lblSessionTimeEnd")).Text = ScheduleObj.Session.SessionDateTimeEnd.ToShortTimeString();
            }
        }

    }
}