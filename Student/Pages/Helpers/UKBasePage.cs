﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;


namespace UK.Stats.Student.Pages
{
    public class UKBasePage : System.Web.UI.Page
    {
        public void StoreInViewstate(string key, object value)
        {
            if (ViewState[key] != null) ViewState.Remove(key);
            ViewState.Add(key, value);            
        }

        public T GetFromViewState<T>(string key)
        {
            if (ViewState[key] != null)
                return ((T)ViewState[key]);
            else
                return default(T);
        }

        public UK.STATS.STATSModel.Authentication GetCurrentAuthentication()
        {
            UK.STATS.STATSModel.Authentication obj = GetFromViewState<UK.STATS.STATSModel.Authentication>("CurrentAuthentication");
            if (obj == null)
            {
                obj = Helpers.UKHelper.CurrentAuthentication();
                StoreInViewstate("CurrentAuthentication", obj);
            }
            return obj;
        }

        public virtual void Bind() { }        
    }
}