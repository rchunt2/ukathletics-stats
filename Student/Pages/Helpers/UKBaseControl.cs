﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace UK.Stats.Student.Pages
{
    public class UKBaseControl : System.Web.UI.UserControl
    {
        public void StoreInViewstate(string key, object value)
        {
            if (ViewState[key] != null) ViewState.Remove(key);
            ViewState.Add(key, value);
        }

        public T GetFromViewState<T>(string key)
        {
            if (ViewState[key] != null)
                return ((T)ViewState[key]);
            else
                return default(T);
        }

        public virtual void Bind() { }   
    }
}