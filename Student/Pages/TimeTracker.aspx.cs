﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace Student.Pages
{
    public partial class TimeTracker : System.Web.UI.Page
    {
        private DateTime _dateStart = DateTime.Today;

        public Guid ResourceID
        {
            get
            {
                string _resourceLogin = System.Web.HttpContext.Current.Session["ResourceLogin"].ToString();
                Resource _currentResource = UK.STATS.STATSModel.Resource.Get(_resourceLogin);
                return _currentResource.ResourceID;
            }
        }

        public Boolean IsVisible
        {
            get;
            set;
        }

        public DateTime RangeStart
        {
            get { return _dateStart; }
            set { _dateStart = value; }
        }

        public DateTime RangeEnd
        {
            get { return _dateStart.AddDays(7); }
        }

        public void BindAndShow()
        {
            if (RangeStart.DayOfWeek != DayOfWeek.Sunday)
            {
                //make range start a sunday by subtracting current day of week
                RangeStart = RangeStart.AddDays(-((int)RangeStart.DayOfWeek));
            }

            RangeStart = RangeStart.Date;

            
                this.Visible = true;
                UK.STATS.STATSModel.Resource ResourceObj = UK.STATS.STATSModel.Resource.Get(this.ResourceID);
                String ResourceName = ResourceObj.ResourceNameFirst + " " + ResourceObj.ResourceNameLast;

                //DM - 9/21/2012 - Show Sunday - Saturday for current week
                String DateRange = RangeStart.ToShortDateString() + " - " + RangeEnd.AddSeconds(-1).ToShortDateString();
                lblTimeTrackerDateRange.Text = "Date Range: " + DateRange;

                var attendanceTime = new AttendanceTime();

                UK.STATS.STATSModel.Attendance.CalculateTime(ResourceObj, RangeStart, attendanceTime);

                var tsCompLabSunday = TimeSpan.FromMinutes(attendanceTime.CompLabSunday);
                var tsCompLabMonday = TimeSpan.FromMinutes(attendanceTime.CompLabMonday);
                var tsCompLabTuesday = TimeSpan.FromMinutes(attendanceTime.CompLabTuesday);
                var tsCompLabWednesday = TimeSpan.FromMinutes(attendanceTime.CompLabWednesday);
                var tsCompLabThursday = TimeSpan.FromMinutes(attendanceTime.CompLabThursday);
                var tsCompLabFriday = TimeSpan.FromMinutes(attendanceTime.CompLabFriday);
                var tsCompLabSaturday = TimeSpan.FromMinutes(attendanceTime.CompLabSaturday);
                var tsCompLabTotal = TimeSpan.FromMinutes(attendanceTime.CompLabTotal);

                var tsTutoringSunday = TimeSpan.FromMinutes(attendanceTime.TutoringSunday);
                var tsTutoringMonday = TimeSpan.FromMinutes(attendanceTime.TutoringMonday);
                var tsTutoringTuesday = TimeSpan.FromMinutes(attendanceTime.TutoringTuesday);
                var tsTutoringWednesday = TimeSpan.FromMinutes(attendanceTime.TutoringWednesday);
                var tsTutoringThursday = TimeSpan.FromMinutes(attendanceTime.TutoringThursday);
                var tsTutoringFriday = TimeSpan.FromMinutes(attendanceTime.TutoringFriday);
                var tsTutoringSaturday = TimeSpan.FromMinutes(attendanceTime.TutoringSaturday);
                var tsTutoringTotal = TimeSpan.FromMinutes(attendanceTime.TutoringTotal);

                var tsStudayHallSunday = TimeSpan.FromMinutes(attendanceTime.StudyHallSunday);
                var tsStudyHallMonday = TimeSpan.FromMinutes(attendanceTime.StudyHallMonday);
                var tsStudyHallTuesday = TimeSpan.FromMinutes(attendanceTime.StudyHallTuesday);
                var tsStudyHallWednesday = TimeSpan.FromMinutes(attendanceTime.StudyHallWednesday);
                var tsStudyHallThursday = TimeSpan.FromMinutes(attendanceTime.StudyHallThursday);
                var tsStudyHallFriday = TimeSpan.FromMinutes(attendanceTime.StudyHallFriday);
                var tsStudyHallSaturday = TimeSpan.FromMinutes(attendanceTime.StudyHallSaturday);
                var tsStudyHallTotal = TimeSpan.FromMinutes(attendanceTime.StudyHallTotal);

                var tsSundayTotal = TimeSpan.FromMinutes(attendanceTime.SundayTotal);
                var tsMondayTotal = TimeSpan.FromMinutes(attendanceTime.MondayTotal);
                var tsTuesdayTotal = TimeSpan.FromMinutes(attendanceTime.TuesdayTotal);
                var tsWednesdayTotal = TimeSpan.FromMinutes(attendanceTime.WednesdayTotal);
                var tsThursdayTotal = TimeSpan.FromMinutes(attendanceTime.ThursdayTotal);
                var tsFridayTotal = TimeSpan.FromMinutes(attendanceTime.FridayTotal);
                var tsSaturdayTotal = TimeSpan.FromMinutes(attendanceTime.SaturdayTotal);
                var tsOverallTotal = TimeSpan.FromMinutes(attendanceTime.OverallTotal);

                lblTimeCompLabSunday.Text = tsCompLabSunday.Hours.ToString() + ":" + tsCompLabSunday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabMonday.Text = tsCompLabMonday.Hours.ToString() + ":" + tsCompLabMonday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabTuesday.Text = tsCompLabTuesday.Hours.ToString() + ":" + tsCompLabTuesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabWednesday.Text = tsCompLabWednesday.Hours.ToString() + ":" + tsCompLabWednesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabThursday.Text = tsCompLabThursday.Hours.ToString() + ":" + tsCompLabThursday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabFriday.Text = tsCompLabFriday.Hours.ToString() + ":" + tsCompLabFriday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabSaturday.Text = tsCompLabSaturday.Hours.ToString() + ":" + tsCompLabSaturday.Minutes.ToString().PadLeft(2, '0');
                lblTimeCompLabTotal.Text = tsCompLabTotal.Hours.ToString() + ":" + tsCompLabTotal.Minutes.ToString().PadLeft(2, '0');

                lblTimeTutoringSunday.Text = tsTutoringSunday.Hours.ToString() + ":" + tsTutoringSunday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringMonday.Text = tsTutoringMonday.Hours.ToString() + ":" + tsTutoringMonday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringTuesday.Text = tsTutoringTuesday.Hours.ToString() + ":" + tsTutoringTuesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringWednesday.Text = tsTutoringWednesday.Hours.ToString() + ":" + tsTutoringWednesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringThursday.Text = tsTutoringThursday.Hours.ToString() + ":" + tsTutoringThursday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringFriday.Text = tsTutoringFriday.Hours.ToString() + ":" + tsTutoringFriday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringSaturday.Text = tsTutoringSaturday.Hours.ToString() + ":" + tsTutoringSaturday.Minutes.ToString().PadLeft(2, '0');
                lblTimeTutoringTotal.Text = tsTutoringTotal.Hours.ToString() + ":" + tsTutoringTotal.Minutes.ToString().PadLeft(2, '0');

                lblTimeStudyHallSunday.Text = tsStudayHallSunday.Hours.ToString() + ":" + tsStudayHallSunday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallMonday.Text = tsStudyHallMonday.Hours.ToString() + ":" + tsStudyHallMonday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallTuesday.Text = tsStudyHallTuesday.Hours.ToString() + ":" + tsStudyHallTuesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallWednesday.Text = tsStudyHallWednesday.Hours.ToString() + ":" + tsStudyHallWednesday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallThursday.Text = tsStudyHallThursday.Hours.ToString() + ":" + tsStudyHallThursday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallFriday.Text = tsStudyHallFriday.Hours.ToString() + ":" + tsStudyHallFriday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallSaturday.Text = tsStudyHallSaturday.Hours.ToString() + ":" + tsTutoringSaturday.Minutes.ToString().PadLeft(2, '0');
                lblTimeStudyHallTotal.Text = tsStudyHallTotal.Hours.ToString() + ":" + tsStudyHallTotal.Minutes.ToString().PadLeft(2, '0');

                lblTimeTotalSunday.Text = tsSundayTotal.Hours.ToString() + ":" + tsSundayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalMonday.Text = tsMondayTotal.Hours.ToString() + ":" + tsMondayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalTuesday.Text = tsTuesdayTotal.Hours.ToString() + ":" + tsTuesdayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalWednesday.Text = tsWednesdayTotal.Hours.ToString() + ":" + tsWednesdayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalThursday.Text = tsThursdayTotal.Hours.ToString() + ":" + tsThursdayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalFriday.Text = tsFridayTotal.Hours.ToString() + ":" + tsFridayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeTotalSaturday.Text = tsSaturdayTotal.Hours.ToString() + ":" + tsSaturdayTotal.Minutes.ToString().PadLeft(2, '0');
                lblTimeOverallTotal.Text = tsOverallTotal.Hours.ToString() + ":" + tsOverallTotal.Minutes.ToString().PadLeft(2, '0');

                lblTimeTrackertitle.Text = "Accumulated Time (HH:MM)";
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ResourceID == Guid.Empty)
            {
                Visible = false;
            }

            if (!Page.IsPostBack)
            {
                BindAndShow();
            }
        }
    }
}