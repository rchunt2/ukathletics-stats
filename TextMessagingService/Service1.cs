﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using UK.STATS.STATSModel;

namespace TextMessagingService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        private Timer _timer;

        protected override void OnStart(string[] args)
        {
            Process();
        }

        public void Process()
        {
             //log.Info("Info - Service Started");
            _timer = new Timer(60 * 1000); // every minute
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerElapsed);
            _timer.Start();
            //for (int i=0; i < 500; i++)
            //{
            //    ProcessMessages();
            //    System.Threading.Thread.Sleep(6*10000);
            //}
        }

        protected override void OnStop()
        {
        }

        private void ProcessMessages()
        {
            #region SendMessages
            List<TextMessage> messages = UK.STATS.STATSModel.TextMessage.GetAllToSend();
            foreach (TextMessage message in messages)
            {
                Resource eResourceTo = Resource.Get(message.ResourceID);
                if (message.CourseSectionID.HasValue)
                {
                    //Get the time of the course section
                    CourseSection cs = CourseSection.Get(message.CourseSectionID.Value);
                    if (cs != null)
                    {
                        DateTime currentTime = DateTime.Now;
                        if (DayOfWeekMatch(currentTime.DayOfWeek, cs.CourseSectionDaysMeet))
                        {
                            //Calculate the time
                            TimeSpan startMinusInterval = cs.CourseSectionTimeStart.Subtract(TimeSpan.FromMinutes(10));

                            if (TimeSpan.Compare(currentTime.TimeOfDay, startMinusInterval) > 0 &&
                                TimeSpan.Compare(currentTime.TimeOfDay, cs.CourseSectionTimeStart) <= 0)
                            {

                                Carrier eCarrier = eResourceTo.Carrier;
                                String TextMessageEmail = eResourceTo.ResourcePhoneNumber + eCarrier.CarrierEmailSuffix;
                                if (eCarrier.CarrierEmailSuffix.Length > 0)
                                {
                                    SendEmail("michael.haley@uky.edu", TextMessageEmail, "Course Reminder", cs.CourseDisplayName + " starts in 10 minutes");
                                    message.ResetSentFlag(true);
                                }

                            }
                        }
                    }
                }
                else if (message.SessionID.HasValue)
                {
                    Session session = Session.Get(message.SessionID.Value);
                    if (session != null)
                    {
                        DateTime currentTime = DateTime.Now;
                        if (currentTime.Day == session.SessionDateTimeStart.Day && 
                            currentTime.Month == session.SessionDateTimeStart.Month &&
                            currentTime.Year == session.SessionDateTimeStart.Year)
                        {
                            //Calculate the time
                            TimeSpan startMinusInterval = session.SessionDateTimeStart.TimeOfDay.Subtract(TimeSpan.FromMinutes(10));

                            if (TimeSpan.Compare(DateTime.Now.TimeOfDay, startMinusInterval) > 0 &&
                                TimeSpan.Compare(DateTime.Now.TimeOfDay, session.SessionDateTimeStart.TimeOfDay) <= 0)
                            {
                                Carrier eCarrier = eResourceTo.Carrier;
                                String TextMessageEmail = eResourceTo.ResourcePhoneNumber + eCarrier.CarrierEmailSuffix;
                                if (eCarrier.CarrierEmailSuffix.Length > 0)
                                {
                                    SendEmail("michael.haley@uky.edu", TextMessageEmail, "Tutoring Session Reminder", "Your scheduled tutoring session starts in 10 minutes");
                                    message.ResetSentFlag(true);
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            #region ResetSentFlag

            List<TextMessage> resetMessages = UK.STATS.STATSModel.TextMessage.GetSentMessages();
            foreach (TextMessage message in resetMessages)
            {
                if (message.CourseSectionID.HasValue)
                {
                    //Get the time of the course section
                    CourseSection cs = CourseSection.Get(message.CourseSectionID.Value);
                    if (cs != null)
                    {
                        DateTime currentTime = DateTime.Now;
                        if (DayOfWeekMatch(currentTime.DayOfWeek, cs.CourseSectionDaysMeet))
                        {
                            //if the current time of day is greater than the start of the course, that means
                            //we can safely reset the flag to false so that it will send again prior to the course
                            //starting again instead of one minute from now

                            if(currentTime.TimeOfDay.CompareTo(cs.CourseSectionTimeStart) > 0) {
                                message.ResetSentFlag(false);
                            }
                        }
                    }
                }
            }

            #endregion
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            ProcessMessages();
        }

        private static void SendEmail(String From, String To, String Subject, String Body)
        {
            var oMail = new MailMessage(From, To, Subject, Body);
            oMail.CC.Add(new MailAddress(From));

            Setting eSettingHost = Setting.GetByKey("SMTPHost");
            Setting eSettingPort = Setting.GetByKey("SMTPPort");

            String SMTPHost = eSettingHost.SettingValue;
            int SMTPPort = Int32.Parse(eSettingPort.SettingValue);

            var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

            try
            {
                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }

        }

        private bool DayOfWeekMatch(DayOfWeek current, String daysMeet)
        {
            List<DayOfWeek> courseDaysMeet = new List<DayOfWeek>();
            foreach (char c in daysMeet)
            {
                if (c == 'M')
                    courseDaysMeet.Add(DayOfWeek.Monday);
                else if (c == 'T')
                    courseDaysMeet.Add(DayOfWeek.Tuesday);
                else if (c == 'W')
                    courseDaysMeet.Add(DayOfWeek.Wednesday);
                else if (c == 'R')
                    courseDaysMeet.Add(DayOfWeek.Thursday);
                else if (c == 'F')
                    courseDaysMeet.Add(DayOfWeek.Friday);
                else if (c == 'S')
                    courseDaysMeet.Add(DayOfWeek.Saturday);
            }

            if (courseDaysMeet.Contains(current))
                return true;
            else
                return false;
        }
    }
}
