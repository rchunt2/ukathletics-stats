﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Mobile
{
    public partial class ResourceSearch : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            STATSModel.Resource[] eResources = STATSModel.Resource.Search(txtSearchQuery.Text);
            gvResults.DataSource = eResources;
            gvResults.DataBind();
            panelResults.Visible = true;
            lblNumResults.Text = eResources.Length.ToString();
        }

        protected void gvResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewTime")
            {
                pnlCourseList.Visible = false;
                panelResults.Visible = false;
                pnlSchedule.Visible = false;
                pnlTimeReview.Visible = true;
                pnlPicture.Visible = false;

                int iRow = Int32.Parse(e.CommandArgument.ToString());
                Label lblresourceID = (Label)gvResults.Rows[iRow].FindControl("lblResourceID");
                Guid ResourceID = Guid.Parse(lblresourceID.Text);

                TimeTrackerModule.ResourceID = ResourceID;
                TimeTrackerModule.IsVisible = true; // Note: incorrectly had "TimeTrackerModule.Visible" here.  Wrong again, Josh.  :(
                TimeTrackerModule.BindAndShow();
            }

            if (e.CommandName == "ViewCourseList")
            {
                pnlCourseList.Visible = true;
                panelResults.Visible = false;
                pnlSchedule.Visible = false;
                pnlTimeReview.Visible = false;
                pnlPicture.Visible = false;
                
                int iRow = Int32.Parse(e.CommandArgument.ToString());
                Label lblresourceID = (Label)gvResults.Rows[iRow].FindControl("lblResourceID");
                Guid ResourceID = Guid.Parse(lblresourceID.Text);

                ResourceRegistrationSummaryControl.ResourceObject = Resource.Get(ResourceID);
                ResourceRegistrationSummaryControl.BindAndShow();
            }

            if (e.CommandName == "ViewSchedule")
            {
                pnlCourseList.Visible = false;
                panelResults.Visible = false;
                pnlSchedule.Visible = true;
                pnlTimeReview.Visible = false;
                pnlPicture.Visible = false;

                int iRow = Int32.Parse(e.CommandArgument.ToString());
                Label lblresourceID = (Label)gvResults.Rows[iRow].FindControl("lblResourceID");
                Guid ResourceID = Guid.Parse(lblresourceID.Text);

                ResourceScheduleModule.ResourceID = ResourceID;
                ResourceScheduleModule.Admin = false;
                ResourceScheduleModule.Printing = false;
                ResourceScheduleModule.RunScheduler();
            }

            if(e.CommandName == "ViewPicture")
            {
                pnlCourseList.Visible = false;
                panelResults.Visible = false;
                pnlSchedule.Visible = false;
                pnlTimeReview.Visible = false;
                pnlPicture.Visible = true;

                int iRow = Int32.Parse(e.CommandArgument.ToString());
                Label lblresourceID = (Label)gvResults.Rows[iRow].FindControl("lblResourceID");
                Guid ResourceID = Guid.Parse(lblresourceID.Text);

                var resource = Resource.Get(ResourceID);
                if(resource != null)
                {
                    var resourcePic = resource.ResourcePicture.ResourcePictureBytes;
                    if(resourcePic != null)
                        ResourceImage.ImageUrl = "data:image/png;base64, " + Convert.ToBase64String(resourcePic);
                }
            }
        }

        protected void btnTimeBack_Click(object sender, EventArgs e)
        {
            pnlCourseList.Visible = false;
            panelResults.Visible = true;
            pnlSchedule.Visible = false;
            pnlTimeReview.Visible = false;
            pnlPicture.Visible = false;
        }

        protected void btnScheduleBack_Click(object sender, EventArgs e)
        {
            pnlCourseList.Visible = false;
            panelResults.Visible = true;
            pnlSchedule.Visible = false;
            pnlTimeReview.Visible = false;
            pnlPicture.Visible = false;
        }

        protected void btnCourseListBack_Click(object sender, EventArgs e)
        {
            pnlCourseList.Visible = false;
            panelResults.Visible = true;
            pnlSchedule.Visible = false;
            pnlTimeReview.Visible = false;
            pnlPicture.Visible = false;
        }

        protected void btnPictureBack_Click(object sender, EventArgs e)
        {
            pnlCourseList.Visible = false;
            panelResults.Visible = true;
            pnlSchedule.Visible = false;
            pnlTimeReview.Visible = false;
            pnlPicture.Visible = false;
        }

        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var eResource = (STATSModel.Resource)e.Row.DataItem;
                ((Label)e.Row.FindControl("lblResourceID")).Text = eResource.ResourceID.ToString();

                if (e.Row.FindControl("lbtnCourseList") != null)
                {
                    ((LinkButton)e.Row.FindControl("lbtnCourseList")).CommandArgument = e.Row.RowIndex.ToString();
                }
                if (e.Row.FindControl("lbtnViewTime") != null)
                {
                    ((LinkButton)e.Row.FindControl("lbtnViewTime")).CommandArgument = e.Row.RowIndex.ToString();
                }
                if (e.Row.FindControl("lbtnSchedule") != null)
                {
                    ((LinkButton)e.Row.FindControl("lbtnSchedule")).CommandArgument = e.Row.RowIndex.ToString();
                }
                if (e.Row.FindControl("lblPicture") != null)
                {
                    ((LinkButton)e.Row.FindControl("lblPicture")).CommandArgument = e.Row.RowIndex.ToString();
                }
                if (string.IsNullOrWhiteSpace(eResource.ResourceEmail) && e.Row.FindControl("lnkEmail") != null)
                {
                    //if no email address, hide the email link
                    e.Row.FindControl("lnkEmail").Visible = false;
                }
                if (string.IsNullOrWhiteSpace(eResource.ResourcePhoneNumberPlain) && e.Row.FindControl("lnkPhoneNumber") != null)
                {
                    //if no phone #, hide the phone # link
                    e.Row.FindControl("lnkPhoneNumber").Visible = false;
                }

            }
        }
    }
}