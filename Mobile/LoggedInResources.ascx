﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoggedInResources.ascx.cs" Inherits="UK.STATS.Mobile.LoggedInResources" %>

Active Users: <asp:Label ID="lblNumResources" runat="server" Text="0"></asp:Label>
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    CellPadding="4" DataKeyNames="AttendanceID" DataSourceID="edsAttendance" 
    ForeColor="#333333" GridLines="None" Width="100%" AllowSorting="True">
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:TemplateField HeaderText="Area" 
            SortExpression="Schedule.Session.SessionType.SessionTypeName">
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" 
                    Text='<%# Bind("Schedule.Session.SessionType.SessionTypeName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Resource Type" 
            SortExpression="it.Schedule.Resource.ResourceType.ResourceTypeName">
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" 
                    Text='<%# Bind("Schedule.Resource.ResourceType.ResourceTypeName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Name, Last" 
            SortExpression="it.Schedule.Resource.ResourceNameLast">
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" 
                    Text='<%# Bind("Schedule.Resource.ResourceNameLast") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Name, First" 
            SortExpression="it.Schedule.Resource.ResourceNameFirst">
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" 
                    Text='<%# Bind("Schedule.Resource.ResourceNameFirst") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EditRowStyle BackColor="#2461BF" />
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#EFF3FB" />
    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#F5F7FB" />
    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
    <SortedDescendingCellStyle BackColor="#E9EBEF" />
    <SortedDescendingHeaderStyle BackColor="#4870BE" />
</asp:GridView>
<asp:EntityDataSource ID="edsAttendance" runat="server" 
    ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
    EnableFlattening="False" EntitySetName="Attendances"
    Include="Schedule.Resource, Schedule.Session.SessionType, Schedule.Resource.ResourceType"
    OrderBy="it.Schedule.Session.SessionType.SessionTypeName, it.Schedule.Resource.ResourceType.ResourceTypeName, it.Schedule.Resource.ResourceNameLast, it.Schedule.Resource.ResourceNameFirst"
    Where="it.AttendanceTimeOut IS NULL" >
</asp:EntityDataSource>



