﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="UK.STATS.Mobile.Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test</title>

    <style type="text/css">
    
        /* -- Reset default style -- */
        body, h1, p{border:0; margin:0; padding:0;}
        body{font-family:Arial, Helvetica, sans-serif; font-size:12px;}

        /* ------------ */
        /* HEADER */
        #header{
        padding:6px;
        background:#444444;
        }
        /* PAGE BODY */
        #page-body{padding:10px;}
        h1{font-size:14px; font-weight:bold;}
        h1 a:link, a:visited{color:#0033CC;}
        .tag{font-size:12px; margin-bottom:20px;}
        .tag a:link, .tag a:visited{color:#999999;}

        /* FOOTER */
        #footer{
        padding:6px;
        border-top:solid 1px #DEDEDE;
        color:#999999;
        font-size:11px;
        }
        #footer a:link, #footer a:visited{
        color:#666666;
        }
    
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <!-- ------------ -->
        <!-- Page Header -->
        <div id="header">
        <a href="index.html"><img src="logo.png" border="0" /></a>
        </div>

        <!-- ------------ -->
        <!-- Page Body -->
        <div id="page-body">
        <!-- Your post here -->
        <h1><a href="post1.html">Title of your post</a></h1>
        <p>A summary of your post</p>
        <p class="tag">
        <a href="tag1">tag1</a>,
        <a href="tag2">tag2</a>,
        <a href="tag3">tag3</a>
        </p>
        <!-- Your post here -->
        <!-- Your post here -->
        <!-- ... -->
        </div>

        <!-- ------------ -->
        <!-- Page Footer -->
        <div id="footer">
        <a href="index.html">Home</a> |
        <a href="mailto:youremail@yoursite.com">Contact me</a>
        </div>



    </form>
</body>
</html>
