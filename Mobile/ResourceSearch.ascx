﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSearch.ascx.cs" Inherits="UK.STATS.Mobile.ResourceSearch" %>

<%@ Register TagPrefix="SIS" TagName="TimeTracker" Src="~/TimeTracker.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceRegistrationSummary" Src="~/ResourceRegistrationSummary.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSchedule" Src="~/ResourceSchedule.ascx" %>


<div class="ResourceSearchArea">
    Search: <asp:TextBox ID="txtSearchQuery" runat="server"></asp:TextBox> <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
</div>

<asp:Panel ID="panelResults" Visible="false" runat="server">
    <div>
        Results Found: <asp:Label ID="lblNumResults" runat="server" Text="0"></asp:Label>
        <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="False" ShowHeader="False" 
            DataKeyNames="ResourceID" CellPadding="4" ForeColor="#333333" 
            GridLines="None" onrowcommand="gvResults_RowCommand" Width="100%" 
            onrowdatabound="gvResults_RowDataBound">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="ResourceID" SortExpression="ResourceID"  Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblResourceID" runat="server" Text='<%# Bind("ResourceID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" SortExpression="ResourceNameDisplayLastFirst">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("ResourceNameDisplayLastFirst") %>'></asp:Label>
                        <asp:HyperLink runat="server" ID="lnkPhoneNumber" NavigateUrl='<%# Eval("ResourceEmailLink") %>'><br /><%# Eval("ResourceEmail") %></asp:HyperLink>
                        <asp:HyperLink runat="server" ID="lnkEmail" NavigateUrl='<%# Eval("ResourcePhoneNumberLink") %>'><br /><%# Eval("ResourcePhoneNumberFormatted") %></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:LinkButton style="white-space:nowrap" ID="lbtnViewTime" runat="server" CommandName="ViewTime" Text="Time Tracker" />
                        <asp:LinkButton style="white-space:nowrap" ID="lbtnCourseList" runat="server" CommandName="ViewCourseList" Text="Course List" />
                        <asp:LinkButton style="white-space:nowrap" ID="lbtnSchedule" runat="server" CommandName="ViewSchedule" Text="Schedule" />
                        <asp:LinkButton style="white-space:nowrap" ID="lblPicture" runat="server" CommandName="ViewPicture" Text="Picture" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableFlattening="False" EntitySetName="Resources">
        </asp:EntityDataSource>
    </div>
</asp:Panel>

<asp:Panel ID="pnlTimeReview" runat="server" Visible="false">
    <asp:Button ID="btnTimeBack" runat="server" Text="Back" ForeColor="Blue" onclick="btnTimeBack_Click" />
    <br />
    <SIS:TimeTracker ID="TimeTrackerModule" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlCourseList" runat="server" Visible="false">
    <asp:Button ID="btnCourseListBack" runat="server" Text="Back" ForeColor="Blue" onclick="btnCourseListBack_Click" />
    <br />
    <SIS:ResourceRegistrationSummary ID="ResourceRegistrationSummaryControl" runat="server" />
</asp:Panel>


<asp:Panel ID="pnlSchedule" runat="server" Visible="false">
    <asp:Button ID="btnScheduleBack" runat="server" Text="Back" ForeColor="Blue" onclick="btnScheduleBack_Click" />
    <br />
    <SIS:ResourceSchedule ID="ResourceScheduleModule" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlPicture" runat="server" Visible="false">
    <asp:Button ID="btnPictureBack" runat="server" Text="Back" ForeColor="Blue" onclick="btnPictureBack_Click" />
    <br />
    <asp:Image ID="ResourceImage" runat="server" />
</asp:Panel>


