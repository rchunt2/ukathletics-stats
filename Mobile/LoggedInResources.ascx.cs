﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Mobile
{
    public partial class LoggedInResources : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var context = new STATSModel.STATSEntities();
            var numActive = (from STATSModel.Attendance obj in context.Attendances where obj.AttendanceTimeOut == null select obj).Count();
            lblNumResources.Text = numActive.ToString();
        }
    }
}