﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Mobile
{
    public partial class Default : System.Web.UI.Page
    {
        private Guid? ResourceID
        {
            get
            {
                if (Session["ResourceID"] == null)
                {
                    return null;
                }

                var RID = (Guid) Session["ResourceID"];
                return RID;
            }
        }
        private Resource eResource
        {
            get
            {
                if (ResourceID.HasValue)
                {
                    return STATSModel.Resource.Get(ResourceID.Value);
                }
                else
                {
                    return null;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String ApplicationName = STATSModel.Setting.GetByKey("ApplicationNameShort").SettingValue;
            lblTitle.Text = ApplicationName;

            Setup();
        }


        private void Setup()
        {
            if (eResource == null)
            {
                panelLogin.Visible = true;
                panelLogout.Visible = false;
                panelStudentTutor.Visible = false;
                panelSuperUser.Visible = false;
            }
            else
            {
                panelLogin.Visible = false;
                panelLogout.Visible = true;
                lblWelcome.Text = eResource.ResourceNameFirst;
                var eResourceType = eResource.ResourceType;
                String TypeName = eResourceType.ResourceTypeName;
                if (TypeName == "Student" || TypeName == "Tutor")
                {
                    panelStudentTutor.Visible = true;
                    panelSuperUser.Visible = false;
                }

                if (TypeName == "Administrator" || TypeName == "Monitor" || TypeName == "Director" || TypeName == "Time Mgr")
                {
                    panelStudentTutor.Visible = false;
                    panelSuperUser.Visible = true;
                }
            }
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("./Default.aspx", true);
        }

        protected void login_Authenticate(object sender, AuthenticateEventArgs e)
        {
            String resourceLogin = login.UserName;
            String resourcePass = login.Password;


            bool adLoginAssociatedWithResource = STATSModel.Resource.IsADLoginAssociatedWithResource(resourceLogin);
            bool validLegacyLogin = STATSModel.Resource.IsValidLegacyLogin(resourceLogin, resourcePass);
            bool validADLogin = false;
            bool legacyLoginHasADLogin = false;
            if (!validLegacyLogin)
            {
                //only Check for valid AD login if it's not a Legacy login
                validADLogin = STATSModel.Resource.IsValidADLogin(resourceLogin, resourcePass);
            }
            else
            {
                legacyLoginHasADLogin = STATSModel.Resource.DoesResourceHaveADLoginAssociated(resourceLogin);
            }

            if (!validADLogin && !validLegacyLogin)
            {
                //Login is not a valid AD login and not a valid legacy login
                login.FailureText = "Invalid username or password";
                return;
            }

            if (validLegacyLogin)
            {
                if (legacyLoginHasADLogin)
                {
                    //Legacy login, but already has an AD account associated...
                    login.FailureText = "You have tried logging in using a legacy user/pass, you must use your Active Directory login instead.";
                    login.UserName = "";
                    ((TextBox)login.FindControl("UserName")).Text = "";
                }
                else
                {
                    if (Session["NewADLoginID"] != null)
                    {
                        //associate the AD login with the resource using the old legacy login to map it
                        bool success = STATSModel.Resource.AssociateADLogin(resourceLogin, Session["NewADLoginID"] as string);
                        if (success)
                        {
                            adLoginAssociatedWithResource = true;
                            resourceLogin = Session["NewADLoginID"] as string;
                        }
                        else
                        {
                            login.FailureText = "Could not associate AD login: " + (Session["NewADLoginID"] as string) + " for legacy user: " + resourceLogin;
                            login.UserName = "";
                            ((TextBox)login.FindControl("UserName")).Text = "";
                        }
                    }
                    else
                    {
                        //Legacy login, ask for new AD account to associate with
                        login.FailureText = "You have tried logging in using a legacy user/pass, please now enter your Active Directory login.";
                        login.UserName = "";
                        ((TextBox)login.FindControl("UserName")).Text = "";
                        Session.Add("LegacyLoginID", resourceLogin);
                    }
                }
            }

            if (validADLogin && !adLoginAssociatedWithResource)
            {
                if (Session["LegacyLoginID"] != null)
                {
                    //associate the AD login with the resource using the old legacy login to map it
                    bool success = STATSModel.Resource.AssociateADLogin(Session["LegacyLoginID"] as string, resourceLogin);
                    if (success)
                    {
                        adLoginAssociatedWithResource = true;
                    }
                    else
                    {
                        login.FailureText = "Could not associate AD login: " + resourceLogin + " for legacy user: " + (Session["LegacyLoginID"] as string);
                    }
                }
                else
                {
                    login.FailureText = "You have entered a valid Active Directory login, but it is not associated with a Resource. Login using your old (legacy) user/pass.";
                    login.UserName = "";
                    ((TextBox)login.FindControl("UserName")).Text = "";
                    Session.Add("NewADLoginID", resourceLogin);
                }
            }

            if (adLoginAssociatedWithResource && validADLogin)
            {
                var Resource = STATSModel.Resource.GetByADLogin(resourceLogin);

                Session.Clear();
                Session.Add("ResourceID", Resource.ResourceID);
                Setup();
            }
        }
    }
}