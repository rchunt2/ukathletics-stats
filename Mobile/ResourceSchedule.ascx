﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSchedule.ascx.cs"
    Inherits="UK.STATS.Mobile.ResourceSchedule" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">

</style>
<asp:Panel ID="panelScreen" runat="server">
    <div style="float: left;">
        <asp:Label ID="lblSource" runat="server" Text="Resource Name"></asp:Label>
    </div>

    <asp:Panel ID="pnlPrintLink" runat="server">
        <div class="noPrint" style="float: right;">
            <a href="#" onclick="javascript: print(); return false;">Print</a>
        </div>
    </asp:Panel>
    <div style="clear: both;"></div>
</asp:Panel>
<asp:Label runat="server" ID="lblAppointmentInfo" Visible="false" />
<asp:Button runat="server" ID="btnCloseAppointmentInfo" Visible="false" Text="Close" OnClick="btnCloseAppointmentInfo_Click" />
<asp:UpdatePanel ID="upScheduler" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div id="ResourceScheduler">
            <telerik:radscheduler 
                id="ResourceRadScheduler" 
                runat="server" 
                height="100%" 
                datakeyfield="ID"
                datasubjectfield="Subject" 
                dataendfield="End" 
                datastartfield="Start" 
                dayendtime="23:00:00" 
                enabledatepicker="True" 
                enabledescriptionfield="True"
                overflowbehavior="Expand" 
                selectedview="WeekView" 
                showviewtabs="False"
                FirstDayOfWeek="Monday" 
                LastDayOfWeek="Sunday" 
                ShowAllDayRow="False" 
                Width="100%" 
                WorkDayEndTime="23:59:00" 
                WorkDayStartTime="00:00:00" 
                onappointmentcreated="ResourceRadScheduler_AppointmentCreated1" 
                onappointmentinsert="ResourceRadScheduler_AppointmentInsert" 
                AllowDelete="False" 
                onappointmentupdate="ResourceRadScheduler_AppointmentUpdate" 
                ShowFooter="False" 
                onnavigationcomplete="ResourceRadScheduler_NavigationComplete" 
                AppointmentStyleMode="Default" AdvancedForm-Enabled="True" 
                onappointmentdelete="ResourceRadScheduler_AppointmentDelete" 
                ShowHeader="False" ShowNavigationPane="False" 
                ShowResourceHeaders="False" RowHeight="30px"
                OnAppointmentClick="ResourceRadScheduler_AppointmentClick">
                    <WeekView WorkDayEndTime="23:59:00" ColumnHeaderDateFormat="ddd" />
            </telerik:radscheduler><%--skin="Windows7" --%>
            <asp:Label ID="lblDebug" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

    
    