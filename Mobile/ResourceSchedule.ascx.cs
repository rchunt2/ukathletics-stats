﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;
using Resource = UK.STATS.STATSModel.Resource;

namespace UK.STATS.Mobile
{
    public partial class ResourceSchedule : System.Web.UI.UserControl
    {
        private System.Drawing.Color ColorClassBorder;
        private System.Drawing.Color ColorTutoringCATSBorder;
        private System.Drawing.Color ColorTutoringFootballBorder;
        private System.Drawing.Color ColorCompLabBorder;
        private System.Drawing.Color ColorFreeFormBorder;
        private System.Drawing.Color ColorStudyHallCATSBorder;
        private System.Drawing.Color ColorStudyHallFootballBorder;
        private System.Drawing.Color ColorDefaultBorder;

        private System.Drawing.Color ColorClass;
        private System.Drawing.Color ColorTutoringCATS;
        private System.Drawing.Color ColorTutoringFootball;
        private System.Drawing.Color ColorCompLab;
        private System.Drawing.Color ColorFreeForm;
        private System.Drawing.Color ColorStudyHallCATS;
        private System.Drawing.Color ColorStudyHallFootball;
        private System.Drawing.Color ColorDefault;

        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            LoadColors();
            base.OnPreRender(e);
        }

        public void LoadColors()
        {
            // Get the configuration settings for the colors of the individual calendar items.
            String StrColorClass = Setting.GetByKey("ScheduleColorClass").SettingValue;
            String StrColorTutoringCATS = Setting.GetByKey("ScheduleColorTutoringCATS").SettingValue;
            String StrColorTutoringFootball = Setting.GetByKey("ScheduleColorTutoringFootball").SettingValue;
            String StrColorCompLab = Setting.GetByKey("ScheduleColorCompLab").SettingValue;
            String StrColorFreeForm = Setting.GetByKey("ScheduleColorFreeForm").SettingValue;
            String StrColorStudyHallCATS = Setting.GetByKey("ScheduleColorStudyHallCATS").SettingValue;
            String StrColorStudyHallFootball = Setting.GetByKey("ScheduleColorStudyHallFootball").SettingValue;
            String StrColorDefault = Setting.GetByKey("ScheduleColorDefault").SettingValue;

            ColorClassBorder = System.Drawing.Color.FromName(StrColorClass);
            ColorTutoringCATSBorder = System.Drawing.Color.FromName(StrColorTutoringCATS);
            ColorTutoringFootballBorder = System.Drawing.Color.FromName(StrColorTutoringFootball);
            ColorCompLabBorder = System.Drawing.Color.FromName(StrColorCompLab);
            ColorFreeFormBorder = System.Drawing.Color.FromName(StrColorFreeForm);
            ColorStudyHallCATSBorder = System.Drawing.Color.FromName(StrColorStudyHallCATS);
            ColorStudyHallFootballBorder = System.Drawing.Color.FromName(StrColorStudyHallFootball);
            ColorDefaultBorder = System.Drawing.Color.FromName(StrColorDefault);

            // Set the fill color of the calendar items.
            ColorClass = System.Drawing.Color.FromName("Light" + StrColorClass);
            ColorTutoringCATS = System.Drawing.Color.FromName("Light" + StrColorTutoringCATS);
            ColorTutoringFootball = System.Drawing.Color.FromName("Light" + StrColorTutoringFootball);
            ColorCompLab = System.Drawing.Color.FromName("Light" + StrColorCompLab);
            ColorFreeForm = System.Drawing.Color.FromName("Light" + StrColorFreeForm);
            ColorStudyHallCATS = System.Drawing.Color.FromName("Light" + StrColorStudyHallCATS);
            ColorStudyHallFootball = System.Drawing.Color.FromName("Light" + StrColorStudyHallFootball);
            ColorDefault = System.Drawing.Color.FromName("Light" + StrColorDefault);
        }

        public Guid ResourceID { get; set; }
        public Guid GroupID { get; set; }
        public Guid ComparisonResourceID { get; set; }
        public Boolean RenderComparison { get; set; }
        public Boolean Printing { get; set; }
        public Boolean Admin { get; set; }
        private List<Guid> _resourceIDs { get; set; }

        private List<Resource> _resources
        {
            get 
            { 
                var eResources = new List<Resource>();
                foreach (Guid rid in _resourceIDs)
                {
                    Resource eResource = Resource.Get(rid);
                    eResources.Add(eResource);
                }
                return eResources;
            }

            set 
            { 
                var resourceIDList = new List<Guid>();
                List<Resource> eResources = value;
                foreach (var eResource in eResources)
                {
                    resourceIDList.Add(eResource.ResourceID);
                }
                _resourceIDs = resourceIDList;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (this.Admin)
            {
                panelScreen.Visible = false;
            }
            else
            {
                panelScreen.Visible = true;
                if (this.Printing)
                {
                    pnlPrintLink.Visible = true;
                }
                else
                {
                    pnlPrintLink.Visible = false;
                }
            }
            ResourceRadScheduler.ReadOnly = (!this.Admin);
        }

        public void RunScheduler()
        {
            //if(!Page.IsPostBack)
            //{
                String strSemesterDateBegin = Setting.GetByKey("SemesterDateBegin").SettingValue;
                String strSemesterDateEnd = Setting.GetByKey("SemesterDateEnd").SettingValue;
                DateTime SemesterDateBegin = DateTime.Parse(strSemesterDateBegin);
                DateTime SemesterDateBeginFollowingWeek = SemesterDateBegin.AddDays(7);
                DateTime SemesterDateEnd = DateTime.Parse(strSemesterDateEnd);
                if (DateTime.Today > SemesterDateBeginFollowingWeek) { SemesterDateBeginFollowingWeek = DateTime.Today; }

                // If we are within two weeks of the end of the semester we need to set the calendar to look at the current week 10-5-12 ASW
                var currentCultureCalendar = System.Threading.Thread.CurrentThread.CurrentCulture;
                int currentWeekNumber = currentCultureCalendar.Calendar.GetWeekOfYear(DateTime.Today, currentCultureCalendar.DateTimeFormat.CalendarWeekRule, currentCultureCalendar.DateTimeFormat.FirstDayOfWeek);
                int endSemesterWeekNumber = currentCultureCalendar.Calendar.GetWeekOfYear(SemesterDateEnd, currentCultureCalendar.DateTimeFormat.CalendarWeekRule, currentCultureCalendar.DateTimeFormat.FirstDayOfWeek);
                if ((endSemesterWeekNumber - currentWeekNumber) <= 2)
                    while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(-1); }
                else
                    while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(1); }
                //while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.Subtract(new TimeSpan(1,0,0,0)); }

                // Walk backwards from today until we're on a Monday
                //while (SemesterDateBeginFollowingWeek.DayOfWeek != DayOfWeek.Monday) { SemesterDateBeginFollowingWeek = SemesterDateBeginFollowingWeek.AddDays(1); }
                ResourceRadScheduler.SelectedDate = SemesterDateBeginFollowingWeek;
            //}

            ResourceRadScheduler.DataKeyField = "ID";
            ResourceRadScheduler.DataSubjectField = "Subject";
            ResourceRadScheduler.DataStartField = "Start";
            ResourceRadScheduler.DataEndField = "End";

            var ResourceIDList = new List<Guid>();
            var eResources = new List<Resource>();

            if (GroupID != Guid.Empty)
            {
                Group eGroup = Group.Get(GroupID);
                eResources.AddRange(eGroup.GetMembers());
                lblSource.Text = "Group: " + eGroup.GroupName;
            }
            else
            {
                var eResource = Resource.Get(ResourceID);
                lblSource.Text = eResource.ResourceNameDisplayLastFirst;
                eResources.Add(eResource);
                if (RenderComparison)
                {
                    var eResourceComparison = Resource.Get(ComparisonResourceID);
                    lblSource.Text += " & " + eResourceComparison.ResourceNameDisplayLastFirst;
                    eResources.Add(eResourceComparison);
                }
            }

            _resources = eResources;
            ResourceRadScheduler.DataSource = BindScheduler(eResources.ToArray());
            ResourceRadScheduler.DataBind();
        }

        public List<Appointment> BindScheduler(IEnumerable<Resource> eResources)
        {
            SessionType eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
            SessionType eSessionTypeTutoringFootball = SessionType.GetByName("Tutoring - Football");
            SessionType eSessionTypeClass = SessionType.GetByName("Class");
            SessionType eSessionTypeNote = SessionType.GetByName("Note");
            var AppointmentList = new List<Appointment>();

            // Note: The scheduler only displays things that start AFTER this range value.
            // Note: If it starts at 8AM, and this value is 8AM, it WILL NOT DISPLAY.
            // Note: The same goes for the RangeEnd value, just in reverse.
            DateTime RangeStart = ResourceRadScheduler.VisibleRangeStart.AddMinutes(-1);
            DateTime RangeEnd = ResourceRadScheduler.VisibleRangeEnd.AddMinutes(1);

            foreach (var eResource in eResources)
            {
                // Only display the name if this schedule is a composite of multiple resources.
                String strResourceName = "";
                if (GroupID != Guid.Empty || RenderComparison)
                {
                    strResourceName = eResource.ResourceNameFirst + " " + eResource.ResourceNameLast;
                }

                // Class Sessions
                // Note: Class Sessions used to have Session and Schedule records associated with them.
                // Note: These are now dynamically derived when requested.
                CourseSection[] RegisteredCourses = ResourceRegistration.GetRegisteredCourses(eResource);
                foreach (var RegisteredCourse in RegisteredCourses)
                {
                    Session[] Sessions = ResourceRegistration.GetCourseSectionSessions(RegisteredCourse);
                    foreach (var ClassSession in Sessions)
                    {
                        Boolean AfterStart = (ClassSession.SessionDateTimeStart >= RangeStart);
                        Boolean BeforeEnd = (ClassSession.SessionDateTimeEnd <= RangeEnd);
                        if (AfterStart && BeforeEnd) // Check if the session falls within the expected time range.
                        {
                            var eCourse = RegisteredCourse.Course;
                            var eSubject = eCourse.Subject;

                            String strSubject = eSubject.SubjectNameShort;
                            String strCourse = eCourse.CourseNumber;
                            String strSection = "-" + RegisteredCourse.CourseSectionNumber;
                            String strBuilding = RegisteredCourse.CourseSectionBuilding;
                            String strRoom = RegisteredCourse.CourseSectionRoom;
                            String strSessionTime = ClassSession.SessionDateTimeStart.ToShortTimeString() + "-" + ClassSession.SessionDateTimeEnd.ToShortTimeString();

                            // NOTE: SessionTypeName isn't displayed.  Why?  Because it's color coded, that's why.
                            String ApptText = strBuilding + " " + strRoom + " " + strSessionTime + "\r\n" + strResourceName + " " + " " + strSubject + strCourse + strSection ;

                            if (ClassSession.SessionIsScheduled)
                            {
                                ApptText = "SessionType=" + eSessionTypeClass.SessionTypeName + "$$$$" + ApptText;
                            }

                            var a = new Appointment(Guid.NewGuid(), ClassSession.SessionDateTimeStart, ClassSession.SessionDateTimeEnd, ApptText);
                            AppointmentList.Add(a);
                        }
                    }
                }

                Schedule[] eSchedules = Schedule.Get(eResource, RangeStart, RangeEnd);
                foreach (Schedule eSchedule in eSchedules)
                {
                    var eSession = eSchedule.Session;
                    var eSessionType = eSession.SessionType;

                    String strAnnotation = "";
                    String strSubject = "";
                    String strCourse= "";
                    String strSection = "";
                    String strBuilding = "";
                    String strRoom = "";
                    String strSessionTime = eSession.SessionDateTimeStart.ToShortTimeString() + "-" + eSession.SessionDateTimeEnd.ToShortTimeString();
                    String strTutorInfo = "";

                    if (eSession.SessionTypeID == eSessionTypeNote.SessionTypeID)
                    {
                        // If this session falls outside the range (expected for FreeForm events)
                        if (eSession.SessionDateTimeStart < RangeStart || eSession.SessionDateTimeStart > RangeEnd)
                        {
                            // We'll need to "pretend" that it falls within the current week.
                            DateTime OldDateStart = eSession.SessionDateTimeStart.Date;
                            DayOfWeek DayStart = OldDateStart.DayOfWeek;
                            TimeSpan OldTimeStart = eSession.SessionDateTimeStart.TimeOfDay;
                            TimeSpan OldTimeEnd = eSession.SessionDateTimeEnd.TimeOfDay;

                            DateTime dtIteratorStart = RangeStart;
                            while (dtIteratorStart.DayOfWeek != DayStart)
                            {
                                dtIteratorStart = dtIteratorStart.AddDays(1);
                            }

                            DateTime dtIteratorEnd = dtIteratorStart;
                            while (dtIteratorEnd.DayOfWeek != DayStart)
                            {
                                dtIteratorEnd = dtIteratorEnd.AddDays(1);
                            }

                            DateTime NewDateTimeStart = dtIteratorStart.Date.Add(OldTimeStart);
                            DateTime NewDateTimeEnd = dtIteratorEnd.Date.Add(OldTimeEnd);
                            eSession.SessionDateTimeStart = NewDateTimeStart;
                            eSession.SessionDateTimeEnd = NewDateTimeEnd;
                        }
                    }

                    // Free-Form Sessions
                    Annotation[] eAnnotations = Annotation.Get(eSchedule.ScheduleID);
                    if (eAnnotations.Length > 0)
                    {
                        foreach (Annotation eAnnotation in eAnnotations)
                        {
                            strAnnotation += eAnnotation.AnnotationText + " ";
                        }
                    }

                    // Tutoring Sessions
                    if (eSessionType.SessionTypeID == eSessionTypeTutoring.SessionTypeID || eSessionType.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID)
                    {
                        //strTutorInfo = "Tutoring: ";
                        STATSModel.Resource[] eResourcesTutoring = eSession.GetResourceInSession();
                        int NumberScheduled = eResourcesTutoring.Count();
                        if (NumberScheduled > 1) // There's more than a single tutor and a student, so it's a group session.
                        {
                            STATSModel.Resource oResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                            if (NumberScheduled > 2)
                            {
                                if (eResource.ResourceID == oResourceTutor.ResourceID)
                                {
                                    strTutorInfo += "Group Tutoring Session";
                                }
                                else
                                {
                                    strTutorInfo += oResourceTutor.ResourceNameFirst + " " + oResourceTutor.ResourceNameLast;
                                }
                            }
                            else
                            {
                                foreach (var eResourceInSession in eResourcesTutoring)
                                {
                                    if (eResourceInSession.ResourceID != eResource.ResourceID)
                                    {
                                        strTutorInfo += " " + eResourceInSession.ResourceNameFirst + " " + eResourceInSession.ResourceNameLast;
                                    }
                                }
                            }
                            if (eSession.HasAdditionalData())
                            {
                                STATSModel.SessionTutor eSessionTutor = eSession.GetAdditionalData();
                                if (eSessionTutor.CourseID.HasValue)
                                {
                                    Course eCourse = eSessionTutor.Course;
                                    Subject eSubject = eCourse.Subject;
                                    strSubject = eSubject.SubjectNameShort;
                                    strCourse = eCourse.CourseNumber;
                                }
                            }

                        }
                    }

                    // NOTE: SessionTypeName isn't displayed.  Why?  Because it's color coded, that's why.
                    String ApptText = strBuilding + " " + strRoom + " " + strSessionTime + "\r\n" + strResourceName + " " + strAnnotation + " " + strSubject + strCourse + strSection + "\r\n" + strTutorInfo;

                    if (!eSession.SessionIsScheduled)
                    {
                        ApptText = "SessionType=U" + eSessionType.SessionTypeName + "$$$$" + ApptText;
                    }
                    else
                    {
                        ApptText = "SessionType=" + eSessionType.SessionTypeName + "$$$$" + ApptText;
                    }

                    var a = new Appointment(eSchedule.ScheduleID, eSession.SessionDateTimeStart, eSession.SessionDateTimeEnd, ApptText);
                    AppointmentList.Add(a);
                }
            }
            return AppointmentList;
        }

        protected void ResourceRadScheduler_AppointmentCreated1(object sender, AppointmentCreatedEventArgs e)
        {
            try
            {
                string classType = e.Appointment.Subject.Substring(0, e.Appointment.Subject.IndexOf("$$$$"));
                classType = classType.Substring(classType.IndexOf("=") + 1);
                e.Appointment.Subject = e.Appointment.Subject.Replace(e.Appointment.Subject.Substring(0, e.Appointment.Subject.IndexOf("$$$$") + 4), "");

                var rsApt = (AppointmentControl)e.Appointment.AppointmentControls[0];
                var rsAptContent = (WebControl)rsApt.Controls[0].Controls[0].Controls[0].Controls[0];
                var rsAptOut = (WebControl)rsApt.Controls[0];
                var rsAptIn = (WebControl)rsApt.Controls[0].Controls[0].Controls[0];
                var rsAptMid = (WebControl)rsApt.Controls[0].Controls[0];

                // This was repeated multiple times in the switch statement below.  I think it goes better out here anyway.
                e.Appointment.BorderStyle = System.Web.UI.WebControls.BorderStyle.Solid;
                e.Appointment.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);

                // By Default, no schedule entries should be able to be edited.
                e.Appointment.AllowEdit = false;
                e.Appointment.AllowDelete = false;

                switch (classType.ToLower())
                {
                    case "tutoring - cats":
                        e.Appointment.BackColor = ColorTutoringCATS;
                        e.Appointment.BorderColor = ColorTutoringCATSBorder;
                        break;
                    case "tutoring - football":
                        e.Appointment.BackColor = ColorTutoringFootball;
                        e.Appointment.BorderColor = ColorTutoringFootballBorder;
                        break;
                    case "class":
                        e.Appointment.BackColor = ColorClass;
                        e.Appointment.BorderColor = ColorClassBorder;
                        break;
                    case "computer lab":
                        e.Appointment.BackColor = ColorCompLab;
                        e.Appointment.BorderColor = ColorCompLabBorder;
                        break;
                    case "note":
                        if (this.Admin) // Read only unless we're in administration mode!
                        {
                            // Schedule entries associated with a Session that has a SessionType of "Note" are editable.
                            e.Appointment.AllowEdit = true;
                            e.Appointment.AllowDelete = true;
                        }
                        e.Appointment.BackColor = ColorFreeForm;
                        e.Appointment.BorderColor = ColorFreeFormBorder;
                        break;
                    case "study hall - cats":
                        e.Appointment.BackColor = ColorStudyHallCATS;
                        e.Appointment.BorderColor = ColorStudyHallCATSBorder;
                        break;
                    case "study hall - football":
                        e.Appointment.BackColor = ColorStudyHallFootball;
                        e.Appointment.BorderColor = ColorStudyHallFootballBorder;
                        break;
                    default:
                        e.Appointment.BackColor = ColorDefault;
                        e.Appointment.BorderColor = ColorDefaultBorder;
                        break;
                }
            }
            catch
            {
                // do something
            }
        }

        protected void ResourceRadScheduler_AppointmentInsert(object sender, AppointmentInsertEventArgs e)
        {
            if (!this.Admin) // Read only unless we're in administration mode!
            {
                ScriptManager.RegisterClientScriptBlock(ResourceRadScheduler, ResourceRadScheduler.GetType(), "SchedulerError", "<script language='javascript'>alert('You do not have the proper permissions to add items to the calendar!');</script>", false);
                return;
            } 

            // TODO: Practices should occur on the calendar every week.

            STATSModel.Resource ResourceObj = STATSModel.Resource.Get(this.ResourceID);
            STATSModel.SessionType SessionTypeObj = STATSModel.SessionType.GetByName("Note");
            DateTime Start = e.Appointment.Start;
            DateTime End = e.Appointment.End;
            var SessionObj = new STATSModel.Session();
            SessionObj.SessionID = Guid.NewGuid();
            SessionObj.SessionTypeID = SessionTypeObj.SessionTypeID;
            SessionObj.SessionIsScheduled = true;
            SessionObj.SessionIsCancelled = false;
            SessionObj.SessionDateTimeStart = Start;
            SessionObj.SessionDateTimeEnd = End;

            // NOTE: Improper use of logic outside of the EntityModel!
            var context = new STATSEntities();
            context.Sessions.AddObject(SessionObj);

            foreach (Resource eResource in _resources)
            {
                var ScheduleObj = new STATSModel.Schedule();
                ScheduleObj.ScheduleID = Guid.NewGuid();
                ScheduleObj.ResourceID = eResource.ResourceID;
                ScheduleObj.SessionID = SessionObj.SessionID;
                STATSModel.Annotation AnnotationObj = STATSModel.Annotation.CreateAnnotation(Guid.NewGuid(), ScheduleObj.ScheduleID, e.Appointment.Subject);
                context.Schedules.AddObject(ScheduleObj);
                context.Annotations.AddObject(AnnotationObj);
            }

            context.SaveChanges();
            RunScheduler();
        }

        protected void ResourceRadScheduler_AppointmentUpdate(object sender, AppointmentUpdateEventArgs e)
        {
            if (!this.Admin) // Read only unless we're in administration mode!
            {
                ScriptManager.RegisterClientScriptBlock(ResourceRadScheduler, ResourceRadScheduler.GetType(), "SchedulerError", "<script language='javascript'>alert('You do not have the proper permissions to modify calendar items!');</script>", false);
                return;
            } 

            Appointment UpdatedAppt = e.ModifiedAppointment;
            Guid ScheduleID = (Guid)e.ModifiedAppointment.ID;

            // NOTE: Improper use of logic outside of the EntityModel!
            var context = new STATSEntities();
            //var eAnnotation = (from STATSModel.Annotation obj in context.Annotations where obj.SourceID == ScheduleID select obj).First();
            //eAnnotation.AnnotationText = e.ModifiedAppointment.Subject;
            var eSession = (from STATSModel.Schedule obj in context.Schedules.Include("Session") where obj.ScheduleID == ScheduleID select obj.Session).First();
            eSession.SessionDateTimeStart = UpdatedAppt.Start;
            eSession.SessionDateTimeEnd = UpdatedAppt.End;
            context.SaveChanges(); 
            RunScheduler();
        }

        protected void ResourceRadScheduler_NavigationComplete(object sender, SchedulerNavigationCompleteEventArgs e)
        {
            RunScheduler();
        }

        protected void ResourceRadScheduler_AppointmentDelete(object sender, AppointmentDeleteEventArgs e)
        {
            Object ID = e.Appointment.ID;
            String sID = ID.ToString();
            Guid gID = Guid.Parse(sID);
            Guid ScheduleID = gID;

            // NOTE: Improper use of logic outside of the EntityModel!
            var context = new STATSEntities();
            var eSchedule = (from STATSModel.Schedule obj in context.Schedules where obj.ScheduleID == ScheduleID select obj).Single();

            /*
                1. Get a ScheduleID
                2. Get it's associated SessionID
                3. If that Session record is of a specific SessionType (Required: "Note"), continue.  Else, this shouldn't be deleted anyway.
                3. Using that SessionID, get a list of the ScheduleID's that are tied to it.
                4. End Result: All the schedules for a particular group schedule item.
            */

            Session eSession = eSchedule.Session;
            SessionType eSessionType = eSession.SessionType;
            SessionType eSessionTypeNotes = SessionType.GetByName("Note");

            if(eSessionType.SessionTypeID == eSessionTypeNotes.SessionTypeID)
            {
                // If it's a note, delete all of them.
                Schedule[] eSchedules = eSession.Schedules.ToArray();
                foreach (var schedule in eSchedules)
                {
                    context.DeleteObject(schedule);
                }
            }
            context.SaveChanges();
            RunScheduler();
        }

        protected void ResourceRadScheduler_AppointmentClick(object sender, SchedulerEventArgs e)
        {
            lblAppointmentInfo.Text = e.Appointment.Subject + "<br/>";
            ResourceRadScheduler.Visible = false;
            lblAppointmentInfo.Visible = true;
            btnCloseAppointmentInfo.Visible = true;
        }

        protected void btnCloseAppointmentInfo_Click(object sender, EventArgs e)
        {
            ResourceRadScheduler.Visible = true;
            lblAppointmentInfo.Visible = false;
            btnCloseAppointmentInfo.Visible = false;
        }
    }
}