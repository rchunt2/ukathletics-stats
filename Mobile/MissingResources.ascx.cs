﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Mobile
{
    public partial class MissingResources : System.Web.UI.UserControl
    {
        private List<SessionType> eSessionType = new List<SessionType>();
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            eSessionType.Add(STATSModel.SessionType.GetByName("Tutoring - CATS"));
            eSessionType.Add(STATSModel.SessionType.GetByName("Tutoring - Football"));
            var numActive = PopulateMissing();
            lblNumResources.Text = numActive.ToString();
        }

        private int PopulateMissing()
        {
            var lstAbsent = new List<GVAbsent>();
            var eSessions = STATSModel.Session.GetActive(eSessionType[0]).Concat(STATSModel.Session.GetActive(eSessionType[1]));
            foreach (var eSession in eSessions)
            {
                if (eSession.SessionIsCancelled) { continue; } // If the session has been cancelled, don't even worry about it.
                var eResources = eSession.GetResourceInSession();
                foreach (var eResource in eResources)
                {
                    if (eResource.IsPresent(eSession))
                    {
                        lstAbsent.AddRange(GetMissingRequirements(eResource, eSession));
                    }
                }
            }
            gvMissing.DataSource = lstAbsent;
            gvMissing.DataBind();
            return lstAbsent.Count;
        }

        private List<GVAbsent> GetMissingRequirements(Resource eResource, Session eSession)
        {
            // NOTE: Tutors require ALL Students.
            // NOTE: Students require ONE Tutor.
            var result = new List<GVAbsent>();
            if (!eSession.SessionIsScheduled) { return result; } // If this isn't scheduled, it's not required for anyone...
            if (!eResource.IsPresent(eSession)) { return result; } // If this resource isn't here, of course there are no missing requirements.

            String strSubject = "N/A";

            if (eSession.HasAdditionalData())
            {
                var eSessionTutor = eSession.GetAdditionalData();
                var eCourse = eSessionTutor.Course;
                var eSubject = eCourse.Subject;
                strSubject = eSubject.SubjectNameShort + eCourse.CourseNumber;
            }

            if (eResource.ResourceType.ResourceTypeName == "Tutor")
            {
                Resource[] eResources = eSession.GetResourceInSession();
                foreach (var eResourceInSession in eResources)
                {
                    if (eResource.ResourceID != eResourceInSession.ResourceID) // Resources can't require themselves.
                    {
                        if (!eResourceInSession.IsPresent(eSession))
                        {
                            var gva = new GVAbsent(eResource.ResourceNameDisplayLastFirst, eResourceInSession.ResourceNameDisplayLastFirst, strSubject);
                            result.Add(gva);
                        }
                    }
                }
            }
            else
            {
                var eResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                if (!eResourceTutor.IsPresent(eSession))
                {
                    var gva = new GVAbsent(eResource.ResourceNameDisplayLastFirst, eResourceTutor.ResourceNameDisplayLastFirst, strSubject);
                    result.Add(gva);
                }
            }
            return result;
        }

        public class GVAbsent
        {
            private String _present;
            private String _missing;
            private String _subject;

            public String Present
            {
                get { return _present; }
            }

            public String Missing
            {
                get { return _missing; }
            }

            public String Subject
            {
                get { return _subject; }
            }

            public GVAbsent()
            {
            }

            public GVAbsent(String Present, String Missing, String Subject)
            {
                _present = Present;
                _missing = Missing;
                _subject = Subject;
            }
        }
    }
}