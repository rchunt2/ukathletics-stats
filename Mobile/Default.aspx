﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mobile.Master" AutoEventWireup="true"
	CodeBehind="Default.aspx.cs" Inherits="UK.STATS.Mobile.Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSearch" Src="~/ResourceSearch.ascx" %>
<%@ Register TagPrefix="SIS" TagName="LoggedInResources" Src="~/LoggedInResources.ascx" %>
<%@ Register TagPrefix="SIS" TagName="MissingResources" Src="~/MissingResources.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MobileSiteHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MobileSiteBody" runat="server">
	<asp:UpdatePanel ID="upMobile" runat="server">
		<ContentTemplate>
			<asp:Panel ID="panelLogin" Visible="True" runat="server" CssClass="LoginContainer"
				HorizontalAlign="Center">
                <img src="Logo_Full.png" alt="UK Athlethics Logo" />
				<asp:Login ID="login" runat="server" onauthenticate="login_Authenticate" 
					DisplayRememberMe="False" TextLayout="TextOnTop" TitleText="" Width="100%">
				    <LayoutTemplate>
                        <table id="LoginTable" cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                            <tr>
                                <td>
                                    <table cellpadding="0" style="width:100%;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="UserName" runat="server" Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                                    ControlToValidate="UserName" ErrorMessage="User Name is required." 
                                                    ToolTip="User Name is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" 
                                                    ControlToValidate="Password" ErrorMessage="Password is required." 
                                                    ToolTip="Password is required." ValidationGroup="login">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="color:Red;">
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
                                                    ValidationGroup="login" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
				</asp:Login>
			</asp:Panel>
			<asp:Panel ID="panelLogout" Visible="False" runat="server" CssClass="MobileHeader">
                <div class="MobileHeaderLogo">
                    <img src="Logo_Mobile.png" alt="UK Athletics Logo" />
                </div>
                <div class="MobileHeaderTitle">
                    <asp:Label ID="lblTitle" runat="server" Text="Application Title Goes Here"></asp:Label>
                </div>
                <div class="MobileHeaderText">
                    Welcome back,
				    <asp:Label ID="lblWelcome" runat="server" Text="UserName"></asp:Label><br />
				    <asp:LinkButton ID="lbLogout" runat="server" OnClick="lbLogout_Click">Logout?</asp:LinkButton>
                </div>
			</asp:Panel>
			<asp:Panel ID="panelStudentTutor" Visible="False" runat="server">
				<telerik:RadPanelBar ID="rpbMobileStandard" runat="server" 
					Width="100%"><%-- Skin="Web20"--%>
					<Items>
						<telerik:RadPanelItem runat="server" Text="Schedule">
						    <ContentTemplate>
							    Placeholder for Schedule Summary Module
						    </ContentTemplate>
						</telerik:RadPanelItem>
						<telerik:RadPanelItem runat="server" Text="Time">
							<ContentTemplate>
							    Placeholder for Time Tracker Module
						    </ContentTemplate>
						</telerik:RadPanelItem>
						<telerik:RadPanelItem runat="server" Text="Messages">
							<ContentTemplate>
                                Placeholder for Messaging Module
						    </ContentTemplate>
						</telerik:RadPanelItem>
					</Items>
				</telerik:RadPanelBar>
			</asp:Panel>
			<br />
			<br />
			<asp:Panel ID="panelSuperUser" Visible="False" runat="server">
				<telerik:RadPanelBar ID="rpbMobileSuper" runat="server" 
					Width="100%"><%-- Skin="Web20"--%>
					<Items>
						<telerik:RadPanelItem runat="server" Text="Search">
						<ContentTemplate>
							<SIS:ResourceSearch ID="ResourceSearchModule" runat="server" />
						</ContentTemplate>
						</telerik:RadPanelItem>
						<telerik:RadPanelItem runat="server" Text="Logged In">
							<ContentTemplate>
                                <SIS:LoggedInResources ID="LoggedInResourcesModule" runat="server" />
						    </ContentTemplate>
						</telerik:RadPanelItem>
						<telerik:RadPanelItem runat="server" Text="Missing">
							<ContentTemplate>
                                <SIS:MissingResources ID="MissinResourcesModule" runat="server" />
						    </ContentTemplate>
						</telerik:RadPanelItem> 
					</Items>
				</telerik:RadPanelBar>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
