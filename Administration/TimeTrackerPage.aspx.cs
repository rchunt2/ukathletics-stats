﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    public partial class TimeTrackerPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["resourceid"]))
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);

                TimeTrackerModule.ResourceID = ResourceID;
                TimeTrackerModule.IsVisible = true;
            }
            else
            {
                lblError.Text = "Time Tracking cannot be displayed at this time.";
                lblError.Visible = true;
                TimeTrackerModule.IsVisible = false;
                TimeTrackerModule.Visible = false;
            }
        }
    }
}