﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true"
    CodeBehind="Reports.aspx.cs" Inherits="UK.STATS.Administration.Pages.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSelection" Src="~/UserControls/ResourceSelection.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSelectionSingle" Src="~/UserControls/ResourceSelectionSingle.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <div id="paramFilter">
        <asp:Panel runat="server" ID="pnlParam">
            <telerik:RadPanelBar ID="rpbReportParams" runat="server" Width="700px">
                <Items>
                    <telerik:RadPanelItem Expanded="True" Text="Report Parameters">
                        <ContentTemplate>
                            <asp:Panel ID="panelDateFilterStart" runat="server" Visible="True">
                                <div class="header">
                                    <div style="display: inline-block; width: 50px;">From:</div>
                                    <telerik:RadDatePicker ID="txtDateFrom" runat="server">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                                        <%-- Skin="Web20"--%>
                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy"></DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                    <%-- Skin="Web20"--%>
                                </div>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="panelDateFilterEnd" runat="server" Visible="True">
                                <div class="header">
                                    <div style="display: inline-block; width: 50px;">To:</div>
                                    <telerik:RadDatePicker ID="txtDateTo" runat="server">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                                            ViewSelectorText="x">
                                        </Calendar>
                                        <%-- Skin="Web20"--%>
                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                    <%-- Skin="Web20"--%>
                                </div>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="panelResourceSelection" runat="server" Visible="True">
                                <SIS:ResourceSelection runat="server" ID="ResourceSelectionModule" />
                            </asp:Panel>
                            <asp:Panel ID="panelResourceSelectionSingle" runat="server" Visible="True">
                                <SIS:ResourceSelectionSingle runat="server" ID="ResourceSelectionSingleModule" />
                            </asp:Panel>
                            <asp:Panel ID="panelResourceSelectionToEmail" runat="server" Visible="True">
                                <br />
                                <asp:CheckBox ID="cbResourceSelectionToEmail" runat="server" Text="Send email to:  " Checked="false" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ErrorMessage="Invalid email address."    ControlToValidate="txtResourceSelectionToEmail" 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtResourceSelectionToEmail" runat="server"></asp:TextBox>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="panelAreaFilter" runat="server" Visible="True">
                                <div class="header">
                                    <div style="display: inline-block; width: 50px;">Area:</div>
                                    <%--<asp:DropDownList ID="ddlArea" runat="server" DataSourceID="edsSessionTypeParam"
                                        DataTextField="SessionTypeName" DataValueField="SessionTypeID"></asp:DropDownList>--%>
                                    <asp:ListBox ID="lbArea" SelectionMode="Multiple" runat="server" DataSourceID="edsSessionTypeParam"
                                        DataTextField="SessionTypeName" DataValueField="SessionTypeID" Height="80px"></asp:ListBox>
                                    <asp:CheckBox ID="cbFilterArea" runat="server" Text="Filter by Area" Checked="false" />
                                    <asp:CheckBox ID="cbFilterAreaUnscheduled" runat="server" Text="Unscheduled" Checked="false" Visible="false"/>
                                    <asp:CheckBox ID="cbFilterAreaScheduled" runat="server" Text="Scheduled" Checked="false" Visible="false"/>
                                    <asp:EntityDataSource ID="edsSessionTypeParam" runat="server" ConnectionString="name=STATSEntities"
                                        DefaultContainerName="STATSEntities" EnableFlattening="False" EntitySetName="SessionTypes"
                                        Select="it.[SessionTypeName], it.[SessionTypeID]" Where="it.[SessionTypeName] != 'Class' AND it.[SessionTypeName] != 'Note'" OrderBy="it.[SessionTypeName]">
                                    </asp:EntityDataSource>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelLocationFilter" runat="server" Visible="True">
                                <div class="header">
                                    <div style="display: inline-block; width: 50px;">Location:</div>
                                    <asp:DropDownList ID="ddlLocation" runat="server" DataSourceID="edsLocationParam"
                                        DataTextField="SessionTypeName" DataValueField="SessionTypeID">
                                    </asp:DropDownList>
                                    <asp:CheckBox ID="cbFilterLocation" runat="server" Text="Filter by Location" Checked="false" />
                                    <asp:EntityDataSource ID="edsLocationParam" runat="server" ConnectionString="name=STATSEntities"
                                        DefaultContainerName="STATSEntities" EnableFlattening="False" EntitySetName="SessionTypes"
                                        Select="it.[SessionTypeName], it.[SessionTypeID]" Where="it.[SessionTypeName] LIKE 'Tutoring%'" OrderBy="it.[SessionTypeName]">
                                    </asp:EntityDataSource>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelOnlyShowClasses" runat="server" Visible="True">
                                <asp:CheckBox ID="chkOnlyShowClasses" runat="server" Text="Only Show Classes" Checked="false" AutoPostBack="true" OnCheckedChanged="chkOnlyShowClasses_CheckedChanged" />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="panelEmailMessage" Visible="False">
                                <label>Email Message</label>
                                <br />
                                <asp:TextBox runat="server" ID="txtEmailMessage" TextMode="MultiLine" Width="100%" Height="100"></asp:TextBox>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="panelCourseSearch" Visible="False">
                                <label>Course Filter</label>
                                <br />
                                <asp:TextBox runat="server" ID="txtCourseSearch" Width="200"></asp:TextBox>
                            </asp:Panel>
                            <asp:Panel ID="panelNoParameters" runat="server" Visible="True">
                                <div style="text-align: center;">
                                    Note: No parameters are required for this report.<br />
                                    Please click the "View Report" button below to run your report.
                                </div>
                            </asp:Panel>
                            <br />
                            <asp:Button ID="btnRunReports" runat="server" Text="View Report" OnClick="btnRunReports_Click" />
                            <asp:Button ID="btnEmailReports" Visible="False" runat="server" Text="Email Report" OnClick="btnEmailReports_Click" />
                        </ContentTemplate>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <%-- Skin="Web20"--%>
        </asp:Panel>
    </div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
    <br />
        <asp:LinkButton ID="printreport" runat="server" Visible="true" Text="" OnClick="btnPrintReport_Click" />
    <br />
    <asp:Panel ID="pnlReportViewer" runat="server">
        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" Font-Names="Verdana"
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
            WaitMessageFont-Size="14pt" SizeToReportContent="True" ShowPrintButton="true" KeepSessionAlive="false">
        </rsweb:ReportViewer>
    </asp:Panel>
    
    <asp:Panel ID="pnlFrameReport" runat="server" Visible="false">
        <asp:HtmlIframe id="frameReport" src="" width="100%" height="500px" runat="server" seamless="true"></asp:HtmlIframe>
    </asp:Panel>
</asp:Content>