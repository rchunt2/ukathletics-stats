﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    public partial class Room : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["roomid"]))
            {
                Guid RoomID = Guid.Parse(Request.QueryString["roomid"]);
                STATSModel.Room eRoom = STATSModel.Room.Get(RoomID);
                HyperLink1.NavigateUrl = "~/Pages/Room.aspx";

                // Details View for Room
                panelRoomList.Visible = false;
                panelRoomView.Visible = true;
            }
            else
            {
                panelRoomList.Visible = true;
                panelRoomView.Visible = false;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvRoom.ChangeMode(DetailsViewMode.Insert);
            panelRoomList.Visible = false;
            panelRoomView.Visible = true;
            dvRoom.AutoGenerateInsertButton = true;
        }

        protected void dvRoom_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["roomid"]))
                {
                    Guid RoomID = Guid.Parse(Request.QueryString["roomid"]);
                    Response.Redirect("~/Pages/Room.aspx?roomid=" + RoomID.ToString(), true);
                }
                Response.Redirect("~/Pages/Room.aspx", true);
            }
        }

        protected void dvRoom_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            if (e.AffectedRows > 0)
            {
                string strMessage = "This Room has been Deleted.";
                string url = this.ResolveClientUrl("~/Pages/Room.aspx");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Delete", "<script language='javascript'>alert('" + strMessage + "');window.location='" + url + "';</script>", false);
            }
            //Response.Redirect("~/Pages/Room.aspx", true);
        }

        protected void dvRoom_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            e.Values["RoomID"] = Guid.NewGuid();
            DropDownList location = (DropDownList)dvRoom.Controls[0].Controls[0].FindControl("ddlLocation");
            e.Values["LocationID"] = location.SelectedValue;
        }

       
        protected void dvRoom_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/Room.aspx?roomid=" + e.Values["roomid"], true);
        }

        protected void dvRoom_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid RoomID = (Guid)dv.DataKey.Value;

            var eRoom = STATSModel.Room.Get(RoomID);

            if (!eRoom.CanBeDeleted)
            {
                String strError = "The Room has activity associated with it and cannot be deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }

        protected void dvRoom_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            DropDownList location = (DropDownList)dvRoom.Controls[0].Controls[0].FindControl("ddlLocation");
            e.NewValues["LocationID"] = location.SelectedValue;
        }
    }
}