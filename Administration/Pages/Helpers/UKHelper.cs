﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using STATSModel = UK.STATS.STATSModel;


namespace UK.STATS.Administration.Helpers
{
    public static class UKHelper 
    {
        public static UK.STATS.STATSModel.Authentication CurrentAuthentication()
        {
            if (LoggedIn())
            {
                Object SessionAuthenticationKey = System.Web.HttpContext.Current.Session["AuthenticationKey"];
                String StringAuthenticationKey = SessionAuthenticationKey.ToString();
                Guid AuthenticationKey = Guid.Parse(StringAuthenticationKey);
                STATSModel.Authentication obj = STATSModel.Authentication.Get(AuthenticationKey);
                return obj;
            }
            else
                return null;
        }

        public static bool LoggedIn()
        {
            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["AuthenticationKey"] == null || (Guid)System.Web.HttpContext.Current.Session["AuthenticationKey"] == Guid.Empty)
                return false;
            else
                return true;
        }
    }
}