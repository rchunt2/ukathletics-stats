﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;


namespace UK.STATS.Administration.Pages
{
    public partial class SubjectPage : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            SubjectSearchControl.ButtonAddEvent += new UK.STATS.Administration.UserControls.SubjectSearch.ButtonAddDelegate(ShowCreateControls);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Authentication curAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                
            if (curAuthentication.Resource.ResourceType.ResourceTypeName != "Director") 
            {
                panelSecurityDenied.Visible = true;
                return;
            }

            if (String.IsNullOrEmpty(Request.QueryString["subjectid"]))
            { 
                // No Subject selected - List all subjects in a table.
                panelSubjectList.Visible = true;
                panelSubjectView.Visible = false;
            }
            else
            { 
                // Subject selected - Display Subject Information
                panelSubjectList.Visible = false;
                panelSubjectView.Visible = true;
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ShowCreateControls();
        }

        private void ShowCreateControls()
        {
            dvSubject.ChangeMode(DetailsViewMode.Insert);
            dvSubject.AutoGenerateInsertButton = true;
            panelSubjectList.Visible = false;
            panelSubjectView.Visible = true;
        }

        protected void dvSubjectView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/Subject.aspx", false);
        }

        protected void dvSubjectView_OnItemInserting(object Sender, DetailsViewInsertEventArgs e)
        {
            String SubjectNameShort = e.Values["SubjectNameShort"].ToString();
            String SubjectName = SubjectNameShort;

            Boolean Exists = Subject.Exists(SubjectNameShort);
            if (Exists)
            {
                Subject eSubject = Subject.Get(SubjectNameShort);
                e.Values["SubjectID"] = eSubject.SubjectID;
                e.Values["SubjectNameShort"] = eSubject.SubjectNameShort;
                e.Values["SubjectName"] = eSubject.SubjectName;
                e.Cancel = true;
                Response.Redirect("~/Pages/Course.aspx?subjectid=" + e.Values["SubjectID"], false);
            }

            e.Values["SubjectID"] = Guid.NewGuid();
            e.Values["SubjectName"] = e.Values["SubjectNameShort"].ToString();

            if (SubjectNameShort.Length > 8)
            {
                var eResource = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication().Resource;
                String msg = "SubjectNameShort Field is too long!  Field was truncated.  Original: " + SubjectNameShort;
                STATSModel.Error.LogMessage(msg, eResource, HttpContext.Current.Request.UserHostAddress);
                e.Values["SubjectNameShort"] = SubjectNameShort.Substring(0, 8);
            }

            if (SubjectName.Length > 64)
            {
                var eResource = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication().Resource;
                String msg = "SubjectName Field is too long!  Field was truncated.  Original: " + SubjectName;
                STATSModel.Error.LogMessage(msg, eResource, HttpContext.Current.Request.UserHostAddress);
                e.Values["SubjectName"] = SubjectName.Substring(0, 64);
            }
        }

        protected void dvSubjectView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/Course.aspx?subjectid=" + e.Values["SubjectID"], false);
        }

        protected void dvSubjectView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/Subject.aspx", true);
            }
        }

        protected void dvSubject_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid SubjectID = (Guid)dv.DataKey.Value;

            var eSubject = STATSModel.Subject.Get(SubjectID);

            if (!eSubject.CanBeDeleted)
            {
                String strError = "The Subject has activity associated with it and cannot be deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }
    }
}