﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="TeamworksDownload.aspx.cs" Inherits="UK.STATS.Administration.Pages.TeamworksDownload" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSearch" Src="~/UserControls/ResourceSearch.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSchedule" Src="~/UserControls/ResourceSchedule.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceRegistrationSummary" Src="~/UserControls/ResourceRegistrationSummary.ascx" %>
<%@ Register TagPrefix="SIS" TagName="CourseRegistration" Src="~/UserControls/CourseRegistration.ascx" %>
<%@ Register TagPrefix="SIS" TagName="TimeTracker" Src="~/UserControls/TimeTracker.ascx" %>
<%@ Register TagPrefix="SIS" TagName="Messaging" Src="~/UserControls/Messaging.ascx" %>
<%@ Register TagPrefix="SIS" TagName="TutoringSkills" Src="~/UserControls/TutoringSkills.ascx" %>
<%@ Register TagPrefix="SIS" TagName="TimeReview" Src="~/UserControls/TimeReview.ascx" %>
<%@ Register TagPrefix="SIS" TagName="TutoringMultiRegistration" Src="~/UserControls/TutoringMultiRegistration.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceGroupMembership" Src="~/UserControls/ResourceGroupMembership.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourcePictureEdit" Src="~/UserControls/ResourcePictureEdit.ascx" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <telerik:RadPanelItem Expanded="True" Text="Report Parameters">
        <ContentTemplate>
            <asp:Panel ID="panelDateFilterStart" runat="server" Visible="True">
                <div class="header">
                    <div style="display: inline-block; width: 50px;">From:</div>
                    <telerik:RadDatePicker ID="txtDateFrom" runat="server">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>
                        <%-- Skin="Web20"--%>
                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy"></DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDatePicker>
                    <%-- Skin="Web20"--%>
                </div>
            </asp:Panel>
            <br />
            <asp:Panel ID="panelDateFilterEnd" runat="server" Visible="True">
                <div class="header">
                    <div style="display: inline-block; width: 50px;">To:</div>
                    <telerik:RadDatePicker ID="txtDateTo" runat="server">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                            ViewSelectorText="x">
                        </Calendar>
                        <%-- Skin="Web20"--%>
                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDatePicker>
                    <%-- Skin="Web20"--%>
                </div>
            </asp:Panel>
                           <br /><br />
            <asp:Button ID="btnDownloadCSV" runat="server" Text="Download CSV" OnClick="btnTeamworksDownload_Click" />
            <asp:Button ID="btnSynchronize" runat="server" Text="Synchronize Teamworks IDs" OnClick="btnSynchronize_Click" />
            

            
        </ContentTemplate>
    </telerik:RadPanelItem>
</asp:Content>
