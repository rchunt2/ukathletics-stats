﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.Pages
{
    public partial class CoursePage : UKBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Boolean hasCourseID = (!String.IsNullOrEmpty(Request.QueryString["courseid"]));
            Boolean hasSubjectID = (!String.IsNullOrEmpty(Request.QueryString["subjectid"]));

            if (hasCourseID)
            {
                // Details View for Course
                panelCourseList.Visible = false;
                panelCourseView.Visible = true;

                Guid CourseID = Guid.Parse(Request.QueryString["courseid"]);

                STATSModel.Course eCourse = STATSModel.Course.Get(CourseID);

                lblSubjectName.Text = "Subject: " + eCourse.Subject.SubjectNameShort;
                linkReturnToList.NavigateUrl = "~/Pages/Course.aspx?subjectid=" + eCourse.SubjectID;
                linkReturnToList.Visible = true;
            }
            else if (hasSubjectID)
            {
                // List all Courses in Subject
                Guid SubjectID = Guid.Parse(Request.QueryString["subjectid"]);
                linkReturnToList.NavigateUrl = "~/Pages/Course.aspx?subjectid=" + SubjectID;
                linkReturnToSubject.Visible = true;
                STATSModel.Subject eSubject = STATSModel.Subject.Get(SubjectID);
                lblSubjectName.Text = "Subject: " + eSubject.SubjectNameShort;
                panelCourseList.Visible = true;
                panelCourseView.Visible = false;
            }
            else
            {
                Response.Redirect("~/Pages/Subject.aspx", true);
            }
        }

        protected void dvCourseView_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            String strSubjectID = e.Values["SubjectID"].ToString();
            String strCourseNumber = e.Values["CourseNumber"].ToString();
            Guid SubjectID = Guid.Parse(strSubjectID);

            if (Course.Exists(SubjectID, strCourseNumber))
            {
                e.Cancel = true;
                Course eCourse = Course.Get(SubjectID, strCourseNumber);
                Response.Redirect("~/Pages/CourseSection.aspx?courseid=" + eCourse.CourseID.ToString(), true);
                return;
            }

            e.Values["CourseID"] = Guid.NewGuid();
        }

        protected void dvCourseView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            var temp = e.Values["CourseID"];
            Response.Redirect("~/Pages/CourseSection.aspx?courseid=" + e.Values["CourseID"], true);
        }

        protected void dvCourseView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            if (e.AffectedRows > 0)
            {
                string strMessage = "This Course has been Deleted.";
                string url = this.ResolveClientUrl("~/Pages/Course.aspx?subjectid=" + e.Values["SubjectID"]);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteError", "<script language='javascript'>alert('" + strMessage + "');window.location='" + url + "';</script>", false);
            }
        }

        protected void dvCourseView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/Course.aspx?subjectid=" + Request.QueryString["subjectid"], true);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvCourseView.ChangeMode(DetailsViewMode.Insert);
            dvCourseView.AutoGenerateInsertButton = true;
            panelCourseList.Visible = false;
            panelCourseView.Visible = true;
            linkReturnToSubject.Visible = false;
        }

        protected void dvCourseView_OnItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            Guid SubjectID = Guid.Parse(Request.QueryString["subjectid"]);
            STATSModel.Subject eSubject = STATSModel.Subject.Get(SubjectID);
            e.Values["CourseID"] = Guid.NewGuid();
            e.Values["SubjectID"] = SubjectID;
            e.Values["CourseModuleID"] = eSubject.SubjectNameShort + e.Values["CourseNumber"].ToString(); // Auto-populate default values.

            String CourseNumber = e.Values["CourseNumber"].ToString();
            Boolean Exists = Course.Exists(SubjectID, CourseNumber);
            if (Exists)
            {
                e.Cancel = true;
                Course eCourse = Course.Get(SubjectID, CourseNumber);
                Response.Redirect("~/Pages/CourseSection.aspx?courseid=" + eCourse.CourseID, true);
            }
        }

        protected void dvCourseView_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid CourseID = (Guid)dv.DataKey.Value;
            var eCourse = STATSModel.Course.Get(CourseID);
            if (!eCourse.CanBeDeleted)
            {
                String strError = "The Course has activity associated with it and cannot be deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }
    }
}