﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="SessionType.aspx.cs" Inherits="UK.STATS.Administration.Pages.SessionTypePage" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:Panel ID="panelSessionTypeList" runat="server" Visible="false">
        <div>
            <asp:Button ID="btnAdd" runat="server" Text="Create Session Type" 
                onclick="btnAdd_Click" />
            <br />
            <asp:GridView ID="gvSessionTypeList" runat="server" CellPadding="4" 
                EnableTheming="True" ForeColor="#333333" 
                HorizontalAlign="Left" Width="100%" AllowPaging="True" 
                onpageindexchanging="gvSessionTypeList_PageIndexChanging" PageSize="10">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SessionTypeName" HeaderText="Name" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionTypeDescription" HeaderText="Description" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="SessionTypeID" DataNavigateUrlFormatString="SessionType.aspx?Sessiontypeid={0}" Text="View Details" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
    </asp:Panel>

    <asp:Panel ID="panelSessionTypeView" runat="server" Visible="false">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/SessionType.aspx">Return to List</asp:HyperLink>
        <asp:DetailsView 
                ID="dvSessionTypeView" 
                runat="server" 
                DataSourceID="SessionTypeDataSource" 
                AutoGenerateRows="False" 
                CellPadding="4" 
                ForeColor="#333333" 
                GridLines="None" 
                Width="100%"
                DataKeyNames="SessionTypeID" 
                onitemdeleted="dvSessionTypeView_ItemDeleted" 
                oniteminserted="dvSessionTypeView_ItemInserted" 
                onitemcommand="dvSessionTypeView_ItemCommand">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="SessionTypeID" HeaderText="Session Type ID" ReadOnly="true" Visible="false" />                
                <asp:TemplateField HeaderText="Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditSessionTypeName" runat="server" Text='<%# Bind("SessionTypeName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredEditSessionTypeName" runat="server" 
                            ControlToValidate="txtEditSessionTypeName" ErrorMessage="*Required" 
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtEditSessionTypeName" runat="server" Text='<%# Bind("SessionTypeName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredInsertSessionTypeName" runat="server" 
                            ControlToValidate="txtEditSessionTypeName" ErrorMessage="*Required" 
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SessionTypeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditSessionTypeDesc" runat="server" 
                            Text='<%# Bind("SessionTypeDescription") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredEditSessionTypeDesc" runat="server" 
                            ControlToValidate="txtEditSessionTypeDesc" ErrorMessage="*Required" 
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtEditSessionTypeDesc" runat="server" 
                            Text='<%# Bind("SessionTypeDescription") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredInsertSessionTypeDesc" runat="server" 
                            ControlToValidate="txtEditSessionTypeDesc" ErrorMessage="*Required" 
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" 
                            Text='<%# Bind("SessionTypeDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:ObjectDataSource 
            ID="SessionTypeDataSource" 
            runat="server" 
            DeleteMethod="Delete"
            InsertMethod="Insert" 
            SelectMethod="Select" 
            UpdateMethod="Update" 
            TypeName="UK.STATS.STATSModel.SessionType"
            EnableViewState="False" 
            OldValuesParameterFormatString="{0}">
        <InsertParameters>
            <asp:Parameter DbType="Guid" Name="SessionTypeID" />
            <asp:Parameter Name="SessionTypeName" Type="String" />
            <asp:Parameter Name="SessionTypeDescription" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DbType="Guid" DefaultValue="" Name="SessionTypeID" QueryStringField="sessiontypeid" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="SessionTypeName" Type="String" />
            <asp:Parameter Name="SessionTypeDescription" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    </asp:Panel>
    <div class="clear"></div>
</asp:Content>