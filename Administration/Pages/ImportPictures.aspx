﻿<%@ Page Title="Import Pictures" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="ImportPictures.aspx.cs" Inherits="UK.STATS.Administration.Pages.ImportPictures" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    Starting URL: <asp:TextBox ID="txtStartingURL" runat="server" Width="200px">https://ukathletics.com/</asp:TextBox><br />
    <asp:Button ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click" /><br />
    
    Log:<br /> <asp:TextBox ID="txtLog" runat="server" TextMode="MultiLine" Width="100%" Height="600px"></asp:TextBox>
</asp:Content>