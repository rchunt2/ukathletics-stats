﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Session.aspx.cs" Inherits="UK.STATS.Administration.Pages.SessionPage" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:Panel ID="panelSessionList" runat="server" Visible="false">
        <div>
            <asp:Button ID="btnAdd" runat="server" Text="Create Session" 
                onclick="btnAdd_Click" />
            <br />
            <asp:GridView ID="gvSessionList" runat="server" CellPadding="4" 
                EnableTheming="True" ForeColor="#333333" 
                HorizontalAlign="Left" Width="100%" AllowPaging="True" 
                onpageindexchanging="gvSessionList_PageIndexChanging" PageSize="10">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SessionID" HeaderText="SessionID" Visible="false">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionID" HeaderText="CourseSectionID" Visible="false">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionTypeID" HeaderText="SessionTypeID" Visible="false">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionDateTimeStart" HeaderText="Date/Time Start" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionDateTimeEnd" HeaderText="Date/Time End" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionIsCancelled" HeaderText="Cancelled" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SessionIsScheduled" HeaderText="Scheduled" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="SessionID" DataNavigateUrlFormatString="Session.aspx?sessionid={0}" Text="View Details" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
    </asp:Panel>

    <asp:Panel ID="panelSessionView" runat="server" Visible="false">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/Session.aspx">Return to List</asp:HyperLink>
        <asp:DetailsView ID="dvSessionView" runat="server" 
            DataSourceID="SessionDataSource" AutoGenerateRows="False" CellPadding="4" 
            ForeColor="#333333" GridLines="None" Width="100%"
            DataKeyNames="SessionID" onitemdeleted="dvSessionView_ItemDeleted">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="SessionID" HeaderText="Session ID" ReadOnly="true" Visible="false" />
                <asp:BoundField DataField="CourseSectionID" HeaderText="Course Section ID" ReadOnly="true" Visible="false" />
                <asp:BoundField DataField="SessionTypeID" HeaderText="Session Type ID" ReadOnly="true" Visible="false" />
                <asp:BoundField DataField="SessionDateTimeStart" HeaderText="Date/Time Start" />
                <asp:BoundField DataField="SessionDateTimeEnd" HeaderText="Date/Time End" />
                <asp:BoundField DataField="SessionIsScheduled" HeaderText="Scheduled" />
                <asp:BoundField DataField="SessionIsCancelled" HeaderText="Cancelled" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:ObjectDataSource ID="SessionDataSource" runat="server" 
            DataObjectTypeName="UK.STATS.STATSModel.Session" DeleteMethod="Delete" 
            InsertMethod="Insert" SelectMethod="Select" 
            TypeName="UK.STATS.STATSModel.Session" UpdateMethod="Update">
            <DeleteParameters>
                <asp:QueryStringParameter Name="SessionID" QueryStringField="sessionid" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter DbType="Guid" Name="SessionID" />
                <asp:Parameter DbType="Guid" Name="CourseSectionID" />
                <asp:Parameter DbType="Guid" Name="SessionTypeID" />
                <asp:Parameter Name="SessionDateTimeStart" Type="DateTime" />
                <asp:Parameter Name="SessionDateTimeEnd" Type="DateTime" />
                <asp:Parameter Name="SessionIsScheduled" Type="Boolean" />
                <asp:Parameter Name="SessionIsCancelled" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter DbType="Guid" 
                    DefaultValue="" Name="SessionID"
                    QueryStringField="sessionid" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter DbType="Guid" Name="SessionID" />
                <asp:Parameter DbType="Guid" Name="CourseSectionID" />
                <asp:Parameter DbType="Guid" Name="SessionTypeID" />
                <asp:Parameter Name="SessionDateTimeStart" Type="DateTime" />
                <asp:Parameter Name="SessionDateTimeEnd" Type="DateTime" />
                <asp:Parameter Name="SessionIsScheduled" Type="Boolean" />
            </UpdateParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
    <div class="clear"></div>
</asp:Content>