﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="UK.STATS.Administration.Pages.SettingsPage" %>
<%@ Register TagPrefix="SIS" TagName="OpenParams" Src="~/UserControls/isOpenParameters.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">

    <asp:Panel ID="panelSettingsList" runat="server" Visible="false">
    <div>
            <asp:GridView ID="gvSettingsList" runat="server" CellPadding="4" 
                EnableTheming="True" ForeColor="#333333" 
                HorizontalAlign="Left" Width="100%" 
                onpageindexchanging="gvSettingsList_PageIndexChanging" OnRowDataBound="gvSettingsList_RowDataBound">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SettingKey" HeaderText="Key" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SettingValue" HeaderText="Value" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="SettingID" DataNavigateUrlFormatString="Settings.aspx?settingid={0}" Text="View Details" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <div style="clear: both;"></div>
            <SIS:OpenParams ID="isOPenParamsContorl" runat="server" />
    </div>
    </asp:Panel>

     <asp:Panel ID="panelSettingsView" runat="server" Visible="false">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/Settings.aspx">Return to List</asp:HyperLink>
         <asp:DetailsView ID="dvSettings" runat="server" Height="50px" Width="100%" 
             AutoGenerateRows="False" CellPadding="4" 
             DataKeyNames="SettingID,SettingKey,SettingValue" DataSourceID="edsSettings" 
             ForeColor="#333333" GridLines="None">
             <AlternatingRowStyle BackColor="White" />
             <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
             <EditRowStyle BackColor="#2461BF" />
             <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
             <Fields>
                 <asp:BoundField DataField="SettingKey" HeaderText="SettingKey" ReadOnly="True" 
                     SortExpression="SettingKey" ConvertEmptyStringToNull="False" />
                 <asp:BoundField DataField="SettingValue" HeaderText="SettingValue" 
                     SortExpression="SettingValue" ConvertEmptyStringToNull="False" />
                 <asp:CommandField ShowEditButton="True" />
             </Fields>
             <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
             <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
             <RowStyle BackColor="#EFF3FB" />
         </asp:DetailsView>
         <asp:EntityDataSource ID="edsSettings" runat="server" 
             ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
             EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
             EnableUpdate="True" EntitySetName="Settings" Where="(it.SettingID == @querySettingID)">
            <WhereParameters>
                <asp:QueryStringParameter QueryStringField="settingid" DbType="Guid" Name="querySettingID" ConvertEmptyStringToNull="True"/>
            </WhereParameters>


         </asp:EntityDataSource>
    </asp:Panel>
    <div class="clear"></div>

</asp:Content>
