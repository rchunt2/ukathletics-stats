﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="CourseSection.aspx.cs" Inherits="UK.STATS.Administration.Pages.CourseSectionPage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="Messaging" Src="~/UserControls/Messaging.ascx" %>


<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:HyperLink ID="linkReturnToCourseList" NavigateUrl="" runat="server" Visible="False">Return to Course List</asp:HyperLink>
    <br />
    <asp:Label ID="lblCourse" runat="server" Text="CourseName"></asp:Label>
    <asp:Panel ID="panelCourseSectionList" runat="server" Visible="false">
        <div>
            <asp:Button ID="btnAdd" runat="server" Text="Create Course Section" onclick="btnAdd_Click" />
            <asp:GridView ID="gvCourseSection" runat="server" AllowPaging="True" 
                AllowSorting="False" AutoGenerateColumns="False" CellPadding="4" 
                DataKeyNames="CourseSectionID" DataSourceID="edsCourseSectionList" 
                ForeColor="#333333" GridLines="None" Width="100%" 
                onrowdatabound="gvCourseSection_RowDataBound">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="CourseSectionID" HeaderText="CourseSectionID" ReadOnly="True" SortExpression="CourseSectionID" Visible="False"><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseDisplayName" HeaderText="Course Name" ReadOnly="True" SortExpression="CourseDisplayName" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionNumber" HeaderText="Section" SortExpression="CourseSectionNumber" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionDateStart" HeaderText="Date Start" SortExpression="CourseSectionDateStart" Visible="False" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionDateEnd" HeaderText="Date End" SortExpression="CourseSectionDateEnd" Visible="False" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionDaysMeet" HeaderText="Days Meet" SortExpression="CourseSectionDaysMeet" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeStart" DataFormatString="{0:t}" HeaderText="Time Start" SortExpression="CourseSectionTimeStart" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeEnd" DataFormatString="{0:t}" HeaderText="Time End" SortExpression="CourseSectionTimeEnd" ><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                    <asp:TemplateField HeaderText="Instructor Name" SortExpression="InstructorID">
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:Label ID="lblInstructor" runat="server" Text='<%# Bind("Instructor.InstructorName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField Text="Email Instructor" DataNavigateUrlFields="CourseSectionID,CourseID"  DataNavigateUrlFormatString="../Pages/CourseSection.aspx?coursesectionid={0}&amp;CourseID={1}&amp;message=1"   />
                    <asp:HyperLinkField DataNavigateUrlFields="CourseSectionID,CourseID" DataNavigateUrlFormatString="../Pages/CourseSection.aspx?coursesectionid={0}&amp;CourseID={1}" Text="View Details" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <asp:EntityDataSource ID="edsCourseSectionList" runat="server" 
                ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
                EnableFlattening="False" EntitySetName="CourseSections" Include="Instructor" Where="it.CourseID = @queryCourseID" OrderBy="it.CourseSectionNumber">
                <WhereParameters>
                    <asp:QueryStringParameter QueryStringField="courseid" DbType="Guid" Name="queryCourseID" ConvertEmptyStringToNull="True"/>
                </WhereParameters>
            </asp:EntityDataSource>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelCourseSectionView" runat="server" Visible="false">

        <asp:Panel ID="panelInsertWarning" runat="server" Visible="False">
            <div style="font-size: smaller; font-style: italic;">
                Before filling out this form, ensure that the Instructor is listed as an option in the drop-down list.
            </div>
        </asp:Panel>

        <asp:HyperLink ID="HyperLink1" NavigateUrl="" runat="server">Return to List</asp:HyperLink>
        <asp:DetailsView ID="dvCourseSection" runat="server" Height="50px" Width="100%" 
            CellPadding="4" ForeColor="#333333" GridLines="None" 
            AutoGenerateRows="False" DataKeyNames="CourseSectionID" 
            DataSourceID="edsCourseSectionView" 
            onitemcommand="dvCourseSection_ItemCommand" 
            onitemdeleted="dvCourseSection_ItemDeleted" 
            onitemdeleting="dvCourseSection_ItemDeleting"
            oniteminserted="dvCourseSection_ItemInserted" 
            oniteminserting="dvCourseSection_ItemInserting" 
            AutoGenerateEditButton="True" AutoGenerateDeleteButton="True" 
            onmodechanged="dvCourseSection_ModeChanged" 
            ondatabound="dvCourseSection_DataBound" 
            onitemupdating="dvCourseSection_ItemUpdating" >
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="CourseSectionID" HeaderText="CourseSectionID" 
                    ReadOnly="True" SortExpression="CourseSectionID" Visible="False" />
                <asp:TemplateField HeaderText="Date Start (Please set to semester beginning date)" 
                    SortExpression="CourseSectionDateStart">
                    <ItemTemplate>
                        <asp:Label ID="lblDateStart" runat="server" 
                            Text='<%# Bind("CourseSectionDateStart", "{0:MM/dd/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditDateStart" runat="server" 
                            Text='<%# Bind("CourseSectionDateStart", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtDateStart" runat="server" 
                            Text='<%# Bind("CourseSectionDateStart", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date End (Please set to semester ending date)" 
                    SortExpression="CourseSectionDateEnd">
                    <ItemTemplate>
                        <asp:Label ID="lblDateEnd" runat="server" 
                            Text='<%# Bind("CourseSectionDateEnd", "{0:MM/dd/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditDateend" runat="server" 
                            Text='<%# Bind("CourseSectionDateEnd", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtDateEnd" runat="server" 
                            Text='<%# Bind("CourseSectionDateEnd", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" 
                    HeaderText="Days Meet" SortExpression="CourseSectionDaysMeet">
                    <ItemTemplate>
                        <asp:Label ID="lblDaysMeet" runat="server" 
                            Text='<%# Bind("CourseSectionDaysMeet") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditDaysMeet" runat="server" 
                            Text='<%# Bind("CourseSectionDaysMeet") %>'></asp:TextBox><span style="color: #FFF;">(M)onday, (T)uesday, (W)ednesday, Thu(R)sday, (F)riday, (S)aturday</span>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="txtDaysMeet" runat="server" Text='<%# Bind("CourseSectionDaysMeet") %>'></asp:TextBox><span style="color: #FFF;">(M)onday, (T)uesday, (W)ednesday, Thu(R)sday, (F)riday, (S)aturday</span>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Time Start" 
                    SortExpression="CourseSectionTimeStart">
                    <ItemTemplate>
                        <asp:Label ID="lblTimeStart" runat="server" Text='<%# Bind("CourseSectionTimeStart", "{0:t}") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadMaskedTextBox ID="txtEditTimeStart"  Runat="server" Mask="##:##:##" Text='<%# Bind("CourseSectionTimeStart") %>' DisplayPromptChar="0" PromptChar="0"></telerik:RadMaskedTextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <telerik:RadMaskedTextBox ID="txtTimeStart"  Runat="server" Mask="##:##:##" Text='<%# Bind("CourseSectionTimeStart") %>' DisplayPromptChar="0" PromptChar="0"></telerik:RadMaskedTextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Time End" SortExpression="CourseSectionTimeEnd">
                    <ItemTemplate>
                        <asp:Label ID="lblTimeEnd" runat="server" Text='<%# Bind("CourseSectionTimeEnd", "{0:t}") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadMaskedTextBox ID="txtEditTimeEnd"  Runat="server" Mask="##:##:##" Text='<%# Bind("CourseSectionTimeEnd") %>' DisplayPromptChar="0" PromptChar="0"></telerik:RadMaskedTextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <telerik:RadMaskedTextBox ID="txtTimeEnd"  Runat="server" Mask="##:##:##" Text='<%# Bind("CourseSectionTimeEnd") %>' DisplayPromptChar="0" PromptChar="0"></telerik:RadMaskedTextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Section Number" 
                    SortExpression="CourseSectionNumber">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("CourseSectionNumber") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CourseSectionNumber") %>'></asp:TextBox>
                        <span style="font-style: italic; font-size: smaller; color: #FFF;">Note: Must be 3 digits.</span>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CourseSectionNumber") %>'></asp:TextBox>
                        <span style="font-style: italic; font-size: smaller; color: #FFF;">Note: Must be 3 digits.</span>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" 
                    HeaderText="Event Package ID" SortExpression="CourseSectionPackageID">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" 
                            Text='<%# Bind("CourseSectionPackageID") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" 
                            Text='<%# Bind("CourseSectionPackageID") %>'></asp:TextBox>
                        <span style="font-style: italic; font-size: smaller; color: #FFF;">Example: ENG 104 Section 001 = ENG104001</span>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" 
                            Text='<%# Bind("CourseSectionPackageID") %>'></asp:TextBox>
                        <span style="font-style: italic; font-size: smaller; color: #FFF;">Example: ENG 104 Section 001 = ENG104001</span>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Building" 
                    SortExpression="CourseSectionBuilding">
                    <EditItemTemplate>
                        <telerik:RadComboBox ID="rcbBuilding" Runat="server" AllowCustomText="True" 
                            DataSourceID="edsBuildings" DataTextField="CourseSectionBuilding" 
                            DataValueField="CourseSectionBuilding" EnableLoadOnDemand="True" 
                            Filter="StartsWith" MarkFirstMatch="True" 
                            Text='<%# Bind("CourseSectionBuilding") %>'>
                        </telerik:RadComboBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <telerik:RadComboBox ID="rcbBuilding" Runat="server" AllowCustomText="True" 
                            DataSourceID="edsBuildings" DataTextField="CourseSectionBuilding" 
                            DataValueField="CourseSectionBuilding" EnableLoadOnDemand="True" 
                            Filter="StartsWith" MarkFirstMatch="True">
                        </telerik:RadComboBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblBuilding" runat="server" 
                            Text='<%# Eval("CourseSectionBuilding") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CourseSectionRoom" 
                    HeaderText="Room" SortExpression="CourseSectionRoom" 
                    ConvertEmptyStringToNull="False" NullDisplayText=" " />
                <asp:BoundField ConvertEmptyStringToNull="False" 
                    DataField="CourseSectionEventType" HeaderText="CourseSectionEventType" 
                    ReadOnly="True" SortExpression="CourseSectionEventType" Visible="False" />
                <asp:TemplateField HeaderText="Instructor (Names are alphabetized by the last name)" SortExpression="InstructorID" 
                    ConvertEmptyStringToNull="True">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" 
                            DataSourceID="edsInstructor" DataTextField="InstructorName" AppendDataBoundItems="True"
                            DataValueField="InstructorID" SelectedValue='<%# Bind("InstructorID", "{0}") %>'>
                            <asp:ListItem Value="">None</asp:ListItem>
                        </asp:DropDownList>
                        <div style="font-style: italic; font-size: smaller; color: #FFF;">
                            Note: If the Instructor for this Section is not listed, you can add it on <a style="color: #D1DDF1;" href="./Professor.aspx">this page</a>.
                        </div>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" 
                            DataSourceID="edsInstructor" DataTextField="InstructorName" 
                            DataValueField="InstructorID" SelectedValue='<%# Bind("InstructorID", "{0}") %>' AppendDataBoundItems="True">
                            <asp:ListItem Value="">None</asp:ListItem>
                        </asp:DropDownList>
                        <div style="font-style: italic; font-size: smaller; color: #FFF;">
                            Note: If the Instructor for this Section is not listed, you can add it on <a style="color: #D1DDF1;" href="./Professor.aspx">this page</a>.
                        </div>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblInstructorName" runat="server" Text='<%# Eval("Instructor.InstructorName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:EntityDataSource ID="edsCourseSectionView" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
            EnableUpdate="True" EntitySetName="CourseSections" Include="Instructor" Where="it.CourseSectionID = @queryCourseSectionID">
            <WhereParameters>
                <asp:QueryStringParameter QueryStringField="coursesectionid" DbType="Guid" Name="queryCourseSectionID" ConvertEmptyStringToNull="True"/>
            </WhereParameters>
        </asp:EntityDataSource>
        <asp:EntityDataSource ID="edsInstructor" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableFlattening="False" EntitySetName="vw_Instructors"
            OrderBy="it.LastName">
        </asp:EntityDataSource>
        <asp:EntityDataSource ID="edsBuildings" runat="server" 
                            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
                            EnableFlattening="False" EntitySetName="CourseSections" 
                            Select="DISTINCT it.[CourseSectionBuilding]" OrderBy="it.[CourseSectionBuilding]">
                        </asp:EntityDataSource>
    </asp:Panel>
    
    <asp:Panel runat="server" Visible="false" ID="panelCourseSectionMessage">
    <asp:UpdatePanel ID="upSendMessage" runat="server">
            <ContentTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server">Return to List</asp:HyperLink>
            <br />
            <SIS:Messaging ID="MessagingModule" runat="server" />
            </ContentTemplate>
            </asp:UpdatePanel>
    </asp:Panel>
    <div class="clear"></div>
</asp:Content>
