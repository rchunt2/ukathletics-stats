﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Xml.Serialization;
using Microsoft.Reporting.WebForms;
using UK.STATS.Administration.UKSTATSDSReportsTableAdapters;
using UK.STATS.STATSModel;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    /// <summary>
    /// Contains functionality for running SSRS reports from the website.
    /// </summary>
    public partial class Reports : System.Web.UI.Page
    {
        private void StoreInViewstate(string key, object value)
        {
            if (ViewState[key] != null) ViewState.Remove(key);
            ViewState.Add(key, value);
        }

        private T GetFromViewState<T>(string key)
        {
            if (ViewState[key] != null)
                return ((T)ViewState[key]);
            return default(T);
        }

        private String XmlData
        {
            get
            {
                var xml = GetFromViewState<String>("ReportPage_XmlData");
                if (String.IsNullOrWhiteSpace(xml))
                {
                    xml = null;
                }
                return xml;
            }
            set { StoreInViewstate("ReportPage_XmlData", value); }
        }

        private String CourseSearch
        {
            get
            {
                var courseSearch = txtCourseSearch.Text.Trim();
                if (String.IsNullOrWhiteSpace(courseSearch))
                {
                    courseSearch = "";
                }
                return courseSearch;
            }
            set { CourseSearch = value; }
        }

        private Guid? SingleResourceID
        {
            get
            {
                var g = GetFromViewState<Guid?>("ReportPage_SingleResourceID");
                return g;
            }
            set { StoreInViewstate("ReportPage_SingleResourceID", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResourceSelectionModule.OnSelectionChanged += ResourceSelectionModule_OnSelectionChanged;
            ResourceSelectionSingleModule.OnSelectionChanged += ResourceSelectionSingleModule_OnSelectionChanged;
            //ResourceSelectionToEmail.OnSelectionChanged += ResourceSelectionToEmail_OnSelectionChanged;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtDateFrom.SelectedDate = DateTime.Now.AddDays(-7);
                txtDateTo.SelectedDate = DateTime.Now;
            }
            lblError.Text = "";
            String reportName = (Request.QueryString["r"]);

            if (String.IsNullOrEmpty(reportName))
            {
                pnlParam.Visible = false;
                btnRunReports.Visible = false;
                panelDateFilterStart.Visible = false;
                panelDateFilterEnd.Visible = false;
                panelResourceSelection.Visible = false;
                panelAreaFilter.Visible = false;
                panelNoParameters.Visible = false;
                panelLocationFilter.Visible = false;
                pnlReportViewer.Visible = false;
                
            }
            else
            {
                DisplayReportPanel(reportName);
            }
        }

        private void ResourceSelectionModule_OnSelectionChanged(UserControls.ResourceSelection.ResourceSelectionUpdatedEventArgs e)
        {
            XmlData = e.XmlData;

            String reportName = (Request.QueryString["r"]);
            if (reportName == "BatchSchedule")
            {
                FillBatchSchedule(XmlData);
                SetBatchSchedulePopupAddress();
            }
        }

        protected void chkOnlyShowClasses_CheckedChanged(object sender, EventArgs e)
        {
            SetBatchSchedulePopupAddress();
        }


        private void SetBatchSchedulePopupAddress()
        {
            string popupAddress = Request.Url.GetLeftPart(UriPartial.Authority) + this.ResolveUrl("~/Reports/PrintScheduleBatch.aspx");
            popupAddress += "?OnlyShowClasses=" + chkOnlyShowClasses.Checked;

            btnRunReports.Attributes.Add("onclick", "window.open('" + popupAddress + "'); return false;");
        }


        private void ResourceSelectionSingleModule_OnSelectionChanged(UserControls.ResourceSelectionSingle.ResourceSelectionUpdatedEventArgs e)
        {
            SingleResourceID = e.SelectedResourceID;
        }

        private void ResourceSelectionToEmail_OnSelectionChanged(UserControls.ResourceSelectionSingle.ResourceSelectionUpdatedEventArgs e)
        {
            SingleResourceID = e.SelectedResourceID;
        }

        // Used for some reports:
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        // Used for some reports:
        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        private void DisplayReportPanel(String sReportTitle)
        {
            // Parameter Selection Panel
            pnlParam.Visible = false;                       // Parameter Selection Container
            panelDateFilterStart.Visible = false;           // Date Start
            panelDateFilterEnd.Visible = false;             // Date End
            panelResourceSelection.Visible = false;         // Resource Selection
            panelResourceSelectionSingle.Visible = false;   // Resource Selection (SINGLE)
            panelAreaFilter.Visible = false;                // Area Filter
            panelNoParameters.Visible = false;              // No Parameters Message
            panelOnlyShowClasses.Visible = false;           // Only show classes checkbox
            panelLocationFilter.Visible = false;            // Filter by New Football Location or Original
            panelResourceSelectionToEmail.Visible = false;  // Resource Selection to receive email report
            panelCourseSearch.Visible = false;              // Allows filter on Course field of Course Roster report

            panelEmailMessage.Visible = false;
            btnEmailReports.Visible = false;

            // Report Viewer Panel
            pnlReportViewer.Visible = false;                // Rendered Reports display here

            // Batch Schedule Panel
            pnlFrameReport.Visible = false;                 // Batch Schedules display in an iframe.

            printreport.Text = "";
            
            switch (sReportTitle)
            {
                case "Resource":                            // Resource Information Report
                    pnlParam.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    panelResourceSelectionToEmail.Visible = true;
                    panelEmailMessage.Visible = true;
                    btnEmailReports.Visible = true;

                    break;

                case "CourseCheckSheet":                    // Course Check Sheet Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "CourseRoster":                        // Course Roster Report
                    pnlParam.Visible = true;
                    panelCourseSearch.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "BatchSchedule":                       // Batch Schedule Report
                    pnlParam.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlFrameReport.Visible = true;
                    panelOnlyShowClasses.Visible = true;
                    panelEmailMessage.Visible = true;
                    panelResourceSelectionToEmail.Visible = true;
                    
                    btnEmailReports.Visible = true;

                    //ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "ResourceCourseList":                  // Resource Course List Report
                    pnlParam.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    panelResourceSelectionToEmail.Visible = true;
                    panelEmailMessage.Visible = true;
                    btnEmailReports.Visible = true;

                    break;

                case "StudentActivity":                     // Student Activity Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    panelAreaFilter.Visible = true;
                    cbFilterAreaScheduled.Visible = true;
                    cbFilterAreaUnscheduled.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "AttendanceData":                     // 8/4/2019 - NEW Attendance Data Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = false;
                    panelAreaFilter.Visible = false;
                    cbFilterAreaScheduled.Visible = false;
                    cbFilterAreaUnscheduled.Visible = false;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "StudentActivitySummary":              // Student Activity Summary Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    //printreport.Visible = true;
                    panelResourceSelectionToEmail.Visible = true;
                    panelEmailMessage.Visible = true;
                    btnEmailReports.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeStudent.ResourceTypeID;
                    break;

                case "TutoringActivity":                    // Tutoring Activity Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelLocationFilter.Visible = true;
                    panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.AllowNullSelection = true;
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutoringActivityBatch":               // Tutoring Activity Batch Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutorException":                      // Tutoring Exceptions
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    //ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    //panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    //ResourceSelectionSingleModule.AllowNullSelection = true;
                    //ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutorWeeklySchedule":                 // Tutor Weekly Schedule Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutorWeeklyScheduleBatch":            // Tutor Weekly Schedule Batch Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutorSkills":                         // Tutor Skills Report
                    pnlParam.Visible = true;
                    panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.AllowNullSelection = true;
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutorSkillsByCourse":                 // Tutor Skills By Course Report
                    pnlParam.Visible = true;
                    panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.AllowNullSelection = true;
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutoringPayroll":                     // Tutoring Payroll Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelectionSingle.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.AllowNullSelection = false; //Issue ID 211 - must select a resource
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutoringActiveSessions":
                    pnlParam.Visible = true;
                    panelLocationFilter.Visible = true;
                    panelDateFilterStart.Visible = false;
                    panelDateFilterEnd.Visible = false;
                    panelResourceSelectionSingle.Visible = false;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionSingleModule.AllowNullSelection = false; //Issue ID 211 - must select a resource
                    ResourceSelectionSingleModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TutoringPayrollBatch":                // Tutoring Payroll Batch Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    ResourceSelectionModule.ResourceTypeIDFilter = ResourceTypeTutor.ResourceTypeID;
                    break;

                case "TransactionReport":                   // Transaction Report
                    pnlParam.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    panelAreaFilter.Visible = true;
                    //Only show the Scheduled checkbox on Student Activity Report
                    cbFilterAreaScheduled.Visible = false;
                    cbFilterAreaUnscheduled.Visible = false;
                    pnlReportViewer.Visible = true;

                   
                    break;

                case "UsageReport":                         // Usage Report
                    pnlParam.Visible = true;
                    //panelLocationFilter.Visible = true;
                    panelAreaFilter.Visible = true;
                    panelDateFilterStart.Visible = true;
                    panelDateFilterEnd.Visible = true;
                    panelResourceSelection.Visible = true;
                    pnlReportViewer.Visible = true;
                    break;
                default:

                    // Will throw an exception (and subsequently log it to the database) about the issue that occurred.
                    String errorText = String.Format("The requested report ({0}) could not be found.  This error was encountered while attempting to initialize the parameter selection panel.", sReportTitle);
                    throw new ArgumentException(errorText);
            }
        }

        private void RenderReport(string sReportTitle)
        {
            DateTime? dtFrom = txtDateFrom.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateFrom.ValidationDate) : null;
            DateTime? dtTo = txtDateTo.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateTo.ValidationDate) : null;
            StringBuilder sbSessionTypeIDs = new StringBuilder();
            foreach (ListItem item in lbArea.Items)
            {
                if (item.Selected)
                {
                    sbSessionTypeIDs.Append(item.Value + ",");
                }
            }
            if (sbSessionTypeIDs.Length > 0)
            {
                sbSessionTypeIDs.Remove(sbSessionTypeIDs.Length - 1, 1);
            }

            String sessionTypeIDs = cbFilterArea.Checked ? sbSessionTypeIDs.ToString() : "";

            bool? filterByScheduled = null;

            if (cbFilterAreaScheduled.Checked && !cbFilterAreaUnscheduled.Checked)
                filterByScheduled = true;
            else if (cbFilterAreaUnscheduled.Checked && !cbFilterAreaScheduled.Checked)
                filterByScheduled = false;

            Guid? sessionTypeID = cbFilterLocation.Checked ? (Guid?)Guid.Parse(ddlLocation.SelectedValue) : null;
            Guid? resourceID = SingleResourceID;
            String courseSearch = CourseSearch;

            String xmlData = XmlData;
            printreport.Text = "Print Report";

            switch (sReportTitle)
            {
                case "Resource":
                    FillResourceReport(xmlData);
                    break;

                case "CourseCheckSheet":
                    FillCheck(dtFrom, xmlData);
                    break;

                case "CourseRoster":
                    FillCourseRoster(xmlData, courseSearch);
                    break;
                    
                case "BatchSchedule":
                    FillBatchSchedule(xmlData);
                    printreport.Text = "";
                    break;

                case "ResourceCourseList":
                    FillResourceCourseList(xmlData);
                    break;
                    
                case "StudentActivity":
                    FillStudentActivityReport(dtFrom, dtTo, xmlData, sessionTypeIDs, filterByScheduled);
                    break;

                case "AttendanceData":
                    FillAttendanceDataReport(dtFrom, dtTo);
                    break;

                case "StudentActivitySummary":
                    FillWeeklyActivityReport(dtFrom, dtTo, xmlData);
                    break;

                case "TutoringActivity":
                    FillTutoringActivityReport(dtFrom, dtTo, resourceID, sessionTypeID);
                    break;

                case "TutoringActivityBatch":
                    FillTutoringActivityReportBatch(dtFrom, dtTo, xmlData);
                    break;

                case "TutorException":
                    FillTutoringExceptionReport(dtFrom, dtTo, xmlData);
                    break;

                case "TutorWeeklySchedule":
                    FillTutorWeeklyScheduleReport(dtFrom, dtTo, resourceID);
                    break;

                case "TutorWeeklyScheduleBatch":
                    FillTutorWeeklyScheduleBatchReport(dtFrom, dtTo, xmlData);
                    break;

                case "TutorSkills":
                    FillTutorSkillsReport(resourceID);
                    break;

                case "TutoringActiveSessions":
                    FillTutorActiveSessionReport(sessionTypeID);
                    break;

                case "TutorSkillsByCourse":
                    FillTutorSkillsReportByCourse(resourceID);
                    break;

                case "TutoringPayroll":
                    FillTutorPayrollReport(dtFrom, dtTo, resourceID);
                    break;

                case "TutoringPayrollBatch":
                    FillTutorPayrollBatchReport(dtFrom, dtTo, xmlData);
                    break;

                case "TransactionReport":
                    FillTransactionReport(dtFrom, dtTo, sessionTypeIDs, xmlData);
                    break;

                case "UsageReport":
                    FillUsageReport(dtFrom, dtTo, xmlData, sessionTypeIDs);
                    break;
                default:

                    // Will throw an exception (and subsequently log it to the database) about the issue that occurred.
                    String errorText = String.Format("The requested report ({0}) could not be found.  This error was encountered while attempting to render the report.", sReportTitle);
                    throw new ArgumentException(errorText);
            }
        }

        private void EmailReport(string reportName)
        {
            string xmlData = XmlData;

            switch (reportName)
            {
                case "BatchSchedule":
                    EmailBatchScheduleReport(xmlData);
                    break;
                case "Resource":
                    EmailResource(xmlData);
                    break;
                case "StudentActivitySummary":
                    EmailStudentActivitySummary(xmlData);
                    break;
                case "ResourceCourseList":
                    EmailResourceCourseList(xmlData);
                    break;
                default:
                    String errorText = String.Format("The requested report ({0}) could not be found.  This error was encountered while attempting to render the report.", reportName);
                    throw new ArgumentException(errorText);
            }
        }

        public Stream WKHtmlToPdf(string url)
        {
            var fileName = " - ";

            string strFile = "";
            if(File.Exists( Path.Combine(GetProgramFilesx86Path(), @"wkhtmltopdf\wkhtmltopdf.exe")))
                    strFile = Path.Combine(GetProgramFilesx86Path(), @"wkhtmltopdf\wkhtmltopdf.exe");
                else if (File.Exists(Path.Combine(GetProgramFilesx86Path(), @"wkhtmltopdf\bin\wkhtmltopdf.exe")))
                    strFile = Path.Combine(GetProgramFilesx86Path(), @"wkhtmltopdf\bin\wkhtmltopdf.exe");
            var psi = new ProcessStartInfo
            {
                FileName = strFile,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var p = new Process();

            p.StartInfo = psi;

            string switches = "";
            switches += "--page-size Letter ";
            p.StartInfo.Arguments = switches + " " + url + " " + fileName;
            p.Start();

            //read output
            var toReturn = new MemoryStream();
            var buffer = new byte[32768];
            int read;
            while ((read = p.StandardOutput.BaseStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                toReturn.Write(buffer, 0, read);
            }
            toReturn.Position = 0;

            // wait or exit
            p.WaitForExit(60000);
            p.Close();

            return toReturn;
        }

        #region Emailing Report to PDF Methods

        public Stream StudentActivityToPdf()
        {
            DateTime? dtFrom = txtDateFrom.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateFrom.ValidationDate) : null;
            DateTime? dtTo = txtDateTo.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateTo.ValidationDate) : null;
            StringBuilder sessionTypeIDs = new StringBuilder();
            foreach (ListItem item in lbArea.Items)
            {
                if (item.Selected)
                {
                    sessionTypeIDs.Append(item.Value + ",");
                }
            }
            if (sessionTypeIDs.Length > 0)
            {
                sessionTypeIDs.Remove(sessionTypeIDs.Length - 1, 1);
            }


            Guid? resourceID = SingleResourceID;
            String xmlData = XmlData;

            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new WeeklyActivityTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Stream stream = new MemoryStream(bytes);

            return stream;
        }

        public Stream ResourceCourseListToPdf()
        {
            String xmlData = XmlData;

            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new ResourceCourseListTableAdapter();
            var tbl = new UKSTATSDSReports.ResourceCourseListDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Stream stream = new MemoryStream(bytes);

            return stream;
        }

        public Stream ResourceToPdf()
        {
            DateTime? dtFrom = txtDateFrom.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateFrom.ValidationDate) : null;
            DateTime? dtTo = txtDateTo.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateTo.ValidationDate) : null;
            StringBuilder sessionTypeIDs = new StringBuilder();
            foreach (ListItem item in lbArea.Items)
            {
                if (item.Selected)
                {
                    sessionTypeIDs.Append(item.Value + ",");
                }
            }
            if (sessionTypeIDs.Length > 0)
            {
                sessionTypeIDs.Remove(sessionTypeIDs.Length - 1, 1);
            }
            Guid? resourceID = SingleResourceID;
            String xmlData = XmlData;

            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new WeeklyActivityTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Stream stream = new MemoryStream(bytes);

            return stream;
        }

        #endregion

        #region Report to PDF for Printing

        private MemoryStream ReportToPDF(string sReportTitle)
        {
            DateTime? dtFrom = txtDateFrom.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateFrom.ValidationDate) : null;
            DateTime? dtTo = txtDateTo.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateTo.ValidationDate) : null;
            StringBuilder sessionTypeIDs = new StringBuilder();
            foreach (ListItem item in lbArea.Items)
            {
                if (item.Selected)
                {
                    sessionTypeIDs.Append(item.Value + ",");
                }
            }
            if (sessionTypeIDs.Length > 0)
            {
                sessionTypeIDs.Remove(sessionTypeIDs.Length - 1, 1);
            }
            bool isScheduled = cbFilterAreaScheduled.Checked;
            Guid? resourceID = SingleResourceID;
            Guid? locationID = cbFilterLocation.Checked ? (Guid?)Guid.Parse(ddlLocation.SelectedValue) : null;
            String xmlData = XmlData;
            String courseSearch = CourseSearch;

            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }

            switch (sReportTitle)
            {
                case "Resource":
                    return ResourceReportToPdf(xmlData);
                case "CourseCheckSheet":
                    return CheckToPdf(dtFrom, xmlData);
                case "CourseRoster":
                    return CourseRosterToPdf(xmlData, courseSearch);
                //case "BatchSchedule":
                //    return BatchScheduleToPdf(xmlData);
                case "ResourceCourseList":
                    return ResourceCourseListToPdf(xmlData);
                case "StudentActivity":
                    return StudentActivityReportToPdf(dtFrom, dtTo, xmlData, sessionTypeIDs.ToString(), isScheduled);
                case "StudentActivitySummary":
                    return StudentActivitySummaryToPdf(dtFrom, dtTo, xmlData);
                case "TutoringActivity":
                    return TutoringActivityReportToPdf(dtFrom, dtTo, resourceID, locationID);
                case "TutoringActivityBatch":
                    return TutoringActivityReportBatchToPdf(dtFrom, dtTo, xmlData);
                case "TutorException":
                    return TutoringExceptionReportToPdf(dtFrom, dtTo, xmlData);
                case "TutorWeeklySchedule":
                    return TutorWeeklyScheduleReportToPdf(dtFrom, dtTo, resourceID);
                case "TutorWeeklyScheduleBatch":
                    return TutorWeeklyScheduleBatchReportToPdf(dtFrom, dtTo, xmlData);
                case "TutorSkills":
                    return TutorSkillsReportToPdf(resourceID);
                case "TutoringActiveSessions":
                    return TutorActiveSessionReportToPdf(locationID);
                case "TutorSkillsByCourse":
                    return TutorSkillsReportByCourseToPdf(resourceID);
                case "TutoringPayroll":
                    return TutorPayrollReportToPdf(dtFrom, dtTo, resourceID);
                case "TutoringPayrollBatch":
                    return TutorPayrollBatchReportToPdf(dtFrom, dtTo, xmlData);
                case "TransactionReport":
                    return TransactionReportToPdf(dtFrom, dtTo, sessionTypeIDs.ToString(), xmlData);
                case "UsageReport":
                    return UsageReportToPdf(dtFrom, dtTo, sessionTypeIDs.ToString(), xmlData);
                default:

                    // Will throw an exception (and subsequently log it to the database) about the issue that occurred.
                    String errorText = String.Format("The requested report ({0}) could not be found.  This error was encountered while attempting to render the report for printing.", sReportTitle);
                    throw new ArgumentException(errorText);
            }
        }

        private MemoryStream ResourceReportToPdf(String xmlData)
        {
            var ta = new ResourceReportTableAdapter();
            var tbl = new UKSTATSDSReports.ResourceReportDataTable();
            if (String.IsNullOrWhiteSpace(xmlData)) // Only set this parameter if the value is not null
            {
                ta.Fill(tbl, DBNull.Value);
            }
            else
            {
                ta.Fill(tbl, xmlData);
            }

            rptViewer.LocalReport.ReportPath = "Reports\\ResourceReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\ResourceReport.rdlc";
            if (!String.IsNullOrWhiteSpace(xmlData)) // Only set this parameter if the value is not null
            {
                rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            }
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
            
            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }
        
        private MemoryStream TutorActiveSessionReportToPdf(Guid? locationID)
        {
            var ta = new ActiveTutorSessionsTableAdapter();
            var tbl = new UKSTATSDSReports.ActiveTutorSessionsDataTable();
            ta.Fill(tbl, locationID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActiveSessions.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActiveSessions.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("LocationID", (locationID.HasValue ? locationID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream BatchScheduleToPdf(String xmlData)
        {
            try
            {
                
                var serializer = new XmlSerializer(typeof(List<Guid>));
                object deserialized = serializer.Deserialize(new StringReader(xmlData));
                var lstGuid = (List<Guid>)deserialized;
                Session["BatchScheduleList"] = lstGuid; // Persist this data in the session.
            }
            catch (Exception)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Batch Schedule report with data.  Please ensure that you have selected some resources and try again.");
            }

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream CheckToPdf(DateTime? dtFrom, String xmlData)
        {
            var ta = new CourseCheckSheetTableAdapter();
            var tbl = new UKSTATSDSReports.CourseCheckSheetDataTable();
            ta.Fill(tbl, dtFrom, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\CourseCheckSheet.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\CourseCheckSheet.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("dt", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream ResourceCourseListToPdf(String xmlData)
        {

            var ta = new ResourceCourseListTableAdapter();
            var tbl = new UKSTATSDSReports.ResourceCourseListDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream CourseRosterToPdf(String xmlData, String courseSearch)
        {
            var ta = new CourseRosterTableAdapter();
            var tbl = new UKSTATSDSReports.CourseRosterDataTable();
            ta.Fill(tbl, xmlData, courseSearch);
            rptViewer.LocalReport.ReportPath = "Reports\\CourseRoster.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\CourseRoster.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("CourseSearch", courseSearch));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorPayrollBatchReportToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringPayrollBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringPayrollBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutorPayrollBatchReport_SubreportProcessing;

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorPayrollReportToPdf(DateTime? dtFrom, DateTime? dtTo, Guid? rscID)
        {
            var ta = new TutoringPayrollTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringPayrollDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringPayroll.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringPayroll.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TransactionReportToPdf(DateTime? dtFrom, DateTime? dtTo, String sessionTypeIDs, String xmlData)
        {
            var ta = new TransactionsTableAdapter();
            var tbl = new UKSTATSDSReports.TransactionsDataTable();
            ta.Fill(tbl, dtFrom, dtTo, sessionTypeIDs, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TransactionReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TransactionReport.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream StudentActivityReportToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData, String sessionTypeIDs, bool isScheduled)
        {
            var ta = new StudentActivityTableAdapter();
            var tbl = new UKSTATSDSReports.StudentActivityDataTable();

            ta.Fill(tbl, dtFrom, dtTo, sessionTypeIDs, xmlData, isScheduled);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivity.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivity.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));


            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("IsScheduled", isScheduled.ToString()));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream WeeklyActivityReportToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new WeeklyActivityTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorWeeklyScheduleBatchReportToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorWeeklyScheduleBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorWeeklyScheduleBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutorWeeklyScheduleBatchReport_SubreportProcessing;

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorWeeklyScheduleReportToPdf(DateTime? dtFrom, DateTime? dtTo, Guid? rscID)
        {
            var ta = new WeeklyTutorScheduleTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyTutorScheduleDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorWeeklySchedule.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorWeeklySchedule.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("TutorID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutoringActivityReportToPdf(DateTime? dtFrom, DateTime? dtTo, Guid? rscID, Guid? sessionTypeID)
        {
            var ta = new TutoringActivityTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID, sessionTypeID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActivity.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActivity.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("TutorID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeID.HasValue ? sessionTypeID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutoringActivityReportBatchToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActivityBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActivityBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutoringActivityReportBatch_SubreportProcessing;

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorSkillsReportToPdf(Guid? rscID)
        {
            var ta = new TutorSkillsTableAdapter();
            var tbl = new UKSTATSDSReports.TutorSkillsDataTable();
            ta.Fill(tbl, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorSkills.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorSkills.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream UsageReportToPdf(DateTime? dtFrom, DateTime? dtTo, String sessionTypeIDs, String xmlData)
        {
            var ta = new UsageReportTableAdapter();
            var tbl = new UKSTATSDSReports.UsageReportDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData, sessionTypeIDs);
            rptViewer.LocalReport.ReportPath = "Reports\\UsageReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\UsageReport.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutoringExceptionReportToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new ExceptionReportTableAdapter();
            var tbl = new UKSTATSDSReports.ExceptionReportDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringException.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringException.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream TutorSkillsReportByCourseToPdf(Guid? rscID)
        {
            var ta = new CourseTutorSkillsTableAdapter();
            var tbl = new UKSTATSDSReports.CourseTutorSkillsDataTable();
            ta.Fill(tbl, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorSkillsByCourse.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorSkillsByCourse.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        private MemoryStream StudentActivitySummaryToPdf(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new WeeklyActivityTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            MemoryStream stream = new MemoryStream(bytes);

            return stream;
        }

        #endregion

        private static string GetProgramFilesx86Path()
        {
            //if (8 == IntPtr.Size ||
            //    (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            //{
            //    return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            //}
            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        #region Email Schedules
        /// <summary>
        /// Send a single email with multiple schedules to one individual
        /// </summary>
        /// <param name="toEmail">To</param>
        /// <param name="fromEmail">From</param>
        /// <param name="scheduleResourceIDs">Schedules to Retrieve</param>
        /// <param name="reportName">The name of the report to use in the message </param>
        private void SendScheduleEmail(string toEmail, string fromEmail, List<Guid> scheduleResourceIDs)
        {
            var eSettingHost = Setting.GetByKey("WebHost");
            var webHost = eSettingHost.SettingValue;

            var From = fromEmail;

            if (!Regex.IsMatch(toEmail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                
                lblError.Text += "Invalid email address<br />";
                return;
            }

            var To = toEmail;
            var Body = "UK Schedule \n\n" + txtEmailMessage.Text;
            var Subject = "Message From UK";

            try
            {
                var oMail = new MailMessage(From, To, Subject, Body);
                oMail.CC.Add(new MailAddress(From));

                // Get each of the schedules that were selected and add them as pdf to the email that
                // is being sent to one user.
                foreach (Guid singleResourceID in scheduleResourceIDs)
                {
                    Resource resource = Resource.Get(singleResourceID);
                    var attachStream = WKHtmlToPdf(webHost + "PrintSchedule.aspx?resourceid=" + singleResourceID);
                    oMail.Attachments.Add(new Attachment(attachStream, "Schedule" + resource.ResourceNameLast + resource.ResourceNameFirst + ".pdf"));
                }

                Setting eSettingSMTPHost = Setting.GetByKey("SMTPHost");
                Setting eSettingSMTPPort = Setting.GetByKey("SMTPPort");

                String SMTPHost = eSettingSMTPHost.SettingValue;
                int SMTPPort = Int32.Parse(eSettingSMTPPort.SettingValue);

                var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                STATSModel.Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }
        }

        private void SendBatchScheduleEmail(Guid resourceId, string fromEmail)
        {
            var eSettingHost = Setting.GetByKey("WebHost");
            var webHost = eSettingHost.SettingValue;

            var attachStream = WKHtmlToPdf(webHost + "PrintSchedule.aspx?resourceid=" + resourceId);
            var toEmail = string.Empty;
            Resource resource;
            using(var context = new STATSEntities())
            {
                resource = context.Resources.SingleOrDefault(x => x.ResourceID == resourceId);
                if (resource != null)
                    toEmail = resource.ResourceEmail;
            }

            if (string.IsNullOrEmpty(toEmail))
            {
                lblError.Text += resource != null ? resource.ResourceNameDisplayLastFirst + " does not have an email" : "Problem getting resource" + "<br />";
                return;
            }

            var From = fromEmail;
            var To = toEmail;
            var Body = "UK Schedule\n\n" + txtEmailMessage.Text;
            var Subject = "Message From UK";

            try
            {
                var oMail = new MailMessage(From, To, Subject, Body);
                oMail.CC.Add(new MailAddress(From));
                oMail.Attachments.Add(new Attachment(attachStream, "schedule.pdf"));

                Setting eSettingSMTPHost = Setting.GetByKey("SMTPHost");
                Setting eSettingSMTPPort = Setting.GetByKey("SMTPPort");

                String SMTPHost = eSettingSMTPHost.SettingValue;
                int SMTPPort = Int32.Parse(eSettingSMTPPort.SettingValue);

                var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                STATSModel.Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }
        }

        private void EmailBatchScheduleReport(string xmlData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    lblError.Text = "No groups or resources selected.";
                    return;
                }
                var serializer = new XmlSerializer(typeof(List<Guid>));
                var deserialized = serializer.Deserialize(new StringReader(xmlData));
                var lstGuid = (List<Guid>)deserialized;

                string curUserEmail;
                using(var context = new STATSEntities())
                {
                    var authId = (Guid)Session["AuthenticationKey"];
                    var curResourceId = context.Authentications.Single(x => x.AuthenticationID == authId).ResourceID;
                    curUserEmail = context.Resources.Single(x => x.ResourceID == curResourceId).ResourceEmail;
                }

                //If the user has chosen to send the schedules to one person rather then each individual
                //within the group, do so now. 
                if (cbResourceSelectionToEmail.Checked == true && !string.IsNullOrEmpty(txtResourceSelectionToEmail.Text))
                {
                    SendScheduleEmail(txtResourceSelectionToEmail.Text, curUserEmail, lstGuid);
                }
                else
                {
                    foreach (var resourceId in lstGuid)
                    {
                        using (var context = new STATSEntities())
                        {
                            if (context.Groups.SingleOrDefault(x => x.GroupID == resourceId) != null)
                            {
                                var groupId = resourceId;
                                foreach (var memId in context.GroupMembers.Where(x => x.GroupID == groupId).Select(x => x.ResourceID))
                                    SendBatchScheduleEmail(memId, curUserEmail);
                            }
                        }

                        SendBatchScheduleEmail(resourceId, curUserEmail);
                    }
                }
            }
            catch (Exception ex)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Batch Schedule report with data.  Please ensure that you have selected some resources and try again. " + ex.ToString());
            }
        }

        #endregion

        #region Email Student Activity Summary

        /// <summary>
        /// Send the email to one user that includes one or more resource's Activity
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="fromEmail"></param>
        /// <param name="scheduleResourceIDs"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        private void SendStudentActivityEmail(string toEmail, string fromEmail, string xmlData)
        {
            var eSettingHost = Setting.GetByKey("WebHost");
            var webHost = eSettingHost.SettingValue;

            var From = fromEmail;

            if (!Regex.IsMatch(toEmail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {

                lblError.Text += "Invalid email address<br />";
                return;
            }

            var To = toEmail;
            var Body = "UK Student Activity \n\n" + txtEmailMessage.Text;
            var Subject = "Message From UK";

            try
            {
                var oMail = new MailMessage(From, To, Subject, Body);
                oMail.CC.Add(new MailAddress(From));

                // Get each of the schedules that were selected and add them as pdf to the email that
                // is being sent to one user.
                
                var attachStream = StudentActivityToPdf();
                oMail.Attachments.Add(new Attachment(attachStream, "StudentActivity.pdf"));

                Setting eSettingSMTPHost = Setting.GetByKey("SMTPHost");
                Setting eSettingSMTPPort = Setting.GetByKey("SMTPPort");

                String SMTPHost = eSettingSMTPHost.SettingValue;
                int SMTPPort = Int32.Parse(eSettingSMTPPort.SettingValue);

                var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                STATSModel.Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }
        }

        /// <summary>
        /// Send the email to one user that includes one or more resource's Activity
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="fromEmail"></param>
        /// <param name="scheduleResourceIDs"></param>
        private void SendResourceEmail(string toEmail, string fromEmail, string xmlData)
        {
            var eSettingHost = Setting.GetByKey("WebHost");
            var webHost = eSettingHost.SettingValue;

            var From = fromEmail;

            if (!Regex.IsMatch(toEmail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {

                lblError.Text += "Invalid email address<br />";
                return;
            }

            var To = toEmail;
            var Body = "UK Resource \n\n" + txtEmailMessage.Text;
            var Subject = "Message From UK";

            try
            {
                var oMail = new MailMessage(From, To, Subject, Body);
                oMail.CC.Add(new MailAddress(From));

                // Get each of the schedules that were selected and add them as pdf to the email that
                // is being sent to one user.

                var attachStream = ResourceToPdf();
                oMail.Attachments.Add(new Attachment(attachStream, "Resource.pdf"));

                Setting eSettingSMTPHost = Setting.GetByKey("SMTPHost");
                Setting eSettingSMTPPort = Setting.GetByKey("SMTPPort");

                String SMTPHost = eSettingSMTPHost.SettingValue;
                int SMTPPort = Int32.Parse(eSettingSMTPPort.SettingValue);

                var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                STATSModel.Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }
        }

        /// <summary>
        /// Send the email to one user that includes one or more resource's Activity
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="fromEmail"></param>
        /// <param name="scheduleResourceIDs"></param>
        private void SendResourceCourseListEmail(string toEmail, string fromEmail, string xmlData)
        {
            var eSettingHost = Setting.GetByKey("WebHost");
            var webHost = eSettingHost.SettingValue;

            var From = fromEmail;

            if (!Regex.IsMatch(toEmail, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {

                lblError.Text += "Invalid email address<br />";
                return;
            }

            var To = toEmail;
            var Body = "UK Resource Course List \n\n" + txtEmailMessage.Text;
            var Subject = "Message From UK";

            try
            {
                var oMail = new MailMessage(From, To, Subject, Body);
                oMail.CC.Add(new MailAddress(From));

                // Get each of the schedules that were selected and add them as pdf to the email that
                // is being sent to one user.

                var attachStream = ResourceCourseListToPdf();
                oMail.Attachments.Add(new Attachment(attachStream, "ResourceCourseList.pdf"));

                Setting eSettingSMTPHost = Setting.GetByKey("SMTPHost");
                Setting eSettingSMTPPort = Setting.GetByKey("SMTPPort");

                String SMTPHost = eSettingSMTPHost.SettingValue;
                int SMTPPort = Int32.Parse(eSettingSMTPPort.SettingValue);

                var SMTPServer = new SmtpClient(SMTPHost, SMTPPort);

                SMTPServer.Send(oMail);
            }
            catch (Exception ex)
            {
                String emailContents = "From:" + From + "|To:" + To + "|Subject:" + Subject + "|Body:" + Body;
                STATSModel.Error.LogMessage("Email failed to send.  Email Contents: [" + emailContents + "] " + "Exception: " + ex.ToString(), new Resource(), "0.0.0.0");
            }
        }

        private void EmailStudentActivitySummary(string xmlData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    lblError.Text = "No groups or resources selected.";
                    return;
                }
                var serializer = new XmlSerializer(typeof(List<Guid>));
                var deserialized = serializer.Deserialize(new StringReader(xmlData));
                //var lstGuid = (List<Guid>)deserialized;

                string curUserEmail;
                using (var context = new STATSEntities())
                {
                    var authId = (Guid)Session["AuthenticationKey"];
                    var curResourceId = context.Authentications.Single(x => x.AuthenticationID == authId).ResourceID;
                    curUserEmail = context.Resources.Single(x => x.ResourceID == curResourceId).ResourceEmail;
                }

                //If the user has chosen to send the schedules to one person rather then each individual
                //within the group, do so now. 
                if (cbResourceSelectionToEmail.Checked == true && !string.IsNullOrEmpty(txtResourceSelectionToEmail.Text))
                {
                    SendStudentActivityEmail(txtResourceSelectionToEmail.Text, curUserEmail, xmlData);
                }
            }
            catch (Exception ex)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Student Activity report with data.  Please ensure that you have selected some resources and try again. " + ex.ToString());
            }
        }

        private void EmailResource(string xmlData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    lblError.Text = "No groups or resources selected.";
                    return;
                }
                var serializer = new XmlSerializer(typeof(List<Guid>));
                var deserialized = serializer.Deserialize(new StringReader(xmlData));
                
                string curUserEmail;
                using (var context = new STATSEntities())
                {
                    var authId = (Guid)Session["AuthenticationKey"];
                    var curResourceId = context.Authentications.Single(x => x.AuthenticationID == authId).ResourceID;
                    curUserEmail = context.Resources.Single(x => x.ResourceID == curResourceId).ResourceEmail;
                }

                //If the user has chosen to send the schedules to one person rather then each individual
                //within the group, do so now. 
                if (cbResourceSelectionToEmail.Checked == true && !string.IsNullOrEmpty(txtResourceSelectionToEmail.Text))
                {
                   SendResourceEmail(txtResourceSelectionToEmail.Text, curUserEmail, xmlData);
                }
            }
            catch (Exception ex)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Resource report with data.  Please ensure that you have selected some resources and try again. " + ex.ToString());
            }
        }

        private void EmailResourceCourseList(string xmlData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    lblError.Text = "No groups or resources selected.";
                    return;
                }
                var serializer = new XmlSerializer(typeof(List<Guid>));
                var deserialized = serializer.Deserialize(new StringReader(xmlData));
                //var lstGuid = (List<Guid>)deserialized;

                string curUserEmail;
                using (var context = new STATSEntities())
                {
                    var authId = (Guid)Session["AuthenticationKey"];
                    var curResourceId = context.Authentications.Single(x => x.AuthenticationID == authId).ResourceID;
                    curUserEmail = context.Resources.Single(x => x.ResourceID == curResourceId).ResourceEmail;
                }

                //If the user has chosen to send the schedules to one person rather then each individual
                //within the group, do so now. 
                if (cbResourceSelectionToEmail.Checked == true && !string.IsNullOrEmpty(txtResourceSelectionToEmail.Text))
                {
                    SendResourceCourseListEmail(txtResourceSelectionToEmail.Text, curUserEmail, xmlData);
                }
            }
            catch (Exception ex)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Resource Course List report with data.  Please ensure that you have selected some resources and try again. " + ex.ToString());
            }
        }

        #endregion

        #region Email Resource Information

        #endregion

        #region Email Resource Course List

        #endregion

        #region Fill Reports

        private void FillTutorActiveSessionReport(Guid? sessionTypeID)
        {
            var ta = new ActiveTutorSessionsTableAdapter();
            var tbl = new UKSTATSDSReports.ActiveTutorSessionsDataTable();
            ta.Fill(tbl, sessionTypeID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActiveSessions.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActiveSessions.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeID.HasValue ? sessionTypeID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillCheck(DateTime? dtFrom, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new CourseCheckSheetTableAdapter();
            var tbl = new UKSTATSDSReports.CourseCheckSheetDataTable();
            ta.Fill(tbl, dtFrom, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\CourseCheckSheet.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\CourseCheckSheet.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("dt", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillResourceReport(String xmlData)
        {
            var ta = new ResourceReportTableAdapter();
            var tbl = new UKSTATSDSReports.ResourceReportDataTable();
            if (String.IsNullOrWhiteSpace(xmlData)) // Only set this parameter if the value is not null
            {
                ta.Fill(tbl, DBNull.Value);
            }
            else
            {
                ta.Fill(tbl, xmlData);
            }

            rptViewer.LocalReport.ReportPath = "Reports\\ResourceReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\ResourceReport.rdlc";
            if (!String.IsNullOrWhiteSpace(xmlData)) // Only set this parameter if the value is not null
            {
                rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            }
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillBatchSchedule(String xmlData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xmlData))
                {
                    lblError.Text = "No groups or resources selected.";
                    return;
                }
                var serializer = new XmlSerializer(typeof(List<Guid>));
                object deserialized = serializer.Deserialize(new StringReader(xmlData));
                var lstGuid = (List<Guid>)deserialized;
                Session["BatchScheduleList"] = lstGuid; // Persist this data in the session.
            }
            catch (Exception)
            {
                // The user probably forgot to select something.
                throw new Exception("An error occurred while filling the Batch Schedule report with data.  Please ensure that you have selected some resources and try again.");
            }
        }

        private void FillResourceCourseList(String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new ResourceCourseListTableAdapter();
            var tbl = new UKSTATSDSReports.ResourceCourseListDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\ResourceCourseList.rdlc";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillCourseRoster(String xmlData, String courseSearch)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new CourseRosterTableAdapter();
            var tbl = new UKSTATSDSReports.CourseRosterDataTable();
            ta.Fill(tbl, xmlData, courseSearch);
            rptViewer.LocalReport.ReportPath = "Reports\\CourseRoster.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\CourseRoster.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("CourseSearch", courseSearch));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorPayrollBatchReport(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringPayrollBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringPayrollBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutorPayrollBatchReport_SubreportProcessing;
        }

        private void FillTutorPayrollBatchReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            var ta = new TutoringPayrollTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringPayrollDataTable();

            DateTime dtFrom = DateTime.Parse(e.Parameters["DateRangeStart"].Values.First());
            DateTime dtTo = DateTime.Parse(e.Parameters["DateRangeEnd"].Values.First());
            Guid rID = Guid.Parse(e.Parameters["ResourceID"].Values.First());

            ta.Fill(tbl, dtFrom, dtTo, rID);

            if (tbl.Rows.Count == 0)
            {
                //DM - 10/19 - Issue ID 235 - To prevent blank pages and some tutors in a group not showing, add a new row with basic info set.
                UKSTATSDSReports.TutoringPayrollRow newRow = tbl.NewTutoringPayrollRow();
                STATSModel.Resource tutor = STATSModel.Resource.Get(rID);
                newRow.ResourceNameFirst = tutor.ResourceNameFirst;
                newRow.ResourceNameLast = tutor.ResourceNameLast;
                newRow.Scheduled = 0;
                newRow.Unscheduled = 0;
                newRow.Attended = 0;
                newRow.AverageMinutes = 0;
                newRow.TotalMinutes = 0;
                tbl.Rows.Add(newRow);
            }

            e.DataSources.Clear();
            e.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorPayrollReport(DateTime? dtFrom, DateTime? dtTo, Guid? rscID)
        {
            var ta = new TutoringPayrollTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringPayrollDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringPayroll.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringPayroll.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));

            rptViewer.LocalReport.Refresh();
        }

        private void FillTransactionReport(DateTime? dtFrom, DateTime? dtTo, String sessionTypeIDs, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new TransactionsTableAdapter();
            var tbl = new UKSTATSDSReports.TransactionsDataTable();
            ta.Fill(tbl, dtFrom, dtTo, (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null), xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TransactionReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TransactionReport.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeIDs", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillStudentActivityReport(DateTime? dtFrom, DateTime? dtTo, String xmlData, String sessionTypeIDs, bool? filterByScheduled)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new StudentActivityTableAdapter();
            var tbl = new UKSTATSDSReports.StudentActivityDataTable();

            ta.Fill(tbl, dtFrom, dtTo, (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null), xmlData, filterByScheduled);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivity.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivity.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("IsScheduled", filterByScheduled.HasValue ? filterByScheduled.Value.ToString() : null));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillAttendanceDataReport(DateTime? dtFrom, DateTime? dtTo)
        {
            var ta = new AttendanceDataTableAdapter();
            var tbl = new UKSTATSDSReports.AttendanceDataDataTable();

            ta.Fill(tbl, dtFrom, dtTo);
            rptViewer.LocalReport.ReportPath = "Reports\\AttendanceData.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\AttendanceData.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }


        private void FillWeeklyActivityReport(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new WeeklyActivityTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\StudentActivitySummary.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorWeeklyScheduleBatchReport(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorWeeklyScheduleBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorWeeklyScheduleBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutorWeeklyScheduleBatchReport_SubreportProcessing;
        }

        private void FillTutorWeeklyScheduleBatchReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            var ta = new WeeklyTutorScheduleTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyTutorScheduleDataTable();

            DateTime dtFrom = DateTime.Parse(e.Parameters["DateRangeStart"].Values.First());
            DateTime dtTo = DateTime.Parse(e.Parameters["DateRangeEnd"].Values.First());
            Guid rID = Guid.Parse(e.Parameters["TutorID"].Values.First());

            ta.Fill(tbl, dtFrom, dtTo, rID);

            if (tbl.Rows.Count == 0)
            {
                //DM - 10/18 - Issue ID 233 - To prevent blank pages and some tutors in a group not showing, add a new row with basic info set. In the report since the SessionID is null, it knows it is not a real row and shows a "No results for this tutor" label
                UKSTATSDSReports.WeeklyTutorScheduleRow newRow = tbl.NewWeeklyTutorScheduleRow();
                STATSModel.Resource tutor = STATSModel.Resource.Get(rID);
                newRow.TutorName = tutor.ResourceNameDisplayLastFirst;

                tbl.Rows.Add(newRow);
            }

            e.DataSources.Clear();
            e.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorWeeklyScheduleReport(DateTime? dtFrom, DateTime? dtTo, Guid? rscID)
        {
            var ta = new WeeklyTutorScheduleTableAdapter();
            var tbl = new UKSTATSDSReports.WeeklyTutorScheduleDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorWeeklySchedule.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorWeeklySchedule.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("TutorID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutoringActivityReport(DateTime? dtFrom, DateTime? dtTo, Guid? rscID, Guid? sessionTypeID)
        {
            var ta = new TutoringActivityTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringActivityDataTable();
            ta.Fill(tbl, dtFrom, dtTo, rscID, sessionTypeID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActivity.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActivity.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("TutorID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeID.HasValue ? sessionTypeID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        //FillTutoringActivityReportBatch
        private void FillTutoringActivityReportBatch(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new ExtractGuidFromXMLTableAdapter();
            var tbl = new UKSTATSDSReports.ExtractGuidFromXMLDataTable();
            ta.Fill(tbl, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringActivityBatch.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringActivityBatch.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.ShowDetailedSubreportMessages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSBatchDataSet", (System.Data.DataTable)tbl));
            rptViewer.LocalReport.SubreportProcessing += FillTutoringActivityReportBatch_SubreportProcessing;
        }

        private void FillTutoringActivityReportBatch_SubreportProcessing(object Sender, SubreportProcessingEventArgs e)
        {
            var ta = new TutoringActivityTableAdapter();
            var tbl = new UKSTATSDSReports.TutoringActivityDataTable();

            DateTime dtFrom = DateTime.Parse(e.Parameters["DateRangeStart"].Values.First());
            DateTime dtTo = DateTime.Parse(e.Parameters["DateRangeEnd"].Values.First());
            Guid tID = Guid.Parse(e.Parameters["TutorID"].Values.First());
            Guid stID = Guid.Parse(e.Parameters["SessionTypeID"].Values.First());

            ta.Fill(tbl, dtFrom, dtTo, tID, stID);
            e.DataSources.Clear();

            if (tbl.Rows.Count == 0)
            {
                //DM - 10/17 - Issue ID 227 - To prevent blank pages and some tutors in a group not showing, add a new row with basic info set. In the report since the SessionID is null, it knows it is not a real row and shows a "No results for this tutor" label
                UKSTATSDSReports.TutoringActivityRow newRow = tbl.NewTutoringActivityRow();
                STATSModel.Resource tutor = STATSModel.Resource.Get(tID);
                newRow.ResourceNameFirst = tutor.ResourceNameFirst;
                newRow.ResourceNameLast = tutor.ResourceNameLast;
                newRow.SessionIsScheduled = false;
                newRow.SessionTime = DateTime.Now.ToShortTimeString();
                tbl.Rows.Add(newRow);
            }
            e.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorSkillsReport(Guid? rscID)
        {
            var ta = new TutorSkillsTableAdapter();
            var tbl = new UKSTATSDSReports.TutorSkillsDataTable();
            ta.Fill(tbl, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorSkills.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorSkills.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillUsageReport(DateTime? dtFrom, DateTime? dtTo, String xmlData, String sessionTypeIDs)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                lblError.Text = "No groups or resources selected.";
            }
            var ta = new UsageReportTableAdapter();
            var tbl = new UKSTATSDSReports.UsageReportDataTable();
            
            ta.Fill(tbl, dtFrom, dtTo, xmlData, (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null));
            rptViewer.LocalReport.ReportPath = "Reports\\UsageReport.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\UsageReport.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("xml", xmlData));
            rptViewer.LocalReport.SetParameters(new ReportParameter("SessionTypeID", (sessionTypeIDs.Length > 0 ? sessionTypeIDs : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutoringExceptionReport(DateTime? dtFrom, DateTime? dtTo, String xmlData)
        {
            var ta = new ExceptionReportTableAdapter();
            var tbl = new UKSTATSDSReports.ExceptionReportDataTable();
            ta.Fill(tbl, dtFrom, dtTo, xmlData);
            rptViewer.LocalReport.ReportPath = "Reports\\TutoringException.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutoringException.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeStart", (dtFrom.HasValue ? dtFrom.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.SetParameters(new ReportParameter("DateRangeEnd", (dtTo.HasValue ? dtTo.Value.ToShortDateString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        private void FillTutorSkillsReportByCourse(Guid? rscID)
        {
            var ta = new CourseTutorSkillsTableAdapter();
            var tbl = new UKSTATSDSReports.CourseTutorSkillsDataTable();
            ta.Fill(tbl, rscID);
            rptViewer.LocalReport.ReportPath = "Reports\\TutorSkillsByCourse.rdlc";
            rptViewer.LocalReport.ReportEmbeddedResource = "Reports\\TutorSkillsByCourse.rdlc";
            rptViewer.LocalReport.SetParameters(new ReportParameter("ResourceID", (rscID.HasValue ? rscID.ToString() : null)));
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("UKSTATSDataSet", (System.Data.DataTable)tbl));
        }

        #endregion

        protected void btnEmailReports_Click(object sender, EventArgs e)
        {
            var reportName = Request.QueryString["r"];
            if (String.IsNullOrEmpty(reportName))
                return;

            EmailReport(reportName);
            RenderReport(reportName);
            rpbReportParams.CollapseAllItems();
        }

        protected void btnRunReports_Click(object sender, EventArgs e)
        {
            String reportName = (Request.QueryString["r"]);

            if (String.IsNullOrEmpty(reportName))
            {
                return;
            }

            RenderReport(reportName);
            rpbReportParams.CollapseAllItems();
        }

        protected void btnPrintReport_Click(object sender, EventArgs e)
        {
            var reportName = Request.QueryString["r"];
            if (String.IsNullOrEmpty(reportName))
                return;

            var pdfSource = ReportToPDF(reportName);
            var outputStream = new MemoryStream();
            var pdfReader = new PdfReader(pdfSource);
            var pdfStamper = new PdfStamper(pdfReader, outputStream);
            
            //Add the auto-print javascript
            var writer = pdfStamper.Writer;
            PdfAction jAction = PdfAction.JavaScript("this.print(true);\r", writer);
            writer.AddJavaScript(jAction);
        
            //writer.AddJavaScript(GetAutoPrintJs());
            pdfStamper.Close();
            var content = outputStream.ToArray();
            outputStream.Close();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(content);
            Response.End();
            outputStream.Close();
            outputStream.Dispose();

        }

        

    }
}