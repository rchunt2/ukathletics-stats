﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="UK.STATS.Administration.Pages.MessagesPage" %>


<%--<%@ Register TagPrefix="SIS" TagName="Messaging" Src="~/UserControls/Messaging.ascx" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:Panel ID="panelResourceList" runat="server" Visible="False">
        <asp:DropDownList ID="ddlResources" runat="server" DataSourceID="edsResource" 
            DataTextField="ResourceNameDisplayLastFirst" DataValueField="ResourceID" 
            AppendDataBoundItems="True">
            <asp:ListItem Value="">All Resources</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnViewInbox" runat="server" Text="View Inbox" onclick="btnViewInbox_Click" />

        <asp:EntityDataSource ID="edsResource" runat="server" 
                ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
                EnableFlattening="False" EnableUpdate="True" EntitySetName="Resources" OrderBy="it.ResourceNameLast ASC, it.ResourceNameFirst ASC">
        </asp:EntityDataSource>
    </asp:Panel>

    <asp:Panel ID="panelMessageList" runat="server" Visible="False">
        <asp:Label ID="lblNoMessages" runat="server" Text="No Messages" Visible="false"></asp:Label>
        <asp:GridView ID="gvMessage" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CellPadding="4" 
            DataKeyNames="MessageInboxID" 
            ForeColor="#333333" GridLines="None" Width="100%" 
            onrowdatabound="gvMessage_RowDataBound" 
            onpageindexchanging="gvMessage_PageIndexChanging"
            onrowcommand="gvMessage_RowCommand" AllowSorting="True" OnSorting="gvMessage_Sorting">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="MessageInboxDateSent" DataFormatString="{0:d}" 
                    HeaderText="Date Sent" SortExpression="MessageInboxDateSent" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Subject"><%-- SortExpression="Message.GetSubject()"--%>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbSubject" runat="server" Text="Subject" CommandName="SortSubject" ForeColor="White"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSubject" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sender"><%-- SortExpression="Message.ResourceFrom.ResourceNameLast"--%>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbSender" runat="server" Text="Sender" CommandName="SortSender" ForeColor="White"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSender" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Recipient"><%-- SortExpression="Message.ResourceTo.ResourceNameLast"--%>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbRecipient" runat="server" Text="Recipient" CommandName="SortRecipient" ForeColor="White"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblRecipient" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField DataField="MessageInboxDateRead" DataFormatString="{0:d}" 
                    HeaderText="Date Read" SortExpression="MessageInboxDateRead" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MessageInboxDateExpires" DataFormatString="{0:d}" 
                    HeaderText="Expires On" SortExpression="MessageInboxDateExpires" >
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:HyperLinkField DataNavigateUrlFields="MessageInboxID" DataNavigateUrlFormatString="../Pages/Message.aspx?messageinboxid={0}" Text="View Details" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="panelMessageView" runat="server" Visible="False">
        <asp:HyperLink ID="HyperLink1" runat="server">Return To List</asp:HyperLink>
        <asp:DetailsView ID="dvMessage" runat="server" 
            Height="50px" Width="100%" AutoGenerateRows="False" CellPadding="4" 
            DataKeyNames="MessageInboxID" DataSourceID="edsMessageInboxView" 
            ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="MessageInboxDateSent" DataFormatString="{0:d}" 
                    HeaderText="Date Sent" />
                <asp:BoundField DataField="MessageInboxDateRead" DataFormatString="{0:d}" 
                    HeaderText="Date Read" ReadOnly="True" SortExpression="MessageInboxDateRead" />
                <asp:TemplateField HeaderText="Subject">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" 
                            Text='<%# Bind("Message.MessageContent.MessageContentSubject") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Content">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Message.MessageContent.MessageContentBody") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MessageInboxDateExpires" HeaderText="Expires On" 
                    DataFormatString="{0:d}" SortExpression="MessageInboxDateExpires" />
                <asp:CommandField />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:EntityDataSource ID="edsMessageInboxView" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
            EnableUpdate="True" EntitySetName="MessageInboxes" Include="Message.MessageContent" Where="(it.MessageInboxID == @queryMessageInboxID)">
            <WhereParameters>
                <asp:QueryStringParameter QueryStringField="messageinboxid" DbType="Guid" Name="queryMessageInboxID" ConvertEmptyStringToNull="True"/>
            </WhereParameters>
        </asp:EntityDataSource>
    </asp:Panel>
</asp:Content>