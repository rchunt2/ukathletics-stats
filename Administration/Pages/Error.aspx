﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="UK.STATS.Administration.Pages.Error" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataKeyNames="ErrorID" DataSourceID="edsError" 
        ForeColor="#333333" GridLines="None" Width="100%">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="ErrorDateTime" HeaderText="ErrorDateTime" SortExpression="ErrorDateTime" ItemStyle-Width="200px" />
            <asp:TemplateField HeaderText="ErrorText" SortExpression="ErrorText">
                <ItemTemplate>
                    <asp:TextBox ID="txtErrorText" runat="server" Rows="5" TextMode="MultiLine" Width="100%" Text='<%# Bind("ErrorText") %>' Wrap="False"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>

    <asp:EntityDataSource ID="edsError" runat="server" 
        ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
        EnableFlattening="False" EntitySetName="Errors" OrderBy="it.ErrorDateTime DESC" >
    </asp:EntityDataSource>

</asp:Content>
