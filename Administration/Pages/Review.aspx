﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Review.aspx.cs" Inherits="UK.STATS.Administration.Pages.Review" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="TimeReview" Src="~/UserControls/TimeReview.ascx" %>
<%@ Register TagPrefix="SIS" TagName="TimeTracker" Src="~/UserControls/TimeTracker.ascx" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSelectionSingle" Src="~/UserControls/ResourceSelectionSingle.ascx" %>



<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:ScriptManagerProxy ID="smpReview" runat="server">
    </asp:ScriptManagerProxy>

    <SIS:ResourceSelectionSingle ID="ResourceSelectionSingleModule" runat="server" />

    <asp:UpdatePanel ID="upTimeTracker" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <SIS:TimeTracker ID="TimeTrackerModule" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="upTimeReview" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <SIS:TimeReview ID="TimeReviewControl" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
