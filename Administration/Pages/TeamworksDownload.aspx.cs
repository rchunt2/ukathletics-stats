﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;
using ResourceType = UK.STATS.STATSModel.ResourceType;

namespace UK.STATS.Administration.Pages
{
    public partial class TeamworksDownload : UKBasePage
    {
        public STATSModel.Resource ResourceObj { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = STATSModel.ResourceType.Get("Administrator");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtDateFrom.SelectedDate = DateTime.Now;
                txtDateTo.SelectedDate = DateTime.Now.AddDays(180);
            }
        }

        protected void btnTeamworksDownload_Click(object sender, EventArgs e)
        {
            ExportCSV();
        }

        private void ExportCSV()
        {
            string constr = ConfigurationManager.ConnectionStrings["UKSTATSConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("TeamworksDownload", con ))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DateTime? dtFrom = txtDateFrom.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateFrom.ValidationDate) : null;
                        DateTime? dtTo = txtDateTo.SelectedDate.HasValue ? (DateTime?)DateTime.Parse(txtDateTo.ValidationDate) : null;

                        cmd.Parameters.AddWithValue("begin", dtFrom);
                        cmd.Parameters.AddWithValue("end", dtTo);
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);

                            //Build the CSV file data as a Comma separated string.
                            string csv = string.Empty;

                            foreach (DataColumn column in dt.Columns)
                            {
                                //Add the Header row for CSV file.
                                csv += column.ColumnName + ',';
                            }

                            //Add new line.
                            csv += "\r\n";

                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    //Add the Data rows.
                                    csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                                }

                                //Add new line.
                                csv += "\r\n";
                            }

                            //Download the CSV file.
                            Response.Clear();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=TeamworksDownload.csv");
                            Response.Charset = "";
                            Response.ContentType = "application/text";
                            Response.Output.Write(csv);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
            }
        }

        protected void btnSynchronize_Click(object sender, EventArgs e)
        {
            STATSModel.Teamworks.TeamworksAPI.SyncTeamworksIDs();
        }
    }
}