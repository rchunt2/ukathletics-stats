﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace UK.STATS.Administration.Pages
{
    public partial class SettingsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Request.QueryString["settingid"]))
            { // No resource type selected - List all resource types in a table.
                panelSettingsList.Visible = true;
                STATSModel.Setting[] eSettings = STATSModel.Setting.Get();
                
                gvSettingsList.AutoGenerateColumns = false;
                gvSettingsList.DataSource = eSettings;
                gvSettingsList.DataBind();
            }
            else
            { // Setting selected - Display Setting Information
                if (!Page.IsPostBack)
                {
                    panelSettingsView.Visible = true;
                    dvSettings.AutoGenerateInsertButton = false;
                }
            }
        }

        protected void gvSettingsList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSettingsList.PageIndex = e.NewPageIndex;
            gvSettingsList.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvSettings.ChangeMode(DetailsViewMode.Insert);
            panelSettingsList.Visible = false;
            panelSettingsView.Visible = true;
            dvSettings.AutoGenerateInsertButton = true;
        }

        protected void dvSettingsView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/Settings.aspx?settingid=" + e.Values["SettingID"], false);
        }

        protected void dvSettingsView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/Settings.aspx", false);
        }

        protected void dvSettingsView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/Settings.aspx", true);
            }
        }

        protected void dvSettingsView_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {

        }

        protected void gvSettingsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var gvSettingsList = (GridView)sender;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                STATSModel.Setting curSetting = (STATSModel.Setting)e.Row.DataItem;
                //((HyperLinkField)e.Row.FindControl("AttendanceID")).Text = ;
                String settingName = curSetting.SettingKey.ToString();
                if (settingName.ToLower().Contains("color"))
                {
                    ((Control)e.Row.Cells[2]).Visible = false;
                }
            }
        }
    }
}