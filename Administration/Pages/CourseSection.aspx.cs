﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.Administration.UserControls;

namespace UK.STATS.Administration.Pages
{
    public partial class CourseSectionPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            linkReturnToCourseList.Visible = false;

            if (!String.IsNullOrEmpty(Request.QueryString["coursesectionid"]))
            {
                Guid CourseSectionID = Guid.Parse(Request.QueryString["coursesectionid"]);
                STATSModel.CourseSection eCourseSection = STATSModel.CourseSection.Get(CourseSectionID);
                lblCourse.Text = "Course: " + eCourseSection.Course.Subject.SubjectNameShort + eCourseSection.Course.CourseNumber + " - " + eCourseSection.Course.CourseName;
                HyperLink1.NavigateUrl = "~/Pages/CourseSection.aspx?courseid=" + eCourseSection.CourseID.ToString();
                HyperLink2.NavigateUrl = "~/Pages/CourseSection.aspx?courseid=" + eCourseSection.CourseID.ToString();

                if (!String.IsNullOrEmpty(Request.QueryString["message"]))
                {
                    panelCourseSectionList.Visible = false;
                    panelCourseSectionView.Visible = false;
                    panelCourseSectionMessage.Visible = true;
                    STATSModel.Authentication eAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                    MessagingModule.FromResourceID = eAuthentication.ResourceID;
                    MessagingModule.RecipientID = CourseSectionID;
                    MessagingModule.Mode = Messaging.MessagingMode.Compose;
                    MessagingModule.TypeOfRecipient = Messaging.RecipientType.Professor;
                    MessagingModule.BindAndShow();
                }
                else
                {
                    // Details View for CourseSection
                    panelCourseSectionList.Visible = false;
                    panelCourseSectionView.Visible = true;
                    panelCourseSectionMessage.Visible = false;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(Request.QueryString["courseid"]))
                {
                    Response.Redirect("~/Pages/Subject.aspx", true);
                }
                else
                {
                    Guid CourseID = Guid.Parse(Request.QueryString["courseid"]);
                    STATSModel.Course eCourse = STATSModel.Course.Get(CourseID);
                    linkReturnToCourseList.NavigateUrl = "~/Pages/Course.aspx?subjectid=" + eCourse.SubjectID.ToString();
                    linkReturnToCourseList.Visible = true;
                    lblCourse.Text = "Course: " + eCourse.Subject.SubjectNameShort + eCourse.CourseNumber + " - " + eCourse.CourseName;
                    HyperLink1.NavigateUrl = "~/Pages/CourseSection.aspx?courseid=" + CourseID.ToString();

                    // List all Sections for Course
                    panelCourseSectionList.Visible = true;
                    panelCourseSectionView.Visible = false;
                    panelCourseSectionMessage.Visible = false;
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvCourseSection.ChangeMode(DetailsViewMode.Insert);
            panelCourseSectionList.Visible = false;
            panelCourseSectionView.Visible = true;
            panelCourseSectionMessage.Visible = false;
            dvCourseSection.AutoGenerateInsertButton = true;
            linkReturnToCourseList.Visible = false;
            panelInsertWarning.Visible = true;
            panelInsertWarning.ForeColor = System.Drawing.Color.Red;
        }

        protected void dvCourseSection_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["courseid"]))
                {
                    Guid CourseID = Guid.Parse(Request.QueryString["courseid"]);
                    Response.Redirect("~/Pages/CourseSection.aspx?courseid=" + CourseID.ToString(), true);
                }

                if (!String.IsNullOrEmpty(Request.QueryString["coursesectionid"]))
                {
                    Guid CourseSectionID = Guid.Parse(Request.QueryString["coursesectionid"]);
                    STATSModel.CourseSection eCourseSection = STATSModel.CourseSection.Get(CourseSectionID);
                    Response.Redirect("~/Pages/CourseSection.aspx?courseid=" + eCourseSection.CourseID.ToString(), true);
                }
                Response.Redirect("~/Pages/Subject.aspx", true);
            }
        }

        protected void dvCourseSection_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            string strMessage = "This Course Section has been Deleted.";
            string url = this.ResolveClientUrl("~/Pages/CourseSection.aspx?courseid=" + Request.QueryString["courseid"]);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteError", "<script language='javascript'>alert('" + strMessage + "');window.location='" + url + "';</script>", false);
        }

        protected void dvCourseSection_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var dv = (DetailsView)sender;
            var rcb = (Telerik.Web.UI.RadComboBox)dvCourseSection.FindControl("rcbBuilding");

            Guid CourseID = Guid.Parse(Request.QueryString["courseid"]);
            e.Values["CourseID"] = CourseID;
            e.Values["CourseSectionBuilding"] = rcb.Text;
            e.Values["CourseSectionID"] = Guid.NewGuid();
            e.Values["CourseSectionEventType"] = "LEC";

            e.Values["CourseSectionTimeStart"] = ((Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtTimeStart")).TextWithPromptAndLiterals;
            e.Values["CourseSectionTimeEnd"] = ((Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtTimeEnd")).TextWithPromptAndLiterals;

            String CourseSectionNumber = e.Values["CourseSectionNumber"].ToString();
            String daysMeet = e.Values["CourseSectionDaysMeet"].ToString();
            TimeSpan dtStartTime = TimeSpan.Parse((e.Values["CourseSectionTimeStart"].ToString()));
            TimeSpan dtEndTime = TimeSpan.Parse((e.Values["CourseSectionTimeEnd"].ToString()));
            Boolean exists = STATSModel.CourseSection.Exists(CourseID, CourseSectionNumber, dtStartTime, dtEndTime, daysMeet);
            if (exists)
            {
                e.Cancel = true;
                STATSModel.CourseSection eCourseSection = STATSModel.CourseSection.Get(CourseID, CourseSectionNumber);
                Response.Redirect("~/Pages/CourseSection.aspx?coursesectionid=" + eCourseSection.CourseSectionID + "&CourseID=" + eCourseSection.CourseID, true);
            }
        }

        protected void dvCourseSection_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/CourseSection.aspx?coursesectionid=" + e.Values["CourseSectionID"] + "&CourseID=" + e.Values["CourseID"], true);
        }

        protected void gvCourseSection_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var eCourseSection = (STATSModel.CourseSection)e.Row.DataItem;
                if (!eCourseSection.InstructorID.HasValue)
                {
                    TableCell tc = e.Row.Cells[9];
                    ControlCollection cc = tc.Controls;
                    foreach (Control c in cc)
                    {
                        c.Visible = false;
                    }
                }
            }
        }

        protected void dvCourseSection_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid CourseSectionID = (Guid)dv.DataKey.Value;

            var eCourseSection = STATSModel.CourseSection.Get(CourseSectionID);

            if (!eCourseSection.CanBeDeleted)
            {
                String strError = "This Course Section has Sessions or Resource Registrations associated with it and Cannot be Deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }

        protected void dvCourseSection_ModeChanged(object sender, EventArgs e)
        {
        }

        protected void dvCourseSection_DataBound(object sender, EventArgs e)
        {
            DetailsView dv = (DetailsView)sender;
            if (dv.CurrentMode == DetailsViewMode.Insert)
            {
                var txtDateStart = (TextBox)dv.FindControl("txtDateStart");
                var txtDateEnd = (TextBox)dv.FindControl("txtDateEnd");
                var txtDaysMeet = (TextBox)dv.FindControl("txtDaysMeet");
                var txtTimeStart = (Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtTimeStart");
                var txtTimeEnd = (Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtTimeEnd");

                txtTimeStart.Text = DateTime.Parse(STATSModel.Setting.GetByKey("SemesterDateBegin").SettingValue).ToShortDateString();

                txtDateStart.Text = DateTime.Parse(STATSModel.Setting.GetByKey("SemesterDateBegin").SettingValue).ToShortDateString();
                txtDateEnd.Text = DateTime.Parse(STATSModel.Setting.GetByKey("SemesterDateEnd").SettingValue).ToShortDateString();

                txtDaysMeet.Text = "MTWRF";

                txtTimeStart.Text = TimeSpan.FromHours(8).ToString();
                txtTimeEnd.Text = TimeSpan.FromHours(9).ToString();
            }
        }

        protected void dvCourseSection_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var dv = (DetailsView)sender;
            e.NewValues["CourseSectionTimeStart"] = ((Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtEditTimeStart")).TextWithPromptAndLiterals;
            e.NewValues["CourseSectionTimeEnd"] = ((Telerik.Web.UI.RadMaskedTextBox)dv.FindControl("txtEditTimeEnd")).TextWithPromptAndLiterals;
        }
    }
}