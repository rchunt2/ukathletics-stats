﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace UK.STATS.Administration.Pages
{
    public partial class Review : UKBasePage
    {
        public Guid? SingleResourceID { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResourceSelectionSingleModule.OnSelectionChanged += ResourceSelectionSingleModule_OnSelectionChanged;
            TimeReviewControl.OnFreeTimeUpdated += TimeReviewControl_OnFreeTimeUpdated;
            TimeReviewControl.OnTimeOverridden += TimeReviewControl_OnTimeOverridden;
        }

        void TimeReviewControl_OnTimeOverridden()
        {
            TimeTrackerModule.BindAndShow();
            upTimeTracker.Update();
        }

        void TimeReviewControl_OnFreeTimeUpdated()
        {
            TimeTrackerModule.BindAndShow();
            upTimeTracker.Update();
        }

        void ResourceSelectionSingleModule_OnSelectionChanged(UserControls.ResourceSelectionSingle.ResourceSelectionUpdatedEventArgs e)
        {
            SingleResourceID = e.SelectedResourceID;
            PrepareControls();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ResourceSelectionSingleModule.AllowNullSelection = true;
            }
        }

        private void PrepareControls()
        {
            if (SingleResourceID.HasValue)
            {
                Guid ResourceID = SingleResourceID.Value;
                TimeReviewControl.ResourceID = SingleResourceID.Value;
                TimeTrackerModule.ResourceID = SingleResourceID.Value;

                TimeReviewControl.BindAndShow();
                TimeTrackerModule.BindAndShow();
            }
            else
            {
                TimeReviewControl.Visible = false;
                TimeTrackerModule.Visible = false;
            }
            


        }
    }
}