﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    public partial class Professor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["instructorid"]))
            {
                Guid Instructorid = Guid.Parse(Request.QueryString["instructorid"]);
                STATSModel.Instructor eInstructor = STATSModel.Instructor.Get(Instructorid);
                HyperLink1.NavigateUrl = "~/Pages/Professor.aspx";

                // Details View for Professors
                panelInstructorList.Visible = false;
                panelInstructorView.Visible = true;
            }
            else
            {
                panelInstructorList.Visible = true;
                panelInstructorView.Visible = false;
                txtSearch.Focus();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvInstructor.ChangeMode(DetailsViewMode.Insert);
            panelInstructorList.Visible = false;
            panelInstructorView.Visible = true;
            dvInstructor.AutoGenerateInsertButton = true;
        }

        protected void dvInstructor_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["instructorid"]))
                {
                    Guid instructorid = Guid.Parse(Request.QueryString["instructorid"]);
                    Response.Redirect("~/Pages/Professor.aspx?instructorid=" + instructorid.ToString(), true);
                }
                Response.Redirect("~/Pages/Professor.aspx", true);
            }
        }

        protected void dvInstructor_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/Professor.aspx", true);
        }

        protected void dvInstructor_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            e.Values["InstructorID"] = Guid.NewGuid();
        }

        protected void dvInstructor_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/Professor.aspx?instructorid=" + e.Values["instructorid"], true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RunSearch();
        }

        private void RunSearch()
        {
            edsInstructorList.Where = "it.InstructorName LIKE '%" + txtSearch.Text + "%'";
            //Request.QueryString.Add("Search", txtSearch.Text);
            gvInstructor.DataBind();
        }

        protected void dvInstructor_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid InstructorID = (Guid)dv.DataKey.Value;

            var eInstructor = STATSModel.Instructor.Get(InstructorID);

            if (!eInstructor.CanBeDeleted)
            {
                String strError = "This Professor has activity associated with it and cannot be deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }

        protected void gvInstructor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvInstructor.PageIndex = e.NewPageIndex;
            RunSearch();
        }
    }
}