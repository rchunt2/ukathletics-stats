﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace UK.STATS.Administration.Pages
{
    public partial class SessionTypePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Request.QueryString["sessiontypeid"]))
            { // No session type selected - List all session types in a table.
                panelSessionTypeList.Visible = true;
                STATSModel.SessionType[] eSessionTypes = STATSModel.SessionType.Get();
                
                gvSessionTypeList.AutoGenerateColumns = false;
                gvSessionTypeList.DataSource = eSessionTypes;
                gvSessionTypeList.DataBind();
            }
            else
            { // Session types selected - Display Session Type Information
                if (!Page.IsPostBack)
                {
                    panelSessionTypeView.Visible = true;
                    dvSessionTypeView.AutoGenerateInsertButton = false;
                    SessionTypeDataSource.Select();
                    dvSessionTypeView.DataBind();
                }

            }

        }

        protected void gvSessionTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSessionTypeList.PageIndex = e.NewPageIndex;
            gvSessionTypeList.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvSessionTypeView.ChangeMode(DetailsViewMode.Insert);
            panelSessionTypeList.Visible = false;
            panelSessionTypeView.Visible = true;
            dvSessionTypeView.AutoGenerateInsertButton = true;
        }

        protected void dvSessionTypeView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/SessionType.aspx?sessiontypeid=" + e.Values["SessionTypeID"], false);
        }

        protected void dvSessionTypeView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/SessionType.aspx", false);
        }

        protected void dvSessionTypeView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/SessionType.aspx", true);
            }
        }
    }
}