﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Professor.aspx.cs" Inherits="UK.STATS.Administration.Pages.Professor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">
    <asp:Panel ID="panelInstructorList" runat="server" Visible="False" DefaultButton="btnSearch">
        <div>
            Professor Search: <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" /> <asp:Button ID="btnAdd" runat="server" Text="Create Instructor" onclick="btnAdd_Click" UseSubmitBehavior="False" />
        </div>
        <asp:GridView ID="gvInstructor" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataKeyNames="InstructorID" DataSourceID="edsInstructorList" 
            ForeColor="#333333" GridLines="None" style="margin-right: 0px" 
            Width="100%" AllowPaging="True" AllowSorting="True" 
            onpageindexchanging="gvInstructor_PageIndexChanging">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="InstructorName" HeaderText="Name" SortExpression="InstructorName"><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                <asp:BoundField DataField="InstructorEmail" HeaderText="Email" SortExpression="InstructorEmail"><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                <asp:BoundField DataField="InstructorPhone" HeaderText="Phone" SortExpression="InstructorPhone"><HeaderStyle HorizontalAlign="Left" /></asp:BoundField>
                <asp:HyperLinkField  DataNavigateUrlFields="InstructorID" DataNavigateUrlFormatString="../Pages/Professor.aspx?instructorid={0}" Text="View Details" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <asp:EntityDataSource ID="edsInstructorList" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableFlattening="False" EntitySetName="Instructors">
        </asp:EntityDataSource>
    </asp:Panel>

    <asp:Panel ID="panelInstructorView" runat="server" Visible="false">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/Professor.aspx">Return to List</asp:HyperLink>
        <asp:DetailsView ID="dvInstructor" runat="server" Height="50px" Width="100%" 
            AutoGenerateRows="False" CellPadding="4" DataKeyNames="InstructorID" 
            DataSourceID="edsInstructorView" ForeColor="#333333" GridLines="None"
            onitemcommand="dvInstructor_ItemCommand" 
            onitemdeleted="dvInstructor_ItemDeleted" 
            oniteminserted="dvInstructor_ItemInserted" 
            oniteminserting="dvInstructor_ItemInserting" 
            onitemdeleting="dvInstructor_ItemDeleting">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="InstructorName" HeaderText="Name" 
                    SortExpression="InstructorName" />
                <asp:BoundField DataField="InstructorEmail" HeaderText="Email" 
                    SortExpression="InstructorEmail" />
                <asp:BoundField DataField="InstructorPhone" HeaderText="Phone" 
                    SortExpression="InstructorPhone" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:EntityDataSource ID="edsInstructorView" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
            EnableUpdate="True" EntitySetName="Instructors" Where="(it.InstructorID == @queryInstructorID)">
            <WhereParameters>
                <asp:QueryStringParameter QueryStringField="instructorid" DbType="Guid" Name="queryInstructorID" ConvertEmptyStringToNull="True"/>
            </WhereParameters>
        </asp:EntityDataSource>
    </asp:Panel>

</asp:Content>
