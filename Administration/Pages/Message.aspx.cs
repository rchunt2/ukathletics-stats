﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;


namespace UK.STATS.Administration.Pages
{
    public partial class MessagesPage : System.Web.UI.Page
    {
        private String strResourceID;

        protected void Page_Load(object sender, EventArgs e)
        {
            strResourceID = Request.QueryString["resourceid"];
            String strMessageInboxID = Request.QueryString["messageinboxid"];
            lblNoMessages.Visible = false;

            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrWhiteSpace(strResourceID))
                {
                    Guid ResourceID = Guid.Parse(strResourceID);
                    ddlResources.SelectedValue = ResourceID.ToString();
                }
            }

            Boolean ValidResourceSupplied = false;

            if (!String.IsNullOrWhiteSpace(strResourceID))
            {
                try
                {
                    Guid ResourceID = Guid.Parse(strResourceID);
                    if (ResourceID != Guid.Empty)
                    {
                        ValidResourceSupplied = true;
                    }

                    STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceID);
                    MessageInbox[] datasource = STATSModel.MessageInbox.Get(eResource);
                    lblNoMessages.Visible = datasource.Length < 1;
                    gvMessage.DataSource = STATSModel.MessageInbox.Get(eResource);
                    gvMessage.DataBind();
                }
                catch (Exception)
                {
                }
            }
            if (!ValidResourceSupplied)
            {
                gvMessage.DataSource = STATSModel.MessageInbox.Get();
                gvMessage.DataBind();
            }

            if (!String.IsNullOrWhiteSpace(strMessageInboxID))
            {
                Guid MessageInboxID = Guid.Parse(Request.QueryString["messageinboxid"]);
                STATSModel.MessageInbox eMessageInbox = STATSModel.MessageInbox.Get(MessageInboxID);
                HyperLink1.NavigateUrl = "~/Pages/Message.aspx";

                // Details View for MessageInbox
                panelResourceList.Visible = false;
                panelMessageList.Visible = false;
                panelMessageView.Visible = true;
            }
            else
            {
                // Grid View for MessageInbox
                panelResourceList.Visible = true;
                panelMessageList.Visible = true;
                panelMessageView.Visible = false;
            }

        }

        private List<MessageInbox> getResourceInbox()
        {
            Boolean ValidResourceSupplied = false;
            List<MessageInbox> messageList = new List<MessageInbox>();

            if (!String.IsNullOrWhiteSpace(strResourceID))
            {
                try
                {
                    Guid ResourceID = Guid.Parse(strResourceID);
                    if (ResourceID != Guid.Empty)
                    {
                        ValidResourceSupplied = true;
                    }

                    STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceID);
                    MessageInbox[] datasource = STATSModel.MessageInbox.Get(eResource);
                    lblNoMessages.Visible = datasource.Length < 1;
                    messageList = STATSModel.MessageInbox.Get(eResource).ToList<MessageInbox>();
                }
                catch (Exception)
                {
                }
            }
            if (!ValidResourceSupplied)
            {
                messageList = STATSModel.MessageInbox.Get().ToList<MessageInbox>();
            }
            return messageList;
        }

        protected void gvMessage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblSubject = (Label)e.Row.Cells[1].FindControl("lblSubject");
                var lblSender = (Label) e.Row.Cells[2].FindControl("lblSender");
                var lblRecipient = (Label)e.Row.Cells[3].FindControl("lblRecipient");

                var eMessageInbox = (STATSModel.MessageInbox)e.Row.DataItem;

                lblSubject.Text = eMessageInbox.Message.GetSubject();
                lblSender.Text = eMessageInbox.Message.ResourceFrom.ResourceNameDisplayLastFirst;
                lblRecipient.Text = eMessageInbox.Message.ResourceTo.ResourceNameDisplayLastFirst;
            }
        }

        protected void btnViewInbox_Click(object sender, EventArgs e)
        {
            String QueryString = Request.Url.Query;
            Boolean HasQueryString = QueryString.Length > 0;

            String url = Request.Url.OriginalString;

            if (HasQueryString)
            {
                url = url.Replace(QueryString, "");
            }

            url += "?resourceid=" + ddlResources.SelectedValue;
            Response.Redirect(url, true);
        }

        protected void gvMessage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["sorting"] != null)
            {
                String sortExpression = Session["sorting"].ToString();
                if (!string.IsNullOrEmpty(sortExpression))
                {
                    List<MessageInbox> messageList = sortMessages(sortExpression);
                    gvMessage.DataSource = messageList;
                }
            }
            gvMessage.PageIndex = e.NewPageIndex;
            gvMessage.DataBind();
        }

        protected void gvMessage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            List<MessageInbox> messageList = getResourceInbox();

            switch (e.CommandName)
            {
                case "SortSubject":
                    //currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Tutor.ResourceNameLast.CompareTo(y.Tutor.ResourceNameLast); });
                    //List<STATSModel.Attendance> sortUs2 = eAttendances.ToList<STATSModel.Attendance>();
                    //var temp = eAttendances[0].Schedule.Session.SessionDateTimeStart;
                    //var temp2 = eAttendances[1].Schedule.Session.SessionDateTimeStart;
                    //sortUs2.Sort(delegate(STATSModel.Attendance x, STATSModel.Attendance y) { return x.Schedule.Session.SessionDateTimeStart.CompareTo(y.Schedule.Session.SessionDateTimeStart); });
                    messageList.Sort(delegate(MessageInbox x,MessageInbox y) { return x.Message.GetSubject().ToString().CompareTo(y.Message.GetSubject().ToString());});
                    gvMessage.DataSource = messageList;
                    //gvSystemLoggedIn.DataSource = sortUs2;
                    gvMessage.DataBind();
                    Session["sorting"] = e.CommandName;
                    break;
                case "SortSender":
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.Message.ResourceFrom.ResourceNameLast.ToString().CompareTo(y.Message.ResourceFrom.ResourceNameLast.ToString()); });
                    //messageList.Sort((x, y) => y.Message.ResourceFrom.ResourceNameLast.ToString().CompareTo(x.Message.ResourceFrom.ResourceNameLast.ToString()));
                    //messageList.OrderBy(obj => obj.Message.ResourceFrom.ResourceNameLast).ThenBy(obj => obj.Message.ResourceFrom.ResourceNameFirst);
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    Session["sorting"] = e.CommandName;
                    break;
                case "SortRecipient":
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.Message.ResourceTo.ResourceNameLast.ToString().CompareTo(y.Message.ResourceTo.ResourceNameLast.ToString()); });
                    //messageList.OrderBy(obj => obj.Message.ResourceTo.ResourceNameLast).ThenBy(obj => obj.Message.ResourceTo.ResourceNameFirst);
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    Session["sorting"] = e.CommandName;
                    break;
                default:
                    break;
            }
        }

        protected void gvMessage_Sorting(object sender, GridViewSortEventArgs e)
        {
            List<MessageInbox> messageList = getResourceInbox();

            switch (e.SortExpression)
            {
                case "MessageInboxDateSent":
                    messageList.OrderBy(obj => obj.MessageInboxDateSent);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateSent.CompareTo(y.MessageInboxDateSent); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    Session["sorting"] = e.SortExpression;
                    break;
                case "MessageInboxDateRead":
                    messageList.OrderBy(obj => obj.MessageInboxDateRead);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateRead.ToLongDateString().CompareTo(y.MessageInboxDateRead.ToLongDateString()); });
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateRead.ToString().CompareTo(y.MessageInboxDateRead.ToString()); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    Session["sorting"] = e.SortExpression;
                    break;
                case "MessageInboxDateExpires":
                    //messageList.OrderBy(obj => obj.MessageInboxDateExpires);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateExpires.ToShortDateString().CompareTo(y.MessageInboxDateExpires.ToShortDateString()); });
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { var tempX = (DateTime)x.MessageInboxDateExpires; var tempY = (DateTime)y.MessageInboxDateExpires; return tempY.CompareTo(tempX); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    Session["sorting"] = e.SortExpression;
                    break;
            }
        }

        protected List<MessageInbox> sortMessages(String sortExpression)
        {
            List<MessageInbox> messageList = getResourceInbox();

            switch(sortExpression)
            {
                case "MessageInboxDateSent":
                    messageList.OrderBy(obj => obj.MessageInboxDateSent);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateSent.CompareTo(y.MessageInboxDateSent); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    break;
                case "MessageInboxDateRead":
                    messageList.OrderBy(obj => obj.MessageInboxDateRead);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateRead.ToLongDateString().CompareTo(y.MessageInboxDateRead.ToLongDateString()); });
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateRead.ToString().CompareTo(y.MessageInboxDateRead.ToString()); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    break;
                case "MessageInboxDateExpires":
                    //messageList.OrderBy(obj => obj.MessageInboxDateExpires);
                    //messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.MessageInboxDateExpires.ToShortDateString().CompareTo(y.MessageInboxDateExpires.ToShortDateString()); });
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { var tempX = (DateTime)x.MessageInboxDateExpires; var tempY = (DateTime)y.MessageInboxDateExpires; return tempY.CompareTo(tempX); });
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    break;
                case "SortSubject":
                    //currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Tutor.ResourceNameLast.CompareTo(y.Tutor.ResourceNameLast); });
                    //List<STATSModel.Attendance> sortUs2 = eAttendances.ToList<STATSModel.Attendance>();
                    //var temp = eAttendances[0].Schedule.Session.SessionDateTimeStart;
                    //var temp2 = eAttendances[1].Schedule.Session.SessionDateTimeStart;
                    //sortUs2.Sort(delegate(STATSModel.Attendance x, STATSModel.Attendance y) { return x.Schedule.Session.SessionDateTimeStart.CompareTo(y.Schedule.Session.SessionDateTimeStart); });
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.Message.GetSubject().ToString().CompareTo(y.Message.GetSubject().ToString()); });
                    gvMessage.DataSource = messageList;
                    //gvSystemLoggedIn.DataSource = sortUs2;
                    gvMessage.DataBind();
                    break;
                case "SortSender":
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.Message.ResourceFrom.ResourceNameLast.ToString().CompareTo(y.Message.ResourceFrom.ResourceNameLast.ToString()); });
                    //messageList.Sort((x, y) => y.Message.ResourceFrom.ResourceNameLast.ToString().CompareTo(x.Message.ResourceFrom.ResourceNameLast.ToString()));
                    //messageList.OrderBy(obj => obj.Message.ResourceFrom.ResourceNameLast).ThenBy(obj => obj.Message.ResourceFrom.ResourceNameFirst);
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    break;
                case "SortRecipient":
                    messageList.Sort(delegate(MessageInbox x, MessageInbox y) { return x.Message.ResourceTo.ResourceNameLast.ToString().CompareTo(y.Message.ResourceTo.ResourceNameLast.ToString()); });
                    //messageList.OrderBy(obj => obj.Message.ResourceTo.ResourceNameLast).ThenBy(obj => obj.Message.ResourceTo.ResourceNameFirst);
                    gvMessage.DataSource = messageList;
                    gvMessage.DataBind();
                    break;
                default:
                    break;
            }
            return messageList;
        }
    }
}