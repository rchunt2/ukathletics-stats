﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    public partial class GroupPage : UKBasePage
    {
        public STATSModel.Group GroupObj { get; set; }

        public STATSModel.ResourceType ResourceTypeMonitor
        {
            get
            {
                if (Application["ResourceTypeMonitor"] == null)
                {
                    Application["ResourceTypeMonitor"] = STATSModel.ResourceType.Get("Monitor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeMonitor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeTimeMgr
        {
            get
            {
                if (Application["ResourceTypeTimeMgr"] == null)
                {
                    Application["ResourceTypeTimeMgr"] = STATSModel.ResourceType.Get("Time Mgr");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTimeMgr"];
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            GroupSearchControl.ButtonAddEvent += new UK.STATS.Administration.UserControls.GroupSearch.ButtonAddDelegate(ShowCreateControls);
            GroupMemberAdministrationControl.OnGroupMembersChanged += GroupMemberAdministrationControl_OnGroupMembersChanged;
        }

        private void GroupMemberAdministrationControl_OnGroupMembersChanged()
        {
            //ResourceScheduleControl.RunScheduler();
        }

        protected void RefreshSchedule()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["groupid"]))
            {
                Guid GroupID = Guid.Parse(Request.QueryString["groupid"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            STATSModel.Authentication auth = GetCurrentAuthentication();
            bool readOnly = false;

            if (auth != null && auth.Resource.ResourceTypeID == ResourceTypeMonitor.ResourceTypeID)
            {
                readOnly = true;
                //This page should be read only for monitors!
                this.GroupSearchControl.IsReadOnly = true;
                dvGroup.Enabled = false;
                this.GroupMemberAdministrationControl.IsReadOnly = true;
                this.CourseRegistrationControl.IsReadOnly = true;
                this.tabSendMessage.Enabled = false;
            }

            if (auth != null && auth.Resource.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID)
            {
                //readOnly = true;
                dvGroup.AutoGenerateInsertButton = false;
                //dvGroup.Enabled = false;

            }

            if (String.IsNullOrEmpty(Request.QueryString["groupid"]))
            {
                HyperLink1.Visible = false;
                lblGroupName.Visible = false;
            }
            else
            {
                GroupSearchControl.Visible = false;

                Guid GroupID = Guid.Parse(Request.QueryString["groupid"]);
                GroupObj = STATSModel.Group.Get(GroupID);
                HyperLink1.Visible = true;

                lblGroupName.Text = "Viewing Group: " + GroupObj.GroupName;
                lblGroupName.Visible = true;

                // Schedule Summary Module
                ScheduleSummaryModule.GroupID = GroupID;

                // Course Registration Module
                CourseRegistrationControl.Mode = UserControls.CourseRegistration.RegistrationMode.Group;
                CourseRegistrationControl.ObjectID = GroupObj.GroupID;
                CourseRegistrationControl.Visible = true;
                CourseRegistrationControl.BindAndShow();

                // Schedule Module
                if (!Page.IsPostBack)
                {
                    ResourceScheduleControl.GroupID = GroupObj.GroupID;
                    if (!readOnly)
                    {
                        //Only set Admin mode if not read only (monitor security group)
                        ResourceScheduleControl.Admin = true;
                    }
                    ResourceScheduleControl.RunScheduler(false);
                }

                // Messaging Module
                MessagingModule.TypeOfRecipient = UK.STATS.Administration.UserControls.Messaging.RecipientType.Group;
                MessagingModule.RecipientID = GroupObj.GroupID;
                if (Session["AuthenticationKey"] != null) { MessagingModule.FromResourceID = auth.ResourceID; }
                MessagingModule.Mode = UK.STATS.Administration.UserControls.Messaging.MessagingMode.Compose;
                MessagingModule.BindAndShow();

                // Group Member Administration Module
                GroupMemberAdministrationControl.GroupID = GroupObj.GroupID;
                GroupMemberAdministrationControl.BindAndShow();

                // Set visibility on page elements...
                panelGroupScreen.Visible = true;
                panelGroupView.Visible = true;
                dvGroup.Visible = true;
                dvGroup.AutoGenerateInsertButton = false;

                string url = Request.Url.GetLeftPart(UriPartial.Authority) + this.ResolveUrl("~/PrintSchedule.aspx?groupid=" + GroupObj.GroupID.ToString());
                HyperlinkPrintSchedule.NavigateUrl = url;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ShowCreateControls();
        }

        private void ShowCreateControls()
        {
            dvGroup.ChangeMode(DetailsViewMode.Insert);
            dvGroup.AutoGenerateInsertButton = true;

            panelGroupView.Visible = true;

            GroupSearchControl.Visible = false;
            panelGroupScreen.Visible = false;
            GroupMemberAdministrationControl.Visible = false;
            CourseRegistrationControl.Visible = false;
            ResourceScheduleControl.Visible = false;
            MessagingModule.Visible = false;
        }

        protected void dvGroup_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            e.Values["GroupID"] = Guid.NewGuid();
        }

        protected void dvGroupView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect("~/Pages/Group.aspx?groupid=" + e.Values["GroupID"], false);
        }

        protected void dvGroupView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/Group.aspx", false);
        }

        protected void dvGroupView_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/Group.aspx", true);
            }
        }

        protected void dvGroup_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid GroupID = (Guid)dv.DataKey.Value;

            var eGroup = STATSModel.Group.Get(GroupID);

            if (!eGroup.CanBeDeleted)
            {
                String strError = "The Group has activity associated with it and cannot be deleted.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;
            }
        }

        protected void tabGroupMenu_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (e.Tab.Text == "Schedule")
            {
                ResourceScheduleControl.RunScheduler(false);
            }
        }

        protected void dvGroup_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            STATSModel.Authentication auth = GetCurrentAuthentication();

            if (e.NewMode == DetailsViewMode.Insert)
            {
                if (auth != null && auth.Resource.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID)
                {
                    string test = "";
                    
                    //bool primaryGroup = ((CheckBox)dvGroup.Rows[0].Cells[2].FindControl("PrimaryGroup")).Checked;
                    //dvGroup.Rows[0].Cells[0].Enabled = false;
                    //dvGroup.Rows[0].Cells[1].Enabled = false;
                    //dvGroup.Rows[0].Cells[2].Enabled = false;
                    //dvGroup.Rows[0].Cells[3].Enabled = false;
                }
            }
        
            //string MyOrderNumber = Order_DetailsView.Rows[0].Cells[0].Text;
        }

        protected void dvGroup_DataBound(object sender, EventArgs e)
        {
            STATSModel.Authentication auth = GetCurrentAuthentication();
            UK.STATS.STATSModel.Group loadedGroup = (UK.STATS.STATSModel.Group)dvGroup.DataItem;

            if (auth != null && auth.Resource.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID)
            {
                dvGroup.Rows[3].Cells[1].Enabled = false;
                if (loadedGroup.PrimaryGroup == true) 
                {
                    dvGroup.Enabled = false;
                }
                else
                {
                    dvGroup.Enabled = true;
                }
            }
        }
    }
}