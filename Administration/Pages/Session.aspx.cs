﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace UK.STATS.Administration.Pages
{
    public partial class SessionPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Request.QueryString["sessionid"]))
            { // No Session selected - List all Sessions in a table.
                panelSessionList.Visible = true;
                STATSModel.Session[] eSessions = STATSModel.Session.Get();
                
                gvSessionList.AutoGenerateColumns = false;
                gvSessionList.DataSource = eSessions;
                gvSessionList.DataBind();
            }
            else
            { // Session selected - Display Session Information
                panelSessionView.Visible = true;
                dvSessionView.AutoGenerateInsertButton = false;
                SessionDataSource.Select();
                dvSessionView.DataBind();

            }

        }

        protected void gvSessionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSessionList.PageIndex = e.NewPageIndex;
            gvSessionList.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            dvSessionView.ChangeMode(DetailsViewMode.Insert);
            panelSessionList.Visible = false;
            panelSessionView.Visible = true;
            dvSessionView.AutoGenerateInsertButton = true;
        }

        protected void dvSessionView_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {

            Response.Redirect("~/Pages/Session.aspx", false);
        }

        //needs updating functionality

        //protected void SessionDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        //{

        //}
    }
}
