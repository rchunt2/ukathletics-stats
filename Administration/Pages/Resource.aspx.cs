﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;
using ResourceType = UK.STATS.STATSModel.ResourceType;

namespace UK.STATS.Administration.Pages
{
    public partial class ResourcePage : UKBasePage
    {
        public STATSModel.Resource ResourceObj { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResourceSearchControl.ButtonAddEvent += ShowCreateControls;
            CourseRegistrationControl.ButtonAddEvent += CourseRegistrationControl_ButtonAddEvent;
            ResourceRegistrationSummaryControl.ButtonAddEvent += CourseRegistrationControl_ButtonAddEvent;
            TutoringMultiRegistrationControl.ButtonAddEvent += TutoringMultiRegistrationControl_ButtonAddEvent;
            TutoringSkillsModule.OnTutoringSkillsChanged += TutoringSkillsModule_OnTutoringSkillsChanged;
            ResourceGroupMembershipControl.OnGroupsChanged += ResourceGroupMembershipControl_OnGroupsChanged;
            ResourceGroupMembershipControl.OnActiveChanged += ResourceGroupMembershipControl_OnActiveChanged;
        }

        void ResourceGroupMembershipControl_OnGroupsChanged()
        {
            ResourceRegistrationSummaryControl.BindAndShow();
            ResourceScheduleControl.RunScheduler(false);
            Response.Redirect("Resource.aspx?resourceid=" + this.ResourceObj.ResourceID);
        }

        void ResourceGroupMembershipControl_OnActiveChanged()
        {
            Response.Redirect("Resource.aspx?resourceid=" + this.ResourceObj.ResourceID);

        }
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public STATSModel.ResourceType ResourceTypeDirector
        {
            get
            {
                if (Application["ResourceTypeDirector"] == null)
                {
                    Application["ResourceTypeDirector"] = STATSModel.ResourceType.Get("Director");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeDirector"];
            }
        }

        public STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = STATSModel.ResourceType.Get("Administrator");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        public STATSModel.ResourceType ResourceTypeTimeMgr
        {
            get
            {
                if (Application["ResourceTypeTimeMgr"] == null)
                {
                    Application["ResourceTypeTimeMgr"] = STATSModel.ResourceType.Get("Time Mgr");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTimeMgr"];
            }
        }

        public STATSModel.ResourceType ResourceTypeMonitor
        {
            get
            {
                if (Application["ResourceTypeMonitor"] == null)
                {
                    Application["ResourceTypeMonitor"] = STATSModel.ResourceType.Get("Monitor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeMonitor"];
            }
        }

        public string ResourceGroupName
        {
            get; set;
        }


        private STATSModel.Authentication curAuthentication;
        private STATSModel.Resource curResource;
        private STATSModel.ResourceType curResourceType;

        protected void Page_Load(object sender, EventArgs e)
        {
            Boolean HasResourceID = (!String.IsNullOrEmpty(Request.QueryString["resourceid"]));
            Boolean IsLoggedIn = Helpers.UKHelper.LoggedIn();
            Boolean HasSearchText = (!String.IsNullOrEmpty(Request.QueryString["st"]));
            Boolean HasGroupID = (!String.IsNullOrEmpty(Request.QueryString["sg"]));
            Boolean HasResourceTypeID = (!String.IsNullOrEmpty(Request.QueryString["srt"]));

            if (curAuthentication == null)
            {
                curAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                if (curAuthentication == null)
                {
                    return;
                }
                curResource = curAuthentication.Resource;
                curResourceType = curResource.ResourceType;
            }

            lblInsertError.Text = "";

            if (HasSearchText)
            {
                String SearchText = Request.QueryString["st"];
                ResourceSearchControl.QueryString = SearchText;
            }

            if (HasGroupID)
            {
                Guid GroupID = Guid.Parse(Request.QueryString["sg"]);
                ResourceSearchControl.GroupID = GroupID;
            }

            if (HasResourceTypeID)
            {
                Guid ResourceTypeID = Guid.Parse(Request.QueryString["srt"]);
                ResourceSearchControl.ResourceTypeID = ResourceTypeID;
            }

            String ReturnPageUrl = Request.FilePath;
            List<String> QueryParams = new List<String>();
            if (HasSearchText) { QueryParams.Add("st=" + ResourceSearchControl.QueryString); }
            if (HasGroupID) { QueryParams.Add("sg=" + ResourceSearchControl.GroupID.ToString()); }
            if (HasResourceTypeID) { QueryParams.Add("srt=" + ResourceSearchControl.ResourceTypeID.ToString()); }

            int max = QueryParams.Count;
            for (int i = 0; i < max; i++)
            {
                if (i == 0) { ReturnPageUrl += "?"; } // query parameters always begin with a question mark
                ReturnPageUrl += QueryParams[i]; // append our parameter
                if (i + 1 < max) { ReturnPageUrl += "&"; } // if this isnt the last parameter, put an ampersand to denote the presence of the following parameter
            }
            linkReturn.NavigateUrl = ReturnPageUrl;

            if (HasResourceID && IsLoggedIn)
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
                ResourceObj = STATSModel.Resource.Get(ResourceID, true);
                if (ResourceObj == null)
                {
                    //If we couldn't find the resource for this ID, redirect to search
                    Response.Redirect("~/Pages/Resource.aspx", true);
                }
                // Now, we'll need to conditionally bind certain modules.
                // I assume that it would be wise to also hide the tabs that are not relevant to certain ResourceTypes.
                ResourceType eResourceType = ResourceObj.ResourceType;

                Boolean isTutor = (eResourceType.ResourceTypeID == ResourceTypeTutor.ResourceTypeID);
                Boolean isStudent = (eResourceType.ResourceTypeID == ResourceTypeStudent.ResourceTypeID);

                RadTabCollection tabs = tabResourceMenu.Tabs;
                foreach (RadTab radTab in tabs)
                {
                    switch (radTab.Text)
                    {
                        case "Info":
                            radTab.Visible = true; // The Information tab is visible for ALL ResourceTypes.

                            if (!(isTutor || isStudent) && !Page.IsPostBack)
                            {
                                radTab.Selected = true;
                            }
                            break;

                        case "Membership":
                            radTab.Visible = true; // The Membership tab is visible for ALL ResourceTypes.
                            break;

                        case "Time Tracker":
                            radTab.Visible = (isTutor || isStudent); // Time Tracking is only applicable to Students and Tutors
                            break;

                        case "Schedule":
                            radTab.Visible = (isTutor || isStudent); // Only Students and Tutors can have a Schedule
                            if ((isTutor || isStudent) && !Page.IsPostBack)
                            {
                                tabResourceMenu.SelectedIndex = 3;
                                tabResourcePages.SelectedIndex = 3;
                                radTab.Selected = true;
                            }
                            break;

                        case "Send Message":
                            radTab.Visible = (isTutor || isStudent); // You can only send messages to Students and Tutors this way.
                            break;

                        case "Picture":
                            radTab.Visible = true; //Picture is visible for ALL ResourceTypes
                            break;

                        default:
                            radTab.Visible = false;
                            break;
                    }
                }

                if (!(isTutor || isStudent) && !Page.IsPostBack)
                {
                    tabResourceMenu.SelectedIndex = 0;
                    tabResourcePages.SelectedIndex = 0;
                }



                if (!Page.IsPostBack)
                {
                    // Resource Registration Summary
                    ResourceRegistrationSummaryControl.ResourceObject = ResourceObj;
                    ResourceRegistrationSummaryControl.Visible = true;
                    panelResourceRegistrationSummaryModule.Visible = true;
                    ResourceRegistrationSummaryControl.BindAndShow();

                    // Display the name of the selected Resource.
                    lblResourceName.Visible = true;
                    lblResourceName.Text = "Viewing Resource: " + ResourceObj.ResourceNameDisplayLastFirst;

                    lblResourceInactive.Visible = false;
                    lblInactiveMessage.Visible = false;
                    if (!ResourceObj.ResourceIsActive.GetValueOrDefault())
                    {
                        lblResourceInactive.Visible = true;
                        lblInactiveMessage.Visible = true;
                    }


                    // Modules that display for both Students AND Tutors.
                    if (isStudent || isTutor)
                    {
                        // Daily Schedule + Print Schedule Link
                        HyperlinkPrintSchedule.NavigateUrl = "../PrintSchedule.aspx?resourceid=" + ResourceObj.ResourceID.ToString();
                        ResourceScheduleControl.ResourceID = ResourceObj.ResourceID;
                        ResourceScheduleControl.Printing = false;
                        ResourceScheduleControl.Admin = true;
                        ResourceScheduleControl.Visible = true;
                        panelResourceSchedule.Visible = true;
                        ResourceScheduleControl.RunScheduler(false);

                        // Time Tracker
                        TimeTrackerModule.ResourceID = ResourceObj.ResourceID;
                        TimeTrackerModule.IsVisible = true;
                    }

                    // Modules that ONLY display for Tutors.
                    if (isTutor)
                    {
                        // Tutoring
                        TutoringSkillsModule.ResourceID = ResourceObj.ResourceID;
                        TutoringSkillsModule.IsVisible = true;
                        TutoringSkillsModule.BindAndShow();
                        panelTutorSkills.Visible = true;

                        // Multiple Resource Tutoring Registration Control
                        TutoringMultiRegistrationControl.ResourceObj = ResourceObj;
                        TutoringMultiRegistrationControl.BindAndShow();
                        panelTutorMulti.Visible = true;
                    }

                    // Modules that ONLY display for Students.
                    if (isStudent)
                    {
                        // Course Registration
                        CourseRegistrationControl.Mode = UserControls.CourseRegistration.RegistrationMode.Resource;
                        CourseRegistrationControl.ObjectID = ResourceObj.ResourceID;
                        CourseRegistrationControl.IsVisible = true;
                        CourseRegistrationControl.BindAndShow();
                    }

                    // Messaging
                    MessagingModule.Mode = UserControls.Messaging.MessagingMode.Compose;
                    MessagingModule.RecipientID = ResourceObj.ResourceID;
                    MessagingModule.TypeOfRecipient = UserControls.Messaging.RecipientType.Resource;
                    MessagingModule.FromResourceID = GetCurrentAuthentication().ResourceID;
                    MessagingModule.IsVisible = true;
                    MessagingModule.BindAndShow();

                    //ResourcePictureModule
                    ResourcePictureEditModule.ResourceID = ResourceObj.ResourceID;

                    // Resource Group Membership
                    ResourceGroupMembershipControl.ResourceObj = ResourceObj;
                    ResourceGroupMembershipControl.BindAndShow();

                    // Set proper visibility on the panels.
                    ResourceSearchControl.isVisible = false;
                    panelResourceScreen.Visible = true;
                    panelResourceInfo.Visible = true;
                    panelResourceGroups.Visible = true;
                    panelResourceTime.Visible = true;
                }
            }
            else // No resource selected, so this is the search controls?
            {
                ResourceSearchControl.RunSearch();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ShowCreateControls();
        }

        private void ShowCreateControls()
        {
            dvResource.Visible = true;
            panelResourceInfo.Visible = true;
            dvResource.ChangeMode(DetailsViewMode.Insert);
            panelResourceSchedule.Visible = false;
            panelResourceGroups.Visible = false;
            panelResourceScreen.Visible = false;
            dvResource.AutoGenerateInsertButton = true;
            ResourceSearchControl.Visible = false;
            panelResourceInfo.Visible = true;
            tabResourceMenu.SelectedIndex = 0;
            tabResourcePages.SelectedIndex = 0;
            tabResourcePages.Visible = true;


            RadTabCollection tabs = tabResourceMenu.Tabs;
            foreach (RadTab radTab in tabs)
            {
                if (radTab.Text == "Info")
                {
                    //Set the selected tab as the info tab
                    radTab.Visible = true;
                    radTab.Selected = true;
                    break;
                }
            }
        }

        protected void dvResource_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
        }

        protected void dvResource_OnItemInserting(object Sender, DetailsViewInsertEventArgs e)
        {
            System.Threading.Thread.Sleep(3000);
            if (curAuthentication == null)
            {
                curAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                curResource = curAuthentication.Resource;
                curResourceType = curResource.ResourceType;
            }
            if (curResourceType.ResourceTypeName != ResourceTypeDirector.ResourceTypeName && curResourceType.ResourceTypeName != ResourceTypeAdministrator.ResourceTypeName && curResourceType.ResourceTypeName != ResourceTypeTimeMgr.ResourceTypeName)
            {
                lblInsertError.Text = "You do not have sufficient rights to add resources.";
                e.Cancel = true;
            }
            else
            {
                var addedResourceType = (string)e.Values["ResourceTypeID"];
                if (curResourceType.ResourceTypeName != ResourceTypeDirector.ResourceTypeName && (addedResourceType == ResourceTypeAdministrator.ResourceTypeID.ToString() || addedResourceType == ResourceTypeDirector.ResourceTypeID.ToString() || addedResourceType == ResourceTypeTimeMgr.ResourceTypeID.ToString()))
                {
                    lblInsertError.Text = "You do not have sufficient rights to add a resource of this type.";
                    e.Cancel = true;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace((string)e.Values["ResourceNameLast"]))
                    {
                        lblInsertError.Text = "You must enter a value for Resource Last Name";
                        e.Cancel = true;
                    }
                    else if (string.IsNullOrWhiteSpace((string)e.Values["ResourceNameFirst"]))
                    {
                        lblInsertError.Text = "You must enter a value for Resource First Name";
                        e.Cancel = true;
                    }
                    else if (string.IsNullOrWhiteSpace((string)e.Values["ResourceLogin"]))
                    {
                        lblInsertError.Text = "You must enter a value for Resource Login";
                        e.Cancel = true;
                    }
                    else if (addedResourceType == ResourceTypeStudent.ResourceTypeID.ToString() || addedResourceType == ResourceTypeTutor.ResourceTypeID.ToString())
                    {
                        e.Values["ResourceIsActive"] = false;
                        e.Values["ResourceID"] = Guid.NewGuid();

                        //Get the Group and insert the item
                        DropDownList ddlGroup = (DropDownList)dvResource.FindControl("ddlGroup");
                        if (ddlGroup.SelectedIndex != 0)
                        {
                            ResourceGroupName = ddlGroup.SelectedItem.Text;
                        }
                        else
                        {
                            lblInsertError.Text = "You must select a Primary Group";
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        e.Values["ResourceIsActive"] = true;
                        e.Values["ResourceID"] = Guid.NewGuid(); //this actually doesnt do anything, it will get overwritten in the entity data source insert, i think its because of the querystring parameter binding
                    }
                }
            }
        }


        protected void eResource_Inserting(object sender, EntityDataSourceChangingEventArgs e)
        {
        }


        protected void eResource_Inserted(object sender, EntityDataSourceChangedEventArgs e)
        {
            //Get the ResourceID and GroupID and insert the group if necessary
            if(!string.IsNullOrEmpty(ResourceGroupName))
            {
                STATSModel.Group GroupObj = STATSModel.Group.Get(ResourceGroupName);
                GroupObj.AddMember(((STATSModel.Resource)e.Entity));

                //Add any Group Sessions to the individual's Schedule
                if (GroupObj.Sessions.Count > 0)
                {
                    var context = new STATSEntities();
                    foreach (STATSModel.Session SessionObj in GroupObj.Sessions)
                    {
                        var ScheduleObj = new STATSModel.Schedule();
                        ScheduleObj.ScheduleID = Guid.NewGuid();
                        ScheduleObj.ResourceID = ((STATSModel.Resource)e.Entity).ResourceID;
                        ScheduleObj.SessionID = SessionObj.SessionID;
                        context.Schedules.AddObject(ScheduleObj);
                    }
                    context.SaveChanges();
                }

                //if the resource was just added to a group, activate them in the system
                if (((STATSModel.Resource)e.Entity).Groups.Count() == 0)
                {
                    STATSModel.Resource.ActivateResource(((STATSModel.Resource)e.Entity).ResourceID);
                }
            }
            Response.Redirect("~/Pages/Resource.aspx?redirectfrom=dvResource_ItemInserted&resourceid=" + ((STATSModel.Resource)e.Entity).ResourceID, true);
        }


        protected void dvResource_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            if (curAuthentication == null)
            {
                curAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                curResource = curAuthentication.Resource;
                curResourceType = curResource.ResourceType;
            }
            var updatedResourceTypeID = (string)e.NewValues["ResourceTypeID"];
            var oldResourceTypeID = (string)e.OldValues["ResourceTypeID"];
            if (oldResourceTypeID != updatedResourceTypeID)
            {
                if ((curResourceType.ResourceTypeName == ResourceTypeAdministrator.ResourceTypeName || curResourceType.ResourceTypeName == ResourceTypeTimeMgr.ResourceTypeName) && (updatedResourceTypeID == ResourceTypeAdministrator.ResourceTypeID.ToString() || updatedResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID.ToString() || updatedResourceTypeID == ResourceTypeDirector.ResourceTypeID.ToString()))
                {
                    lblInsertError.Text = "You do not have rights to change a resource to this level of authority.";
                    e.Cancel = true;
                }
                if (oldResourceTypeID == ResourceTypeDirector.ResourceTypeID.ToString() && updatedResourceTypeID != ResourceTypeDirector.ResourceTypeID.ToString())
                {
                    lblInsertError.Text = "You cannot change a director to another level of authority.";
                    e.Cancel = true;
                }
            }
            // This prevents users from changing students to other resources types
            if ((string)e.OldValues["ResourceTypeID"] == ResourceTypeStudent.ResourceTypeID.ToString() && (string)e.NewValues["ResourceTypeID"] != ResourceTypeStudent.ResourceTypeID.ToString())
            {
                lblInsertError.Text = "You cannot change a student to another resource type.";
                e.Cancel = true;
            }
            // This prevents users from changing tutors to other resources types
            if ((string)e.OldValues["ResourceTypeID"] == ResourceTypeTutor.ResourceTypeID.ToString() && (string)e.NewValues["ResourceTypeID"] != ResourceTypeTutor.ResourceTypeID.ToString())
            {
                lblInsertError.Text = "You cannot change a tutor to another resource type.";
                e.Cancel = true;
            }
            // This prevents users from changing other resources types to students
            if ((string)e.OldValues["ResourceTypeID"] != ResourceTypeStudent.ResourceTypeID.ToString() && (string)e.NewValues["ResourceTypeID"] == ResourceTypeStudent.ResourceTypeID.ToString())
            {
                lblInsertError.Text = "You cannot change another resource type to a student.";
                e.Cancel = true;
            }
            // This prevents users from changing other resources types to tutors
            if ((string)e.OldValues["ResourceTypeID"] != ResourceTypeTutor.ResourceTypeID.ToString() && (string)e.NewValues["ResourceTypeID"] == ResourceTypeTutor.ResourceTypeID.ToString())
            {
                lblInsertError.Text = "You cannot change another resource type to a tutor.";
                e.Cancel = true;
            }

            if (string.IsNullOrWhiteSpace((string)e.NewValues["ResourceNameLast"]))
            {
                lblInsertError.Text = "You must enter a value for Resource Last Name";
                e.Cancel = true;
            }
            else if (string.IsNullOrWhiteSpace((string)e.NewValues["ResourceNameFirst"]))
            {
                lblInsertError.Text = "You must enter a value for Resource First Name";
                e.Cancel = true;
            }
            else if (string.IsNullOrWhiteSpace((string)e.NewValues["ResourceLogin"]))
            {
                lblInsertError.Text = "You must enter a value for Resource Login";
                e.Cancel = true;
            }
        }

        protected void dvResource_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect("~/Pages/Resource.aspx", false);
        }

        protected void dvResource_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
            {
                Response.Redirect("~/Pages/Resource.aspx", true);
            }
        }

        public void CourseRegistrationControl_ButtonAddEvent()
        {
            ResourceRegistrationSummaryControl.BindAndShow();
            ResourceScheduleControl.RunScheduler(false);
        }

        private void TutoringMultiRegistrationControl_ButtonAddEvent()
        {
            ResourceRegistrationSummaryControl.BindAndShow();
            ResourceScheduleControl.RunScheduler(false);
        }

        private void TutoringSkillsModule_OnTutoringSkillsChanged()
        {
            TutoringMultiRegistrationControl.BindAndShow();
        }

        protected void dvResource_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var dv = (DetailsView)sender;
            Guid ResourceID = (Guid)dv.DataKey.Value;
            var eResource2 = STATSModel.Resource.Get(ResourceID, true);
            if (!eResource2.CanBeDeleted)
            {
                String strError = "The Resource has activity associated with it and cannot be deleted.  It has been inactivated instead.";
                ScriptManager.RegisterClientScriptBlock(dv, dv.GetType(), "DeleteError", "<script language='javascript'>alert('" + strError + "');</script>", false);
                e.Cancel = true;

                STATSModel.Resource.InactivateResource(ResourceID);
            }
        }

        protected void dvResource_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
            Response.Redirect("~/Pages/Resource.aspx?redirectfrom=dvResource_ItemUpdated&resourceid=" + ResourceID, false);
        }

        protected void cvLoginEdit_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String ResourceLogin = args.Value;
            int iCount = STATSModel.Resource.CountResourceByLoginName(ResourceLogin);

            if (iCount == 0) // No instances, then the value is valid.
            {
                args.IsValid = true;
            }
            else if (iCount == 1) // 1 instance exists, validate that the CURRENT RESOURCE is the existing owner of the Login.
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
                STATSModel.Resource ResourceOwner = STATSModel.Resource.Get(ResourceLogin);
                args.IsValid = (ResourceOwner.ResourceID == ResourceID);
            }
            else // If 2 or more instances exist, it is invalid.  End of story.
            {
                args.IsValid = false;
            }
        }

        protected void cvADLoginEdit_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String resourceADLogin = args.Value;
            int iCount = STATSModel.Resource.CountResourceByADLoginName(resourceADLogin);

            if (iCount == 0) // No instances, then the value is valid.
            {
                args.IsValid = true;
            }
            else if (iCount == 1) // 1 instance exists, validate that the CURRENT RESOURCE is the existing owner of the Login.
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
                STATSModel.Resource ResourceOwner = STATSModel.Resource.GetByADLogin(resourceADLogin);
                args.IsValid = (ResourceOwner.ResourceID == ResourceID);
            }
            else // If 2 or more instances exist, it is invalid.  End of story.
            {
                args.IsValid = false;
            }
        }

        protected void cvLoginInsert_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String ResourceLogin = args.Value;
            int iCount = STATSModel.Resource.CountResourceByLoginName(ResourceLogin);
            args.IsValid = (iCount == 0);
        }

        protected void cvADLoginInsert_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String resourceADLogin = args.Value;
            int iCount = STATSModel.Resource.CountResourceByADLoginName(resourceADLogin);
            args.IsValid = (iCount == 0);
        }

        protected void dvResource_ItemCreated(object sender, EventArgs e)
        {
            //// Test FooterRow to make sure all rows have been created
            //if (dvResource.FooterRow != null)
            //{
            //    // The command bar is the last element in the Rows collection
            //    int commandRowIndex = dvResource.Rows.Count - 1;
            //    DetailsViewRow commandRow = dvResource.Rows[commandRowIndex];

            //    // Look for the DELETE button
            //    DataControlFieldCell cell = (DataControlFieldCell) commandRow.Controls[0];
            //    foreach (Control ctl in cell.Controls)
            //    {
            //        LinkButton link = ctl as LinkButton;
            //        if (link != null)
            //        {
            //            if (link.CommandName == "Delete")
            //            {
            //                link.ToolTip = "Click here to delete";
            //                link.OnClientClick = "return confirm('Do you really want to delete this record?');";
            //            }
            //        }
            //    }
            //}
        }

        protected void dvResource_DataBound(object sender, EventArgs e)
        {
            if (dvResource.CurrentMode == DetailsViewMode.Insert)
            {
                DropDownList ddlGroup = (DropDownList)dvResource.FindControl("ddlGroup");
                ddlGroup.Items.Insert(0, new ListItem("--Select Primary Group--", "NONE"));
            }
        }
    }
}