﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Subject.aspx.cs" Inherits="UK.STATS.Administration.Pages.SubjectPage" %>
<%@ Register TagPrefix="SIS" TagName="SubjectSearch" Src="~/UserControls/SubjectSearch.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_MainContents" runat="server">   
    
    <asp:Panel ID="panelSecurityDenied" runat="server" Visible="false">
        <h2> You must be a Director in order to access this page.</h2>
    </asp:Panel>
     
    <asp:Panel ID="panelSubjectList" runat="server" Visible="false">
        <ul>
            <li>To create a Class Prefix (e.g. ENG), click "Create Class Prefix".  Please verify that it does not exist before creating one.</li>
            <li>To create a Course (e.g. ENG101), click the "View Courses" link for "ENG", then click "Create Course"</li>
            <li>To create a Section of a Course (e.g. ENG101-001), click the "View Courses" link for "ENG", then click the "View Sections" link for "ENG101", then click "Create Course Section"</li>
            <li>To edit existing entries, click "View Details", and then at the bottom of the grid, click "Edit".  You must click "Update" to save your changes.</li>
        </ul>
        <SIS:SubjectSearch ID="SubjectSearchControl" runat="server" />
    </asp:Panel>

    <asp:Panel ID="panelSubjectView" runat="server" Visible="false">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/Subject.aspx">Return to List</asp:HyperLink>
        <asp:DetailsView ID="dvSubject" runat="server" AutoGenerateRows="False" 
            CellPadding="4" DataKeyNames="SubjectID" DataSourceID="edsSubject" 
            ForeColor="#333333" GridLines="None" Height="50px" Width="100%" 
            OnItemInserting="dvSubjectView_OnItemInserting" 
            OnItemInserted="dvSubjectView_ItemInserted" 
            OnItemCommand="dvSubjectView_ItemCommand" 
            OnItemDeleted="dvSubjectView_ItemDeleted" 
            OnItemDeleting="dvSubject_ItemDeleting">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
            <EditRowStyle BackColor="#2461BF" />
            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="SubjectID" HeaderText="SubjectID" ReadOnly="True" 
                    SortExpression="SubjectID" Visible="False" />
                <asp:BoundField DataField="SubjectNameShort" HeaderText="Class Prefix" 
                    SortExpression="SubjectNameShort" ConvertEmptyStringToNull="False" />
                <asp:BoundField DataField="SubjectDescription" HeaderText="Class Prefix Description (Optional)" 
                    SortExpression="SubjectDescription" ConvertEmptyStringToNull="False" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
        </asp:DetailsView>
        <asp:EntityDataSource ID="edsSubject" runat="server" 
            ConnectionString="name=STATSEntities" DefaultContainerName="STATSEntities" 
            EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
            EnableUpdate="True" EntitySetName="Subjects" Where="(it.SubjectID == @querySubjectID)">
            <WhereParameters>
                <asp:QueryStringParameter QueryStringField="subjectid" DbType="Guid" Name="querySubjectID" ConvertEmptyStringToNull="True"/>
            </WhereParameters>
        </asp:EntityDataSource>
    </asp:Panel>
    <div class="clear"></div>
</asp:Content>