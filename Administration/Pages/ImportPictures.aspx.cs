﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.Pages
{
    public partial class ImportPictures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                Uri baseUri = new Uri(txtStartingURL.Text);

                // Manually enter the roster links in the method below
                List<string> rosterLinks = GetRosterLinks(baseUri);
                List<Link> playerAndCoachLinks = GetPlayerAndCoachLinks(baseUri, rosterLinks);
                //At this point we have a list of players (with names) and coaches (without names) and urls to their profile page
                List<Link> playerImageLinks = GetPlayerImageLinks(baseUri, playerAndCoachLinks);
                DownloadAndSavePlayerImages(playerImageLinks);
                txtLog.Text += "DONE\r\n";
            }
            catch (Exception ex)
            {
                txtLog.Text += "UNCAUGHT ERROR: " + ex.ToString() + "\r\n";
            }
        }

        private List<Link> GetTeamLinks(Uri baseUri)
        {
            List<Link> teamLinks = new List<Link>();

            try
            {
                HtmlWeb webGet = new HtmlWeb();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HtmlDocument document = webGet.Load(txtStartingURL.Text);

                var teamsNavMenu = (from item in document.DocumentNode.Descendants()
                                    where item.Id == "2"
                                    select item).FirstOrDefault();

                if (teamsNavMenu == null)
                {
                    txtLog.Text += "Could not find Teams navigation menu with ID 'nav-01'\r\n";
                }
                else
                {
                    teamLinks = (from item in teamsNavMenu.Descendants()
                                 where item.Name == "a" &&
                                       item.Attributes["href"] != null &&
                                       !string.IsNullOrWhiteSpace(item.Attributes["href"].Value) &&
                                       !string.IsNullOrWhiteSpace(item.InnerText) &&
                                       item.Attributes["href"].Value != "#"
                                 select new Link
                                 {
                                     Url = new Uri(baseUri, item.Attributes["href"].Value).AbsoluteUri,
                                     Text = item.InnerText
                                 }).ToList();

                    txtLog.Text += "Found " + teamLinks.Count() + " team links to parse\r\n";
                }
            }
            catch (Exception ex)
            {
                txtLog.Text += "ERROR: " + ex.ToString() + "\r\n";
            }

            return teamLinks;
        }

        private List<string> GetRosterLinks(Uri baseUri)
        {
            List<string> rosterLinks = new List<string>();

            try
            {
                txtLog.Text += "Working on getting roster link for Baseball\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=baseball").ToString());

                txtLog.Text += "Working on getting roster link for Men's Basketball\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=mbball").ToString());

                txtLog.Text += "Working on getting roster link for Men's Cross Country\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=mcross").ToString());

                txtLog.Text += "Working on getting roster link for Men's Golf\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=mgolf").ToString());

                txtLog.Text += "Working on getting roster link for Men's Soccer\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=msoc").ToString());

                txtLog.Text += "Working on getting roster link for Swimming & Diving\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=mswim").ToString());

                txtLog.Text += "Working on getting roster link for Men's Tennis\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=mten").ToString());

                // Women & Men's 
                txtLog.Text += "Working on getting roster link for Track & Field\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=track").ToString());

                txtLog.Text += "Working on getting roster link for Women's Basketball\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wbball").ToString());

                txtLog.Text += "Working on getting roster link for Cross Country\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=cross").ToString());

                txtLog.Text += "Working on getting roster link for Women's Golf\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wgolf").ToString());

                txtLog.Text += "Working on getting roster link for Gymnastics\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wgym").ToString());

                // Women's & Men's
                txtLog.Text += "Working on getting roster link for Rifle\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=rifle").ToString());

                txtLog.Text += "Working on getting roster link for Soccer\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wsoc").ToString());

                txtLog.Text += "Working on getting roster link for Softball\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=softball").ToString());

                txtLog.Text += "Working on getting roster link for Swimming & Diving\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wswim").ToString());

                txtLog.Text += "Working on getting roster link for Women's Tennis\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wten").ToString());

                txtLog.Text += "Working on getting roster link for Volleyball\r\n";
                rosterLinks.Add(new Uri(baseUri, "https://ukathletics.com/roster.aspx?path=wvball").ToString());
            }
            catch (Exception ex)
            {
                txtLog.Text += "ERROR: " + ex.ToString() + "\r\n";
            }

            return rosterLinks;
        }

        private List<Link> GetPlayerAndCoachLinks(Uri baseUri, List<string> rosterLinks)
        {
            List<Link> playerAndCoachLinks = new List<Link>();

            Parallel.ForEach(rosterLinks, rosterLink =>
            {
                List<Link> playerLinks = new List<Link>();
                List<Link> coachLinks = new List<Link>();

                try
                {
                    HtmlWeb rosterWebGet = new HtmlWeb();
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HtmlDocument rosterDocument = rosterWebGet.Load(rosterLink);

                    var rosterTables = (from item in rosterDocument.DocumentNode.Descendants("ul")
                                          where item.Attributes["class"] != null &&
                                          item.Attributes["class"].Value == "sidearm-roster-players"
                                            select item);
                    
                    foreach (var rosterTable in rosterTables)
                    {
                        List<Link> playerLinksToAdd = (from item in rosterTable.Descendants()
                                                       where item.Name == "a" &&
                                                             item.Attributes["href"] != null &&
                                                             !string.IsNullOrWhiteSpace(item.Attributes["href"].Value) &&
                                                             !string.IsNullOrWhiteSpace(item.InnerText) &&
                                                             item.ParentNode.Name == "p" &&
                                                             item.Attributes["href"].Value != "#"
                                                       select new Link
                                                       {
                                                           Url = new Uri(baseUri, item.Attributes["href"].Value).AbsoluteUri,
                                                           Text = item.InnerText
                                                       }).ToList();

                        playerLinks.AddRange(playerLinksToAdd);
                    }
                    txtLog.Text += "Found " + playerLinks.Count() + " player links\r\n";

                   
                    var rosterTablesCoaches = (from item in rosterDocument.DocumentNode.Descendants("ul")
                                        where item.Attributes["class"] != null &&
                                        item.Attributes["class"].Value.Contains("sidearm-roster-coaches")
                                        select item);
                    foreach (var rosterTableCoach in rosterTablesCoaches)
                    {
                        List<Link> coachLinksToAdd = (from item in rosterTableCoach.Descendants()
                                                      where item.Name == "a" &&
                                                            item.Attributes["href"] != null &&
                                                            !string.IsNullOrWhiteSpace(item.Attributes["href"].Value) &&
                                                            !string.IsNullOrWhiteSpace(item.InnerText) &&
                                                            item.Attributes["href"].Value != "#"
                                                      select new Link
                                                      {
                                                          Url = new Uri(baseUri, item.Attributes["href"].Value).AbsoluteUri,
                                                          Text = item.Attributes["aria-label"] != null ? item.Attributes["aria-label"].Value.Replace(" - View Full Bio","") : ""
                                                       }).ToList();
                                                        //Note:  We haven't attached the Coach's name here.  Make sure to do it later.

                        coachLinks.AddRange(coachLinksToAdd);
                    }
                    txtLog.Text += "Found " + coachLinks.Count() + " coach links\r\n";

                    playerAndCoachLinks.AddRange(playerLinks);
                    playerAndCoachLinks.AddRange(coachLinks);
                }
                catch (Exception ex)
                {
                    txtLog.Text += "ERROR: " + ex.ToString() + "\r\n";
                }
            });

            return playerAndCoachLinks;
        }

        private List<Link> GetPlayerImageLinks(Uri baseUri, List<Link> playerProfileLinks)
        {
            List<Link> playerImageLinks = new List<Link>();

            Parallel.ForEach(playerProfileLinks, currentPlayerProfileLink =>
            {
                playerImageLinks.Add(GetPlayerImageLink(baseUri, currentPlayerProfileLink.Url, currentPlayerProfileLink.Text));
            });

            return playerImageLinks;
        }

        private Link GetPlayerImageLink(Uri baseUri, string playerProfilePage, string playerProfileName)
        {
            try
            {
                HtmlWeb playerWebGet = new HtmlWeb();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HtmlDocument playerDocument = playerWebGet.Load(playerProfilePage);

                Link imageLink = (from item in playerDocument.DocumentNode.Descendants()
                                  where item.Name == "div" &&
                                        item.Attributes["class"] != null &&
                                        (item.Attributes["class"].Value == "sidearm-roster-player-image" ||
                                        item.Attributes["class"].Value.Contains("sidearm-coach-bio-image"))
                                  select new Link
                                  {
                                      Url = new Uri(baseUri, item.Descendants("img").First().Attributes["src"].Value).AbsoluteUri,
                                      Text = playerProfileName
                                  }).FirstOrDefault();

                txtLog.Text += "Found image link for: '" + imageLink.Text + "' url: " + imageLink.Url + "\r\n";

                return imageLink;
            }
            catch (Exception ex)
            {
                txtLog.Text += "ERROR: " + ex.ToString() + "\r\n";
                return null;
            }
        }

        private void DownloadAndSavePlayerImages(List<Link> playerImageLinks)
        {
            int numSuccess = 0;

            Parallel.ForEach(playerImageLinks, currentPlayerImageLink =>
            {
                bool success = DownloadAndSavePlayerImages(currentPlayerImageLink);
                if (success)
                {
                    numSuccess++;
                }
            });

            txtLog.Text += "Successfully imported: " + numSuccess + " images. Failed: " + (playerImageLinks.Count - numSuccess) + "\r\n";
        }

        private bool DownloadAndSavePlayerImages(Link imageLink)
        {
            try
            {
                //get the resources name split out into first/last
                string[] nameSplit = imageLink.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string firstName = "";
                string lastName = "";
                if (nameSplit.Length > 0)
                {
                    firstName = nameSplit[0];
                    if (nameSplit.Length > 1)
                    {
                        lastName = nameSplit[1];
                    }
                }

                //check for invalid placeholder logo (will return 404 not found)
                /*if (imageLink.Url.Contains("mtt-logo.gif"))
                {
                    txtLog.Text += "Resource with name: '" + imageLink.Text + "' First: '" + firstName + "' Last: '" + lastName + "' does not have a valid image\r\n";
                    return false;
                }*/

                //try to get the resource with matching first/last name
                Guid resourceID = Resource.Get(firstName, lastName);

                if (resourceID != Guid.Empty) //only download the image if we found a valid resource
                {
                    using (var client = new System.Net.WebClient())
                    {
                        //download the picture bytes and save to the database
                        byte[] pictureBytes = client.DownloadData(imageLink.Url);
                        ResourcePicture.SavePicture(resourceID, pictureBytes);
                        return true;
                    }
                }
                else
                {
                    txtLog.Text += "Could not find resource with name: '" + imageLink.Text + "' First: '" + firstName + "' Last: '" + lastName + "'\r\n";
                }
            }
            catch (Exception ex)
            {
                txtLog.Text += "ERROR: " + ex.ToString() + "\r\n";
            }
            return false;
        }

        private class Link
        {
            public string Url;
            public string Text;
        }
    }
}