﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;


namespace UK.STATS.Administration
{
    public partial class Layout : System.Web.UI.MasterPage
    {
        //private SiteMapNode OnSiteMapResolve(Object sender, SiteMapResolveEventArgs e)
        //{ 
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            //SiteMap.SiteMapResolve += new SiteMapResolveEventHandler(OnSiteMapResolve);
            lblApplicationName.Text = STATSModel.Setting.GetByKey("ApplicationName").SettingValue.ToString();

            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["AuthenticationKey"] == null || (Guid)System.Web.HttpContext.Current.Session["AuthenticationKey"] == Guid.Empty)
            {   // There is no authenticated user.
                Response.Redirect("~/Login.aspx", true);
            }
            else
            {
                // An authentication key exists.
                Guid AuthKey = (Guid)Session["AuthenticationKey"];

                // If Authentication Key is an Empty guid, then something is seriously wrong.
                if (AuthKey == Guid.Empty)
                {
                    Response.Redirect("~/Login.aspx", true);
                }

                // Grab it, and verify it against the one stored within the database.
                STATSModel.Authentication AuthenticationObj = STATSModel.Authentication.Get(AuthKey);

                if (AuthenticationObj.AuthenticationExpire < DateTime.Now)
                {
                    AuthenticationObj.Expire();
                    Session.Clear();
                    Response.Redirect("~/Login.aspx", true);
                }
                else
                {
                    String strSessionTimeout = STATSModel.Setting.GetByKey("SessionTimeout").SettingValue;
                    int SessionTimeout = Int32.Parse(strSessionTimeout);

                    // NOTE: Improper use of logic outside of the EntityModel!
                    var context = new STATSEntities();
                    var eAuthentication = (from STATSModel.Authentication obj in context.Authentications where obj.AuthenticationID == AuthenticationObj.AuthenticationID select obj).First();
                    eAuthentication.AuthenticationExpire = DateTime.Now.AddMinutes(SessionTimeout);
                    context.SaveChanges();

                    AuthenticatedUserInfoControl.AuthenticationID = AuthKey;
                    AuthenticatedUserInfoControl.ShowAndBind();
                }
            }

            
        }
    }
}