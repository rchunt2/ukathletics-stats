﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TutoringSkills.ascx.cs"
    Inherits="UK.STATS.Administration.UserControls.TutoringSkills" %>

<asp:Panel ID="panelTutoringSkills" runat="server">
    <div id="TutoringSkillsModule">
        <div id="listBox">
            <div class="Header">Tutoring Skills:</div>
            <asp:ListBox ID="lbSkills" runat="server" Width="350px" Height="100px"></asp:ListBox>
        </div>
        <div id="TutoringSkillsButtons" style="float: right">
            <div id="addButtonTutoring">
                <asp:DropDownList ID="ddlSubject" runat="server" Width="150px" Height="20px" 
                    AutoPostBack="True" onselectedindexchanged="ddlSubject_SelectedIndexChanged"></asp:DropDownList>
                <asp:DropDownList ID="ddlCourse" runat="server" Width="150px" Height="20px"></asp:DropDownList>
                <br />
                <asp:Button ID="btnAddSkill" runat="server" Text="Add Skill"
                    Height="30px" Width="100px" onclick="btnAddSkill_Click" /></div>
            <div id="deleteButtonTutoring">
                <asp:Button ID="btnDeleteSkill" runat="server" Text="Delete Skill"
                    Height="30px" Width="100px" onclick="btnDeleteSkill_Click" /></div>
        </div>
    </div>
</asp:Panel>

