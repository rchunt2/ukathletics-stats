﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSelection.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourceSelection" %>

<style type="text/css">
    
    .threeColumnContainer 
    {
    	position: relative;
    	width: 750px;
    }
    
    .threeColumnColumnHeader
    {
    	text-align: center;
    	font-weight: bold;
    	font-size: 18px;
    }
    
    .column
    {
    	width: 100%;
    	text-align: center;
    	margin: 0 auto;
    }
    .threeColumnLeft { width: 40%; display: inline-block; }
    .threeColumnMiddle { position: relative; width: 10%; display: inline-block; }
    .threeColumnMiddleButton { width: 100%; }
    .threeColumnRight { width: 40%; display: inline-block; }
    .column {}    
    .pickerSourceContainer {}
    .pickerActionContainer {}
    .pickerDestinationContainer {}
    
</style>

<div>
    <div class="threeColumnContainer">
        <div class="pickerSourceContainer threeColumnLeft column">
            <div class="threeColumnColumnHeader">Available</div>
            <asp:ListBox ID="lbGroupAvailable" runat="server" Width="100%" Rows="7" 
                SelectionMode="Multiple"></asp:ListBox>
        </div>

        <div class="pickerActionContainer threeColumnMiddle column">
            <asp:Button ID="btnGroupAddAll" runat="server" Text="All&nbsp;&gt;&gt;" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnGroupAddAll_Click" />
            <br />
            <asp:Button ID="btnGroupRemoveAll" runat="server" Text="&lt;&lt;&nbsp;All" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnGroupRemoveAll_Click" />
            <br />
            Groups
            <br />
            <asp:Button ID="btnGroupAdd" runat="server" Text="&gt;&gt;" Height="25px" 
                CssClass="threeColumnMiddleButton" onclick="btnGroupAdd_Click" />
            <br />
            <asp:Button ID="btnGroupRemove" runat="server" Text="&lt;&lt;" Height="25px" 
                CssClass="threeColumnMiddleButton" onclick="btnGroupRemove_Click" />
        </div>

        <div class="pickerDestinationContainer threeColumnRight column">
            <div class="threeColumnColumnHeader">Selected</div>
            <asp:ListBox ID="lbGroupSelected" runat="server" Width="100%" Rows="7" 
                SelectionMode="Multiple"></asp:ListBox>
        </div>
    </div>

    <hr />

    <div class="threeColumnContainer">
        <div class="pickerSourceContainer threeColumnLeft column">
            <div class="threeColumnColumnHeader">Available</div>
            <asp:ListBox ID="lbResourceAvailable" runat="server" Width="100%" Rows="7" 
                SelectionMode="Multiple"></asp:ListBox>
        </div>

        <div class="pickerActionContainer threeColumnMiddle column">
            <asp:Button ID="btnResourceAddAll" runat="server" Text="All&nbsp;&gt;&gt;" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnResourceAddAll_Click" />
            <br />
            <asp:Button ID="btnResourceRemoveAll" runat="server" Text="&lt;&lt;&nbsp;All" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnResourceRemoveAll_Click" />
            <br />
            Resources
            <br />
            <asp:Button ID="btnResourceAdd" runat="server" Text="&gt;&gt;" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnResourceAdd_Click" />
            <br />
            <asp:Button ID="btnResourceRemove" runat="server" Text="&lt;&lt;" Height="25px" CssClass="threeColumnMiddleButton" onclick="btnResourceRemove_Click" />
        </div>

        <div class="pickerDestinationContainer threeColumnRight column">
            <div class="threeColumnColumnHeader">Selected</div>
            <asp:ListBox ID="lbResourceSelected" runat="server" Width="100%" Rows="7" 
                SelectionMode="Multiple"></asp:ListBox>
        </div>
    </div>
    <%--
    <hr />
    <div class="reviewSelectionContainer">
        <asp:Button ID="btnConfirm" runat="server" Text="Confirm Selection" 
            onclick="btnConfirm_Click" />
    </div>
    --%>
</div>