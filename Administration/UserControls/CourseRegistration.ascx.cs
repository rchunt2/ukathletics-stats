﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class CourseRegistration : UK.STATS.Administration.Pages.UKBaseControl
    {
        public enum RegistrationMode { Resource, Group }

        public delegate void ButtonAddDelegate();
        public event ButtonAddDelegate ButtonAddEvent;

        public Guid ObjectID
        {
            get { return GetFromViewState<Guid>("CourseRegistration_ObjectID"); }
            set { StoreInViewstate("CourseRegistration_ObjectID", value); }
        }

        public RegistrationMode Mode
        {
            get { return GetFromViewState<RegistrationMode>("CourseRegistration_Mode"); }
            set { StoreInViewstate("CourseRegistration_Mode", value); }
        }

        public Boolean IsVisible
        {
            get { return GetFromViewState<Boolean>("CourseRegistration_IsVisible"); }
            set { StoreInViewstate("CourseRegistration_IsVisible", value); }
        }

        /// <summary>
        /// This will enable/disable the Create button
        /// </summary>
        public Boolean IsReadOnly
        {
            set
            {
                this.btnRegisterCourse.Enabled = !value;
            }
        }

        public Guid SelectedSubjectID
        {
            get { return Guid.Parse(ddlSubjects.SelectedValue); }
        }

        public Guid SelectedCourseID
        {
            get { return Guid.Parse(ddlCourses.SelectedValue); }
        }

        public Guid SelectedCourseSectionID
        {
            get { return Guid.Parse(ddlCourseSections.SelectedValue); }
        }


        public void BindAndShow()
        {
            if (this.ObjectID != Guid.Empty)
            {
                if (this.IsVisible)
                {
                    BindRegisterCourseModuleSubjects();
                }
            }
            else
            {
                this.IsVisible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.ObjectID == Guid.Empty) { return; }

            switch(this.Mode)
            {
                case RegistrationMode.Resource:
                    Resource eResource = Resource.Get(this.ObjectID, true);
                    if (eResource.ResourceType.ResourceTypeName == "Tutor")
                    {
                        this.IsVisible = false;
                        panelCourseRegistration.Visible = false;
                    }
                    else
                    {
                        this.IsVisible = true;
                        panelCourseRegistration.Visible = true;
                        lblTitle.Text = "Register For Course:";
                        if (!Page.IsPostBack)
                        {
                            BindAndShow();
                        }
                    }
                    break;
                case RegistrationMode.Group:
                    this.IsVisible = true;
                    lblTitle.Text = "Register For Course:";
                    if (!Page.IsPostBack)
                    {
                        BindAndShow();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void BindRegisterCourseModuleSubjects()
        {
            ddlSubjects.Items.Clear();
            Subject[] AllSubjects = Subject.Get();
            foreach (Subject SubjectObj in AllSubjects)
            {
                String liName = SubjectObj.SubjectNameShort;
                String liValue = SubjectObj.SubjectID.ToString();

                var li = new ListItem(liName, liValue);
                ddlSubjects.Items.Add(li);
            }

            BindRegisterCourseModuleCourses(AllSubjects[0].SubjectID);
        }

        private void BindRegisterCourseModuleCourses(Guid SubjectID)
        {
            ddlCourses.Items.Clear();
            Subject SubjectObj = Subject.Get(SubjectID);
            Course[] AllCourses = (from Course obj in SubjectObj.Courses orderby obj.CourseNumber ascending select obj).ToArray();
            foreach (Course CourseObj in AllCourses)
            {
                String liName = SubjectObj.SubjectNameShort + CourseObj.CourseNumber;
                String liValue = CourseObj.CourseID.ToString();
                var li = new ListItem(liName, liValue);
                ddlCourses.Items.Add(li);
            }
            BindRegisterCourseModuleSections(AllCourses[0].CourseID);
        }

        private void BindRegisterCourseModuleSections(Guid CourseID)
        {
            var addedCourseSections = new List<string>();

            ddlCourseSections.Items.Clear();
            Course CourseObj = Course.Get(CourseID);
            CourseSection[] AllCourseSections = (from CourseSection obj in CourseObj.CourseSections orderby obj.CourseSectionNumber ascending select obj).ToArray();
            foreach (CourseSection CourseSectionObj in AllCourseSections)
            {
                if(!addedCourseSections.Contains(CourseSectionObj.CourseSectionNumber))
                {
                    String Section = "Section " + CourseSectionObj.CourseSectionNumber;
                    String Start = DateTime.Today.Add(CourseSectionObj.CourseSectionTimeStart).ToShortTimeString();
                    String End = DateTime.Today.Add(CourseSectionObj.CourseSectionTimeEnd).ToShortTimeString();
                    String DaysMeet = CourseSectionObj.CourseSectionDaysMeet;
                    String Time = Start + " to " + End;
                    String liName = Section + " (" + DaysMeet + " @ " + Time + ")";
                    String liValue = CourseSectionObj.CourseSectionID.ToString();
                    var li = new ListItem(liName, liValue);
                    ddlCourseSections.Items.Add(li);
                    addedCourseSections.Add(CourseSectionObj.CourseSectionNumber);
                }
            }
            //BindSectionInfo();
        }

        private void BindSectionInfo()
        {
            // When selecting a Section, populate a label with the relevant information about it.
            String SelectedValue = ddlCourseSections.SelectedValue;
            Guid CourseSectionID = Guid.Parse(SelectedValue);
            CourseSection eCourseSection = CourseSection.Get(CourseSectionID);
            Course eCourse = eCourseSection.Course;
            Subject eSubject = eCourse.Subject;
            String Start = DateTime.Today.Add(eCourseSection.CourseSectionTimeStart).ToShortTimeString();
            String End = DateTime.Today.Add(eCourseSection.CourseSectionTimeEnd).ToShortTimeString();

            String strSubject = eSubject.SubjectNameShort;
            String strCourse = eCourse.CourseNumber;
            String strCourseName = eCourse.CourseName;
            String strSection = "Section " + eCourseSection.CourseSectionNumber;
            String strMeetDays = eCourseSection.CourseSectionDaysMeet;
            String strMeetTime = Start + " to " + End;
            String strDisplayText = strSubject + strCourse + " " + strSection + " - Meets " + strMeetDays + " @ " + strMeetTime;
            lblMeetingTime.Text = "Currently Selected: " + strDisplayText;
        }

        protected void ddlSubjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRegisterCourseModuleCourses(Guid.Parse(ddlSubjects.SelectedValue));
            BindSectionInfo(); // Not sure if this is needed, but it can't hurt.
        }

        protected void ddlCourses_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRegisterCourseModuleSections(Guid.Parse(ddlCourses.SelectedValue));
            BindSectionInfo(); // Not sure if this is needed, but it can't hurt.
        }

        protected void ddlCourseSections_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSectionInfo();
        }

        public void RegisterResource(CourseSection CourseSectionObj, Resource ResourceObj, Boolean DisregardConflicts)
        {
            //Boolean success = CourseSectionObj.Register(ResourceObj, DisregardConflicts); // Deprecated.
            Boolean success = ResourceRegistration.Register(CourseSectionObj, ResourceObj, DisregardConflicts);

            if(!success)
            {
                ScriptManager.RegisterClientScriptBlock(btnRegisterCourse, btnRegisterCourse.GetType(), "TimeConflict", "<script language='javascript'>alert('This course was not registered due to a time conflict!');</script>", false);
            }
        }

        public void Register(CourseSection CourseSectionObj, Boolean DisregardConflicts)
        {
            switch (this.Mode)
            {
                case RegistrationMode.Resource:
                    Resource ResourceObj = Resource.Get(this.ObjectID);
                    this.RegisterResource(CourseSectionObj, ResourceObj, DisregardConflicts);
                    break;
                case RegistrationMode.Group:
                    Group GroupObj = Group.Get(this.ObjectID);
                    GroupMember[] eGroupMembers = GroupObj.GroupMembers.ToArray();

                    var eResources = new List<Resource>();
                    foreach (var eGroupMember in eGroupMembers)
                    {
                        eResources.Add(eGroupMember.Resource);
                    }

                    foreach (Resource CurrentResourceObj in eResources)
                    {
                        this.RegisterResource(CourseSectionObj, CurrentResourceObj, DisregardConflicts);
                    }
                    break;
                default:
                    break;
            }
        }

        protected void btnRegisterCourse_Click(object sender, EventArgs e)
        {
            Guid CourseSectionID = Guid.Parse(ddlCourseSections.SelectedValue);
            CourseSection CourseSectionObj = CourseSection.Get(CourseSectionID);

            Boolean DisregardConflicts = (!cbOverride.Checked);

            this.Register(CourseSectionObj, DisregardConflicts);
            if (ButtonAddEvent != null)
            {
                ButtonAddEvent();
            }
        }
    }
}