﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupMemberAdministration.ascx.cs" Inherits="UK.STATS.Administration.UserControls.GroupMemberAdministration" %>

<%@ Register TagPrefix="SIS" TagName="ResourceSelectionSingle" Src="~/UserControls/ResourceSelectionSingle.ascx" %>

<style type="text/css">

    .GroupMemberAddArea
    {
    	border: 1px solid #ddd;
    	background-color: #eee;
    	width: 550px;
    	padding: 3px;
    	font-family: Arial;
    }
    
    .GroupMemberAddAreaButton
    {
    	float: right;
    }

</style>

    <div class="GroupMemberAddArea">
        <div class="GroupMemberAddAreaButton">
            <asp:Button ID="btnAddToGroup" runat="server" Text="Add To Group" onclick="btnAddToGroup_Click" />
        </div>
        <SIS:ResourceSelectionSingle ID="ResourceSelectionSingleControl" runat="server" /> 
        <div class="clear"></div>
    </div>


    <asp:Label ID="lblNumGroupMembers" runat="server" Text=""></asp:Label>
    <asp:GridView ID="gvGroupMembers" runat="server" CellPadding="2" AutoGenerateColumns="False" 
        EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left" 
    Width="580px" 
    onrowcommand="gvSearchResults_RowCommand" >
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="ResourceID_HIDDEN" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblResourceID" runat="server" Text='<%# Bind("ResourceID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ResourceNameFirst" HeaderText="First Name" >
                <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="ResourceNameLast" HeaderText="Last Name" >
                <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:ButtonField CommandName="remove" Text="Remove" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>



