﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.UserControls
{
    public partial class SubjectSearch : System.Web.UI.UserControl
    {
        private String _queryString = "";
        private String _detailPage = "";

        public delegate void ButtonAddDelegate();
        public event ButtonAddDelegate ButtonAddEvent;

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Button btn = new Button();
            btn.ID = "btnCreate";
            btn.Text = "Create Class Prefix";
            btn.Click += new EventHandler(btnCreate_Click);
            phSubject.Controls.Add(btn);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (txtSearchQuery.Text == String.Empty)
            {
                RunSearch(txtSearchQuery.Text);
            }
        }

        public String QueryString
        {
            get { return _queryString; }
            set { _queryString = value; }
        }

        public void RunSearch(String Query)
        {
            this.QueryString = Query;
            this.RunSearch();
        }

        public String DetailsPage
        {
            get { return _detailPage; }
            set { _detailPage = value; }
        }

        public Boolean isVisible
        {
            get { return panelSubjectSearch.Visible; }
            set { panelSubjectSearch.Visible = value; }
        }

        public void RunSearch()
        {
            STATSModel.Subject[] ResultSet = STATSModel.Subject.Search(this.QueryString);
            lblResultsCount.Visible = true;
            lblResultsCount.Text = ResultSet.Length + " results for '" + this.QueryString + "'";
            gvSearchResults.DataSource = ResultSet;
            gvSearchResults.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RunSearch(txtSearchQuery.Text);
        }

        protected void gvSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSearchResults.PageIndex = e.NewPageIndex;
            RunSearch(txtSearchQuery.Text);
            gvSearchResults.DataBind();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (ButtonAddEvent != null)
                ButtonAddEvent();
        }
    }
}            