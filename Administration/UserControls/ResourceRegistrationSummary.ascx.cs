﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourceRegistrationSummary : UK.STATS.Administration.Pages.UKBaseControl
    {
        public delegate void ButtonAddDelegate();

        public event ButtonAddDelegate ButtonAddEvent;

        public Resource ResourceObject
        {
            get
            {
                return GetFromViewState<STATSModel.Resource>("ResourceRegistrationSummary_ResourceObject");
            }
            set
            {
                StoreInViewstate("ResourceRegistrationSummary_ResourceObject", value);
            }
        }

        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public Boolean IsTutor
        {
            get
            {
                return (ResourceObject.ResourceTypeID == ResourceTypeTutor.ResourceTypeID);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //var monday = DateTime.Today.AddDays(-(DateTime.Today.DayOfWeek - DayOfWeek.Monday));
                //var sunday = monday.AddDays(6);
                //dtpkTutorSession.MinDate = monday;
                //dtpkTutorSession.MaxDate = sunday;
                dtpkTutorSession.SelectedDate = DateTime.Today.AddHours(8);
                BindTutoringModuleSubjects();
                BindLocations();
            }
        }

        public void BindAndShow()
        {
            CourseSection[] eCourseSections = ResourceRegistration.GetRegisteredCourses(ResourceObject);
            eCourseSections = (from CourseSection obj in eCourseSections orderby obj.Course.Subject.SubjectNameShort ascending, obj.Course.CourseNumber ascending select obj).ToArray();

            if (eCourseSections.Length > 0 && eCourseSections[0] != null)
            {
                gvResourceRegistration.Visible = true;
                gvResourceRegistration.DataSource = eCourseSections;
                gvResourceRegistration.DataBind();
            }
            else // If there are no courses registered, or the last course was dropped.
            {
                gvResourceRegistration.Visible = false;
            }

            if (ResourceObject.ResourceTypeID == ResourceTypeTutor.ResourceTypeID)
            {
                panelResourceTutoringRegistration.Visible = false;
                panelTutorSummary.Visible = true;
            }
            else
            {
                panelResourceTutoringRegistration.Visible = true;
                panelTutorSummary.Visible = true;
            }

            var eSessionsTutoring = this.ResourceObject.TutoringSessions;
            var DistinctSessions = new List<Session>();
            foreach (var eSession in eSessionsTutoring)
            {
                Boolean match = false;
                foreach (var distinctSession in DistinctSessions)
                {
                    // Note: The below if statement is done to make sure that group sessions are displayed correctly.  For tutors they want to see the sessions repeated with each student
                    // name in their grid.  Students however only need to the see the session once and with their tutor's name.  By checking for the resource ids with the tutors it ensures
                    // that the right amount of sessions will be displayed.
                    // If the resource is a tutor we need to pull up all of the group sessions
                    if (IsTutor)
                    {
                        if (distinctSession.SessionDateTimeStart.TimeOfDay == eSession.SessionDateTimeStart.TimeOfDay 
                            && distinctSession.SessionDateTimeStart.DayOfWeek == eSession.SessionDateTimeStart.DayOfWeek 
                            && distinctSession.CourseSectionID == eSession.CourseSectionID )
                            //&& distinctSession.GetStudents().First().ResourceID == eSession.GetStudents().First().ResourceID)
                        {
                            match = true;
                        }
                    }
                    // If the resource is a student then we don't need to worry about filtering sessions based on ResourceID
                    else
                    {
                        if (distinctSession.SessionDateTimeStart.TimeOfDay == eSession.SessionDateTimeStart.TimeOfDay && distinctSession.SessionDateTimeStart.DayOfWeek == eSession.SessionDateTimeStart.DayOfWeek && distinctSession.CourseSectionID == eSession.CourseSectionID) // && distinctSession.GetStudents().First().ResourceID == eSession.GetStudents().First().ResourceID)
                        {
                            match = true;
                        }
                    }
                }

                if (!match)
                {
                    //If Tutor, Add each of the students as its own unique "session"
                    if (IsTutor)
                    {
                        foreach (Resource resource in eSession.GetStudents())
                        {
                            Session newSession = (Session)eSession.Clone();
                            newSession.ResourceID = resource.ResourceID;
                            DistinctSessions.Add(newSession);
                        }
                    }
                    else
                    {
                        DistinctSessions.Add(eSession);
                    }
                }
            }

            if (ResourceObject.ResourceTypeID == ResourceTypeTutor.ResourceTypeID)
            {
                foreach (DataControlField dataControlField in gvTutorSummary.Columns)
                {
                    if (dataControlField.HeaderText == "Tutor")
                    {
                        dataControlField.HeaderText = "Student";
                    }
                }

                //sort based on the first student
                DistinctSessions = DistinctSessions.OrderBy(x => x.GetStudents()[0].ResourceNameDisplayLastFirst).ToList();
            }
            else
            {
                //sort based on the tutor
                DistinctSessions = DistinctSessions.OrderBy(x => x.GetTutor(ResourceTypeTutor).ResourceNameDisplayLastFirst).ToList();
            }

            gvTutorSummary.DataSource = DistinctSessions;
            gvTutorSummary.DataBind();
        }

        private void BindTutoringModuleSubjects()
        {
            ddlTutorSubject.Items.Clear();
            Subject[] AllSubjects = STATSModel.Subject.Get();
            foreach (Subject SubjectObj in AllSubjects)
            {
                String liName = SubjectObj.SubjectNameShort;
                String liValue = SubjectObj.SubjectID.ToString();

                ListItem li = new ListItem(liName, liValue);
                ddlTutorSubject.Items.Add(li);
            }

            BindTutoringModuleCourses(AllSubjects[0].SubjectID);
        }

        private void BindTutoringModuleSubjects(Guid SubjectID)
        {
            ddlTutorSubject.Items.Clear();
            STATSModel.Subject CurrentSubjectObj = Subject.Get(SubjectID);
            String liDefaultName = CurrentSubjectObj.SubjectNameShort;
            String liDefaultValue = CurrentSubjectObj.SubjectID.ToString();
            ListItem liDefault = new ListItem(liDefaultName, liDefaultValue);
            ddlTutorSubject.Items.Add(liDefault);

            Subject[] AllSubjects = Subject.Get();
            foreach (Subject SubjectObj in AllSubjects)
            {
                String liName = SubjectObj.SubjectNameShort;
                String liValue = SubjectObj.SubjectID.ToString();
                ListItem li = new ListItem(liName, liValue);
                ddlTutorSubject.Items.Add(li);
            }
        }

        private void BindTutoringModuleCourses(Guid SubjectID)
        {
            ddlTutorCourse.Items.Clear();
            Subject SubjectObj = Subject.Get(SubjectID);
            Course[] AllCourses = SubjectObj.Courses.ToArray();
            foreach (Course CourseObj in AllCourses)
            {
                String liName = SubjectObj.SubjectNameShort + CourseObj.CourseNumber;
                String liValue = CourseObj.CourseID.ToString();
                ListItem li = new ListItem(liName, liValue);
                ddlTutorCourse.Items.Add(li);
            }
            BindTutoringModuleTutors(AllCourses[0].CourseID);
        }

        private void BindLocations()
        {
            ddlLocation.Items.Clear();
            ddlLocation.Items.Add(new ListItem("Main Location", "7D387642-8238-E111-8E81-005056936D51"));
            ddlLocation.Items.Add(new ListItem("Football Location", "7B487642-8238-E111-8E81-005056936D51"));
        }

        private void BindTutoringModuleCoursesDefault(Guid CourseID)
        {
            ddlTutorCourse.Items.Clear();
            STATSModel.Course DefaultCourseObj = STATSModel.Course.Get(CourseID);
            STATSModel.Subject SubjectObj = DefaultCourseObj.Subject;
            BindTutoringModuleSubjects(SubjectObj.SubjectID);
            STATSModel.Course[] AllCourses = SubjectObj.Courses.ToArray();

            foreach (STATSModel.Course CourseObj in AllCourses)
            {
                String liName = SubjectObj.SubjectNameShort + CourseObj.CourseNumber;
                String liValue = CourseObj.CourseID.ToString();
                ListItem li = new ListItem(liName, liValue);
                ddlTutorCourse.Items.Add(li);
            }
            BindTutoringModuleTutors(AllCourses[0].CourseID);
        }

        private void BindTutoringModuleTutors(Guid CourseID)
        {
            ddlAvailableTutors.Items.Clear();
            STATSModel.Course CourseObj = STATSModel.Course.Get(CourseID);
            //STATSModel.CourseTutor[] eCourseTutors = CourseObj.CourseTutors.ToArray();
            List<STATSModel.CourseTutor> eCourseTutors = CourseObj.CourseTutors.ToList<STATSModel.CourseTutor>();
            eCourseTutors.Sort(delegate(STATSModel.CourseTutor x, STATSModel.CourseTutor y) { return x.Resource.ResourceNameLast.CompareTo(y.Resource.ResourceNameLast); });
            List<STATSModel.Resource> eResources = new List<Resource>();
            foreach (var eCourseTutor in eCourseTutors)
            {
                eResources.Add(eCourseTutor.Resource);
            }

            Boolean TutorsExist = false;
            eResources = eResources.OrderBy(x => x.ResourceNameLast).ThenBy(x => x.ResourceNameFirst).ToList();
            foreach (STATSModel.Resource TutorResourceObj in eResources)
            {
                String liText = TutorResourceObj.ResourceNameLast + ", " + TutorResourceObj.ResourceNameFirst;
                String liVal = TutorResourceObj.ResourceID.ToString();
                ListItem li = new ListItem(liText, liVal);
                ddlAvailableTutors.Items.Add(li);
                TutorsExist = true;
            }
            if (!TutorsExist)
            {
                String liText = "No Tutors For This Course!";
                String liVal = Guid.Empty.ToString();
                ListItem li = new ListItem(liText, liVal);
                ddlAvailableTutors.Items.Add(li);
                btnTutoringAdd.Enabled = false;
                hyperlinkCheckAvailability.Visible = false;
                dtpkTutorSession.Visible = false;
            }
            else
            {
                BindAvailabilityLink();
                btnTutoringAdd.Enabled = true;
                hyperlinkCheckAvailability.Visible = true;
                dtpkTutorSession.Visible = true;
            }
            ListItemCollection tutors = ddlAvailableTutors.Items;
        }

        private void BindAvailabilityLink()
        {
            if (this.ResourceObject != null)
            {
                String ResourceID = this.ResourceObject.ResourceID.ToString();
                String TutorID = ddlAvailableTutors.SelectedValue;

                String pageURL = "../PrintSchedule.aspx";
                String queryParameters = "?resourceid=" + ResourceID + "&resourceidcompare=" + TutorID;

                hyperlinkCheckAvailability.NavigateUrl = pageURL + queryParameters;
                hyperlinkCheckAvailability.Target = "_blank";
            }
        }

        protected void gvResourceRegistration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DropCourse")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Label lblCourseSectionID = (Label)gvResourceRegistration.Rows[i].Cells[0].FindControl("CourseSectionID");
                Guid CourseSectionID = Guid.Parse(lblCourseSectionID.Text);
                STATSModel.CourseSection CourseSectionObj = STATSModel.CourseSection.Get(CourseSectionID);
                STATSModel.Resource ResourceObj = this.ResourceObject;
                Boolean success = ResourceRegistration.DropCourse(CourseSectionObj, ResourceObj);
                // CourseSectionObj.DropCourse(ResourceObj); // Deprecated.
                BindAndShow();
            }

            if (e.CommandName == "CreateTutorSession")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Label lblCourseSectionID = (Label)gvResourceRegistration.Rows[i].Cells[0].FindControl("CourseSectionID");
                Guid CourseSectionID = Guid.Parse(lblCourseSectionID.Text);
                STATSModel.CourseSection CourseSectionObj = STATSModel.CourseSection.Get(CourseSectionID);
                STATSModel.Course CourseObj = CourseSectionObj.Course;
                STATSModel.Resource ResourceObj = this.ResourceObject;
                panelResourceTutoringRegistration.Visible = true;
                BindTutoringModuleCoursesDefault(CourseObj.CourseID);
            }

            if (e.CommandName == "EmailProfessor")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Label lblCourseSectionID = (Label)gvResourceRegistration.Rows[i].Cells[0].FindControl("CourseSectionID");
                Guid CourseSectionID = Guid.Parse(lblCourseSectionID.Text);
                STATSModel.Authentication eAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                MessagingModule.FromResourceID = eAuthentication.ResourceID;
                MessagingModule.RecipientID = CourseSectionID;
                MessagingModule.Mode = Messaging.MessagingMode.Compose;
                MessagingModule.TypeOfRecipient = Messaging.RecipientType.Professor;
                MessagingModule.BindAndShow();
                panelResourceRegistration.Visible = false;
                upSendMessage.Visible = true;
            }

            if (ButtonAddEvent != null)
            {
                ButtonAddEvent();
            }
        }

        protected void gvResourceRegistration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                STATSModel.CourseSection CourseSectionObj = (STATSModel.CourseSection)e.Row.DataItem;

                ((Label)e.Row.FindControl("CourseSectionID")).Text = CourseSectionObj.CourseSectionID.ToString();

                //Only enable the checkbox if the resource meets the requirement to receive text messages
                Boolean canText = (ResourceObject.ResourceTextingEnabled);
                Boolean hasCell = (ResourceObject.CarrierID != Guid.Empty);

                if (!canText || !hasCell)
                {
                    //ScriptManager.RegisterClientScriptBlock(ddlMessageType, ddlMessageType.GetType(), "MessagingNoticeSMS", "<script language='javascript'>alert('Text messages can not be sent to this Resource.  Please ensure that a proper Cellular Provider has been selected, and that texting is enabled for this Resource.');</script>", false);
                    //ddlMessageType.SelectedIndex = 0;
                    ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Enabled = false;
                }
                else
                {
                    if(TextMessage.Get(ResourceObject, null, CourseSectionObj.CourseSectionID).Count > 0)
                        ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Checked = true;
                    else
                        ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Checked = false;
                }
            }
        }

        protected void btnTutoringAdd_Click(object sender, EventArgs e)
        {
            var CourseID = Guid.Parse(ddlTutorCourse.SelectedValue);
            var eCourse = Course.Get(CourseID);
            var eResourceTutor = STATSModel.Resource.Get(Guid.Parse(ddlAvailableTutors.SelectedValue));
            var eResources = new List<Resource> { this.ResourceObject };

            if (!dtpkTutorSession.SelectedDate.HasValue)
            {
                return; // NO DATE SELECTED!!!
            }

            //int delta = DayOfWeek.Monday - ((DateTime)DateTime.Parse(Setting.GetByKey("SemesterDateEnd").SettingValue)).DayOfWeek;
            int delta = DayOfWeek.Monday - DateTime.Today.DayOfWeek;
            DateTime monday = DateTime.Today.AddDays(delta);
            DateTime sunday = monday.AddDays(6);

            // We are now adding tutoring sessions from the first day of the semester until the end
            //var dtOccurs = DateTime.Parse(Setting.GetByKey("SemesterDateEnd").SettingValue);
            var dtOccurs = dtpkTutorSession.SelectedDate.Value;
            delta = dtOccurs.DayOfWeek - monday.DayOfWeek;
            DateTime selectedDate = monday.AddDays(delta);
            TimeSpan selectedTime = dtOccurs.TimeOfDay;
            selectedDate = selectedDate.Date + selectedTime;

            CourseSection fakeCourse = new CourseSection();
            fakeCourse.CourseSectionDateStart = selectedDate;
            fakeCourse.CourseSectionDateEnd = selectedDate.AddMinutes(Convert.ToInt32(Setting.GetByKey("TutorSessionLength").SettingValue));
            Boolean studentConflict = STATSModel.Session.DoesTimeConflict(this.ResourceObject, fakeCourse);
            Boolean tutorConflict = STATSModel.Session.DoesTimeConflict(eResourceTutor, fakeCourse);
            if (studentConflict && chkAllowDouble.Checked)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Possible Double Booking", "<script language='javascript'> alert('There is already something scheduled at this time for the student.  If you would like to double book, please uncheck the \\'Conflict Check\\' option next to the \\'Check Availability\\' link.');</script>", false);
            }
            else
            {
                if (tutorConflict && chkAllowDouble.Checked)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Possible Double Booking", "<script language='javascript'> alert('There is already something scheduled at this time for the tutor.  If you would like to double book, please uncheck the \\'Conflict Check\\' option next to the \\'Check Availability\\' link.');</script>", false);
                }
            }

            if ((!studentConflict && !tutorConflict) || (studentConflict && !chkAllowDouble.Checked) || (tutorConflict && !chkAllowDouble.Checked))
            {
                var temp = ((DateTime)DateTime.Parse(Setting.GetByKey("SemesterDateBegin").SettingValue)).DayOfWeek;
                delta = DayOfWeek.Monday - ((DateTime)DateTime.Parse(Setting.GetByKey("SemesterDateBegin").SettingValue)).DayOfWeek;
                monday = ((DateTime)DateTime.Parse(Setting.GetByKey("SemesterDateBegin").SettingValue)).AddDays(delta);
                sunday = monday.AddDays(6);

                // We are now adding tutoring sessions from the first day of the semester until the end
                dtOccurs = dtpkTutorSession.SelectedDate.Value;
                delta = dtOccurs.DayOfWeek - monday.DayOfWeek;
                selectedDate = monday.AddDays(delta);
                selectedTime = dtOccurs.TimeOfDay;
                selectedDate = selectedDate.Date + selectedTime;

                Setting eSettingTutorSessionLength = Setting.GetByKey("TutorSessionLength");
                int iTutorSessionLength = Int32.Parse(eSettingTutorSessionLength.SettingValue.ToString());
                
                SessionType football = SessionType.GetByName("Tutoring - Football");
                if (ddlLocation.SelectedValue.ToUpper() == football.SessionTypeID.ToString().ToUpper())
                {
                    bool success = STATSModel.Session.CreateTutoringSession(eCourse, eResourceTutor, eResources, selectedDate, true, true, cbxVirtual.Checked);
                }
                else
                {
                    bool success = STATSModel.Session.CreateTutoringSession(eCourse, eResourceTutor, eResources, selectedDate, true, false, cbxVirtual.Checked);
                }
                

                if (ButtonAddEvent != null)
                {
                    ButtonAddEvent();
                }

                chkAllowDouble.Checked = true;
            }
        }

        protected void ddlTutorSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTutoringModuleCourses(Guid.Parse(ddlTutorSubject.SelectedValue));
        }

        protected void ddlTutorCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTutoringModuleTutors(Guid.Parse(ddlTutorCourse.SelectedValue));
        }

        protected void ddlAvailableTutors_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAvailabilityLink();
        }

        protected void gvTutorSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "CancelSession")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                Guid sessionID = GetSessionIDFromRow(rowIndex);
                CancelSession(sessionID);

                if (ButtonAddEvent != null)
                {
                    ButtonAddEvent();
                }
            }

            if (e.CommandName == "RemoveStudent")
            
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                Guid sessionID = GetSessionIDFromRow(rowIndex);
                Guid resourceID = GetResourceIDFromRow(rowIndex);
                RemoveGroupSessionStudent(sessionID, resourceID);
                
                if (ButtonAddEvent != null)
                    ButtonAddEvent();

                BindAndShow();
                
            }
        }

        private Guid GetSessionIDFromRow(int rowIndex)
        {
            Label lblSessionID = (Label)gvTutorSummary.Rows[rowIndex].FindControl("SessionID");
            Guid sessionID = Guid.Parse(lblSessionID.Text);
            return sessionID;
        }

        private Guid GetResourceIDFromRow(int rowIndex)
        {
            Label lblResourceID = (Label)gvTutorSummary.Rows[rowIndex].FindControl("ResourceID");
            Guid resourceID = Guid.Parse(lblResourceID.Text);
            return resourceID;
        }


        private Guid GetCourseSectionIDFromRow(int rowIndex)
        {
            Label lblCourseSectionID = (Label)gvResourceRegistration.Rows[rowIndex].FindControl("CourseSectionID");
            Guid courseSectionID = Guid.Parse(lblCourseSectionID.Text);
            return courseSectionID;
        }

        private void CancelSession(Guid sessionID)
        {
            Session oSession = STATSModel.Session.Get(sessionID);
            STATSModel.Resource oResource = this.ResourceObject;
            Session[] eSessions = oResource.TutoringSessions;

            var RelevantSessionIDs = new List<Guid>();
            foreach (var eSession in eSessions)
            {
                if (eSession.SessionDateTimeStart.TimeOfDay == oSession.SessionDateTimeStart.TimeOfDay && eSession.SessionDateTimeStart.DayOfWeek == oSession.SessionDateTimeStart.DayOfWeek)
                {
                    RelevantSessionIDs.Add(eSession.SessionID);
                }
            }

            var context = new STATSModel.STATSEntities();

            var eSessionsInContext = (from STATSModel.Session obj in context.Sessions where RelevantSessionIDs.Contains(obj.SessionID) select obj).ToList();

            foreach (var eSession in eSessionsInContext)
            {
                eSession.SessionIsCancelled = true;

                if (eSession.TeamworksID.HasValue && eSession.TeamworksID.Value != 0)
                {
                    STATSModel.Teamworks.TeamworksAPI.RemoveSession(eSession.TeamworksID.Value);
                }

                var textMessagesInContext = (from STATSModel.TextMessage obj in context.TextMessages
                                             where obj.ResourceID == oResource.ResourceID && obj.SessionID == eSession.SessionID
                                             select obj).ToList();

                //Delete any associated text message entries
                textMessagesInContext.ForEach(context.DeleteObject);
            }

            context.SaveChanges();
        }

        private void RemoveGroupSessionStudent(Guid sessionID, Guid studentResourceID)
        {
            //Note - the sessionID is unique to the student so we can use that to remove
            Session eSession = STATSModel.Session.Get(sessionID);

            STATSModel.Resource eResourceTutor = this.ResourceObject;
            Session[] allTutoringSessions = eResourceTutor.TutoringSessions;
            List<Session> applicableSessions = new List<Session>();
            foreach (Session session in allTutoringSessions)
            {
                if (session.SessionDateTimeStart.DayOfWeek == eSession.SessionDateTimeStart.DayOfWeek
                    && session.SessionDateTimeStart.TimeOfDay == eSession.SessionDateTimeStart.TimeOfDay
                    && session.CourseSectionID == eSession.CourseSectionID
                    )
                    applicableSessions.Add(session);
            }

            var context = new STATSModel.STATSEntities();

            //Remove the schedule for the user
            foreach (Session session in applicableSessions)
            {
                var schedulesInContext = (from STATSModel.Schedule obj in context.Schedules
                                          where obj.SessionID == session.SessionID && obj.ResourceID == studentResourceID
                                          select obj).ToList();
                schedulesInContext.ForEach(context.DeleteObject);
            }

            context.SaveChanges();
        }

        protected void gvTutorSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var eSession = (STATSModel.Session)e.Row.DataItem;
                ((Label)e.Row.FindControl("SessionID")).Text = eSession.SessionID.ToString();
                ((Label)e.Row.FindControl("CourseDisplayName")).Text = eSession.SessionTutors.Single().Course.CourseDisplayName;
                ((Label)e.Row.FindControl("SessionMeetingTime")).Text = eSession.SessionDateTimeStart.DayOfWeek.ToString() + " " + eSession.SessionDateTimeStart.TimeOfDay.ToString();
                ((Label)e.Row.FindControl("lblLocation")).Text = eSession.SessionType.SessionTypeName;
                ((CheckBox)e.Row.FindControl("chkVirtual")).Checked = eSession.Schedules.First().Virtual;
                Button btnRemoveStudent = (Button)e.Row.FindControl("btnRemoveStudent");
                btnRemoveStudent.Visible = eSession.IsGroup;
                // If the Resource is a tutor, we want the summary to display the student's name.
                // If the Resource is a student, we want the summary to display the tutor's name.
                var lblDisplayName = (Label)e.Row.FindControl("TutorDisplayName");

                if (ResourceObject.ResourceTypeID == ResourceTypeStudent.ResourceTypeID)
                {
                    lblDisplayName.Text = eSession.GetTutor(ResourceTypeTutor).ResourceNameDisplayLastFirst;
                    //The resource ID needs to be that of the student's (even though tutor's name is displayed)
                    //This is so the student can still be removed from a group session
                    ((Label)e.Row.FindControl("ResourceID")).Text = this.ResourceObject.ResourceID.ToString(); 
                }
                else
                {
                    if (eSession.IsGroup)
                    {
                        Resource student = Resource.Get(eSession.ResourceID);
                        lblDisplayName.Text = student.ResourceNameDisplayLastFirst;
                        ((Label)e.Row.FindControl("ResourceID")).Text = eSession.ResourceID.ToString();
                    }
                    else
                    {
                        Resource[] eResourceStudents = eSession.GetStudents();
                        lblDisplayName.Text = eResourceStudents.First().ResourceNameDisplayLastFirst;
                        ((Label)e.Row.FindControl("ResourceID")).Text = eResourceStudents.First().ResourceID.ToString();
                    }
                }
                
                //Only enable the checkbox if the resource meets the requirement to receive text messages
                Boolean canText = (ResourceObject.ResourceTextingEnabled);
                Boolean hasCell = (ResourceObject.CarrierID != Guid.Empty);

                if (!canText || !hasCell)
                {
                    ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Enabled = false;
                }
                else
                {
                    if (TextMessage.Get(ResourceObject, eSession.SessionID, null).Count > 0)
                        ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Checked = true;
                    else
                        ((CheckBox)e.Row.FindControl("chkSendTextMessage")).Checked = false;
                }
            }
        }

        protected void btnClearAll_Click(object sender, EventArgs e)
        {
            //Drop all courses
            CourseSection[] eCourseSections = ResourceRegistration.GetRegisteredCourses(ResourceObject);
            foreach (var courseSection in eCourseSections)
            {
                ResourceRegistration.DropCourse(courseSection, ResourceObject);
            }

            //Delete any annotations/schedules and cancel sessions
            Schedule.Delete(ResourceObject);

            if (ButtonAddEvent != null)
            {
                ButtonAddEvent();
            }
        }

        protected void MessagingModuleHide_Click(object sender, EventArgs e)
        {
            upSendMessage.Visible = false;
            panelResourceRegistration.Visible = true;
            panelResourceRegistration.Focus();
        }

        protected void chkSendTextMessage_CheckedChanged(object sender, EventArgs e)
        {
            int selRowIndex = ((GridViewRow)(((CheckBox)sender).Parent.Parent)).RowIndex;
            CheckBox cb = (CheckBox)gvResourceRegistration.Rows[selRowIndex].FindControl("chkMessage");

            if (cb.Checked)
            {
                Boolean success = STATSModel.TextMessage.CreateTextMessage(ResourceObject, null, GetCourseSectionIDFromRow(selRowIndex));
            }
            else
            {
                //Remove Text Message
                Boolean success = STATSModel.TextMessage.DeleteTextMessage(ResourceObject, null, GetCourseSectionIDFromRow(selRowIndex));
            }
        }

        protected void chkSendTextMessage2_CheckedChanged(object sender, EventArgs e)
        {
            int selRowIndex = ((GridViewRow)(((CheckBox)sender).Parent.Parent)).RowIndex;
            CheckBox cb = (CheckBox)gvTutorSummary.Rows[selRowIndex].FindControl("chkSendTextMessage");

            if (cb.Checked)
            {
                Boolean success = STATSModel.TextMessage.CreateTextMessage(ResourceObject, GetSessionIDFromRow(selRowIndex), null);
            }
            else
            {
                //Remove Text Message
                Boolean success = STATSModel.TextMessage.DeleteTextMessage(ResourceObject, GetSessionIDFromRow(selRowIndex), null);
            }
        }

        protected void chkVirtual_CheckedChanged(object sender, EventArgs e)
        {
            int selRowIndex = ((GridViewRow)(((CheckBox)sender).Parent.Parent)).RowIndex;
            CheckBox cb = (CheckBox)gvTutorSummary.Rows[selRowIndex].FindControl("chkVirtual");

            var context = new STATSModel.STATSEntities();
            Guid sessionID = GetSessionIDFromRow(selRowIndex);
            //Remove the schedule for the user
            var schedulesInContext = (from STATSModel.Schedule obj in context.Schedules
                                          where obj.SessionID == sessionID
                                      select obj).ToList();


            if (cb.Checked)
            {
                foreach (Schedule schedule in schedulesInContext)
                {
                    schedule.Virtual = true;
                }
            }
            else
            {
                foreach (Schedule schedule in schedulesInContext)
                {
                    schedule.Virtual = false;
                }
            }

            context.SaveChanges();
        }
    }
}