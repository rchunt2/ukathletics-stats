﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceRegistrationSummary.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourceRegistrationSummary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="Messaging" Src="~/UserControls/Messaging.ascx" %>

<script type="text/javascript">
    document.write("\<script src='http://code.jquery.com/jquery-latest.min.js' type='text/javascript'>\<\/script>");
</script>
<script type="text/javascript">
    function confirmProceed(confirmed) {
        if (confirmed) {
            $('input[id*="hfPageToggle"][type="hidden"]').val("true");
                            alert("you have been confirmed");
        }
        else {
            $('input[id*="hfPageToggle"][type="hidden"]').val("false");
                            alert("you have NOT been confirmed");
            window.location = window.location;
        }
    }
</script>

<div id="ResourceRegSummary">
    <input type="hidden" id="hfPageToggle" runat="server" />
    <asp:UpdatePanel ID="upSendMessage" runat="server" Visible="false">
            <ContentTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server">Return to List</asp:HyperLink>
            <br />
            <SIS:Messaging ID="MessagingModule" runat="server" />
            <asp:Button ID="MessagingModuleHide" Text="Close Mail" runat="server" OnClick="MessagingModuleHide_Click" />
            </ContentTemplate>
            </asp:UpdatePanel>
     </asp:UpdatePanel>
    <asp:Panel ID="panelResourceRegistration" runat="server" Visible="true">
        <asp:GridView ID="gvResourceRegistration" runat="server" CellPadding="2" AutoGenerateColumns="false" 
                EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left" Width="800px" AllowPaging="false"
                onrowdatabound="gvResourceRegistration_RowDataBound" 
                onrowcommand="gvResourceRegistration_RowCommand" >
                <Columns>
                    <asp:TemplateField HeaderText="CourseSectionID_HIDDEN" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="CourseSectionID" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CourseDisplayName" HeaderText="Course" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionNumber" HeaderText="Section" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionDaysMeet" HeaderText="Days Meet" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeStart" HeaderText="Start Time" DataFormatString="{0:t}" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CourseSectionTimeEnd" HeaderText="End Time" DataFormatString="{0:t}" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Send Text">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSendTextMessage" runat="server" AutoPostBack="true" OnCheckedChanged="chkSendTextMessage_CheckedChanged"  />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="EmailProfessor" Text="Email Professor"   />
                    <asp:ButtonField CommandName="DropCourse" Text="Drop Course" />
                    <asp:ButtonField CommandName="CreateTutorSession" Text="Schedule Tutoring" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <div style="clear: both; height: 0;"></div><br />
    </asp:Panel>

    <asp:Panel ID="panelResourceTutoringRegistration" runat="server" Visible="true" Width="586px">
        <div style="clear: both; height: 0;"></div>
        <div style="border: 1px solid #808080; margin-top: 2px;">
            <div>Select a Subject, Course and a Tutor below:</div>
            <asp:DropDownList ID="ddlTutorSubject" runat="server" 
                onselectedindexchanged="ddlTutorSubject_SelectedIndexChanged" 
                AutoPostBack="True"></asp:DropDownList>
            <asp:DropDownList ID="ddlTutorCourse" runat="server" 
                onselectedindexchanged="ddlTutorCourse_SelectedIndexChanged" 
                AutoPostBack="True"></asp:DropDownList>
            <asp:DropDownList ID="ddlAvailableTutors" runat="server" 
                onselectedindexchanged="ddlAvailableTutors_SelectedIndexChanged" 
                AutoPostBack="True"></asp:DropDownList>
            <asp:HyperLink ID="hyperlinkCheckAvailability" runat="server">Check Availability</asp:HyperLink>
            <asp:CheckBox ID="chkAllowDouble" runat="server" Text="Conflict Check" 
                Enabled="True" Checked="true" />
            <div>
                <telerik:RadDateTimePicker ID="dtpkTutorSession" runat="server" 
                    style="margin-bottom: 0px" Culture="en-US" EnableTyping="False">
                    <timeview cellspacing="-1" endtime="23:00:00" interval="00:30:00" starttime="08:00:00"></timeview>
                    <timepopupbutton hoverimageurl="" imageurl="" />
                    <calendar usecolumnheadersasselectors="False" userowheadersasselectors="False" 
                        viewselectortext="x" CellDayFormat="ddd" DayCellToolTipFormat="ddd" 
                        EnableMonthYearFastNavigation="False" EnableNavigation="False" 
                        ShowColumnHeaders="False" ShowDayCellToolTips="False" ShowRowHeaders="False" 
                        SingleViewRows="1" TitleFormat="'Day Picker'"></calendar>
                    <dateinput dateformat="M/d/yyyy" displaydateformat="dddd hh:mm tt" 
                        ReadOnly="True"></dateinput>
                    <datepopupbutton hoverimageurl="" imageurl="" />
                </telerik:RadDateTimePicker>
            </div>
            <div>
                <asp:DropDownList ID="ddlLocation" runat="server" 
                AutoPostBack="True"></asp:DropDownList>

                <asp:CheckBox ID="cbxVirtual" runat="server" Text="Virtual" />
            </div>
        <div>
        <asp:Button ID="btnTutoringAdd" runat="server" Text="Schedule Session" onclick="btnTutoringAdd_Click" /></div></div>
    </asp:Panel>

    <asp:Panel ID="panelTutorSummary" runat="server">
        <asp:GridView ID="gvTutorSummary" runat="server"  CellPadding="2" AutoGenerateColumns="false" 
            EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left" Width="586px" AllowPaging="false"
            onrowdatabound="gvTutorSummary_RowDataBound" 
            onrowcommand="gvTutorSummary_RowCommand" >
            <Columns>
                <asp:TemplateField HeaderText="SessionID_HIDDEN" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="SessionID" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tutor">
                    <ItemTemplate>
                        <asp:Label ID="TutorDisplayName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Student" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="StudentDisplayName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ResourceID_HIDDEN" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="ResourceID" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Course">
                    <ItemTemplate>
                        <asp:Label ID="CourseDisplayName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Scheduled Time">
                    <ItemTemplate>
                        <asp:Label ID="SessionMeetingTime" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Virtual">
                    <ItemTemplate>
                            <asp:CheckBox ID="chkVirtual" AutoPostBack="true" OnCheckedChanged="chkVirtual_CheckedChanged" runat="server" />
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Send Text">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSendTextMessage" AutoPostBack="true" OnCheckedChanged="chkSendTextMessage2_CheckedChanged" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                <asp:ButtonField CommandName="CancelSession" Text="Cancel Session" />
                <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:Button ID="btnRemoveStudent" CommandArgument='<%# Container.DataItemIndex%>' Text="Remove Student" CommandName="RemoveStudent" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <div style="text-align:right; width:586px;">
            <asp:Button runat="server" ID="btnClearAll" Text="Clear Plan Sheet" OnClick="btnClearAll_Click" OnClientClick="return confirm('Are you sure you want to clear ALL the sessions, courses, schedules, and annotations for this resource?');" />
        </div>
        <div class="clear"></div>
    </asp:Panel>
</div>
