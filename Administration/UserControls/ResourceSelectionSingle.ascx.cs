﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourceSelectionSingle : System.Web.UI.UserControl
    {
        public class ResourceSelectionUpdatedEventArgs : EventArgs
        {
            public Guid? SelectedResourceID { get; private set; }
            public ResourceSelectionUpdatedEventArgs(Guid? ResourceID)
            {
                SelectedResourceID = ResourceID;
            }
        }
        public delegate void SelectionChanged(ResourceSelectionUpdatedEventArgs e);
        public event SelectionChanged OnSelectionChanged;

        private Guid? SelectedResourceID { get; set; }

        public Guid? ResourceTypeIDFilter { get; set; }

        public Boolean AllowNullSelection { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                // Initial data bind.
                Bind();
            }
        }

        private void Bind()
        {
            Resource[] eResources;

            if (AllowNullSelection)
            {
                ddlResource.Items.Add(new ListItem("All", ""));
            }

            if (ResourceTypeIDFilter.HasValue && ResourceTypeIDFilter.Value != Guid.Empty)
            {
                ResourceType eResourceType = ResourceType.Get(ResourceTypeIDFilter.Value);
                eResources = Resource.Get(eResourceType);
            }
            else
            {
                eResources = Resource.Get();
            }

            eResources = (from Resource obj in eResources orderby obj.ResourceNameLast ascending , obj.ResourceNameFirst ascending select obj).ToArray();

            ddlResource.DataTextField = "ResourceNameDisplayLastFirst";
            ddlResource.DataValueField = "ResourceID";
            ddlResource.DataSource = eResources;
            ddlResource.DataBind();

            RunWhenSelectionChanges();
        }

        protected void btnConfirmSelection_Click(object sender, EventArgs e)
        {
            RunWhenSelectionChanges();
        }

        protected void ddlResource_SelectedIndexChanged(object sender, EventArgs e)
        {
            RunWhenSelectionChanges();
        }

        private void RunWhenSelectionChanges()
        {
            String strSelectedValue = ddlResource.SelectedValue;
            Guid? SelectedID = null;

            // Is it a null value (or an empty Guid?)
            if (String.IsNullOrWhiteSpace(strSelectedValue) || strSelectedValue == Guid.Empty.ToString())
            {
                SelectedID = null;
                //lblSelected.Text = "No Resource Selected";
            }
            else
            {
                SelectedID = Guid.Parse(strSelectedValue);
                //lblSelected.Text = Resource.Get(SelectedID.Value).ResourceNameDisplayLastFirst;
            }

            SelectedResourceID = SelectedID;

            if (OnSelectionChanged != null)
            {
                OnSelectionChanged(new ResourceSelectionUpdatedEventArgs(SelectedID));
            }
        }
    }
}