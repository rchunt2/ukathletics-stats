﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class TimeReview : UK.STATS.Administration.Pages.UKBaseControl
    {
        // If the Resource Object hasn't been initialized, then do so.
        // This allows us to use the object multiple times without the costs associated with instantiation.

        public delegate void FreeTimeUpdatedDelegate();
        public event FreeTimeUpdatedDelegate OnFreeTimeUpdated;

        public delegate void TimeOverriddenDelegate();
        public event TimeOverriddenDelegate OnTimeOverridden;




        public Guid ResourceID
        {
            get { return GetFromViewState<Guid>("TimeReview_ResourceID"); }
            set { StoreInViewstate("TimeReview_ResourceID", value); }
        }

        public void BindAndShow()
        {
            
            STATSModel.Resource eResource = STATSModel.Resource.Get(this.ResourceID);

            lblResourceName.Text = eResource.ResourceNameDisplayType;

            var eAttendances = STATSModel.Attendance.Get(eResource);
            eAttendances = (
                                from STATSModel.Attendance obj in eAttendances 
                                where 
                                    (
                                        obj.AttendanceTimeOut.HasValue 
                                        &&
                                        (
                                            (
                                                !obj.Schedule.Session.SessionType.SessionTypeName.Contains("Study Hall")
                                                &&
                                                (
                                                    (obj.AttendanceTimeOut.Value - obj.AttendanceTimeIn).TotalMinutes >= 1
                                                    ||
                                                    (obj.AttendanceTimeOut.Value - obj.AttendanceTimeIn).TotalMinutes < 0
                                                )
                                            )
                                            ||
                                            (
                                                obj.Schedule.Session.SessionType.SessionTypeName.StartsWith("Study Hall")
                                            )
                                        )
                                    )
                                    orderby obj.AttendanceTimeIn descending
                                select obj
                            ).ToArray();
            gvReview.DataSource = eAttendances;
            gvReview.DataBind();

            gvTotalFreeTime.DataSource = STATSModel.FreeTime.Get(eResource).OrderByDescending(x=>x.FreeTimeApplyDate);
            gvTotalFreeTime.DataBind();

            panelReview.Visible = true;
            lblResourceName.Visible = true;
            panelFreeTime.Visible = true;
            this.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Binds Session Type Drop Down for Free Time Module
                BindStudents();
                datepkFreeTime.SelectedDate = DateTime.Today;
            }
        }

        protected void gvReview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var gv = (GridView) sender;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var eAttendance = (STATSModel.Attendance)e.Row.DataItem;
                ((Label)e.Row.FindControl("AttendanceID")).Text = eAttendance.AttendanceID.ToString(); // What does this do?
                int timeCounted = 0;

                var lblTimeActual = ((Label)e.Row.FindControl("lblTimeActual"));
                if (eAttendance.AttendanceTimeOut.HasValue)
                {
                    TimeSpan duration = (eAttendance.AttendanceTimeOut.Value - eAttendance.AttendanceTimeIn);
                    //TimeTutoring += (int)Math.Ceiling((decimal)DurationInSeconds / 60);
                    int iDuration = (int)Math.Ceiling((decimal)duration.TotalSeconds / 60);
                    //int iDuration = (int)Math.Ceiling(duration.TotalMinutes);
                    lblTimeActual.Text = iDuration.ToString();
                }
                else
                {
                    lblTimeActual.Text = "Active";
                }

                if (eAttendance.AttendanceTimeOverride.HasValue)
                {
                    timeCounted = eAttendance.AttendanceTimeOverride.Value;
                } 
                else
                {
                    string sessionType = ((Label)e.Row.FindControl("lblSessionType")).Text;
                    if (sessionType == "Computer Lab")
                    {
                        timeCounted = 0; //Computer Lab time does not get counted unless it is overridden
                    }
                    else if (eAttendance.AttendanceTimeOut.HasValue)
                    {
                        TimeSpan duration = eAttendance.AttendanceTimeOut.Value - eAttendance.AttendanceTimeIn;
                        timeCounted = ((int)Math.Ceiling((decimal)duration.TotalSeconds / 60));
                        //timeCounted = ((int)Math.Ceiling(duration.TotalMinutes));
                    } 
                    else
                    {
                        timeCounted = -1;
                    }
                }

                if (e.Row.RowIndex != gv.EditIndex)
                {
                    if (e.Row.FindControl("lblTimeCounted") != null)
                    {
                        Label lblTimeCounted;
                        lblTimeCounted = ((Label)e.Row.FindControl("lblTimeCounted"));
                        if (timeCounted < 0)
                        {
                            lblTimeCounted.Text = "Active";
                            var lb = (LinkButton)e.Row.FindControl("lbtnOverride");
                            lb.Enabled = false;
                        }
                        else
                        {
                            lblTimeCounted.Text = timeCounted.ToString();
                        }
                    }
                    
                    if (e.Row.FindControl("lbtnOverride") != null)
                    {
                        ((LinkButton)e.Row.FindControl("lbtnOverride")).CommandArgument = e.Row.RowIndex.ToString();
                    }
                }
                else
                {
                    TextBox txtTimeCounted;
                    if (e.Row.FindControl("txtTimeCounted") != null)
                    {
                        txtTimeCounted = ((TextBox)e.Row.FindControl("txtTimeCounted"));
                        txtTimeCounted.Text = timeCounted.ToString();
                    }
                }
            }
        }

        protected void gvReview_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridView GV = (GridView)sender;
            //Override
            if (e.CommandName == "Override")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                GV.EditIndex = i;
                BindAndShow();
            }

            //Save
            if (e.CommandName == "Save")
            {
                int i = Convert.ToInt32(GV.EditIndex);
                TextBox txtTimeCounted = (TextBox)GV.Rows[i].Cells[0].FindControl("txtTimeCounted");
                Label lblAttendanceID = (Label)GV.Rows[i].Cells[0].FindControl("AttendanceID");
                Guid AttendanceID = Guid.Parse(lblAttendanceID.Text);

                // NOTE: Improper use of logic outside of the EntityModel!
                var context = new STATSEntities();
                var eAttendance = (from STATSModel.Attendance obj in context.Attendances where obj.AttendanceID == AttendanceID select obj).First();
                eAttendance.AttendanceTimeOverride = Convert.ToInt32(txtTimeCounted.Text);
                context.SaveChanges();
                
                GV.EditIndex = -1;

                if (OnTimeOverridden != null)
                {
                    OnTimeOverridden();
                }

                BindAndShow();
            }            
        }

        //Bind Course Session Types for Free Time Module
        private void BindStudents()
        {
            ddlSessionType.Items.Clear();

            List<SessionType> AllSessionTypes = new List<SessionType>();
            AllSessionTypes.Add(SessionType.GetByName("Study Hall - CATS"));
            AllSessionTypes.Add(SessionType.GetByName("Study Hall - Football"));
            AllSessionTypes.Add(SessionType.GetByName("Computer Lab"));
            AllSessionTypes.Add(SessionType.GetByName("Tutoring - CATS"));
            AllSessionTypes.Add(SessionType.GetByName("Tutoring - Football"));

            foreach (STATSModel.SessionType SessionTypeObj in AllSessionTypes)
            {
                if (SessionTypeObj.SessionTypeName != "Class")
                {
                    String liName = SessionTypeObj.SessionTypeName;
                    String liValue = SessionTypeObj.SessionTypeID.ToString();

                    ListItem li = new ListItem(liName, liValue);
                    ddlSessionType.Items.Add(li);    
                }
            }
        }

        protected void gvTotalFreeTime_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var gv = (GridView)sender;

            if (e.CommandName != "RemoveTime") return;

            int i = Convert.ToInt32(e.CommandArgument);
            var lblFreeTimeID = (Label)gv.Rows[i].Cells[0].FindControl("lblFreeTimeID");

            Guid freeTimeId = Guid.Parse(lblFreeTimeID.Text.ToString());

            // NOTE: Improper use of logic outside of the EntityModel!
            var context = new STATSEntities();
            var eFreeTime = (from STATSModel.FreeTime obj in context.FreeTimes where obj.FreeTimeID == freeTimeId select obj).First();
            context.DeleteObject(eFreeTime);
            context.SaveChanges();

            if (OnFreeTimeUpdated != null)
            {
                OnFreeTimeUpdated();
            }

            BindAndShow();
        }

        protected void btnAddFreeTime_Click(object sender, EventArgs e)
        {
            Boolean error = false;
            String errorText = "";

            if (txtFreeTime.Text == String.Empty)
            {
                error = true;
                errorText = "You must provide the number of minutes to be added!";
            }

            if (!datepkFreeTime.SelectedDate.HasValue)
            {
                error = true;
                errorText = "You must select the date that the added time will be applied to!";
            }

            if (Guid.Parse(ddlSessionType.SelectedValue) == Guid.Empty)
            {
                error = true;
                errorText = "You must select a Session Type that the added time will be counted as!";
            }

            if (error)
            {
                ScriptManager.RegisterClientScriptBlock(btnAddFreeTime, btnAddFreeTime.GetType(), "FreeTimeNotifier", "<script language='javascript'>alert('" + errorText + "');</script>", false);
                return;
            }
            

            STATSModel.Resource eResource = STATSModel.Resource.Get(this.ResourceID);
            
            Guid SessionTypeID = Guid.Parse(ddlSessionType.SelectedValue);
            STATSModel.SessionType eSessionType = STATSModel.SessionType.Get(SessionTypeID);
            DateTime ApplyOnDay = datepkFreeTime.SelectedDate.GetValueOrDefault();
            int AmountInMinutes = Convert.ToInt32(txtFreeTime.Text);

            STATSModel.FreeTime.Add(eResource, eSessionType, ApplyOnDay, AmountInMinutes);

            datepkFreeTime.SelectedDate = null;
            txtFreeTime.Text = "";

            if (OnFreeTimeUpdated != null)
            {
                OnFreeTimeUpdated();
            }

            BindAndShow();
        }

        protected void gvTotalFreeTime_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.DataRow )
            {
                var objFreeTime = (STATSModel.FreeTime)e.Row.DataItem;
                var lblFreeTimeID = (Label)e.Row.Cells[0].FindControl("lblFreeTimeID");
                lblFreeTimeID.Text = objFreeTime.FreeTimeID.ToString();
            }
        }

        protected void gvReview_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            
        }
    }
}