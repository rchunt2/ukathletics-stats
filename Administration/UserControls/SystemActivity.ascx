﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemActivity.ascx.cs" Inherits="UK.STATS.Administration.UserControls.SystemActivity" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:UpdatePanel ID="upActivity" runat="server">
    <ContentTemplate>
        <telerik:RadTabStrip ID="rtsActivity" runat="server" Width="100%" 
            SelectedIndex="0" AutoPostBack="True" MultiPageID="rmpActivity">
            <Tabs>
                <telerik:RadTab runat="server" PageViewID="rpvAdministration" Text="Administration" Selected="True">
                </telerik:RadTab>
                <telerik:RadTab runat="server" PageViewID="rpvStudyHall" Text="Study Hall">
                </telerik:RadTab>
                <telerik:RadTab runat="server" PageViewID="rpvTutoring" Text="Tutoring">
                </telerik:RadTab>
                <telerik:RadTab runat="server" PageViewID="rpvComputerLab" Text="Computer Lab">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><%-- Skin="Web20"--%>
        <telerik:RadMultiPage ID="rmpActivity" runat="server" RenderSelectedPageOnly="True" SelectedIndex="0">
            <telerik:RadPageView ID="rpvAdministration" runat="server" Selected="True">
                <telerik:RadGrid ID="rgAdmin" runat="server" AllowSorting="True" 
                    CellSpacing="0" GridLines="Vertical">
                </telerik:RadGrid>
            </telerik:RadPageView>
            <telerik:RadPageView ID="rpvStudyHall" runat="server">
                <telerik:RadGrid ID="rgStudyHall" runat="server" AllowSorting="True" 
                    CellSpacing="0" GridLines="Vertical">
                </telerik:RadGrid>
            </telerik:RadPageView>
            <telerik:RadPageView ID="rpvTutoring" runat="server">
                <telerik:RadPanelBar ID="rpbTutoring" runat="server" Width="100%" 
                    AllowCollapseAllItems="True" ExpandMode="SingleExpandedItem">
                    <ItemTemplate>
                        <telerik:RadGrid ID="rgTutoring" runat="server" Width="100%">
                        </telerik:RadGrid>
                    </ItemTemplate>
                </telerik:RadPanelBar>
            </telerik:RadPageView>
            <telerik:RadPageView ID="rpvComputerLab" runat="server">
                <telerik:RadGrid ID="rgComputerLab" runat="server" AllowSorting="True" 
                    CellSpacing="0" GridLines="Vertical">
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </ContentTemplate>
</asp:UpdatePanel>
