﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubjectSearch.ascx.cs" Inherits="UK.STATS.Administration.UserControls.SubjectSearch" %>
<asp:Panel ID="panelSubjectSearch" runat="server" Visible="true" DefaultButton="btnSearch">
        <div id="SubjectSearchModule">
            <div class="Header">Search For a Class Prefix</div>
            <asp:TextBox ID="txtSearchQuery" runat="server"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
            <asp:PlaceHolder ID="phSubject" runat="server" />
        </div>
        <asp:Label ID="lblResultsCount" runat="server" Text="Label" Visible="false"></asp:Label><br />
        <asp:GridView ID="gvSearchResults" runat="server" CellPadding="2" AutoGenerateColumns="false" 
                EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left" 
            Width="100%" AllowPaging="True" PageSize="10" 
            onpageindexchanging="gvSearchResults_PageIndexChanging" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SubjectNameShort" HeaderText="Class Prefix" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SubjectName" HeaderText="Subject Name" Visible="False" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SubjectDescription" HeaderText="Class Prefix Description" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="SubjectID" DataNavigateUrlFormatString="../Pages/Subject.aspx?subjectid={0}" Text="View Details" />
                    <asp:HyperLinkField DataNavigateUrlFields="SubjectID" DataNavigateUrlFormatString="../Pages/Course.aspx?subjectid={0}" Text="View Courses" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <br />
    </asp:Panel>