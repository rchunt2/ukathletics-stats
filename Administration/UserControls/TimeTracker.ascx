﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeTracker.ascx.cs" Inherits="UK.STATS.Administration.UserControls.TimeTracker" %>

<style type="text/css">
    .TimeTracker
    {
    	border: 1px solid #ddd;
    	background-color: #eee;
    	width: 650px;
    	padding: 3px;
    	font-family: Arial;
    }
    
    .TimeTrackerTitle
    {
    	font-size: 14pt;
    	text-align: center;
    	font-weight: bold;
    }
    
    .TimeTrackerResourceName
    {
    	text-align: center;
    }
    
    .TimeTrackerDateRange
    {
    	text-align: center;
    }
    
    .TimeTrackerLeftCol
    {
    	font-weight: bold;
    } 

</style>

<div class="TimeTracker">
    <asp:Panel ID="panelTimeTrackerView" runat="server">
        <div class="TimeTrackerTitle"><asp:Label ID="lblTimeTrackertitle" runat="server" Text="Time Tracking Title"></asp:Label></div>
        <div class="TimeTrackerResourceName"><asp:Label ID="lblResourceName" runat="server" Text="Resource Name"></asp:Label></div>
        <div class="TimeTrackerDateRange"><asp:Label ID="lblTimeTrackerDateRange" runat="server" Text="Date Range"></asp:Label></div>

        <table cellpadding="3">
            <thead>
                <tr>
                    <th></th>
                    <th>Sunday</th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="TimeTrackerLeftCol">Computer Lab</td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabSunday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabMonday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabTuesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabWednesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabThursday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabFriday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabSaturday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeCompLabTotal" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="TimeTrackerLeftCol">Tutoring</td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringSunday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringMonday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringTuesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringWednesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringThursday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringFriday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringSaturday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTutoringTotal" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="TimeTrackerLeftCol">Study Hall</td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallSunday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallMonday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallTuesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallWednesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallThursday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallFriday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallSaturday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeStudyHallTotal" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="TimeTrackerLeftCol">Total</td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalSunday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalMonday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalTuesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalWednesday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalThursday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalFriday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol">
                        <asp:Label ID="lblTimeTotalSaturday" runat="server" Text="0"></asp:Label>
                    </td>
                    <td class="TimeTrackerRightCol" style="background-color: yellow; font-weight: bold;">
                        <asp:Label ID="lblTimeOverallTotal" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>

    </div>
