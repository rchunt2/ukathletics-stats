﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;


namespace UK.STATS.Administration.UserControls
{
    public partial class TutoringSkills : UK.STATS.Administration.Pages.UKBaseControl
	{
        public delegate void SkillsChangedDelegate();
        public event SkillsChangedDelegate OnTutoringSkillsChanged;

        public Guid ResourceID
        {
            get { return GetFromViewState<Guid>("TutoringSkills_ResourceID"); }
            set { StoreInViewstate("TutoringSkills_ResourceID", value); }
        }

        public Boolean IsVisible
        {
            get { return GetFromViewState<Boolean>("TutoringSkills_IsVisible"); }
            set { StoreInViewstate("TutoringSkills_IsVisible", value); }
        }

        public void BindAndShow()
        {
            STATSModel.Resource ResourceObj = STATSModel.Resource.Get(this.ResourceID, true);

            if (ResourceObj.ResourceType.ResourceTypeName == "Tutor")
            {
                panelTutoringSkills.Visible = true;
                this.IsVisible = true;
            }
            else
            {
                panelTutoringSkills.Visible = false;
                this.IsVisible = false;
            }

            if (IsVisible)
            {
                STATSModel.Course[] eCourses = ResourceObj.GetCoursesTutored();
                lbSkills.Items.Clear();
                foreach (STATSModel.Course eCourse in eCourses)
                {
                    STATSModel.Subject SubjectObj = eCourse.Subject;
                    String Name = SubjectObj.SubjectNameShort + eCourse.CourseNumber + " - " + eCourse.CourseName;
                    String Value = eCourse.CourseID.ToString();
                    ListItem li = new ListItem(Name, Value);
                    lbSkills.Items.Add(li);
                }
                BindSubjects();
            }
        }

        public void BindSubjects()
        {
            ddlSubject.Items.Clear();
            STATSModel.Subject[] AllSubjects = STATSModel.Subject.Get();
            foreach (STATSModel.Subject SubjectObj in AllSubjects)
            {
                String liName = SubjectObj.SubjectNameShort;
                String liValue = SubjectObj.SubjectID.ToString();

                ListItem li = new ListItem(liName, liValue);
                ddlSubject.Items.Add(li);
            }

            BindCourses(AllSubjects[0].SubjectID);
        }

        private void BindCourses(Guid SubjectID)
        {
            ddlCourse.Items.Clear();
            STATSModel.Subject SubjectObj = STATSModel.Subject.Get(SubjectID);
            STATSModel.Course[] AllCourses = SubjectObj.Courses.ToArray();
            foreach (STATSModel.Course CourseObj in AllCourses)
            {
                String liName = SubjectObj.SubjectNameShort + CourseObj.CourseNumber;
                String liValue = CourseObj.CourseID.ToString();
                ListItem li = new ListItem(liName, liValue);
                ddlCourse.Items.Add(li);
            }
        }

        protected void ddlSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCourses(Guid.Parse(ddlSubject.SelectedValue));
        }

		protected void Page_Load(object sender, EventArgs e)
		{
		}

        protected void btnAddSkill_Click(object sender, EventArgs e)
        {
            Guid CourseID = Guid.Parse(ddlCourse.SelectedValue);
            Course CourseObj = Course.Get(CourseID);
            Resource eResource = Resource.Get(this.ResourceID);
            CourseObj.AddTutor(eResource); // Duplicate detection is done in the DAL.
            RunWhenTutorSkillsChange();
            BindAndShow();
        }

        protected void btnDeleteSkill_Click(object sender, EventArgs e)
        {
            if (lbSkills.SelectedItem != null)
            {
                Guid CourseID = Guid.Parse(lbSkills.SelectedValue);
                Course CourseObj = Course.Get(CourseID);
                Resource eResource = Resource.Get(this.ResourceID);
                CourseObj.RemoveTutor(eResource);
                RunWhenTutorSkillsChange();
                BindAndShow();
            }
        }

        protected void RunWhenTutorSkillsChange()
        {
            if (OnTutoringSkillsChanged != null)
            {
                OnTutoringSkillsChanged();
            }
        }
	}
}