﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IsOpenParameters.ascx.cs"
    Inherits="UK.STATS.Administration.UserControls.IsOpenParameters" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<br /><br />
<asp:Label ID="lblTutoring" runat="server" Text="Tutoring"></asp:Label>
<table width="100%" id="tblTutoring">
    <tr style="background-color: #507CD1; font-weight: bold; color: White">
        <td>
        </td>
        <td>
            Monday
        </td>
        <td>
            Tuesday
        </td>
        <td>
            Wednesday
        </td>
        <td>
            Thursday
        </td>
        <td>
            Friday
        </td>
        <td>
            Saturday
        </td>
        <td>
            Sunday
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align: right">Start</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorStartSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
    <tr>
        <td>
        <div style="text-align: right">End</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpTutorEndSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
</table>
<br /><br />
<asp:Label ID="lblFootballTutoring" runat="server" Text="Tutoring - Football Location"></asp:Label>
<table width="100%" id="tblFootballTutoring">
    <tr style="background-color: #507CD1; font-weight: bold; color: White">
        <td>
        </td>
        <td>
            Monday
        </td>
        <td>
            Tuesday
        </td>
        <td>
            Wednesday
        </td>
        <td>
            Thursday
        </td>
        <td>
            Friday
        </td>
        <td>
            Saturday
        </td>
        <td>
            Sunday
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align: right">Start</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorStartSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
    <tr>
        <td>
        <div style="text-align: right">End</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballTutorEndSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
</table>
<br /><br />
<asp:Label ID="lblStudyHall" runat="server" Text="Study Hall"></asp:Label>
<table width="100%" id="tblStudyHall">
    <tr style="background-color: #507CD1; font-weight: bold; color: White">
        <td>
        </td>
        <td>
            Monday
        </td>
        <td>
            Tuesday
        </td>
        <td>
            Wednesday
        </td>
        <td>
            Thursday
        </td>
        <td>
            Friday
        </td>
        <td>
            Saturday
        </td>
        <td>
            Sunday
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align: right">Start</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyStartSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
    <tr>
        <td>
        <div style="text-align: right">End</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpStudyEndSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
</table>
<br /><br />
<asp:Label ID="lblFootballStudyHall" runat="server" Text="Study Hall - Football Location"></asp:Label>
<table width="100%" id="tblFootballStudyHall">
    <tr style="background-color: #507CD1; font-weight: bold; color: White">
        <td>
        </td>
        <td>
            Monday
        </td>
        <td>
            Tuesday
        </td>
        <td>
            Wednesday
        </td>
        <td>
            Thursday
        </td>
        <td>
            Friday
        </td>
        <td>
            Saturday
        </td>
        <td>
            Sunday
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align: right">Start</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyStartSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
    <tr>
        <td>
        <div style="text-align: right">End</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpFootballStudyEndSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
</table>
<br /><br />
<%--<asp:Label ID="lblCompLab" runat="server" Text="Computer Lab"></asp:Label>
<table width="100%" id="tblCompLab">
    <tr style="background-color: #507CD1; font-weight: bold; color: White">
        <td>
        </td>
        <td>
            Monday
        </td>
        <td>
            Tuesday
        </td>
        <td>
            Wednesday
        </td>
        <td>
            Thursday
        </td>
        <td>
            Friday
        </td>
        <td>
            Saturday
        </td>
        <td>
            Sunday
        </td>
    </tr>
    <tr>
        <td>
            <div style="text-align: right">Start</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompStartSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
    <tr>
        <td>
        <div style="text-align: right">End</div>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndMonday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndTuesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndWednesday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndThursday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndFriday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndSaturday" runat="server">
            </telerik:RadTimePicker>
        </td>
        <td>
            <telerik:RadTimePicker ID="tpCompEndSunday" runat="server">
            </telerik:RadTimePicker>
        </td>
    </tr>
</table>--%>
<br />
<asp:Button ID="btnSave" runat="server" Text="Save" width="100px" 
    onclick="btnSave_Click"/>