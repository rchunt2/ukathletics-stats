﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourceSearch : System.Web.UI.UserControl
    {
        private String _queryString = "";
        private String _queryEmail = "";
        private String _queryAD = "";
        private String _queryPhone = "";
        private String _detailPage = "";
        private Guid _groupID;
        private Guid _typeID;

        public delegate void ButtonAddDelegate();
        public event ButtonAddDelegate ButtonAddEvent;

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Button btn = new Button();
            btn.ID = "btnCreate";
            btn.Text = "Create Resource";
            btn.Click += new EventHandler(btnCreate_Click);
            phResource.Controls.Add(btn);
        }
        
        public String QueryString
        {
            get { 
                    return _queryString;

                }
            
            set {
                    _queryString = value;
                }
        }

        public String QueryEmail
        {
            get
            {
                return _queryEmail;

            }

            set
            {
                _queryEmail = value;
            }
        }

        public String QueryAD
        {
            get
            {
                return _queryAD;

            }

            set
            {
                _queryAD = value;
            }
        }


        public String QueryPhone
        {
            get
            {
                return _queryPhone;

            }

            set
            {
                _queryPhone = value;
            }
        }

        public void RunSearch(String Query, String Email, String QueryAD, String Phone)
        {
            this.QueryString = Query;
            this.QueryEmail = Email;
            this.QueryAD = QueryAD;
            this.QueryPhone = Phone;
            this.RunSearch();
        }

        public String DetailsPage
        {
            get { return _detailPage; }
            set { _detailPage = value; }
        }

        public Boolean isVisible
        {
            get { return panelResourceSearch.Visible; }
            set { panelResourceSearch.Visible = value; }
        }

        public Guid ResourceTypeID
        {
            get { return this._typeID; }
            set { this._typeID = value; }
        }

        public Guid GroupID
        {
            get { return this._groupID; }
            set { this._groupID = value; }
        }

        public void RunSearch()
        {
            //STATSModel.Resource[] eResources = new Resource[0];
            List<STATSModel.Resource> eResources = new List<STATSModel.Resource>();

            Boolean FilterGroup = (this.GroupID != Guid.Empty);
            Boolean FilterType = (this.ResourceTypeID != Guid.Empty);

            if(FilterGroup && FilterType)
            {
                STATSModel.Group eGroup = STATSModel.Group.Get(GroupID);
                STATSModel.ResourceType eResourceType = STATSModel.ResourceType.Get(ResourceTypeID);
                eResources = STATSModel.Resource.Search(this.QueryString, eGroup, eResourceType).ToList();
            }
            else if(!FilterGroup && !FilterType)
            {
                eResources = STATSModel.Resource.Search(this.QueryString).ToList();
            }
            else
            {
                if(FilterGroup)
                {
                    STATSModel.Group eGroup = STATSModel.Group.Get(GroupID);
                    eResources = STATSModel.Resource.Search(this.QueryString, eGroup).ToList();
                } 
                else if(FilterType)
                {
                    STATSModel.ResourceType eResourceType = STATSModel.ResourceType.Get(ResourceTypeID);
                    eResources = STATSModel.Resource.Search(this.QueryString, eResourceType).ToList();
                }
            }

            if (!String.IsNullOrEmpty(this.QueryEmail))
                eResources = eResources.Where(x => x.ResourceEmail != null && x.ResourceEmail.Contains(this.QueryEmail)).ToList();

            if (!String.IsNullOrEmpty(this.QueryAD))
                eResources = eResources.Where(x => x.ResourceADLogin != null && x.ResourceADLogin.ToLower().Contains(this.QueryAD.ToLower())).ToList();


            if (!String.IsNullOrEmpty(this.QueryPhone))
                eResources = eResources.Where(x => x.ResourcePhoneNumber != null && x.ResourcePhoneNumber.Contains(this.QueryPhone)).ToList();



            lblResultsCount.Visible = true;
            lblResultsCount.Text = eResources.Count.ToString() + " results for '" + this.QueryString + "'";

            if (chkDisabledResources.Checked)
            {
                eResources = eResources.Where(x => x.ResourceIsActive.Value == false).ToList();
            }
            
            gvSearchResults.DataSource = eResources;
            gvSearchResults.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Guid SelectedGroupID = Guid.Parse(ddlGroupFilter.SelectedValue);
            this.GroupID = SelectedGroupID;
            Guid SelectedTypeID = Guid.Parse(ddlResourceTypeFilter.SelectedValue);
            this.ResourceTypeID = SelectedTypeID;
            RunSearch(txtSearchQuery.Text, txtSearchEmail.Text, txtSearchAD.Text, txtSearchPhone.Text);
        }

        protected void gvSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSearchResults.PageIndex = e.NewPageIndex;
            Guid SelectedGroupID = Guid.Parse(ddlGroupFilter.SelectedValue);
            this.GroupID = SelectedGroupID;
            Guid SelectedTypeID = Guid.Parse(ddlResourceTypeFilter.SelectedValue);
            this.ResourceTypeID = SelectedTypeID;
            RunSearch(txtSearchQuery.Text, txtSearchEmail.Text, txtSearchAD.Text, txtSearchPhone.Text);
            gvSearchResults.DataBind();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (ButtonAddEvent != null)
                ButtonAddEvent();
        }

        private void BindGroupFilter()
        {
            ddlGroupFilter.Items.Clear();

            STATSModel.Group[] eGroups = STATSModel.Group.Get();

            if (this.GroupID != Guid.Empty)
            {
                STATSModel.Group SelectedGroup = STATSModel.Group.Get(this.GroupID);
                String initialName2 = SelectedGroup.GroupName;
                String initialValue2 = SelectedGroup.GroupID.ToString();
                ListItem initial2 = new ListItem(initialName2, initialValue2);
                ddlGroupFilter.Items.Add(initial2);
            }

            String initialName = "All Groups";
            String initialValue = Guid.Empty.ToString();
            ListItem initial = new ListItem(initialName, initialValue);
            ddlGroupFilter.Items.Add(initial);
            foreach (STATSModel.Group eGroup in eGroups)
            {
                String liName = eGroup.GroupName;
                String liValue = eGroup.GroupID.ToString();
                ListItem li = new ListItem(liName, liValue);
                ddlGroupFilter.Items.Add(li);
                if (this.GroupID == eGroup.GroupID)
                {
                    ddlGroupFilter.SelectedIndex = ddlGroupFilter.Items.Count - 1;
                }
            }
        }

        private void BindResourceTypeFilter()
        {
            ddlResourceTypeFilter.Items.Clear();
            STATSModel.ResourceType[] AllResourceTypes = STATSModel.ResourceType.Get();
            String initialName = "All Resource Types";
            String initialValue = Guid.Empty.ToString();
            ListItem initial = new ListItem(initialName, initialValue);
            ddlResourceTypeFilter.Items.Add(initial);
            foreach (STATSModel.ResourceType eResourceType in AllResourceTypes)
            {
                String liName = eResourceType.ResourceTypeName;
                String liValue = eResourceType.ResourceTypeID.ToString();

                ListItem li = new ListItem(liName, liValue);
                ddlResourceTypeFilter.Items.Add(li);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGroupFilter();
                BindResourceTypeFilter();

                txtSearchQuery.Text = QueryString;
                txtSearchEmail.Text = QueryEmail;
                txtSearchPhone.Text = QueryPhone;
                ddlGroupFilter.SelectedValue = GroupID.ToString();
                ddlResourceTypeFilter.SelectedValue = ResourceTypeID.ToString();
            }

            
        }

        protected void gvSearchResults_Sorting(object sender, GridViewSortEventArgs e)
        {
        }

        protected void gvSearchResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                STATSModel.Resource eResource = (STATSModel.Resource)e.Row.DataItem;

                Control c = e.Row.FindControl("linkViewDetails");
                if (c != null)
                {
                    HyperLink link = (HyperLink) c;
                    Label lblResourceID = (Label)e.Row.FindControl("ResourceID");
                    lblResourceID.Text = eResource.ResourceID.ToString();

                    String PageUrl = Request.FilePath;
                    List<String> QueryParams = new List<String>();
                    if (!String.IsNullOrWhiteSpace(QueryString)) { QueryParams.Add("st=" + QueryString); }
                    if (GroupID != Guid.Empty) { QueryParams.Add("sg=" + GroupID.ToString()); }
                    if (ResourceTypeID != Guid.Empty) { QueryParams.Add("srt=" + ResourceTypeID.ToString()); }
                    QueryParams.Add("resourceid=" + eResource.ResourceID.ToString());

                    int max = QueryParams.Count;
                    for (int i = 0; i < max; i++)
                    {
                        if (i == 0) { PageUrl += "?"; } // query parameters always begin with a question mark
                        PageUrl += QueryParams[i]; // append our parameter
                        if (i+1 < max) { PageUrl += "&"; } // if this isnt the last parameter, put an ampersand to denote the presence of the following parameter
                    }
                    link.NavigateUrl = PageUrl;
                }
            }
        }
    }
}