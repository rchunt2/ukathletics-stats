﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.UserControls
{
    public partial class NavigationMenu : System.Web.UI.UserControl
    {
        //DM - 9/25/2012 - changed resource types to pull from Application variable to prevent lots of unnessary hits to the DB since they do not change.
        public STATSModel.ResourceType ResourceTypeDirector
        {
            get
            {
                if (Application["ResourceTypeDirector"] == null)
                {
                    Application["ResourceTypeDirector"] = STATSModel.ResourceType.Get("Director");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeDirector"];
            }
        }

        public STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = STATSModel.ResourceType.Get("Administrator");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        public STATSModel.ResourceType ResourceTypeTimeMgr
        {
            get
            {
                if (Application["ResourceTypeTimeMgr"] == null)
                {
                    Application["ResourceTypeTimeMgr"] = STATSModel.ResourceType.Get("Time Mgr");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTimeMgr"];
            }
        }

        public STATSModel.ResourceType ResourceTypeMonitor
        {
            get
            {
                if (Application["ResourceTypeMonitor"] == null)
                {
                    Application["ResourceTypeMonitor"] = STATSModel.ResourceType.Get("Monitor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeMonitor"];
            }
        }


        STATSModel.Authentication eAuthentication;
        STATSModel.Resource eResource;
        STATSModel.ResourceType eResourceType;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void tvNavigation_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            if (e.Node.Parent != null)
            {
                //Administrator - Everything except TimeReview, Settings
                //Director - Everything
                //Monitor - View ONLY Who's logged in, and Schedule information for ONLY those listed.
                //Time Manager - Everything except Settings
                //Student - Can't login.
                //Tutor - Can't login.

                //DM - 9/25/2012 - moved these vars to be global on the page since TreeNodeDataBound is called for each node, and it was hitting the DB 3x for each node, now it is just 3 calls per postback.
                if (eAuthentication == null) 
                {
                    eAuthentication = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication();
                    eResource = eAuthentication.Resource;
                    eResourceType = eResource.ResourceType;
                }

                if (eResourceType.ResourceTypeID == ResourceTypeAdministrator.ResourceTypeID)
                {
                    if (e.Node.Text == "Time Review" || e.Node.Text == "Settings")
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);
                    }
                }
                else if (eResourceType.ResourceTypeID == ResourceTypeDirector.ResourceTypeID)
                {
                    // Everything
                }
                else if (eResourceType.ResourceTypeID == ResourceTypeMonitor.ResourceTypeID)
                {
                    if (e.Node.Text != "Home" && e.Node.Text != "Group") //leave home in for monitor, may add other pages to leave access to here. Also leave in Group page
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);
                    }
                }
                else if (eResourceType.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID)
                {
                    if (e.Node.Text == "Settings")
                    {
                        e.Node.Parent.ChildNodes.Remove(e.Node);
                    }
                }
                else
                {
                    // Remove all the nodes!
                    e.Node.Parent.ChildNodes.Remove(e.Node);
                }
            }
            
        }
    }
}