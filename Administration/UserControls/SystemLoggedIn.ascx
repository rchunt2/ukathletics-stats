﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SystemLoggedIn.ascx.cs" Inherits="UK.STATS.Administration.UserControls.SystemLoggedIn" %>

<style>
    .SmallHiddenOverflow
    {
        overflow: hidden;
        width: 65px;
        height: 20px;
    }

    .machineCol
    {
        width: 130px;
    }
</style>
<asp:Label ID="lblLoggedInCount" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Button 
    ID="btnRefresh" runat="server" Text="Refresh" onclick="btnRefresh_Click" />
<br />
<asp:GridView ID="gvSystemLoggedIn" runat="server" CellPadding="4" 
    EnableTheming="True" ForeColor="#333333" 
    HorizontalAlign="Left" Width="100%" 
    AutoGenerateColumns="False" 
    onrowdatabound="gvSystemLoggedIn_RowDataBound" 
    onrowcommand="gvSystemLoggedIn_RowCommand" AllowSorting="True">
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:TemplateField HeaderText="AttendanceID_HIDDEN" Visible="false">
            <ItemTemplate>
                <asp:Label ID="AttendanceID" runat="server" Text='AttendanceID'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Session ID">
            <HeaderTemplate>
                <asp:LinkButton ID="lbSessionID" runat="server" Text="Session" CommandName="SortSessionID" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="SmallHiddenOverflow">
                    <asp:Label ID="lblSessionID" runat="server"></asp:Label>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Machine">
            <HeaderTemplate>
                <asp:LinkButton ID="lbMachine" runat="server" Text="Machine" CommandName="SortMachine" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="machineCol">
                    <asp:Label ID="lblMachine" runat="server"></asp:Label>
                </div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Location">
            <HeaderTemplate>
                <asp:LinkButton ID="lbLocation" runat="server" Text="Location" CommandName="SortLocations" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblSessionType" runat="server" Text='SessionTypeName'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ResourceType">
            <HeaderTemplate>
                <asp:LinkButton ID="lbResourceType" runat="server" Text="Type" CommandName="SortResourceType" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblResourceType" runat="server" Text='ResourceTypeName'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Name">
            <HeaderTemplate>
                <asp:LinkButton ID="lbName" runat="server" Text="Name" CommandName="SortName" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblName" runat="server" Text='ResourceName'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Status">
            <HeaderTemplate>
                <asp:LinkButton ID="lbStatus" runat="server" Text="Status" CommandName="SortStatus" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblStatus" runat="server" Text='Status'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Logged In Since">
            <HeaderTemplate>
                <asp:LinkButton ID="lbLoggedInSince" runat="server" Text="Logged In Since" CommandName="SortLoggedInSince" CommandArgument="Sessions" ForeColor="White"></asp:LinkButton>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="lblLoginTime" runat="server" Text='LoggedInSince'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Force Logout">
            <ItemTemplate>
                <%--<asp:Button ID="btnForceLogout" runat="server" Text="Force Lougout" CommandName="ForceLogout" />--%>
                <asp:LinkButton ID="btnForceLogout" runat="server" Text="Force Logout" CommandName="ForceLogout"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
<%--        <asp:ButtonField CommandName="ForceLogout" HeaderText="Force Logout" Text="Force Logout" />--%>
    </Columns>
    <EditRowStyle BackColor="#2461BF" />
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#EFF3FB" />
    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#F5F7FB" />
    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
    <SortedDescendingCellStyle BackColor="#E9EBEF" />
    <SortedDescendingHeaderStyle BackColor="#4870BE" />
</asp:GridView>
<telerik:RadToolTipManager ID="RadToolTipManager" runat="server" Position="BottomRight"
    Animation="Fade" OnAjaxUpdate="RadToolTipManager_OnAjaxUpdate" RelativeTo="Mouse" 
    RenderInPageRoot="true" Skin="Web20">
</telerik:RadToolTipManager>