﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScheduleSummary.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ScheduleSummary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .ScheduleSummary
    {
    	width: 600px;
    	padding: 2px;
    	margin: 5px;
    	border: 1px solid #ddd;
    	background-color: #eeeeee;
    }
    
    
</style>

<div class="ScheduleSummary">
    <div>
    <b>Detailed Report</b><br />
    <span style="font-size: 9pt">Use this to generate a detailed report of a 1-hour block of time starting from the selected time.</span>
    </div>
    <telerik:RadDateTimePicker ID="dtpkTime" runat="server" 
        style="margin-bottom: 0px" Culture="en-US" EnableTyping="False">
        <timeview cellspacing="-1" endtime="23:00:00" interval="00:30:00" starttime="08:00:00"></timeview>
        <timepopupbutton hoverimageurl="" imageurl="" />
        <calendar usecolumnheadersasselectors="False" userowheadersasselectors="False" 
            viewselectortext="x" CellDayFormat="ddd" DayCellToolTipFormat="ddd" 
            EnableMonthYearFastNavigation="False" EnableNavigation="False" 
            ShowColumnHeaders="False" ShowDayCellToolTips="False" ShowRowHeaders="False" 
            SingleViewRows="1" TitleFormat="'Day Picker'"></calendar>
        <dateinput dateformat="M/d/yyyy" displaydateformat="dddd hh:mm tt" 
            ReadOnly="True"></dateinput>
        <datepopupbutton hoverimageurl="" imageurl="" />
    </telerik:RadDateTimePicker>

    <asp:Button ID="btnRunReport" runat="server" Text="Detailed Schedule Report" 
        onclick="btnRunReport_Click" />

</div>