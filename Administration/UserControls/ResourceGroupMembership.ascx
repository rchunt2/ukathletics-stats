﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceGroupMembership.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourceGroupMembership" %>

<div id="GroupMemberModule">
    <div id="primaryGroup">
        <div class="Header">
            Primary Group Membership:
        </div>
        <asp:DropDownList ID="ddlPrimaryGroup" runat="server" Width="250px" OnSelectedIndexChanged="ddlPrimaryGroup_SelectedIndexChanged" AutoPostBack="True" ></asp:DropDownList>
    </div>
    <div id="listBox">
        <div class="Header">
            Secondary Group Membership:
        </div>
        <asp:ListBox ID="lbGroups" runat="server" Width="250px" Height="86px"></asp:ListBox>
    </div>
    <div id="GroupMemberButtons" style="float: right">
        <div id="addButton">
            <asp:DropDownList ID="ddlAllGroups" runat="server" Width="100px" Height="20px">
            </asp:DropDownList>
            <br />
            <asp:Button ID="btnAddGroup" runat="server" Text="Join Group" OnClick="btnAddGroup_Click"
                Height="30px" Width="100px" /></div>
        <div id="deleteButton">
            <asp:Button ID="btnDeleteGroup" runat="server" Text="Delete Group" OnClick="btnDeleteGroup_Click"
                Height="30px" Width="100px" /></div>
    </div>
</div>