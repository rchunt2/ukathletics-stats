﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class GroupMemberAdministration : System.Web.UI.UserControl
    {
        public delegate void GroupMembersChangedDelegate();

        public event GroupMembersChangedDelegate OnGroupMembersChanged;

        public void StoreInViewstate(string key, object value)
        {
            if (ViewState[key] != null) ViewState.Remove(key);
            ViewState.Add(key, value);
        }

        public T GetFromViewState<T>(string key)
        {
            if (ViewState[key] != null)
                return ((T)ViewState[key]);
            else
                return default(T);
        }

        public Guid? SingleResourceID
        {
            get
            {
                Guid? g = GetFromViewState<Guid?>("GroupPage_SingleResourceID");
                return g;
            }
            set { StoreInViewstate("GroupPage_SingleResourceID", value); }
        }

        private Guid _groupID;
        private Boolean _isVisible = false;

        public Guid GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        public Boolean IsVisible
        {
            get { return this._isVisible; }
            set { this._isVisible = value; }
        }

        /// <summary>
        /// This will enable/disable the add to group button and gridview
        /// </summary>
        public Boolean IsReadOnly
        {
            set
            {
                btnAddToGroup.Enabled = !value;
                gvGroupMembers.Enabled = !value;
            }
        }

        public void BindAndShow()
        {
            _isVisible = true;
            if (_isVisible)
            {
                if (_groupID != Guid.Empty)
                {
                    STATSModel.Group GroupObj = STATSModel.Group.Get(this._groupID);
                    STATSModel.Resource[] ResourceObjArray = GroupObj.GetMembers().OrderBy(resource => resource.ResourceNameDisplayLastFirst).ToArray();
                    gvGroupMembers.DataSource = ResourceObjArray;
                    gvGroupMembers.DataBind();
                    int NumResources = ResourceObjArray.Length;
                    lblNumGroupMembers.Text = NumResources.ToString() + " Members in Group";
                }
            }
        }

        protected void gvSearchResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "remove")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Label lblResourceID = (Label)gvGroupMembers.Rows[i].Cells[0].FindControl("lblResourceID");
                Guid ResourceID = Guid.Parse(lblResourceID.Text);
                STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceID);
                STATSModel.Group eGroup = STATSModel.Group.Get(this.GroupID);
                eGroup.RemoveMember(eResource);

                // If a group-scheduled session exists, remove those scheduled items from the user
                STATSModel.Schedule.DeleteFromGroup(eResource, this.GroupID);

                // If the resource is no longer part of a group, disable them
                if (eResource.Groups.Count() == 0)
                {
                    STATSModel.Resource.InactivateResource(eResource.ResourceID);
                }

                if (OnGroupMembersChanged != null)
                {
                    OnGroupMembersChanged();
                }

                this.BindAndShow();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResourceSelectionSingleControl.OnSelectionChanged += ResourceSelectionSingleControl_OnSelectionChanged;
        }

        private void ResourceSelectionSingleControl_OnSelectionChanged(ResourceSelectionSingle.ResourceSelectionUpdatedEventArgs e)
        {
            SingleResourceID = e.SelectedResourceID;
        }

        protected void btnAddToGroup_Click(object sender, EventArgs e)
        {
            STATSModel.Group eGroup = STATSModel.Group.Get(GroupID);

            if (SingleResourceID.HasValue && SingleResourceID.Value != Guid.Empty)
            {
                STATSModel.Resource eResource = STATSModel.Resource.Get(SingleResourceID.Value);
                eGroup.AddMember(eResource);

                //Add any Group Sessions to the individual's Schedule
                if(eGroup.Sessions.Count > 0)
                {
                    var context = new STATSEntities();
                    foreach (STATSModel.Session SessionObj in eGroup.Sessions)
                    {
                        var ScheduleObj = new STATSModel.Schedule();
                        ScheduleObj.ScheduleID = Guid.NewGuid();
                        ScheduleObj.ResourceID = eResource.ResourceID;
                        ScheduleObj.SessionID = SessionObj.SessionID;
                        context.Schedules.AddObject(ScheduleObj);
                    }
                    context.SaveChanges();
                }

                //if the resource was just added to a group, activate them in the system
                if (eResource.Groups.Count() == 0)
                {
                    STATSModel.Resource.ActivateResource(SingleResourceID.Value);
                }
            }

            

            if (OnGroupMembersChanged != null)
            {
                OnGroupMembersChanged();
            }

            this.BindAndShow();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}