﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Messaging.ascx.cs" Inherits="UK.STATS.Administration.UserControls.Messaging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel ID="panelInbox" runat="server">
    <asp:Label ID="lblMessageWarning" runat="server" Font-Bold="true" ForeColor="Red" Text="" Visible="false"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblNoMessages" runat="server" Visible="False" 
        Font-Size="X-Large"></asp:Label>
    <asp:GridView ID="gvInbox" runat="server" CellPadding="4" EnableTheming="True" ForeColor="#333333"
        HorizontalAlign="Left" Width="100%" AutoGenerateColumns="False" OnRowDataBound="gvInbox_RowDataBound"
        OnRowCommand="gvInbox_RowCommand">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="MessageInboxID_HIDDEN" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="MessageInboxID" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Read">
                <ItemTemplate>
                    <asp:Label ID="lblRead" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Sent">
                <ItemTemplate>
                    <asp:Label ID="lblSent" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sender">
                <ItemTemplate>
                    <asp:Label ID="lblSender" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recipient">
                <ItemTemplate>
                    <asp:Label ID="lblRecipient" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Subject">
                <ItemTemplate>
                    <asp:Label ID="lblSubject" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Expiration Date">
                <ItemTemplate>
                    <asp:Label ID="lblDateExpires" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField CommandName="ReadMessage" Text="Read Message" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <div style="clear: both;"></div>
</asp:Panel>
<div class="MessageDetailContainer" style="width: 100%">
    <asp:Panel ID="panelRead" runat="server" BackColor="#e8e8e8" BorderWidth="1px" Visible="false">
        <table width="100%">
            <tr style="background-color: #507CD1; font-weight: bold; color: White">
                <td>
                    <div id="MessageReadTitle">
                        <div class="Header">
                            Message Details
                        </div>
                    </div>
                </td>
                <td>
                    <div style="text-align: center">
                        <asp:LinkButton ID="lkbtBack" runat="server" OnClick="btnBack_Click" ForeColor="White">Back To Inbox</asp:LinkButton></div>
                </td>
                <td>
                    <div style="text-align: center">
                    <a href="#" style="color: White;" onclick="javascript: print(); return false;">Print Message</a>
                        </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp
                </td>
                <td>
                    &nbsp
                </td>
            </tr>
            <tr>
                <td>
                    <div class="messageTitles">
                        From:
                    </div>
                </td>
                <td>
                    <asp:Label ID="lblFrom" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="messageTitles">
                        To:
                    </div>
                </td>
                <td>
                    <asp:Label ID="lblTo" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="messageTitles">
                        Expiration Date:
                    </div>
                </td>
                <td>
                    <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="messageTitles">
                        Subject:
                    </div>
                </td>
                <td>
                    <asp:Label ID="lblSubject" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="messageTitles">
                        Message:
                    </div>
                </td>
                <td>
                    <asp:Label ID="lblBody" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
</div>
<asp:Panel ID="panelCompose" runat="server" Visible="false">

    <asp:UpdatePanel ID="upMessaging" runat="server">
        <ContentTemplate>
            <div class="MessageCompContainer">
                <table>
                    <tr>
                        <td>
                            <div id="MessageComposition">
                                <div class="Header">
                                    Message Composition
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;">
                                <asp:Label ID="lblRecipient" runat="server" Text="Recipient: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:TextBox ID="txtRecipient" runat="server" Width="264px" Enabled="false"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;" id="subject">
                                <asp:Label ID="lblSubjectCompose" runat="server" Text="Subject: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:TextBox ID="txtSubjectCompose" runat="server" Width="264px"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;" id="messageType">
                                <asp:Label ID="lblMessageType" runat="server" Text="Type: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:DropDownList ID="ddlMessageType" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="ddlMessageType_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;">
                                <asp:Label ID="lblContentCompose" runat="server" Text="Content: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:TextBox ID="txtContentCompose" runat="server" TextMode="MultiLine" Width="475px" Height="250px"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;">
                                <asp:Label ID="lblSendDate" runat="server" Text="Send Date: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:TextBox ID="txtSendDate" runat="server" ReadOnly="true"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Calendar ID="calDateSend" runat="server" OnSelectionChanged="calDateSend_SelectionChanged">
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: right;">
                                <asp:Label ID="lblDateExpireCompose" runat="server" Text="Expiration Date: "></asp:Label></div>
                        </td>
                        <td>
                            <div style="text-align: left;">
                                <asp:TextBox ID="txtDateExpire" runat="server" ReadOnly="true"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Calendar ID="calDateExpire" runat="server" OnSelectionChanged="calDateExpire_SelectionChanged">
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="confirmationBox" runat="server" visible="false">
                                <asp:Label text="Please confirm the list below before sending:" runat="server"></asp:Label><br />
                                <asp:ListBox Rows="10" Width="100%" ID="lbRecipientList" runat="server"></asp:ListBox>
                                <div style="align-content:center:"><asp:Button runat="server" ID="btnSend" Text="Confirm and Send" OnClick="btnSend_Click"  />
                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" /></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="sendMessage" runat="server" Text="Send Message" OnClick="sendMessage_Click" />
                            <asp:Button ID="cancelMessage" runat="server" Text="Cancel" OnClick="cancelMessage_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>    
</asp:Panel>
