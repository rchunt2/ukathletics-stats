﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourcePictureEdit : UK.STATS.Administration.Pages.UKBaseControl
    {
        private Guid _resourceID;

        public Guid ResourceID
        {
            get
            {
                if (_resourceID == Guid.Empty)
                {
                    _resourceID = GetFromViewState<Guid>("ResourcePictureEdit_ResourceID");
                }
                return _resourceID;
            }
            set
            {
                _resourceID = value;
                StoreInViewstate("ResourcePictureEdit_ResourceID", value);
            }
        }


        public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);



            //save the resized image as a jpeg with a quality of 90
            //    ImageUtilities.SaveJpeg(@"C:\myimage.jpeg", resized, 90);



            var newImage = new Bitmap(newWidth, newHeight);
            newImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (Graphics gr = Graphics.FromImage(newImage))
            {

                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight));


            }

            return newImage;


            // Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            //return newImage;
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }




        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack) //need to actually do this every postback because the image does not load if you change something on another tab and then switch to the picture tab
            //{
            lblMessage.Text = "Please note that the image will be scaled and may lose quality if its size exceeds 105x145.";
            bool hasPicture = ResourcePicture.HasPicture(ResourceID);
            if (hasPicture)
            {
                btnDelete.Visible = true;
                rbiResourcePicture.DataValue = ResourcePicture.GetPicture(ResourceID);
                rbiResourcePicture.Visible = true;
            }
            else
            {
                rbiResourcePicture.Visible = false;
            }
            //}
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (!ResourcePicture.IsValidPicture(this.fuPictureUpload.FileBytes))
            {
                lblError.Text = "File uploaded is not an acceptable Image file. Please try saving the image as a JPG and try again.";
                return;
            }

            Bitmap checkImg = new Bitmap(this.fuPictureUpload.PostedFile.InputStream, false);

            byte[] newImageArray = this.fuPictureUpload.FileBytes;
            if (checkImg.Height > 145 || checkImg.Width > 105)
            {
                // System.Drawing.Image img = CreateImgFromBytes(this.fuPictureUpload.FileBytes);
                System.Drawing.Image newimage = ScaleImage(checkImg, 105, 145);


                newImageArray = imageToByteArray(newimage);
            }

            ResourcePicture.SavePicture(ResourceID, this.fuPictureUpload.FileBytes);
            rbiResourcePicture.DataValue = this.fuPictureUpload.FileBytes;
            btnDelete.Visible = true;
            rbiResourcePicture.Visible = true;
            lblError.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            ResourcePicture.DeletePicture(ResourceID);
            btnDelete.Visible = false;
            rbiResourcePicture.Visible = false;
        }
    }
}