﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSearch.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourceSearch" %>

<asp:Panel ID="panelResourceSearch" runat="server" Visible="true" DefaultButton="btnSearch">
        <div id="ResourceSearchModule">
            <div class="Header">Search For a Resource</div>
            <asp:TextBox ID="txtSearchQuery" Width="100px" runat="server" placeholder="name"></asp:TextBox>
            <asp:TextBox ID="txtSearchEmail" Width="100px" runat="server" placeholder="email"></asp:TextBox>
            <asp:TextBox ID="txtSearchAD" Width="100px" runat="server" placeholder="ad login"></asp:TextBox>
            <asp:TextBox ID="txtSearchPhone" runat="server" placeholder="phone (no formatting)" ></asp:TextBox>
            <asp:DropDownList ID="ddlGroupFilter" runat="server" AutoPostBack="False">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlResourceTypeFilter" runat="server" 
                AutoPostBack="False">
            </asp:DropDownList>
            <asp:CheckBox ID="chkDisabledResources" Text="Disabled Resources Only" runat="server" />
            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" 
                Text="Search" />
            <asp:PlaceHolder ID="phResource" runat="server" />
        </div>
        <asp:Label ID="lblResultsCount" runat="server" Text="Label" Visible="false"></asp:Label><br />
        <asp:GridView ID="gvSearchResults" runat="server" CellPadding="2" AutoGenerateColumns="False" 
                EnableTheming="True" ForeColor="#333333" HorizontalAlign="Left" AllowPaging="True"
            Width="100%" 
            onpageindexchanging="gvSearchResults_PageIndexChanging" 
            onsorting="gvSearchResults_Sorting" 
            onrowdatabound="gvSearchResults_RowDataBound">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="ResourceID" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ResourceNameFirst" HeaderText="First Name" 
                        SortExpression="ResourceNameFirst" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ResourceNameLast" HeaderText="Last Name" 
                        SortExpression="ResourceNameLast" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ResourceEmail" HeaderText="Email Address" 
                        SortExpression="ResourceEmail" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ResourceADLogin" HeaderText="AD Login" 
                        SortExpression="ResourceADLogin" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                     <asp:BoundField DataField="ResourcePhoneNumberFormatted" HeaderText="Phone" 
                        SortExpression="ResourcePhoneNumber" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ResourceLogin" HeaderText="Username" 
                        SortExpression="ResourceLogin" >
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="ResourceID" Target="_blank" DataNavigateUrlFormatString="../PrintSchedule.aspx?resourceid={0}" Text="View Schedule" />
                    <%--<asp:HyperLinkField DataNavigateUrlFields="ResourceID" DataNavigateUrlFormatString="../Pages/Resource.aspx?resourceid={0}" Text="View Details" />--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="linkViewDetails" runat="server">View Details</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
    </asp:Panel>