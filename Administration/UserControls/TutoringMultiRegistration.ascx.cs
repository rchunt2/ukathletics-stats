﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class TutoringMultiRegistration : UK.STATS.Administration.Pages.UKBaseControl
    {
        public delegate void ButtonAddDelegate();

        public event ButtonAddDelegate ButtonAddEvent;

        public STATSModel.Resource ResourceObj
        {
            get { return GetFromViewState<STATSModel.Resource>("TutoringMultiRegistration_ResourceObject"); }
            set { StoreInViewstate("TutoringMultiRegistration_ResourceObject", value); }
        }

        //DM - 9/25/2012 - changed resource types to pull from Application variable to prevent lots of unnessary hits to the DB since they do not change.
        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtpkTutorMultiSession.SelectedDate = DateTime.Today.AddHours(8); // Today at 8AM.
                BindAndShow();
            }
        }

        public void BindAndShow()
        {
            if (this.ResourceObj != null)
            {
                if (this.ResourceObj.ResourceTypeID == ResourceTypeTutor.ResourceTypeID)
                {
                    this.Visible = true;
                    BindStudents();
                    BindTutorSkills();
                    BindExistingSessions();
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        //Bind Student Drop Down for Multi Student Tutor Registration
        private void BindStudents()
        {
            var eResources = STATSModel.Resource.Get(ResourceTypeStudent).ToArray();
            eResources = (from Resource obj in eResources orderby obj.ResourceNameLast ascending, obj.ResourceNameFirst ascending select obj).ToArray();
            ddlAllStudents.Items.Clear();
            foreach (var eResource in eResources)
            {
                var li = new ListItem(eResource.ResourceNameDisplayLastFirst, eResource.ResourceID.ToString());
                ddlAllStudents.Items.Add(li);
            }
        }

        //Bind Tutor Skills for Multi Student Tutor Registration
        private void BindTutorSkills()
        {
            var eResource = STATSModel.Resource.Get(ResourceObj.ResourceID, true);
            STATSModel.CourseTutor[] CourseTutorObjArray = eResource.CourseTutors.ToArray();
            CourseTutorObjArray = (from STATSModel.CourseTutor obj in CourseTutorObjArray orderby obj.Course.Subject.SubjectNameShort ascending select obj).ToArray();
            ddlTutorSkills.Items.Clear();
            foreach (STATSModel.CourseTutor CourseTutorObj in CourseTutorObjArray)
            {
                STATSModel.Course CourseObj = STATSModel.Course.Get(CourseTutorObj.CourseID);
                STATSModel.Subject SubjectObj = STATSModel.Subject.Get(CourseObj.SubjectID);
                String Name = SubjectObj.SubjectNameShort + CourseObj.CourseNumber + " - " + CourseObj.CourseName;
                String Value = CourseTutorObj.CourseID.ToString();
                var li = new ListItem(Name, Value);
                ddlTutorSkills.Items.Add(li);
            }
        }

        //Bind Existing Sessions for Multi Student Tutor Registration
        private void BindExistingSessions()
        {
            var eResource = STATSModel.Resource.Get(ResourceObj.ResourceID, true);
            ddlExistingSessions.Items.Clear();
            
            //Same code as ResourceRegistrationSummary
            var eSessionsTutoring = eResource.TutoringSessions;
            var DistinctSessions = new List<Session>();
            foreach (var eSession in eSessionsTutoring)
            {
                Boolean match = false;
                foreach (var distinctSession in DistinctSessions)
                {
                    // Note: The below if statement is done to make sure that group sessions are displayed correctly.  For tutors they want to see the sessions repeated with each student
                    // name in their grid.  Students however only need to the see the session once and with their tutor's name.  By checking for the resource ids with the tutors it ensures
                    // that the right amount of sessions will be displayed.
                    // If the resource is a tutor we need to pull up all of the group sessions
                    if (distinctSession.SessionDateTimeStart.TimeOfDay == eSession.SessionDateTimeStart.TimeOfDay 
                        && distinctSession.SessionDateTimeStart.DayOfWeek == eSession.SessionDateTimeStart.DayOfWeek 
                        && distinctSession.CourseSectionID == eSession.CourseSectionID
                        )
                    {
                        match = true;
                    }
                   
                }

                if (!match)
                {
                    DistinctSessions.Add(eSession);
                }
            }

            //Add an empty value at the top of the drop down
            ddlExistingSessions.Items.Add(new ListItem("--New Group Session--", ""));
            foreach (STATSModel.Session session in DistinctSessions)
            {
                STATSModel.Resource[] eResourcesTutoring = session.GetResourceInSession();
                //Remove the tutor from the count
                int NumberScheduled = eResourcesTutoring.Count() - 1;

                // Need to load course info, day of week, time, number of current students
                var li = new ListItem(session.SessionTutors.First().Course.CourseDisplayName + " " +
                                        session.SessionDateTimeStart.DayOfWeek + " " +
                                        session.SessionDateTimeStart.TimeOfDay + " " +
                                        "Current Students In Session: " + NumberScheduled.ToString(), session.SessionID.ToString());
                ddlExistingSessions.Items.Add(li);
            }
        }

        protected void btnRegisterMultiStudent_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ddlTutorSkills.SelectedValue))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Skill Selected", "<script language='javascript'> alert('There was no skill selected for the tutoring session.  Your session has not been scheduled.');</script>", false);
                return; // NO SKILL SELECTED!!!
            }

            var CourseID = Guid.Parse(ddlTutorSkills.SelectedValue);
            var eCourse = Course.Get(CourseID);
            var eResourceTutor = ResourceObj;
            var eResources = new List<Resource>();

            if (!dtpkTutorMultiSession.SelectedDate.HasValue)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Date Selected", "<script language='javascript'> alert('There was no date selected for the tutoring session.  Your session has not been scheduled.');</script>", false);
                return; // NO DATE SELECTED!!!
            }

            if (lbStudents.Items.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Student(s) Selected", "<script language='javascript'> alert('There were no students selected for the tutoring session.  Your session has not been scheduled.');</script>", false);
                return;
            }

            foreach (ListItem li in lbStudents.Items)
            {
                Guid ResourceID = Guid.Parse(li.Value);
                Resource eResource = Resource.Get(ResourceID);
                eResources.Add(eResource);
            }

            int delta = DayOfWeek.Monday - DateTime.Today.DayOfWeek;
            DateTime monday = DateTime.Today.AddDays(delta);
            DateTime sunday = monday.AddDays(6);
            var dtOccurs = dtpkTutorMultiSession.SelectedDate.Value;
            delta = dtOccurs.DayOfWeek - monday.DayOfWeek;
            DateTime selectedDate = monday.AddDays(delta);
            TimeSpan selectedTime = dtOccurs.TimeOfDay;
            selectedDate = selectedDate.Date + selectedTime;

            CourseSection fakeCourse = new CourseSection();
            fakeCourse.CourseSectionDateStart = selectedDate;
            Boolean tutorConflict = STATSModel.Session.DoesTimeConflict(eResourceTutor, fakeCourse);
            Boolean studentConflict = false;
            String studentWithConflict = "";

            // Check the selected students' schedules for conflicts
            foreach (Resource curStudent in eResources)
            {
                // If some other student already has a conflict then don't bother checking the rest
                if (studentConflict) break;
                studentConflict = STATSModel.Session.DoesTimeConflict(curStudent, fakeCourse);
                if (studentConflict)
                {
                    studentWithConflict = curStudent.ResourceNameFirst + " " + curStudent.ResourceNameLast;
                }
            }
            if (tutorConflict && chkAllowDouble.Checked)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Possible Double Booking", "<script language='javascript'> alert('There is already something scheduled at this time for the tutor.  If you would like to double book, please uncheck the \\'Conflict Check\\' option above the \\'Register\\' button.');</script>", false);
            }
            else
            {
                if (studentConflict && chkAllowDouble.Checked)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Possible Double Booking", "<script language='javascript'> alert('There is already something scheduled at this time for " + studentWithConflict + ".  If you would like to double book, please uncheck the \\'Conflict Check\\' option above the \\'Register\\' button.');</script>", false);
                }
            }

            if ((!studentConflict && !tutorConflict) || (studentConflict && !chkAllowDouble.Checked) || (tutorConflict && !chkAllowDouble.Checked))
            {
                Boolean success = false;
                //Creating a new Group session
                if (ddlExistingSessions.SelectedIndex == 0)
                {
                    Setting eSettingTutorSessionLength = Setting.GetByKey("TutorSessionLength");
                    int iTutorSessionLength = Int32.Parse(eSettingTutorSessionLength.SettingValue.ToString());

                    int iSessionTeamworksID = 0;

                    //HERE
                    /*if (eResourceTutor.TeamworksID.HasValue)
                    {
                        //Add the event to Teamworks
                        iSessionTeamworksID = Teamworks.TeamworksAPI.AddSession(eResourceTutor.TeamworksID.Value, "Tutoring - " + eCourse.CourseDisplayName, selectedDate, selectedDate.AddMinutes(iTutorSessionLength), true);
                    }
                    foreach (Resource student in eResources)
                    {
                        if (student.TeamworksID.HasValue)
                        {
                            //Add the event to Teamworks
                            iSessionTeamworksID = Teamworks.TeamworksAPI.AddSession(student.TeamworksID.Value, "Tutoring - " + eCourse.CourseDisplayName, selectedDate, selectedDate.AddMinutes(iTutorSessionLength), true);
                        }
                    }
                    */
                    success = STATSModel.Session.CreateTutoringSession(eCourse, eResourceTutor, eResources, dtOccurs, true, rdbLocationFootball.Checked, false);
                   
                }
                else
                {
                    //joining an existing group or single session (to make a group session)
                    Guid sessionID = new Guid(ddlExistingSessions.SelectedValue);

                    success = STATSModel.Session.JoinTutoringSession(sessionID, eResourceTutor, eResources);
                }

                if (success)
                {
                    lbStudents.Items.Clear();
                    BindAndShow();
                }

                if (ButtonAddEvent != null)
                {
                    ButtonAddEvent();
                }
                chkAllowDouble.Checked = true;
            }
        }

        protected void btnAddStudent_Click(object sender, EventArgs e)
        {
            lbStudents.Items.Add(new ListItem(ddlAllStudents.SelectedItem.Text, ddlAllStudents.SelectedValue));
            BindStudents();
        }

        protected void btnDeleteStudent_Click(object sender, EventArgs e)
        {
            if (lbStudents.SelectedIndex > -1)
            {
                ListItem li = lbStudents.SelectedItem;
                lbStudents.Items.Remove(li);
                BindAndShow();
            }
        }

        protected void ddlExistingSessions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlExistingSessions.SelectedIndex == 0)
            {
                ddlTutorSkills.Enabled = true;
                dtpkTutorMultiSession.Enabled = true;
                rdbLocationFootball.Enabled = true;
                rdbLocationMain.Enabled = true;
            }
            else
            {
                ddlTutorSkills.Enabled = false;
                dtpkTutorMultiSession.Enabled = false;
                rdbLocationFootball.Enabled = false;
                rdbLocationMain.Enabled = false;
            }
        }

        //protected void btnRefresh_Click(object sender, EventArgs e)
        //{
        //    BindAndShow();
        //}
    }
}