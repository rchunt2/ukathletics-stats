﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;

namespace UK.STATS.Administration.UserControls
{
    public partial class IsOpenParameters : System.Web.UI.UserControl
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                var context = new STATSModel.STATSEntities();
                var eOperationRanges = (from STATSModel.OperationRange obj in context.OperationRanges.Include("SessionType") select obj).ToArray();
                String SessionTypeName;

                //SessionTypeName = "Computer Lab";
                //tpCompStartSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                //tpCompStartSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());

                //tpCompEndSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                //tpCompEndSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());

                SessionTypeName = "Tutoring - CATS";
                tpTutorStartSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpTutorStartSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());

                tpTutorEndSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpTutorEndSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());

                SessionTypeName = "Tutoring - Football";
                tpFootballTutorStartSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballTutorStartSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());

                tpFootballTutorEndSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballTutorEndSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());

                SessionTypeName = "Study Hall - CATS";
                tpStudyStartSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpStudyStartSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());

                tpStudyEndSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpStudyEndSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());

                SessionTypeName = "Study Hall - Football";
                tpFootballStudyStartSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());
                tpFootballStudyStartSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeStart).FirstOrDefault());

                tpFootballStudyEndSunday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SUN" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndMonday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "MON" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndTuesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "TUE" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndWednesday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "WED" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndThursday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "THU" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndFriday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "FRI" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
                tpFootballStudyEndSaturday.SelectedDate = DateTime.Today.Add((from STATSModel.OperationRange obj in eOperationRanges where obj.OperationRangeDayOfWeek == "SAT" && obj.SessionType.SessionTypeName == SessionTypeName select obj.OperationRangeTimeEnd).FirstOrDefault());
            }

            
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var context = new STATSModel.STATSEntities();
            var eOperationRanges = (from STATSModel.OperationRange obj in context.OperationRanges.Include("SessionType") select obj).ToArray();

            foreach (var eOperationRange in eOperationRanges)
            {
                switch (eOperationRange.OperationRangeDayOfWeek)
                {
                    case "MON":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndMonday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "TUE":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndTuesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "WED":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndWednesday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "THU":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndThursday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "FRI":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndFriday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "SAT":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndSaturday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                    case "SUN":
                        switch (eOperationRange.SessionType.SessionTypeName)
                        {
                            case "Tutoring - CATS":
                                eOperationRange.OperationRangeTimeStart = tpTutorStartSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpTutorEndSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Tutoring - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballTutorStartSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballTutorEndSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - CATS":
                                eOperationRange.OperationRangeTimeStart = tpStudyStartSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpStudyEndSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            case "Study Hall - Football":
                                eOperationRange.OperationRangeTimeStart = tpFootballStudyStartSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                eOperationRange.OperationRangeTimeEnd = tpFootballStudyEndSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                                break;
                            //case "Computer Lab":
                            //    eOperationRange.OperationRangeTimeStart = tpCompStartSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    eOperationRange.OperationRangeTimeEnd = tpCompEndSunday.SelectedDate.GetValueOrDefault().TimeOfDay;
                            //    break;
                        }
                        break;
                }
                context.SaveChanges();
            }
        }
    }
}