﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourcePictureEdit.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourcePictureEdit" %>

<telerik:RadBinaryImage ID="rbiResourcePicture" runat="server" Height="145px" Width="105px" ResizeMode="Fit" />
<br />
<asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Green"></asp:Label>
<br />
Upload Image: <asp:FileUpload ID="fuPictureUpload" runat="server" /><br />
<asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" Visible="false" /><br />
<asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>