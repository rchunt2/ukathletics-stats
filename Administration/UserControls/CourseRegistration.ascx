﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CourseRegistration.ascx.cs" Inherits="UK.STATS.Administration.UserControls.CourseRegistration" %>

<asp:Panel ID="panelCourseRegistration" runat="server" Visible="false">
    <div id="RegisterCourseModule" style="padding: 2px;">
        <div id="top" style="height: 35px; margin-bottom: 5px; width: 494px;">
            <div class="Header" style="width: 280px; float: left; margin-top: 2px;">
                <asp:Label ID="lblTitle" runat="server" Text="Course Registration Module"></asp:Label>
            </div>
        </div>
        <div id="bottomDropDowns" style="width: 633px; height: 30px; clear: both;">
            <asp:DropDownList ID="ddlSubjects" runat="server" Height="20px" Width="120px"
                OnSelectedIndexChanged="ddlSubjects_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlCourses" runat="server" Height="20px" Width="120px"
                OnSelectedIndexChanged="ddlCourses_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlCourseSections" runat="server" Height="20px"
                Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddlCourseSections_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;
            <asp:Button ID="btnRegisterCourse" runat="server" Height="30px" Text="Register" OnClick="btnRegisterCourse_Click" Width="100px" />
            <asp:CheckBox ID="cbOverride" runat="server" Checked="True" Text="Conflict Check" />
        </div>
        <div>
            <asp:Label ID="lblMeetingTime" runat="server" Text=""></asp:Label>
        </div>
        <div style="font-style: italic; font-size: smaller">
            Note: If the Subject, Course, or Section you want is not listed, follow the instructions to add it 
            <asp:HyperLink ID="linkCreate" runat="server" NavigateUrl="~/Pages/Subject.aspx">here</asp:HyperLink>.
        </div>
    </div>
</asp:Panel>
