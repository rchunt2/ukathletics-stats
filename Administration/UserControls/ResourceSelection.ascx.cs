﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourceSelection : System.Web.UI.UserControl
    {
        public Guid? ResourceTypeIDFilter { get; set; }

        public class ResourceSelectionUpdatedEventArgs : EventArgs
        {
            public String XmlData { get; private set; }

            public int XmlDataLength { get; private set; }

            public ResourceSelectionUpdatedEventArgs(String xmlData)
            {
                XmlData = xmlData;
                XmlDataLength = xmlData.Length;
            }
        }

        public delegate void SelectionChanged(ResourceSelectionUpdatedEventArgs e);

        public event SelectionChanged OnSelectionChanged;

        public void StoreInViewstate(string key, object value)
        {
            if (ViewState[key] != null) ViewState.Remove(key);
            ViewState.Add(key, value);
        }

        public T GetFromViewState<T>(string key)
        {
            if (ViewState[key] != null)
                return ((T)ViewState[key]);
            else
                return default(T);
        }

        public List<KeyValuePair<string, Guid>> SelectedResources
        {
            get
            {
                List<KeyValuePair<string, Guid>> lstGuid = GetFromViewState<List<KeyValuePair<string, Guid>>>("Reporting_SelectedResources");
                if (lstGuid == null)
                {
                    SelectedResources = new List<KeyValuePair<string, Guid>>();
                    lstGuid = new List<KeyValuePair<string, Guid>>();
                }
                return lstGuid;
            }
            set
            {
                StoreInViewstate("Reporting_SelectedResources", value);
            }
        }

        public List<KeyValuePair<string, Guid>> SelectedGroups
        {
            get
            {
                List<KeyValuePair<string, Guid>> lstGuid = GetFromViewState<List<KeyValuePair<string, Guid>>>("Reporting_SelectedGroups");
                if (lstGuid == null)
                {
                    SelectedGroups = new List<KeyValuePair<string, Guid>>();
                    lstGuid = new List<KeyValuePair<string, Guid>>();
                }
                return lstGuid;
            }
            set { StoreInViewstate("Reporting_SelectedGroups", value); }
        }

        public String XmlData
        {
            get
            {
                var Result = new List<Guid>();
                var eGroups = new List<Group>();

                //Sort Groups by name
                SelectedGroups = SelectedGroups.OrderBy(g => g.Key).ToList();

                //Sort Resources by name
                SelectedResources = SelectedResources.OrderBy(r => r.Key).ToList();

                SelectedGroups.ForEach(g => eGroups.Add(Group.Get(g.Value)));
                foreach (Group eGroup in eGroups)
                {
                    Resource[] eResources = eGroup.GetMembers();
                    if (ResourceTypeIDFilter.HasValue)
                    {
                        eResources = (from Resource obj in eResources where obj.ResourceTypeID == ResourceTypeIDFilter.Value select obj).ToArray();
                    }

                    //Issue ID 228 - Sort group resources by firstname/lastname within each group
                    eResources = eResources.OrderBy(resource => resource.ResourceNameDisplayLastFirst).ToArray();

                    foreach (Resource eResource in eResources)
                    {
                        if (!Result.Contains(eResource.ResourceID))
                        {
                            Result.Add(eResource.ResourceID);
                        }
                    }
                }
                foreach (KeyValuePair<string, Guid> item in SelectedResources)
                {
                    //only add each value once to the Result list
                    if (!Result.Contains(item.Value))
                    {
                        Result.Add(item.Value);
                    }
                }
                String xmlString = Reports.ReportHelper.GetXMLFromResourceIDArray(Result.ToArray());
                return xmlString;
            }
        }

        public int XmlDataLength
        {
            get { return XmlData.Length; }
        }

        public static ListItem GetListItemFromResource(Resource eResource)
        {
            return new ListItem(eResource.ResourceNameDisplayLastFirst, eResource.ResourceID.ToString());
        }

        public static ListItem GetListItemFromGroup(Group eGroup)
        {
            return new ListItem(eGroup.GroupName, eGroup.GroupID.ToString());
        }

        protected List<KeyValuePair<string, Guid>> GetItems(ListBox lb)
        {
            List<KeyValuePair<string, Guid>> lstID = new List<KeyValuePair<string, Guid>>();
            int numItems = lb.Items.Count;
            for (int i = 0; i < numItems; i++)
            {
                String value = lb.Items[i].Value;
                Guid parsedID = Guid.Parse(value);
                lstID.Add(new KeyValuePair<string, Guid>(lb.Items[i].Text, parsedID));
            }

            return lstID;
        }

        protected List<KeyValuePair<string, Guid>> GetSelection(ListBox lb)
        {
            List<KeyValuePair<string, Guid>> selectedItems = new List<KeyValuePair<string, Guid>>();
            int[] selectedIndices = lb.GetSelectedIndices();
            foreach (int selectedIndex in selectedIndices)
            {
                String value = lb.Items[selectedIndex].Value;
                Guid parsedID = Guid.Parse(value);
                selectedItems.Add(new KeyValuePair<string, Guid>(lb.Items[selectedIndex].Text, parsedID));
            }
            return selectedItems;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindResources();
                BindGroups();
            }
        }

        private List<Guid> GetValuesFromListOfKeyValues(List<KeyValuePair<string, Guid>> listOfKeyValuePairs)
        {
            List<Guid> guids = new List<Guid>(listOfKeyValuePairs.Count);
            listOfKeyValuePairs.ForEach(g => guids.Add(g.Value));
            return guids;
        }

        protected void BindGroups()
        {
            lbGroupAvailable.Items.Clear();
            lbGroupSelected.Items.Clear();

            Group[] eGroups = Group.Get();
            List<Guid> groupGuids = GetValuesFromListOfKeyValues(SelectedGroups);
            List<Group> eGroupsAvailable = (from Group obj in eGroups
                                            where !groupGuids.Contains(obj.GroupID)
                                            select obj).OrderBy(obj => obj.GroupName).ToList();
            eGroupsAvailable.ForEach(g => lbGroupAvailable.Items.Add(GetListItemFromGroup(g)));
            SelectedGroups.ForEach(g => lbGroupSelected.Items.Add(new ListItem(g.Key, g.Value.ToString())));
        }

        protected void BindResources()
        {
            lbResourceAvailable.Items.Clear();
            lbResourceSelected.Items.Clear();

            Resource[] eResources = Resource.Get();
            if (ResourceTypeIDFilter.HasValue)
            {
                eResources = (from Resource obj in eResources where obj.ResourceTypeID == ResourceTypeIDFilter.Value select obj).ToArray();
            }
            List<Guid> selecedResourceGuids = GetValuesFromListOfKeyValues(SelectedResources);

            List<Resource> eResourcesAvailable = (from Resource obj in eResources
                                                  where !selecedResourceGuids.Contains(obj.ResourceID)
                                                  select obj).OrderBy(obj => obj.ResourceNameDisplayLastFirst).ToList();
            eResourcesAvailable.ForEach(r => lbResourceAvailable.Items.Add(GetListItemFromResource(r)));
            SelectedResources.ForEach(r => lbResourceSelected.Items.Add(new ListItem(r.Key, r.Value.ToString())));
        }

        protected void btnGroupAdd_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> selection = GetSelection(lbGroupAvailable);
            SelectedGroups.AddRange(selection);
            BindGroups();
            FireSelectionChangedEvent();
        }

        protected void btnGroupRemove_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> selection = GetSelection(lbGroupSelected);
            selection.ForEach(g => SelectedGroups.Remove(g));
            BindGroups();
            FireSelectionChangedEvent();
        }

        protected void btnResourceAdd_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> selection = GetSelection(lbResourceAvailable);
            SelectedResources.AddRange(selection);
            BindResources();
            FireSelectionChangedEvent();
        }

        protected void btnResourceRemove_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> selection = GetSelection(lbResourceSelected);
            selection.ForEach(g => SelectedResources.Remove(g));
            BindResources();
            FireSelectionChangedEvent();
        }

        protected void FireSelectionChangedEvent()
        {
            // Don't ever fire events unless you're absolutely certain something is bound to them.
            if (OnSelectionChanged != null)
            {
                OnSelectionChanged(new ResourceSelectionUpdatedEventArgs(XmlData));
            }
        }

        protected void btnGroupAddAll_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> AvailableGroupIDs = GetItems(lbGroupAvailable);
            SelectedGroups.AddRange(AvailableGroupIDs);
            BindGroups();
            BindResources();
            FireSelectionChangedEvent();
        }

        protected void btnGroupRemoveAll_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> SelectedGroupIDs = GetItems(lbGroupSelected);
            foreach (KeyValuePair<string, Guid> selectedGroup in SelectedGroupIDs)
            {
                SelectedGroups.Remove(selectedGroup);
            }
            BindGroups();
            BindResources();
            FireSelectionChangedEvent();
        }

        protected void btnResourceAddAll_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> AvailableResourceIDs = GetItems(lbResourceAvailable);
            SelectedResources.AddRange(AvailableResourceIDs);
            BindGroups();
            BindResources();
            FireSelectionChangedEvent();
        }

        protected void btnResourceRemoveAll_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Guid>> SelectedResourceIDs = GetItems(lbResourceSelected);
            foreach (KeyValuePair<string, Guid> selectedResourceID in SelectedResourceIDs)
            {
                SelectedResources.Remove(selectedResourceID);
            }
            BindGroups();
            BindResources();
            FireSelectionChangedEvent();
        }
    }
}