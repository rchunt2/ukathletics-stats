﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMenu.ascx.cs" Inherits="UK.STATS.Administration.UserControls.NavigationMenu" %>

<div id="navigation">
    <asp:TreeView ID="tvNavigation" DataSourceID="SiteMapDataSource1" runat="server" ViewStateMode="Enabled" NodeIndent="0" ShowExpandCollapse="False" ontreenodedatabound="tvNavigation_TreeNodeDataBound"></asp:TreeView>
    <br />
    <br />
    <asp:TreeView ID="TreeView1" DataSourceID="SiteMapDataSource2" runat="server" ViewStateMode="Enabled" NodeIndent="5" ShowExpandCollapse="False"></asp:TreeView>
    
    <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" SiteMapProvider="NavigationSiteMap" />
    <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" SiteMapProvider="ReportsSiteMap" />
</div>