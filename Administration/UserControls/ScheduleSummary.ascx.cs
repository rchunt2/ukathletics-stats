﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ScheduleSummary : System.Web.UI.UserControl
    {
        private Guid _groupid;

        public Guid GroupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DateTime dt = DateTime.Today.AddDays(7).AddHours(8); // One week from today, at 8AM.
                dtpkTime.SelectedDate = dt;
                dtpkTime.FocusedDate = dt;
            }
        }

        protected void btnRunReport_Click(object sender, EventArgs e)
        {
            string url = Request.Url.GetLeftPart(UriPartial.Authority) + this.ResolveUrl("~/ScheduleSummary.aspx?GroupID=" + GroupID.ToString() + "&Date=" + dtpkTime.SelectedDate.Value.Ticks.ToString());
            Response.Redirect(url);
        }
    }
}