﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class ResourceGroupMembership : UK.STATS.Administration.Pages.UKBaseControl
    {
        public delegate void GroupsChangedDelegate();
        public event GroupsChangedDelegate OnGroupsChanged;
        public event GroupsChangedDelegate OnActiveChanged;

        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }


        public STATSModel.Resource ResourceObj
        {
            get { return GetFromViewState<STATSModel.Resource>("TutoringMultiRegistration_ResourceObject"); }
            set { StoreInViewstate("TutoringMultiRegistration_ResourceObject", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //BindAndShow();
        }

        public void BindAndShow()
        {
            if (this.ResourceObj != null)
            {
                BindGroupMembershipModule();
            }
        }

        private void BindGroupMembershipModule()
        {
            // Populate the List Box.
            STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceObj.ResourceID, true);
            STATSModel.Group[] eGroupsResource = eResource.Groups;

            lbGroups.Items.Clear();
            ddlPrimaryGroup.Items.Clear();

            foreach (var eGroup in eGroupsResource)
            {
                var li = new ListItem(eGroup.GroupName, eGroup.GroupID.ToString());

                if (!eGroup.PrimaryGroup)
                {
                    lbGroups.Items.Add(li);
                    //ddlPrimaryGroup.Items.Add(li);
                }
                //else
                //{
                //    lbGroups.Items.Add(li);
                //}
            }


            // If the list box contains an item, remove it from the DropDownList
            ddlAllGroups.Items.Clear();
            STATSModel.Group[] eGroups = STATSModel.Group.Get();

            STATSModel.Group[] eGroupsNotMember = (from STATSModel.Group obj in eGroups where (!(from STATSModel.Group objMember in eGroupsResource select objMember.GroupID).Contains(obj.GroupID)) orderby obj.GroupName ascending select obj).ToArray();

            foreach (var eGroup in eGroupsNotMember)
            {
                var li = new ListItem(eGroup.GroupName, eGroup.GroupID.ToString());
                if (!lbGroups.Items.Contains(li) && !eGroup.PrimaryGroup)
                {
                    ddlAllGroups.Items.Add(li);
                }
            }
            foreach(var eGroup in eGroups)
            {
                var li = new ListItem(eGroup.GroupName, eGroup.GroupID.ToString());

                if (eGroup.PrimaryGroup)
                {
                    ddlPrimaryGroup.Items.Add(li);
                }
            }

            foreach (var eGroup in eGroupsResource)
            {
                if (eGroup.PrimaryGroup)
                {
                    ddlPrimaryGroup.SelectedValue = eGroup.GroupID.ToString();
                }
            }


        }

        protected void btnAddGroup_Click(object sender, EventArgs e)
        {
            if (ddlAllGroups.SelectedItem != null)
            {
                STATSModel.Group GroupObj = STATSModel.Group.Get(Guid.Parse(ddlAllGroups.SelectedValue));
                GroupObj.AddMember(ResourceObj);

                //Add any Group Sessions to the individual's Schedule
                if (GroupObj.Sessions.Count > 0)
                {
                    var context = new STATSEntities();
                    foreach (STATSModel.Session SessionObj in GroupObj.Sessions)
                    {
                        var ScheduleObj = new STATSModel.Schedule();
                        ScheduleObj.ScheduleID = Guid.NewGuid();
                        ScheduleObj.ResourceID = ResourceObj.ResourceID;
                        ScheduleObj.SessionID = SessionObj.SessionID;
                        context.Schedules.AddObject(ScheduleObj);
                    }
                    context.SaveChanges();
                }

                //if the resource was just added to a group, activate them in the system
                //if (ResourceObj.Groups.Count() == 0)
                //{
                //    STATSModel.Resource.ActivateResource(ResourceObj.ResourceID);
                //}

                RunWhenGroupsChange();
                BindGroupMembershipModule();

                //if the resource was just added to a group, activate them in the system
                //if (ResourceObj.Groups.Count() == 0)
                //{
                //    RunWhenActiveChanges();
                //}
            }
        }

        protected void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (lbGroups.Text != "" && lbGroups.Items.Count > 0)
            {
                Guid GroupID = Guid.Parse(lbGroups.SelectedItem.Value);
                STATSModel.Group eGroup = STATSModel.Group.Get(GroupID);
                STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceObj.ResourceID);
                eGroup.RemoveMember(eResource);

                // If a group-scheduled session exists, remove those scheduled items from the user
                STATSModel.Schedule.DeleteFromGroup(eResource, eGroup.GroupID);

                //if (eResource.Groups.Count() == 0)
                //{
                //    //Inactivate user
                //    STATSModel.Resource.InactivateResource(eResource.ResourceID);
                //}

                RunWhenGroupsChange();
                BindGroupMembershipModule();

                //if (eResource.Groups.Count() == 0)
                //{
                //    RunWhenActiveChanges();
                //}
            }
        }

        protected void RunWhenGroupsChange()
        {
            //ResourceScheduleControl.RunScheduler(false);
            if (OnGroupsChanged != null)
            {
                OnGroupsChanged();
            }
        }

        protected void RunWhenActiveChanges()
        {
            //ResourceScheduleControl.RunScheduler(false);
            if (OnActiveChanged != null)
            {
                OnActiveChanged();
            }
        }

        protected void ddlPrimaryGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            STATSModel.Resource eResource = STATSModel.Resource.Get(ResourceObj.ResourceID);
            ResourceObj = STATSModel.Resource.Get(eResource.ResourceID, true);

            // Now, we'll need to conditionally bind certain modules.
            // I assume that it would be wise to also hide the tabs that are not relevant to certain ResourceTypes.
            ResourceType eResourceType = ResourceObj.ResourceType;

            Boolean isTutor = (eResourceType.ResourceTypeID == ResourceTypeTutor.ResourceTypeID);
            Boolean isStudent = (eResourceType.ResourceTypeID == ResourceTypeStudent.ResourceTypeID);

            if (isTutor || isStudent)
            {
                foreach(var resGroups in ResourceObj.Groups)
                {
                    if(resGroups.PrimaryGroup)
                    {
                        resGroups.RemoveMember(ResourceObj);
                    }
                }
                STATSModel.Group GroupObj = STATSModel.Group.Get(Guid.Parse(ddlPrimaryGroup.SelectedValue));
                GroupObj.AddMember(ResourceObj);
            }

            //ResourceScheduleControl.RunScheduler(false);
            if (OnGroupsChanged != null)
            {
                OnGroupsChanged();
            }
        }
    }
}