﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSelectionSingle.ascx.cs" Inherits="UK.STATS.Administration.UserControls.ResourceSelectionSingle" %>

<asp:DropDownList ID="ddlResource" runat="server" Width="350px" 
    AutoPostBack="True" 
    onselectedindexchanged="ddlResource_SelectedIndexChanged" 
    AppendDataBoundItems="True">
</asp:DropDownList> 
<asp:Button ID="btnConfirmSelection" runat="server" Text="Confirm Selection" 
    onclick="btnConfirmSelection_Click" Visible="False" />
<br />
<%--Selected Value: <asp:Label ID="lblSelected" runat="server" Text="<N/A>"></asp:Label>--%>