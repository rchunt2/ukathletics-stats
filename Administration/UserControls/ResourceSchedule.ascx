﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceSchedule.ascx.cs"
    Inherits="UK.STATS.Administration.UserControls.ResourceSchedule" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
</style>

<asp:Panel ID="panelScreen" runat="server">
    <div style="float: left;">
        <asp:Label ID="lblSource" runat="server" Text="Resource Name"></asp:Label>
    </div>

    <asp:Panel ID="pnlPrintLink" runat="server">
        <div class="noPrint" style="float: right;">
            <a href="#" onclick="javascript: print(); return false;">Print</a>
        </div>
    </asp:Panel>
    <div style="clear: both;"></div>
</asp:Panel>
<asp:UpdatePanel ID="upScheduler" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div id="ResourceScheduler">
            <telerik:RadScheduler
                ID="ResourceRadScheduler"
                runat="server"
                Height="100%"
                DataKeyField="ID"
                DataSubjectField="Subject"
                DataEndField="End"
                DataStartField="Start"
                DayEndTime="23:00:00"
                EnableDatePicker="True"
                EnableDescriptionField="True"
                OverflowBehavior="Expand"
                SelectedView="WeekView"
                ShowViewTabs="False"
                FirstDayOfWeek="Monday"
                LastDayOfWeek="Sunday"
                ShowAllDayRow="False"
                Width="100%"
                WorkDayEndTime="23:59:00"
                WorkDayStartTime="00:00:00"
                OnAppointmentCreated="ResourceRadScheduler_AppointmentCreated1"
                OnAppointmentInsert="ResourceRadScheduler_AppointmentInsert"
                AllowDelete="False"
                OnAppointmentUpdate="ResourceRadScheduler_AppointmentUpdate"
                ShowFooter="False"
                OnNavigationComplete="ResourceRadScheduler_NavigationComplete"
                AppointmentStyleMode="Default" AdvancedForm-Enabled="True"
                OnAppointmentDelete="ResourceRadScheduler_AppointmentDelete"
                ShowHeader="False" ShowNavigationPane="False"
                ShowResourceHeaders="False" RowHeight="30px">
                <WeekView WorkDayEndTime="23:59:00" ColumnHeaderDateFormat="ddd" />
            </telerik:RadScheduler>
            <%-- skin="Windows7" --%>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>