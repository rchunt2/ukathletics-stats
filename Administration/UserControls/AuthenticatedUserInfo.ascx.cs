﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace UK.STATS.Administration.UserControls
{
    public partial class AuthenticatedUserInfo : System.Web.UI.UserControl
    {
        private Guid _authenticationID;

        public Guid AuthenticationID
        {
            get { return _authenticationID; }
            set { _authenticationID = value; }
        }

        public Boolean isVisible
        {
            get { return panelAuthenticatedUserInfo.Visible; }
            set { panelAuthenticatedUserInfo.Visible = value; }
        }

        public void ShowAndBind()
        {
            this.Visible = true;
            panelAuthenticatedUserInfo.Visible = true;
            this.Bind();
        }

        private void Bind()
        {
            if (this.Visible)
            {
                if (AuthenticationID != Guid.Empty)
                {
                    STATSModel.Authentication AuthenticationObj = STATSModel.Authentication.Get(AuthenticationID);
                    STATSModel.Resource ResourceObj = STATSModel.Resource.Get(AuthenticationObj.ResourceID);
                    lblName.Text = ResourceObj.ResourceNameFirst + " ";
                    lblName.ToolTip = ResourceObj.ResourceID.ToString();
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Bind();
        }
    }
}