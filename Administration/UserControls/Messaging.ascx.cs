﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class Messaging : UK.STATS.Administration.Pages.UKBaseControl
    {
        public enum MessagingMode { Inbox, Read, Compose}
        public enum RecipientType { Resource, Group, ResourceType, Professor}

        public Boolean viewAll = false;

        public MessagingMode Mode
        {
            get 
            {
                var md = GetFromViewState<MessagingMode>("Messaging_Mode");
                return md;
            }
            set 
            {
                StoreInViewstate("Messaging_Mode", value);
                switch (value)
                {
                    case MessagingMode.Inbox:
                        panelRead.Visible = false;
                        panelInbox.Visible = true;
                        panelCompose.Visible = false;
                        BindAndShow();
                        break;
                    case MessagingMode.Read:
                        panelRead.Visible = true;
                        panelInbox.Visible = false;
                        panelCompose.Visible = false;
                        break;
                    case MessagingMode.Compose:
                        panelRead.Visible = false;
                        panelInbox.Visible = false;
                        panelCompose.Visible = true;
                        break;
                    default:
                        panelRead.Visible = false;
                        panelInbox.Visible = false;
                        panelCompose.Visible = false;
                        break;
                }
            }
        }

        public Guid RecipientID
        {
            get { return GetFromViewState<Guid>("Messaging_RecipientID"); }
            set { StoreInViewstate("Messaging_RecipientID", value); }
        }

        public RecipientType TypeOfRecipient
        {
            get { return GetFromViewState<RecipientType>("Messaging_TypeOfRecipient"); }
            set { StoreInViewstate("Messaging_TypeOfRecipient", value); }
        }

        public Guid FromResourceID
        {
            get { return GetFromViewState<Guid>("Messaging_FromResourceID"); }
            set { StoreInViewstate("Messaging_FromResourceID", value); }
        }

        public Boolean IsVisible
        {
            get { return GetFromViewState<Boolean>("Messaging_isVisible"); }
            set { StoreInViewstate("Messaging_isVisible", value); }
        }

        public void BindAndShow()
        {
            this.IsVisible = true;
            if (this.IsVisible)
            {
                switch (Mode)
                {
                    case MessagingMode.Inbox:

                        STATSModel.MessageInbox[] MessageInboxObjArray;
                        var MessageInboxObjList = new List<STATSModel.MessageInbox>();
                        if (viewAll)
                        {
                            //MessageInboxObjArray = STATSModel.MessageInbox.Get();
                            MessageInboxObjArray = STATSModel.MessageInbox.GetAllNewNotExpired(STATSModel.Resource.Get(RecipientID));
                        }
                        else
                        {
                            MessageInboxObjArray = STATSModel.MessageInbox.GetAllNewNotExpired(STATSModel.Resource.Get(RecipientID));
                        }
                        if (MessageInboxObjArray.Length == 0)
                        {
                            lblMessageWarning.Visible = false;
                            lblNoMessages.Visible = true;
                            lblNoMessages.Text = "There are no messages to be displayed.";
                        }
                        gvInbox.DataSource = MessageInboxObjArray;
                        gvInbox.DataBind();
                        break;
                    case MessagingMode.Read:
                        break;
                    case MessagingMode.Compose:
                        // We need to bind the types of Messages to the control.
                        var curItem = ddlMessageType.SelectedItem;
                        ddlMessageType.Items.Clear(); // We'll need to clear any existing items before adding more.
                        STATSModel.MessageType[] MessageTypeObjArray = STATSModel.MessageType.Get();
                        foreach (STATSModel.MessageType MessageTypeObj in MessageTypeObjArray)
                        {
                            String liTxt = MessageTypeObj.MessageTypeName;
                            String liVal = MessageTypeObj.MessageTypeID.ToString();
                            ListItem li = new ListItem(liTxt, liVal);
                            ddlMessageType.Items.Add(li);
                        }
                        if(curItem != null)
                            ddlMessageType.Items.FindByValue(curItem.Value).Selected = true;

                        switch (this.TypeOfRecipient)
                        {
                            case RecipientType.Resource:
                                STATSModel.Resource ResourceObj = STATSModel.Resource.Get(this.RecipientID, true);
                                txtRecipient.Text = ResourceObj.ResourceNameFirst + " " + ResourceObj.ResourceNameLast;
                                break;
                            case RecipientType.Group:
                                STATSModel.Group GroupObj = STATSModel.Group.Get(this.RecipientID);
                                txtRecipient.Text = GroupObj.GroupName;
                                break;
                            case RecipientType.ResourceType:
                                STATSModel.ResourceType ResourceTypeObj = STATSModel.ResourceType.Get(this.RecipientID);
                                txtRecipient.Text = ResourceTypeObj.ResourceTypeName;
                                break;
                            case RecipientType.Professor:
                                MessageType eMessageType = STATSModel.MessageType.Get("Email");
                                ddlMessageType.SelectedValue = eMessageType.MessageTypeID.ToString();
                                ddlMessageType.Enabled = false;           
                                STATSModel.CourseSection CourseSectionObj = STATSModel.CourseSection.Get(this.RecipientID);
                                String Email = CourseSectionObj.Instructor.InstructorEmail;
                                if (!String.IsNullOrEmpty(Email)) { txtRecipient.Text = Email; }
                                else { txtRecipient.Text = "No Email address on record"; }

                                if (!Page.IsPostBack || string.IsNullOrEmpty(txtContentCompose.Text)) //prevent overwriting the message on post back on page load
                                {
                                    txtContentCompose.Text = "Course: " + CourseSectionObj.CourseDisplayName + " Section " + CourseSectionObj.CourseSectionNumber + Environment.NewLine + Environment.NewLine;
                                    STATSModel.Resource[] resources = ResourceRegistration.GetCourseRoster(CourseSectionObj);
                                    if (resources.Length > 0)
                                    {
                                        txtContentCompose.Text += "This Email is in regards to the following students: ";
                                        foreach (STATSModel.Resource resourceNames in resources)
                                        {
                                            txtContentCompose.Text += resourceNames.ResourceNameLast + ", " + resourceNames.ResourceNameFirst + "; ";
                                        }
                                    }
                                    else
                                    {
                                        txtContentCompose.Text += "No students registered for this class";
                                    }
                                }
                                break;
                            default:
                                txtRecipient.Text = "Unknown Recipient!";
                                break;
                        }
                        break;
                    default:
                        // Somehow, the mode didn't get set, or it was set to an improper value.
                        // I've never seen this happen, but I suppose it's better to be safe than sorry.
                        break;
                }
                
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MessageType mtApplication = STATSModel.MessageType.Get("Application");
            MessageType mtEmail = STATSModel.MessageType.Get("Email");
            MessageType mtSMS = STATSModel.MessageType.Get("SMS (Text Message)");

            String strmtID = ddlMessageType.SelectedValue;
            if (String.IsNullOrWhiteSpace(strmtID))
            {
                strmtID = Guid.Empty.ToString();
            }
            Guid mtID = Guid.Parse(strmtID);

            if (mtID == mtApplication.MessageTypeID)
            {
                txtSendDate.Enabled = true;
                txtDateExpire.Enabled = true;
                calDateSend.Enabled = true;
                calDateExpire.Enabled = true;
            }
            else if (mtID == mtEmail.MessageTypeID)
            {
                txtSendDate.Enabled = false;
                txtDateExpire.Enabled = false;
                calDateSend.Enabled = false;
                calDateExpire.Enabled = false;
            }
            else if (mtID == mtSMS.MessageTypeID)
            {
                txtSendDate.Enabled = false;
                txtDateExpire.Enabled = false;
                calDateSend.Enabled = false;
                calDateExpire.Enabled = false;
            }
        }

        protected void gvInbox_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ReadMessage")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Label lblMessageInboxID = (Label)gvInbox.Rows[i].Cells[0].FindControl("MessageInboxID");
                Guid MessageInboxID = Guid.Parse(lblMessageInboxID.Text);
                STATSModel.MessageInbox MessageInboxObj = STATSModel.MessageInbox.Get(MessageInboxID);
                STATSModel.Message MessageObj = MessageInboxObj.Message;
                STATSModel.Resource From = STATSModel.Resource.Get(MessageObj.MessageFromResourceID);
                STATSModel.Resource To = STATSModel.Resource.Get(MessageObj.MessageToResourceID.Value);
                
                if (!MessageInboxObj.MessageInboxDateRead.HasValue)
                {
                    // If the Resource currently reading the message IS the intended Recipient, MARK READ.
                    
                    // Note: 
                    // FromResourceID is always set to the person viewing messages.
                    // This is in case they ever wanted to REPLY to messages.
                    // Not that it's an option as of now or anything.
                    if (FromResourceID == MessageObj.MessageToResourceID)
                    {
                        MessageInboxObj.MarkRead();
                    }
                }
                
                String DateExpires;
                if (MessageInboxObj.MessageInboxDateExpires.HasValue)
                {
                    DateExpires = MessageInboxObj.MessageInboxDateExpires.Value.ToLongDateString();
                }
                else
                {
                    DateExpires = "Never";
                }
                lblFrom.Text = From.ResourceNameFirst + " " + From.ResourceNameLast;
                lblTo.Text = To.ResourceNameFirst + " " + To.ResourceNameLast;
                lblDate.Text = DateExpires;
                lblSubject.Text = MessageObj.GetSubject();
                lblBody.Text = MessageObj.GetBody();
                this.Mode = MessagingMode.Read;
            }
        }

        protected void gvInbox_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                STATSModel.MessageInbox MessageInboxObj = (STATSModel.MessageInbox)e.Row.DataItem;
                STATSModel.Message MessageObj = MessageInboxObj.Message;
                STATSModel.Resource From = STATSModel.Resource.Get(MessageObj.MessageFromResourceID);
                STATSModel.Resource To = STATSModel.Resource.Get(MessageObj.MessageToResourceID.GetValueOrDefault());

                String DateExpires;

                if (MessageInboxObj.MessageInboxDateExpires.HasValue)
                {
                    DateExpires = MessageInboxObj.MessageInboxDateExpires.Value.ToLongDateString();
                }
                else
                {
                    DateExpires = "Never";
                }
                
                var numUnreadMessages = STATSModel.MessageInbox.NumberMessagesUnread(From);

                String Status = "New";
                if (MessageInboxObj.MessageInboxDateRead.HasValue)
                {
                    Status = MessageInboxObj.MessageInboxDateRead.Value.ToShortDateString();
                }
                else
                {
                    lblMessageWarning.Visible = true;
                    lblMessageWarning.Text = "You must read all unread messages before moving forward.";

                    var inboxID = ((Label)e.Row.FindControl("MessageInboxID"));
                    inboxID.Text = MessageInboxObj.MessageInboxID.ToString();
                    inboxID.Font.Bold = true;
                    inboxID.ForeColor = System.Drawing.Color.Blue;
                    var dateRead = ((Label)e.Row.FindControl("lblRead"));
                    dateRead.Text = Status;
                    dateRead.Font.Bold = true;
                    dateRead.ForeColor = System.Drawing.Color.Blue;
                    var dateSent = ((Label)e.Row.FindControl("lblSent"));
                    dateSent.Text = MessageInboxObj.MessageInboxDateSent.ToShortDateString();
                    dateSent.Font.Bold = true;
                    dateSent.ForeColor = System.Drawing.Color.Blue;
                    var senderName = ((Label)e.Row.FindControl("lblSender"));
                    senderName.Text = From.ResourceNameFirst + " " + From.ResourceNameLast;
                    senderName.Font.Bold = true;
                    senderName.ForeColor = System.Drawing.Color.Blue;
                    var recipientName = ((Label)e.Row.FindControl("lblRecipient"));
                    recipientName.Text = To.ResourceNameFirst + " " + To.ResourceNameLast;
                    recipientName.Font.Bold = true;
                    recipientName.ForeColor = System.Drawing.Color.Blue;
                    var subject = ((Label)e.Row.FindControl("lblSubject"));
                    subject.Text = MessageObj.GetSubject();
                    subject.Font.Bold = true;
                    subject.ForeColor = System.Drawing.Color.Blue;
                    var dateExpires = ((Label)e.Row.FindControl("lblDateExpires"));
                    dateExpires.Text = DateExpires;
                    dateExpires.Font.Bold = true;
                    dateExpires.ForeColor = System.Drawing.Color.Blue;
                }

                if (numUnreadMessages == 0)
                {
                    lblMessageWarning.Visible = false;
                }

                ((Label)e.Row.FindControl("MessageInboxID")).Text = MessageInboxObj.MessageInboxID.ToString();
                ((Label)e.Row.FindControl("lblRead")).Text = Status;
                ((Label)e.Row.FindControl("lblSent")).Text = MessageInboxObj.MessageInboxDateSent.ToShortDateString();
                ((Label)e.Row.FindControl("lblSender")).Text = From.ResourceNameFirst + " " + From.ResourceNameLast;
                ((Label)e.Row.FindControl("lblRecipient")).Text = To.ResourceNameFirst + " " + To.ResourceNameLast;
                ((Label)e.Row.FindControl("lblSubject")).Text = MessageObj.GetSubject();
                ((Label)e.Row.FindControl("lblDateExpires")).Text = DateExpires;
            }

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            this.Mode = MessagingMode.Inbox;
        }

        protected void cancelMessage_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        protected void sendMessage_Click(object sender, EventArgs e)
        {
            //Validate the form first
            if (txtDateExpire.Text == String.Empty)
            {
                calDateExpire.SelectedDate = DateTime.Today;
                calDateExpire_SelectionChanged(this, e);
            }
            if (txtSendDate.Text == String.Empty)
            {
                calDateSend.SelectedDate = DateTime.Today;
                calDateSend_SelectionChanged(this, e);
            }

            Guid MessageTypeID = Guid.Parse(ddlMessageType.SelectedValue);
            MessageType eMessageType = MessageType.Get(MessageTypeID);

            if (String.IsNullOrWhiteSpace(txtSubjectCompose.Text))
            {
                Setting eSettingDefaultSubject = Setting.GetByKey("MessageSubjectDefault");
                String strDefaultSubject = eSettingDefaultSubject.SettingValue;
                txtSubjectCompose.Text = strDefaultSubject;
            }

            if ((txtSubjectCompose.Text == "") || (txtContentCompose.Text == "") || MessageTypeID == Guid.Empty)
            {
                if (txtSubjectCompose.Text == "")
                    lblSubjectCompose.ForeColor = System.Drawing.Color.Red;
                else
                    lblSubjectCompose.ForeColor = System.Drawing.Color.Black;

                if (txtContentCompose.Text == "")
                    lblContentCompose.ForeColor = System.Drawing.Color.Red;
                else
                    lblContentCompose.ForeColor = System.Drawing.Color.Black;

                return;
            }
            
            //Now check to see if it's a group message or just to one individual
            if (this.TypeOfRecipient == RecipientType.Group)
            {
                STATSModel.Group GroupObj = STATSModel.Group.Get(this.RecipientID);
                sendMessage.Enabled = false;
                cancelMessage.Enabled = false;
                STATSModel.Resource[] eResources = GroupObj.GetMembers();
                IEnumerable<STATSModel.Resource> resourceList = eResources.OrderBy(x => x.ResourceNameLast);
                foreach (var eResourceTo in resourceList)
                {
                    lbRecipientList.Items.Add(new ListItem(eResourceTo.ResourceNameDisplayLastFirst));
                }
                confirmationBox.Visible = true;
                
            }
            else if (this.TypeOfRecipient == RecipientType.ResourceType) 
            {
                sendMessage.Enabled = false;
                cancelMessage.Enabled = false;
                STATSModel.ResourceType ResourceTypeObj = STATSModel.ResourceType.Get(this.RecipientID);
                STATSModel.Resource[] ToArray = STATSModel.Resource.Get(ResourceTypeObj);
                IEnumerable<STATSModel.Resource> resourceList = ToArray.OrderBy(x => x.ResourceNameLast);
                foreach (var eResourceTo in resourceList)
                {
                    lbRecipientList.Items.Add(new ListItem(eResourceTo.ResourceNameDisplayLastFirst));
                }
                confirmationBox.Visible = true;
            }
            else
            {
                SendMessage();
            }

        }

        private void SendMessage()
        {
            Guid MessageTypeID = Guid.Parse(ddlMessageType.SelectedValue);
            MessageType eMessageType = MessageType.Get(MessageTypeID);

            STATSModel.Resource ResourceFrom = STATSModel.Resource.Get(this.FromResourceID);
            switch (this.TypeOfRecipient)
            {
                case RecipientType.Resource:
                    STATSModel.Resource ResourceObj = STATSModel.Resource.Get(this.RecipientID);
                    STATSModel.Message.SendIndividualMessage(eMessageType, ResourceFrom, ResourceObj, txtSubjectCompose.Text, txtContentCompose.Text, calDateExpire.SelectedDate, calDateSend.SelectedDate);
                    break;
                case RecipientType.Group:
                    STATSModel.Group GroupObj = STATSModel.Group.Get(this.RecipientID);
                    STATSModel.Message.SendGroupMessage(eMessageType, ResourceFrom, GroupObj, txtSubjectCompose.Text, txtContentCompose.Text, calDateExpire.SelectedDate, calDateSend.SelectedDate);
                    break;
                case RecipientType.ResourceType:
                    STATSModel.ResourceType ResourceTypeObj = STATSModel.ResourceType.Get(this.RecipientID);
                    STATSModel.Resource[] ToArray = STATSModel.Resource.Get(ResourceTypeObj);
                    foreach (STATSModel.Resource ResourceTo in ToArray)
                    {
                        STATSModel.Message.SendIndividualMessage(eMessageType, ResourceFrom, ResourceTo, txtSubjectCompose.Text, txtContentCompose.Text, calDateExpire.SelectedDate, calDateSend.SelectedDate);
                    }
                    break;
                case RecipientType.Professor:
                    STATSModel.CourseSection CourseSectionObj = STATSModel.CourseSection.Get(this.RecipientID);
                    STATSModel.Message.SendIndividualMessage(eMessageType, ResourceFrom, CourseSectionObj, txtSubjectCompose.Text, txtContentCompose.Text, calDateExpire.SelectedDate);
                    break;
                default:
                    break;
            }

            String strMessage = "Your message has been sent.";
            ScriptManager.RegisterClientScriptBlock(ddlMessageType, ddlMessageType.GetType(), "MessageSentNotification", "<script language='javascript'>alert('" + strMessage + "');</script>", false);
            ResetForm();

            lblSubjectCompose.ForeColor = System.Drawing.Color.Black;
            lblContentCompose.ForeColor = System.Drawing.Color.Black;
            lblDateExpireCompose.ForeColor = System.Drawing.Color.Black;
        }

        private void ResetForm()
        {
            sendMessage.Enabled = true;
            cancelMessage.Enabled = true;
            this.txtContentCompose.Text = String.Empty;
            this.txtDateExpire.Text = String.Empty;
            this.txtSubjectCompose.Text = String.Empty;
        }

        protected void calDateExpire_SelectionChanged(object sender, EventArgs e)
        {
            txtDateExpire.Text = calDateExpire.SelectedDate.Date.ToShortDateString();
        }

        protected void calDateSend_SelectionChanged(object sender, EventArgs e)
        {
            txtSendDate.Text = calDateSend.SelectedDate.Date.ToShortDateString();
        }

        protected void ddlMessageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid eMessageTypeID = Guid.Parse(ddlMessageType.SelectedValue);
            MessageType eMessageType = MessageType.Get(eMessageTypeID);
            STATSModel.Resource eResource = STATSModel.Resource.Get(this.RecipientID);
            STATSModel.Resource ResourceFrom = STATSModel.Resource.Get(this.FromResourceID);

            switch(eMessageType.MessageTypeName)
            {
                case "Application":
                    break;
                case "SMS (Text Message)":
                    if (this.TypeOfRecipient != RecipientType.Group)
                    {
                        Boolean canText = (eResource.ResourceTextingEnabled);
                        Boolean hasCell = (eResource.CarrierID != Guid.Empty);

                        if (!canText || !hasCell)
                        {
                            ScriptManager.RegisterClientScriptBlock(ddlMessageType, ddlMessageType.GetType(), "MessagingNoticeSMS", "<script language='javascript'>alert('Text messages can not be sent to this Resource.  Please ensure that a proper Cellular Provider has been selected, and that texting is enabled for this Resource.');</script>", false);
                            ddlMessageType.SelectedIndex = 0;
                        }
                    }
                    break;
                case "Email":
                    if (this.TypeOfRecipient != RecipientType.Group)
                    {
                        Boolean IsMissingEmailTo = String.IsNullOrWhiteSpace(eResource.ResourceEmail);
                        Boolean IsMissingEmailFrom = String.IsNullOrWhiteSpace(ResourceFrom.ResourceEmail);
                        if (IsMissingEmailTo || IsMissingEmailFrom)
                        {
                            ScriptManager.RegisterClientScriptBlock(ddlMessageType, ddlMessageType.GetType(), "MessagingNoticeEmail", "<script language='javascript'>alert('Email messages can not be sent to this Resource.  Please ensure that a proper Email Address has been provided for both the currently logged in Resource, and the intended recipient Resource.');</script>", false);
                            ddlMessageType.SelectedIndex = 0;
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            lbRecipientList.Items.Clear();
            sendMessage.Enabled = true;
            cancelMessage.Enabled = true;
            confirmationBox.Visible = false;
            SendMessage();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lbRecipientList.Items.Clear();
            sendMessage.Enabled = true;
            cancelMessage.Enabled = true;
            confirmationBox.Visible = false;
        }
    }
}