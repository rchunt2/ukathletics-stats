﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TutoringMultiRegistration.ascx.cs" Inherits="UK.STATS.Administration.UserControls.TutoringMultiRegistration" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div id="TutorMultipleModule">
    <div class="Header">
        Register Multiple Students for Tutoring:
    </div>
    <br />
    <div id="RegisterInfo">
        <br />
        <asp:DropDownList ID="ddlExistingSessions" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExistingSessions_SelectedIndexChanged"></asp:DropDownList>
        <br />
        <asp:DropDownList ID="ddlTutorSkills" runat="server">
        </asp:DropDownList>
        <%--<asp:Button ID="btnRefresh" runat="server" onclick="btnRefresh_Click" Text="Refresh" />--%>
        <telerik:RadDateTimePicker ID="dtpkTutorMultiSession" runat="server" 
            style="margin-bottom: 0px" Culture="en-US" EnableTyping="False">
            <timeview cellspacing="-1" endtime="23:00:00" interval="00:30:00" starttime="08:00:00"></timeview>
            <timepopupbutton hoverimageurl="" imageurl="" />
            <calendar usecolumnheadersasselectors="False" userowheadersasselectors="False" 
                viewselectortext="x" CellDayFormat="ddd" DayCellToolTipFormat="ddd" 
                EnableMonthYearFastNavigation="False" EnableNavigation="False" 
                ShowColumnHeaders="False" ShowDayCellToolTips="False" ShowRowHeaders="False" 
                SingleViewRows="1" TitleFormat="'Day Picker'"></calendar>
            <dateinput dateformat="M/d/yyyy" displaydateformat="dddd hh:mm tt" 
                ReadOnly="True"></dateinput>
            <datepopupbutton hoverimageurl="" imageurl="" />
        </telerik:RadDateTimePicker>
        <br />
        <asp:RadioButton ID="rdbLocationMain" Text="CATS-Main" runat="server" Checked="true" GroupName="Location" />
        <asp:RadioButton ID="rdbLocationFootball" Text="Football" runat="server" Checked="false" GroupName="Location" />
    </div>
    <br />
    <div id="listboxStudents">
        <asp:ListBox ID="lbStudents" runat="server" Width="250px" Height="86px" AutoPostBack="True"
            ViewStateMode="Enabled"></asp:ListBox>
        <div id="StudentRegisterButtons" style="float: right">
            <div id="addButtonStudent">
                <asp:DropDownList ID="ddlAllStudents" runat="server" Width="100px" Height="20px" AutoPostBack="True">
                </asp:DropDownList>
                <br />
                <asp:Button ID="btnAddStudent" runat="server" Text="Add" Height="30px" Width="100px"
                    OnClick="btnAddStudent_Click" /></div>
            <div id="deleteButtonStudent">
                <asp:Button ID="btnDeleteStudent" runat="server" Text="Remove" Height="30px" Width="100px"
                    OnClick="btnDeleteStudent_Click" /></div>
        </div>
    </div>
    <br />
    <div id="RegisterButton" style="float: left;">
        <asp:CheckBox ID="chkAllowDouble" runat="server" Text="Conflict Check" 
                Enabled="True" Checked="true" />
        <br />
        <asp:Button ID="btnRegisterMultiStudent" runat="server" Text="Register" OnClick="btnRegisterMultiStudent_Click" />
    </div>
</div>