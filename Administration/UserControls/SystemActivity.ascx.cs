﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;
using Resource = UK.STATS.STATSModel.Resource;
using ResourceType = UK.STATS.STATSModel.ResourceType;

namespace UK.STATS.Administration.UserControls
{
    public partial class SystemActivity : System.Web.UI.UserControl
    {
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            switch(rtsActivity.SelectedTab.Text)
            {
                case "Administration":
                    PopulateAdministration();
                    break;
                case "Study Hall":
                    PopulateStudyHall();
                    break;
                case "Tutoring":
                    PopulateTutoring();
                    break;
                case "Computer Lab":
                    PopulateComputerLab();
                    break;
                default:
                    // No match found.  Oh well.
                    break;
            }
        }

        public void PopulateAdministration()
        {
            Authentication[] eAuthentications = Authentication.Get();
            var AdminGridItems = new List<AdministrationGridItem>();
            foreach (var eAuthentication in eAuthentications)
            {
                AdminGridItems.Add(new AdministrationGridItem(eAuthentication));
            }
            rgAdmin.DataSource = AdminGridItems;
            rgAdmin.DataBind();
        }

        public void PopulateStudyHall()
        {
            var context = new STATSEntities();
            SessionType eSessionTypeStudyHall = SessionType.GetByName("Study Hall - CATS");
            SessionType eSessionTypeStudyHallFootball = SessionType.GetByName("Study Hall - Football");
            var eAttendances = (from Attendance obj in context.Attendances where (obj.Schedule.Session.SessionTypeID == eSessionTypeStudyHall.SessionTypeID || obj.Schedule.Session.SessionTypeID == eSessionTypeStudyHallFootball.SessionTypeID) && obj.AttendanceTimeOut == null select obj).ToArray();
            var StudyHallGridItems = new List<StudyHallGridItem>();
            foreach (var eAttendance in eAttendances)
            {
                StudyHallGridItems.Add(new StudyHallGridItem(eAttendance));
            }
            rgStudyHall.DataSource = StudyHallGridItems;
            rgStudyHall.DataBind();
        }

        public void PopulateTutoring()
        {
            rpbTutoring.Items.Clear();
            var context = new STATSEntities();
            SessionType eSessionTypeCATS = SessionType.GetByName("Tutoring - CATS");
            SessionType eSessionTypeFootball = SessionType.GetByName("Tutoring - Football");
            List<Guid> SessionsWithoutTutors = new List<Guid>();
            var SessionIDs = (from Attendance obj in context.Attendances.Include("Schedule.Session") where (obj.Schedule.Session.SessionTypeID == eSessionTypeCATS.SessionTypeID || obj.Schedule.Session.SessionTypeID == eSessionTypeFootball.SessionTypeID) && obj.AttendanceTimeOut == null select obj.Schedule.SessionID).Distinct().ToArray();
            foreach (var sessionID in SessionIDs)
            {
                Session eSession = STATSModel.Session.Get(sessionID);
                Resource eResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                if (eResourceTutor.ResourceID == Guid.Empty)
                {
                    SessionsWithoutTutors.Add(sessionID);
                }
                else
                {
                    RadPanelItem rpi = new RadPanelItem(eResourceTutor.ResourceNameDisplayLastFirst);
                    rpbTutoring.Items.Add(rpi);
                    var rgTutoring = (RadGrid)rpi.FindControl("rgTutoring");
                    var TutoringGridItems = new List<TutoringGridItem>();
                    Resource[] eResources = eSession.GetResourceInSession();
                    foreach (var eResource in eResources)
                    {
                        ResourceType eResourceType = eResource.ResourceType;
                        if (eResource.IsPresent(eSession))
                        {
                            var eAttendance = (from Attendance obj in context.Attendances.Include("Schedule") where obj.AttendanceTimeOut == null && obj.Schedule.ResourceID == eResource.ResourceID select obj).Single();
                            TutoringGridItems.Add(new TutoringGridItem(eAttendance));
                        }
                    }
                    rgTutoring.DataSource = TutoringGridItems;
                    rgTutoring.DataBind();
                }
            }

            RadPanelItem rpiWaiting = new RadPanelItem("Waiting");
            rpbTutoring.Items.Add(rpiWaiting);
            RadGrid rgTutoringWaiting = (RadGrid)rpiWaiting.FindControl("rgTutoring");

            if (SessionsWithoutTutors.Count > 0)
            {
                var TutoringGridItems = new List<TutoringGridItem>();
                foreach (var sessionID in SessionsWithoutTutors)
                {
                    Session eSession = STATSModel.Session.Get(sessionID);
                    Resource[] eResources = eSession.GetResourceInSession();
                    
                    foreach (var eResource in eResources)
                    {
                        var eAttendance = (from Attendance obj in context.Attendances.Include("Schedule") where obj.AttendanceTimeOut == null && obj.Schedule.ResourceID == eResource.ResourceID select obj).Single();
                        TutoringGridItems.Add(new TutoringGridItem(eAttendance));
                    }
                }

                rgTutoringWaiting.DataSource = TutoringGridItems;
                rgTutoringWaiting.DataBind();
            }
        }

        public void PopulateComputerLab()
        {
            var context = new STATSEntities();
            SessionType eSessionType = SessionType.GetByName("Computer Lab");
            var eAttendances = (from Attendance obj in context.Attendances where obj.Schedule.Session.SessionTypeID == eSessionType.SessionTypeID && obj.AttendanceTimeOut == null select obj).ToArray();
            var ComputerLabGridItems = new List<ComputerLabGridItem>();
            foreach (var eAttendance in eAttendances)
            {
                ComputerLabGridItems.Add(new ComputerLabGridItem(eAttendance));
            }
            rgComputerLab.DataSource = ComputerLabGridItems;
            rgComputerLab.DataBind();
        }

        private class AdministrationGridItem
        {
            public String Name { get; set; }
            public String Type { get; set; }
            public DateTime LoginExpireTime { get; set; }

            public AdministrationGridItem(Authentication eAuthentication)
            {
                Resource eResource = eAuthentication.Resource;
                ResourceType eResourceType = eResource.ResourceType;
                Name = eResource.ResourceNameDisplayLastFirst;
                Type = eResourceType.ResourceTypeName;
                LoginExpireTime = eAuthentication.AuthenticationExpire;
            }
        }

        private class StudyHallGridItem
        {
            public String Name { get; set; }
            public DateTime LoginTime { get; set; }
            public String Duration { get; set; }

            public StudyHallGridItem(Attendance eAttendance)
            {
                Schedule eSchedule = eAttendance.Schedule;
                Resource eResource = eSchedule.Resource;
                Name = eResource.ResourceNameDisplayLastFirst;
                LoginTime = eAttendance.AttendanceTimeIn;
                Duration = Math.Ceiling((DateTime.Now - LoginTime).TotalMinutes).ToString() + " minutes";
            }
        }

        private class TutoringGridItem
        {
            public String Name { get; set; }
            public String Type { get; set; }
            public DateTime LoginTime { get; set; }
            public String Course { get; set; }
            public String Room { get; set; }
            public String Duration { get; set; }
            

            public TutoringGridItem(Attendance eAttendance)
            {
                Schedule eSchedule = eAttendance.Schedule;
                Resource eResource = eSchedule.Resource;
                ResourceType eResourceType = eResource.ResourceType;
                Session eSession = eSchedule.Session;
                SessionTutor[] eSessionTutors = eSession.SessionTutors.ToArray();
                Name = eResource.ResourceNameDisplayLastFirst;
                Type = eResourceType.ResourceTypeName;
                LoginTime = eAttendance.AttendanceTimeIn;
                Duration = Math.Ceiling((DateTime.Now - LoginTime).TotalMinutes).ToString() + " minutes";
                if (eSessionTutors.Length > 0)
                {
                    SessionTutor eSessionTutor = eSessionTutors.First();
                    Course eCourse = eSessionTutor.Course;
                    Room eRoom = eSessionTutor.Room;
                    Course = (eCourse == null) ? "N/A" : eCourse.CourseDisplayName;
                    Room = (eRoom == null) ? "N/A" : eRoom.RoomName;
                }
            }
        }

        private class ComputerLabGridItem
        {
            public String Name { get; set; }
            public DateTime LoginTime { get; set; }
            public String ComputerName { get; set; }
            public String Duration { get; set; }

            public ComputerLabGridItem(Attendance eAttendance)
            {
                Schedule eSchedule = eAttendance.Schedule;
                Resource eResource = eSchedule.Resource;
                Name = eResource.ResourceNameDisplayLastFirst;
                LoginTime = eAttendance.AttendanceTimeIn;
                ComputerName = eAttendance.AttendanceMachineName;
                Duration = Math.Ceiling((DateTime.Now - LoginTime).TotalMinutes).ToString() + " minutes";
            }
        }
    }
}