﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimeReview.ascx.cs"
    Inherits="UK.STATS.Administration.UserControls.TimeReview" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="panelReview" runat="server" Visible="False">
    <div style="clear: both;">
        <asp:Label ID="lblResourceName" runat="server" Text="lblResourceName" Font-Size="Larger"
            Visible="False"></asp:Label>
        <asp:UpdatePanel ID="upFreeTime" runat="server">
            <ContentTemplate>
                <asp:Panel ID="panelFreeTime" runat="server" Visible="false">
                    <div id="FreeTimeModule">
                        <div class="Header" style="float: left; height: 20px; margin-top: 5px; margin-right: 2px;">
                            Free Time Entry:</div>
                        <br />
                        <br />
                        <div id="timeEntry">
                            <div id="sessionTime" style="margin-top: 5px; margin-left: 0px; float: left; width: 600px;">
                                <asp:DropDownList ID="ddlSessionType" runat="server" Height="20px" Width="145px">
                                </asp:DropDownList>
                                <telerik:RadDatePicker ID="datepkFreeTime" runat="server">
                                </telerik:RadDatePicker>
                                <asp:TextBox ID="txtFreeTime" runat="server"></asp:TextBox>
                                <asp:Label ID="lblTimeInfo" runat="server" Text="(Minutes)"></asp:Label>
                            </div>
                            <div id="addFreeTime" style="float: right; width: 100px; margin-right: 6px;">
                                <asp:Button ID="btnAddFreeTime" Height="30px" Width="100px" runat="server" Text="Add"
                                    OnClick="btnAddFreeTime_Click" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div style="clear: both;">
                    <asp:GridView ID="gvTotalFreeTime" runat="server" CellPadding="4" EnableTheming="True"
                        ForeColor="#333333" HorizontalAlign="Left" Width="100%" AutoGenerateColumns="False"
                        OnRowCommand="gvTotalFreeTime_RowCommand" OnRowDataBound="gvTotalFreeTime_RowDataBound">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="FreeTimeID_HIDDEN" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblFreeTimeID" runat="server" Text="lblFreeTimeID"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="SessionTypeName" HeaderText="Session Type" />
                            <asp:BoundField HeaderText="Date Applied" DataField="FreeTimeApplyDate" />
                            <asp:BoundField HeaderText="Added Time" DataField="FreeTimeAmount" />
                            <asp:ButtonField Text="Delete Time" CommandName="RemoveTime" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear: both;">
            <asp:GridView ID="gvReview" runat="server" CellPadding="4" EnableTheming="True" ForeColor="#333333"
                HorizontalAlign="Left" Width="100%" AutoGenerateColumns="False" OnRowDataBound="gvReview_RowDataBound"
                OnRowCommand="gvReview_RowCommand" DataKeyNames="AttendanceID" 
                OnRowUpdated="gvReview_RowUpdated">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Session Type" SortExpression="Schedule.Session.SessionType.SessionTypeName">
                        <ItemTemplate>
                            <asp:Label ID="lblSessionType" runat="server" Text='<%# Bind("Schedule.Session.SessionType.SessionTypeName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AttendanceID_HIDDEN" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="AttendanceID" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AttendanceTimeIn" HeaderText="In" SortExpression="AttendanceTimeIn" ReadOnly="True" />
                    <asp:BoundField DataField="AttendanceTimeOut" HeaderText="Out" SortExpression="AttendanceTimeOut" ReadOnly="True" />
                    <asp:BoundField DataField="AttendanceMachineName" HeaderText="Machine Name" SortExpression="AttendanceMachineName" ReadOnly="True" />
                    <asp:TemplateField HeaderText="Actual Time (Minutes)">
                        <ItemTemplate>
                            <asp:Label ID="lblTimeActual" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time Counted (Minutes)" SortExpression="AttendanceTimeOverride">
                        <ItemTemplate>
                            <asp:Label ID="lblTimeCounted" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTimeCounted" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time Override">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnOverride" runat="server" CommandName="Override" Text="Override" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lbtnSave" runat="server" CommandName="Save" Text="Save" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
    </div>
</asp:Panel>
