﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class GroupSearch : System.Web.UI.UserControl
    {
        private String _queryString = "";
        private String _detailPage = "";

        public delegate void ButtonAddDelegate();
        public event ButtonAddDelegate ButtonAddEvent;

        protected override void CreateChildControls()
        {   
            base.CreateChildControls();
            Button btn = new Button();
            btn.ID = "btnCreate";
            btn.Text = "Create Group";
            btn.Click += new EventHandler(btnCreate_Click);
            phGroup.Controls.Add(btn);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (txtSearchQuery.Text == String.Empty)
            {
                RunSearch(txtSearchQuery.Text);
            }
        }

        public String QueryString
        {
            get { return _queryString; }
            set { _queryString = value; }
        }

        public void RunSearch(String Query)
        {
            this.QueryString = Query;
            this.RunSearch();
        }

        public String DetailsPage
        {
            get { return _detailPage; }
            set { _detailPage = value; }
        }

        public Boolean IsVisible
        {
            get { return panelGroupSearch.Visible; }
            set { panelGroupSearch.Visible = value; }
        }

        /// <summary>
        /// This will enable/disable the Create button
        /// </summary>
        public Boolean IsReadOnly
        {
            set
            {
                if (phGroup.FindControl("btnCreate") != null)
                {
                    ((Button)phGroup.FindControl("btnCreate")).Enabled = !value;
                }
            }
        }

        public void RunSearch()
        {
            STATSModel.Group[] eGroups = STATSModel.Group.Search(this.QueryString);
            lblResultsCount.Visible = true;
            lblResultsCount.Text = eGroups.Length + " results for '" + this.QueryString + "'";
            gvSearchResults.DataSource = eGroups;
            gvSearchResults.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            RunSearch(txtSearchQuery.Text);
        }

        protected void gvSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSearchResults.PageIndex = e.NewPageIndex;
            RunSearch(txtSearchQuery.Text);
            gvSearchResults.DataBind();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (ButtonAddEvent != null)
                ButtonAddEvent();
        }

    }
}