﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.UserControls
{
    public partial class SystemLoggedIn : System.Web.UI.UserControl
    {
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            RadToolTipManager.TargetControls.Clear();
            STATSModel.Attendance[] eAttendances = STATSModel.Attendance.GetActive();

            lblLoggedInCount.Text = eAttendances.Length + " User(s) Currently Logged In";

            //gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.SessionID);
            gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.GetType()).ThenBy(obj => obj.Schedule.SessionID).ThenBy(obj => obj.Schedule.Resource.ResourceNameLast);
            gvSystemLoggedIn.DataBind();
        }

        protected void gvSystemLoggedIn_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                STATSModel.Attendance eAttendance = (STATSModel.Attendance)e.Row.DataItem;
                STATSModel.Schedule eSchedule = eAttendance.Schedule;
                STATSModel.Resource eResource = eSchedule.Resource;
                STATSModel.ResourceType eResourceType = eResource.ResourceType;
                STATSModel.Session eSession = eSchedule.Session;
                STATSModel.SessionType eSessionType = eSession.SessionType;
                STATSModel.SessionType eSessionTypeTutoringCATS = STATSModel.SessionType.GetByName("Tutoring - CATS");
                STATSModel.SessionType eSessionTypeTutoringFootball = STATSModel.SessionType.GetByName("Tutoring - Football");
                STATSModel.Resource[] eResourcesInSession = eSession.GetResourceInSession();

                Boolean MultipleResourceInSession = (eResourcesInSession.Length > 1);
                Boolean MultipleResourcePresentForSession = (eSession.GetPresentResources().Length > 1);
                int StudentsPresent = eSession.GetPresentStudents().Length;
                Boolean IsLegitimateTutoringSession = (MultipleResourceInSession && eSession.SessionIsScheduled && MultipleResourcePresentForSession); // Has to have 2 people, AND be scheduled.  Else, it could be a temp session, or an unscheduled one.
                String Status = "Active";

                if (eSessionType.SessionTypeID == eSessionTypeTutoringCATS.SessionTypeID || eSessionType.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID) //This is a tutoring session.
                {
                    STATSModel.Resource eResourceTutor = null;
                    STATSModel.Resource eResourceStudent = null;
                    Boolean isTutorPresent = false; //Default these to false, once we find them we'll set to true
                    Boolean isStudentPresent = false;

                    if (eResource.ResourceTypeID == ResourceTypeStudent.ResourceTypeID) //Student
                    {
                        eResourceStudent = eResource; //We already have the student resource we are checking, just use it
                        isStudentPresent = true; //We know there is an attendance record, thats where this resource came from (the current attendance record we are examining!)
                    }
                    else
                    {
                        //Get the student(s) from the database since we are checking the tutor
                        STATSModel.Resource[] studentsInSession = eSession.GetStudents();
                        foreach (STATSModel.Resource student in studentsInSession) //switched to loop through each student since a group session could have more than 1
                        {
                            eResourceStudent = student;

                            //only check if student is present if a student is assigned to the session
                            //if no student is assigned to the session, no student is present, so no need to check.
                            if (eResourceStudent != null && eResourceStudent.ResourceID != Guid.Empty)
                            {
                                isStudentPresent = eResourceStudent.IsPresent(eSession);
                            }

                            //we can stop checking if the students are present as soon as we find at least 1 student present
                            if (isStudentPresent)
                            {
                                break;
                            }
                        }
                    }

                    if (eResource.ResourceTypeID == ResourceTypeTutor.ResourceTypeID) //Tutor
                    {
                        eResourceTutor = eResource; //We already have the tutor resource, just use it
                        isTutorPresent = true; //We know there is an attendance record, thats where this resource came from (the current attendance record we are examining!)
                    }
                    else
                    {
                        //Get the tutor from the database since we are checking the student
                        eResourceTutor = eSession.GetTutor(ResourceTypeTutor);

                        //Only check if tutor is present if a tutor is assigned
                        if (eResourceTutor != null && eResourceTutor.ResourceID != Guid.Empty)
                        {
                            isTutorPresent = eResourceTutor.IsPresent(eSession);
                        }
                    }

                    if (eSession.SessionIsScheduled) //Scheduled Tutoring Session
                    {
                        if (isTutorPresent && isStudentPresent)
                        {
                            Status = "Active - Scheduled";
                        }
                        else
                        {
                            Status = "Waiting - Scheduled";
                        }
                    }
                    else
                    {
                        // Unscheduled Tutoring Session - Note: This also includes temporary sessions.

                        if (MultipleResourceInSession)
                        { // Multiple resources in a session requires that one of them is a Tutor.
                            if (isTutorPresent) // check whether the aforementioned tutor is present or not.
                            {
                                // Unscheduled session, yet a Tutor is present.  It's an active session.
                                Status = "Active - Unscheduled";
                            }
                            else
                            {
                                // No tutor is present.  One was supposedly here though...
                                // Note: This could happen if an unscheduled session was fulfilled, and then the tutor somehow left without killing the session.
                                // In all reality, this should rarely, if ever, happen.
                                Status = "Missing Tutor! - Unscheduled";
                            }
                        }
                        else
                        { // Just a single Resource, with nobody else scheduled for the unscheduled session.
                            Status = "Waiting - Unscheduled";
                        }
                    }
                }

                DateTime TimeIn = eAttendance.AttendanceTimeIn;
                TimeSpan TimeSinceLogin = DateTime.Now - eAttendance.AttendanceTimeIn;

                ((Label)e.Row.FindControl("lblSessionID")).Text = eSession.SessionID.ToString();
                ((Label)e.Row.FindControl("lblMachine")).Text = eAttendance.AttendanceMachineName;
                ((Label)e.Row.FindControl("AttendanceID")).Text = eAttendance.AttendanceID.ToString();
                ((Label)e.Row.FindControl("lblName")).Text = eResource.ResourceNameLast + ", " + eResource.ResourceNameFirst;
                ((Label)e.Row.FindControl("lblLoginTime")).Text = TimeIn.ToShortTimeString() + " (approx. " + Convert.ToInt32(Math.Ceiling(TimeSinceLogin.Duration().TotalMinutes)).ToString() + " minutes ago)";
                if(eSessionType.SessionTypeName.StartsWith("Tutoring") && eSession.SessionTutors.Count > 0) 
                {
                    SessionTutor eSessionTutor = eSession.SessionTutors.First();
                    Room eRoom = eSessionTutor.Room;
                    if (eRoom != null)
                        ((Label)e.Row.FindControl("lblSessionType")).Text = eSessionType.SessionTypeName + " - " + eRoom.RoomName;
                    else
                        ((Label)e.Row.FindControl("lblSessionType")).Text = eSessionType.SessionTypeName;
                }
                else 
                {
                    ((Label)e.Row.FindControl("lblSessionType")).Text = eSessionType.SessionTypeName;
                }
                ((Label)e.Row.FindControl("lblStatus")).Text = Status;
                ((Label)e.Row.FindControl("lblResourceType")).Text = eResourceType.ResourceTypeName;
                ((LinkButton)e.Row.FindControl("btnForceLogout")).CommandArgument = eAttendance.AttendanceID.ToString();

                RadToolTipManager.TargetControls.Add(e.Row.FindControl("lblName").ClientID, eResource.ResourceID.ToString(), true);
            }
        }

        protected void RadToolTipManager_OnAjaxUpdate(object sender, ToolTipUpdateEventArgs args)
        {
            UK.STATS.ReusableServerControls.ResourceInfoPopup infoPopup = new ReusableServerControls.ResourceInfoPopup();
            infoPopup.ResourceID = Guid.Parse(args.Value);
            args.UpdatePanel.ContentTemplateContainer.Controls.Add(infoPopup);
        }


        protected void gvSystemLoggedIn_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            STATSModel.Attendance[] eAttendances = STATSModel.Attendance.GetActive();
            int i = 0;
            switch (e.CommandName)
            {
                case "ForceLogout":
                    //i = Convert.ToInt32(e.CommandArgument);
                    //Label lblAttendanceID = (Label)((GridView)sender).Rows[i].Cells[0].FindControl("AttendanceID");
                    Guid AttendanceID = Guid.Parse(e.CommandArgument.ToString());
                    STATSModel.Attendance AttendanceObj = STATSModel.Attendance.Get(AttendanceID);
                    AttendanceObj.LogOut(ResourceTypeTutor, ResourceTypeStudent);
                    String Refresh = Request.Url.PathAndQuery;
                    Response.Redirect(Refresh);
                    break;

                case "SortSessionID":
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.SessionID);
                    gvSystemLoggedIn.DataBind();
                    break;

                case "SortLocations":
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.Session.SessionType.SessionTypeName);
                    gvSystemLoggedIn.DataBind();
                    break;

                case "SortResourceType":
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.Resource.ResourceType.ResourceTypeName);
                    gvSystemLoggedIn.DataBind();
                    break;

                case "SortName":
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.Schedule.Resource.ResourceNameDisplayType);
                    gvSystemLoggedIn.DataBind();
                    break;

                case "SortStatus":

                    // Create a list of the current open attendances and their status
                    List<KeyValuePair<System.Guid, string>> sortUs = new List<KeyValuePair<Guid, string>>();
                    i = 0;
                    foreach (STATSModel.Attendance curAttendance in eAttendances)
                    {
                        var ID = curAttendance.AttendanceID;
                        var Status = GetStatus(curAttendance.Schedule.Session);
                        var insertMe = new KeyValuePair<System.Guid, string>(ID, Status);

                        //sortMe[i] = insertMe;
                        sortUs.Add(insertMe);
                        i++;
                    }

                    // Sort the list of attendances by their status
                    sortUs.Sort(delegate(KeyValuePair<Guid, string> x, KeyValuePair<Guid, string> y) { return x.Value.CompareTo(y.Value); });

                    // Create a list of sorted attendances to bind to the gridview
                    List<STATSModel.Attendance> bindUs = new List<STATSModel.Attendance>();
                    foreach (KeyValuePair<System.Guid, string> curAttendance in sortUs)
                    {
                        bindUs.Add(STATSModel.Attendance.Get(curAttendance.Key));
                    }

                    gvSystemLoggedIn.DataSource = bindUs;
                    gvSystemLoggedIn.DataBind();
                    break;

                case "SortLoggedInSince":

                    //currentActive.Sort(delegate(GVActive x, GVActive y) { return x.Tutor.ResourceNameLast.CompareTo(y.Tutor.ResourceNameLast); });
                    //List<STATSModel.Attendance> sortUs2 = eAttendances.ToList<STATSModel.Attendance>();
                    //var temp = eAttendances[0].Schedule.Session.SessionDateTimeStart;
                    //var temp2 = eAttendances[1].Schedule.Session.SessionDateTimeStart;
                    //sortUs2.Sort(delegate(STATSModel.Attendance x, STATSModel.Attendance y) { return x.Schedule.Session.SessionDateTimeStart.CompareTo(y.Schedule.Session.SessionDateTimeStart); });
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.AttendanceTimeIn);

                    //gvSystemLoggedIn.DataSource = sortUs2;
                    gvSystemLoggedIn.DataBind();
                    break;
                case "SortMachine":
                    gvSystemLoggedIn.DataSource = eAttendances.OrderBy(obj => obj.AttendanceMachineName);
                    gvSystemLoggedIn.DataBind();
                    break;
                default:
                    break;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        private String GetStatus(STATSModel.Session eSession)
        {
            STATSModel.SessionType eSessionType = eSession.SessionType;
            STATSModel.SessionType eSessionTypeTutoringCATS = STATSModel.SessionType.GetByName("Tutoring - CATS");
            STATSModel.SessionType eSessionTypeTutoringFootball = STATSModel.SessionType.GetByName("Tutoring - Football");
            String Status = "Active";
            STATSModel.Resource[] eResourcesInSession = eSession.GetResourceInSession();
            int iResourceInSession = eResourcesInSession.Length;
            Boolean MultipleResourceInSession = (iResourceInSession > 1);

            if (eSessionType.SessionTypeID == eSessionTypeTutoringCATS.SessionTypeID || eSessionType.SessionTypeID == eSessionTypeTutoringFootball.SessionTypeID)
            { // This is a tutoring session.
                STATSModel.Resource eResourceTutor = eSession.GetTutor(ResourceTypeTutor);
                Boolean isTutorPresent = false; // Default this to false.
                if (eResourceTutor.ResourceID != Guid.Empty) { isTutorPresent = eResourceTutor.IsPresent(eSession); }
                if (eSession.SessionIsScheduled)
                { // Scheduled Tutoring Session
                    if (isTutorPresent)
                    {
                        Status = "Active - Scheduled";
                    }
                    else
                    {
                        Status = "Waiting - Scheduled";
                    }
                }
                else
                {
                    // Unscheduled Tutoring Session - Note: This also includes temporary sessions.

                    if (MultipleResourceInSession)
                    { // Multiple resources in a session requires that one of them is a Tutor.
                        if (isTutorPresent) // check whether the aforementioned tutor is present or not.
                        {
                            // Unscheduled session, yet a Tutor is present.  It's an active session.
                            Status = "Active - Unscheduled";
                        }
                        else
                        {
                            // No tutor is present.  One was supposedly here though...
                            // Note: This could happen if an unscheduled session was fulfilled, and then the tutor somehow left without killing the session.
                            // In all reality, this should rarely, if ever, happen.
                            Status = "Missing Tutor! - Unscheduled";
                        }
                    }
                    else
                    { // Just a single Resource, with nobody else scheduled for the unscheduled session.
                        Status = "Waiting - Unscheduled";
                    }
                }
            }
            return Status;
        }
    }
}