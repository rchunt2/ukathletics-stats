﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration
{
    public partial class ScheduleSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String strGroupID = Request.QueryString["GroupID"];
            Guid GroupID = Guid.Parse(strGroupID);
            Boolean HasDate = (!String.IsNullOrWhiteSpace(Request.QueryString["Date"]));

            if (!HasDate)
            {
                Response.Write("Enter A Date!");
                return;
            }

            DateTime selectedDate = new DateTime(long.Parse(Request.QueryString["Date"]));

            Execute(GroupID, selectedDate);
        }

        public struct ResourceCourse
        {
            public Session S;
            public Resource R;
        }

        public void Execute(Guid GroupID, DateTime SelectedTime)
        {
            while (SelectedTime < DateTime.Today)
            {
                SelectedTime = SelectedTime.AddDays(7);
            }
            DateTime RangeEnd = SelectedTime.AddHours(1);


            Group eGroup = Group.Get(GroupID);
            String GroupName = eGroup.GroupName;
            Resource[] eResources = eGroup.GetMembers();

            var SchedulesTutoring = new List<Schedule>();
            var SchedulesNote = new List<Schedule>();
            var SessionsClass = new List<ResourceCourse>();

            SessionType eSessionTypeTutoring = SessionType.GetByName("Tutoring - CATS");
            SessionType eSessionTypeNote = SessionType.GetByName("Note");

            foreach (Resource eResource in eResources)
            {
                Schedule[] ResourceSchedules = Schedule.Get(eResource, SelectedTime.Date, SelectedTime.Date.AddDays(1));
                CourseSection[] ResourceCourseSections = ResourceRegistration.GetRegisteredCourses(eResource);

                foreach (CourseSection eCourseSection in ResourceCourseSections)
                {
                    Session[] eSessions = ResourceRegistration.GetCourseSectionSessions(eCourseSection);
                    foreach (Session eSession in eSessions)
                    {
                        Boolean IsRelevant = OccursWithinRange(eSession, SelectedTime, RangeEnd);
                        if (IsRelevant)
                        {
                            SessionsClass.Add(new ResourceCourse() {S = eSession, R = eResource});
                        }
                    }
                }

                foreach (Schedule eSchedule in ResourceSchedules)
                {
                    Session eSession = eSchedule.Session;
                    Boolean isNote = (eSession.SessionTypeID == eSessionTypeNote.SessionTypeID);
                    Boolean isTutoring = (eSession.SessionTypeID == eSessionTypeTutoring.SessionTypeID);
                    Boolean IsRelevant = OccursWithinRange(eSession, SelectedTime, RangeEnd);

                    if (IsRelevant)
                    {
                        if (isNote)
                        {
                            SchedulesNote.Add(eSchedule);
                        }
                        if (isTutoring)
                        {
                            SchedulesTutoring.Add(eSchedule);
                        }
                    }
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Course Meeting Times");
            sb.AppendLine("Type,Start,ResourceName,Course");

            SessionsClass = (from ResourceCourse obj in SessionsClass orderby obj.S.SessionDateTimeStart select obj).ToList();
            foreach (ResourceCourse eResourceCourse in SessionsClass)
            {
                var eResource = eResourceCourse.R;
                var eSession = eResourceCourse.S;
                var eCourseSection = CourseSection.Get(eSession.CourseSectionID.GetValueOrDefault());

                String tStart = eSession.SessionDateTimeStart.ToShortTimeString();
                String Name = eResource.ResourceNameDisplayLastFirst;
                String Notes = eCourseSection.CourseDisplayName;

                String s = "Class," + tStart + "," + '"' + Name + '"' + "," + '"' + Notes + '"';
                sb.AppendLine(s);
            }

            sb.AppendLine("");
            sb.AppendLine("Tutoring Sessions");
            sb.AppendLine("Type,Start,ResourceName");
            SchedulesTutoring = (from Schedule obj in SchedulesTutoring orderby obj.Session.SessionDateTimeStart select obj).ToList();
            foreach (Schedule eSchedule in SchedulesTutoring)
            {
                Session eSession = eSchedule.Session;
                Resource eResource = eSchedule.Resource;

                String tStart = eSession.SessionDateTimeStart.ToShortTimeString();
                String Name = eResource.ResourceNameDisplayLastFirst;

                String s = "Tutoring," + tStart + "," + '"' + Name + '"' + "";
                sb.AppendLine(s);
            }

            sb.AppendLine("");
            sb.AppendLine("Freeform Entries");
            sb.AppendLine("Type,Start,ResourceName,Text");
            SchedulesNote = (from Schedule obj in SchedulesNote orderby obj.Session.SessionDateTimeStart select obj).ToList();
            foreach (Schedule eSchedule in SchedulesNote)
            {
                Session eSession = eSchedule.Session;
                Resource eResource = eSchedule.Resource;
                Annotation eAnnotation = Annotation.Get(eSchedule.ScheduleID).First();

                String tStart = eSession.SessionDateTimeStart.ToShortTimeString();
                String Name = eResource.ResourceNameDisplayLastFirst;
                String Notes = eAnnotation.AnnotationText;

                String s = "Freeform," + tStart + "," + '"' + Name + '"' + "," + '"' + Notes + '"';
                sb.AppendLine(s);
            }

            String CSVData = sb.ToString();
            String Time = String.Format("{0:ddd-HH-mm}",SelectedTime);
            String filename = GroupName + "-" + Time + ".csv";
            ExportCSV(CSVData, filename);
        }

        private Boolean OccursWithinRange(Session eSession, DateTime StartTime, DateTime EndTime)
        {
            // The range was originally inclusive.  Not anymore.
            StartTime = StartTime.AddMinutes(1);
            EndTime = EndTime.AddMinutes(-1);

            bool Conflict = false;

            // Key:
            //     N = New Session (StartTime, EndTime)
            //     E = Existing Session (ExistingSession.SessionDateTimeStart, ExistingSession.SessionDateTimeEnd)

            // Case 1: Starts after Existing Session Starts and also starts before Existing Session Ends
            //       N-------N
            //    E------E 
            if (StartTime >= eSession.SessionDateTimeStart && StartTime < eSession.SessionDateTimeEnd)
            {
                Conflict = true;
            }

            // Case 2: Ends before the Existing Session Ends, and also Ends after the Existing Session Starts
            //    N-------N
            //        E---------E
            if (EndTime <= eSession.SessionDateTimeEnd && EndTime > eSession.SessionDateTimeStart)
            {
                Conflict = true;
            }

            // Case 3: Starts before the Existing Session Starts, and Ends after the Existing Session Ends
            //  N----------N
            //     E----E
            if (StartTime <= eSession.SessionDateTimeStart && EndTime >= eSession.SessionDateTimeEnd)
            {
                Conflict = true;
            }

            return Conflict;
        }

        public void ExportCSV(String CSVData, String Filename)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + Filename);
            context.Response.Write(CSVData);
            context.Response.End();
        }
    }
}