﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace UK.STATS.Administration
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // This is causing an issue when the first HTTP request (the start of a new session)
            // is requesting a Resource Calendar
            // Response.Redirect("~/Login.aspx", true);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.Print(Request.RawUrl);
                Context.Items.Add("startime", DateTime.Now);
            }
            catch (Exception)
            { }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.Print(Request.RawUrl);

                //Get the start time
                DateTime dt = (DateTime)Context.Items["startime"];

                //calculate the time difference between start and end of request
                TimeSpan ts = DateTime.Now - dt;
                System.Diagnostics.Debug.Print(Request.RawUrl + " -- " + ts.TotalMilliseconds.ToString("N0"));
            }
            catch (Exception)
            { }
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                if (Request.RawUrl.ToLower().Contains("favicon"))
                {
                    //Ignore 404 errors for favicon
                    return;
                }
                Exception ex = Context.Server.GetLastError();
                var eResource = UK.STATS.Administration.Helpers.UKHelper.CurrentAuthentication().Resource;
                var IPAddress = HttpContext.Current.Request.UserHostAddress;
                UK.STATS.STATSModel.Error.LogException(ex, eResource, IPAddress);
            }
            catch (Exception)
            {
                //ignore errors logging errors
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}