﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UK.STATS.Administration.Pages
{
    public partial class MessagePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["resourceid"]))
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
                STATSModel.Resource ResourceObj = STATSModel.Resource.Get(ResourceID);
                MessagingModule.FromResourceID = ResourceID;
                MessagingModule.RecipientID = ResourceID;
                MessagingModule.TypeOfRecipient = UserControls.Messaging.RecipientType.Resource;
                MessagingModule.Mode = UserControls.Messaging.MessagingMode.Inbox;
                MessagingModule.BindAndShow();
            }
            else
            {
                Response.Write("Messages cannot be viewed at this time.");
            }
        }
    }
}