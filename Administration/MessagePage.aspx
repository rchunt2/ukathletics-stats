﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MessagePage.aspx.cs" Inherits="UK.STATS.Administration.Pages.MessagePage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="Messaging" Src="~/UserControls/Messaging.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>UK C.A.T.S Messages</title>
    <link href="./Stylesheets/Standard.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    body { background-color: #FFF !important; width:100%; margin: 0 !important; padding: 0 !important; }
    
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <SIS:Messaging ID="MessagingModule" runat="server" />
    </form>
</body>
</html>

