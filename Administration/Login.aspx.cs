﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace UK.STATS.Administration
{
    public partial class Login : System.Web.UI.Page
    {
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        public STATSModel.ResourceType ResourceTypeDirector
        {
            get
            {
                if (Application["ResourceTypeDirector"] == null)
                {
                    Application["ResourceTypeDirector"] = STATSModel.ResourceType.Get("Director");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeDirector"];
            }
        }

        public STATSModel.ResourceType ResourceTypeAdministrator
        {
            get
            {
                if (Application["ResourceTypeAdministrator"] == null)
                {
                    Application["ResourceTypeAdministrator"] = STATSModel.ResourceType.Get("Administrator");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeAdministrator"];
            }
        }

        public STATSModel.ResourceType ResourceTypeTimeMgr
        {
            get
            {
                if (Application["ResourceTypeTimeMgr"] == null)
                {
                    Application["ResourceTypeTimeMgr"] = STATSModel.ResourceType.Get("Time Mgr");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTimeMgr"];
            }
        }

        public STATSModel.ResourceType ResourceTypeMonitor
        {
            get
            {
                if (Application["ResourceTypeMonitor"] == null)
                {
                    Application["ResourceTypeMonitor"] = STATSModel.ResourceType.Get("Monitor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeMonitor"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblApplicationName.Text = STATSModel.Setting.GetByKey("ApplicationName").SettingValue.ToString();

            STATSModel.Authentication.ExpireSessions();
            Guid AuthKey = Guid.Empty;
            if (Session["AuthenticationKey"] != null)
            {
                AuthKey = Guid.Parse(Session["AuthenticationKey"].ToString());
            }

            if (AuthKey != Guid.Empty)
            {
                Response.Redirect("Default.aspx", true);
            }

            var txtLogin = (TextBox)LoginControl.FindControl("UserName");
            txtLogin.Focus();
        }


        public void LoginControl_Authenticate(object sender, AuthenticateEventArgs e)
        {
            STATSModel.Authentication.ExpireSessions();

            string resourceLogin = LoginControl.UserName;
            string resourcePass = LoginControl.Password;
            bool adLoginAssociatedWithResource = STATSModel.Resource.IsADLoginAssociatedWithResource(resourceLogin);
            bool validLegacyLogin = STATSModel.Resource.IsValidLegacyLogin(resourceLogin, resourcePass);
            bool validADLogin = false;
            bool legacyLoginHasADLogin = false;
            if (!validLegacyLogin)
            {
                //only Check for valid AD login if it's not a Legacy login
                validADLogin = STATSModel.Resource.IsValidADLogin(resourceLogin, resourcePass);
            }
            else
            {
                legacyLoginHasADLogin = STATSModel.Resource.DoesResourceHaveADLoginAssociated(resourceLogin);
            }

            if (!validADLogin && !validLegacyLogin)
            {
                //Login is not a valid AD login and not a valid legacy login
                LoginControl.FailureText = "Invalid username or password";
                return;
            }

            if (validLegacyLogin)
            {
                if (legacyLoginHasADLogin)
                {
                    //Legacy login, but already has an AD account associated...
                    LoginControl.FailureText = "You have tried logging in using a legacy user/pass, you must use your Active Directory login instead.";
                    LoginControl.UserName = "";
                    ((TextBox)LoginControl.FindControl("UserName")).Text = "";
                }
                else
                {
                    if (Session["NewADLoginID"] != null)
                    {
                        //associate the AD login with the resource using the old legacy login to map it
                        bool success = STATSModel.Resource.AssociateADLogin(resourceLogin, Session["NewADLoginID"] as string);
                        if (success)
                        {
                            adLoginAssociatedWithResource = true;
                            resourceLogin = Session["NewADLoginID"] as string;
                        }
                        else
                        {
                            LoginControl.FailureText = "Could not associate AD login: " + (Session["NewADLoginID"] as string) + " for legacy user: " + resourceLogin;
                        }
                    }
                    else
                    {
                        //Legacy login, ask for new AD account to associate with
                        LoginControl.FailureText = "You have tried logging in using a legacy user/pass, please now enter your Active Directory login.";
                        LoginControl.UserName = "";
                        ((TextBox)LoginControl.FindControl("UserName")).Text = "";
                        Session.Add("LegacyLoginID", resourceLogin);
                    }
                }
            }

            if (validADLogin && !adLoginAssociatedWithResource)
            {
                if (Session["LegacyLoginID"] != null)
                {
                    //associate the AD login with the resource using the old legacy login to map it
                    bool success = STATSModel.Resource.AssociateADLogin(Session["LegacyLoginID"] as string, resourceLogin);
                    if (success)
                    {
                        adLoginAssociatedWithResource = true;
                    }
                    else
                    {
                        LoginControl.FailureText = "Could not associate AD login: " + resourceLogin + " for legacy user: " + (Session["LegacyLoginID"] as string);
                    }
                }
                else
                {
                    LoginControl.FailureText = "You have entered a valid Active Directory login, but it is not associated with a Resource. Login using your old (legacy) user/pass.";
                    LoginControl.UserName = "";
                    ((TextBox)LoginControl.FindControl("UserName")).Text = "";
                    Session.Add("NewADLoginID", resourceLogin);
                }
            }

            if (adLoginAssociatedWithResource && validADLogin)
            {
                //We have a valid AD login, and the AD login is asscoiated with a Resource, allow the login.
                var eResource = STATSModel.Resource.GetByADLogin(resourceLogin);

                if (eResource.ResourceTypeID == ResourceTypeDirector.ResourceTypeID ||
                    eResource.ResourceTypeID == ResourceTypeAdministrator.ResourceTypeID ||
                    eResource.ResourceTypeID == ResourceTypeTimeMgr.ResourceTypeID ||
                    eResource.ResourceTypeID == ResourceTypeMonitor.ResourceTypeID)
                {
                    e.Authenticated = true;
                }

                STATSModel.Authentication LoginAuthentication = STATSModel.Authentication.Get(eResource);
                Guid AuthenticationID = LoginAuthentication.AuthenticationID;
                // Save that information to the user's session for convenient usage!
                Session.Clear();
                Session.Add("AuthenticationKey", AuthenticationID);
            }
        }

        protected void LoginControl_LoggedIn(object sender, EventArgs e)
        {

        }
    }
}