﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration
{
    public partial class ViewAllImages : System.Web.UI.Page
    {

        public string GetImage(object img)
        {
            return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            STATSModel.STATSEntities db = new STATSModel.STATSEntities();

            var images = (from p in db.ResourcePictures
                          select p);


            Repeater1.DataSource = images;
            Repeater1.DataBind();
        }
    }
}