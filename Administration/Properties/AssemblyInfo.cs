﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UK STATS UK.STATS.Administration")]
[assembly: AssemblyDescription("STATS UK.STATS.Administration")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Software Information Systems")]
[assembly: AssemblyProduct("STATS UK.STATS.Administration")]
[assembly: AssemblyCopyright("Copyright Software Information Systems 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fc8094f8-333e-4b06-b150-3a7b54756b01")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2013.0523.*")]