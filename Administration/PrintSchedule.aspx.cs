﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;


namespace UK.STATS.Administration
{
    public partial class PrintSchedule : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Boolean resourceInitialized = false;
            Boolean isGroup = false;
            Boolean isCompare = false;
            STATSModel.Resource ResourceObj = new STATSModel.Resource();
            STATSModel.Group GroupObj = new STATSModel.Group();
            STATSModel.Resource ResourceObjCompare = new STATSModel.Resource();
            if (!String.IsNullOrEmpty(Request.QueryString["resourceid"]))
            {
                Guid ResourceID = Guid.Parse(Request.QueryString["resourceid"]);
                ResourceObj = STATSModel.Resource.Get(ResourceID);
                ResourceScheduleControl.Visible = true;
                resourceInitialized = true;
                if(!String.IsNullOrEmpty(Request.QueryString["resourceidcompare"]))
                {
                    Guid ResourceIDCompare = Guid.Parse(Request.QueryString["resourceidcompare"]);
                    ResourceObjCompare = STATSModel.Resource.Get(ResourceIDCompare);
                    isCompare = true;
                }
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["resourcelogin"]))
            {
                String ResourceLogin = Request.QueryString["resourcelogin"];
                ResourceObj = STATSModel.Resource.Get(ResourceLogin);
                ResourceScheduleControl.ResourceID = ResourceObj.ResourceID;
                ResourceScheduleControl.Visible = true;
                resourceInitialized = true;
            }

            else if (!String.IsNullOrEmpty(Request.QueryString["groupid"]))
            {
                Guid GroupID = Guid.Parse(Request.QueryString["groupid"]);
                GroupObj = STATSModel.Group.Get(GroupID);
                ResourceScheduleControl.Visible = true;
                resourceInitialized = true;
                isGroup = true;

            }

            if (resourceInitialized == true)
            {
                ResourceScheduleControl.Admin = false;
                if (isGroup)
                {
                    ResourceScheduleControl.GroupID = GroupObj.GroupID;
                }
                else
                {
                    ResourceScheduleControl.ResourceID = ResourceObj.ResourceID;
                    if (isCompare)
                    {
                        ResourceScheduleControl.ComparisonResourceID = ResourceObjCompare.ResourceID;
                        ResourceScheduleControl.RenderComparison = true;
                    }
                    else
                    {
                        ResourceScheduleControl.RenderComparison = false;
                    }
                    ResourceScheduleControl.Printing = true;
                    if (ResourceScheduleControl.Printing)
                    {
                        ScriptManager.RegisterClientScriptBlock(ResourceScheduleControl, ResourceScheduleControl.GetType(), "SchedulerNotifier", "<script language='javascript'>printPage();</script>", false);
                    }
                }
                ResourceScheduleControl.Visible = true;
                ResourceScheduleControl.RunScheduler(false);
            }
            else
            {
                Response.Write("The Schedule cannot be printed at this time.");
                ResourceScheduleControl.Visible = false;
            }
        }
    }
}