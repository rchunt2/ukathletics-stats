﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAllImages.aspx.cs" Inherits="UK.STATS.Administration.ViewAllImages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <div>

        <asp:Repeater  ID="Repeater1"  runat="server">
            <ItemTemplate>
                <asp:Label ID="Label1" Text='<%#  Convert.ToString(Eval("ResourcePictureBytes")) %>' runat="server" ></asp:Label>
                <asp:Image ID="Image2" ImageUrl='<%#GetImage(Eval("ResourcePictureBytes")) %>' runat="server" />
            </ItemTemplate>

        </asp:Repeater>
        <asp:Image ID="Image1" runat="server" />
    </div>
    </form>
</body>
</html>
