﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TimeTrackerPage.aspx.cs" Inherits="UK.STATS.Administration.Pages.TimeTrackerPage" %>
<%@ Register TagPrefix="SIS" TagName="TimeTracker" Src="~/UserControls/TimeTracker.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblError" runat="server" Text="lblError" Visible="false"></asp:Label>
    <SIS:TimeTracker ID="TimeTrackerModule" runat="server" />
    </form>
</body>
</html>
