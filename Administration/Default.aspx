﻿<%@ Page Title="Home" Language="C#" AutoEventWireup="true" MasterPageFile="~/Layout.Master" CodeBehind="Default.aspx.cs" Inherits="UK.STATS.Administration.Default" EnableEventValidation="false" %>

<%@ Register TagPrefix="SIS" TagName="SystemLoggedIn" Src="~/UserControls/SystemLoggedIn.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_MainContents" runat="server">
    

    <SIS:SystemLoggedIn ID="SystemLoggedInControl" runat="server" />

</asp:Content>