﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintScheduleBatch.aspx.cs" Inherits="UK.STATS.Administration.Reports.PrintScheduleBatch" %>

<%@ Register TagPrefix="SIS" TagName="ResourceSchedule" Src="~/UserControls/ResourceSchedule.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Schedule</title>
    <link rel="stylesheet" href="../Stylesheets/Print.css" type="text/css" media="print" />

    <style type="text/css">
        body
        {
            width: 8in !important;
        }

        .rsSpacerCell, .rsVerticalHeaderWrapper, .rsVerticalHeaderWrapper *
        {
            width: 50px !important;
        }

        .rsContentWrapper
        {
            /* width: 7in !important; */
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="noPrint" style="float: right;">
                <a href="#" onclick="javascript: print(); return false;">Print</a>
            </div>

            <asp:Repeater ID="rptScheduleBatch" runat="server" OnItemDataBound="rptScheduleBatch_ItemDataBound">
                <ItemTemplate>
                    <div style="page-break-after: always">
                        <SIS:ResourceSchedule ID="ResourceScheduleControl" runat="server" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>