﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace UK.STATS.Administration.Reports
{
    public static class ReportHelper
    {
        public static String GetXMLFromResourceIDArray(Guid[] ResourceArray)
        {
            StringWriter stringWriter = new StringWriter();
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Guid[]));
            xmlSerializer.Serialize(stringWriter, ResourceArray);
            String xmlString = stringWriter.ToString();
            return xmlString;
        }

    }
}