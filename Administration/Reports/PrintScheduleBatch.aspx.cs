﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UK.STATS.STATSModel;

namespace UK.STATS.Administration.Reports
{
    public partial class PrintScheduleBatch : System.Web.UI.Page
    {
        private bool? _onlyShowClasses = null;

        /// <summary>
        /// Determines rather the schedule control should only show classes or not
        /// </summary>
        private bool OnlyShowClasses
        {
            get
            {
                if (_onlyShowClasses == null) //only do the parsing once per page load
                {
                    bool onlyShowClasses = false;

                    if (!string.IsNullOrWhiteSpace(Request.QueryString["OnlyShowClasses"]))
                    {
                        bool.TryParse(Request.QueryString["OnlyShowClasses"], out onlyShowClasses);
                    }
                    _onlyShowClasses = onlyShowClasses;
                }

                return _onlyShowClasses.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["BatchScheduleList"] != null)
            {
                List<Guid> lstGuid = (List<Guid>)Session["BatchScheduleList"];
                rptScheduleBatch.DataSource = lstGuid;
                rptScheduleBatch.DataBind();
            }
            else
            {
                Response.Write("No data supplied!");
            }
        }

        protected void rptScheduleBatch_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Guid eResourceGuid = (Guid)e.Item.DataItem;
            var ResourceScheduleControl = (UK.STATS.Administration.UserControls.ResourceSchedule)e.Item.FindControl("ResourceScheduleControl");

            ResourceScheduleControl.Admin = false;
            ResourceScheduleControl.ResourceID = eResourceGuid;
            ResourceScheduleControl.Printing = false;
            ResourceScheduleControl.RunScheduler(this.OnlyShowClasses);
        }
    }
}