﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintSchedule.aspx.cs" Inherits="UK.STATS.Administration.PrintSchedule" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="SIS" TagName="ResourceSchedule" Src="~/UserControls/ResourceSchedule.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UK C.A.T.S Schedule</title>
    <link rel="stylesheet" href="./Stylesheets/Print.css"  type="text/css" media="print"/>
    <style type="text/css">
                  
        body
        {
        	width: 8in !important;
        }
                                    
        .rsSpacerCell, .rsVerticalHeaderWrapper, .rsVerticalHeaderWrapper *
        {
        	width: 50px !important;
        }
        
        .rsContentWrapper
        {
        	/* width: 7in !important; */
        }
    
    </style>
    <script language="javascript">
        function printPage() {
//            if (confirm("Would you like to print the schedule?")) 
//            {
//                alert("For best results, print in Landscape mode");
//                print();
//            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <SIS:ResourceSchedule ID="ResourceScheduleControl" runat="server" />
    </form>
</body>
</html>
