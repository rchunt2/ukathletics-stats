﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using UK.STATS.STATSModel;
using System.Drawing.Imaging;
namespace UK.STATS.Administration
{





    public partial class ResizeExistingImages : System.Web.UI.Page
    {
       

        public string GetImage(object img)
        {
            return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
        }

        public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

           // var resized = ImageUtilities.ResizeImage(image, newWidth, newHeight);

            //save the resized image as a jpeg with a quality of 90
            //    ImageUtilities.SaveJpeg(@"C:\myimage.jpeg", resized, 90);



            var newImage = new Bitmap(newWidth, newHeight);


            using (Graphics gr = Graphics.FromImage(newImage))
            {

                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight));


            }

            //return resized;


             Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }

        public static System.Drawing.Image CreateImgFromBytes(byte[] image)
        {
            System.Drawing.Image img;
            using (MemoryStream stream = new MemoryStream(image))
            {
                try
                {
                    img = System.Drawing.Image.FromStream(stream);
                }
                catch (ArgumentException ex)
                {
                    throw new ArgumentException("The provided binary data may not be valid image or may contains unknown header", ex);
                }
            }
            return img;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            STATSModel.STATSEntities db = new STATSModel.STATSEntities();

            Guid abc = new Guid("13f999e0-a8f1-e311-a713-0050568a0003");



            List<STATSModel.ResourcePicture> images = (from p in db.ResourcePictures

                                                       select p).ToList();

            foreach (ResourcePicture img in images)
            {
                System.Drawing.Image newImage = CreateImgFromBytes(img.ResourcePictureBytes);
                if (newImage.Height > 145 || newImage.Width > 105)
                {
                    System.Drawing.Image scaledImage = ScaleImage(newImage, 105, 145);
                    img.ResourcePictureBytes = imageToByteArray(scaledImage);
                }
                //Label1.Text = GetImage(imageToByteArray(scaledImage));
                //   Image2.ImageUrl = GetImage(imageToByteArray(scaledImage));
            }

            db.SaveChanges();

            //  Label1.Text = images.Count().ToString();
        }
    }
}