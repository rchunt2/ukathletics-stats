﻿<%@ Page Title="STATS Study Hall (Football Location)" Language="C#" AutoEventWireup="true" MasterPageFile="~/UserAuthentication.Master" CodeBehind="FootballLocation.aspx.cs" Inherits="UK.STATS.StudyHall.FootballLocation" EnableEventValidation="false" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Navigate() {
            var url = '<%# WebHost %>./PrintSchedule.aspx';
            var e = document.getElementById("txtResourceLogin");
            var query = "?resourcelogin=" + e.value;
            var url2 = url + query;
            window.open(url2, "Print Schedule", "menubar=0, directories=0, location=0, toolbar=0, status=0");
        }
    </script>
    <link href="./Stylesheets/StudyHall.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .rlbItem
        {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBody" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" EnablePartialRendering="True"></asp:ScriptManager>
    <script type="text/javascript">
        document.write("\<script src='https://code.jquery.com/jquery-latest.min.js' type='text/javascript'>\<\/script>");
    </script>
    <script type="text/javascript">
        function detectEvent(e) {
            var evt = window.event;
            
                if ($('input[id*="hfPageToggle"][type="hidden"]').val() == "true") {
                    if (evt.keyCode == 13) {
                        $('input[id*="hfPageToggle"][type="hidden"]').val("false");
                        $('input[id*="btnLogin"]').click();
                    }
                }
                else {
                    if (evt.keyCode == 13) {
                        var messagesRead = $('input[id*="btnBackToMain"]').attr("disabled");
                        if (messagesRead != "disabled") {
                            //alert("Your login is being processed. Please press 'Enter' or click 'OK' to continue.");
                            $('input[id*="hfPageToggle"][type="hidden"]').val("true");
                            $('input[id*="btnBackToMain"]').click();
                        }
                        else {
                            alert("Please read all of your messages before proceeding to login.");
                        }
                    }
                }
            
        }

        $(document).keyup(detectEvent);

        //        function loginService() {
        //            var login = $('input[id*="txtResourceLogin"][type="text"]').val();
        //            if (login != "") {
        //                $.ajax({
        //                    type: "POST",
        //                    url: "Services/LoginService.asmx/UserLogin",
        //                    contentType: "application/json; charset=utf-8",
        //                    dataType: "json",
        //                    data: " { login: \"" + login + " \" } ",
        //                    cache: false,
        //                    success: function (data) {
        //                        if (data) {
        ////                            var result = JSON.stringify(data)
        ////                            var result = String(data);
        ////                            alert(data.d);
        ////                            $("#hfUserId").val(login);
        //                            if (data.d == "Success") {
        //                                window.location = window.location + "?login=" + login;
        //                            }
        //                        }
        //                    },
        //                    error: function (xhr, ajaxOptions, thrownError) {
        //                        alert("Error: " + xhr.statusText);
        //                    }
        //                });
        //            }
        //            else return;
        //        }

        function confirmProceed(confirmed) {
            if (confirmed) {
                $('input[id*="hfPageToggle"][type="hidden"]').val("true");
                //                alert("you have been confirmed");
            }
            else {
                $('input[id*="hfPageToggle"][type="hidden"]').val("false");
                //                alert("you have NOT been confirmed");
                window.location = window.location;
            }
        }
    </script>
    <%--    <input type="hidden" id="hfUserId" runat="server" value="login" />--%>
    <input type="hidden" id="hfPageToggle" runat="server" />
    <input type="hidden" runat="server" id="hfUserResponse" />
    
     
    <asp:UpdatePanel ID="upPage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="studyHallContainer">
                <asp:Panel ID="pLoginStudents" Visible="true" runat="server">
                    <div id="loginContainer" style="width: 775px; margin: auto;">
                        <div class="loginPanelRight">
                            <div class="LoginScreenTitle">
                                <img class="LoginScreenLogo" alt="UK STATS Logo" src="Images/Logo_Full.png" /><br />
                                University of Kentucky - Ohio Casualty Group<br />
                                Center for Academic and Tutorial Services (C.A.T.S.)<br />
                                Football Location
                            </div>
                            <div class="LoginScreenForm" style="text-align: center; margin: auto">
                                <asp:UpdatePanel runat="server" ID="upLogin" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        Enter User Login:<br />
                                        <asp:TextBox ClientIDMode="Static" ID="txtResourceLogin" runat="server" Width="150px" MaxLength="9" Name="txtResourceLogin"></asp:TextBox><br />
                                        <br />
                                        <asp:UpdatePanel runat="server" ID="upButtons" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btnLogin" runat="server" Height="40px" Text="Login" Width="150px" OnClick="btnLogin_Click" />
                                                <br />
                                                <asp:Button ID="btnRefresh" runat="server" Height="40px" Text="Refresh" Width="150px" UseSubmitBehavior="False" CausesValidation="False" OnClick="btnRefresh_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <br />
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <asp:UpdatePanel runat="server" ID="upTime" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="TutorApplicationLargeTime">
                                        <asp:Label ID="lblCurrentTime" runat="server" Text="lblCurrentTime" />
                                    </div>
                                    <div class="TutorApplicationLargeDate">
                                        <asp:Label ID="lblCurrentDate" runat="server" Text="lblCurrentDate" />
                                    </div>
                                    <div class="TutorDebug">
                                        <asp:Label ID="lblSyncTime" runat="server" Text="lblSyncTime" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="StudyHallLeft">
                            <strong>Students Logged In </strong> - <asp:Label ID="lblNumberLoggedIn" runat="server"></asp:Label>

                            <asp:UpdatePanel runat="server" ID="upList" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <telerik:RadToolTipManager ID="RadToolTipManager" runat="server" Position="BottomRight"
                                          Animation="Fade" OnAjaxUpdate="RadToolTipManager_OnAjaxUpdate" RelativeTo="Mouse" 
                                          RenderInPageRoot="true" Skin="Web20" AutoCloseDelay="0">
                                     </telerik:RadToolTipManager>
                                    <telerik:RadGrid ID="grdStudyHallStudent" OnItemDataBound="grdStudyHallStudent_ItemDataBound" OnNeedDataSource="grdStudyHallStudent_NeedDataSource" runat="server" Height="570px" Width="99%"
                                        AllowSorting="true"  >
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="true" />
                                            <Scrolling AllowScroll="True" SaveScrollPosition="true" UseStaticHeaders="True" FrozenColumnsCount="2"></Scrolling>
                                        </ClientSettings>
                                        <SortingSettings SortedBackColor="#FFF6D6" EnableSkinSortStyles="false"></SortingSettings>
                                        <MasterTableView AutoGenerateColumns="false" DataKeyNames="AttendanceID">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="AttendanceID" HeaderText="AttendanceID" SortExpression="AttendanceID"
                                                    UniqueName="AttendanceID" Visible="false" >
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ResourceID" HeaderText="ResourceID" SortExpression="ResourceID"
                                                    UniqueName="ResourceID" Visible="false" >
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Left" DataField="ResourceName" HeaderText="Name" SortExpression="ResourceName"
                                                    UniqueName="ResourceName">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn ItemStyle-HorizontalAlign="Left" DataField="AttendanceTimeIn" HeaderText="Time In" SortExpression="AttendanceTimeIn"
                                                    UniqueName="AttendanceTimeIn" DataFormatString="{0:t}">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <div style="margin: auto">
                                        <asp:Button ID="btnLogoutStudent" runat="server" Height="40px" Text="Logout Student" CssClass="LogoutButton" OnClick="btnLogoutStudent_Click" UseSubmitBehavior="False" />
                                    </div>
                                    <asp:Timer ID="timerUpdatePage" runat="server" OnTick="timerUpdatePage_Tick"></asp:Timer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pViewMessages" runat="server" Visible="false">
                    <div class="TimeTracker">
                        <iframe id="ifTimeTracker" src="" width="100%" height="270px" runat="server"></iframe>
                    </div>
                    <iframe id="ifMessage" src="" width="100%" height="400px" runat="server"></iframe>
                    <asp:UpdatePanel ID="upContinueLogin" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Timer ID="tmrContinueLogin" runat="server" OnTick="tmrContinueLogin_Tick"></asp:Timer>
                            <asp:Button ID="btnBackToMain" runat="server" Height="60px" Text="Continue Login" Enabled="false" Width="165px" OnClick="btnContinueLogin_Click" BackColor="#99CCFF" />
                            <asp:HyperLink ID="hpPrintSchedule" Target="_blank" NavigateUrl="" runat="server">Print Schedule</asp:HyperLink>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <asp:UpdatePanel ID="upTimeTrackingPopup" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <telerik:RadWindow ID="rwLogin" runat="server"></telerik:RadWindow>
                    <%-- Skin="Windows7"--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
 