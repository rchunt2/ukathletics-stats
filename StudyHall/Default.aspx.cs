﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using UK.STATS.STATSModel;

namespace UK.STATS.StudyHall
{
    public partial class Default : System.Web.UI.Page
    {
        private DateTime _dateStart = DateTime.MinValue;
        private DateTime _dateEnd = DateTime.MaxValue;
        private STATSModel.SessionType CurrentSessionType = STATSModel.SessionType.GetByName("Study Hall - CATS");

        //DM - 9/25/2012 - changed resource types to pull from Application variable to prevent lots of unnessary hits to the DB since they do not change.
        public STATSModel.ResourceType ResourceTypeTutor
        {
            get
            {
                if (Application["ResourceTypeTutor"] == null)
                {
                    Application["ResourceTypeTutor"] = STATSModel.ResourceType.Get("Tutor");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeTutor"];
            }
        }

        public STATSModel.ResourceType ResourceTypeStudent
        {
            get
            {
                if (Application["ResourceTypeStudent"] == null)
                {
                    Application["ResourceTypeStudent"] = STATSModel.ResourceType.Get("Student");
                }
                return (STATSModel.ResourceType)Application["ResourceTypeStudent"];
            }
        }

        private int StudyHallSyncInterval
        {
            get
            {
                String strStudyHallSyncInterval = UK.STATS.STATSModel.Setting.GetByKey("StudyHallSyncInterval").SettingValue;
                int SyncInterval = Convert.ToInt32(strStudyHallSyncInterval);
                return SyncInterval;
            }
        }

        private Boolean AutoUpdateEnabled
        {
            get { return (StudyHallSyncInterval > 0); }
        }

        public void ForceReloadPage()
        {
            Response.Redirect(Request.Url.OriginalString, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            STATSModel.Attendance.AutoLogout(ResourceTypeTutor, ResourceTypeStudent);

            rwLogin.VisibleOnPageLoad = false;

            if (!Page.IsPostBack)
            {
                UpdateActive();
                UpdateButtons();
            }

            if (btnBackToMain.Visible)
            {
                btnBackToMain.Focus(); // Set focus on the "Continue Login" button.
            }

            if (txtResourceLogin.Visible)
            {
                txtResourceLogin.Focus();
                hfPageToggle.Value = "true";
            }
            else
            {
                hfPageToggle.Value = "false";
            }

            btnLogin.Enabled = CurrentSessionType.CanLogin;
            btnLogin.Text = CurrentSessionType.CanLogin ? "Login" : "Closed";

            PrepareTimer();
            UpdateDateTimeDisplay();

            if (!Page.IsPostBack)
            {
                tmrContinueLogin.Interval = 2500;

                //tmrContinueLogin.Interval = 60000;
                tmrContinueLogin.Enabled = true;
            }

            //var login = Request.QueryString["login"];
            //if (login != "" && login != null)
            //{
            //    txtResourceLogin.Text = login;
            //    btnLogin_Click(sender, e);
            //}
        }

        private void PrepareTimer()
        {
            int UpdateInterval = StudyHallSyncInterval;
            timerUpdatePage.Enabled = true; // Always true, even if disabled.  (The page only updates if it's enabled though.)
            if (!AutoUpdateEnabled)
            {
                UpdateInterval = 60; // The page should not update, but should still check for global settings changes.
            }
            timerUpdatePage.Interval = UpdateInterval * 1000;
            UpdateDateTimeDisplay();
        }

        private void UpdateButtons()
        {
            Boolean IsOpen = CurrentSessionType.CanLogin;

            btnLogin.Enabled = IsOpen; // If Tutoring is open, enable the login button.

            if (IsOpen)
            {
                btnLogin.Text = "Login";
                btnLogin.CssClass = "";
            }
            else
            {
                btnLogin.Text = "Closed";
                btnLogin.CssClass = "LoginButtonClosed";
            }

            upButtons.Update();
        }

        private void UpdateDateTimeDisplay()
        {
            lblCurrentTime.Text = DateTime.Now.ToShortTimeString();
            lblCurrentDate.Text = DateTime.Now.ToShortDateString();
            lblSyncTime.Text = "Last Refreshed: " + DateTime.Now.ToLongTimeString();
            upTime.Update();
        }

        protected void timerUpdatePage_Tick(object sender, EventArgs e)
        {
            if (AutoUpdateEnabled) // Don't update unless we're supposed to.
            {
                UpdateActive();
                UpdateDateTimeDisplay();
            }

            UpdateButtons();
        }

        protected void tmrContinueLogin_Tick(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteError", "<script language='javascript'>alert('rebinding inbox in tmrContinueLogin_Tick');</script>", false);
            String ResourceLogin = txtResourceLogin.Text;
            STATSModel.Resource ResourceObj = STATSModel.Resource.Get(ResourceLogin);
            if (STATSModel.MessageInbox.NumberMessagesUnread(ResourceObj) > 0)
            {
                btnBackToMain.Enabled = false;
            }
            else
            {
                btnBackToMain.Enabled = true;
            }
        }

        private void UpdateActive()
        {
            RadToolTipManager.TargetControls.Clear();
            STATSModel.SessionType CurrentSessionType = STATSModel.SessionType.GetByName("Study Hall - CATS");

            // query database for active users

            List<STATSModel.Attendance> StudyHallAttendances = new List<STATSModel.Attendance>();

            STATSModel.Attendance[] eAttendances = STATSModel.Attendance.GetActive();
            foreach (var eAttendance in eAttendances)
            {
                var eSchedule = eAttendance.Schedule;
                var eSession = eSchedule.Session;
                var eSessionType = eSession.SessionType;
                if (eSessionType.SessionTypeID == CurrentSessionType.SessionTypeID)
                {
                    StudyHallAttendances.Add(eAttendance);
                }
            }

            grdStudyHallStudent.DataSource = null;

            List<AttendanceRow> arList = new List<AttendanceRow>();
            foreach (STATSModel.Attendance AttendanceObj in StudyHallAttendances)
            {
                STATSModel.Resource AssociatedResource = AttendanceObj.Schedule.Resource;
                String liText = AssociatedResource.ResourceNameLast + ", " + AssociatedResource.ResourceNameFirst + " [" + AttendanceObj.AttendanceTimeIn.ToShortTimeString() + "]";
                String liVal = AttendanceObj.AttendanceID.ToString();

                AttendanceRow ar = new AttendanceRow();
                ar.AttendanceID = AttendanceObj.AttendanceID;
                ar.AttendanceTimeIn = AttendanceObj.AttendanceTimeIn;
                ar.ResourceName = AssociatedResource.ResourceNameLast + ", " + AssociatedResource.ResourceNameFirst;
                ar.ResourceID = AssociatedResource.ResourceID;
                arList.Add(ar);

            }

            grdStudyHallStudent.DataSource = arList;
            lblNumberLoggedIn.Text = arList.Count.ToString();

            upList.Update();
        }

        protected void RadToolTipManager_OnAjaxUpdate(object sender, ToolTipUpdateEventArgs args)
        {
            UK.STATS.ReusableServerControls.ResourceInfoPopup infoPopup = new ReusableServerControls.ResourceInfoPopup();
            infoPopup.ResourceID = Guid.Parse(args.Value);
            args.UpdatePanel.ContentTemplateContainer.Controls.Add(infoPopup);
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            String ResourceLogin = txtResourceLogin.Text;
            Boolean isValid = STATSModel.Resource.Exists(ResourceLogin);

            if (isValid)
            {
                lblError.Text = "";
                STATSModel.Resource CurrentResource = STATSModel.Resource.Get(ResourceLogin);
                STATSModel.SessionType CurrentSessionType = STATSModel.SessionType.GetByName("Study Hall - CATS");

                // Check if the student is logged in somewhere else and ask them if they would like to continue logging in here
                Boolean isTutor = (CurrentResource.ResourceTypeID == ResourceTypeTutor.ResourceTypeID);
                if (CurrentResource.IsLoggedInElsewhere.Value == true && !isTutor)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Already Logged In", "<script language='javascript'> confirmProceed(confirm('You are already logged into a " + CurrentResource.IsLoggedInElsewhere.Key + " session. If you proceed you will be logged out of this session in order to log in here.'));</script>", false);
                }
                if (isTutor)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Tutors Cannot Login", "<script language='javascript'>alert('Tutors are not allowed to login to Study Hall.');</script>", false);
                    txtResourceLogin.Text = "";
                    return;
                }

                //STATSModel.Attendance CurrentAttendance = CurrentResource.Login(CurrentSessionType, false, Request.UserHostAddress);
                pLoginStudents.Visible = false;
                hfPageToggle.Value = "false";
                pViewMessages.Visible = true;
                BindAndShow(ResourceLogin);

                //txtResourceLogin.Text = "";
                UpdateActive();
            }
            else
            {
                lblError.Text = "The entered username was invalid.<br />Please try again.";
                pLoginStudents.Visible = true;
                hfPageToggle.Value = "true";
                pViewMessages.Visible = false;
            }
            upPage.Update();
        }

        protected void btnPrintSchedule_Click(object sender, EventArgs e)
        {
        }

        protected void btnContinueLogin_Click(object sender, EventArgs e)
        {
            STATSModel.Resource CurrentResource = STATSModel.Resource.Get(txtResourceLogin.Text);
            STATSModel.SessionType CurrentSessionType = STATSModel.SessionType.GetByName("Study Hall - CATS");
            txtResourceLogin.Text = "";
            STATSModel.Attendance CurrentAttendance = CurrentResource.Login(CurrentSessionType, false, Request.UserHostAddress, ResourceTypeTutor, ResourceTypeStudent);

            //UpdateActive();

            pLoginStudents.Visible = true;
            hfPageToggle.Value = "true";
            pViewMessages.Visible = false;

            Response.Redirect("Default.aspx");
        }

        public void BindAndShow(String ResourceLogin)
        {
            /********************* TIME TRACKER ***********************************/
            STATSModel.Resource ResourceObj = STATSModel.Resource.Get(ResourceLogin);
            String ResourceName = ResourceObj.ResourceNameFirst + " " + ResourceObj.ResourceNameLast;
            String DateRange = this._dateStart.ToShortDateString() + " - " + this._dateEnd.ToShortDateString();
            Guid ResourceID = ResourceObj.ResourceID;

            ifTimeTracker.Attributes["src"] = WebHost + "./TimeTrackerPage.aspx?resourceid=" + ResourceID;
            ifMessage.Attributes["src"] = WebHost + "./MessagePage.aspx?resourceid=" + ResourceID;
            hpPrintSchedule.NavigateUrl = WebHost + "./PrintSchedule.aspx?resourceid=" + ResourceID;
            if (STATSModel.MessageInbox.NumberMessagesUnread(ResourceObj) > 0)
            {
                btnBackToMain.Enabled = false;
            }
            else
            {
                btnBackToMain.Enabled = true;
            }
        }

        protected void btnLogoutStudent_Click(object sender, EventArgs e)
        {
            if (grdStudyHallStudent.SelectedItems.Count >= 0)
            {
                GridDataItem item = (GridDataItem)grdStudyHallStudent.SelectedItems[0];
                STATSModel.Attendance AttendanceObj = STATSModel.Attendance.Get(Guid.Parse(item["AttendanceID"].Text.ToString()));
                Guid ResourceID = Guid.Parse(item["ResourceID"].Text.ToString());

                AttendanceObj.LogOut(ResourceTypeTutor, ResourceTypeStudent);
                lblError.Text = "";
                rwLogin.NavigateUrl = WebHost + "./TimeTrackerPage.aspx?resourceid=" + ResourceID.ToString();
                rwLogin.Width = new Unit(700, UnitType.Pixel);
                rwLogin.Height = new Unit(350, UnitType.Pixel);
                rwLogin.VisibleOnPageLoad = true;
            }
            else
            {
                lblError.Text = "You must select a user to be logged out.";
            }

            UpdateActive();
            grdStudyHallStudent.Rebind();
        }

        //DM - Changed to only load the WebHost variable from the database once per page load instead of four times
        private string _webHost;

        public String WebHost
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_webHost))
                {
                    STATSModel.Setting eSettingHost = STATSModel.Setting.GetByKey("WebHost");
                    _webHost = eSettingHost.SettingValue;
                }

                return _webHost;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ForceReloadPage();
        }

        protected void grdStudyHallStudent_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            UpdateActive();
        }

        protected void grdStudyHallStudent_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                AttendanceRow AttendanceObj = (AttendanceRow)e.Item.DataItem;

                GridDataItem gridItem = e.Item as GridDataItem;
                RadToolTipManager.TargetControls.Add(gridItem.ClientID, AttendanceObj.ResourceID.ToString(), true);
            }
        }

        public class AttendanceRow
        {
            public Guid AttendanceID { get; set; }
            public Guid ResourceID { get; set; }
            public string ResourceName { get; set; }
            public DateTime AttendanceTimeIn { get; set; }

        }
    }
}